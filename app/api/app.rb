# rubocop:disable Style/FrozenStringLiteralComment

require "graphql/client"
require "github/tagging_helper"
require "github/pi_link_collection"
require "forwardable"

# Core Sinatra controller for all API requests.
class Api::App < Sinatra::Base
  DefaultMediaTypes      = %w(application/json).freeze
  ParamsExcludedFromLogs = %w(captures splat).freeze
  AcceptedSortOrderings  = %w(asc desc).freeze

  DEFAULT_PER_PAGE = 30
  MAX_PER_PAGE     = 100

  class << self
    attr_accessor :acceptable_media_types
  end

  extend Forwardable
  include Scientist
  include Platform::Authorization

  # Wrap the method from `Platform::Authorization` with tracing for Lightstep
  def access_allowed?(verb, options = {})
    GitHub.tracer.with_span("access_allowed.#{verb}") do |span|
      begin
        result = super
      ensure
        span.set_tag("authorized", result.inspect)
      end
    end
  end

  # Wrap sinatra `before do` blocks with tracing
  def self.before(*args, **options, &block)
    path = args.first
    source_location = caller(1, 1).first
    super do
      GitHub.tracer.with_span("before_block") do |span|
        if path
          span.set_tag("before_path", path)
        end
        span.set_tag("source_location", source_location)
        instance_eval(&block)
      end
    end
  end

  # See GitHub::ServiceMapping#push_service_mapping_context
  def service_mapping_method
    env["github.api.route"]
  end

  include RequestMethodHelper

  register Api::App::HttpMethods

  include Api::App::HmacDependency
  include Api::App::InputDependency
  include Api::App::DeliveryDependency
  include Api::App::FindersDependency
  include Api::App::RateLimitDependency
  include Api::App::ErrorDependency
  include Api::App::HelpersDependency
  include Api::App::FeatureFlagsDependency
  include Api::App::PollIntervalDependency
  include Api::App::AreasOfResponsibilityDependency
  include Api::App::AuditDependency
  include Api::App::CursorPaginationDependency
  include Api::App::StatsDependency
  include Api::App::DeprecationDependency

  include GitHub::ServiceMapping

  disable :show_exceptions
  disable :protection

  use ActionDispatch::Executor, ActiveSupport::Executor # Installs the Executor so that we can use Rails Query Cache
  use Api::Middleware::RouteFinder
  use Api::Middleware::RequestAuthenticationFingerprint
  use Api::Middleware::EnforceMediaType
  use Api::Middleware::Cors
  use Api::Middleware::Limiting,
      GitHub::Limiters::ByPath.new("/limittown"),
      Api::Limiters::AuthenticationFingerprintByPath.new(max: GitHub.authentication_fingerprint_by_path_max),
      Api::Limiters::ElapsedTimeByAuthenticationFingerprint.new(max: GitHub.elapsed_time_by_authentication_fingerprint_max),
      Api::Limiters::ElapsedAuthenticatedTimeByPath.new(max: GitHub.elapsed_authenticated_time_by_path_graphql_max, path: "/graphql"),
      Api::Limiters::SearchElapsedTime.new(max: GitHub.search_elapsed_time_max, ttl: GitHub.search_elapsed_time_ttl),
      Api::Limiters::GraphQLAuthenticationFingerprint.new(max: GitHub.graphql_authentication_fingerprint_max),
      Api::Limiters::TwirpAuthenticationFingerprintByPath.new(max: GitHub.twirp_authentication_fingerprint_by_path_max),
      Api::Limiters::SearchIpAddress.new(max: GitHub.search_ip_address_max, ttl: GitHub.search_ip_address_ttl),
      Api::Limiters::ConcurrentAuthenticationFingerprint.new(
        max: GitHub.concurrent_authentication_fingerprint_max,
        ttl: GitHub.concurrent_authentication_fingerprint_ttl,
      )
  use Api::Middleware::Limiting::Disabled
  use Api::Middleware::DatabaseSelection
  use Api::Middleware::SilenceEventsAndNotificationsBlacklist

  attr_reader :meta

  # Save some bytes and send a very terse and effective policy.
  SecureHeaders::Configuration.override(:api) do |config|
    config.csp = {
      default_src: [SecureHeaders::CSP::NONE],
      script_src: SecureHeaders::OPT_OUT,
    }
    # N/A for API clients
    config.expect_certificate_transparency = SecureHeaders::OPT_OUT
  end

  class PlatformExecute
    def self.execute(document:, operation_name: nil, variables: {}, context: {})
      target = context.delete(:target) || :internal
      Platform.execute(document, target: target, context: context, variables: variables, raise_exceptions: true).to_h
    end
  end

  PlatformClient = GraphQL::Client.new(
    schema: Platform::Schema,
    execute: PlatformExecute,
    enforce_collocated_callers: ENV["FASTDEV"] ? false : (Rails.development? || Rails.test?),
  )

  PlatformTypes = PlatformClient.types

  class TimeoutHandler
    def initialize(controller)
      @controller = controller
    end

    def timeout(_env)
      @controller.increment_rate_limit_and_set_headers!
    end
  end

  before do
    timeout_handler = TimeoutHandler.new(self)
    GitHub::TimeoutMiddleware.notify(env, timeout_handler)
    SecureHeaders.use_secure_headers_override(request, :api)
    @request_start_time = Time.now
    @original_time_zone = Time.zone
    @meta  = {}
    @current_url = url_without_query(env)
    @links = PiLinkCollection.new @current_url, env["rack.request.query_hash"]
    @oauth = @current_repo = @pagination = nil

    initialize_marginalia
    initialize_context
    initialize_log_data
    initialize_failbot
    initialize_sensitive_data
    initialize_areas_of_responsibility
    push_service_mapping_context
    initialize_audit_context
    populate_context_with_authentication_details
    populate_log_data_with_authentication_details
    populate_audit_context_with_authentication_details
    check_whitelist_for_request_limiting
    initialize_trace_data

    @diff_custom_timeout_before = GitHub::Diff.custom_default_timeout
    GitHub::Diff.custom_default_timeout = GitRPC.timeout unless GitRPC.timeout.nil?

    ensure_acceptable_media_types!

    ensure_request_is_within_rate_limit!

    protect_access_to_garage_hosts
    protect_access_to_enterprise_hosts
    protect_access_to_unbootstrapped_private_instance
  end

  before "/repositories/:repository_id/?*" do
    if path_includes_relocated_repo?
      redirect_to_new_repo_location_or_404! do |path, requested_nwo, redirected_repo|
        path.sub %r(\A/repositories/\d+), "/repositories/#{redirected_repo.id}"
      end
    end

    if find_repo
      ensure_repo_is_accessible
      ensure_route_on_advisory_workspace_whitelist if current_repo&.advisory_workspace?
    end
  end

  def ensure_repo_is_accessible
    if current_repo_disabled_or_blocked?
      current_repo.disabled_access_reason # preload association prior to serialization

      deliver_blocked_repo_error!(current_repo) if current_repo.access.dmca?
      deliver_disabled_repo_error!(current_repo)
    end

    if !current_repo.active? ||  current_repo.hide_from_user?(current_user)
      deliver_error!(missing_repository_status_code)
    end

    current_repo.network_broken?
  rescue Repository::NetworkMissingError => e
    Failbot.report e

    deliver_disabled_repo_error!(current_repo)
  end

  def ensure_route_on_advisory_workspace_whitelist
    deliver_error!(404) unless
      AdvisoryWorkspaceWhitelist.whitelisted?(current_repo, route_pattern, request.request_method)
  end

  def route_missing
    deliver_error(404)
  end

  after do
    GitHub::Diff.custom_default_timeout = @diff_custom_timeout_before
    Time.zone = @original_time_zone
    increment_rate_limit_and_set_headers!
    record_stats
    finalize_log_data
    finalize_trace_data
    finalize_hydro_context
  end

  # Helper methods to determine if the current request is being made by a robot,
  # and if so, which one.
  def robot?
    GitHub.robot?(request.user_agent.to_s)
  end

  def robot_type
    GitHub.robot_type(request.user_agent.to_s)
  end

  def password_auth_deprecated?
    true
  end

  # We still allow users to auth to the API via password auth
  # during the deprecation warning period
  def password_auth_blocked?
    false
  end

  # Internal
  #
  # Returns nothing.
  def initialize_marginalia
    Marginalia::Comment.clear!
    Marginalia::Comment.update!(self)
  end

  # Internal: Populates Failbot with basic information about the current
  # request.
  #
  # Returns nothing.
  def initialize_failbot
    context = {
      accept: env["HTTP_ACCEPT"],
      api_route: proc { route_pattern },
      connections: ApplicationRecord.connection_info,
      controller: self.class.to_s,
      language: env["HTTP_ACCEPT_LANGUAGE"],
      master_pid: Process.ppid,
      master_started_at: GitHub.unicorn_master_start_time,
      method: env["REQUEST_METHOD"],
      params: proc { params_for_logging },
      private_repo: proc { current_repo_loaded? && current_repo.try(:private?) },
      rails: Rails.version,
      repo_id: proc { current_repo_loaded? && current_repo.try(:id) },
      request_category: request_category,
      request_id: env["HTTP_X_GITHUB_REQUEST_ID"],
      request_wait_time: env[GitHub::TaggingHelper::REQ_WAIT_TIME],
      route: proc { current_repo_loaded? && (current_repo.try(:route) rescue nil) },
      server_id: Rack::ServerId.get(env),
      user: proc { current_user.to_s },
      user_agent: env["HTTP_USER_AGENT"],
      worker_pid: Process.pid,
      worker_request_count: GitHub.unicorn_worker_request_count,
      worker_started_at: GitHub.unicorn_worker_start_time,
    }
    context[:robot] = robot_type if robot?
    ::Failbot.push(context)
  end

  # Internal: Track sensitive data in this request.
  #
  # Returns nothing.
  def initialize_sensitive_data
    context = {
      # repo name is sensitive when it's private
      repo: proc { current_repo_loaded? && current_repo.private? && current_repo.name_with_owner },
      remote_ip: remote_ip,
    }

    SensitiveData.context.push(context)
  end

  # Internal: Populates GitHub.context with basic information about the current
  # request.
  #
  # Returns nothing.
  def initialize_context
    # WARNING: Do not call any methods that can cause the request to
    # short-circuit  (e.g., don't call any methods that attempt to authenticate
    # the request). Doing so will prevent us from fully populating the context.
    context = {
      actor_ip: remote_ip,
      user_agent: request.user_agent.to_s,
      controller: self.class.to_s,
      from: "%s#%s" % [self.class, request.request_method],
      connections: ApplicationRecord.connection_info,
      referrer: request.env["HTTP_REFERER"],
      request_id: request.env["HTTP_X_GITHUB_REQUEST_ID"],
      request_method: request.env["REQUEST_METHOD"].downcase,
      request_category: request_category,
      api_route: proc { route_pattern },
      server_id: Rack::ServerId.get(request.env),
      version: medias.to_api_version,
    }

    context[:robot] = robot_type if robot?

    # Leave this here instead of placing it in Rack::RequestLogger so that we
    # can keep the middleware lightweight.
    unless request.query_string.empty?
      context[:query_string] = query_string_for_logging
    end

    GitHub.context.push(context)
  end

  # Internal
  #
  # Returns nothing.
  def populate_context_with_authentication_details
    context = { auth: detect_auth }
    context[:current_user] = current_user.to_s if logged_in?
    context[:oauth_application_id] = current_app.id if current_app
    context[:integration_id] = current_integration.id if current_integration
    context[:installation_id] = current_integration_installation.id if current_integration_installation
    context[:parent_installation_id] = current_parent_integration_installation.id if current_parent_integration_installation
    context[:enterprise_installation_id] = current_enterprise_installation.id if current_enterprise_installation
    if logged_in? && current_user.oauth_access
      context[:oauth_scopes] = current_user.oauth_access.scopes_string
      context[:oauth_access_id] = current_user.oauth_access.id
    end

    GitHub.context.push context
  end

  # Internal: A Hash of data to be logged that is stored in the request env.
  # This will eventually be logged by Rack::RequestLogger.
  #
  # Returns a Hash.
  def log_data
    env[Rack::RequestLogger::APPLICATION_LOG_DATA] ||= GitHub::Logger.empty
  end

  # Internal: Store basic information to be logged for the current request.
  #
  # Returns nothing.
  def initialize_log_data
    context = GitHub.context.to_hash

    # WARNING: Do not call any methods that can cause the request to
    # short-circuit  (e.g., don't call any methods that attempt to authenticate
    # the request). Doing so will prevent us from fully populating the logging
    # context.
    context_log_data = {
      controller: context[:controller],
      path_info: context[:path_info],
      query_string: context[:query_string],
      version: context[:version],
    }
    log_data.merge!(context_log_data)
  end

  # Internal: Store information to be logged about the authenticated actor.
  #
  # Returns nothing.
  def populate_log_data_with_authentication_details
    context = GitHub.context.to_hash
    log_data.merge!(
      auth: context[:auth],
      current_user: context[:current_user],
      oauth_access_id: context[:oauth_access_id],
      oauth_application_id: context[:oauth_application_id],
      oauth_scopes: context[:oauth_scopes],
      integration_id: context[:integration_id],
      installation_id: context[:installation_id],
      parent_installation_id: context[:parent_installation_id],
    )
  end

  # Internal: Add data only available at the end of the request to log_data.
  #
  # Returns nothing.
  def finalize_log_data
    log_route_pattern
    log_org_owned_resource_info
    log_rate_limit_info
    log_repo_info
  end

  # Mark request as whitelisted from request limiting by setting an env
  # variable if the request matches whitelist rules.
  def check_whitelist_for_request_limiting
    if logged_in? && current_user.rate_limit_exempt_user?
      GitHub::Limiters::Middleware.skip_limit_checks(env)
    end
  end

  # Internal: Add API-specific data to the span that represents this request.
  #
  # Returns nothing.
  def initialize_trace_data
    Rack::OctoTracer.get_span(request.env).when_enabled do |span|
      span.set_tag(GitHub::TaggingHelper::COMPONENT_TAG, "api")
      span.set_tag(GitHub::TaggingHelper::CONTROLLER_TAG, GitHub::TaggingHelper.controller(request.env))
      span.set_tag(GitHub::TaggingHelper::ACTION_TAG, route_pattern)
      span.set_tag(GitHub::TaggingHelper::CATEGORY_TAG, GitHub::TaggingHelper.category(request.env))
      span.set_tag(GitHub::TaggingHelper::RAILS_VERSION_TAG, GitHub::TaggingHelper.rails_version)
      span.set_tag(GitHub::TaggingHelper::LOGGED_IN_TAG, GitHub::TaggingHelper.logged_in(request.env))
      span.set_tag(GitHub::TaggingHelper::ROBOT_TAG, GitHub::TaggingHelper.robot_name(request.env))
      span.set_tag(GitHub::TaggingHelper::PJAX_TAG, GitHub::TaggingHelper.pjax(request.env))
      accept_headers = request.accept.map(&:entry).join(",")
      span.set_tag("http.accept", accept_headers)
    end
  end

  # Internal: Add data only available at the end of the request to the span
  # that represents this request.
  #
  # Returns nothing.
  def finalize_trace_data
    Rack::OctoTracer.get_span(request.env).when_enabled do |span|
      span.operation_name = "#{request.request_method} #{route_pattern}"
    end
  end

  # Returns the elapsed time for the request in seconds
  def request_elapsed_time
    return nil if @request_start_time.blank?

    Time.now - @request_start_time
  end

  # Returns the elapsed time for the request in milliseconds
  def request_elapsed_time_in_ms
    if elapsed = request_elapsed_time
      elapsed * 1000
    end
  end

  def record_stats
    GitHub::Stats::Api.record \
      version: medias.to_api_version,
      auth: detect_auth,
      call: route_pattern,
      method: env["REQUEST_METHOD"],
      status: response.status.to_i,
      elapsed: request_elapsed_time_in_ms,
      query_time: GitHub::MysqlInstrumenter.query_time * 1000,
      transaction_times: GitHub::MysqlInstrumenter.transaction_times.map { |t| t * 1000 }
  end

  # Returns the abstract URI pattern for the Sinatra route that is
  # processing the current request.
  #
  # The route pattern is not available until the route gets executed (e.g., it's
  # not available inside a Sinatra `before` block).
  #
  # This method requires https://github.com/sinatra/sinatra/pull/702 or a local
  # patch.
  #
  # env - Hash request environment.
  #
  # Examples
  #
  #   route_pattern
  #   => "/"
  #
  #   route_pattern
  #   => "/repositories/:repository_id/issues/:id"
  #
  # Returns a String, or nil if the route pattern has not yet been determined
  # for the request.
  def self.route_pattern(env)
    route = env["sinatra.route"] || env["github.api.route"]
    return nil unless route

    route.split[1]
  end

  def route_pattern
    self.class.route_pattern(env)
  end

  def detect_auth
    return :anon if anonymous_request?
    # Some auth types are only relevent when a user is authenticated and some
    # are only valid when an application is authenticated (using their
    # application credentials)
    if logged_in?
      if current_user.oauth_access
        return :integration if current_integration.present?
        return :personal_access_token if current_app.blank?
        return :oauth
      else
        return :integration_installation if current_integration_installation.present?
        return :basic if request_credentials.login_password_present?
      end
    else
      return :oauth_key_secret if current_app.present?
    end
    :unclassified
  end

  error GitRPC::RepositoryOffline do
    failbot env, app: "github-unrouted"

    deliver_error! 503, message: "Repository offline"
  end

  error GitRPC::InvalidRepository do
    Failbot.push app: "github-unrouted"
    Failbot.report env["sinatra.error"]

    message = "Unable to read Git repository contents. " \
              "We've notified our support staff. If this error " \
              "persists, or if you have any questions, please " \
              "contact us."

    deliver_error! 500,
      message: message,
      documentation_url: "https://github.com/contact"
  end

  error GitRPC::BadObjectState do
    Failbot.report_user_error(env["sinatra.error"])

    deliver_error! 500,
      message: "There is a problem with this repository on disk. Please contact support for additional information or help."
  end

  error do
    error = env["sinatra.error"]
    message = error&.message

    if message !~ /#{OrganizationInvitation::CREATED_TOO_QUICKLY_ERROR_MESSAGE}/
      failbot!(env)
    end

    log_exception(error)
  end

  # Log an exception to Failbot.
  #
  # env     - The Rack environment Hash.
  # options - The Hash of options to add to the Failbot payload (default: {}).
  #           :exception - The exception that will be provided to Failbot
  #                        (optional).
  #
  # Examples
  #
  #   # Gets the exception from the env['sinatra.error'] & reports it to Failbot
  #   failbot(env)
  #
  #   # Reports the given exception (+boom+) to Failbot
  #   failbot(env, :exception => boom)
  #
  # Returns nothing.
  def failbot(env, options = {})
    options_for_failbot = options.dup

    begin
      err = options_for_failbot.delete(:exception) || env["sinatra.error"]
      ::Failbot.report(err, options_for_failbot.update(
        time: ((Time.now - @request_start_time) rescue "N/A"),
      ))
    rescue
      puts $!
    end
  end

  # Logs an exception to Failbot and delivers an error (HTTP 500) as the
  # API response.
  #
  # See +failbot+ for more details.
  #
  # Returns nothing.
  def failbot!(env, options = {})
    failbot(env, options)
    deliver_error 500
  end

  # Internal: Filters sensitive data (if any) from the URL, to provide a URL
  # that is suitable for logging.
  #
  # Examples
  #
  #   # GET https://api.github.com/users?client_id=abc&client_secret=def
  #   query_string_for_logging
  #   # => "https://api.github.com/users?client_id=abc&client_secret=-FILTERED-"
  #
  # Returns a String.
  def url_for_logging
    return request.url if request.query_string.empty?

    "#{GitHub.api_url}#{request.path}?#{query_string_for_logging}"
  end

  # Internal: Filters sensitive data (if any) from the query string, to provide
  # a query string that is suitable for logging.
  #
  # Examples
  #
  #   # GET /users?client_id=abc&client_secret=def
  #   query_string_for_logging
  #   # => "client_id=abc&client_secret=-FILTERED-"
  #
  # Returns a String, or nil if the request has no query string.
  def query_string_for_logging
    return nil if request.query_string.empty?

    uri = Addressable::URI.parse(request.url)
    unfiltered_query_values = uri.query_values || {}
    uri.query_values =
      filter_sensitive_params_for_logging(unfiltered_query_values)
    uri.query
  rescue URI::InvalidURIError
    # This is unlikely to happen, since `request.url` was already accepted by the HTTP and application layers,
    # but if it does, don't raise this error since it might contain sensitive information.
    raise ArgumentError, "Failed to parse `request.url` for extracting the query string"
  end

  # Internal: Filters sensitive data (if any) from the request parameters, to
  # provide a parameter Hash that is suitable for logging. (The request
  # parameters include parameters provided via the query string and parameters
  # provided via the request body.)
  #
  # Returns a Hash.
  def params_for_logging
    without_noisy_params = reject_noisy_params_for_logging(params)

    filter_sensitive_params_for_logging(without_noisy_params)
  end

  def platform_execute(operation, variables: {}, target: :internal, force_readonly: false)
    PlatformClient.query(operation, context: platform_context.merge(target: target, force_readonly: force_readonly), variables: variables)
  end

  def platform_context
    {
      viewer:                        current_user,
      oauth_app:                     current_app,
      integration:                   current_integration,
      installation:                  current_integration_installation,
      actor:                         current_actor,
      feature_flags:                 request.env["HTTP_GRAPHQL_FEATURES"].to_s.split(",").map(&:to_sym),
      granted_oauth_scopes:          current_user.try(:oauth_access).try(:scopes),
      origin:                        :rest_api,
      request_token:                 request_credentials.token,
      response:                      response,
      rate_limit_configuration:      rate_limit_configuration,
      user_agent:                    request.env["HTTP_USER_AGENT"],
      internal_ip_request:           internal_ip_request?,
      real_ip:                       request.env["HTTP_X_REAL_IP"],
      forwarded_for:                 request.env["HTTP_X_FORWARDED_FOR"],
      ip:                            request.ip,
      unauthorized_organization_ids: protected_organization_ids,
    }
  end

  # Internal
  def reject_noisy_params_for_logging(params)
    params.reject { |k, _| ParamsExcludedFromLogs.include?(k) }
  end

  # Internal
  def filter_sensitive_params_for_logging(params)
    sensitive_params_filter.filter(params)
  end

  def sensitive_params_filter
    @sensitive_params_filter ||= GitHub::ParameterFilter.create
  end

  # Indicates if the request has requested pagination (regardless of if the method supports
  # or requires pagination).
  def wants_pagination?
    params[:page] || params[:per_page]
  end

  # Public: Paginator deals with pagination logic.
  #
  # Returns the cached REST::Paginator or initializes a new one with the class default values
  def paginator
    return @paginator if @paginator
    @paginator = build_paginator
  end

  # Public: Initialize and cache a new paginator object.
  #
  # Returns the newly initialized paginator
  def build_paginator(default_per_page: self.class::DEFAULT_PER_PAGE, max_per_page: self.class::MAX_PER_PAGE)
    options = {
      page: params[:page],
      per_page: params[:per_page],
      default_per_page: default_per_page,
      max_per_page: max_per_page,
    }
    REST::Paginator.new(**options)
  end

  def_delegators :paginator, :per_page, :default_per_page, :max_per_page

  def current_page
    paginator.page
  end

  # Public: Pagination parameters to pass to pagination methods.
  #
  # per_page - Integer number of records to fetch.
  # page     - The Integer page of records to fetch.
  #
  # Returns a Hash.
  def pagination
    @pagination = {
      per_page: paginator.per_page,
      page: paginator.page,
    }

    @pagination.update total_entries: pagination_capped_to_n_entries if pagination_capped_to_n_entries

    if (paginator.per_page * paginator.page) > WillPaginate::PageNumber::BIGINT
      deliver_error!(422, \
        message: "Pagination exceeds maximum limit of #{WillPaginate::PageNumber::BIGINT}", \
        documentation_url: "/rest/overview/resources-in-the-rest-api#pagination")
    end

    @pagination
  end

  def paginate_rel(rel, pagination_opts = nil)
    pagination_opts ||= pagination

    if rel.respond_to?(:limit)
      paginated_rel = rel.limit(pagination_opts[:per_page]).page(pagination_opts[:page])
      if pagination_opts[:total_entries]
        # We also need to unscope any set order here to avoid accidentally selecting the wrong
        # index, which could result in a slow query. As we're just interested in a count, the order
        # of results is irrelevant here.
        #
        # Also, the count is performed using a subquery to ensure that queries that make use of
        # `GROUP BY` and `HAVING` return the correct count.
        capped_count = rel.unscoped.from(
          rel.unscope(:order, :select).select("1 as one").limit(pagination_opts[:total_entries]),
        ).count
        paginated_rel.total_entries = capped_count
      end
      paginated_rel
    elsif rel.respond_to?(:paginate)
      rel.paginate(page: pagination_opts[:page], per_page: pagination_opts[:per_page])
    else
      WillPaginate::Collection.create(pagination_opts[:page], pagination_opts[:per_page],
                                      rel.length) do |pager|
        # cast nil, or, say, `Google::Protobuf::RepeatedField` types into `Array`
        pager.replace(Array(rel)[pager.offset, pager.per_page])
      end
    end
  end

  DEFAULT_PAGINATION_CAP = 40_000

  # Public: Declare the maximum number of entries returned by a method.
  #
  # entries - Integer number of entries.
  #
  # Halts with 422 if current pagination exceeds this limit.
  def cap_paginated_entries!(count = DEFAULT_PAGINATION_CAP)
    @pagination_capped_to_n_entries = count

    deliver_pagination_cap_exceeded! if count < (current_page * per_page)
  end

  # Internal: The maximum number of entries returned by the current
  # resource listing.
  #
  # Returns an Integer if the resource is capped, otherwise nil.
  def pagination_capped_to_n_entries
    @pagination_capped_to_n_entries
  end

  def protect_access_to_garage_hosts
    return unless GitHub.garage_unicorn?

    unless user_agent_allows_access_to_garage_hosts? || (logged_in? && current_user.site_admin?)
      deliver_error! 401, message: "Must authenticate to access this API."
    end
  end

  # A super secret string in your User-Agent header allows unauthenticated
  # testing of garage environments. App-level authentication is still required
  # for protected resources.
  def user_agent_allows_access_to_garage_hosts?
    request.user_agent =~ /#{GitHub.current_sha}/
  end

  def protect_access_to_enterprise_hosts
    return unless GitHub.private_mode_enabled?

    return if request.path_info == "/meta"
    return if request.path_info =~ /\A\/app-manifests\/[0-9a-f]+\/conversions\z/

    return if trusted_port? || authenticated_for_private_mode?

    deliver_error! 401, message: "Must authenticate to access this API."
  end

  # Prevent platform access to GitHub Private Instances before the initial
  # site administrator has completed the bootstrapping flow.
  def protect_access_to_unbootstrapped_private_instance
    return unless GitHub.private_instance?
    return if GitHub.private_instance_bootstrapper.bootstrapped?

    deliver_error! 403, message: "This GitHub Private Instance must be bootstrapped before you can access this API."
  end

  # Public: Determines whether the request is sufficiently authenticated to
  # access the API when running in Private Mode.
  #
  # For the vast majority of the API, being "authenticated" for Private Mode
  # means that you are authenticated as a *user*. So, in the default
  # implementation of this method, we consider the request to be authenticated
  # if it is authenticated as a user. For endpoints that allow other entities to
  # authenticate (e.g., OAuth Applications using client ID and secret), those
  # endpoints can override this method as appropriate to fit their definition of
  # "authenticated."
  #
  # See also: GitHub.private_mode_enabled?.
  #
  # Returns true if the request is authenticated; false otherwise.
  def authenticated_for_private_mode?
    logged_in?
  end

  # Internal: For a request to any route that includes the repository
  # name-with-owner in the path (e.g., `/repos/:user/:repo`,
  # `/teams/:id/repos/:user/:repo`), return the repository name-with-owner.
  #
  # Returns a String or nil.
  def repo_nwo_from_path
    find_repo_nwo_from_router || find_repo_nwo_from_path
  end

  # Internal: For a request that matches `/repos/:user/:repo/*`, return the
  # repository name-with-owner.
  #
  # Returns a String or nil.
  def find_repo_nwo_from_router
    # GitHub::Routers::Api takes any request to `/repos/:user/:repo/*` and
    # re-routes it to `/repositories/:id/*`. When doing so, GitHub::Routers::Api
    # also preserves the originally-requested repository name-with-owner in an
    # environment variable.
    env[GitHub::Routers::Api::ThisRepositoryNameWithOwnerKey]
  end

  # Internal: For a request to any route that includes the `:user` and `:repo`
  # as named path segments (e.g., `/teams/:id/repos/:user/:repo`), return the
  # repository name-with-owner.
  #
  # Returns a String or nil.
  def find_repo_nwo_from_path
    nwo = "#{params[:user] || params[:owner]}/#{params[:repo]}"

    return nwo if nwo =~ Repository::NAME_WITH_OWNER_PATTERN
  end

  # Internal: Is this request attempting to access a repository that has moved
  # to a new location (e.g., a renamed repository, a transferred repository)?
  #
  # Returns a Boolean.
  def path_includes_relocated_repo?
    find_redirected_repo_from_path.present?
  end

  # Internal: If this request is attempting to access a repository that has
  # moved to a new location, find the relocated repository.
  #
  # Returns a Repository or nil.
  def find_redirected_repo_from_path
    if repo_nwo_from_path

      # If a repo exists for the requested name-with-owner, then don't check for
      # a redirect.
      return if Repository.nwo(repo_nwo_from_path)

      RepositoryRedirect.find_redirected_repository(repo_nwo_from_path)
    end
  end

  # Internal: Redirect the request to the repository's new location.
  #
  # block - A block that returns the path to use for the redirect. The block
  #         receives the following arguments to assist in constructing the
  #         redirect path:
  #         :path            - A String representing the full path for this
  #                            request.
  #         :requested_nwo   - A String representing the repository
  #                            name-with-owner that this request is attempting
  #                            to access.
  #         :redirected_repo - The relocated Repository (that previously existed
  #                            at the requested name-with-owner location).
  #
  # Halts with a redirect if the requested repository has relocated (e.g., if it
  #   has been renamed, if it has been transferred, etc.).
  # Halts with a 404 if the request lacks permission to access the relocated
  #   repository.
  # Returns nothing.
  def redirect_to_new_repo_location_or_404!(&block)
    redirected_repo = find_redirected_repo_from_path

    env[GitHub::Routers::Api::ThisRepositoryKey] = redirected_repo

    if access_allowed?(:follow_repo_redirect, resource: redirected_repo, allow_integrations: true, allow_user_via_integration: true)
      redirect_path =
        block.call(request.fullpath, repo_nwo_from_path, redirected_repo)

      deliver_redirect! \
        api_url(redirect_path), status: permanent_redirect_status_code
    else
      deliver_error!(missing_repository_status_code)
    end
  end

  # Internal: The default status code to use if the repository for the current
  # request does not exist, or the user has no access to it.
  #
  # Returns an Integer.
  def missing_repository_status_code
    404
  end

  # Internal: The default status code to use if responding to this request with
  # a permanent redirect.
  #
  # For HEAD and GET requests, use a "301 Moved Permanently". When an HTTP
  # client follows the redirect, it will reuse the original HTTP verb.
  #
  # For other HTTP verbs, use a "307 Temporary Redirect". (If we used a 301,
  # most HTTP clients would change the HTTP verb to GET when following the
  # redirect.)
  #
  # Returns an Integer.
  def permanent_redirect_status_code
    (request.get? || request.head?) ? 301 : 307
  end

  # Sets one or more custom media types that API Apps may accept.
  #
  # *types - One or more String media types.
  #
  # Returns nothing.
  def self.allow_media(*types)
    (self.acceptable_media_types ||= []).push(*types)
  end

  # Public: Get the GitHub-accepted media types from the request's accepted
  # media types.
  #
  # The API accepts "application/json" or any "application/vnd.github*" type.
  #
  # Returns an array of Api::MediaType.
  def medias
    @medias ||= Api::AcceptedMediaTypes.new(request.accept, request.env["PATH_INFO"])
  end

  def content_media
    @content_media ||= Api::App::EnforceMediaType.current(env)
  end

  def allow_media(*types)
    (@acceptable_media_types ||= []).push(*types)
  end

  def acceptable_media_types
    acceptable_types = Array(self.class.acceptable_media_types)

    if @acceptable_media_types
      acceptable_types.unshift(*@acceptable_media_types)
    end

    acceptable_types
  end

  # Checks to see if the current request's acceptable media types are valid.
  # Typically we only care about JSON, but some API classes might want to
  # support other types (like Atom).
  #
  # Returns true if the API response is not acceptable to the client, or false.
  def unacceptable_media_types?
    unless medias.acceptable?
      types = DefaultMediaTypes.dup
      types.push(*acceptable_media_types)
      !request.preferred_type(*types)
    end
  end

  # Public: Gets the preview headers passed as part of the request
  #
  # Returns an array of symbols.
  def schema_previews
    medias.api_versions
  end

  # Public: Gets the user agent for the request.
  #
  # Returns an Api::UserAgent.
  def user_agent
    @user_agent ||= Api::UserAgent.new(request.user_agent)
  end

  # Public: Gets the port the request is coming in on. This is so we
  # can limit particular API namespaces to only work for requests
  # coming in on trusted ports.
  #
  # Returns a String port.
  def server_port
    env["SERVER_PORT"]
  end

  # Public: Determines whether the port the request is coming in on
  # is a trusted port.
  def trusted_port?
    GitHub.trusted_ports_enabled? && GitHub.trusted_ports.include?(server_port) && %w(127.0.0.1 ::1).include?(remote_ip)
  end

  # Public: Determines originating IP address.
  def remote_ip
    env["api.remote_ip"] ||= request.ip
  end

  def request_reflog_data(via)
    {
      real_ip: remote_ip,
      repo_name: current_repo.name_with_owner,
      repo_public: current_repo.public?,
      user_login: current_user.login,
      user_agent: request.user_agent,
      from: GitHub.context[:from],
      via: via,
    }
  end

  # Internal: Log exception.
  #
  # exception - The Exception object to log.
  # data      - A Hash of key/value pairs to log in addition to the standard
  #             logging data (default: {}).
  #
  # Returns nothing.
  def log_exception(e, data = {})
    log_exception_to_stdout(e) if Rails.test?

    exception_logging_context = GitHub.context.to_hash.slice(:now, :request_id, :server_id)
    GitHub::Logger.log_exception(exception_logging_context.merge(data), e)
  end

  # Internal: Log exception to STDOUT.
  #
  # exception - The Exception object to log.
  #
  # Returns nothing.
  def log_exception_to_stdout(exception)
    puts exception.message
    pp exception.backtrace
  end

  # Internal: Instrument an API call failing because of content authorization
  # errors.
  #
  # error - The ContentAuthorizationError instance
  #
  # Returns nothing.
  def instrument_content_authorization_failure(error:)
    GitHub.dogstats.increment "error.content_authorization", tags: ["via:api", "error:#{error.name}", "route:#{graph_friendly_route_key}"]

    log_data[:content_authorization_failure] = error.name
  end

  # Internal: Adds current route pattern to log context.
  #
  # Returns nothing.
  def log_route_pattern
    log_data[:route] = route_pattern
  end

  # Internal: Adds org and OAuth-related details to log context for org-owned
  # resources.
  #
  # Returns nothing.
  def log_org_owned_resource_info
    if org = current_resource_owner
      log_data[:org] = org.to_s
      if logged_in? && current_user.using_oauth?
        oauth_party = if current_app.nil?
          "personal"
        elsif current_app.owned_by?(org)
          "first"
        else
          "third"
        end
        log_data[:oauth_party] = oauth_party
      end
    end
  end

  # Internal: Adds current rate limit info to log context.
  #
  # Returns nothing.
  def log_rate_limit_info
    if @rate
      @rate.update_logs(log_data)
    end
  end

  # Internal: Adds name and visibility for current repo (if any) to log
  # context.
  #
  # Returns nothing.
  def log_repo_info
    if repo = find_repo
      log_data.update({
        repo: repo.name_with_owner,
        repo_visibility: repo.visibility,
      })
    end
  end

  # for mysql hints.
  # see config/initializers/marginalia
  def marginalia_route
    @marginalia_route ||= "%s#%s" %
      [self.class.name.underscore, request.request_method.downcase]
  end

  # Human-readable route descriptor for use in tracking.
  #
  # Returns a String, e.g. "POST_repositories_REPOSITORY_ID_commits_SPLAT_comments"
  def graph_friendly_route_key
    @graph_friendly_route_key ||= begin
      [request.request_method,
       route_pattern.gsub(/\/:([\w_]+)\/?/) { |id| id.upcase }.
                     gsub("/", "_").
                     gsub("*", "SPLAT").
                     gsub(%r{[^a-zA-Z0-9_]}, "")].join
    end
  end

  def request_category
    REQUEST_CATEGORY
  end

  # no need to rebuild this string for _every_ AR query
  REQUEST_CATEGORY = "api".freeze

  # Helper for returning a proper API URL for the given relative path.
  #
  # path - Relative API path as String
  #
  # Examples:
  #
  #   Dotcom:
  #
  #   api_url("/foo/bar/baz")
  #   => "https://api.github.com/foo/bar/baz"
  #
  #   Enterprise:
  #
  #   api_url("/foo/bar/baz")
  #   => "https://ghe.io/api/v3/foo/bar/baz"
  #
  # Returns a String URL
  def api_url(path)
    Api::Serializer.url(path)
  end

  # Private: Build a web UI URL with the given path.
  #
  # path    - The path of the URL to build.
  # options - Query parameters to include in the URL.
  #           :auth - Whether to inclue a SignedAuthToken for the feed
  #                   (Default false).
  #
  # Returns a String URL.
  def html_url(path, options = {})
    if (_auth = options.delete(:auth))
      options[:token] = feed_token(path)
    end
    Api::Serializer.html_url path, options
  end

  # Private: Get a SignedAuthToken for an Atom feed with a given path.
  #
  # path - The path of the Atom feed the SignedAuthToken is for.
  #
  # Returns a String SignedAuthToken or nil if no user is logged in.
  def feed_token(path)
    return unless logged_in?
    GitHub::Authentication::Feed.token current_user, path
  end

  def instrument_non_integer_id(id)
    GitHub.dogstats.increment("error.non_integer_id", tags: ["via:api"])
    log_data[:non_integer_id] = id
  end

  # Get the timezone name if given in headers
  def timezone_name
    if header = request.env["HTTP_TIME_ZONE"] || request.env["HTTP_X_TIME_ZONE"] ||
                request.env["HTTP_TIMEZONE"]
      return nil unless header =~ /[[:print:]]+/
      name = header.split(";").last
    end
    name.to_s
  end

  UTC = ActiveSupport::TimeZone["UTC"]

  # Get the timezone based on current request and user context
  def timezone_for_request(user)
    zone = ActiveSupport::TimeZone[timezone_name]
    zone ||= user.time_zone if user
    zone || UTC
  end

  # Internal: Indicates if the current repo is unavailable due to being
  # disabled, blocked, or broken.
  #
  # Returns a Boolean.
  def current_repo_disabled_or_blocked?
    current_repo.access.disabled? || current_repo.network_broken? || current_repo.disabled?
  end

  def last_write_timestamp_for_current_user
    return unless logged_in?
    last_operations = DatabaseSelector::LastOperations.from_request_creds(
      request_credentials,
    )
    Timestamp.from_time(last_operations.last_write_timestamp)
  end

  def api_response_shape(payload)
    return unless payload

    # When comparing API response shapes, we're only concerned with JSON
    # primitives like `Integer`, `String`, `Array` and `Hash`. By parsing the
    # payload as serialized JSON, it will convert things like
    # `Addressable::URI` to `String`.
    payload = JSON.parse(payload.to_json)

    case payload
    when Array
      payload.map { |item| api_response_shape(item) }
    when Hash
      payload.each_with_object(Hash.new) do |(key, value), hash|
        hash[key] = api_response_shape(value)
      end
    else
      payload.class
    end
  end

  def graphql_mime_body_variables(options)
    variables = {
      includeBody: true,
      includeBodyHTML: false,
      includeBodyText: false,
    }

    if options[:mime_params].include?(:full)
      variables[:includeBodyHTML] = true
      variables[:includeBodyText] = true
    end

    if options[:mime_params].include?(:html)
      variables[:includeBodyHTML] = true
      variables[:includeBody]     = false
    end

    if options[:mime_params].include?(:text)
      variables[:includeBodyText] = true
      variables[:includeBody]     = false
    end

    if options[:mime_params].include?(:raw)
      variables[:includeBody] = true
    end

    variables
  end

  # @return [Organization, User, nil]
  def request_owner
    if defined?(@request_owner)
      @request_owner
    else
      # app/api/applications.rb overrides #find_current_app to return an Integration,
      # handle that here
      if current_app.is_a?(Integration) && current_integration.nil?
        req_integration = current_app
        req_oauth_app = nil
      else
        req_integration = current_integration
        req_oauth_app = current_app
      end

      @request_owner = Api::RequestOwner.call(
        # These inputs might be `nil`, `RequestOwner` will handle it:
        user: current_user,
        installation: current_integration_installation,
        integration: req_integration,
        oauth_application: req_oauth_app,
      )
    end
  end

  def finalize_hydro_context
    if hydro_context[:enabled]
      hydro_context.merge!({
        request_category: request_category,
        controller: self.class.name,
        api_route: route_pattern,
        auth_type: Hydro::EntitySerializer.auth_type(GitHub.context[:auth]) || :AUTH_UNKNOWN,
        current_user: current_user&.login,
        current_user_id: current_user&.id,
        analytics_tracking_id: current_user&.analytics_tracking_id,
        oauth_application_id: GitHub.context[:oauth_application_id],
        oauth_access_id: GitHub.context[:oauth_access_id],
        oauth_scopes: GitHub.context[:oauth_scopes],
        integration_id: GitHub.context[:integration_id],
        integration_installation_id: GitHub.context[:installation_id],
        api_version: medias.to_api_version,
        rate_limit_amount: increment_rate_limit_amount,
        server: Failbot.context&.first&.[]("server"),
        api_request_owner_id: request_owner&.id,
      })

      # This is split out here, as the GraphQL code could have set this already in the context.
      # The processing of rate limits from GraphQL is handled in `app/api/graph_ql.rb` and
      # `rate_limiter.rb` so for those requests, @rate is nil.
      if @rate
        @rate.update_hydro(hydro_context)
      end

      if current_repo_loaded?
        hydro_context.merge!({
          current_repo: current_repo.nwo,
          current_repo_id: current_repo.id,
          current_repo_visibility: (current_repo.private? ? :PRIVATE : :PUBLIC),
        })
      end

      if org = current_resource_owner
        hydro_context.merge!({
          current_org: org.name,
          current_org_id: org.id,
        })
      end
    end
  end

  def disable_hydro_request_logging
    hydro_context[:enabled] = false
  end

  def hydro_context
    request.env[GitHub::HydroMiddleware::CONTEXT] ||= {}
  end

  private def require_preview(name)
    preview = REST::Previews.get(name)
    unless medias.api_version?(preview.media_version)
      deliver_preview_access_error! feature: preview.description
    end
  end

  # Does the API require an allowed IP when IP allow list enforcement is enabled?
  # Returns true by default.
  #
  # You can call bypass_allowed_ip_enforcement in your endpoint definition to
  # specifically bypass IP allow list enforcement for the endpoint.
  #
  # Returns Boolean.
  def require_allowed_ip?
    return @require_allowed_ip if defined?(@require_allowed_ip)

    @require_allowed_ip = true
  end

  # Call this in your endpoint definition to bypass IP allow list enforcement
  # for the endpoint.
  def bypass_allowed_ip_enforcement
    @require_allowed_ip = false
  end
end
