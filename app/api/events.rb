# rubocop:disable Style/FrozenStringLiteralComment

class Api::Events < Api::App
  areas_of_responsibility :news_feeds, :api

  MAX_EVENTS = 300 # 10 pages of 30

  # All public events
  get "/events" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-public-events"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: false
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(:public, current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(:public, events)
      end
    end
  end

  # All Repository events
  get "/repositories/:repository_id/events" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-repository-events"
    control_access :list_events, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: false
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(repo.events_key, current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(repo.events_key, events)
      end
    end
  end

  # All Issue-related events for a Repsitory
  get "/repositories/:repository_id/events/issues" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/issues#list-issue-events-for-a-repository"
    control_access :list_repo_issue_events, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: false
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(repo.events_key(type: :issues), current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(repo.events_key(type: :issues), events)
      end
    end
  end

  # All public events for a repository's network (Repository#network_id)
  get "/networks/:owner/:repo/events" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-public-events-for-a-network-of-repositories"
    control_access :list_network_events, resource: repo = this_repo, allow_integrations: true, allow_user_via_integration: true
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(repo.events_key(type: :network), current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(repo.events_key(type: :network), events)
      end
    end
  end

  # All public Organization events.
  get "/organizations/:organization_id/events" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-public-organization-events"
    org = find_org!
    control_access :public_site_information, resource: Platform::PublicResource.new, enforce_oauth_app_policy: false, allow_integrations: true, allow_user_via_integration: true
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(org.events_key, current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(org.events_key, events)
      end
    end
  end

  # All events that a User received through watching.
  get "/user/:user_id/received_events" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-events-received-by-the-authenticated-user"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: false
    key = if access_allowed?(:list_user_events, resource: user = find_user!, allow_integrations: false, allow_user_via_integration: false)
      :user_received_events
    else
      :user_public
    end
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(user.events_key(type: key), current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(user.events_key(type: key), events)
      end
    end
  end

  # All events performed by a User.
  get "/user/:user_id/events" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-events-for-the-authenticated-user"
    control_access :apps_audited, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: false
    key = if access_allowed?(:list_user_events, resource: user = find_user!, allow_integrations: false, allow_user_via_integration: false)
      :actor
    else
      :actor_public
    end

    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(user.events_key(type: key), current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(user.events_key(type: key), events)
      end
    end
  end

  # All Organization events that a User can see. Visibility is determined by
  # Team access at the time of the Event.
  get "/user/:user_id/events/orgs/:org" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-organization-events-for-the-authenticated-user"
    control_access :list_user_org_events,
                   resource: user = find_user!,
                   enforce_oauth_app_policy: false,
                   organization: find_org!,
                   allow_integrations: false,
                   allow_user_via_integration: false
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(user.events_key(type: :org, param: this_organization), current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(user.events_key(type: :org, param: this_organization), events)
      end
    end
  end

  # Public events that a User received through watching.
  get "/user/:user_id/received_events/public" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-public-events-received-by-a-user"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: false
    user = find_user!
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(user.events_key(type: :user_public), current_user)
      events = timeline.events(**pagination)

      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(user.events_key(type: :user_public), events)
      end
    end
  end

  # Public events performed by a User.
  get "/user/:user_id/events/public" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#list-public-events-for-a-user"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: false
    user = find_user!
    timed_event_delivery do
      deliver_pagination_cap_exceeded! if pagination_limit_exceeded?

      timeline = Stratocaster::Timeline.for(user.events_key(type: :actor_public), current_user)
      events = timeline.events(**pagination)
      if timeline.unavailable?
        deliver_empty status: 503
      else
        deliver :stratocaster_event_hash, events,
          last_modified: calc_last_modified(events),
          etag: timeline.class.fingerprint(user.events_key(type: :actor_public), events)
      end
    end
  end

private

  def timed_event_delivery
    start_time = Time.now
    set_poll_interval_header!
    yield
  ensure
    ms = ((Time.now - start_time) * 1000).round
    GitHub.dogstats.timing("events.deliver.full", ms)
  end

  def poll_interval
    60
  end

  def pagination_limit_exceeded?
    (current_page * per_page) > MAX_EVENTS
  end
end
