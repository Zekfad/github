# frozen_string_literal: true
class Api::EnterpriseUsersScim < Api::App
  include Api::App::SCIMHelpers

  allow_media SCIM::CONTENT_TYPE

  before do
    # Bypass IP allow list enforcement for calls to these endpoints since we do
    # not require an allow list to include the IP addresses of an IdP.
    bypass_allowed_ip_enforcement
  end

  # GET a list of users
  get "/scim/v2/enterprises/:enterprise_id/Users" do
    enterprise = find_enterprise!
    deliver_error!(404) unless enterprise_scim_enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#list-scim-provisioned-identities-for-an-enterprise"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    identities = enterprise.saml_provider.external_identities.not_deleted.
      provisioned_by(:scim).unlinked_or_users

    if params[:filter]
      identities = begin
          identities.scim_filter(params[:filter])
        rescue SCIM::Filter::InvalidFilterError => e
          # https://tools.ietf.org/html/rfc7644#section-3.12
          deliver_scim_error!(400, scim_type: "invalidFilter", detail: e.message)
        end
    end

    results = paginated_results(identities)

    GitHub::PrefillAssociations.for_external_identities(results.resources)
    deliver_scim(:enterprise_scim_identities_hash, results)
  end

  # get details about a specific user
  get "/scim/v2/enterprises/:enterprise_id/Users/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless enterprise_scim_enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#get-scim-provisioning-information-for-an-enterprise-user"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = enterprise.saml_provider.external_identities.not_deleted.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    GitHub::PrefillAssociations.for_external_identities([@external_identity])
    deliver_scim(:enterprise_scim_identity_hash, @external_identity)
  end

  # provision an external identity
  post "/scim/v2/enterprises/:enterprise_id/Users" do
    enterprise = find_enterprise!
    unless enterprise_scim_enabled?(enterprise)
      deliver_error!(404, message: "This Enterprise account does not support membership provisioning.")
    end

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#provision-and-invite-a-scim-enterprise-user"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    json = receive_with_schema("enterprise-scim-user", "provision")

    user_data = Platform::Provisioning::ScimUserData.load(json)
    if user_data.email.nil?
      deliver_scim_error! 400,
        scim_type: "invalidSyntax",
        detail: "\"emails\" wasn't supplied. If no \"emails\" field is supplied, the \"userName\" field must be a valid email."
    end

    options = {
      target: enterprise,
      user_data: user_data,
      mapper: Platform::Provisioning::SCIMMapperWithMigrationSupport,
    }
    result = if operation_can_invite_identity?(action: :post, user_data: user_data)
      apply_identity_invite_operation(options)
    else
      provisioner.provision(**options)
    end

    if result.success?
      GitHub::PrefillAssociations.for_external_identities([result.external_identity])
      deliver_scim(:enterprise_scim_identity_hash, result.external_identity, status: 201)
    elsif result.invalid_identity_error?
      # https://tools.ietf.org/html/rfc7644#section-3.3
      # If the service provider determines that the creation of the requested
      # resource conflicts with existing resources (e.g., a "User" resource
      # with a duplicate "userName"), the service provider MUST return HTTP
      # status code 409 (Conflict) with a "scimType" error code of
      # "uniqueness"
      deliver_scim_error!(409, scim_type: "uniqueness")
    else
      # Something unexpected happened. SCIM doesn't define a general error so
      # we'll just respond with a 500.
      GitHub::Logger.log \
        error: result.errors.first.reason,
        error_messages: result.error_messages,
        enterprise_id: enterprise.id
      deliver_scim_error!(500, detail: result.errors.full_messages.join(" "))
    end
  end

  # replace a user attribute
  put "/scim/v2/enterprises/:enterprise_id/Users/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless enterprise_scim_enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#set-scim-information-for-a-provisioned-enterprise-user"

    control_access :enterprise_scim_writer,
                   resource: enterprise,
                   allow_integrations: true,
                   allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = enterprise.saml_provider.external_identities.
        find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    json = receive_with_schema("enterprise-scim-user", "update")
    user_data = Platform::Provisioning::ScimUserData.load(json)

    result = if @external_identity.scim_user_data.active? && json["active"] == false
      provisioner.deprovision \
        target: enterprise,
        user_data: user_data,
        identity_guid: params[:external_identity_guid],
        actor_id: current_user.id,
        hard_delete: false
    else
      if user_data.email.nil?
        deliver_scim_error! 400,
          scim_type: "invalidSyntax",
          detail: "\"emails\" wasn't supplied. If no \"emails\" field is supplied, the \"userName\" field must be a valid email."
      end

      options = {
        target: enterprise,
        user_data: user_data,
        mapper: Platform::Provisioning::ScimMapper,
      }

      if operation_can_invite_identity?(action: :put, user_data: user_data)
        options[:identity] = @external_identity

        apply_identity_invite_operation(options)
      else
        options[:identity_guid] = params[:external_identity_guid]

        provisioner.provision(**options)
      end
    end

    if result.success?
      GitHub::PrefillAssociations.for_external_identities([result.external_identity])
      deliver_scim(:enterprise_scim_identity_hash, result.external_identity)
    elsif result.forbidden_error?
      deliver_scim_error!(403, detail: result.error_messages.join("\n"))
    else
      # Something went wrong. Scim doesn't specify how to handle general errors
      # so let's just return a generic 500.
      deliver_scim_error!(500)
    end
  end

  # update a user attribute
  patch "/scim/v2/enterprises/:enterprise_id/Users/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless enterprise_scim_enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#update-an-attribute-for-a-scim-enterprise-user"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = enterprise.saml_provider.external_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    patched_user_data = @external_identity.scim_user_data

    json = receive_with_schema("enterprise-scim-user", "patch")

    json["Operations"].each do |op|
      scim_op = op["op"]&.downcase
      if scim_op == "remove" && op["path"].blank?
        deliver_scim_error!(400, scimType: "noTarget")
      else
        patched_user_data = SCIM::Operation.apply(patched_user_data, op)
      end
    end

    options = {
      target: enterprise,
      user_data: patched_user_data,
    }
    result = if operation_deactivates_identity?(@external_identity.scim_user_data, patched_user_data)
      # Deprovisioning request
      options[:identity_guid] = params[:external_identity_guid]
      options[:actor_id] = current_user.id
      options[:hard_delete] = false

      provisioner.deprovision(**options)
    else
      options[:mapper] = Platform::Provisioning::ScimMapper

      if operation_can_invite_identity?(action: :patch,
                                        user_data: @external_identity.scim_user_data,
                                        patched_user_data: patched_user_data)
        options[:identity] = @external_identity

        apply_identity_invite_operation(options)
      else
        options[:identity_guid] = params[:external_identity_guid]
        options[:identity] = @external_identity

        provisioner.provision(**options)
      end
    end

    if result.success?
      GitHub::PrefillAssociations.for_external_identities([result.external_identity])
      deliver_scim(:enterprise_scim_identity_hash, result.external_identity)
    elsif result.forbidden_error?
      deliver_scim_error!(403, detail: result.error_messages.join("\n"))
    else
      # Something went wrong. Scim doesn't specify how to handle general errors
      # so let's just return a generic 500.
      deliver_scim_error!(500)
    end
  end

  # deprovision a user from the org
  delete "/scim/v2/enterprises/:enterprise_id/Users/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless enterprise_scim_enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#delete-a-scim-user-from-an-enterprise"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    # Introducing strict validation of the scim-user.deprovision
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("scim-user", "deprovision", skip_validation: true)

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = enterprise.saml_provider.external_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    result = provisioner.deprovision \
      target: enterprise,
      identity_guid: params[:external_identity_guid],
      actor_id: current_user.id

    if result.success?
      deliver_empty status: 204
    elsif result.forbidden_error?
      deliver_scim_error!(403, detail: result.error_messages.join("\n"))
    else
      deliver_scim_error!(500, detail: result.error_messages.join("\n"))
    end
  end

  private

  def provisioner
    if GitHub.private_instance_user_provisioning?
      Platform::Provisioning::EnterpriseServerIdentityProvisioner
    else
      Platform::Provisioning::IdentityProvisioner
    end
  end

  def enterprise_scim_enabled?(enterprise)
    return true if GitHub.private_instance_user_provisioning? && enterprise.saml_sso_enabled?
    return true if GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)
    false
  end

  # Private: Determine whether or not the current operation can invite the identity to the
  # Enterprise. Call #provision_and_invite if invites are possible, and #provision otherwise.
  #
  # For  GHPI, the behavior is set: a POST creates and invites the identity to GHPI. PUT and PATCH
  # only update an identity, but do not invite.
  #
  # For GHEC, behavior is dependent on whether the [optional] `groups` attribute is present. If it
  # is, call #provision_and_invite to sync up the user's org/group memberships. If it isn't, call
  # #provision and don't touch org/group memberships.
  #
  # action              -   HTTP action we're responding to (:post, :put, or :patch)
  # user_data           -   user_data for this user
  # patched_user_data   -   user_data with the PATCH changes applied (only used for :patch case)
  #
  # Returns: Boolean
  def operation_can_invite_identity?(action:, user_data:, patched_user_data: nil)
    case action
    when :post
      GitHub.private_instance_user_provisioning? || user_data.fetch("groups").present?
    when :put
      return false if GitHub.private_instance_user_provisioning?
      user_data.fetch("groups").present?
    when :patch
      return false if GitHub.private_instance_user_provisioning?
      (user_data.fetch("groups") || patched_user_data.fetch("groups")).present?
    else
      false
    end
  end

  def operation_deactivates_identity?(original_scim_data, patched_scim_data)
    original_scim_data.active? && !patched_scim_data.active?
  end

  def apply_identity_invite_operation(options)
    # If the request is being performed by an app
    # we need to pass along the `IntegrationInstallation`
    # record as the inviter instead of the `Bot`.
    if current_user.bot?
      options[:inviter_id]   = current_user.installation.id
      options[:inviter_type] = IntegrationInstallation
    else
      options[:inviter_id] = current_user.id
    end

    provisioner.provision_and_invite(**options)
  end

end
