# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationOutsideCollaborators < Api::App
  areas_of_responsibility :orgs, :api

  # list outside collaborators in org
  get "/organizations/:organization_id/outside_collaborators" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-outside-collaborators-for-an-organization"

    org = find_org!

    set_forbidden_message "You must be an owner of this organization to list outside collaborators."
    control_access :list_outside_collaborators,
      resource: org,
      user: current_user,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    users = org.outside_collaborators
    users = users.two_factor_disabled if wants_two_factor_audit?

    deliver :user_hash, paginate_rel(users.order(:login))
  end

  put "/organizations/:organization_id/outside_collaborators/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#convert-an-organization-member-to-outside-collaborator"

    org  = find_org!
    user = record_or_404(this_user)
    data = receive(Hash, required: false) || {}
    async = data["async"] == "true"

    control_access :manage_org_users,
      resource: org,
      user: current_user,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    if org.member?(user)
      begin
        org.prevent_removal_of_last_admin!(user, "You can't demote the last admin")
      rescue Organization::NoAdminsError
        deliver_error! 403, message: "Cannot convert the last owner to an outside collaborator"
      else
        # Introducing strict validation of the organization-membership.convert-to-outside-collaborator
        # JSON schema would cause breaking changes for integrators
        # skip_validation until a rollout strategy can be determined
        # TODO: replace `receive` with `receive_with_schema`
        # see: https://github.com/github/ecosystem-api/issues/1555
        _ = receive_with_schema("organization-membership", "convert-to-outside-collaborator", skip_validation: true)

        if async
          # this runs a background job to convert the user to an outside collaborator
          org.convert_to_outside_collaborator(user)
          deliver_empty(status: 202)
        else
          org.convert_to_outside_collaborator!(user)
          deliver_empty(status: 204)
        end
      end
    else
      deliver_error! 404, message: "#{user} is not a member of the #{org} organization."
    end
  end

  # remove an outside collaborator
  delete "/organizations/:organization_id/outside_collaborators/:username" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the organization-collaborator.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("organization-collaborator", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/orgs#remove-outside-collaborator-from-an-organization"

    org = find_org!
    outside_collaborator = this_user

    set_forbidden_message "You must be an owner of this organization to remove outside collaborators."
    control_access :remove_outside_collaborator,
      resource: org,
      user: current_user,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    if org.member?(outside_collaborator)
      # The model method will protect against this case, but we explicitly check
      # here so we can deliver a better error message.
      deliver_error! 422, message: "You cannot specify an organization member to remove as an outside collaborator."
    else
      if org.user_is_outside_collaborator?(outside_collaborator.id)
        org.remove_outside_collaborator(outside_collaborator)
      end
      deliver_empty(status: 204)
    end
  end

  private

  def filters
    params[:filter].try(:split, ",") || []
  end

  def wants_two_factor_audit?
    filters.include? "2fa_disabled"
  end

end
