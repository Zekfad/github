# frozen_string_literal: true

class Api::RepositoryContentReferences < Api::App
  # Get a content reference
  get "/repositories/:repository_id/content_references/:content_reference_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:content_references)

    repo = find_repo!
    content_reference = ContentReference.by_repo_and_id(repo, int_id_param!(key: :content_reference_id))
    record_or_404(content_reference)

    control_access :read_content_reference,
      resource: content_reference,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver :content_reference_hash, content_reference
  end

  # Create an attachment on a content reference
  post "/repositories/:repository_id/content_references/:content_reference_id/attachments" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/apps#create-a-content-attachment"
    require_preview(:content_references)

    repo = find_repo!
    content_reference = ContentReference.by_repo_and_id(repo, int_id_param!(key: :content_reference_id))
    record_or_404(content_reference)

    control_access :write_content_reference,
      resource: content_reference,
      allow_integrations: true,
      allow_user_via_integration: false

    data = receive_with_schema("content-reference-attachment", "create")
    attachment = ContentReferenceAttachment.new(
      content_reference: content_reference,
      integration: current_integration,
      title: data["title"],
      body: data["body"],
      state: :processed,
    )

    if attachment.save
      deliver :content_reference_attachment_hash, attachment
    else
      deliver_error 422, errors: attachment.errors, documentation_url: @documentation_url
    end
  end

  # DEPRECATED: Get a content reference
  get "/content_references/:content_reference_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_preview(:content_references)

    content_reference = ContentReference.find_by(id: int_id_param!(key: :content_reference_id))
    record_or_404(content_reference)
    if content_reference.repository.nil?
      deliver_error!(410, message: "The context that this content reference was attached to has been deleted, so you won't be able to create a content attachment.")
    end

    control_access :read_content_reference,
      resource: content_reference,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver :content_reference_hash, content_reference
  end

  # DEPRECATED: Create an attachment on a content reference
  post "/content_references/:content_reference_id/attachments" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/apps#create-a-content-attachment"
    require_preview(:content_references)

    content_reference = ContentReference.find_by(id: int_id_param!(key: :content_reference_id))
    record_or_404(content_reference)
    if content_reference.repository.nil?
      deliver_error!(410, message: "The context that this content reference was attached to has been deleted, so you won't be able to create a content attachment.")
    end

    control_access :write_content_reference,
      resource: content_reference,
      allow_integrations: true,
      allow_user_via_integration: false

    data = receive_with_schema("content-reference-attachment", "create-legacy")
    attachment = ContentReferenceAttachment.new(
      content_reference: content_reference,
      integration: current_integration,
      title: data["title"],
      body: data["body"],
      state: :processed,
    )

    if attachment.save
      deliver :content_reference_attachment_hash, attachment
    else
      deliver_error 422, errors: attachment.errors, documentation_url: @documentation_url
    end
  end
end
