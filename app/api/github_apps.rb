# frozen_string_literal: true

class Api::GitHubApps < Api::App
  areas_of_responsibility :api, :ecosystem_apps

  IntegrationQuery = PlatformClient.parse <<-'GRAPHQL'
    query($id: ID!){
      node(id: $id){
        ... on App{
          ...Api::Serializer::IntegrationsDependency::IntegrationFragment
        }
      }
    }
  GRAPHQL

  get "/app/:app_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::WE_FORGOT

    control_access :github_app_viewer,
      resource: app = find_app!,
      allow_integrations: true,
      allow_user_via_integration: true,
      installation_required: false

    GitHub::PrefillAssociations.for_integrations([app])
    deliver :integration_hash, app,
      compare_payload_to: :app_candidate
  end

  def app_candidate(app, options)
    variables = { id: app.global_relay_id }

    results = platform_execute(IntegrationQuery, variables: variables)
    graphql_app = results.data.node

    Api::Serializer.serialize(:graphql_integration_hash, graphql_app)
  end

  private

  def find_app!
    if (app = env[GitHub::Routers::Api::ThisAppKey])
      app
    else
      app = Integration.find_by_id(params[:app_id])
      record_or_404(app)
    end
  end
end
