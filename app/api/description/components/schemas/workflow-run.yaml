---
title: Workflow Run
description: An invocation of a workflow
type: object
properties:
  id:
    type: integer
    description: The ID of the workflow run.
    example: 5
  node_id:
    type: string
    example: MDEwOkNoZWNrU3VpdGU1
  head_branch:
    type: string
    nullable: true
    example: master
  head_sha:
    description: The SHA of the head commit that points to the version of the worflow
      being run.
    example: '009b8a3a9ccbb128af87f9b1c0f4c62e8a304f6d'
    type: string
  run_number:
    type: integer
    description: The auto incrementing run number for the workflow run.
    example: 106
  event:
    type: string
    example: push
  status:
    type: string
    nullable: true
    example: completed
  conclusion:
    type: string
    nullable: true
    example: neutral
  workflow_id:
    type: integer
    description: The ID of the parent workflow.
    example: 5
  url:
    type: string
    description: The URL to the workflow run.
    example: https://api.github.com/repos/github/hello-world/actions/runs/5
  html_url:
    type: string
    example: https://github.com/github/hello-world/suites/4
  pull_requests:
    type: array
    nullable: true
    items:
      "$ref": "./pull-request-minimal.yaml"
  created_at:
    type: string
    nullable: true
    format: date-time
  updated_at:
    type: string
    nullable: true
    format: date-time
  jobs_url:
    description: The URL to the jobs for the workflow run.
    type: string
    example: https://api.github.com/repos/github/hello-world/actions/runs/5/jobs
  logs_url:
    description: The URL to download the logs for the workflow run.
    type: string
    example: https://api.github.com/repos/github/hello-world/actions/runs/5/logs
  check_suite_url:
    description: The URL to the associated check suite.
    type: string
    example: https://api.github.com/repos/github/hello-world/check-suites/12
  artifacts_url:
    description: The URL to the artifacts for the workflow run.
    type: string
    example: https://api.github.com/repos/github/hello-world/actions/runs/5/rerun/artifacts
  cancel_url:
    description: The URL to cancel the workflow run.
    type: string
    example: https://api.github.com/repos/github/hello-world/actions/runs/5/cancel
  rerun_url:
    description: The URL to rerun the workflow run.
    type: string
    example: https://api.github.com/repos/github/hello-world/actions/runs/5/rerun
  workflow_url:
    description: The URL to the workflow.
    type: string
    example: https://api.github.com/repos/github/hello-world/actions/workflows/main.yml
  head_commit:
    "$ref": "./simple-commit.yaml"
  repository:
    "$ref": "./minimal-repository.yaml"
  head_repository:
    "$ref": "./minimal-repository.yaml"
  head_repository_id:
    type: integer
    example: 5
required:
- id
- node_id
- head_branch
- run_number
- event
- status
- conclusion
- head_sha
- workflow_id
- url
- html_url
- created_at
- updated_at
- head_commit
- head_repository
- repository
- jobs_url
- logs_url
- check_suite_url
- cancel_url
- rerun_url
- artifacts_url
- workflow_url
- pull_requests
