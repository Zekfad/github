---
title: Team Discussion Comment
description: A reply to a discussion within a team.
type: object
properties:
  author:
    nullable: true
    allOf:
    - "$ref": "./simple-user.yaml"
  body:
    description: The main text of the comment.
    example: I agree with this suggestion.
    type: string
  body_html:
    type: string
    example: "<p>Do you like apples?</p>"
  body_version:
    description: The current version of the body content. If provided, this update
      operation will be rejected if the given version does not match the latest version
      on the server.
    example: 0307116bbf7ced493b8d8a346c650b71
    type: string
  created_at:
    type: string
    format: date-time
    example: '2018-01-15T23:53:58Z'
  last_edited_at:
    type: string
    format: date-time
    nullable: true
  discussion_url:
    type: string
    format: uri
    example: https://api.github.com/organizations/1/team/2403582/discussions/1
  html_url:
    type: string
    format: uri
    example: https://github.com/orgs/github/teams/justice-league/discussions/1/comments/1
  node_id:
    type: string
    example: MDIxOlRlYW1EaXNjdXNzaW9uQ29tbWVudDE=
  number:
    description: The unique sequence number of a team discussion comment.
    example: 42
    type: integer
  updated_at:
    type: string
    format: date-time
    example: '2018-01-15T23:53:58Z'
  url:
    type: string
    format: uri
    example: https://api.github.com/organizations/1/team/2403582/discussions/1/comments/1
  reactions:
    "$ref": "./reaction-rollup.yaml"
required:
- author
- body
- body_html
- body_version
- created_at
- last_edited_at
- discussion_url
- html_url
- node_id
- number
- updated_at
- url
