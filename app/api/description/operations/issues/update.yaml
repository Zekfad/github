---
summary: Update an issue
description: Issue owners and users with push access can edit an issue.
tags:
- issues
operationId: issues/update
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/issues/#update-an-issue"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: issues/update-by-id
x-github-backfill:
  id: patch:repos:%:%:issues:%
  path: "/repos/{owner}/{repo}/issues/{issue_number}"
  http-method: patch
  openapi-operation-file: app/api/description/backfill/operations/issues/update.yaml
  errors: []
  openapi-docs-file: definitions/operations/issues/update.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/issue_number.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          title:
            type: string
            description: The title of the issue.
          body:
            type: string
            description: The contents of the issue.
          assignee:
            type: string
            description: Login for the user that this issue should be assigned to.
              **This field is deprecated.**
          state:
            type: string
            description: State of the issue. Either `open` or `closed`.
            enum:
            - open
            - closed
          milestone:
            type: integer
            description: 'The `number` of the milestone to associate this issue with
              or `null` to remove current. _NOTE: Only users with push access can
              set the milestone for issues. The milestone is silently dropped otherwise._'
            nullable: true
          labels:
            type: array
            description: 'Labels to associate with this issue. Pass one or more Labels
              to _replace_ the set of Labels on this Issue. Send an empty array (`[]`)
              to clear all Labels from the Issue. _NOTE: Only users with push access
              can set labels for issues. Labels are silently dropped otherwise._'
            items:
              oneOf:
              - type: string
              - type: object
                properties:
                  id:
                    type: integer
                  name:
                    type: string
                  description:
                    type: string
                  color:
                    type: string
          assignees:
            type: array
            description: 'Logins for Users to assign to this issue. Pass one or more
              user logins to _replace_ the set of assignees on this Issue. Send an
              empty array (`[]`) to clear all assignees from the Issue. _NOTE: Only
              users with push access can set assignees for new issues. Assignees are
              silently dropped otherwise._'
            items:
              type: string
      example:
        title: Found a bug
        body: I'm having a problem with this.
        assignees:
        - octocat
        milestone: 1
        state: open
        labels:
        - bug
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/issue.yaml"
        examples:
          default:
            "$ref": "../../components/examples/issue.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
  '503':
    "$ref": "../../components/responses/service_unavailable.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '301':
    "$ref": "../../components/responses/moved_permanently.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
  '410':
    "$ref": "../../components/responses/gone.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
