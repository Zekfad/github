---
summary: Check team permissions for a project
description: |-
  Checks whether a team has `read`, `write`, or `admin` permissions for an organization project. The response includes projects inherited from a parent team.

  **Note:** You can also specify a team by `org_id` and `team_id` using the route `GET /organizations/{org_id}/team/{team_id}/projects/{project_id}`.
tags:
- teams
operationId: teams/check-permissions-for-project-in-org
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/teams/#check-team-permissions-for-a-project"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: teams/check-permissions-for-project-in-org-by-id
x-github-backfill:
  id: get:orgs:%:teams:%:projects:%
  path: "/orgs/{org}/teams/{team_slug}/projects/{project_id}"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/teams/check-permissions-for-project-in-org.yaml
  errors: []
  openapi-docs-file: definitions/operations/teams/check-permissions-for-project-in-org.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
- "$ref": "../../components/parameters/team_slug.yaml"
- "$ref": "../../components/parameters/project-id.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/team-project.yaml"
        examples:
          default:
            "$ref": "../../components/examples/team-project.yaml"
  '404':
    description: Response if project is not managed by this team
x-github-releases:
- ghes:
  - ">= 2.21"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/inertia.yaml"
    required: true
