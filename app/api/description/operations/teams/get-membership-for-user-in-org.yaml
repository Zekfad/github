---
summary: Get team membership for a user
description: |-
  Team members will include the members of child teams.

  To get a user's membership with a team, the team must be visible to the authenticated user.

  **Note:** You can also specify a team by `org_id` and `team_id` using the route `GET /organizations/{org_id}/team/{team_id}/memberships/{username}`.

  **Note:** The `role` for organization owners returns as `maintainer`. For more information about `maintainer` roles, see [Create a team](${externalDocsUrl}/v3/teams/#create-a-team).
tags:
- teams
operationId: teams/get-membership-for-user-in-org
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/teams/members/#get-team-membership-for-a-user"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: teams/get-membership-for-user-in-org-by-id
x-github-backfill:
  id: get:orgs:%:teams:%:memberships:%
  path: "/orgs/{org}/teams/{team_slug}/memberships/{username}"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/teams/get-membership-for-user-in-org.yaml
  errors: []
  openapi-docs-file: definitions/operations/teams/get-membership-for-user-in-org.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
- "$ref": "../../components/parameters/team_slug.yaml"
- "$ref": "../../components/parameters/username.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/team-membership.yaml"
        examples:
          response-if-user-has-an-active-membership-with-team:
            "$ref": "../../components/examples/team-membership-response-if-user-has-an-active-membership-with-team.yaml"
          response-if-user-is-a-team-maintainer:
            "$ref": "../../components/examples/team-membership-response-if-user-is-a-team-maintainer.yaml"
          response-if-user-has-a-pending-membership-with-team:
            "$ref": "../../components/examples/team-membership-response-if-user-has-a-pending-membership-with-team.yaml"
  '404':
    description: Response if user has no team membership
x-github-releases:
- ghes:
  - ">= 2.21"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
