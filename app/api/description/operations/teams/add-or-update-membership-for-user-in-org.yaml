---
summary: Add or update team membership for a user
description: |-
  Team synchronization is available for organizations using GitHub Enterprise Cloud. For more information, see [GitHub's products](https://help.github.com/github/getting-started-with-github/githubs-products) in the GitHub Help documentation.

  Adds an organization member to a team. An authenticated organization owner or team maintainer can add organization members to a team.

  **Note:** When you have team synchronization set up for a team with your organization's identity provider (IdP), you will see an error if you attempt to use the API for making changes to the team's membership. If you have access to manage group membership in your IdP, you can manage ${apiName} team membership through your identity provider, which automatically adds and removes team members in an organization. For more information, see "[Synchronizing teams between your identity provider and ${apiName}](https://help.github.com/articles/synchronizing-teams-between-your-identity-provider-and-github/)."

  An organization owner can add someone who is not part of the team's organization to a team. When an organization owner adds someone to a team who is not an organization member, this endpoint will send an invitation to the person via email. This newly-created membership will be in the "pending" state until the person accepts the invitation, at which point the membership will transition to the "active" state and the user will be added as a member of the team.

  If the user is already a member of the team, this endpoint will update the role of the team member's role. To update the membership of a team member, the authenticated user must be an organization owner or a team maintainer.

  **Note:** You can also specify a team by `org_id` and `team_id` using the route `PUT /organizations/{org_id}/team/{team_id}/memberships/{username}`.
tags:
- teams
operationId: teams/add-or-update-membership-for-user-in-org
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/teams/members/#add-or-update-team-membership-for-a-user"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: teams/add-or-update-membership-for-user-in-org-by-id
x-github-backfill:
  id: put:orgs:%:teams:%:memberships:%
  path: "/orgs/{org}/teams/{team_slug}/memberships/{username}"
  http-method: put
  openapi-operation-file: app/api/description/backfill/operations/teams/add-or-update-membership-for-user-in-org.yaml
  errors: []
  openapi-docs-file: definitions/operations/teams/add-or-update-membership-for-user-in-org.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
- "$ref": "../../components/parameters/team_slug.yaml"
- "$ref": "../../components/parameters/username.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          role:
            type: string
            description: "The role that this user should have in the team. Can be
              one of:  \n\\* `member` - a normal member of the team.  \n\\* `maintainer`
              - a team maintainer. Able to add/remove other team members, promote
              other team members to team maintainer, and edit the team's name and
              description."
            enum:
            - member
            - maintainer
            default: member
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/team-membership.yaml"
        examples:
          response-if-users-membership-with-team-is-now-active:
            "$ref": "../../components/examples/team-membership-response-if-users-membership-with-team-is-now-active.yaml"
          response-if-users-membership-with-team-is-now-pending:
            "$ref": "../../components/examples/team-membership-response-if-users-membership-with-team-is-now-pending.yaml"
  '403':
    description: Response if team synchronization is set up
  '422':
    description: Response if you attempt to add an organization to a team
    content:
      application/json:
        schema:
          type: object
          properties:
            message:
              type: string
            errors:
              type: array
              items:
                type: object
                properties:
                  code:
                    type: string
                  field:
                    type: string
                  resource:
                    type: string
        examples:
          response-if-you-attempt-to-add-an-organization-to-a-team:
            value:
              message: Cannot add an organization as a member.
              errors:
              - code: org
                field: user
                resource: TeamMember
x-github-releases:
- ghes:
  - ">= 2.21"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
