---
summary: List IdP groups for an organization
description: |-
  Team synchronization is available for organizations using GitHub Enterprise Cloud. For more information, see [GitHub's products](https://help.github.com/github/getting-started-with-github/githubs-products) in the GitHub Help documentation.

  List IdP groups available in an organization. You can limit your page results using the `per_page` parameter. ${apiName} generates a url-encoded `page` token using a cursor value for where the next page begins. For more information on cursor pagination, see "[Offset and Cursor Pagination explained](https://dev.to/jackmarchant/offset-and-cursor-pagination-explained-b89)."

  The `per_page` parameter provides pagination for a list of IdP groups the authenticated user can access in an organization. For example, if the user `octocat` wants to see two groups per page in `octo-org` via cURL, it would look like this:
tags:
- teams
operationId: teams/list-idp-groups-for-org
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/teams/team_sync/#list-idp-groups-for-an-organization"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: teams/list-idp-groups-for-org-by-id
x-github-backfill:
  id: get:orgs:%:team-sync:groups
  path: "/orgs/{org}/team-sync/groups"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/teams/list-id-p-groups-for-org.yaml
  errors: []
  openapi-docs-file: definitions/operations/teams/list-id-p-groups-for-org.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/group-mapping.yaml"
        examples:
          default:
            "$ref": "../../components/examples/group-mapping-3.yaml"
    headers:
      Link:
        example: <https://api.github.com/resource?per_page=2&page=url-encoded-next-page-token>;
          rel="next"
        schema:
          type: string
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: true
  enabledForGitHubApps: true
  previews: []
