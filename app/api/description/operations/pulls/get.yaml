---
summary: Get a pull request
description: |-
  Draft pull requests are available in public repositories with GitHub Free and GitHub Free for organizations, GitHub Pro, and legacy per-repository billing plans, and in public and private repositories with GitHub Team and GitHub Enterprise Cloud. For more information, see [GitHub's products](https://help.github.com/github/getting-started-with-github/githubs-products) in the GitHub Help documentation.

  Lists details of a pull request by providing its number.

  When you get, [create](${externalDocsUrl}/v3/pulls/#create-a-pull-request), or [edit](${externalDocsUrl}/v3/pulls/#update-a-pull-request) a pull request, ${apiName} creates a merge commit to test whether the pull request can be automatically merged into the base branch. This test commit is not added to the base branch or the head branch. You can review the status of the test commit using the `mergeable` key. For more information, see "[Checking mergeability of pull requests](${externalDocsUrl}/v3/git/#checking-mergeability-of-pull-requests)".

  The value of the `mergeable` attribute can be `true`, `false`, or `null`. If the value is `null`, then ${apiName} has started a background job to compute the mergeability. After giving the job time to complete, resubmit the request. When the job finishes, you will see a non-`null` value for the `mergeable` attribute in the response. If `mergeable` is `true`, then `merge_commit_sha` will be the SHA of the _test_ merge commit.

  The value of the `merge_commit_sha` attribute changes depending on the state of the pull request. Before merging a pull request, the `merge_commit_sha` attribute holds the SHA of the _test_ merge commit. After merging a pull request, the `merge_commit_sha` attribute changes depending on how you merged the pull request:

  *   If merged as a [merge commit](https://help.github.com/articles/about-merge-methods-on-github/), `merge_commit_sha` represents the SHA of the merge commit.
  *   If merged via a [squash](https://help.github.com/articles/about-merge-methods-on-github/#squashing-your-merge-commits), `merge_commit_sha` represents the SHA of the squashed commit on the base branch.
  *   If [rebased](https://help.github.com/articles/about-merge-methods-on-github/#rebasing-and-merging-your-commits), `merge_commit_sha` represents the commit that the base branch was updated to.

  Pass the appropriate [media type](${externalDocsUrl}/v3/media/#commits-commit-comparison-and-pull-requests) to fetch diff and patch formats.
tags:
- pulls
operationId: pulls/get
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/pulls/#get-a-pull-request"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: pulls/get-by-id
x-github-backfill:
  id: get:repos:%:%:pulls:%
  path: "/repos/{owner}/{repo}/pulls/{pull_number}"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/pulls/get.yaml
  errors: []
  openapi-docs-file: definitions/operations/pulls/get.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/pull-number.yaml"
responses:
  '200':
    description: Pass the appropriate [media type](${externalDocsUrl}/v3/media/#commits-commit-comparison-and-pull-requests)
      to fetch diff and patch formats.
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/pull-request.yaml"
        examples:
          default:
            "$ref": "../../components/examples/pull-request.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '500':
    "$ref": "../../components/responses/internal_error.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/sailor-v.yaml"
    required: false
