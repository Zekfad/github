---
summary: Mark notifications as read
description: Marks all notifications as "read" removes it from the [default view on
  ${apiName}](https://github.com/notifications). If the number of notifications is
  too large to complete in one request, you will receive a `202 Accepted` status and
  ${apiName} will run an asynchronous process to mark notifications as "read." To
  check whether any "unread" notifications remain, you can use the [List notifications
  for the authenticated user](${externalDocsUrl}/v3/activity/notifications/#list-notifications-for-the-authenticated-user)
  endpoint and pass the query parameter `all=false`.
tags:
- activity
operationId: activity/mark-notifications-as-read
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/activity/notifications/#mark-notifications-as-read"
x-github-resource-owner: "@github/notifications"
x-github-backfill:
  id: put:notifications
  path: "/notifications"
  http-method: put
  openapi-operation-file: app/api/description/backfill/operations/activity/mark-notifications-as-read.yaml
  impl-file: app/api/notifications.rb
  errors: []
  openapi-docs-file: definitions/operations/activity/mark-notifications-as-read.yml
  json-schema-file: app/api/schemas/v3/schemas/notification-status.json
parameters: []
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          last_read_at:
            description: Describes the last point that notifications were checked.
            type: string
            format: date-time
          read:
            description: Whether the notification has been read.
            type: boolean
responses:
  '202':
    description: response
    content:
      application/json:
        schema:
          type: object
          properties:
            message:
              type: string
  '205':
    description: response
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '401':
    "$ref": "../../components/responses/requires_authentication.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
