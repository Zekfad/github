---
summary: Create an organization invitation
description: |-
  Invite people to an organization by using their GitHub user ID or their email address. In order to create invitations in an organization, the authenticated user must be an organization owner.

  This endpoint triggers [notifications](https://help.github.com/articles/about-notifications/). Creating content too quickly using this endpoint may result in abuse rate limiting. See "[Abuse rate limits](${externalDocsUrl}/v3/#abuse-rate-limits)" and "[Dealing with abuse rate limits](${externalDocsUrl}/v3/guides/best-practices-for-integrators/#dealing-with-abuse-rate-limits)" for details.
tags:
- orgs
operationId: orgs/create-invitation
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/orgs/members/#create-an-organization-invitation"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: orgs/create-invitation-by-id
x-github-backfill:
  id: post:orgs:%:invitations
  path: "/orgs/{org}/invitations"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/orgs/create-invitation.yaml
  errors: []
  openapi-docs-file: definitions/operations/orgs/create-invitation.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          invitee_id:
            type: integer
            description: "**Required unless you provide `email`**. GitHub user ID
              for the person you are inviting."
          email:
            type: string
            description: "**Required unless you provide `invitee_id`**. Email address
              of the person you are inviting, which can be an existing GitHub user."
          role:
            type: string
            description: "Specify role for new member. Can be one of:  \n\\* `admin`
              - Organization owners with full administrative rights to the organization
              and complete access to all repositories and teams.  \n\\* `direct_member`
              - Non-owner organization members with ability to see other members and
              join teams by invitation.  \n\\* `billing_manager` - Non-owner organization
              members with ability to manage the billing settings of your organization."
            enum:
            - admin
            - direct_member
            - billing_manager
            default: direct_member
          team_ids:
            type: array
            description: Specify IDs for the teams you want to invite new members
              to.
            items:
              type: integer
      example:
        email: octocat@github.com
        role: direct_member
        team_ids:
        - 12
        - 26
responses:
  '201':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/organization-invitation.yaml"
        examples:
          default:
            "$ref": "../../components/examples/organization-invitation.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- api.github.com
x-github:
  triggersNotification: true
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
