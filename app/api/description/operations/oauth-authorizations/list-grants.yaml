---
summary: List your grants
description: |-
  **Deprecation Notice:** ${apiName} will discontinue the [OAuth Authorizations API](${externalDocsUrl}/v3/oauth_authorizations/), which is used by integrations to create personal access tokens and OAuth tokens, and you must now create these tokens using our [web application flow](${externalDocsUrl}/apps/building-oauth-apps/authorizing-oauth-apps/#web-application-flow). The [OAuth Authorizations API](${externalDocsUrl}/v3/oauth_authorizations/) will be removed on November, 13, 2020. For more information, including scheduled brownouts, see the [blog post](${externalDocsUrl}/changes/2020-02-14-deprecating-oauth-auth-endpoint/).

  You can use this API to list the set of OAuth applications that have been granted access to your account. Unlike the [list your authorizations](${externalDocsUrl}/v3/oauth_authorizations/#list-your-authorizations) API, this API does not manage individual tokens. This API will return one entry for each OAuth application that has been granted access to your account, regardless of the number of tokens an application has generated for your user. The list of OAuth applications returned matches what is shown on [the application authorizations settings screen within GitHub](https://github.com/settings/applications#authorized). The `scopes` returned are the union of scopes authorized for the application. For example, if an application has one token with `repo` scope and another token with `user` scope, the grant will return `["repo", "user"]`.
tags:
- oauth-authorizations
operationId: oauth-authorizations/list-grants
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/oauth_authorizations/#list-your-grants"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: get:applications:grants
  path: "/applications/grants"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/apps/list-oauth-grants.yaml
  impl-file: app/api/grants.rb
  errors: []
  openapi-docs-file: definitions/operations/oauth-authorizations/list-grants.yml
parameters:
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/application-grant.yaml"
        examples:
          default:
            "$ref": "../../components/examples/application-grant-items.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '401':
    "$ref": "../../components/responses/requires_authentication.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
  removalDate: '2020-11-13'
  deprecationDate: '2020-02-14'
deprecated: true
x-deprecation-details:
  date: '2020-02-14'
  sunsetDate: '2020-11-13'
