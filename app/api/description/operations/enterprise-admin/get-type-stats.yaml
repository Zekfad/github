---
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: 
x-github-backfill:
  path: "/enterprise/stats/{type}"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/enterprise-admin/get-type-stats.yaml
  errors: []
  openapi-docs-file: definitions/operations/enterprise-admin/get-type-stats.yml
summary: Get statistics
description: |-
  There are a variety of types to choose from:

  | Type         | Description                                                                                         |
  | ------------ | --------------------------------------------------------------------------------------------------- |
  | `issues`     | The number of open and closed issues.                                                               |
  | `hooks`      | The number of active and inactive hooks.                                                            |
  | `milestones` | The number of open and closed milestones.                                                           |
  | `orgs`       | The number of organizations, teams, team members, and disabled organizations.                       |
  | `comments`   | The number of comments on issues, pull requests, commits, and gists.                                |
  | `pages`      | The number of GitHub Pages sites.                                                                   |
  | `users`      | The number of suspended and admin users.                                                            |
  | `gists`      | The number of private and public gists.                                                             |
  | `pulls`      | The number of merged, mergeable, and unmergeable pull requests.                                     |
  | `repos`      | The number of organization-owned repositories, root repositories, forks, pushed commits, and wikis. |
  | `all`        | All of the statistics listed above.                                                                 |

  These statistics are cached and will be updated approximately every 10 minutes.
operationId: enterprise-admin/get-type-stats
tags:
- enterprise-admin
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/enterprise-admin/admin_stats/#get-statistics"
parameters:
- name: type
  in: path
  required: true
  schema:
    type: string
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/enterprise-overview.yaml"
        examples:
          default:
            "$ref": "../../components/examples/enterprise-overview.yaml"
x-github-releases:
- ghes: ">= 2.18"
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
