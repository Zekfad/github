---
summary: Search topics
description: |-
  Find topics via various criteria. Results are sorted by best match. This method returns up to 100 results [per page](${externalDocsUrl}/v3/#pagination). See "[Searching topics](https://help.github.com/articles/searching-topics/)" for a detailed list of qualifiers.

  When searching for topics, you can get text match metadata for the topic's **short\_description**, **description**, **name**, or **display\_name** field when you pass the `text-match` media type. For more details about how to receive highlighted search results, see [Text match metadata](${externalDocsUrl}/v3/search/#text-match-metadata).

  For example, if you want to search for topics related to Ruby that are featured on https://github.com/topics. Your query might look like this:

  `q=ruby+is:featured`

  This query searches for topics with the keyword `ruby` and limits the results to find only topics that are featured. The topics that are the best match for the query appear first in the search results.
tags:
- search
operationId: search/topics
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/search/#search-topics"
x-github-resource-owner: "@github/ee-search"
x-github-backfill:
  id: get:search:topics
  path: "/search/topics"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/search/topics.yaml
  impl-file: app/api/search.rb
  errors: []
  openapi-docs-file: definitions/operations/search/topics.yml
parameters:
- name: q
  description: The query contains one or more search keywords and qualifiers. Qualifiers
    allow you to limit your search to specific areas of GitHub. The REST API supports
    the same qualifiers as GitHub.com. To learn more about the format of the query,
    see [Constructing a search query](${externalDocsUrl}/v3/search/#constructing-a-search-query).
  in: query
  required: true
  schema:
    type: string
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: object
          properties:
            total_count:
              type: integer
            incomplete_results:
              type: boolean
            items:
              type: array
              items:
                "$ref": "../../components/schemas/topic-search-result-item.yaml"
        examples:
          default:
            "$ref": "../../components/examples/topic-search-result-item-paginated.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '415':
    "$ref": "../../components/responses/preview_header_missing.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/mercy.yaml"
    required: true
