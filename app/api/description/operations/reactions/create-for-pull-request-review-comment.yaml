---
summary: Create reaction for a pull request review comment
description: 'Create a reaction to a [pull request review comment](${externalDocsUrl}/v3/pulls/comments/).
  A response with a `Status: 200 OK` means that you already added the reaction type
  to this pull request review comment.'
tags:
- reactions
operationId: reactions/create-for-pull-request-review-comment
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/reactions/#create-reaction-for-a-pull-request-review-comment"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: reactions/create-for-pull-request-review-comment-by-id
x-github-backfill:
  id: post:repos:%:%:pulls:comments:%:reactions
  path: "/repos/{owner}/{repo}/pulls/comments/{comment_id}/reactions"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/reactions/create-for-pull-request-review-comment.yaml
  errors: []
  openapi-docs-file: definitions/operations/reactions/create-for-pull-request-review-comment.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/comment_id.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          content:
            type: string
            description: The [reaction type](${externalDocsUrl}/v3/reactions/#reaction-types)
              to add to the pull request review comment.
            enum:
            - "+1"
            - "-1"
            - laugh
            - confused
            - heart
            - hooray
            - rocket
            - eyes
        required:
        - content
      example:
        content: heart
responses:
  '200':
    description: Reaction exists
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/reaction.yaml"
        examples:
          default:
            "$ref": "../../components/examples/reaction.yaml"
  '201':
    description: Reaction created
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/reaction.yaml"
        examples:
          default:
            "$ref": "../../components/examples/reaction.yaml"
  '415':
    "$ref": "../../components/responses/preview_header_missing.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/squirrel-girl.yaml"
    required: true
