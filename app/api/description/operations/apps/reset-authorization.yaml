---
summary: Reset an authorization
description: |-
  **Deprecation Notice:** ${apiName} will replace and discontinue OAuth endpoints containing `access_token` in the path parameter. We are introducing new endpoints that allow you to securely manage tokens for OAuth Apps by using `access_token` as an input parameter. The OAuth Application API will be removed on May 5, 2021. For more information, including scheduled brownouts, see the [blog post](${externalDocsUrl}/changes/2020-02-14-deprecating-oauth-app-endpoint/).

  OAuth applications can use this API method to reset a valid OAuth token without end-user involvement. Applications must save the "token" property in the response because changes take effect immediately. You must use [Basic Authentication](${externalDocsUrl}/v3/auth#basic-authentication) when accessing this endpoint, using the OAuth application's `client_id` and `client_secret` as the username and password. Invalid tokens will return `404 NOT FOUND`.
tags:
- apps
operationId: apps/reset-authorization
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/oauth_applications/#reset-an-authorization"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: post:applications:%:tokens:%
  path: "/applications/{client_id}/tokens/{access_token}"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/apps/reset-authorization.yaml
  impl-file: app/api/applications.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/reset-authorization.yml
  json-schema-file: app/api/schemas/v3/schemas/authorization.json
parameters:
- "$ref": "../../components/parameters/client-id.yaml"
- "$ref": "../../components/parameters/access-token.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/authorization.yaml"
        examples:
          default:
            "$ref": "../../components/examples/authorization-with-user.yaml"
x-github-releases:
- ghes:
  - ">= 2.20"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
  removalDate: '2021-05-05'
  deprecationDate: '2020-02-14'
deprecated: true
x-deprecation-details:
  date: '2020-02-14'
  sunsetDate: '2021-05-05'
