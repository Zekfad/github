---
summary: List installations for the authenticated app
description: |-
  You must use a [JWT](${externalDocsUrl}/apps/building-github-apps/authenticating-with-github-apps/#authenticating-as-a-github-app) to access this endpoint.

  The permissions the installation has are included under the `permissions` key.
tags:
- apps
operationId: apps/list-installations
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/#list-installations-for-the-authenticated-app"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: get:app:installations
  path: "/app/installations"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/apps/list-installations.yaml
  impl-file: app/api/integrations.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/list-installations.yml
parameters:
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
- "$ref": "../../components/parameters/since.yaml"
- name: outdated
  in: query
  required: false
  schema:
    type: string
responses:
  '200':
    description: The permissions the installation has are included under the `permissions`
      key.
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/installation.yaml"
        examples:
          default:
            "$ref": "../../components/examples/base-installation-items.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/machine-man.yaml"
    required: true
