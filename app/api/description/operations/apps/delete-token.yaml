---
summary: Delete an app token
description: OAuth application owners can revoke a single token for an OAuth application.
  You must use [Basic Authentication](${externalDocsUrl}/v3/auth#basic-authentication)
  when accessing this endpoint, using the OAuth application's `client_id` and `client_secret`
  as the username and password.
tags:
- apps
operationId: apps/delete-token
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/oauth_applications/#delete-an-app-token"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: delete:applications:%:token
  path: "/applications/{client_id}/token"
  http-method: delete
  openapi-operation-file: app/api/description/backfill/operations/apps/delete-token.yaml
  impl-file: app/api/applications.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/delete-token.yml
  json-schema-file: app/api/schemas/v3/schemas/authorization.json
parameters:
- "$ref": "../../components/parameters/client-id.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          access_token:
            type: string
            description: The OAuth access token used to authenticate to the GitHub
              API.
      example:
        access_token: e72e16c7e42f292c6912e7710c838347ae178b4a
responses:
  '204':
    description: Empty response
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
x-github-releases:
- ghes:
  - ">= 2.20"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
