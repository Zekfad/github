---
summary: Create an installation access token for an app
description: |-
  Creates an installation access token that enables a GitHub App to make authenticated API requests for the app's installation on an organization or individual account. Installation tokens expire one hour from the time you create them. Using an expired token produces a status code of `401 - Unauthorized`, and requires creating a new installation token. By default the installation token has access to all repositories that the installation can access. To restrict the access to specific repositories, you can provide the `repository_ids` when creating the token. When you omit `repository_ids`, the response does not contain the `repositories` key.

  You must use a [JWT](${externalDocsUrl}/apps/building-github-apps/authenticating-with-github-apps/#authenticating-as-a-github-app) to access this endpoint.
tags:
- apps
operationId: apps/create-installation-access-token
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/#create-an-installation-access-token-for-an-app"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: post:app:installations:%:access_tokens
  path: "/app/installations/{installation_id}/access_tokens"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/apps/create-installation-token.yaml
  impl-file: app/api/integrations.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/create-installation-access-token.yml
  json-schema-file: app/api/schemas/v3/schemas/installation-token.json
parameters:
- "$ref": "../../components/parameters/installation_id.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          repositories:
            description: List of repository names that the token should have access
              to
            type: array
            items:
              type: string
              example: rails
          repository_ids:
            description: List of repository IDs that the token should have access
              to
            example:
            - 1
            type: array
            items:
              type: integer
          permissions:
            type: object
            properties:
              contents:
                type: string
              issues:
                type: string
              deployments:
                type: string
              single_file:
                type: string
              def_not_a_repo:
                type: string
                example: '"read"'
            example:
              contents: read
              issues: read
              deployments: write
              single_file: read
responses:
  '201':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/installation-token.yaml"
        examples:
          default:
            "$ref": "../../components/examples/installation-token.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '415':
    "$ref": "../../components/responses/preview_header_missing.yaml"
  '401':
    "$ref": "../../components/responses/requires_authentication.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/machine-man.yaml"
    required: true
