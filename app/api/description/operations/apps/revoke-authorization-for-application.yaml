---
summary: Revoke an authorization for an application
description: |-
  **Deprecation Notice:** ${apiName} will replace and discontinue OAuth endpoints containing `access_token` in the path parameter. We are introducing new endpoints that allow you to securely manage tokens for OAuth Apps by using `access_token` as an input parameter. The OAuth Application API will be removed on May 5, 2021. For more information, including scheduled brownouts, see the [blog post](${externalDocsUrl}/changes/2020-02-14-deprecating-oauth-app-endpoint/).

  OAuth application owners can revoke a single token for an OAuth application. You must use [Basic Authentication](${externalDocsUrl}/v3/auth#basic-authentication) when accessing this endpoint, using the OAuth application's `client_id` and `client_secret` as the username and password.
tags:
- apps
operationId: apps/revoke-authorization-for-application
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/oauth_applications/#revoke-an-authorization-for-an-application"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: delete:applications:%:tokens:%
  path: "/applications/{client_id}/tokens/{access_token}"
  http-method: delete
  openapi-operation-file: app/api/description/backfill/operations/apps/revoke-authorization-for-application.yaml
  impl-file: app/api/applications.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/revoke-authorization-for-application.yml
  json-schema-file: app/api/schemas/v3/schemas/authorization.json
parameters:
- "$ref": "../../components/parameters/client-id.yaml"
- "$ref": "../../components/parameters/access-token.yaml"
responses:
  '204':
    description: Empty response
x-github-releases:
- ghes:
  - ">= 2.20"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
  removalDate: '2021-05-05'
  deprecationDate: '2020-02-14'
deprecated: true
x-deprecation-details:
  date: '2020-02-14'
  sunsetDate: '2021-05-05'
