---
summary: List workflow runs
description: |-
  List all workflow runs for a workflow. You can also replace `:workflow_id` with `:workflow_file_name`. For example, you could use `main.yml`. You can use parameters to narrow the list of results. For more information about using parameters, see [Parameters](${externalDocsUrl}/v3/#parameters).

  Anyone with read access to the repository can use this endpoint. If the repository is private you must use an access token with the `repo` scope.
tags:
- actions
operationId: actions/list-workflow-runs
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/actions/workflow-runs/#list-workflow-runs"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: actions/list-workflow-runs-by-id
x-github-backfill:
  id: get:repos:%:%:actions:workflows:%:runs
  path: "/repos/{owner}/{repo}/actions/workflows/{workflow_id}/runs"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/actions/list-workflow-runs.yaml
  errors: []
  openapi-docs-file: definitions/operations/actions/list-workflow-runs.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/workflow-id.yaml"
- "$ref": "../../components/parameters/actor.yaml"
- "$ref": "../../components/parameters/workflow-run-branch.yaml"
- "$ref": "../../components/parameters/event.yaml"
- "$ref": "../../components/parameters/workflow-run-status.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: object
          properties:
            total_count:
              type: integer
            workflow_runs:
              type: array
              items:
                "$ref": "../../components/schemas/workflow-run.yaml"
        examples:
          default:
            "$ref": "../../components/examples/workflow-run-paginated.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
