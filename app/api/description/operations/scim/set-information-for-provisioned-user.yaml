---
summary: Update a provisioned organization membership
description: |-
  Replaces an existing provisioned user's information. You must provide all the information required for the user as if you were provisioning them for the first time. Any existing user information that you don't provide will be removed. If you want to only update a specific attribute, use the [Update an attribute for a SCIM user](${externalDocsUrl}/v3/scim/#update-an-attribute-for-a-scim-user) endpoint instead.

  You must at least provide the required values for the user: `userName`, `name`, and `emails`.

  **Warning:** Setting `active: false` removes the user from the organization, deletes the external identity, and deletes the associated `{scim_user_id}`.
tags:
- scim
operationId: scim/set-information-for-provisioned-user
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/scim/#set-scim-information-for-a-provisioned-user"
x-github-resource-owner: "@github/iam"
x-github-alternate-operation-id: scim/update-org-membership-by-id
x-github-backfill:
  id: put:scim:v2:organizations:%:users:%
  path: "/scim/v2/organizations/{org}/Users/{scim_user_id}"
  http-method: put
  openapi-operation-file: app/api/description/backfill/operations/scim/update-org-membership.yaml
  impl-file: app/api/organizations_scim.rb
  errors: []
  openapi-docs-file: definitions/operations/scim/set-information-for-provisioned-user.yml
  json-schema-file: app/api/schemas/v3/schemas/scim-user.json
parameters:
- "$ref": "../../components/parameters/org.yaml"
- "$ref": "../../components/parameters/scim_user_id.yaml"
responses:
  '200':
    description: response
    content:
      application/scim+json:
        schema:
          "$ref": "../../components/schemas/scim-user.yaml"
        examples:
          default:
            "$ref": "../../components/examples/scim-user.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '404':
    "$ref": "../../components/responses/scim_not_found.yaml"
  '403':
    "$ref": "../../components/responses/scim_forbidden.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          schemas:
            type: array
            items:
              type: string
          displayName:
            type: string
          externalId:
            type: string
          groups:
            type: array
            items:
              type: string
          active:
            type: boolean
          userName:
            description: Configured by the admin. Could be an email, login, or username
            example: someone@example.com
            type: string
          displayName:
            description: The name of the user, suitable for display to end-users
            example: Jon Doe
            type: string
          name:
            type: object
            properties:
              givenName:
                type: string
              familyName:
                type: string
              formatted:
                type: string
            required:
            - givenName
            - familyName
            example: Jane User
          emails:
            description: user emails
            example:
            - someone@example.com
            - another@example.com
            type: array
            minItems: 1
            items:
              type: object
              properties:
                type:
                  type: string
                value:
                  type: string
                primary:
                  type: boolean
              required:
              - value
        required:
        - userName
        - name
        - emails
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: true
  enabledForGitHubApps: true
  previews: []
