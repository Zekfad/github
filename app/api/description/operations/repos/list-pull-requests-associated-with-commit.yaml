---
summary: List pull requests associated with a commit
description: Lists all pull requests containing the provided commit SHA, which can
  be from any point in the commit history. The results will include open and closed
  pull requests. Additional preview headers may be required to see certain details
  for associated pull requests, such as whether a pull request is in a draft state.
  For more information about previews that might affect this endpoint, see the [List
  pull requests](${externalDocsUrl}/v3/pulls/#list-pull-requests) endpoint.
tags:
- repos
operationId: repos/list-pull-requests-associated-with-commit
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/commits/#list-pull-requests-associated-with-a-commit"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/list-pull-requests-associated-with-commit-by-id
x-github-backfill:
  id: get:repos:%:%:commits:%:pulls
  path: "/repos/{owner}/{repo}/commits/{commit_sha}/pulls"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/repos/list-pull-requests-associated-with-commit.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/list-pull-requests-associated-with-commit.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/commit_sha.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/pull-request-simple.yaml"
        examples:
          default:
            "$ref": "../../components/examples/pull-request-simple-items.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '415':
    "$ref": "../../components/responses/preview_header_missing.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/groot.yaml"
    required: true
