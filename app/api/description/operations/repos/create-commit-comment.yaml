---
summary: Create a commit comment
description: |-
  Create a comment for a commit using its `:commit_sha`.

  This endpoint triggers [notifications](https://help.github.com/articles/about-notifications/). Creating content too quickly using this endpoint may result in abuse rate limiting. See "[Abuse rate limits](${externalDocsUrl}/v3/#abuse-rate-limits)" and "[Dealing with abuse rate limits](${externalDocsUrl}/v3/guides/best-practices-for-integrators/#dealing-with-abuse-rate-limits)" for details.
tags:
- repos
operationId: repos/create-commit-comment
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/comments/#create-a-commit-comment"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/create-commit-comment-by-id
x-github-backfill:
  id: post:repos:%:%:commits:%:comments
  path: "/repos/{owner}/{repo}/commits/{commit_sha}/comments"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/repos/create-commit-comment.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/create-commit-comment.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/commit_sha.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          body:
            type: string
            description: The contents of the comment.
          path:
            type: string
            description: Relative path of the file to comment on.
          position:
            type: integer
            description: Line index in the diff to comment on.
          line:
            type: integer
            description: "**Deprecated**. Use **position** parameter instead. Line
              number in the file to comment on."
        required:
        - body
      example:
        body: Great stuff
        path: file1.txt
        position: 4
        line: 1
responses:
  '201':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/commit-comment.yaml"
        examples:
          default:
            "$ref": "../../components/examples/commit-comment.yaml"
    headers:
      Location:
        example: https://api.github.com/repos/octocat/Hello-World/comments/1
        schema:
          type: string
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  triggersNotification: true
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
