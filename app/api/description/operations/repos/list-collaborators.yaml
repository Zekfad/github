---
summary: List repository collaborators
description: |-
  For organization-owned repositories, the list of collaborators includes outside collaborators, organization members that are direct collaborators, organization members with access through team memberships, organization members with access through default organization permissions, and organization owners.

  Team members will include the members of child teams.
tags:
- repos
operationId: repos/list-collaborators
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/collaborators/#list-repository-collaborators"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/list-collaborators-by-id
x-github-backfill:
  id: get:repos:%:%:collaborators
  path: "/repos/{owner}/{repo}/collaborators"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/repos/list-collaborators.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/list-collaborators.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- name: affiliation
  description: "Filter collaborators returned by their affiliation. Can be one of:
    \ \n\\* `outside`: All outside collaborators of an organization-owned repository.
    \ \n\\* `direct`: All collaborators with permissions to an organization-owned
    repository, regardless of organization membership status.  \n\\* `all`: All collaborators
    the authenticated user can see."
  in: query
  required: false
  schema:
    type: string
    enum:
    - outside
    - direct
    - all
    default: all
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/collaborator.yaml"
        examples:
          default:
            "$ref": "../../components/examples/collaborator-items.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
