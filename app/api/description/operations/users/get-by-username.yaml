---
summary: Get a user
description: |-
  Provides publicly available information about someone with a GitHub account.

  GitHub Apps with the `Plan` user permission can use this endpoint to retrieve information about a user's ${apiName} plan. The GitHub App must be authenticated as a user. See "[Identifying and authorizing users for GitHub Apps](${externalDocsUrl}/apps/building-github-apps/identifying-and-authorizing-users-for-github-apps/)" for details about authentication. For an example response, see "[Response with ${apiName} plan information](${externalDocsUrl}/v3/users/#response-with-github-plan-information)."

  The `email` key in the following response is the publicly visible email address from your ${apiName} [profile page](https://github.com/settings/profile). When setting up your profile, you can select a primary email address to be “public” which provides an email entry for this endpoint. If you do not set a public email address for `email`, then it will have a value of `null`. You only see publicly visible email addresses when authenticated with ${apiName}. For more information, see [Authentication](${externalDocsUrl}/v3/#authentication).

  The Emails API enables you to list all of your email addresses, and toggle a primary email to be visible publicly. For more information, see "[Emails API](${externalDocsUrl}/v3/users/emails/)".
tags:
- users
operationId: users/get-by-username
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/users/#get-a-user"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: users/get-by-username-by-id
x-github-backfill:
  id: get:users:%
  path: "/users/{username}"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/users/get-by-username.yaml
  errors: []
  openapi-docs-file: definitions/operations/users/get-by-username.yml
parameters:
- "$ref": "../../components/parameters/username.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          oneOf:
          - "$ref": "../../components/schemas/private-user.yaml"
          - "$ref": "../../components/schemas/public-user.yaml"
        examples:
          default-response:
            "$ref": "../../components/examples/public-user-default-response.yaml"
          response-with-git-hub-plan-information:
            "$ref": "../../components/examples/public-user-response-with-git-hub-plan-information.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
