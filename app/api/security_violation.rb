# frozen_string_literal: true

class Api::SecurityViolation < Api::JavaScriptError; end
