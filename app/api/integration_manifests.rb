# rubocop:disable Style/FrozenStringLiteralComment

class Api::IntegrationManifests < Api::App
  areas_of_responsibility :api, :ecosystem_integrations

  # This endpoint is unauthenticated by design, so has a higher unauthenticated
  # rate limit than other endpoints to enable valid, non-abusive usage on platforms
  # like Glitch that would otherwise be prohibited by the default rate limiting rules
  rate_limit_as Api::RateLimitConfiguration::INTEGRATION_MANIFEST_FAMILY

  post "/app-manifests/:code/conversions" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/apps#create-a-github-app-from-a-manifest"

    control_access :write_integration_manifest_conversion,
      allow_integrations: false,
      allow_user_via_integration: false

    receive_with_schema("app-manifest", "convert")

    manifest = IntegrationManifest.find_by(code: params["code"])
    record_or_404(manifest)

    unless manifest.valid?
      deliver_error! 422, errors: manifest.errors.full_messages
    end

    integration, pem = manifest.create_integration!

    GlobalInstrumenter.instrument "integration.create", {
      integration: integration,
      actor: manifest.creator,
      owner: manifest.owner,
      from_manifest: true,
      manifest: manifest.data,
    }

    deliver :integration_hash, integration, pem: pem, status: 201
  end
end
