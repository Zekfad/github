# rubocop:disable Style/FrozenStringLiteralComment

class Api::Issues < Api::App
  module EnsureIssuesEnabled

    # Checks repository after access control to ensure issues
    # are enabled for this repository.
    def ensure_issues_enabled!(repo)
      unless repo.has_issues?
        deliver_error!(410,
          message: "Issues are disabled for this repo",
          documentation_url: "/v3/issues/")
      end
    end

    # Halts with a 410 if the issue is not a Pull and the repo
    # has disabled issues
    def ensure_issues_enabled_or_pr!(repo, issue)
      return if !issue || repo.has_issues? || issue.pull_request_id
      ensure_issues_enabled!(repo)
    end
  end

  include Api::Labels::Helpers, Api::Issues::EnsureIssuesEnabled, Scientist

  STATE     = "state".freeze
  ASSIGNEE  = "assignee".freeze
  ASSIGNEES = "assignees".freeze
  MILESTONE = "milestone".freeze
  CREATOR   = "creator".freeze
  MENTIONED = "mentioned".freeze
  LABELS    = "labels".freeze
  COLLAB_ONLY_ATTRIBUTES = [ASSIGNEE, ASSIGNEES, LABELS, MILESTONE]

  # Internal: filter issues given a list of repositories to restrict by.
  #
  # repo_ids_or_scope - either a Repository scope, or an array of ids.
  #
  # Returns a scope to filter Issues based on request parameters and the
  # list or scope of repositories.
  #
  # NOTE: the ids-or-scope argument is ambiguous to allow for handling either
  # lists of ids or scopes, depending on how each performs:
  #
  # * An array of ids uses Issue.for_repository_ids (orginal behavior, but now
  #   only used for org repositories)
  # * A Repository scope uses Issue.for_repositories (this version performed
  #   consistently faster for everything but org.org_repositories scopes)
  #
  def filter_issues(repo_ids_or_scope)
    scope = if current_user.using_auth_via_integration?
      filter_issues_for_repos_without_installation(repo_ids_or_scope)
    elsif repo_ids_or_scope.is_a? ActiveRecord::Relation
      Issue.for_repositories(repo_ids_or_scope)
    else
      Issue.for_repository_ids(repo_ids_or_scope)
    end

    scope = scope.filter_spam_for(current_user).excluding_issues_in_repos_with_issues_disabled

    if protected_organization_ids.any?
      set_sso_partial_results_header if protected_sso_organization_ids.any?
      scope = scope.excluding_organization_ids(protected_organization_ids)
    end

    scope =
      case params[:filter]
      when "created"
        scope.created_by(current_user.login)
      when "subscribed"
        if repo_ids_or_scope.is_a? ActiveRecord::Relation
          repo_ids = repo_ids_or_scope.pluck(:id)
        else
          repo_ids = repo_ids_or_scope
        end
        lists = repo_ids.map { |id| Newsies::List.new(Repository.name, id) }
        threads_response = science("issues_rest_subscribed_force_user_id_index") do |e|
          e.use { GitHub.newsies.subscribed_threads(current_user, lists, "Issue") }
          e.try { GitHub.newsies.subscribed_threads(current_user, lists, "Issue", true) }
          e.compare { |control, candidate| control.map(&:thread_id).sort == candidate.map(&:thread_id).sort }
        end

        if threads_response.failed?
          deliver_notifications_unavailable!
        end
        scope.from_ids(threads_response.map(&:thread_id))
      when "mentioned"
        scope.mentioning(current_user)
      when "repos", "all"
        # we're already filtered by repos
        scope
      when "assigned", nil
        scope.assigned_to(current_user.login)
      else
        deliver_error! 422,
          errors: [api_error(:Issue, :filter, :invalid, value: params[:filter])],
          documentation_url: "/v3/issues/"
      end

    # filter by state
    scope = case params[:state]
      when /close/ then scope.closed_issues
      when /all/   then scope
      else         scope.open_issues
    end

    # filter by labels
    if (labels = params[:labels]).present?
      scope = scope.labeled(labels.split(","))
    end

    # filter by updated_at
    if (since = time_param!(:since)).present?
      scope = scope.since(since.getlocal)
    end

    scope = filter_dash_scope(scope)

    # specify sort
    scope = scope.sorted_by(params[:sort], params[:direction])

    if scope.is_a?(ActiveRecord::NullRelation)
      paginate_rel(scope)
    else
      pagination_scope = scope.select(:id).paginate(page: pagination[:page], per_page: pagination[:per_page])

      scope = Issue.where("`issues`.`id` IN (SELECT * FROM (?) subquery_for_limit)", pagination_scope).sorted_by(params[:sort], params[:direction])

      # Copy over pagination attributes
      scope = scope.extending(WillPaginate::ActiveRecord::RelationMethods)
      scope = scope.per_page(pagination_scope.per_page)
      scope.current_page = pagination_scope.current_page
      scope.total_entries = pagination_scope.total_entries

      scope
    end
  end

  # List issues for the current user across all organization, owned, and member repositories
  get "/issues" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-issues-assigned-to-the-authenticated-user"
    resource = Platform::DashboardResource.new(current_user)
    control_access :dashboard, resource: resource, allow_integrations: false, allow_user_via_integration: true, installation_required: false
    cap_paginated_entries!

    private_access_allowed = access_allowed?(:private_dashboard, resource: resource, allow_integrations: false, allow_user_via_integration: true, installation_required: false)

    repo_ids = current_user.associated_repository_ids
    repo_ids = Repository.where(id: repo_ids).public_scope.pluck(:id) unless private_access_allowed

    # ensure we filter by repos this user owns or is a collab on
    issues = filter_issues repo_ids

    GitHub::PrefillAssociations.for_issues(issues)
    Reaction::Summary.prefill(issues)
    deliver :issue_hash, issues, repositories: true
  end

  # List issues for repositories under a specific organization
  get "/organizations/:organization_id/issues" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-organization-issues-assigned-to-the-authenticated-user"
    org = find_org!
    resource = Platform::DashboardResource.new(org)
    control_access :org_issue_dashboard, resource: resource, allow_integrations: false, allow_user_via_integration: true

    repos = org.visible_repositories_for(current_user)

    repos = if current_user.using_auth_via_integration?
      installation = current_integration.installations.with_target(org).first

      if installation.present? && Ability.can_at_least?(:read, installation.permissions["issues"])
        repos.where("repositories.public = ? OR repositories.id IN (?)", true, installation.repository_ids)
      else
        repos.public_scope
      end
    else
      unless access_allowed?(:private_dashboard, resource: resource, allow_integrations: false, allow_user_via_integration: false)
        repos.public_scope
      else
        repos
      end
    end

    # For more information, see performance graphs on #38575.
    # The org-admin case was the only version of this that performed
    # consistently better with repository ids rather than a repository scope
    # converted into a join/subselect. If this endpoint ever gets slow, that's
    # the first place to look.
    if org.adminable_by? current_user
      issues = filter_issues repos.pluck(:id)
    else
      issues = filter_issues repos
    end

    GitHub::PrefillAssociations.for_issues(issues)
    Reaction::Summary.prefill(issues)

    deliver :issue_hash, issues, repositories: true
  end

  # List issues for the current user across all owned and member repositories
  get "/user/issues" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-user-account-issues-assigned-to-the-authenticated-user"
    resource = Platform::DashboardResource.new(current_user)
    control_access :dashboard, resource: resource, allow_integrations: false, allow_user_via_integration: true, installation_required: false

    owned_repos = current_user.repositories
    member_repos = current_user.member_repositories

    unless access_allowed?(:private_dashboard, resource: resource, allow_integrations: false, allow_user_via_integration: true, installation_required: false)
      owned_repos = owned_repos.public_scope
      member_repos = member_repos.public_scope
    end

    owned_repo_ids = owned_repos.pluck(:id)
    member_repo_ids = member_repos.pluck(:id)

    repo_ids = owned_repo_ids | member_repo_ids

    scope = filter_issues repo_ids
    GitHub.dogstats.time "prefill", tags: ["via:api", "action:user_issues_list"] do
      GitHub::PrefillAssociations.for_issues(scope)
      Reaction::Summary.prefill(scope)
      deliver :issue_hash, scope, repositories: true
    end
  end

  # List issues for this Repository
  get "/repositories/:repository_id/issues" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-repository-issues"
    control_access :list_issues,
      repo: repo = current_repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    query = search_param_mapping(params)

    # Bring in labels and split them if it's a comma,delimited,string
    if (labels = params[:labels]).present? && labels.respond_to?(:split)
      query << [:label, labels.split(",")]
    end

    # Scope to a particular sort
    if params[:sort] || params[:direction]
      sort = params[:sort] || "created"
      direction = params[:direction] || "desc"
      query << [:sort, "#{sort}-#{direction}"]
    end

    # Default sort is "open"
    if !params[:state]
      query << [:state, "open"]
    end

    # Scope to a milestone
    if params[MILESTONE]
      query << milestone_filter(repo, query, params[MILESTONE])
    end

    # Scope to an assignee
    if params[ASSIGNEE]
      query << assignee_filter(query, params[ASSIGNEE])
    end

    # Scope mentions
    if params[MENTIONED]
      query << mention_filter(query, params[MENTIONED])
    end

    # Scope type
    #
    # | I | Issues   | pr :read | issues :read | Force type | Comment                    |
    # |---|----------|----------|--------------|------------|----------------------------|
    # | 1 | Enabled  | Yes      | Yes          | N/A        | No filtering needed        |
    # | 2 | Enabled  | Yes      | No           | pr         |                            |
    # | 3 | Enabled  | No       | Yes          | issues     |                            |
    # | 4 | Enabled  | No       | No           | N/A        | No access to this endpoint |
    # | 5 | Disabled | No       | No           | N/A        | No access to this endpoint |
    # | 6 | Disabled | No       | Yes          | pr         |                            |
    # | 7 | Disabled | Yes      | No           | pr         |                            |
    # | 8 | Disabled | Yes      | Yes          | pr         |                            |
    #

    # 5-8 Filter to PRs only if issues are disabled for the repo
    force_type = if !repo.has_issues?
      :pull_requests
    else
      pr_read = repo.resources.pull_requests.readable_by?(current_user)
      issue_read = repo.resources.issues.readable_by?(current_user)

      case
      # 2
      when pr_read && !issue_read
        :pull_requests
      # 3
      when !pr_read && issue_read
        :issues
      # 1,4,5
      else
        nil
      end
    end

    # Check if state is invalid
    if params[:state] && !%w(open closed all).include?(params[:state])
      deliver_error! 422,
        errors: [api_error(:Issue, :state, :invalid, value: params[:state])],
        documentation_url: "/v3/issues/#list-issues"
    end

    # Check if labels are invalid
    if params[:labels].present? && !params[:labels].is_a?(String)
      deliver_error! 422,
        errors: [api_error(:Issue, :labels, :invalid, value: params[:labels])],
        documentation_url: "/v3/issues/#list-issues"
    end

    # Build a custom initial scope if there's a since filter
    # This is deliberately not adding an initial scope if it's
    # not needed to ensure the query hints in MysqlSearch work.
    if params[:since]
      scope = since_filter(repo.issues)
    end

    scope = Issue::MysqlSearch.search \
               query: query,
               repo: repo,
               current_user: current_user,
               page: (pagination[:page] || 1).to_i,
               per_page: (pagination[:per_page] || 30).to_i,
               initial_scope: scope,
               force_pulls: !repo.has_issues?, # Filter to PRs only if issues are disabled for the repo
               force_type: force_type,
               show_spam_to_staff: true        # Staff should see the spam issues

    issues = scope[:issues]
    GitHub.dogstats.time "prefill", tags: ["via:api", "action:repository_issues_list"] do
      GitHub::PrefillAssociations.for_issues(issues, repository: repo)
      Reaction::Summary.prefill(issues)

      deliver :issue_hash, issues
    end
  end

  # Create an Issue
  # TODO: refactor so all data massaging code is done first
  post "/repositories/:repository_id/issues" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#create-an-issue"

    control_access :open_issue,
      repo: repo = current_repo,
      enforce_oauth_app_policy: current_repo.private?,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_issues_enabled!(repo)
    ensure_not_blocked! current_user, repo.owner_id
    authorize_content(repo, :create)

    data = receive_with_schema("issue", "create-legacy")

    # Don't let non collabs set these
    unless repo.resources.issues.writable_by?(current_user)
      data.delete_if { |key, value| COLLAB_ONLY_ATTRIBUTES.include?(key) }
    end

    if data["labels"]
      labels = labels_for(repo, Array(data["labels"]))
      data["labels"] = labels
      data["label_ids"] = labels&.map(&:id)
    end

    issue_builder = Issue::Builder.new(current_user, repo)
    issue = issue_builder.build_from_api(data)

    if id = data[MILESTONE]
      milestone = repo.milestones.find_by_number(id.to_i) if valid_milestone_id?(id)
      if milestone
        issue.milestone = milestone
      else
        deliver_error! 422,
          errors: [api_error(:Issue, :milestone, :invalid, value: id)],
          documentation_url: @documentation_url
      end
    end

    assignee_keys = [ASSIGNEE, ASSIGNEES].select { |key| data.key?(key) }

    if assignee_keys.length == 2
      deliver_error! 422, message: "You cannot pass both `assignee` and `assignees`. Only one may be provided."
    end

    if assignee_keys.present?
      assignees = prepare_assignees(repo: repo, assignee_keys: assignee_keys, data: data, documentation_url: @documentation_url, issue: issue)
      issue.assignees = assignees
      data["user_assignee_ids"] = assignees.map(&:id)
    end

    if integration_user_request?
      issue.performed_via_integration = current_integration
      issue.modifying_integration = current_integration
      data["integration_id"] = current_integration.id
    end

    begin
      saved = GitHub::SchemaDomain.allowing_cross_domain_transactions { issue.save }
    rescue GitHub::Prioritizable::Context::LockedForRebalance
      deliver_error! 503,
        message: "This issue's milestone is temporarily locked for maintenance. Please try again.",
        documentation_url: @documentation_url
    end

    if saved
      issue.replace_labels(labels) if labels.present?
      GitHub.dogstats.increment("issue", tags: ["via:api", "action:create", "valid:true"])

      GitHub::PrefillAssociations.for_issues([issue],
        repository: repo,
        only_prefill: [:assignees])
      deliver :issue_hash, issue, status: 201, repo: repo, full: true
    else
      GitHub.dogstats.increment("issue", tags: ["via:api", "action:create", "valid:false"])

      deliver_error 422,
        errors: issue.errors,
        documentation_url: @documentation_url
    end
  end

  IssueQuery = Api::App::PlatformClient.parse <<-'GRAPHQL'
    query($id: ID!, $includeBody: Boolean!, $includeBodyHTML: Boolean!, $includeBodyText: Boolean!){
      node(id: $id){
        ... on Issue {
          ...Api::Serializer::IssuesDependency::IssueFragment
        }
        ... on PullRequest {
          ...Api::Serializer::IssuesDependency::PullRequestIssueFragment
        }
      }
    }
  GRAPHQL

  def issue_candidate(issue, options)
    variables = { id: issue.global_relay_id }
    variables.update(graphql_mime_body_variables(options))

    results = platform_execute(IssueQuery, variables: variables)
    issue = results.data.node

    Api::Serializer.serialize(:graphql_issue_hash, issue, options)
  end

  # Get a single Issue
  get "/repositories/:repository_id/issues/:issue_number" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#get-an-issue"
    repo  = current_repo
    issue = repo && repo.issues.find_by_number(int_id_param!(key: :issue_number))

    if issue
      control_access :show_issue,
        resource: issue,
        repo: repo,
        allow_integrations: true,
        allow_user_via_integration: true,
        approved_integration_required: false

      ensure_issues_enabled_or_pr!(repo, issue)

      deliver_error!(404) unless issue_visible_to_user?(issue)

      GitHub::PrefillAssociations.for_issues([issue], repository: repo)
      Reaction::Summary.prefill([issue])
      deliver :issue_hash, issue,
        repo: repo,
        full: true,
        last_modified: calc_last_modified(issue),
        compare_payload_to: :issue_candidate
    else
      handle_issue_not_found
    end
  end

  # Edit an Issue
  verbs :patch, :post, "/repositories/:repository_id/issues/:issue_number" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#update-an-issue"

    repo = current_repo
    unless issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))
      return handle_issue_not_found
    end

    control_access :edit_issue,
      resource: issue,
      repo: repo,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    ensure_not_blocked! current_user, repo.owner_id
    ensure_issue_visible!(repo, issue)
    authorize_content(repo, :update)

    data = receive_with_schema("issue", "update-legacy")

    # Don't let non collabs set these
    unless repo.pushable_by?(current_user) || repo.resources.issues.writable_by?(current_user)
      data.delete_if { |key, value| COLLAB_ONLY_ATTRIBUTES.include?(key) }
    end

    assignee_keys = [ASSIGNEE, ASSIGNEES].select { |key| data.key?(key) }
    if assignee_keys.length == 2
      deliver_error! 422, message: "You cannot pass both `assignee` and `assignees`. Only one may be provided."
    end

    if assignee_keys.present?
      assignees = prepare_assignees(repo: repo, assignee_keys: assignee_keys,
        data: data, documentation_url: @documentation_url, issue: issue)
      issue.assignees = assignees
      data["user_assignee_ids"] = assignees.map(&:id)
    end

    if (id = data[MILESTONE]).present?
      milestone = repo.milestones.find_by_number(id.to_i) if valid_milestone_id?(id)
      if milestone
        issue.milestone = milestone
        data["milestone_id"] = milestone.id
      else
        deliver_error! 422,
          errors: [api_error(:Issue, :milestone, :invalid, value: id)],
          documentation_url: @documentation_url
      end
    elsif data.include?(MILESTONE) && data[MILESTONE].blank?
      issue.milestone = nil
      data["milestone_id"] = nil
    end

    if data["labels"]
      labels = labels_for(repo, Array(data["labels"]))
      data["label_ids"] = labels&.map(&:id)
    end

    if integration_user_request?
      issue.modifying_integration = current_integration
      data["integration_id"] = current_integration.id
    end

    errors = nil
    saved = false
    timeout = false

    begin

      attributes = attr(data, :title)
      previous_title = issue.title
      previous_body = issue.body

      saved = if data["state"] =~ /\Aclose/i && issue.open?
        issue.close(current_user, attributes)
      elsif data["state"] =~ /\Aopen/i && issue.closed?
        issue.open(current_user, attributes)
      else
        GitHub::SchemaDomain.allowing_cross_domain_transactions { issue.update(attributes) }
      end

      if data.key?("body")
        issue.update_body(data["body"], current_user)
      end

      if saved
        issue.instrument_hydro_update_event(
          actor: current_user,
          updater: current_user,
          repo: repo,
          prev_title: previous_title,
          prev_body: previous_body,
        )
        issue.replace_labels(labels) unless labels.nil?
      else
        errors = issue.errors
      end
    rescue GitHub::Prioritizable::Context::LockedForRebalance
      deliver_error! 503,
        message: "This issue's milestone is temporarily locked for maintenance. Please try again.",
        documentation_url: @documentation_url
    end

    if saved
      GitHub::PrefillAssociations.for_issues([issue], repository: repo)
      Reaction::Summary.prefill([issue])
      deliver :issue_hash, issue, repo: repo, full: true
    else
      deliver_error (timeout ? 500 : 422),
        errors: errors,
        documentation_url: @documentation_url
    end
  end

  LockIssueQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($issueId: ID!, $lockReason: LockReason, $clientMutationId: String!) {
      lockLockable(input: {
        lockableId: $issueId,
        lockReason: $lockReason,
        clientMutationId: $clientMutationId
      }) {
        lockedRecord {
          locked
        }
      }
    }
  GRAPHQL

  put "/repositories/:repository_id/issues/:issue_number/lock" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#lock-an-issue"

    repo = current_repo
    issue = repo.issues.find_by_number int_id_param!(key: :issue_number)

    record_or_404(issue)
    @accepted_scopes = repo.public? ? %w[public_repo repo] : %w[repo]

    control_access :lock_issue,
      resource: issue,
      repo: repo,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    data = receive_with_schema("issue-lock", "close-legacy")
    lock_reason = data["lock_reason"]

    input_variables = {
      "issueId"          => issue.global_relay_id,
      "clientMutationId" => request_id,
      "enterprise"       => GitHub.enterprise?,
    }
    input_variables["lockReason"] = Platform::Enums::Base.convert_string_to_enum_value(lock_reason) if lock_reason.present?

    results = platform_execute(LockIssueQuery, variables: input_variables)

    if results.errors.all.any?
      errors = results.errors.all.details["data"]
      error_types = errors.map { |err| err["type"] }
      if !(error_types & %w(NOT_FOUND UNAUTHENTICATED)).empty?
        deliver_error! 404, message: "Not Found"
      elsif error_types.any? { |err| err == "FORBIDDEN" }
        deliver_error! 403, message: "Forbidden"
      elsif error_types.any? { |err| err == "REPOSITORY_MIGRATION" }
        deliver_error! 403, message: "Repository has been locked for migration."
      elsif error_types.any? { |err| err == "REPOSITORY_ARCHIVED" }
        deliver_error! 403, message: "Repository was archived so is read-only."
      elsif error_types.any? { |err| err == "ISSUES_DISABLED" }
        deliver_error! 410, message: "Issues are disabled for this repo", documentation_url: "/v3/issues/"
      else
        deliver_error! 422, errors: results.errors.values.flatten
      end
    end

    deliver_empty(status: 204)
  end

  UnlockIssueQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($issueId: ID!, $clientMutationId: String!) {
      unlockLockable(input: {
        lockableId: $issueId,
        clientMutationId: $clientMutationId
      }) {
        unlockedRecord {
          locked
        }
      }
    }
  GRAPHQL

  # Unlock an Issue's conversation
  delete "/repositories/:repository_id/issues/:issue_number/lock" do
    @route_owner = "@github/pe-issues-projects"

    # Introducing strict validation of the issue-lock.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("issue-lock", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/issues#unlock-an-issue"

    repo = current_repo
    issue = repo.issues.find_by_number int_id_param!(key: :issue_number)

    record_or_404(issue)
    @accepted_scopes = repo.public? ? %w[public_repo repo] : %w[repo]

    control_access :unlock_issue,
      resource: issue,
      repo: repo,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    input_variables = {
      "issueId"          => issue.global_relay_id,
      "clientMutationId" => request_id,
      "enterprise"       => GitHub.enterprise?,
    }

    results = platform_execute(UnlockIssueQuery, variables: input_variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error! errors: results.errors, resource: "Issue"
    elsif has_graphql_mutation_errors?(results)
      deliver_graphql_mutation_errors! results, input_variables: input_variables, resource: "Issue"
    end

    deliver_empty(status: 204)
  end

  get "/repositories/:repository_id/assignees" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-assignees"
    control_access :list_assignees,
      repo: current_repo,
      allow_integrations: true,
      allow_user_via_integration: true

    assignees = paginate_rel(get_assignees)
    deliver :user_hash, assignees
  end

  get "/repositories/:repository_id/assignees/:username" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#check-if-a-user-can-be-assigned"
    control_access :list_assignees, repo: current_repo, allow_integrations: true, allow_user_via_integration: true
    assignees = get_assignees

    if assignees.pluck(:login).include?(params[:username])
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  private

  def handle_issue_not_found
    repo  = current_repo
    if issue_transfer = IssueTransfer.find_from(repository: repo, number: int_id_param!(key: :issue_number))
      transferred_issue = issue_transfer.new_issue

      # Ensure we can see issues in the old repo
      control_access :list_issues,
        repo: repo,
        allow_integrations: true,
        allow_user_via_integration: true,
        approved_integration_required: false

      new_repo = transferred_issue.repository
      ensure_issues_enabled_or_pr!(new_repo, transferred_issue)

      if access_allowed?(:list_issues, repo: new_repo, user: current_user, allow_integrations: true, allow_user_via_integration: true)
        deliver_redirect!(api_url("/repos/#{new_repo.owner}/#{new_repo}/issues/#{transferred_issue.number}"), status: 301)
      else
        deliver_redirect!("", status: 301)
      end
    elsif DeletedIssue.where(repository_id: repo.id, number: int_id_param!(key: :issue_number)).exists? &&
      access_allowed?(:list_issues, repo: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true)
      deliver_error!(410, message: "This issue was deleted")
    else
      deliver_error!(404)
    end
  end

  def ensure_issue_visible!(repo, issue)
    ensure_repo_writable!(repo)
    ensure_issues_enabled_or_pr!(repo, issue)
    deliver_error!(404) unless issue_visible_to_user?(issue)
  end

  def issue_visible_to_user?(issue)
    issue.present? && !issue.hide_from_user?(current_user)
  end

  def valid_milestone_id?(id)
    id.is_a?(Numeric) || (id.is_a?(String) && id =~ /\A\d+\z/)
  end

  def filter_dash_scope(scope)
    if explicitly_false(params[:collab])
      scope = scope.excluding_repository_ids(@member_repo_ids)
    end

    if explicitly_false(params[:orgs])
      scope = scope.excluding_repository_ids(@org_repo_ids)
    end

    if explicitly_false(params[:owned])
      scope = scope.excluding_repository_ids(@owned_repo_ids)
    end

    if explicitly_false(params[:pulls])
      scope = scope.without_pull_requests
    end

    scope
  end

  def filter_issues_for_repos_without_installation(repo_ids_or_scope)
    repository_scope = if repo_ids_or_scope.is_a?(ActiveRecord::Relation)
      repo_ids_or_scope
    else
      Repository.with_ids(repo_ids_or_scope)
    end

    public_repository_ids = []
    private_repository_ids = []
    private_owner_ids = []

    repository_scope.pluck(:id, :public, :owner_id).each do |id, is_public, owner_id|
      if is_public
        public_repository_ids << id
      else
        private_repository_ids << id
        private_owner_ids << owner_id
      end
    end

    private_repository_ids.uniq!
    private_owner_ids.uniq!

    return Issue.for_repository_ids(public_repository_ids) if private_repository_ids.empty?

    both, issues, pull_requests = [], [], []
    GitHub.dogstats.time("api.issues.filter_issues_for_repos_without_installation") do
      installation_ids = Authorization.service.actor_ids_with_granular_permissions_on(
        actor_type: "IntegrationInstallation",
        subject_type: "Repository",
        subject_ids: private_repository_ids,
        owner_ids: private_owner_ids,
        permissions: %w(issues pull_requests),
      )

      IntegrationInstallation.where(integration: current_integration, id: installation_ids).each do |installation|
        ids = installation.repositories.where(public: false).pluck(:id)

        can_read_issues = Ability.can_at_least?(:read, installation.permissions["issues"])
        can_read_pull_requests = Ability.can_at_least?(:read, installation.permissions["pull_requests"])

        if can_read_issues && can_read_pull_requests
          both.concat(ids)
        elsif can_read_pull_requests
          pull_requests.concat(ids)
        elsif can_read_issues
          issues.concat(ids)
        end
      end

      # The integration might be installed on more repositories than the
      # set of repos we're actually interested in, so we build the intersection
      both &= private_repository_ids
      issues &= private_repository_ids
      pull_requests &= private_repository_ids
    end

    scopes = []
    scopes << Issue.joins(:repository).with_ids(public_repository_ids, field: "repositories.id") if public_repository_ids.any?
    scopes << Issue.joins(:repository).with_ids(both, field: "repositories.id") if both.any?
    scopes << Issue.joins(:repository).with_ids(pull_requests, field: "repositories.id").where("issues.pull_request_id IS NOT NULL") if pull_requests.any?
    scopes << Issue.joins(:repository).with_ids(issues, field: "repositories.id").where("issues.pull_request_id IS NULL") if issues.any?

    return Issue.none if scopes.empty?

    scopes.inject(:or)
  end

  def get_assignees
    current_repo.available_assignees.order("users.login").filter_spam_for(current_user)
  end

  # Internal: Looks up the user with the given login and determines whether the
  # user can be an assignee on issues in the given repository.
  #
  # repo  - A Repository.
  # login - A String username.
  #
  # Returns the User, or nil if the user doesn't exist or is not assignable to
  # issues in the given repo.
  def find_assignable_user(repo, login, issue)
    user = User.find_by_login(login)
    return nil unless user

    is_author = issue.user == user
    is_member = repo.assignable_member?(user)
    is_commenter = issue.comments.pluck(:user_id).include?(user.id)

    return nil unless is_author || is_member || is_commenter

    user
  end

  # Takes a given assignee passed in by the client, parses it for keywords (like
  # "*", "none", and so on), and correctly scopes the database query
  # accordingly.
  #
  # query - The query components Array to pass on to Issue::MysqlSearch.
  # login - The String login from the client that we want to use to filter.
  #
  # Returns a modified `query` object.
  def assignee_filter(query, login)
    case login
    when "*"
      query << [:assignee, login]
    when "none"
      query << [:no, "assignee"]
    else
      assignee = User.find_by_login(login)

      if assignee
        query << [:assignee, assignee.login]
      else
        deliver_error! 422,
          errors: [api_error(:Issue, :assignee, :invalid, value: login)],
          documentation_url: "/v3/issues/#list-issues"
      end
    end
  end

  # Filters the issue query to the user that's mentioned.
  #
  # query - The query components Array to pass on to Issue::MysqlSearch.
  # login - The String login from the client that we want to use to filter.
  #
  # Returns a modified `query` object.
  def mention_filter(query, login)
    query << [:mentions, login]
  end

  # Takes a given milestone passed in by the client, parses it for keywords (like
  # "*", "none", and so on), and correctly scopes the database query
  # accordingly.
  #
  # repo  - The Repository whose issues we're filtering.
  # query - The query components Array to pass on to Issue::MysqlSearch.
  # text  - The String text from the client that we want to use to filter.
  #
  # Returns a modified `query` object.
  def milestone_filter(repo, query, text)
    case text
    when Search::Filter::WILDCARD, Search::Filter::ANY
      query << [:milestone, Search::Filter::WILDCARD]
    when Search::Filter::NONE
      query << [:no, "milestone"]
    when Array, Hash
      deliver_error! 422,
        errors: [api_error(:Issue, :milestone, :invalid, value: text)],
        documentation_url: "/v3/issues/#list-issues"
    else
      milestone = repo.milestones.find_by_number(text.to_i)

      if milestone
        query << [:milestone, milestone.title]
      else
        deliver_error! 422,
          errors: [api_error(:Issue, :milestone, :invalid, value: text)],
          documentation_url: "/v3/issues/#list-issues"
      end
    end
  end

  # Maps API parameters to their search term syntax.
  SEARCH_PARAM_MAPPING = {
    STATE   => :state,
    CREATOR => :author,
    MENTIONED => :mentions,
  }

  # Takes the mappings above and prepopulates the query components from the
  # request params.
  def search_param_mapping(params)
    query = []

    SEARCH_PARAM_MAPPING.each do |key, term|
      query += Array(params[key]).map { |value| [term, value] }
    end

    query
  end

  def authorize_content(authorizable, operation = :create)
    authorization = ContentAuthorizer.authorize(current_user, :issue, operation, repo: authorizable)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end

  # Inspects the data for assignment via direct (single) assignment or multiple
  # assignment. Builds up a list of valid assignees, responding with a 422 on
  # the first invalid assignment.
  #
  #   repo - The Repository to check for assignability
  #   assignee_keys - Array of keys with values in the data hash
  #   data - The Hash of data parsed from the request
  #
  # Returns an Array of assignees
  def prepare_assignees(repo:, assignee_keys:, data:, documentation_url:, issue:)
    assignees = []

    assignee_keys.each do |key|
      assignee_logins = Array.wrap(data.fetch(key)).compact.uniq

      assignee_logins.each do |login|
        next if login.blank?
        if user = find_assignable_user(repo, login.to_s, issue)
          assignees << user
        else
          deliver_error! 422,
            errors: [api_error(:Issue, key.to_sym, :invalid, value: login)],
            documentation_url: documentation_url
        end
      end
    end

    assignees
  end
end
