# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client/self_hosted_runners"
require "github/launch_client"

# Register self hosted runners for Actions
class Api::RepositoryActionsRunners < Api::App
  areas_of_responsibility :api
  map_to_service :actions_runners

  include Api::App::GrpcHelpers
  include GitHub::LaunchClient

  def attempt_login
    input_token = Api::RequestCredentials.token_from_scheme(env, "remoteauth")
    scope = current_repo.runner_registration_token_scope

    token = User.verify_signed_auth_token(token: input_token, scope: scope)
    if !token.valid?
      if token.expired?
        deliver_error!(401, message: "Token expired.")
      else
        deliver_error! 404
      end
    end

    @current_user = token.user
    @remote_token_auth = true
  end

  post "/repositories/:repository_id/actions-runners/registration" do
    @route_owner = "@github/c2c-actions-experience-reviewers"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    require_preview(:actions_runners)

    deliver_error!(404) unless GitHub.actions_enabled?

    control_access :register_actions_runner_repo,
      resource: current_repo,
      user: current_user,
      allow_integrations: true,
      allow_user_via_integration: true

    # result is a GitHub::Launch::Services::Selfhostedrunners::RegisterRunnerResponse,
    # which contains result.token and result.url which represent a short-lived token
    # from the c2c-actions-service system (AZP) and the AZP URL the runner can
    # self-register at.
    result = rescue_from_grpc_errors("SelfHostedRunners") do
      SelfHostedRunners.get_runner_registration_token(current_repo, actor: current_user)
    end

    validate_result!(result)
    deliver :actions_runner_registration_hash, {
      url: result.url,
      token: result.token,
      token_schema: result.token_schema,
    }
  end

  private

  def validate_result!(result)
    unless result
      Failbot.report(StandardError.new("no response from register_runner"))
      deliver_error!(503, message: "Runner register service unavailable")
    end
  end
end
