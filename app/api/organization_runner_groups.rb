# frozen_string_literal: true

class Api::OrganizationRunnerGroups < Api::App
  include Api::App::GrpcHelpers
  map_to_service :actions_runners

  # List runner groups
  get "/organizations/:organization_id/actions/runner-groups" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org) && can_create_runner_groups?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.list_groups(owner: org)
    end

    validate_listing!(resp&.runner_groups)

    # We map to an Array so pagination works
    runner_groups = paginate_rel(resp&.runner_groups.map { |runner_group| runner_group })
    deliver :actions_org_runner_groups_hash, {
      runner_groups: runner_groups,
      total_count: runner_groups.total_entries,
      org: org,
    }
  end

  # Get a single runner group
  get "/organizations/:organization_id/actions/runner-groups/:runner_group_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.get_group(owner: org, group_id: params[:runner_group_id].to_i)
    end

    runner_group = resp&.runner_group
    deliver_error! 404 unless runner_group

    deliver :actions_org_runner_group_hash, {
      runner_group: runner_group,
      org: org,
    }
  end

  # Create a single runner group
  post "/organizations/:organization_id/actions/runner-groups" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)
    deliver_error! 404 unless can_create_runner_groups?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    data = receive_with_schema("organization-runner-group", "create-group")
    name = data["name"]
    runner_ids = data["runners"]
    visibility = GitHub::LaunchClient::RunnerGroups::FROM_VISIBILITY_MAP[data["visibility"]]
    selected_repository_ids = data["selected_repository_ids"]

    # Filter repositories within the organization
    org_repository_ids = org.repositories.where(id: selected_repository_ids).map(&:global_relay_id)

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.create_group(
        actor: current_user,
        owner: org,
        name: name,
        runner_ids: runner_ids,
        visibility: visibility,
        selected_targets: org_repository_ids
      )
    end

    runner_group = result&.runner_group
    validate_result!(runner_group)

    deliver :actions_org_runner_group_hash, {
      runner_group: runner_group,
      org: org,
    }, status: 201
  end

  # Delete a runner group
  delete "/organizations/:organization_id/actions/runner-groups/:runner_group_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("organization-runner-group", "delete-group")

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.delete_group(
        actor: current_user,
        owner: org,
        group_id: params[:runner_group_id].to_i,
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Update a runner group
  patch "/organizations/:organization_id/actions/runner-groups/:runner_group_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    data = receive_with_schema("organization-runner-group", "update-group")
    name = data["name"]
    visibility = GitHub::LaunchClient::RunnerGroups::FROM_VISIBILITY_MAP[data["visibility"]]

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.update_group(
        actor: current_user,
        owner: org,
        group_id: params[:runner_group_id].to_i,
        name: name,
        visibility: visibility,
      )
    end

    runner_group = resp&.runner_group
    deliver_error! 404 unless runner_group

    deliver :actions_org_runner_group_hash, {
      runner_group: runner_group,
      org: org,
    }
  end

  # Get runners in a runner group
  get "/organizations/:organization_id/actions/runner-groups/:runner_group_id/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.get_group(
        owner: org,
        group_id: params[:runner_group_id].to_i,
        include_runners: true,
      )
    end

    validate_listing!(resp&.runner_group&.runners)

    # We map to an Array so pagination works
    runners = paginate_rel(resp&.runner_group&.runners&.map { |runner| runner })
    deliver :actions_runners_hash, { runners: runners, total_count: runners.total_entries }
  end

  # Update runners in a runner group
  put "/organizations/:organization_id/actions/runner-groups/:runner_group_id/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    data = receive_with_schema("organization-runner-group", "update-runners")
    runner_ids = data["runners"]

    result = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.update_runners(
        actor: current_user,
        owner: org,
        group_id: params[:runner_group_id].to_i,
        runner_ids: runner_ids,
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Add a runner to a runner group
  put "/organizations/:organization_id/actions/runner-groups/:runner_group_id/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("organization-runner-group", "add-runner")

    result = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.add_runners(
        actor: current_user,
        owner: org,
        group_id: params[:runner_group_id].to_i,
        runner_ids: [params[:runner_id].to_i],
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Remove a runner from a runner group
  delete "/organizations/:organization_id/actions/runner-groups/:runner_group_id/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("organization-runner-group", "remove-runner")

    result = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.remove_runner(
        actor: current_user,
        owner: org,
        group_id: params[:runner_group_id].to_i,
        runner_id: params[:runner_id].to_i,
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # List the repositories that have access to a runner group
  get "/organizations/:organization_id/actions/runner-groups/:runner_group_id/repositories" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.get_group(owner: org, group_id: params[:runner_group_id].to_i)
    end

    runner_group = resp&.runner_group
    deliver_error! 404 unless runner_group

    repository_targets = runner_group.selected_targets.map { |identity| identity.global_id }.to_set
    repository_ids = repository_targets.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    filtered_repositories = org.repositories.where(id: repository_ids)
    paginated_repositories = paginate_rel(filtered_repositories.sorted_by(:full_name, "asc"))

    deliver :actions_runner_group_repositories_hash, { repositories: paginated_repositories, total_count: filtered_repositories.size }
  end

  # Update repositories that have access to a runner group
  put "/organizations/:organization_id/actions/runner-groups/:runner_group_id/repositories" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    data = receive_with_schema("organization-runner-group", "set-repositories")
    selected_repository_ids = data["selected_repository_ids"]

    # Filter repositories to org repositories
    org_repository_ids = org.repositories.where(id: selected_repository_ids).map(&:global_relay_id)

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.update_targets(
        owner: org,
        group_id: params[:runner_group_id].to_i,
        selected_targets: org_repository_ids)
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Add a repository to a runner group
  put "/organizations/:organization_id/actions/runner-groups/:runner_group_id/repositories/:repository_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("organization-runner-group", "add-repository")

    # Validate repository
    repo = find_repo!
    deliver_error! 422 unless repo.organization_id == org.id

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.add_target(
        owner: org,
        group_id: params[:runner_group_id].to_i,
        selected_target: repo.global_relay_id)
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Remove a repository from a runner group
  delete "/organizations/:organization_id/actions/runner-groups/:runner_group_id/repositories/:repository_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404 unless enterprise_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("organization-runner-group", "remove-repository")

    # Validate repository
    repo = find_repo!
    deliver_error! 422 unless repo.organization_id == org.id

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.remove_target(
        owner: org,
        group_id: params[:runner_group_id].to_i,
        selected_target: repo.global_relay_id)
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  private

  def enterprise_runners_enabled?
    GitHub.actions_enabled? && (
      GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) ||
      GitHub.enterprise?
    )
  end

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_runnergroups: GitHub.launch_runnergroups)
      deliver_error!(503, message: "Runner Groups unavailable. Please try again later.")
    end
  end

  def validate_result!(result)
    unless result
      Failbot.report(StandardError.new("no response from create runner group"), launch_runnergroups: GitHub.launch_runnergroups)
      deliver_error!(503, message: "Runner Groups unavailable. Please try again later.")
    end
  end

  def ensure_tenant!(org)
    result = rescue_from_grpc_errors("OrgTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(org.business) if org.business
      GitHub::LaunchClient::Deployer.setup_tenant(org)
    end
    deliver_error! 500 unless result
  end

  def can_use_org_runners?(org)
    Billing::ActionsPermission.new(org).status[:error][:reason] != "PLAN_INELIGIBLE"
  end

  def can_create_runner_groups?(org)
    GitHub.enterprise? || org.plan.business_plus? || org.plan.enterprise?
  end
end
