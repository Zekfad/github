# rubocop:disable Style/FrozenStringLiteralComment

class Api::Gists < Api::App
  # list all of a user's gists
  get "/user/:user_id/gists" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-gists-for-a-user"
    control_access :list_gists,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: false

    user = find_user!
    scope = all_or_public_gists_for_user(user)
    scope = scope.select(:id)

    # For performance, we first fetch the ids, then paginate the ids,
    # and finally fetch the gists for those ids
    paginated_gists = paginate_rel(filter(scope.most_recent)).to_a
    gist_ids = paginated_gists.map(&:id)
    gists = Gist.find(gist_ids).index_by(&:id)
    gists = gist_ids.map { |id| gists[id] }

    # Inject fetched gists into WillPaginate::Collection so that pagination
    # information is available when constructing the response
    total_gists = paginated_gists.total_entries
    paginated_gists.replace(gists)
    paginated_gists.total_entries = total_gists

    GitHub::PrefillAssociations.for_gists(paginated_gists, prefill_forks: false)
    deliver :gist_hash, paginated_gists, files: true
  end

  # list all public gists
  get "/gists/public" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-public-gists"
    control_access :list_gists,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: false

    cap_paginated_entries! Gist::MAX_PUBLIC_GISTS_TO_PAGINATE

    gists = public_gists
    GitHub::PrefillAssociations.for_gists(gists, prefill_forks: false)

    deliver :gist_hash, gists, files: true
  end

  # list the logged in user's gists
  # returns all public gists if called anonymously
  get "/gists" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-gists-for-the-authenticated-user"
    control_access :list_gists, resource: Platform::PublicResource.new, allow_integrations: false, allow_user_via_integration: false

    gists = if logged_in?
      scope = science "gists.most_recent_public_gists_for_user_with_index_hint" do |e|
        e.try { all_or_public_gists_for_user(current_user).most_recent.from("`#{Gist.table_name}` IGNORE INDEX FOR ORDER BY (PRIMARY)") }
        e.use { all_or_public_gists_for_user(current_user).most_recent }
        e.compare_record_sequence
      end
      paginate_rel(filter(scope))
    else
      cap_paginated_entries! Gist::MAX_PUBLIC_GISTS_TO_PAGINATE
      public_gists
    end

    GitHub::PrefillAssociations.for_gists(gists, prefill_forks: false)
    deliver :gist_hash, gists, files: true
  end

  # list the logged in user's starred gists
  get "/gists/starred" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-starred-gists"
    require_authentication!

    control_access :list_gists,
      resource: current_user,
      allow_integrations: false,
      allow_user_via_integration: false

    scope = all_or_public_gists_for_user(current_user, scope: current_user.starred_gists).alive.most_recent
    gists = paginate_rel(filter(scope))

    GitHub::PrefillAssociations.for_gists(gists, prefill_forks: false)
    deliver :gist_hash, gists, files: true
  end

  # get a gist
  get "/gists/:gist_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#get-a-gist"
    control_access :get_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false
    GitHub::PrefillAssociations.for_gists([gist], prefill_parent: true)
    deliver :gist_hash, gist, version: "master", full: true, last_modified: calc_last_modified(gist)
  end

  # create a gist
  post "/gists" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#create-a-gist"
    control_access :create_gist,
      resource: current_user,
      allow_integrations: false,
      allow_user_via_integration: false,
      challenge: true

    data = receive_with_schema("gist", "create-legacy")

    visibility = data.fetch("public", false)

    gist_creator = Gist::Creator.new \
      user: current_user,
      public: visibility,
      description: data["description"],
      contents: data_to_contents(data["files"]),
      creator_ip: logged_in? ? nil : remote_ip

    if gist_creator.create
      GitHub.dogstats.increment("gist.api.create", \
                                tags: ["visibility:#{gist_creator.gist.visibility}",
                                       "ownership:#{gist_creator.gist.ownership}"])

      GitHub::PrefillAssociations.for_gists([gist_creator.gist])
      deliver :gist_hash, gist_creator.gist, full: true, status: 201
    else
      deliver_error 422,
        errors: map_gist_errors(gist_creator.gist),
        documentation_url: "/v3/gists/#create-a-gist"
    end
  end

  # update a gist
  verbs :patch, :post, "/gists/:gist_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#update-a-gist"
    control_access :update_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false

    previous_gist_payload = gist.hydro_payload
    data = receive_with_schema("gist", "update-legacy")
    if data.key?("description")
      gist.description = data["description"]
    end

    begin
      saved = gist.update \
        contents: data_to_contents(data["files"], gist)
    rescue GitRPC::Failure => e
      handle_gitrpc_error e
    end

    if saved
      gist.instrument_hydro_update_event(actor: current_user, previous_gist_payload: previous_gist_payload)
      GitHub::PrefillAssociations.for_gists([gist])
      deliver :gist_hash, gist, full: true
    else
      deliver_error 422,
        errors: map_gist_errors(gist)
    end
  end

  # delete a gist
  delete "/gists/:gist_id" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the gist.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("gist", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/gists#delete-a-gist"
    control_access :delete_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false

    gist.remove

    GitHub.dogstats.increment("gist.api.destroy")

    deliver_empty status: 204
  end

  # fork a gist (singular, deprecated)
  # https://developer.github.com/v3/gists/#fork-a-gist
  #
  # DEPRECATED: Will be removed in API v4.
  post "/gists/:gist_id/fork" do
    @route_owner = "@github/pe-repos"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :fork_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false
    unprocessable_if_own! gist
    GitHub.dogstats.increment("gist.api.fork")

    deliver :gist_hash, gist.fork(current_user), version: "master", files: true, status: 201
  end

  # fork a gist
  post "/gists/:gist_id/forks" do
    @route_owner = "@github/pe-repos"
    receive_with_schema("gist", "fork")

    @documentation_url = "/rest/reference/gists#fork-a-gist"
    control_access :fork_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false
    unprocessable_if_own! gist
    forked_gist = gist.fork(current_user)
    if forked_gist.valid?
      GitHub::PrefillAssociations.for_gists([forked_gist])
      GitHub.dogstats.increment("gist.api.fork")

      deliver :gist_hash, forked_gist, version: "master", files: true, status: 201
    else
      deliver_error 422,
        errors: map_gist_errors(forked_gist),
        documentation_url: "/v3/gists/#fork-a-gist"
    end
  end

  # get gist forks
  get "/gists/:gist_id/forks" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-gist-forks"
    control_access :get_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false
    forks = gist.visible_forks
    forks = paginate_rel(forks)

    GitHub::PrefillAssociations.for_gists(forks)

    deliver :gist_hash, forks, last_modified: calc_last_modified(gist)
  end

  # check if a Gist is starred
  get "/gists/:gist_id/star" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#check-if-a-gist-is-starred"
    control_access :get_gist_star, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false

    if current_user.starred?(gist)
      deliver_empty(status: 204, last_modified: calc_last_modified(gist))
    else
      deliver_empty(status: 404)
    end
  end

  # star a gist
  put "/gists/:gist_id/star" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the gist.star
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("gist", "star", skip_validation: true)

    @documentation_url = "/rest/reference/gists#star-a-gist"
    control_access :star_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false

    current_user.star(gist, context: "api")

    deliver_empty(status: 204)
  end

  # unstar a gist
  delete "/gists/:gist_id/star" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the gist.unstar
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("gist", "unstar", skip_validation: true)

    @documentation_url = "/rest/reference/gists#unstar-a-gist"
    control_access :unstar_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false

    current_user.unstar(gist)

    deliver_empty(status: 204)
  end

  # get gist history
  get "/gists/:gist_id/commits" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-gist-commits"
    control_access :get_gist, resource: gist = find_gist!(param_name: :gist_id), allow_integrations: false, allow_user_via_integration: false

    commits = Api::Serializer.gist_history(gist, pagination)

    if commits.size == pagination[:per_page]
      @links.add_current({page: current_page + 1}, rel: "next")
    end

    if current_page > 1
      @links.add_current({page: 1}, rel: "first")
      @links.add_current({page: current_page - 1}, rel: "prev")
    end

    deliver_raw commits, last_modified: calc_last_modified(gist)
  end

  # get a gist revision
  get "/gists/:gist_id/:version" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#get-a-gist-revision"
    control_access :get_gist, resource: gist = Gist.find_by_repo_name(params[:gist_id]), allow_integrations: false, allow_user_via_integration: false

    commit = find_commit!(gist, params[:version], @documentation_url)
    deliver_error!(404) unless commit
    GitHub::PrefillAssociations.for_gists([gist],  prefill_parent: gist.fork?)

    deliver :gist_hash, gist, version: commit.oid, full: true, last_modified: calc_last_modified(gist)
  end

  private

  def public_gists
    scope = filter(Gist.alive.are_public.not_spammy.most_recent)

    paginate_rel(scope)
  end

  GIST_ERROR_MAP = {
    "can't be empty" => "can't be blank",
    "files can't be in subdirectories or include '/' in the name" => "is invalid",
  }

  def map_gist_errors(gist)
    errors = ActiveModel::Errors.new(gist)

    if GitHub.rails_6_0?
      gist.errors.each do |attr, msg|
        if attr.to_sym == :contents && (message = GIST_ERROR_MAP[msg])
          errors.add(:files, message)
        else
          errors.add(attr, msg)
        end
      end
    else
      gist.errors.each do |error|
        if error.attribute.to_sym == :contents && (message = GIST_ERROR_MAP[error.message])
          errors.add(:files, message)
        else
          errors.add(error.attribute, error.message)
        end
      end
    end

    errors
  end

  # Internal: Convert file params to contents form for Gists
  #
  # files - The inbound file params as required by the API
  # gist  - An optional Gist record for updates to existing Gists.
  def data_to_contents(files, gist = nil)
    return if files.blank?

    existing_contents = {}
    if gist
      gist.files.each { |file| existing_contents[file.name] = file }
    end

    files.map do |name, file|
      filename = (file && file["filename"]) || name
      new_file = { name: filename, value: (file && file["content"]) }

      existing_file = existing_contents[name]
      new_file[:oid] = existing_file.oid if existing_file

      if existing_file && new_file[:value].blank?
        # Allow file renaming without specifying content
        if filename != name
          new_file[:value] = existing_file.data
        else
          new_file[:delete] = true
        end
      end

      new_file
    end
  end

  def filter(scope)
    # filter by updated_at
    if (since = params[:since]).present?
      message = "Invalid since parameter: '#{since}'. Must be an ISO 8601 timestamp."
      url = "/v3/gists/#parameters"
      since = parse_time!(since, message: message, documentation_url: url).getlocal
      scope = scope.since(since)
    end
    scope
  end

  def handle_gitrpc_error(e)
    # check for deletion of non-existent file
    pattern = %r{index does not contain (\w.+) at stage 0}
    if matches = e.message.match(pattern)
      deliver_error! 422,
        message: "Cannot delete non-existent file: #{matches[1]}",
        documentation_url: "/v3/gists/#edit-a-gist"
    else
      deliver_error! 500, message: "Server error"
    end
  end

  # Internal: Halts and delivers a 422 if the owner of the gist is the current user.
  #
  # Delivers a 422 or returns nil.
  def unprocessable_if_own!(gist)
    if logged_in? && (gist.user_id == current_user.id)
      deliver_error! 422,
        message: "You cannot fork your own gist.",
        errors: [api_error(:Gist, :forks, :unprocessable)],
        documentation_url: "/v3/gists/#fork-a-gist"
    end
  end

  def all_or_public_gists_for_user(user, scope: nil)
    scope ||= user.gists.alive

    if access_allowed?(:list_user_secret_gists, target: user, allow_integrations: false, allow_user_via_integration: false)
      scope
    else
      scope.are_public
    end
  end
end
