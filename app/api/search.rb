# rubocop:disable Style/FrozenStringLiteralComment

class Api::Search < Api::App
  areas_of_responsibility :search, :api

  statsd_tag_actions "/search/code"
  statsd_tag_actions "/search/commits"
  statsd_tag_actions "/search/issues"
  statsd_tag_actions "/search/repositories"
  statsd_tag_actions "/search/users"

  module Helpers
    # Internal: Get the results for the given query, and handle any exceptions.
    #
    # query - The Search::Query to execute.
    #
    # If the search tier reports that the query exceeds the maximum offset, the
    # current API request is immediately halted, and an API response is sent to
    # communicate the problematic input.
    #
    # If the search tier fails during query execution, the exception is reported
    # to Failbot, and an empty Search::Results object is returned.
    #
    # Returns a Search::Results object.
    def results_for(query)
      if GitHub.elasticsearch_access_ignored?
        deliver_error! 503,
          message: "Search is currently unavailable. Try again later.",
          documentation_url: "/v3/search/"
      end

      unless query.valid_query?
        deliver_error! 422,
          errors: [
            api_error(:Search, :q, :invalid, message: query.invalid_reason),
          ],
          documentation_url: "/v3/search/"
      end

      begin
        query.execute
      rescue Search::Query::MaxOffsetError => boom
        deliver_error! 422,
          message: boom.message,
          documentation_url: "/v3/search/"
      rescue StandardError => boom
        failbot(env, exception: boom, fatal: "NO")
        Search::Results.empty
      end
    end

    def transform_highlights(results)
      results.each do |result|
        ::Search::OffsetHighlighter.transform result["highlight"]
      end
    end

    def sort_expression_for(search_type)
      sort = params[:sort]
      if Api::App::AcceptedSortOrderings.include?(params[:order])
        direction = params[:order]
      else
        direction = "desc"
      end

      return nil if sort.blank?

      return nil unless valid_sort_values_for(search_type).include?(sort)

      [sort, direction]
    end

    def valid_sort_values_for(search_type)
      case search_type
      when :issue_search then ::Search::Queries::IssueQuery::SORT_MAPPINGS.keys
      when :label_search then ::Search::Queries::LabelQuery::SORT_MAPPINGS.keys
      when :repo_search  then ::Search::Queries::RepoQuery::SORT_MAPPINGS.keys
      when :user_search  then ::Search::Queries::UserQuery::SORT_MAPPINGS.keys
      when :code_search  then ::Search::Queries::CodeQuery::SORT_MAPPINGS.keys
      when :commit_search then ::Search::Queries::CommitQuery::SORT_MAPPINGS.keys
      end
    end

    def highlight?
      medias.api_param?("text-match")
    end

    def highlight
      if highlight?
        ::Search::OffsetHighlighter.defaults
      else
        false
      end
    end
  end

  include Helpers

  rate_limit_as Api::RateLimitConfiguration::SEARCH_FAMILY

  # Internal: Overrides default auth scheme to also support Integration Bearer
  # Assertions (user for public searches from Enterprise)
  def attempt_login
    @current_user = nil
    assertion = Api::IntegrationAssertion.new(env)

    if assertion.valid?
      self.current_integration = assertion.integration
    else
      super
    end
  end

  before do
    cache_control "no-cache"

    if params[:q] && !params[:q].to_s.dup.force_encoding("UTF-8").valid_encoding?
      deliver_error! 422,
        errors: [api_error(:Search, :q, :unprocessable)],
        documentation_url: "/v3/search"
    end

    if !params[:q].present?
      deliver_error! 422,
        errors: [api_error(:Search, :q, :missing)],
        documentation_url: "/v3/search"
    end
  end

  get "/search/users" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-users"

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    query = ::Search::Queries::UserQuery.new \
      current_enterprise_installation: current_enterprise_installation,
      remote_ip: remote_ip,
      phrase: params[:q],
      source_fields: false,
      sort: sort_expression_for(:user_search),
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights)

    deliver :user_search_result_hash,
      results_for(query), highlight: highlight?
  end

  get "/search/topics" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-topics"
    require_preview(:repository_topics)

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    query = ::Search::Queries::TopicQuery.new \
      remote_ip: remote_ip,
      phrase: params[:q],
      source_fields: false,
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights)

    deliver :topic_search_result_hash, results_for(query), highlight: highlight?
  end

  get "/search/labels" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-labels"

    return deliver_error!(422) unless params[:repository_id].present?

    repo = find_repo!

    control_access :get_repo,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    query = ::Search::Queries::LabelQuery.new \
      remote_ip: remote_ip,
      phrase: params[:q],
      source_fields: false,
      repo_id: repo.id,
      sort: sort_expression_for(:label_search),
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights)

    deliver :label_search_result_hash, results_for(query), highlight: highlight?
  end

  get "/search/repositories" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-repositories"

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    query = ::Search::Queries::RepoQuery.new \
      current_user: current_user,
      current_enterprise_installation: current_enterprise_installation,
      remote_ip: remote_ip,
      phrase: params[:q],
      source_fields: false,
      sort: sort_expression_for(:repo_search),
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights)

    deliver :repo_search_result_hash, results_for(query),
      highlight: highlight?
  end

  get "/search/issues" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-issues-and-pull-requests"

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    # NOTE: If highlighting is requested, the response may have an IssueComment in the `text_matches`.
    #       Since `object_type` and `object_url` are extracted from _source[comments], we need
    #       source_fields when highlighting.
    source_fields = highlight? ? [:comments] : false
    query = ::Search::Queries::IssueQuery.new \
      allow_insecure_user_to_server_app_query: false,
      current_user: current_user,
      current_enterprise_installation: current_enterprise_installation,
      remote_ip: remote_ip,
      phrase: params[:q],
      source_fields: source_fields,
      sort: sort_expression_for(:issue_search),
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights)

    deliver :issue_search_result_hash, results_for(query), highlight: highlight?
  rescue Search::Queries::IssueQuery::InsecureUserToServerAppQuery => error
    deliver_error!(422, message: error.message)
  end

  get "/search/code" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-code"

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    query = ::Search::Queries::CodeQuery.new \
      current_user: current_user,
      current_enterprise_installation: current_enterprise_installation,
      remote_ip: remote_ip,
      phrase: params[:q],
      sort: sort_expression_for(:code_search),
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights),
      request_category: "api"

    ensure_codesearch_is_enabled!(query)

    deliver :code_search_result_hash, results_for(query),
      highlight: highlight?
  end

  get "/search/commits" do
    @route_owner = "@github/ee-search"
    @documentation_url = "/rest/reference/search#search-commits"
    require_preview(:commit_search)

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    query = ::Search::Queries::CommitQuery.new \
      current_user: current_user,
      current_enterprise_installation: current_enterprise_installation,
      remote_ip: remote_ip,
      phrase: params[:q],
      sort: sort_expression_for(:commit_search),
      page: pagination[:page],
      per_page: pagination[:per_page],
      highlight: highlight,
      normalizer: method(:transform_highlights)

    results = results_for(query)

    repo_commits = results.map { |result| [result["_source"]["repo_id"], result["_source"]["hash"]] }

    comment_counts = fetch_comment_counts(repo_commits)

    results.zip(comment_counts).each do |result, comment_count|
      result["_comment_count"] = comment_count
    end

    deliver :commit_search_result_hash, results,
      highlight: highlight?
  end

  def rate_limit_verified?
    !current_enterprise_installation.nil?
  end

  private

  # Queries MySQL for the number of comments on each given repo commit.
  #
  # repo_commits - the list of repo commits (e.g. [[3, <oid>], [5, <oid>], ...])
  #
  # Returns the respective comment counts (e.g. [2, 0, ...])
  def fetch_comment_counts(repo_commits)
    return [] if repo_commits == []

    sql = CommitComment.github_sql.new
    sql.add(<<-SQL)
      SELECT repository_id, commit_id, COUNT(*)
      FROM commit_comments
      WHERE
    SQL

    sql.add(<<-SQL)
      (
    SQL

    repo_commits.each_with_index do |(repository_id, commit_id), index|
      sql.add(" OR ") if index > 0

      sql.add(<<-SQL, repository_id: repository_id, commit_id: commit_id)
        (repository_id = :repository_id AND commit_id = :commit_id)
      SQL
    end

    sql.add(<<-SQL)
      )
    SQL

    if GitHub.spamminess_check_enabled?
      sql.add(<<-SQL)
         AND user_hidden = false
      SQL
    end

    sql.add(<<-SQL)
      GROUP BY repository_id, commit_id
    SQL

    count_map = sql.run.results.map { |repo_id, hash, count| [[repo_id, hash], count] }.to_h

    repo_commits.map { |key| count_map[key] || 0 }
  end

  def ensure_codesearch_is_enabled!(query)
    return if GitHub.enterprise?

    # Code search has been disabled for the current user or the current OAuth app.
    # https://github.com/devtools/feature_flags/disable_codesearch
    if codesearch_disabled?
      deliver_error!(403, message: "Code search is not available at this time.")
    end

    # Unauthenticated requests can not make global code search queries.
    if anonymous_global_code_search?(query)
      message = "Must include at least one user, organization, or repository"
      deliver_error! 422,
        errors: [api_error(:Search, :q, :invalid, message: message)],
        documentation_url: @documentation_url
    end
  end

  # Internal: Determine if code search is disabled for the logged in user or for
  # he current OAuth app.
  #
  # see https://github.com/devtools/feature_flags/disable_codesearch
  #
  # Returns `true` or `false`
  def codesearch_disabled?
    (logged_in? && current_user.codesearch_disabled?) ||
    (current_app_via_oauth && current_app_via_oauth.codesearch_disabled?)
  end

  # Internal: Returns true if the given `query` is a global code search query
  # being run from an unauthenticated API request.
  def anonymous_global_code_search?(query)
    query.valid_query? && query.global? && !logged_in? && !current_enterprise_installation
  end
end
