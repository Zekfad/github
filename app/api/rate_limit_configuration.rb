# frozen_string_literal: true

# Public: A RateLimitConfiguration represents the rate limit rules for a
# specific API consumer and resource family. For example, for a given consumer
# (e.g., a specific user, a specific remote IP address, a specific OAuth app)
# accessing a given API resource family (e.g., the "search" family), a
# RateLimitConfiguration can tell you the rate limit rules (e.g., 20 requests
# per minute).
#
# If you want to know the *status* of a consumer's rate limit (i.e., how many
# requests they've used and when the rate limit resets), check out
# Api::RateLimitStatus and Api::Throttler.
class Api::RateLimitConfiguration
  # Public: String name used to represent the family of resources governed by
  # the main API rate limit.
  DEFAULT_FAMILY = "core"

  # Public: String name used to represent the family of resources governed by
  # the Search API's rate limit.
  SEARCH_FAMILY = "search"

  # Public: String name used to represent the family of resources governed by
  # the Porter Import API's rate limit.
  SOURCE_IMPORT_FAMILY = "source_import"

  # Public: String name used to represent the Git LFS API.
  LFS_FAMILY = "lfs"

  # Public: String name used to represent the GraphQL API endpoint.
  GRAPHQL_FAMILY = "graphql"

  # Public: String name used to represent the intenral raw API endpoint.
  RAW_FAMILY = "raw"

  # Public: String name used to represent the intenral archive API endpoint.
  ARCHIVE_FAMILY = "archive"

  # Public: String name used to represent the App manifest API
  INTEGRATION_MANIFEST_FAMILY = "integration_manifest"

  # This lists the families shown on the /rate_limit endpoint.
  # See Api::RateLimitStatus.
  PUBLIC_FAMILIES = [DEFAULT_FAMILY, SEARCH_FAMILY, GRAPHQL_FAMILY, INTEGRATION_MANIFEST_FAMILY, SOURCE_IMPORT_FAMILY].freeze

  # This lists the private families not shown on the /rate_limit endpoint.
  # See Api::RateLimitStatus.
  PRIVATE_FAMILIES = [LFS_FAMILY, RAW_FAMILY, ARCHIVE_FAMILY].freeze

  # The entire list of rate limit families
  ALL_FAMILIES = (PUBLIC_FAMILIES + PRIVATE_FAMILIES).freeze

  # Families for which we allow dynamic rate limits coming from apps and installations
  DYNAMIC_RATE_FAMILIES = [DEFAULT_FAMILY, GRAPHQL_FAMILY]

  # Internal: Hash of per-family rate limits for exempt users. On GHES these
  # users can be configured and on dotcom that should only be Hubot.
  #
  # IMPORTANT: If you need a higher rate limit for your API requests see this
  # article:
  #
  # https://githubber.com/article/crafts/engineering/api-and-addons/increasing-api-rate-limits-for-hubbers
  EXEMPT_RATE_LIMITS = {
    DEFAULT_FAMILY => 120_000,
    SEARCH_FAMILY  => 500,
  }.freeze

  # Public: Provide the rate limit configuration for a specific API consumer and
  # resource family.
  #
  # family          - The String name of the resource family.
  # request_context - An object that describes the request. Specifically, the
  #                   object must respond to #current_user, #current_app, and
  #                   #remote_ip. This information is used to identify the API
  #                   consumer.
  #
  # Returns an object that supports the public interface of an
  # Api::RateLimitConfiguration instance.
  def self.for(family, request_context)
    config_params = case family
    when DEFAULT_FAMILY
      {
        request_context: request_context,
        family: DEFAULT_FAMILY,
        unauthenticated_limit: GitHub.api_unauthenticated_rate_limit,
        authenticated_limit: GitHub.api_default_rate_limit,
        enterprise_cloud_soft_limit: GitHub.api_enterprise_cloud_soft_rate_limit,
        enterprise_cloud_hard_limit: GitHub.api_enterprise_cloud_hard_rate_limit,
        duration: 1.hour,
      }
    when SEARCH_FAMILY
      {
        request_context: request_context,
        family: SEARCH_FAMILY,
        unauthenticated_limit: GitHub.api_search_unauthenticated_rate_limit,
        authenticated_limit: GitHub.api_search_default_rate_limit,
        enterprise_cloud_hard_limit: GitHub.api_search_enterprise_cloud_hard_rate_limit,
        verified_limit: GitHub.api_search_default_rate_limit,
        duration: 1.minute,
      }
    when SOURCE_IMPORT_FAMILY
      {
        request_context: request_context,
        family: SOURCE_IMPORT_FAMILY,
        unauthenticated_limit: 5,
        authenticated_limit: 100,
        duration: 1.minute,
      }
    when LFS_FAMILY
      {
        request_context: request_context,
        family: LFS_FAMILY,
        unauthenticated_limit: GitHub.api_lfs_unauthenticated_rate_limit,
        authenticated_limit: GitHub.api_lfs_default_rate_limit,
        duration: 1.minute,
      }
    when RAW_FAMILY
      {
        request_context: request_context,
        family: RAW_FAMILY,
        unauthenticated_limit: GitHub.api_raw_unauthenticated_rate_limit,
        authenticated_limit: GitHub.api_raw_default_rate_limit,
        verified_limit: GitHub.api_raw_default_rate_limit,
        duration: 1.minute,
      }
    when ARCHIVE_FAMILY
      {
        request_context: request_context,
        family: ARCHIVE_FAMILY,
        unauthenticated_limit: GitHub.api_archive_unauthenticated_rate_limit,
        authenticated_limit: GitHub.api_archive_default_rate_limit,
        verified_limit: GitHub.api_archive_default_rate_limit,
        duration: 1.minute,
      }
    when INTEGRATION_MANIFEST_FAMILY
      {
        request_context: request_context,
        family: INTEGRATION_MANIFEST_FAMILY,
        unauthenticated_limit: GitHub.api_integration_manifest_unauthenticated_rate_limit,
        authenticated_limit: GitHub.api_default_rate_limit,
        duration: 1.hour,
      }
    when GRAPHQL_FAMILY
      user = request_context.try(:current_user)
      limit = if user && GitHub.flipper[:graphql_higher_limit].enabled?(user)
        if user.site_admin?
          GitHub.api_graphql_much_higher_rate_limit
        else
          GitHub.api_graphql_higher_rate_limit
        end
      else
        GitHub.api_graphql_default_rate_limit
      end
      {
        request_context: request_context,
        family: GRAPHQL_FAMILY,
        unauthenticated_limit: GitHub.api_graphql_unauthenticated_rate_limit,
        authenticated_limit: limit,
        enterprise_cloud_hard_limit: GitHub.api_graphql_enterprise_cloud_hard_rate_limit,
        duration: 1.hour,
      }
    else
      raise ArgumentError, "Unhandled RateLimitConfiguration family: #{family.inspect}. Add a case for it, or use one of the defined families."
    end

    new(**config_params)
  end

  # @return [Boolean] true if this organization is on a high enough plan to qualify for an elevated rate limit
  def self.qualifies_for_higher_limit?(org)
    org.plan.business? || org.plan.business_plus?
  end

  attr_reader :family, :duration

  class << self
    # Use `.for(...)` to initialize RateLimitConfigurations, instead
    protected :new
  end

  # Initialize a RateLimitConfiguration.
  #
  # args - The Hash arguments used to initialize the instance:
  #        :family                - The String name of the resource family.
  #        :request_context       - An object that describes the request.
  #                                 Specifically, the object must respond to
  #                                 #current_user, #current_app, and #remote_ip.
  #        :unauthenticated_limit - The Integer maximum number of requests that
  #                                 an unauthenticated consumer can make for the
  #                                 resource family within the given rate limit
  #                                 duration.
  #        :authenticated_limit   - The Integer maximum number of requests that
  #                                 an authenticated consumer can make for the
  #                                 resource family within the given rate limit
  #                                 duration.
  #        :enterprise_cloud_soft_limit - If `request_context.request_owner` is an enterprise cloud customer,
  #                                       and this request is a dynamic rate limit family, this is the published limit.
  #                                       (In fact, they've got as much room as `enterprise_cloud_hard_limit`.)
  #        :enterprise_cloud_hard_limit - This is where we _actually_ cut off an enterprise cloud customer.
  #        :verified_limit        - The Integer maximum number of requests that
  #                                 an unauthenticated but verified consumer can
  #                                 make for the resource family within the
  #                                 given rate limit duration.
  #        :duration              - The Integer size of rate limit window in
  #                                 seconds.
  #
  # This constructor is not intended for use outside of this class. To obtain a
  # RateLimitConfiguration, use `RateLimitConfiguration.for`.
  def initialize(family:, request_context:, unauthenticated_limit:, authenticated_limit:, enterprise_cloud_soft_limit: nil, enterprise_cloud_hard_limit: nil, verified_limit: nil, duration:)
    @request_context       = request_context
    @user                  = request_context.current_user
    @family                = family
    @unauthenticated_limit = unauthenticated_limit
    @authenticated_limit   = @user && @user.spammy? ? @unauthenticated_limit : authenticated_limit
    @enterprise_cloud_soft_limit = enterprise_cloud_soft_limit || authenticated_limit
    @enterprise_cloud_hard_limit = enterprise_cloud_hard_limit || @enterprise_cloud_soft_limit
    @verified_limit        = verified_limit || unauthenticated_limit
    @duration              = duration

    @app = if request_context.current_app && !request_context.current_app.is_a?(Integration)
      request_context.current_app
    else
      nil
    end
    @integration_installation = request_context.current_integration_installation
    @request_owner = request_context.request_owner
    # Figure out what kind of authentication and API access scenario we have,
    # and assign limits based on that.
    #
    # Each branch must assign `@limit` and `@runway`
    #
    # Wrap this in in `reading` because for `POST /graphql`, Api::Middleware::DatabaseSelection
    # doesn't apply, but we still want to send these queries to a replica.
    ActiveRecord::Base.connected_to(role: :reading) do
      if @user && @user.rate_limit_exempt_user? && EXEMPT_RATE_LIMITS.key?(family)
        @limit = EXEMPT_RATE_LIMITS[family]
        @runway = 0
      elsif DYNAMIC_RATE_FAMILIES.include?(family) && @integration_installation.present?
        @limit, @runway = ghec_limit_or(@integration_installation.rate_limit)
      elsif DYNAMIC_RATE_FAMILIES.include?(family) && @app.present?
        if @app.suspended?
          @limit = @unauthenticated_limit
          @runway = 0
        else
          @limit, @runway = ghec_limit_or(@app.rate_limit)
        end
      elsif authenticated_request?
        @limit, @runway = ghec_limit_or(@authenticated_limit)
      elsif @request_context.respond_to?(:rate_limit_verified?) && @request_context.rate_limit_verified?
        @limit = @verified_limit
        @runway = 0
      else
        @limit = @unauthenticated_limit
        @runway = 0
      end
    end
  end

  # @return [Integer] the max number of requests allowed for this config.
  attr_reader :limit

  # This is the number of tries which we allow _before_
  # decrementing the "remaining tries" number.
  #
  # For example, it's used for GHEC customers to grant them
  # a limit _higher_ than the documented limit, which we
  # can turn off with a feature flag.
  #
  # The size of the runway is the
  # difference between the hard limit and the soft limit:
  #
  # requests: ..............................xx
  #           |----------------------------|   hard limit
  #                                 |------|   soft limit
  #           |---------------------|          runway
  #                                         ^^ rate-limited
  #
  # @return [Integer]
  attr_reader :runway

  def key
    @key ||= if @user.present?
      RateLimitKey.for(@user)
    elsif @app.present?
      RateLimitKey.for(@app)
    else
      @request_context.env["HTTP_X_CLIENT_IP"] || @request_context.remote_ip
    end
  end

  def authenticated_request?
    if defined?(@authenticated_request)
      @authenticated_request
    else
      # don't credit suspended oauth apps with authenticated rate limits
      @authenticated_request = @user || (@app && !@app.suspended?) || @request_context.current_integration
    end
  end

  private

  # If the GHEC limit applies to the `@request_owner` and
  # it's greater than `default_limit`, return it, along with the configured runway.
  # Otherwise, return the default limit and the default runway of `0`.
  # @param default_limit [Integer] returned if `@request_owner` doesn't qualify for the higher limit, or if `default_limit` is greater than the higher limit.
  # @param flag_enabled [Boolean] The flag check is done outside the method so that our linters recognize their usage
  # @return [Array(Integer, Integer)] The limit and runway
  def ghec_limit_or(default_limit)
    if @request_owner.present? && self.class.qualifies_for_higher_limit?(@request_owner)

      limit = if @enterprise_cloud_soft_limit > default_limit
        @enterprise_cloud_soft_limit
      else
        default_limit
      end

      runway = if GitHub.flipper[:ghec_api_soft_limit].enabled?(@request_owner)
        # The hard limit is bigger than the soft limit, and that's where we actually cut people off.
        # The runway is the difference between the (big) hard limit and the (small) soft limit,
        # during that range, people's rate limit doesn't seem to go down --
        # not until they reach the "end" of the hard limit, then it decrements.

        hard_limit = @enterprise_cloud_hard_limit

        # In some cases, the previous limit is greater than the
        # new GHEC limit.
        # For example, some integration installations get 12,500 req/h,
        # but the new GraphQL hard limit is 10,000.
        if hard_limit > limit
          hard_limit - limit
        else
          0
        end
      else
        0
      end

      [
        limit,
        runway
      ]
    else
      [default_limit, 0]
    end
  end
end
