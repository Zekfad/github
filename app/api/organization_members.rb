# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationMembers < Api::App
  areas_of_responsibility :orgs, :api

  # list members of :org
  get "/organizations/:organization_id/members" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-organization-members"
    org = find_org!

    control_access :apps_audited,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: false # we aren't enforcing it here because the filters underneath are.

    can_audit_two_factor = logged_in? && \
      meets_oauth_application_policy_for_this_org? && \
      access_allowed?(:list_two_factor_disabled_members,
      resource: org,
      allow_integrations: true,
      allow_user_via_integration: true,
    )

    if wants_two_factor_audit? && !can_audit_two_factor
      deliver_error! 422,
        message: "Only owners can use this filter.",
        documentation_url: "/v3/orgs/members/#audit-two-factor-auth"
    end

    if access_allowed?(:list_private_org_members,
        resource: org,
        allow_integrations: true,
        allow_user_via_integration: true,
      )
      user_ids = org.visible_user_ids_for(current_user, type: type_of_users_to_fetch)
    else
      user_ids = org.public_member_ids.take(Organization::MEGA_ORG_MEMBER_THRESHOLD)
    end

    users = if user_ids.empty?
      User.none
    else
      User.where(ActiveRecord::Base.sanitize_sql(["users.id IN (?)", user_ids]))
    end

    if wants_two_factor_audit?
      users = users.two_factor_disabled
    end

    users = paginate_rel(users.order("users.login"))
    GitHub::PrefillAssociations.for_profiles(users) if medias.api_param?("user-identity")
    deliver :user_hash, users
  end

  # list public members of :org
  get "/organizations/:organization_id/public_members" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-public-organization-members"
    org = find_org!
    @accepted_scopes = []
    control_access :list_public_members,
                   resource: org,
                   allow_integrations: true,
                   allow_user_via_integration: true,
                   enforce_oauth_app_policy: false

    users = org.visible_users_for(current_user, actor_ids: org.public_member_ids)

    users = users.sort_by(&:login).paginate(pagination)
    deliver :user_hash, users
  end

  # get if a user is a member of :org
  get "/organizations/:organization_id/members/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#check-organization-membership-for-a-user"
    org, user = find_org!, this_user

    @accepted_scopes = %w(read:org repo user)
    control_access :list_public_members,
                   resource: org,
                   enforce_oauth_app_policy: false,
                   allow_integrations: true,
                   allow_user_via_integration: true

    redirect(public_members_url) unless can_view_private_members?

    # requesters w/o private membership visibility are redirected to
    # equivalent public membership urls

    if user && org.visible_user_ids_for(current_user, actor_ids: user.id).any?
      deliver_empty(status: 204)
    else
      deliver_error 404,
        message: "User does not exist or is not a member of the organization",
        documentation_url: @documentation_url
    end
  end

  # get if a user is a public member of :org
  get "/organizations/:organization_id/public_members/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#check-public-organization-membership-for-a-user"
    org = find_org!
    @accepted_scopes = []
    control_access :get_public_member,
                   resource: org,
                   enforce_oauth_app_policy: false,
                   allow_integrations: true,
                   allow_user_via_integration: true

    user = User.find_by_login params[:username]

    if org.public_member?(user)
      deliver_empty(status: 204)
    else
      deliver_error 404,
        message: "User does not exist or is not a public member of the organization",
        documentation_url: @documentation_url
    end
  end

  # publicize a user's membership in :org
  put "/organizations/:organization_id/public_members/:username" do
    @route_owner = "@github/identity"
    receive_with_schema("organization-membership", "publicize")

    @documentation_url = "/rest/reference/orgs#set-public-organization-membership-for-the-authenticated-user"
    org = find_org!

    user = org ? org.find_direct_or_team_member_by_login(params[:username]) : nil

    forbid_unless_self(org, current_user, user)

    control_access :publicize_membership,
      resource: user,
      organization: org,
      allow_integrations: false,
      allow_user_via_integration: true

    if GitHub.private_org_membership_visibility_enforced?
      deliver_error! 403, message:
        "You cannot publicize membership because private membership is enforced."
    end

    org.publicize_member(user)
    deliver_empty(status: 204)
  end

  def forbid_unless_self(org, publicizer, publicizee)
    if org.adminable_by?(publicizer) && publicizer != publicizee
      set_forbidden_message("Only the user can publicize their membership")
    end
  end

  # remove a user as a member of :org
  delete "/organizations/:organization_id/members/:username" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the organization-membership.delete-member
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("organization-membership", "delete-member", skip_validation: true)

    @documentation_url = "/rest/reference/orgs#remove-an-organization-member"

    control_access :remove_member,
      resource: org = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    if org.has_full_trade_restrictions?
      deliver_error! 403,
        message: ::TradeControls::Notices::Plaintext.api_access_restricted,
        documentation_url: GitHub.trade_controls_help_url
    end

    user = this_user

    if org.direct_or_team_member?(user)
      attempt_remove_member_from_org(user, org)
    else
      deliver_error 404, message: "Cannot find #{params[:username]}"
    end
  end

  # conceal a user's membership in :org
  delete "/organizations/:organization_id/public_members/:username" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the organization-membership.conceal
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("organization-membership", "conceal", skip_validation: true)

    @documentation_url = "/rest/reference/orgs#remove-public-organization-membership-for-the-authenticated-user"
    org = find_org!

    user = org.people.where(login: params[:username]).first

    control_access :conceal_membership,
      resource: user,
      organization: org,
      allow_integrations: false,
      allow_user_via_integration: true

    if GitHub.public_org_membership_visibility_enforced?
      deliver_error! 403, message:
        "You cannot conceal membership because public membership is enforced."
    end

    org.conceal_member(user)
    deliver_empty(status: 204)
  end

  # add or update a user's membership with :org
  put "/organizations/:organization_id/memberships/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#set-organization-membership-for-a-user"
    org, user = find_org!, this_user

    set_forbidden_message "You must be an admin to add or update an organization membership."

    if org.invitation_rate_limit_exceeded?
      deliver_error! 403,
        message: org.invitation_rate_limit_error_message,
        documentation_url: "/v3/orgs/members/#rate-limits"
    end

    control_access :manage_org_users,
      resource: org,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_not_blocked! current_user, user

    if user.organization?
      deliver_error! 422, message: "Members must be users, not organizations."
    end

    # This is kept for now as is to avoid a breaking change in the API. This should actually be
    # a HTTP status 451.
    if org.has_full_trade_restrictions?
      deliver_error! 403,
        message: ::TradeControls::Notices::Plaintext.api_access_restricted,
        documentation_url: GitHub.trade_controls_help_url
    end

    # Introducing strict validation of the organization-membership.replace
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("organization-membership", "replace", skip_validation: true)
    requested_role = data["role"] || "member"

    if requested_role == "member" && current_user == user && org.adminable_by?(current_user)
      deliver_error! 403, message: "You cannot demote yourself. Admins must be demoted by another admin."
    end

    if %w(admin member).exclude?(requested_role)
      deliver_error! 422, message: "User must have a valid role."
    end

    if !org.direct_or_team_member?(user) && !org_invitation_sendable?(org, user)
      error_code = :no_seat
    else
      legacy_owners_team = org.legacy_owners_team

      # Munge the passed role to work for our member updating and inviting
      # methods.
      if requested_role == "admin"
        role   = :admin
        action = :admin
      else
        role   = :direct_member
        action = :read
      end

      # Temporary backwards compatibility
      #
      # The org making this request has direct org membership enabled, so this
      # API will be updating the user's role in the organization.
      #
      # This block of code keeps the Owners team in sync if they're changing
      # an admin to a member or a member to an admin.
      teams = []
      if role == :admin
        if org.direct_or_team_member?(user)
          legacy_owners_team.add_member user if legacy_owners_team
        else
          teams = [legacy_owners_team].compact
        end
      elsif role == :direct_member
        attempt_remove_member_from_legacy_owners_team(user, legacy_owners_team)
      end

      if org.direct_member? user
        begin
          org.update_member(user, action: action)
        rescue Organization::NoAdminsError
          deliver_error! 403, message: "You can't demote the last admin to a member."
        end
      elsif invitation = org.pending_invitation_for(user)
        invitation.update_attribute(:role, role)
        if role == :admin && legacy_owners_team.present?
          invitation.add_team(legacy_owners_team, inviter: current_user)
        end

        # If invites are disabled, go ahead and accept any existing invites
        if GitHub.bypass_org_invites_enabled?
          invitation.accept
        end
      elsif OrganizationInvitation::OptOut.opted_out?(org: org, invitee: user)
        deliver_error! 422, message: "The request could not be processed."
      else
        if GitHub.bypass_org_invites_enabled?
          org.add_member(user, action: action, adder: current_user)
        elsif user.blocked_by? org
          deliver_error! 422, message: "You cannot invite a user who is blocked by the organization."
        else
          org.invite(user, inviter: current_user, role: role, teams: teams)
        end
      end
    end

    if error_code.present?
      error = translate_error(error_code, org, @documentation_url)
      deliver_error(422, error)
    else
      deliver :org_membership_hash, org, user: user
    end
  end

  # get a user's membership of :org
  get "/organizations/:organization_id/memberships/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#get-organization-membership-for-a-user"
    org, user = find_org!, this_user

    set_forbidden_message "You must be a member of #{org.login} to see membership information for #{user.login}."

    control_access :get_member,
      resource: org,
      user: current_user,
      member: user,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    if membership_unviewable?(org, current_user, user)
      deliver_error 404
    else
      deliver :org_membership_hash, org, user: user
    end
  end

  delete "/organizations/:organization_id/memberships/:username" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the organization-membership.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("organization-membership", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/orgs#remove-organization-membership-for-a-user"
    org, user = find_org!, this_user

    set_forbidden_message "You must be an admin to remove an organization membership."
    control_access :manage_org_users,
      resource: org,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    invitation = org.pending_invitation_for(user)

    if invitation.present? && invitation.cancelable_by?(current_user)
      invitation.cancel(actor: current_user)
      deliver_empty(status: 204)
    elsif org.member?(user)
      attempt_remove_member_from_org(user, org)
    else
      deliver_error 404, message: "Cannot find #{params[:username]}"
    end
  end

  private

  def filters
    params[:filter].try(:split, ",") || []
  end

  def wants_two_factor_audit?
    filters.include? "2fa_disabled"
  end

  def can_view_private_members?
    access_allowed?(:get_member, resource: find_org, member: this_user, allow_integrations: true, allow_user_via_integration: true)
  end

  def public_members_url
    api_url("/organizations/#{params[:organization_id]}/public_members/#{params[:username]}")
  end

  def role
    case params[:role]
    when "admin"
      :admin
    when "member"
      :member_without_admin
    end
  end

  def type_of_users_to_fetch
    org = find_org!

    if role.present?
      return role if org.direct_member?(current_user)
      return role if current_integration.present? && org.resources.members.readable_by?(current_user)
    end

    :all
  end

  def org_invitation_sendable?(org, user)
    !org.at_seat_limit?
  end

  # Private: Translate an error code to an API 422 response.
  #
  # error_code        - A Symbol error code.
  # org               - An Organization.
  # documentation_url - String url for documentation.
  #
  # Returns a Hash.
  def translate_error(error_code, org, documentation_url)
    response = {
      message: "Validation Failed",
      errors: [{ code: error_code, field: :user }],
      documentation_url: documentation_url,
    }

    case error_code
    when :blocked
      response[:message] = "User is blocked. #{GitHub.support_link_text}."
    when :org
      response[:message] = "Cannot add an organization as a member."
    when :no_seat
      response[:message] = "You must purchase at least one more seat to add this user as a member."
      response[:documentation_url] = "https://github.com/organizations/#{org}/settings/billing/seats"
    when :restricted_org
      response[:message] = ::TradeControls::Notices::Plaintext.organization_account_restricted
      response[:documentation_url] = GitHub.trade_controls_help_url
    end

    response
  end

  # Private: Try to remove the specified member from the specified org. This
  # will deliver an API response, so don't try to deliver another one after
  # calling this.
  #
  # You should set @documentation_url before calling this method, so that
  # deliver_error will pick it up if necessary.
  #
  # member - User to attempt to remove.
  # org    - Organization to remove the member from.
  #
  # Returns nothing.
  def attempt_remove_member_from_org(member, org)
    if org.member?(member)
      begin
        org.remove_member(member, background_team_remove_member: true)
      rescue Organization::NoAdminsError
        deliver_error 403, message: "Cannot remove the last owner"
      else
        deliver_empty(status: 204)
      end
    else
      deliver_error 404, message: "Not a member"
    end
  end

  # Private: Try to remove the specified member from the specified legacy owners
  # team of the.
  #
  # member             - User to attempt to remove.
  # legacy_owners_team - Legacy owners team to remove the member from.
  #
  # Returns nothing.
  def attempt_remove_member_from_legacy_owners_team(member, legacy_owners_team)
    return if legacy_owners_team.nil?

    begin
      legacy_owners_team.remove_member(member)
    rescue Team::EmptyOwnersError
      deliver_error! 403, message: "You can't remove the last member of the owners team."
    end
  end

  # Private: When fetching a member, there are scenarios outside of basic access
  # control where we want to limit what the current_user can access. If the
  # member is inactive, is one example.
  #
  # Similarly, :billing_manager members are a special case where only org admins
  # should be aware of them as described here
  # https://github.com/github/github/issues/120931
  #
  # Returns a boolean.
  def membership_unviewable?(org, current_user, member)
    return true if org.membership_state_of(member) == :inactive
    return false if org.adminable_by?(current_user) || current_user == member

    org.billing_manager?(member) ||
      org.pending_invitation_for(member)&.role == "billing_manager"
  end
end
