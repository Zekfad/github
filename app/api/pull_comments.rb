# rubocop:disable Style/FrozenStringLiteralComment

class Api::PullComments < Api::App
  include Scientist
  areas_of_responsibility :code_collab, :pull_requests, :api

  statsd_tag_actions "/repositories/:repository_id/pulls/:pull_number/comments"
  statsd_tag_actions "/repositories/:repository_id/pulls/:pull_number/comments/:in_reply_to_id/replies"
  statsd_tag_actions "/repositories/:repository_id/pulls/comments/:comment_id"

  # List all review comments for a repo
  get "/repositories/:repository_id/pulls/comments" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#list-review-comments-in-a-repository"
    repo = find_repo!
    control_access :list_pull_requests,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    scope = repo.pull_request_review_comments
    scope = scope.filter_spam_for(current_user)
    scope = filter_and_sort(scope)
    review_comments = paginate_rel(scope)

    # Remove pending comments.
    filtered = review_comments.select(&:submitted?)

    # Wrap filtered array into a pagination collection.
    review_comments = WillPaginate::Collection.create(review_comments.current_page, review_comments.per_page, review_comments.total_entries) { |p| p.replace(filtered) }

    GitHub.dogstats.time "prefill", tags: ["via:api", "action:pulls_comments_list"] do
      GitHub::PrefillAssociations.for_review_comments(review_comments)
      Reaction::Summary.prefill(review_comments)

      deliver :pull_request_review_comment_hash, review_comments, repo: repo
    end
  end

  # List comments for a pull request review comments
  #
  # NOTE:
  # - Issue comments can be retrieved via Issues API.
  # - Commit comments can be retrieved via Commits API.
  get "/repositories/:repository_id/pulls/:pull_number/comments" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#list-review-comments-on-a-pull-request"
    repo, issue, pull = find_repo_and_pull_request
    control_access :list_pull_request_comments,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true

    scope = pull.review_comments.filter_spam_for(current_user)
    scope = filter_and_sort(scope)
    review_comments = paginate_rel(scope)

    # Remove pending comments.
    filtered = review_comments.select(&:submitted?)

    # Wrap filtered array into a pagination collection.
    review_comments = WillPaginate::Collection.create(review_comments.current_page, review_comments.per_page, review_comments.total_entries) { |p| p.replace(filtered) }

    GitHub::PrefillAssociations.for_review_comments(review_comments, issue: issue)
    Reaction::Summary.prefill(review_comments)

    deliver :pull_request_review_comment_hash, review_comments, repo: repo
  end

  # Share behavior between optional "in_reply_to" data key and new param.
  #
  # repo - The repository for this PR
  # pull - The pull request
  # rel - String that describes which part of the schema we should use (create-legacy or create-reply)
  def create_pull_request_review_comment(repo, pull, rel)
    data = receive_with_schema("pull-request-review-comment", rel)

    end_commit_oid = data["commit_id"] || pull.head_sha

    pull_comparison = PullRequest::Comparison.find(
      pull: pull,
      start_commit_oid: pull.merge_base,
      end_commit_oid: end_commit_oid,
      base_commit_oid: pull.merge_base,
    )

    unless pull_comparison
      deliver_error!(422, {
        errors: [
          api_error(:PullRequestReviewComment, :commit_id, :invalid, {
            message: "commit_id is not part of the pull request",
          }),
        ],
        documentation_url: @documentation_url,
      })
    end

    review = pull.reviews.build(user: current_user, head_sha: end_commit_oid)

    if (parent = yield pull, data).blank?
      if data&.has_key?("line")
        thread = review.build_thread

        attributes = {
          user: current_user,
          diff: pull_comparison.diffs,
          body: data["body"],
          path: data["path"],
          line: data["line"],
          side: data["side"]&.downcase&.to_sym || :right,
        }

        if data.has_key?("start_line")
          attributes.merge!(
            start_line: data["start_line"],
            start_side: data["start_side"]&.downcase&.to_sym || :right,
          )
        end

        comment = thread.build_first_comment(**attributes)
      else
        thread, comment = review.build_thread_with_comment(
          user: current_user,
          body: data["body"],
          diff: pull_comparison.diffs,
          position: data["position"].to_i,
          path: data["path"],
        )
      end
    else
      thread = parent.pull_request_review_thread
      comment = thread.build_reply(
        pull_request_review: review,
        user: current_user,
        body: data["body"],
      )
    end

    PullRequestReview.transaction do
      raise ActiveRecord::Rollback unless review.save && thread.save && comment.save
      review.comment!
    end

    if comment.new_record?
      errors = []
      errors.concat(convert_error(comment.errors)) if comment
      errors.concat(convert_error(thread.errors)) if thread && errors.empty?
      errors.concat(convert_error(review.errors)) if review && errors.empty?

      deliver_error(422, {
        errors: errors,
        documentation_url: @documentation_url,
      })
    else
      deliver :pull_request_review_comment_hash, comment, status: 201, repo: repo
    end
  end

  # Create a new pull request review comment
  post "/repositories/:repository_id/pulls/:pull_number/comments" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#create-a-review-comment-for-a-pull-request"

    repo, _issue, pull = find_repo_and_pull_request

    control_access :create_pull_request_comment,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true
    authorize_content :update, repo: repo

    rel = "create-legacy"
    create_pull_request_review_comment(repo, pull, rel) do |pull, data|
      ensure_data_satisfies_schema!(data, "pull-request-review-comment", rel)
      next unless data["in_reply_to"].present?

      parent = pull.review_comments.find_by_id(data["in_reply_to"])

      unless parent
        deliver_error!(422, {
          errors: [api_error(:PullRequestReviewComment, :in_reply_to, :invalid)],
          documentation_url: @documentation_url,
        })

        next
      end

      parent
    end
  end

  # Create a new pull request review comment
  post "/repositories/:repository_id/pulls/:pull_number/comments/:in_reply_to_id/replies" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#create-a-reply-for-a-review-comment"

    repo, _issue, pull = find_repo_and_pull_request

    control_access :create_pull_request_comment,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true
    authorize_content :update, repo: repo

    create_pull_request_review_comment(repo, pull, "create-reply") do |pull, data|
      ensure_data_satisfies_schema!(data, "pull-request-review-comment", "create-reply")
      in_reply_to = int_id_param!(key: :in_reply_to_id)

      parent = pull.review_comments.find_by_id(in_reply_to)

      unless parent
        deliver_error!(404, {
          message: "Parent comment not found",
          documentation_url: @documentation_url,
        })

        next
      end

      parent
    end
  end

  # Get a single pull request review comment
  get "/repositories/:repository_id/pulls/comments/:comment_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#get-a-review-comment-for-a-pull-request"
    repo, comment = find_repo_and_pull_request_comment

    control_access :get_pull_request_comment,
      repo: repo,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true

    GitHub::PrefillAssociations.for_review_comments([comment])
    Reaction::Summary.prefill([comment])

    deliver :pull_request_review_comment_hash, comment,
      repo: repo, last_modified: calc_last_modified(comment)
  end

  # Update a pull request review comment
  verbs :patch, :post, "/repositories/:repository_id/pulls/comments/:comment_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#update-a-review-comment-for-a-pull-request"
    repo, comment = find_repo_and_pull_request_comment

    control_access :update_pull_request_comment,
      repo: repo,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true
    authorize_content :update, repo: repo

    data = receive_with_schema("comment", "update-for-pull")

    if comment.update_body(data["body"], current_user)
      GitHub::PrefillAssociations.for_review_comments([comment])
      Reaction::Summary.prefill([comment])
      deliver :pull_request_review_comment_hash, comment, repo: repo
    else
      deliver_error 422,
        errors: comment.errors,
        documentation_url: @documentation_url
    end
  end

  delete "/repositories/:repository_id/pulls/comments/:comment_id" do
    @route_owner = "@github/pe-pull-requests"

    # Introducing strict validation of the comment.delete-for-pull
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("comment", "delete-for-pull", skip_validation: true)

    @documentation_url = "/rest/reference/pulls#delete-a-review-comment-for-a-pull-request"
    repo = find_repo!
    comment = repo.pull_request_review_comments.find_by_id(int_id_param!(key: :comment_id))

    deliver_error!(404) unless comment
    deliver_error!(404) if comment && comment.hide_from_user?(current_user)

    control_access :delete_pull_request_comment,
      repo: repo,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true
    ensure_repo_writable!(repo)

    comment.destroy

    deliver_empty(status: 204)
  end

private
  def find_repo_and_pull_request
    repo = find_repo!
    issue = repo.issues.find_by_number(int_id_param!(key: :pull_number)) if repo
    pull = issue.pull_request if issue

    deliver_error!(404) if pull && pull.hide_from_user?(current_user)

    [repo, issue, pull]
  end

  def find_repo_and_pull_request_comment
    repo = find_repo!
    comment = repo.pull_request_review_comments.find_by_id(int_id_param!(key: :comment_id))
    deliver_error!(404) unless comment
    deliver_error!(404) if comment && comment.hide_from_user?(current_user)
    deliver_error!(404) if comment && comment.pending?

    [repo, comment]
  end

  def filter_and_sort(scope)
    # filter by updated_at
    if (since = params[:since]).present?
      since = parse_time!(since).getlocal
      scope = scope.since(since)
    end

    if (sort = params[:sort]).present?
      direction = params[:direction] || "asc"
      scope = scope.sorted_by sort, direction
    end

    scope
  end

  def paginated_ids(relation)
    paginate_rel(relation.scoped(select: "#{relation.quoted_table_name}.id")).map(&:id)
  end

  def authorize_content(operation = :create, data = {})
    authorization = ContentAuthorizer.authorize(current_user, :pull_request_comment, operation, data)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end
end
