# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryPreReceiveHooks < Api::App
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  get "/repositories/:repository_id/pre-receive-hooks" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#list-pre-receive-hooks-for-a-repository"

    control_access :list_repo_pre_receive_hooks, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    targets = PreReceiveHookTarget.visible_for_hookable(repo)
    targets = paginate_rel(sort(targets))
    GitHub::PrefillAssociations.for_pre_receive_hook_targets targets
    deliver :pre_receive_repo_target_hash, targets
  end

  get "/repositories/:repository_id/pre-receive-hooks/:pre_receive_hook_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#get-a-pre-receive-hook-for-a-repository"

    control_access :read_repo_pre_receive_hooks, repo: find_repo!, allow_integrations: true, allow_user_via_integration: true
    target = find_pre_receive_hook_target!(param_name: :pre_receive_hook_id)
    GitHub::PrefillAssociations.for_pre_receive_hook_targets [target]
    deliver :pre_receive_repo_target_hash, target
  end

  verbs :patch, :post, "/repositories/:repository_id/pre-receive-hooks/:pre_receive_hook_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#update-pre-receive-hook-enforcement-for-a-repository"

    control_access :update_repo_pre_receive_hooks, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    data = receive_with_schema("pre-receive-hook", "update-for-repo-legacy")
    accepted_attributes = attr(data, :enforcement)
    begin
      target = PreReceiveHookTarget.override_upstream_target(repo, int_id_param!(key: :pre_receive_hook_id), accepted_attributes)
      deliver_error! 422, errors: target.errors if target.errors.present?
      GitHub::PrefillAssociations.for_pre_receive_hook_targets [target]
      deliver :pre_receive_repo_target_hash, target, status: 200
    rescue PreReceiveHookTarget::NotAllowedByUpstreamError
      deliver_error! 422, message: "Overriding settings is disallowed by an upstream configuration"
    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique
      deliver_error! 409, message: "Conflicting updates for this record"
    end
  end

  delete "/repositories/:repository_id/pre-receive-hooks/:pre_receive_hook_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#remove-pre-receive-hook-enforcement-for-a-repository"

    control_access :delete_repo_pre_receive_hooks, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    enforcement_target = find_pre_receive_hook_target!(param_name: :pre_receive_hook_id)
    # only delete if enforcement_target's hookable is the same org from the url
    unless enforcement_target.hookable == repo
      deliver_error! 422, message: "There is no enforcement override to destroy"
    end
    enforcement_target.destroy
    target = find_pre_receive_hook_target!(param_name: :pre_receive_hook_id)
    GitHub::PrefillAssociations.for_pre_receive_hook_targets [target]
    deliver :pre_receive_repo_target_hash, target
  end

  private

  def sort(scope)
    scope.sorted_by("hook.#{params[:sort] || "id"}", params[:direction] || "asc")
  end
end
