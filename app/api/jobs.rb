# frozen_string_literal: true

class Api::Jobs < Api::App
  map_to_service :actions_experience

  ALLOWED_FILTER_PARAMS = ["latest", "all"]

  # Get all jobs for a run.
  get "/repositories/:repository_id/actions/runs/:run_id/jobs" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-jobs-for-a-workflow-run"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    deliver_error! 404 unless workflow_run

    deliver_error!(404) unless repo.id == workflow_run.check_suite.repository_id

    filter = params[:filter]
    filter = "latest" unless ALLOWED_FILTER_PARAMS.include?(filter)

    jobs = filter == "latest" ? workflow_run.latest_jobs : workflow_run.jobs
    jobs = paginate_rel(jobs)

    deliver :jobs_hash, { jobs: jobs, total_count: jobs.total_entries }
  end

  # Get a single job.
  get "/repositories/:repository_id/actions/jobs/:job_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-job-for-a-workflow-run"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    job = CheckRun.includes(:steps).find_by(id: params[:job_id])
    deliver_error! 404 unless job

    deliver_error!(404) unless repo.id == job.check_suite.repository_id

    deliver :job_hash, job
  end

  # Get logs for a job
  get "/repositories/:repository_id/actions/jobs/:job_id/logs" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#download-job-logs-for-a-workflow-run"

    repo = find_repo!

    control_access :read_actions_downloads,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    job = CheckRun.find_by(id: params[:job_id])
    deliver_error! 404 unless job

    deliver_error!(404) unless repo.id == job.check_suite.repository_id
    deliver_error!(404) unless job.completed_log_url
    deliver_error!(410) if job.expired_logs?

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::ExchangeURLRequest.new({
        unauthenticated_url: job.completed_log_url,
        repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repo.global_relay_id),
        resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_COMPLETED_JOB_LOG,
      })

      GitHub.launch_artifacts_exchange_for_check_suite(job.check_suite).exchange_url(grpc_request)
    end

    if result.call_succeeded?
      redirect result.value.authenticated_url
    else
      deliver_error! 500, message: "Failed to generate URL to download logs"
    end
  end
end
