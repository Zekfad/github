# rubocop:disable Style/FrozenStringLiteralComment

require "set"
require "github/pi_media_type"

module Api
  # Represents a parsed GitHub media type.
  class MediaType < GitHub::PiMediaType
    V3 = DefaultVersion = :v3
    V4 = :v4
    PlatformPreviews = Platform::Schema.enabled_previews.map { |preview| preview.toggled_by.to_s }
    Versions = Set.new(%w(
      beta
      v4
      v3
      two-gun-kid-preview
    )) + PlatformPreviews + REST::Previews.media_versions

    def self.default(api_version = V3)
      new("application/vnd.github.#{api_version}+json", api_version)
    end

    # Provide the list of valid API versions, excluding the default version.
    #
    # Returns an Enumerable of Strings.
    def self.non_default_versions
      Versions - [V3.to_s, V4.to_s]
    end

    def self.identify_api_version(path)
      path == "/graphql" ? V4 : V3
    end

    def initialize(type, api_version = V3)
      super(type)
      @implicit_api_version = api_version
    end

    def api?
      @string =~ /(application|text|\*)\/(json|\*)/i || vendor =~ /^github/i
    end

    def json?
      suffix == "json"
    end

    # Provide the media type's derived API version. If the media type does not
    # explicitly declare a valid GitHub API version, we use the default API
    # version.
    #
    # Returns a String.
    def api_version
      @api_version ||= parse_api_options(:version)
    end

    # Determine whether the media type's derived version matches the given
    # version.
    #
    # version - String or Symbol API version name (e.g., 'beta', 'v3').
    #
    # Returns a Boolean.
    def api_version?(version)
      api_version == version.to_sym
    end

    # Determine whether the media type explicitly declares the given version.
    #
    # version - String or Symbol API version name (e.g., 'beta', 'v3').
    #
    # Examples
    #
    #   MediaType.new('application/json').explicit_api_version?('v3')
    #   # => false
    #
    #   MediaType.new('application/vnd.github').explicit_api_version?('v3')
    #   # => false
    #
    #   MediaType.new('application/vnd.github.beta').explicit_api_version?('v3')
    #   # => false
    #
    #   MediaType.new('application/vnd.github.v3').explicit_api_version?('v3')
    #   # => true
    #
    # Returns a Boolean.
    def explicit_api_version?(version)
      @string =~ /\Aapplication\/vnd.github([-\w]*)?.#{version}/
    end

    def preview_version?
      api_version.to_s.end_with?("-preview")
    end

    def v3?
      api_version?(V3)
    end

    def v4?
      api_version?(V4)
    end

    def api_params
      @api_params ||= parse_api_options(:params)
    end

    def api_param?(param)
      param && api_params.include?(param.to_sym)
    end

    def to_http_header
      return "unknown" unless api?
      header = "github.#{api_version}"
      header << "; param=%s" % api_params.to_a.join(".") if !api_params.empty?
      header << "; format=%s" % suffix unless suffix.empty?
      header
    end

    def parse_api_options(return_suffix)
      @api_version = @implicit_api_version
      @api_params = Set.new

      if pieces = version && version.split(".")
        if Versions.include?(pieces[0])
          @api_version = pieces.shift.to_sym
        end

        pieces.each do |param|
          @api_params << param.to_sym
        end
      end

      instance_variable_get "@api_#{return_suffix}"
    end
  end
end
