# rubocop:disable Style/FrozenStringLiteralComment

class Api::Grants < Api::App
  areas_of_responsibility :api

  DEPRECATION_WARNING_INTERVAL = 1.month
  FORBIDDEN_MESSAGE = "This API can only be accessed with username and " +
                      "password Basic Auth"

  DEPRECATION_DATE = DateTime.iso8601("2020-02-14T16:00:00+00:00")
  SUNSET_DATE = DateTime.iso8601("2020-11-13T16:00:00+00:00")
  DEPRECATION_BLOG_POST = "#{GitHub.developer_blog_url}/2020-02-14-deprecating-oauth-auth-endpoint/"

  # This API endpoint provides access to OAuth authorizations. Due to backward
  # compatibility, the naming convention used in this API can be confusing.
  # Externally an OAuthAccess is called an "authorization" and an
  # OAuthAuthorization is called a "grant". The `/authorizations` endpoints
  # include the ability to list, get, create, update, and delete OAuth accesses
  # (authorizations) and the `/applications/grants` endpoints lets you list,
  # get, and delete their associated authorizations (grants).
  #
  # Because this endpoint deals with authentication credentials, it is only
  # accessible via basic authorization.
  before do
    if GitHub.flipper[:brownout_grants_api_removal].enabled?(current_user)
      deliver_error! 404, message: "You are receiving this error due to a service brownout. Please see #{DEPRECATION_BLOG_POST} for more information."
      return
    end

    @accepted_scopes = []
    set_forbidden_message(FORBIDDEN_MESSAGE, true)
    check_authorization { logged_in? && !current_user.using_oauth? &&
      current_user.can_authenticate_via_username_password_basic_auth?
    }

    if GitHub.authorization_apis_and_password_authentication_deprecated?
      deprecated(
        deprecation_date: DEPRECATION_DATE,
        sunset_date: SUNSET_DATE,
        info_url: DEPRECATION_BLOG_POST,
        alternate_path_url: nil, # these APIs are not being replaced
      )
    end
  end

  after do
    if logged_in? && current_user.using_basic_auth? && GitHub.authorization_apis_and_password_authentication_deprecated? && !GitHub.flipper[:brownout_grants_api_removal].enabled?(current_user)
      if notify_about_grants_api_deprecation?
        AccountMailer.authorizations_grants_api_basic_auth_deprecation(route: "Applications", user: current_user, user_agent: request.user_agent, blog_endpoint: "2020-02-14-deprecating-oauth-app-endpoint/").deliver_later
      end

      if notify_app_owner_about_grants_api_deprecation?
        AccountMailer.app_owner_authorizations_grants_api_basic_auth_deprecation(route: "Applications", oauth_application: @grant_application, user_agent: request.user_agent, blog_endpoint: "2020-02-14-deprecating-oauth-app-endpoint/").deliver_now
      end
    end
  end

  # Private: Determine if we should notify a user the basic auth deprecation.
  def notify_about_grants_api_deprecation?
    return false if @grant_application && @grant_application.id != OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    GitHub.flipper[:notify_about_grants_api_deprecation].enabled?(current_user) &&
    !GitHub.flipper[:opt_out_of_notify_about_grants_api_deprecation].enabled?(current_user) &&
      GitHub::ActionRestraint.perform?(
        "notify_about_grants_api_deprecation",
        interval: DEPRECATION_WARNING_INTERVAL,
        user_id: current_user.id,
      )
  end

  # Private: Determine if we should notify the owner of a given OAuth
  # application of the basic auth deprecation.
  def notify_app_owner_about_grants_api_deprecation?
    return false unless @grant_application
    return false if @grant_application.id == OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    GitHub.flipper[:notify_app_owner_about_grants_api_deprecation].enabled?(@grant_application.owner) &&
    !GitHub.flipper[:opt_out_of_notify_app_owner_about_grants_api_deprecation].enabled?(@grant_application.owner) &&
      GitHub::ActionRestraint.perform?(
        "notify_app_owner_about_grants_api_deprecation",
        interval: DEPRECATION_WARNING_INTERVAL,
        application_type: @grant_application.class.name,
        application_id: @grant_application.id,
      )
  end

  def password_auth_deprecated?
    false
  end

  # List OAuth authorizations
  #
  # Returns a list of OAuth authorizations for the logged in user.
  get "/applications/grants" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#list-your-grants"
    control_access :apps_audited, resource: Platform::PublicResource.new, allow_integrations: false, allow_user_via_integration: false

    scoped = current_user.oauth_authorizations.third_party
    if key = params[:client_id].presence
      scoped = scoped.for_client_id(key)
      @grant_application = OauthApplication.where(key: params[:client_id]).first || Integration.where(key: params[:client_id]).first
    end

    authorizations = paginate_rel(scoped)

    GitHub::PrefillAssociations.fill_applications(authorizations)

    deliver :oauth_authorization_hash, authorizations
  end

  # Get a specific OAuth authorization
  #
  # Returns an OAuth authorization.
  get "/applications/grants/:grant_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#get-a-single-grant"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    if authorization = current_user.oauth_authorizations.third_party.find_by_id(int_id_param!(key: :grant_id))
      @grant_application = authorization.application
      GitHub::PrefillAssociations.fill_applications([authorization])
      deliver :oauth_authorization_hash, authorization,
        last_modified: calc_last_modified(authorization)
    else
      deliver_error 404
    end
  end

  # Delete an OAuth authorization
  #
  # Destroys an OAuth authorization.
  delete "/applications/grants/:grant_id" do
    @route_owner = "@github/ecosystem-apps"

    # Introducing strict validation of the application-grant.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("application-grant", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/oauth-authorizations#delete-a-grant"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    if authorization = current_user.oauth_authorizations.third_party.find_by_id(int_id_param!(key: :grant_id))
      @grant_application = authorization.application
      authorization.destroy_with_explanation(:api_user)
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end
end
