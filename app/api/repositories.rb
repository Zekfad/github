# rubocop:disable Style/FrozenStringLiteralComment

require "cache_key_logging_blacklist"

class Api::Repositories < Api::App

  USER_REPOS_TTL = 1.minute
  ALLOWED_AFFILIATIONS = [
    "owner".freeze,
    "collaborator".freeze,
    "organization_member".freeze,
  ].freeze

  DEFAULT_AFFILIATION_STRING = ALLOWED_AFFILIATIONS.join(",").freeze

  include Api::App::EarthsmokeKeyHelper
  include Settings::SecurityAnalysisSettings

  statsd_tag_actions "/user/repos"

  map_to_service :repo_info, only: [
    "GET /user/repos",
    "GET /user/:user_id/repos",
    "GET /organizations/:organization_id/repos",
    "GET /repositories/:repository_id",
    "PATCH /repositories/:repository_id",
    "POST /repositories/:repository_id",
    "GET /repositories/:repository_id/contributors",
    "GET /repositories/:repository_id/languages",
    "GET /repositories/:repository_id/forks",
    "GET /repositories",
    "GET /repositories/:repository_id/mentionables/users"
  ]

  map_to_service :repo_creation, only: [
    "POST /user/repos",
    "POST /organizations/:organization_id/repos",
    "POST /repositories/:template_repository_id/generate",
    "POST /repositories/:repository_id/forks"
  ]

  map_to_service :repo_admin, only: [
    "POST /repositories/:repository_id/transfer"
  ]

  map_to_service :repo_deletion, only: [
    "DELETE /repositories/:repository_id"
  ]

  map_to_service :teams, only: [
    "GET /repositories/:repository_id/teams"
  ]

  map_to_service :ref, only: [
    "GET /repositories/:repository_id/tags"
  ]

  map_to_service :diff, only: [
    "GET /repositories/:repository_id/compare/*"
  ]

  # List repositories for the authenticated user.
  get "/user/repos" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-repositories-for-the-authenticated-user"
    control_access :list_repos,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if params["type"] && (params["visibility"] || params["affiliation"])
        deliver_error!(422,
          message: "If you specify visibility or affiliation, you cannot specify type.",
          documentation_url: "/rest/reference/repos#list-repositories-for-the-authenticated-user",
        )
    end

    default_sort = :full_name
    type = params.delete("type")
    metric_type = type
    legacy = true

    case type
    when "all"
      params["affiliation"] = DEFAULT_AFFILIATION_STRING
      params["visibility"]  = "all"
    when "owner"
      default_sort = :name
      params["affiliation"] = "owner"
      params["visibility"]  = "all"
    when Repository::PUBLIC_VISIBILITY
      params["affiliation"] = DEFAULT_AFFILIATION_STRING
      params["visibility"]  = Repository::PUBLIC_VISIBILITY
    when Repository::PRIVATE_VISIBILITY
      default_sort = :name
      params["affiliation"] = DEFAULT_AFFILIATION_STRING
      params["visibility"]  = Repository::PRIVATE_VISIBILITY
    when "member"
      params["affiliation"] = "collaborator"
      params["visibility"]  = "all"
    else
      metric_type = "other"
      legacy = false
      params["affiliation"] ||= DEFAULT_AFFILIATION_STRING
    end

    visibility = params["visibility"] || "all"
    affiliation = ALLOWED_AFFILIATIONS & params["affiliation"].split(",").map(&:strip)

    GitHub.dogstats.time "user", tags: ["action:#{legacy ? "legacy_repos" : "user_repos"}", "type:#{metric_type}"] do
      # Build a cache keyed by:
      #   * Namespace
      #   * User ID
      #   * token used for auth, can affect output by org whitelisting
      #   * when user has performed writes to expire when creating/deleting
      #   * combination of visibility and affiliation specified in request
      #   * current_integration_id (can change results)
      #   * protected_org_ids (ditto)
      #   * the attribute used for sorting
      #   * the direction of the sort
      cache_key = [
        CacheKeyLoggingBlacklist::API_REPOS_PREFIX,
        current_user.id,
        current_user.try(:oauth_access).try(:id),
        last_write_timestamp_for_current_user,
        affiliation.join(","),
        visibility,
        (current_integration&.id || "0"),
        protected_organization_ids.sort.join(","),
        params[:sort],
        params[:direction],
      ].join(":")

      repo_ids = GitHub.cache.fetch(cache_key, ttl: USER_REPOS_TTL, stats_key: "api.cache.user-repos.#{metric_type}") do
        include_org_owned_repos = access_allowed?(:list_associated_public_org_owned_repos,
                                                 resource: current_user,
                                                 allow_integrations: false,
                                                 allow_user_via_integration: true,
                                                 installation_required: false)

        including = []
        including << :owned    if affiliation.include?("owner")
        including << :direct   if affiliation.include?("collaborator")
        including << :indirect if affiliation.include?("organization_member") && include_org_owned_repos

        associated_repository_ids = current_user.associated_repository_ids(including: including, include_indirect_forks: false)
        scope = Repository.where(ActiveRecord::Base.sanitize_sql(["repositories.id IN (?)", associated_repository_ids]))

        if include_org_owned_repos
          if protected_organization_ids.any?
            set_sso_partial_results_header if protected_sso_organization_ids.any?
            scope = scope.where("repositories.organization_id NOT IN (?) OR repositories.organization_id IS NULL", protected_organization_ids)
          end
        else
          scope = scope.user_owned
        end

        if current_user.using_auth_via_integration?
          if visibility == Repository::PUBLIC_VISIBILITY
            scope = scope.public_scope
          elsif visibility == "all" || visibility == Repository::PRIVATE_VISIBILITY
            private_repo_ids = scope.private_scope.pluck(:id)

            installation_accessible_repo_ids = current_integration.accessible_repository_ids(
              current_integration_installation: current_integration_installation,
              repository_ids: private_repo_ids,
            )

            scope = if visibility == "all"
              scope.where(ActiveRecord::Base.sanitize_sql(["public is true or id IN (?)", installation_accessible_repo_ids]))
            else
              scope.private_scope.where(id: installation_accessible_repo_ids)
            end
          end
        else
          if visibility == Repository::PRIVATE_VISIBILITY
            scope = scope.private_scope
          elsif visibility == Repository::PUBLIC_VISIBILITY
            scope = scope.public_scope
          end

          scope = scope.public_scope unless access_allowed?(:list_private_repos, allow_integrations: false, allow_user_via_integration: false)
        end

        filter_and_sort(scope, default_sort).pluck(:id)
      end

      repos = repo_ids.paginate(pagination)

      repo_records = Repository.where(id: repos).index_by(&:id)
      repos.replace(repos.map { |id| repo_records[id] }.compact)

      GitHub::PrefillAssociations.for_repositories(repos)
      GitHub::PrefillAssociations.fill_licenses(repos)

      deliver :repository_hash, repos, code_of_conduct: medias.api_preview?(:code_of_conduct)
    end
  end

  # List public repositories for a user.
  get "/user/:user_id/repos" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-repositories-for-a-user"
    control_access :list_repos,
                   resource: Platform::PublicResource.new,
                   user: user = find_user!,
                   enforce_oauth_app_policy: false,
                   allow_integrations: true,
                   allow_user_via_integration: true

    default_sort = nil
    type = params[:type] || "owner"

    metric_type = type
    metric_type = "other" unless %w(all member owner).include?(metric_type)

    GitHub.dogstats.time "user", tags: ["via:api", "action:repos", "type:#{metric_type}"] do
      scope = case type
        when /all/i
          Repository.where({
            id: user.associated_repository_ids(including: [:owned, :direct]),
          })
        when /member/i
          user.member_repositories
        when /owner/i
          default_sort = :name
          user.repositories
      end

      scope = if scope
        scope.public_scope
      else
        default_sort = :name
        user.public_repositories
      end

      scope = filter_and_sort(scope, default_sort)

      repos = paginate_rel(scope)

      GitHub::PrefillAssociations.for_repositories(repos)
      GitHub::PrefillAssociations.fill_licenses(repos)

      deliver :repository_hash, repos, code_of_conduct: medias.api_preview?(:code_of_conduct)
    end
  end

  # List repositories for an organization.
  get "/organizations/:organization_id/repos" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-organization-repositories"
    org = find_org!
    type = params[:type] || "all"
    user = current_user || User.new

    metric_type = type
    metric_type = "other" unless %w(all public private internal member fork source).include?(metric_type)

    control_access :apps_audited,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: false # we aren't enforcing it here because the filters underneath are.

    GitHub.dogstats.time "organization", tags: ["via:api", "action:repos", "type:#{metric_type}"] do
      repo_scope = Repository.all

      unless current_integration.present?
        unless access_allowed?(:list_private_repos, resource: org, allow_integrations: true,
                               allow_user_via_integration: true)
          repo_scope = repo_scope.public_scope
        end
      end

      repo_scope = case type
        when /public/i
          org.org_repositories.public_scope.merge(repo_scope)
        when /private/i
          if logged_in? && access_allowed?(:list_private_repos, resource: org, allow_integrations: true, allow_user_via_integration: true)
            repository_ids = if current_user.using_auth_via_integration?
              current_integration.accessible_repository_ids(
                current_integration_installation: current_integration_installation,
                repository_ids: Repository.owned_by(org).private_not_internal_scope.pluck(:id),
              )
            else
              Repository.owned_by(org).private_not_internal_scope.ids
            end

            repository_ids = user.associated_repository_ids(repository_ids: repository_ids)

            repo_scope.with_ids(repository_ids)
          else
            Repository.none
          end
        when /internal/i
          if logged_in? && org.business && current_user.is_business_member?(org.business.id)
            org.internal_repositories
          else
            Repository.none
          end

        when /member/i
          if logged_in? && access_allowed?(:list_member_repositories, resource: org, allow_integrations: true, allow_user_via_integration: true)
            repository_ids = if current_user.using_auth_via_integration?
              current_integration.accessible_repository_ids(
                current_integration_installation: current_integration_installation,
                repository_ids: Repository.owned_by(org).pluck(:id),
              )
            else
              Repository.owned_by(org).ids
            end

            repository_ids = user.associated_repository_ids(repository_ids: repository_ids)

            repo_scope.where(id: repository_ids)
          else
            Repository.none
          end

        when /fork/i
          forked_org_constrained_accessible_repositories(user, org).merge(repo_scope)
        when /source/i
          source_org_constrained_accessible_repositories(user, org).merge(repo_scope)
        else # default to type 'all'
          all_org_constrained_accessible_repositories(user, org, scope: repo_scope)
      end

      repo_scope = if repo_scope.is_a?(Array)
        sort_repo_list(repo_scope, "id")
      else
        sort_repo_scope(repo_scope, "id")
      end

      repos = paginate_rel(repo_scope)

      GitHub::PrefillAssociations.for_repositories(repos, internal: org.supports_internal_repositories?)
      GitHub::PrefillAssociations.fill_licenses(repos)

      Repository.preload_repository_permissions(repositories: repos, users: [user])
      deliver :repository_hash, repos, current_user: user, code_of_conduct: medias.api_preview?(:code_of_conduct)
    end
  end

  RepositoryQuery = PlatformClient.parse <<~'GRAPHQL'
    query($repoId: ID!){
      node(id: $repoId){
        ... on Repository {
          ...Api::Serializer::RepositoriesDependency::ExtendedRepositoryFragment
        }
      }
    }
  GRAPHQL

  # get a single repository
  get "/repositories/:repository_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-a-repository"
    @accepted_scopes = %w(repo)

    control_access :get_repo,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    GitHub::PrefillAssociations.for_repositories([repo], template: repository_templates_enabled?)
    GitHub::PrefillAssociations.fill_licenses([repo])

    options = {
      last_modified: calc_last_modified(repo),
      show_merge_settings: access_allowed?(:push, resource: repo, allow_integrations: true, allow_user_via_integration: true),
      code_of_conduct: medias.api_preview?(:code_of_conduct),
      compare_payload_to: :repository_candidate,
      generate_temp_clone_token: access_allowed?(:get_temp_clone_token, resource: repo, current_repo: repo, allow_integrations: true, allow_user_via_integration: false),
    }

    deliver :full_repository_hash, repo, options
  end

  def repository_candidate(repo, options)
    variables = { repoId: repo.global_relay_id }

    results = platform_execute(RepositoryQuery, variables: variables)
    graphql_repo = results.data.node

    Api::Serializer.serialize(:graphql_full_repository_hash, graphql_repo, options)
  end

  # create a repository for the authenticated user
  post "/user/repos" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-a-repository-for-the-authenticated-user"

    control_access :create_repo, resource: current_user, challenge: true, allow_integrations: false, allow_user_via_integration: true

    # Introducing strict validation of the repository.create-for-user
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("repository", "create-for-user", skip_validation: true, expected_type: Hash)

    # DEPRECATED: In v4, remove support for the public attribute. v4 should
    # *only* support the private attribute.
    if data.key?(Repository::PUBLIC_VISIBILITY) && data.key?(Repository::PRIVATE_VISIBILITY)
      deliver_error! 422, message: "Cannot send public and private attributes. Pick one."
    end

    attributes = attr(data,
      :name,
      :description,
      :homepage,
      :public,
      :private,
      :has_issues,
      :has_projects,
      :has_wiki,
      :has_downloads,
      :gitignore_template,
      :license_template,
      :auto_init,
    )

    # when :private is set but :public isn't, make :public the inverse of :private
    attributes[:public] = attributes.fetch(:public) { !attributes.fetch(:private, false) }
    attributes.delete(:private)

    if data.has_key?("auto_init")
      attributes["auto_init"] = parse_bool(data["auto_init"])
    end

    if data.has_key?("allow_merge_commit")
      attributes["allow_merge_commit"] = parse_bool(data["allow_merge_commit"])
    end

    if data.has_key?("allow_squash_merge")
      attributes["allow_squash_merge"] = parse_bool(data["allow_squash_merge"])
    end

    if data.has_key?("allow_rebase_merge")
      attributes["allow_rebase_merge"] = parse_bool(data["allow_rebase_merge"])
    end

    if data.has_key?("delete_branch_on_merge")
      attributes["delete_branch_on_merge"] = parse_bool(data["delete_branch_on_merge"])
    end

    set_template_attribute(data, attributes)

    authorize_content(:create)

    if creating_private_repo?(data) && !access_allowed?(:create_private_repo, resource: current_user, allow_integrations: false, allow_user_via_integration: true)
      deliver_create_private_repo_denied!
    end

    # https://github.com/github/VisualStudio/issues/62
    if block_visual_studio_from_creating_repos?(data)
      deliver_create_repo_via_visual_studio_denied!
    end

    result = Repository.handle_creation(
      current_user,
      current_user.login,
      false,
      attributes,
      new_repository_reflog_data(current_user, data, "initial commit api"),
    )

    if result.success
      install_current_integration_on_repository!(target: current_user, repository: result.repository)

      initialize_security_settings(current_user, result.repository) unless GitHub.enterprise?

      deliver :full_repository_hash, result.repository, show_merge_settings: true, status: 201
    elsif result.repository.errors[:trade_controls_restricted_owner].present?
      deliver_error 422,
        message: ::TradeControls::Notices::Plaintext.api_access_restricted,
        documentation_url: GitHub.trade_controls_help_url
    elsif result.repository.errors[:trade_controls_restricted_creator].present?
      deliver_error 422,
        message: ::TradeControls::Notices::Plaintext.api_access_restricted,
        documentation_url: GitHub.trade_controls_help_url
    elsif result.repository.errors[:visibility].present?
      deliver_error 422,
        message: "Please upgrade your subscription to create a new private repository.",
        documentation_url: "https://github.com/pricing"
    else
      deliver_error 422,
        message: result.error_message,
        errors: result.repository.errors,
        documentation_url: @documentation_url
    end
  end

  # create a repository for :org
  post "/organizations/:organization_id/repos" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-an-organization-repository"
    set_forbidden_message("You need admin access to the organization before adding a repository to it.")

    org = find_org!
    control_access :create_repo_for_org,
      resource: org,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    # Introducing strict validation of the repository.create-for-organization
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("repository", "create-for-organization", skip_validation: true)

    # DEPRECATED: In v4, remove support for the public attribute. v4 should
    # *only* support the private attribute.
    if data.key?(Repository::PUBLIC_VISIBILITY) && data.key?(Repository::PRIVATE_VISIBILITY)
      deliver_error! 422, message: "Cannot send public and private attributes. Pick one."
    end

    attributes = attr(data,
      :name,
      :description,
      :homepage,
      :public,
      :private,
      :has_issues,
      :has_projects,
      :has_wiki,
      :has_downloads,
      :gitignore_template,
      :license_template,
      :auto_init,
    )

    # when :private is set but :public isn't, make :public the inverse of :private
    attributes[:public] = attributes.fetch(:public) { !attributes.fetch(:private, false) }
    attributes.delete(:private)

    # If specified, visibility overrides public/private.
    if internal_visibility_enabled? && data.has_key?("visibility")
      attributes[:visibility] = data["visibility"]
      attributes.delete(:public)
      attributes.delete(:private)
    end

    if data.has_key?("auto_init")
      attributes["auto_init"] = parse_bool(data["auto_init"])
    end

    if data.has_key?("allow_merge_commit")
      attributes["allow_merge_commit"] = parse_bool(data["allow_merge_commit"])
    end

    if data.has_key?("allow_squash_merge")
      attributes["allow_squash_merge"] = parse_bool(data["allow_squash_merge"])
    end

    if data.has_key?("allow_rebase_merge")
      attributes["allow_rebase_merge"] = parse_bool(data["allow_rebase_merge"])
    end

    if data.has_key?("delete_branch_on_merge")
      attributes["delete_branch_on_merge"] = parse_bool(data["delete_branch_on_merge"])
    end

    set_template_attribute(data, attributes)

    if team_id = data["team_id"]
      team = org.teams.find_by_id(team_id)

      if team && team.can_add_repositories?(current_user, allow_owners_team: true)
        attributes[:team_id] = team_id
      else
        deliver_error! 422,
          message: "You need admin access to the team before adding a repository to it.",
          documentation_url: @documentation_url
      end
    end

    if creating_private_repo?(attributes) &&
      !access_allowed?(:create_private_repo_for_org, resource: org, allow_integrations: true, allow_user_via_integration: true)

      deliver_create_private_repo_denied!
    end

    # https://github.com/github/VisualStudio/issues/62
    if block_visual_studio_from_creating_repos?(data)
      deliver_create_repo_via_visual_studio_denied!
    end

    result = Repository.handle_creation(
      current_user,
      org.login,
      false,
      attributes,
      new_repository_reflog_data(org, data, "initial commit api"),
    )

    if result.success
      install_current_integration_on_repository!(target: org, repository: result.repository)

      initialize_security_settings(org, result.repository) unless GitHub.enterprise?

      deliver :full_repository_hash, result.repository, show_merge_settings: true, status: 201
    elsif !result.allowed
      deliver_error 403, message: forbidden_message
    elsif result.repository.errors[:trade_controls_restricted_owner].present?
      deliver_error 422,
        message: ::TradeControls::Notices::Plaintext.api_access_restricted,
        documentation_url: GitHub.trade_controls_help_url
    elsif result.repository.errors[:trade_controls_restricted_creator].present?
      deliver_error 422,
        message: ::TradeControls::Notices::Plaintext.api_access_restricted,
        documentation_url: GitHub.trade_controls_help_url
    elsif result.repository.errors[:visibility].present?
      deliver_error 422,
        message: result.repository.errors.full_messages.to_sentence,
        documentation_url: "https://github.com/pricing"
    else
      deliver_error 422,
        message: result.error_message,
        errors: result.repository.errors,
        documentation_url: @documentation_url
    end
  end

  CloneTemplateQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: CloneTemplateRepositoryInput!) {
      cloneTemplateRepository(input: $input) {
        repository {
          ...Api::Serializer::RepositoriesDependency::ExtendedRepositoryFragment
        }
      }
    }
  GRAPHQL

  # Clone a template repository to start a new repository
  post "/repositories/:template_repository_id/generate" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-a-repository-using-a-template"
    require_preview(:repository_templates)

    repo = find_template_repo!

    control_access :get_repo,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    data = receive_with_schema("repository", "generate-from-template")
    attributes = attr(data, :owner, :name, :description, :private, :include_all_branches)

    owner = current_user
    owner = User.find_by_login(attributes[:owner]) if attributes[:owner]

    unless owner
      deliver_error! 404, message: "Invalid owner", documentation_url: @documentation_url
    end

    if owner.bot?
      deliver_error! 422, message: "Invalid owner. Owner must be a user or organization.", documentation_url: @documentation_url
    elsif owner.user?
      control_access :create_repo, resource: owner, challenge: true, allow_integrations: false,
        allow_user_via_integration: true

      if creating_private_repo?(data) &&
         !access_allowed?(:create_private_repo, resource: owner, allow_integrations: false,
                          allow_user_via_integration: true)
        deliver_create_private_repo_denied!
      end

      authorize_content(:create)
    elsif owner.organization?
      set_forbidden_message("You need admin access to the organization before adding a " \
                            "repository to it.")

      control_access :create_repo_for_org, resource: owner, challenge: true,
        allow_integrations: true, allow_user_via_integration: true, organization: owner

      if creating_private_repo?(attributes) &&
         !access_allowed?(:create_private_repo_for_org, resource: owner, allow_integrations: true,
                          allow_user_via_integration: true)
        deliver_create_private_repo_denied!
      end

      is_blocked_app = requestor_governed_by_oauth_application_policy? &&
        !owner.allows_oauth_application?(current_app_via_oauth)
      if is_blocked_app
        error_options = {
          message: "The OAuth application is not authorized for access to #{owner}.",
          documentation_url: @documentation_url,
        }
        deliver_error!(403, error_options)
      end
    else
      deliver_error! 404, message: "Invalid owner", documentation_url: @documentation_url
    end

    results = platform_execute(CloneTemplateQuery, variables: {
      input: {
        repositoryId: repo.global_relay_id,
        name: attributes[:name],
        description: attributes[:description],
        ownerId: owner.global_relay_id,
        visibility: attributes[:private] ? Repository::PRIVATE_VISIBILITY.upcase : Repository::PUBLIC_VISIBILITY.upcase,
        includeAllBranches: attributes[:include_all_branches],
      },
    })

    if results.errors.all.any?
      deliver_graphql_error!(results, resource: "Repository")
    else
      deliver :graphql_full_repository_hash,
        results.data.clone_template_repository.repository, status: 201
    end
  end

  TransferRepositoryQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($repositoryId: ID!, $newOwnerId: ID!, $teamIds: [ID!]) {
      transferRepository(input: {repositoryId: $repositoryId, newOwnerId: $newOwnerId, teamIds: $teamIds}) {
        repository {
          ...Api::Serializer::RepositoriesDependency::SimpleRepositoryFragment
        }
      }
    }
  GRAPHQL

  # Transfer a repository
  post "/repositories/:repository_id/transfer" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#transfer-a-repository"

    repository = find_repo!

    attributes = attr(receive_with_schema("repository-transfer", "transfer-legacy"), :new_owner, :team_ids)

    new_owner = User.find_by(login: attributes[:new_owner])
    unless new_owner
      deliver_error! 422, message: "Invalid new_owner", documentation_url: @documentation_url
    end

    control_access :transfer_repo, resource: repository, forbid: repository.public?, new_owner: new_owner, challenge: true, allow_integrations: false, allow_user_via_integration: true

    teams = new_owner.teams.where(id: attributes[:team_ids])

    results = platform_execute(TransferRepositoryQuery, variables: {
      repositoryId: repository.global_relay_id,
      newOwnerId: new_owner.global_relay_id,
      teamIds: teams.map(&:global_relay_id),
    })

    if results.errors.all.any?
      deprecated_deliver_graphql_error(
        errors: results.errors.all,
        resource: "Repository",
        documentation_url: @documentation_url,
      )
    else
      deliver :graphql_simple_repository_hash, results.data.transfer_repository.repository, status: 202
    end
  end

  # edit a repository
  verbs :patch, :post, "/repositories/:repository_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#update-a-repository"
    control_access :edit_repo, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    authorize_content(:update, repo: repo)

    data = receive(Hash)
    attributes = attr(data,
                   :description,
                   :homepage,
                   :has_issues,
                   :has_projects,
                   :has_wiki,
                   :has_downloads)

    set_template_attribute(data, attributes)

    begin
      attributes.each do |key, value|
        repo.send "#{key}=", value
      end
    rescue Repository::CannotEnableProjectsError
      deliver_error! 422,
        errors: [
          api_error(:Repository, :has_projects, :invalid,
            message: "This repository's organization has repository projects disabled, so projects cannot be enabled for this repository."),
        ],
        documentation_url: @documentation_url
    end

    saved = if (data.has_key?("name") && repo.name != data["name"])
      repo.rename data["name"]
    elsif attributes.present?
      repo.save
    else
      true
    end

    if internal_visibility_enabled? && data.keys.include?("visibility")
      data.delete(Repository::PRIVATE_VISIBILITY) # The presence of a visibility arg overrides private.
      begin
        case data["visibility"]
        # The fork-related visibility logic below should really be in the model
        # but for now, we set data["private"] here to preserve it.
        when Repository::PRIVATE_VISIBILITY
          data[Repository::PRIVATE_VISIBILITY] = true
          saved = repo.set_visibility(actor: current_user, visibility: Repository::PRIVATE_VISIBILITY)
        when Repository::PUBLIC_VISIBILITY
          data[Repository::PRIVATE_VISIBILITY] = false
        when Repository::INTERNAL_VISIBILITY
          saved = repo.set_visibility(actor: current_user, visibility: Repository::INTERNAL_VISIBILITY)
        end
      rescue ArgumentError => e
        deliver_error! 422, message: e.message, documentation_url: @documentation_url
      end
    end

    if data.keys.include?(Repository::PRIVATE_VISIBILITY)
      # If private is true and it's public OR if private is false and it's already private
      if (parse_bool(data[Repository::PRIVATE_VISIBILITY]) && repo.public?) || (!parse_bool(data[Repository::PRIVATE_VISIBILITY]) && !repo.public?)
        if repo.fork? && repo.matches_root_visibility?
          current, attempted = repo.public? ? [Repository::PUBLIC_VISIBILITY, Repository::PRIVATE_VISIBILITY] : [Repository::PRIVATE_VISIBILITY, Repository::PUBLIC_VISIBILITY]
          repo.errors.add(:base, "#{current.capitalize} forks can't be made #{attempted}")
          deliver_error! 422,
            errors: repo.errors,
            documentation_url: @documentation_url
        else
          # Attempt to toggle visibility of this repository. Authz checking is
          # performed by the model based on the current user.
          saved = repo.toggle_visibility(actor: current_user)
        end
      end
    end

    if data.keys.include?("archived")
      if !repo.resources.administration.writable_by?(current_user)
        deliver_error! 422, message: "You cannot change repository archived state."
      elsif (parse_bool(data["archived"]) && !repo.archived?)
        saved = repo.set_archived
      end
    end

    # update the merge/squash blocking option
    allow_merge_commit = allow_rebase_merge = nil
    if data.has_key?("allow_merge_commit")
      allow_merge_commit = parse_bool(data["allow_merge_commit"])
    end
    if data.has_key?("allow_squash_merge")
      allow_squash_merge = parse_bool(data["allow_squash_merge"])
    end
    if data.has_key?("allow_rebase_merge")
      allow_rebase_merge = parse_bool(data["allow_rebase_merge"])
    end
    if data.has_key?("delete_branch_on_merge")
      delete_branch_allowed = parse_bool(data["delete_branch_on_merge"])
    end

    begin
      repo.set_allowed_merge_types(current_user,
        merge_allowed: allow_merge_commit,
        squash_allowed: allow_squash_merge,
        rebase_allowed: allow_rebase_merge,
        delete_branch_allowed: delete_branch_allowed,
      )
    rescue Repository::MergeMethodError => e
      deliver_error! 422,
        errors: [
          api_error(:Repository, :merge_commit_allowed, :invalid,
            message: "#{e.message} (#{e.reason})"),
        ],
        documentation_url: @documentation_url
    end

    # have to go through a model method to update git
    if new_branch = data["default_branch"]
      if !(saved &&= repo.update_default_branch(new_branch))
        # TODO: Push this to the model, along with similar logic in GitContentController
        if !repo.empty? && new_branch == repo.default_branch
          return deliver :full_repository_hash, repo, show_merge_settings: true
        end
        message = if repo.empty?
          "Cannot update default branch for an empty repository. " \
          "Please init the repository and push first."
        else
          "The branch #{new_branch} was not found. " \
          "Please push that ref first or create it via the Git Data API."
        end
        deliver_error! 422,
          errors: [
            api_error(:Repository, :default_branch, :invalid, message: message),
          ],
          documentation_url: @documentation_url
      end
    end

    # set anonymous git access if provided
    if data.keys.include?("anonymous_access_enabled")
      unless GitHub.anonymous_git_access_enabled?
        repo.errors.add(:base, "Repository anonymous access is not enabled")
        saved = false
      end

      saved &&= begin
        value = data["anonymous_access_enabled"]&.to_s
        error = nil
        if repo.fork?
          error = Repository::AnonymousGitAccess::FORK_ERROR
        elsif repo.anonymous_git_access_locked?(current_user)
          error = Repository::AnonymousGitAccess::LOCKED_ERROR
        elsif value == "true"
          repo.enable_anonymous_git_access(current_user)
        elsif value == "false"
          repo.disable_anonymous_git_access(current_user)
        else
          error = "Invalid value \"#{value}\" given for anonymous_access_enabled"
        end

        repo.errors.add(:base, error) if error
        error.nil?
      end
    end

    if saved
      # Introducing strict validation of the repository.update
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # TODO: replace `receive` with `receive_with_schema`
      # see: https://github.com/github/ecosystem-api/issues/1555
      _ = receive_with_schema("repository", "update", skip_validation: true)

      deliver :full_repository_hash, repo, show_merge_settings: true
    elsif repo.errors[:visibility].present?
      deliver_error 422,
        message: repo.errors.full_messages.to_sentence,
        documentation_url: "https://github.com/pricing"
    else
      deliver_error 422,
        errors: repo.errors,
        documentation_url: @documentation_url
    end
  end

  # delete a repository
  delete "/repositories/:repository_id" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the repository.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("repository", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-a-repository"
    repo = find_repo!

    control_access :delete_repo,
      resource: repo,
      # Respond with 403 when the user can access this repo but doesn't have the delete_repo scope
      forbid: forbids_when_deleting_repo?(repo, current_user),
      allow_integrations: true,
      allow_user_via_integration: true

    error = repo.cannot_delete_repository_reason(current_user)
    case error
    when :ofac_trade_restricted
      deliver_error! 403, message: ::TradeControls::Notices::Plaintext.api_access_restricted
    when :cant_delete_repos_on_this_appliance
      deliver_error! 403, message: "Users cannot delete repositories on this appliance."
    when :members_cant_delete_repositories
      deliver_error! 403, message: "Organization members cannot delete repositories."
    when :not_ready_for_writes
      deliver_error! 403, message: "Repository cannot be deleted until it is done being created on disk."
    end

    repo.remove(current_user)

    deliver_empty(status: 204)
  end

  # Get contributors for a repository
  get "/repositories/:repository_id/contributors" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-repository-contributors"
    control_access :list_contributors, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    if !repo.default_branch_exists?
      deliver_empty status: 204
    else
      GitHub.dogstats.time "repository", tags: ["action:contributors", "via:api"] do
        with_anon = parse_bool(params[:anon])

        response = repo.contributors(with_anon: with_anon, email_limit: 500)
        unless response.computed?
          deliver_error! 403,
            message: "The history or contributor list is too large to " \
                        "list contributors for this repository via the API."
        end

        contributors = response.value.paginate(pagination)
        deliver :contributor_hash, contributors, last_modified: calc_last_modified(repo)
      end
    end
  end

  # List teams for a repository.
  get "/repositories/:repository_id/teams" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-repository-teams"
    control_access :list_repo_teams, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    teams = repo.teams
    teams = paginate_rel(teams)

    GitHub::PrefillAssociations.for_teams(teams)
    deliver :team_hash, teams, repo: repo
  end

  # list languages for a repository
  get "/repositories/:repository_id/languages" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-repository-languages"
    control_access :list_languages,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    deliver_raw repo.language_breakdown, last_modified: calc_last_modified(repo)
  end

  # list tags for a repository
  get "/repositories/:repository_id/tags" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-repository-tags"
    control_access :list_tags,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    tags = repo.tags.to_a
    Git::Ref::Collection.preload_target_objects(tags)

    tags_json = repo.tags.map do |ref|
      next if !ref.commit? # exclude refs that don't resolve to commits

      {
        name: ref.name,
        zipball_url: zipball_path(repo, ref.name),
        tarball_url: tarball_path(repo, ref.name),
        commit: {
          sha: ref.commit.oid,
          url: commit_path(repo, ref.commit.oid),
        },
        node_id: ref.global_relay_id,
      }
    end.compact

    tags_json = tags_json.paginate(pagination)

    deliver_raw tags_json, last_modified: calc_last_modified(repo)
  end

  # list forks for a repository
  get "/repositories/:repository_id/forks" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-forks"
    control_access :list_forks, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    order = case params[:sort]
    when "newest"
      "repositories.created_at desc"
    when "oldest"
      "repositories.created_at"
    when "stargazers", "watchers" # TODO Remove `watchers` in API v4
      "#{Repository.stargazer_count_column} DESC"
    when String
      deliver_error! 400,
      message: "Invalid sort: #{params[:sort].inspect}.",
      documentation_url: @documentation_url
    else
      "repositories.created_at desc"
    end

    # Fallback to public forks only
    scope = if logged_in? && repo.private?
      private_repository_ids = if current_user.using_auth_via_integration?
        current_integration.accessible_repository_ids(
          current_integration_installation: current_integration_installation,
          repository_ids: repo.forks.private_scope.pluck(:id),
          permissions: [:metadata],
        )
      else
        repo.forks.private_scope.ids
      end

      private_repository_ids = current_user.associated_repository_ids(repository_ids: private_repository_ids)

      if private_repository_ids.any?
        repo.forks.public_scope.or(repo.forks.private_scope.where(id: private_repository_ids))
      else
        repo.forks.public_scope
      end
    else
      repo.forks.public_scope
    end

    forks = paginate_rel(scope.order(order))

    GitHub::PrefillAssociations.for_repositories(forks)
    GitHub::PrefillAssociations.fill_licenses(forks)

    deliver :repository_hash, forks
  end

  # create a fork
  post "/repositories/:repository_id/forks" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-a-fork"
    repo = find_repo!

    if repo.access.disabled?
      deliver_error! 404
    end

    # Introducing strict validation of the fork.create
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("fork", "create", skip_validation: true, expected_type: Hash, required: false) || {}

    # DEPRECATED: In API v4, you will no longer be able to specify the
    # organization as a query parameter. You will only be able to specify it in
    # the JSON request body.
    org_name = (params[:org] || params[:organization] || data["organization"]).to_s.dup
    org_name.strip!
    using_org = org_name.present?

    org = User.find_by(login: org_name) if using_org

    if using_org && org.nil?
      error_options = {
        errors: convert_error({organization: "is invalid"}, :Fork),
        documentation_url: @documentation_url,
      }
      deliver_error!(422, error_options)
    end

    if using_org && org.user?
      error_options = {
        errors: convert_error({organization: "is invalid"}, :Fork),
        documentation_url: @documentation_url,
        message: "'#{org_name}' is the login for a user account. You must pass the login for an organization account.",
      }
      deliver_error!(422, error_options)
    end

    is_blocked_app =
      using_org &&
      requestor_governed_by_oauth_application_policy? &&
      !org.allows_oauth_application?(current_app_via_oauth)
    if is_blocked_app
      # TODO: this is the message that currently gets set if an app is blocked.
      # Perhaps we should reconsider.
      error_options = {
        message: "Must have admin rights to Repository.",
        documentation_url: @documentation_url,
      }
      deliver_error!(403, error_options)
    end

    authorize_content(:fork, repo: repo) unless repo.private?
    control_access :create_fork,
      resource: repo,
      organization: org,
      enforce_oauth_app_policy: repo.private?,
      # For historical reasons, if you are forking and you can see the repo,
      # but you have insufficient scopes, then...
      # ... when forking to a user account, you get a 404
      # ... when forking to an organization, you get a 403
      forbid: org && repo.readable_by?(current_user),
      allow_integrations: true,
      allow_user_via_integration: true
    authorize_content(:fork, repo: repo) if repo.private?

    if repo.empty?
      deliver_error! 403,
        message: "The repository exists, but it contains no Git content. " \
                    "Empty repositories cannot be forked.",
        documentation_url: @documentation_url
    end

    if repo.forking_disabled?
      deliver_error!(403, {
        message: "The repository exists, but forking is disabled.",
        documentation_url: @documentation_url,
      })
    end

    if using_org && repo.internal?
      error_options = {
        message: "Cannot fork an internal repo to an organization.",
        documentation_url: @documentation_url,
      }
      deliver_error!(403, error_options)
    end

    options = {
      forker: current_user,
      org: org,
    }
    # If the actor is a GitHub App, then by default
    # it will fork the repository to the [bot]
    # account. Instead, we wwant to fork to the account or
    # organization that the App is installed on.
    if current_actor.is_a?(IntegrationInstallation)
      target = current_actor.target
      if target.user?
        options[:forker] = target
      else
        options[:org] = target
      end
    end

    forked_repo, reason, errors = repo.fork(options)

    if !forked_repo
      message = RepositoryForker.message_from_reason(reason, errors)
      deliver_error!(403, message: message, documentation_url: @documentation_url) if message
    end

    deliver :full_repository_hash, forked_repo, status: 202
  end

  # Compare commits
  get "/repositories/:repository_id/compare/*" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#compare-two-commits"
    repo = find_repo!
    control_access :compare_commits,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    base, head = params[:splat].first.split("...", 2)
    comparison = repo.comparison(base, head, 250)

    unless comparison.valid? && !comparison.diffs.missing_commits? && comparison.viewable_by?(current_user)
      deliver_error! 404
    end

    unless comparison.common_ancestor?
      deliver_error! 404,
        message: "No common ancestor between #{base} and #{head}."
    end

    if !comparison.diffs.available? || comparison.diffs.truncated_for_timeout?
      deliver_undiffable_error!(:Comparison, comparison.diffs)
    end

    if medias.api_param?(:diff)
      deliver_raw comparison.to_diff,
        content_type: "#{medias}; charset=utf-8",
        last_modified: calc_last_modified(comparison)
    elsif medias.api_param?(:patch)
      deliver_raw comparison.to_patch,
        content_type: "#{medias}; charset=utf-8",
        last_modified: calc_last_modified(comparison)
    else
      deliver :github_comparison_hash, comparison,
        repo: repo,
        last_modified: calc_last_modified(comparison)
    end
  end

  get "/repositories" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-public-repositories"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    since = params[:since] ? int_id_param!(key: :since, halt: true) : 0
    options = { cursor: since }

    if GitHub.flipper[:get_repositories_custom_page_size].enabled?(current_user)
      options[:page_size] = params[:per_page].to_i
    end

    repos = if include_private_repos?
      Repository.dump_all(options)
    else
      if protected_organization_ids.any?
        set_sso_partial_results_header if protected_sso_organization_ids.any?
        options[:protected_organization_ids] = protected_organization_ids
      end
      Repository.dump_public(options)
    end
    @links.add_dump_pagination(repos.last)

    include_timestamps = GitHub.flipper[:simple_repository_hash_with_timestamps].enabled?(current_user)

    deliver :simple_repository_hash,
      serializable_repos_with_prefilled_associations(repos),
      include_timestamps: include_timestamps
  end

  get "/repositories/:repository_id/mentionables/users" do
    @route_owner = "@github/pe-repos"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:mentionables)

    control_access :get_repo, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    mentionables = repo.mentionable_users_for(current_user, include_child_teams: false)
    GitHub::PrefillAssociations.for_profiles(mentionables)

    deliver :mentionable_user_hash, mentionables
  end

private
  def serializable_repos_with_prefilled_associations(repos)
    begin
      GitHub::PrefillAssociations.for_repositories(repos)
    rescue
      repos.select { |repo| serializable?(repo) }
    end
  end

  def serializable?(repo)
    begin
      Api::Serializer.simple_repository_hash(repo)
      true
    rescue StandardError => boom
      info = "Serialization fails for repository #{repo.id}. \
        Review stack trace or check ActiveRecord validations to determine root cause."

      failbot(env, exception: boom, info: info, fatal: "NO")
      false
    end
  end

  def creating_private_repo?(data)
    data[Repository::PUBLIC_VISIBILITY] == false || data[Repository::PRIVATE_VISIBILITY] == true
  end

  def zipball_path(repo, name)
    "#{GitHub.api_url}/repos/#{repo.name_with_owner}/zipball/#{name}"
  end

  def tarball_path(repo, name)
    "#{GitHub.api_url}/repos/#{repo.name_with_owner}/tarball/#{name}"
  end

  def commit_path(repo, sha)
    "#{GitHub.api_url}/repos/#{repo.name_with_owner}/commits/#{sha}"
  end

  # Return a list of repositories for the given org, accessible by the given
  # user, including:
  #
  # - public repositories which are owned by the org
  # - repositories owned by the org, when user is a member of org's Owners team
  # - repositories owned by the org, when user is a member of an org team which
  #   grants access to the repository
  #
  # user - a User
  # org  - an Organization
  # scope - an ActiveRecord Repository Relation
  #
  # Returns a Repository scope or an Array of Repositories.
  def all_org_constrained_accessible_repositories(user, org, scope:)
    scope = org.all_org_repos_for_user(user, scope: scope)

    if user.using_auth_via_integration?
      accessible_repository_ids = current_integration.accessible_repository_ids(
        current_integration_installation: current_integration_installation,
        repository_ids: scope.pluck(:id),
      )

      scope = Repository
        .where(owner: org)
        .where("repositories.public = ? OR repositories.id IN (?)", true, accessible_repository_ids)
    end

    scope
  end

  # Public: Return a list of root (non-fork) repositories for the given org,
  # accessible by the given user, including:
  #
  # - public repositories which are owned by the org
  # - repositories owned by the org, when user is a member of org's Owners team
  # - repositories owned by the org, when user is a member of an org team which
  #   grants access to the repository
  #
  # user - a User
  # org  - an Organization
  #
  # Returns a Repository scope.
  def source_org_constrained_accessible_repositories(user, org)
    all_public_org_root_repo_ids = org.org_repositories.public_scope.network_roots.pluck(:id)

    repo_ids_with_team_membership = user.associated_repository_ids(including: [:direct, :indirect], repository_ids: Repository.owned_by(org).network_roots.ids)

    if user.using_auth_via_integration?
      repo_ids_with_team_membership = current_integration.accessible_repository_ids(
        current_integration_installation: current_integration_installation,
        repository_ids: repo_ids_with_team_membership,
      )
    end

    ids = repo_ids_with_team_membership | all_public_org_root_repo_ids

    Repository.where(id: ids)
  end

  # Public: Return a list of fork repositories for the given org, accessible by
  # the given user, including:
  #
  # - public repositories which are owned by the org
  # - repositories owned by the org, when user is a member of org's Owners team
  # - repositories owned by the org, when user is a member of an org team which
  #    grants access to the repository
  #
  # user - a User
  # org  - an Organization
  #
  # Returns a Repository scope.
  def forked_org_constrained_accessible_repositories(user, org)
    all_public_org_fork_repo_ids = org.org_repositories.public_scope.forks_owned_by(org).pluck(:id)

    repo_ids_with_team_membership = user.associated_repository_ids(including: [:direct, :indirect], repository_ids: Repository.forks_owned_by(org).ids)

    if user.using_auth_via_integration?
      repo_ids_with_team_membership = current_integration.accessible_repository_ids(
        current_integration_installation: current_integration_installation,
        repository_ids: repo_ids_with_team_membership,
      )
    end

    ids = repo_ids_with_team_membership | all_public_org_fork_repo_ids

    Repository.where(id: ids)
  end

  def set_repo_direction_and_sort(default_sort)
    params[:direction] = nil if params[:direction].blank?
    params[:sort] = default_sort if params[:sort].blank?
  end

  def sort_repo_scope(scope, default_sort = nil)
    set_repo_direction_and_sort(default_sort)
    scope.sorted_by(params[:sort], params[:direction])
  end

  def sort_repo_list(list, default_sort = nil)
    set_repo_direction_and_sort(default_sort)
    default_direction = "desc"

    list = case params[:sort]
    when "created"
      list.sort_by { |repo| repo.created_at.to_s }
    when "updated"
      list.sort_by { |repo| repo.updated_at.to_s }
    when "pushed"
      list.sort_by { |repo| repo.pushed_at.to_s }
    when "full_name"
      default_direction = "asc"
      GitHub::PrefillAssociations.prefill_repository_owners(list)
      list.sort_by { |repo| [repo.owner&.login, repo.name] }
    else
      default_direction = "asc"
      list.sort_by(&:id)
    end

    params[:direction] ||= default_direction
    list = list.reverse if params[:direction].downcase == "desc"
    list
  end

  def filter_and_sort(scope, default_sort = nil)
    # specify sort
    scope = sort_repo_scope(scope, default_sort)

    # filter by updated_at
    if (since = time_param!(:since)).present?
      scope = scope.since(since.getlocal)
    end

    if (before = time_param!(:before)).present?
      scope = scope.before(before.getlocal)
    end

    scope.filter_spam_for(current_user)
  end

  def deliver_create_private_repo_denied!
    message = "You'll need a different OAuth scope to create a private repository. " \
              "Please see the documentation for full details."
    deliver_error! 403,
      message: message,
      documentation_url: "/rest/reference/repos#create-a-repository-for-the-authenticated-user"
  end

  def block_visual_studio_from_creating_repos?(data)
    return false unless user_agent.github_visual_studio?
    return false if creating_private_repo?(data)

    # TODO implement Comparable in Api::UserAgent
    return true if user_agent.github_app_version.major < 1
    return false if user_agent.github_app_version.minor > 0

    user_agent.github_app_version.patch < 14
  end

  def deliver_create_repo_via_visual_studio_denied!
    message = "Versions of GitHub Extension for Visual Studio " \
              "prior to 1.0.14 are not allowed to create new " \
              "public repositories. Please upgrade your extension " \
              "in Tools - Extensions and Updates"
    deliver_error! 403,
      message: message,
      documentation_url: "/rest/reference/repos#create-a-repository-for-the-authenticated-user"
  end

  def include_private_repos?
    GitHub.enterprise? && access_allowed?(:list_all_repos, allow_integrations: false, allow_user_via_integration: false) && params["visibility"] == "all"
  end

  def authorize_content(operation = :create, data = {})
    authorization = ContentAuthorizer.authorize(current_user, :repo, operation, data)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end

  def forbids_when_deleting_repo?(repo, user)
    return true if repo.public?
    return true if access_allowed?(:get_repo,
                                   resource: repo,
                                   user: user,
                                   allow_integrations: true,
                                   allow_user_via_integration: true)

    return false if current_integration || current_integration_installation

    repo.pullable_by?(user) && scope?(user, "delete_repo")
  end

  def new_repository_reflog_data(creator, data, via)
    {
      real_ip: remote_ip,
      repo_name: "#{creator.login}/#{data[:name]}",
      repo_public: data[:public],
      user_login: current_user.login,
      user_agent: request.user_agent,
      from: GitHub.context[:from],
      via: via,
    }
  end

  def repository_templates_enabled?
    medias.api_version?(REST::Previews.get(:repository_templates).media_version)
  end

  def internal_visibility_enabled?
    medias.api_version?(REST::Previews.get(:repo_internal_visibility).media_version)
  end

  def set_template_attribute(data, attributes)
    if data.has_key?("is_template") && repository_templates_enabled?
      attributes["template"] = parse_bool(data["is_template"])
    end
  end

  # Internal: installs the GitHub App making this API request onto the given
  # target and repository.
  #
  # Returns nothing.
  def install_current_integration_on_repository!(target:, repository:)
    return unless integration_bot_request? || integration_user_request?

    installation, installer =
      if integration_bot_request?
        [current_user.installation, current_user.installation]
      elsif integration_user_request?
        [current_integration.installations_on(target).first, current_user]
      end

    if installation.installed_on_selected_repositories?
      current_integration.install_on(
        target,
        repositories: [repository],
        installer: installer,
        version: installation.version,
      )
    end
  end
end
