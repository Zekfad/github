# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationTeams < Api::App
  areas_of_responsibility :orgs, :api

  EXTERNAL_MANAGEMENT_RESTRICTION_ERROR = [
    "This team's membership is managed exclusively by %s. Learn more at",
    "#{GitHub.help_url}/articles/synchronizing-teams-between-your-identity-provider-and-github",
  ].join(" ")

  # list teams in :org
  get "/organizations/:organization_id/teams" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#list-teams"
    control_access :list_teams,
      resource: org = find_org!,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true

    teams = if can_list_all_teams?
      org.teams
    else
      org.visible_teams_for(current_user)
    end
    teams = paginate_rel(teams)

    deliver :team_hash, teams
  end

  # list the authenticated user's teams across all orgs
  get "/user/teams" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#list-teams-for-the-authenticated-user"
    control_access :list_all_user_teams,
      resource: current_user,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    scope = current_user.teams

    if current_user.using_auth_via_integration?
      orgs = current_user.organizations
      scope = Team.none # set the scope to `none` as a failsafe instead of a bunch of else's

      if orgs.any?
        installation_ids = Authorization.service.actor_ids_with_granular_permissions_on(
          actor_type:   "IntegrationInstallation",
          subject_type: Organization,
          subject_ids:  orgs.pluck(:id),
          permissions:  [:members],
        )

        orgs_with_installation_access = current_integration.installations.where(id: installation_ids).pluck(:target_id)

        orgs  = orgs.where(id: orgs_with_installation_access)
        scope = current_user.teams.owned_by(orgs)
      end
    elsif requestor_governed_by_oauth_application_policy?
      orgs = current_user.organizations
      orgs = orgs.oauth_app_policy_met_by(current_app_via_oauth)
      scope = scope.owned_by(orgs)
    end

    if protected_organization_ids.any?
      set_sso_partial_results_header if protected_sso_organization_ids.any?
      scope = scope.excluding_organization_ids(protected_organization_ids)
    end

    teams = paginate_rel(scope)

    GitHub::PrefillAssociations.for_teams(teams)

    deliver :team_hash, teams, full: true
  end

  CreateTeamQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($organization_id: ID!, $name: String!, $description: String, $privacy: TeamPrivacy!, $permission: LegacyTeamPermission, $repositories: [String], $maintainers: [String], $parentTeamId: ID, $ldap_dn: String, $includeFullTeamDetails: Boolean!) {
      createTeam(input: { organizationId: $organization_id, name: $name, description: $description, privacy: $privacy, permission: $permission, repositories: $repositories, maintainers: $maintainers, parentTeamId: $parentTeamId, ldap_dn: $ldap_dn })  {
        team {
          ...Api::Serializer::OrganizationsDependency::TeamFragment
        }
      }
    }
  GRAPHQL

  # create a team in :org
  post "/organizations/:organization_id/teams" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#create-a-team"
    control_access :create_team,
      resource: org = find_org!,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true

    data  = receive_with_schema("team-full", "create-legacy")
    # DEPRECATED: :permission will be removed in API v4.
    attrs = attr(data, :name, :description, :permission, :privacy, :ldap_dn, :parent_team_id)

    if attrs[:parent_team_id].present?
      parent_team = org.teams.find_by_id(attrs[:parent_team_id])
      has_get_team_access = access_allowed? :get_team,
        team: parent_team,
        organization: parent_team.try(:organization),
        resource: parent_team.try(:organization),
        allow_user_via_integration: true,
        allow_integrations: true

      if parent_team.nil?
        set_forbidden_message "Invalid parent team id"
      elsif has_get_team_access
        set_forbidden_message "You must be a maintainer of the parent team"
      end

      control_access :admin_team,
        resource: parent_team.try(:organization),
        organization: parent_team.try(:organization),
        team: parent_team,
        allow_integrations: true,
        allow_user_via_integration: true
    end

    variables = {}
    variables[:includeFullTeamDetails] = true
    variables[:organization_id] = org.global_relay_id
    variables[:name] = attrs[:name]
    variables[:description] = attrs[:description] if attrs[:description]
    variables[:permission] = attrs[:permission].upcase if attrs[:permission]
    variables[:repositories] = data["repo_names"] if data["repo_names"]
    variables[:maintainers] = data["maintainers"] if data["maintainers"]
    variables[:parentTeamId] = parent_team.global_relay_id if parent_team
    variables[:privacy] = if Team.valid_privacy?(attrs[:privacy])
    variables[:ldap_dn] = attrs[:ldap_dn] if attrs[:ldap_dn] && GitHub.ldap_sync_enabled?
      Platform::Enums::TeamPrivacy.coerce_isolated_result(attrs[:privacy].to_s)
    elsif parent_team
      "VISIBLE"
    else
      "SECRET"
    end

    results = platform_execute(CreateTeamQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
        })
    else
      if attrs[:permission].present?
        GitHub.dogstats.increment("api.team_with_permission", {tags: ["permission:#{attrs[:permission]}", "action:create"]})
      end

      deliver :graphql_team_hash, results.data.create_team.team, full: true, status: 201
    end
  end

  # get a team by id, by slug is automatically mapped
  get "/organizations/:organization_id/team/:team_id" do
    @route_owner = "@github/teams-and-orgs"
    @documentation_url = "/rest/reference/teams#get-a-team-by-name"
    org = find_org!
    team = record_or_404(org.teams.find_by(id: int_id_param!(key: :team_id)))

    control_access :get_team,
      resource: team,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true,
      organization: team.organization

    GitHub::PrefillAssociations.for_teams([team])
    deliver :team_hash, team,
      full: true,
      last_modified: calc_last_modified(team)
  end

  UpdateTeamQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($id: ID!, $name: String, $description: String, $privacy: TeamPrivacy, $permission: LegacyTeamPermission, $parentTeamId: ID, $includeFullTeamDetails: Boolean!) {
      updateTeam(input: { teamId: $id, name: $name, description: $description, privacy: $privacy, permission: $permission, parentTeamId: $parentTeamId })  {
        team {
          ...Api::Serializer::OrganizationsDependency::TeamFragment
        }
      }
    }
  GRAPHQL

  # edit a team
  patch "/organizations/:org_id/team/:team_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#update-a-team"
    team = find_team!
    control_access :admin_team,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true

    data  = receive_with_schema("team-full", "update-legacy")
    attrs = attr(data, :name, :description, :privacy, :permission, :parent_team_id)

    if data&.has_key?("parent_team_id")
      if attrs[:parent_team_id].present?
        parent_team = team.organization.teams.find_by_id(attrs[:parent_team_id])
        has_get_team_access = access_allowed? :get_team,
          team: parent_team,
          organization: parent_team.try(:organization),
          resource: parent_team.try(:organization),
          allow_user_via_integration: true,
          allow_integrations: true

        if has_get_team_access
          set_forbidden_message "You must be a maintainer of the parent team"
        end

        control_access :admin_team,
          resource: parent_team.try(:organization),
          organization: parent_team.try(:organization),
          team: parent_team,
          allow_integrations: true,
          allow_user_via_integration: true
      end
    end

    variables = {
      id: team.global_relay_id,
    }

    variables[:name] = attrs[:name] if attrs[:name]
    variables[:includeFullTeamDetails] = true
    variables[:description] = attrs[:description] if attrs[:description]
    variables[:privacy] = Platform::Enums::TeamPrivacy.coerce_isolated_result(attrs[:privacy]) if Team.valid_privacy?(attrs[:privacy])
    variables[:permission] = attrs[:permission].upcase if attrs[:permission]
    if attrs.has_key?(:parent_team_id)
      variables[:parentTeamId] = parent_team ? parent_team.global_relay_id : nil
    end

    results = platform_execute(UpdateTeamQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: "/rest/reference/teams#update-a-team",
        })
    else
      if attrs[:permission].present?
        log_data[:team_permission_changed_to] = attrs[:permission]
        GitHub.dogstats.increment("api.team_with_permission", {tags: ["permission:#{attrs[:permission].downcase}", "action:update"]})
      end
      deliver :graphql_team_hash, results.data.update_team.team, full: true
    end
  end

  DeleteTeamQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($id: ID!) {
      deleteTeam(input: { teamId: $id })  {
        isDestroyed
      }
    }
  GRAPHQL

  # delete a team
  delete "/organizations/:org_id/team/:team_id" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the team.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("team", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/teams#delete-a-team"
    team = find_team!
    control_access :admin_team,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true

    variables = {
      id: team.global_relay_id,
    }

    results = platform_execute(DeleteTeamQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
        })
    else
      deliver_empty(status: 204)
    end
  end

  ChildTeamsQuery = PlatformClient.parse <<-'GRAPHQL'
    query($id: ID!, $limit: Int!, $numericPage: Int, $includeFullTeamDetails: Boolean!) {
      node(id: $id) {
        ... on Team {
          childTeams(first: $limit, numericPage: $numericPage) {
            totalCount
            edges {
              node {
                ...Api::Serializer::OrganizationsDependency::SimpleTeamFragment
              }
            }
          }
        }
      }
    }
  GRAPHQL

  # get a list of child teams
  get "/organizations/:org_id/team/:team_id/teams" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#list-child-teams"

    team = find_team!

    control_access :get_team,
      resource: team,
      team: team,
      organization: team.organization,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true

    variables = {
      id: team.global_relay_id,
      includeFullTeamDetails: false,
      limit: per_page,
      numericPage: pagination[:page],
    }

    results = platform_execute(ChildTeamsQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
      })
    else
      child_teams = results.data.node.child_teams
      child_team_nodes = results.data.node.child_teams.edges.map(&:node)
      paginator.collection_size = child_teams.total_count
      deliver :graphql_team_hash, child_team_nodes
    end
  end

  # Team Members
  #
  #

  TeamMembersQuery = PlatformClient.parse <<~'GRAPHQL'
    query($id: ID!, $role: TeamMemberRole, $membership: TeamMembershipType, $limit: Int!, $numericPage: Int) {
      team: node(id: $id) {
        ... on Team {
          members(role: $role, membership: $membership, first: $limit, numericPage: $numericPage) {
            totalCount
            edges {
              node {
                ...Api::Serializer::UserDependency::SimpleHumanUserFragment
              }
            }
          }
        }
      }
    }
  GRAPHQL

  # list team members
  get "/organizations/:org_id/team/:team_id/members" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#list-team-members"
    team = find_team!
    control_access :get_team,
      resource: team,
      team: team,
      organization: team.organization,
      allow_integrations: true,
      allow_user_via_integration: true

    role =
      case params[:role]
      when "maintainer" then "MAINTAINER"
      when "member"     then "MEMBER"
      end

    results = platform_execute(TeamMembersQuery, variables: {
      id: team.global_relay_id,
      role: role,
      membership: "ALL",
      limit: per_page,
      numericPage: pagination[:page],
    })

    if results.errors.all.any?
      deprecated_deliver_graphql_error! resource: "Team", status: 404, errors: results.errors.all
    end

    team_members = results.data.team.members
    paginator.collection_size = team_members.total_count
    users = team_members.edges.map(&:node)

    deliver :graphql_simple_human_user_hash, users
  end

  # list team pending invitations
  get "/organizations/:org_id/team/:team_id/invitations" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#list-pending-team-invitations"
    deliver_error! 404 if GitHub.enterprise?

    team = find_team!
    set_forbidden_message "You must be an organization owner or team maintainer to list team invitations."
    control_access :list_team_invitations,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true

    invitations = paginate_rel(team.pending_invitations)

    GitHub::PrefillAssociations.for_organization_pending_invitations(invitations)

    deliver :invitation_hash, invitations
  end

  # get if user is a team member (deprecated)
  get "/organizations/:org_id/team/:team_id/members/:username" do
    @route_owner = "@github/identity"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    team = find_team!
    control_access :get_team,
      team: team,
      organization: team.organization,
      resource: team.organization,
      allow_integrations: true,
      allow_user_via_integration: true

    user = this_user

    if team.member?(user)
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # add user to a team (deprecated)
  put "/organizations/:org_id/team/:team_id/members/:username" do
    @route_owner = "@github/identity"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    @accepted_scopes = %w(admin:org repo)
    team = find_team!
    control_access :admin_team_membership,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_not_blocked! current_user, this_user

    user = this_user

    if !GitHub.bypass_org_invites_enabled? && org_invitation_sendable?(team.organization, user)
      error_code = :unaffiliated
    else
      res        = team.add_member(user, adder: current_user)
      error_code = res.status if res.error?
    end

    if error_code.present?
      error = translate_error(error_code, team.organization,
        @documentation_url)
      deliver_error(422, error)
    else
      deliver_empty(status: 204)
    end
  end

  # remove user from team (deprecated)
  delete "/organizations/:org_id/team/:team_id/members/:username" do
    @route_owner = "@github/identity"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    @accepted_scopes = %w(admin:org repo)
    team = find_team!
    control_access :admin_team_membership,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true

    team.remove_member this_user

    deliver_empty(status: 204)
  end

  # Team Memberships
  #
  #

  # create, or update, a team membership
  put "/organizations/:org_id/team/:team_id/memberships/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#add-or-update-team-membership-for-a-user"
    @accepted_scopes = %w(admin:org repo)
    success_status   = 200
    error_code       = nil
    team             = find_team!

    forbidden_message = if team&.externally_managed?
      EXTERNAL_MANAGEMENT_RESTRICTION_ERROR % tenant_provider(team)
    else
      "You must be an organization owner or team maintainer to add a team membership."
    end

    set_forbidden_message forbidden_message

    control_access :admin_team_membership,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_not_blocked! current_user, this_user

    # Introducing strict validation of the team-membership.replace
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("team-membership", "replace", skip_validation: true)
    team_role = data["role"] || "member"

    # This is kept for now as is to avoid a breaking change in the API. This should actually be
    # a HTTP status 451.
    if team.organization.has_full_trade_restrictions?
      error = translate_error(:restricted_org, team.organization, @documentation_url)
      deliver_error!(422, error)
    end

    if team.organization.direct_or_team_member?(this_user) || GitHub.bypass_org_invites_enabled?
      # The user is already affiliated with the org, or invites are disabled,
      # so we can add them to the team.

      # If invites are disabled, there may be an existing invitation for the user,
      # if so, go ahead and accept it.
      invitation = team.organization.pending_invitation_for(this_user)
      invitation.accept(acceptor: this_user) if invitation.present?

      # The user is already affiliated with the org, so we can add them to the
      # team.

      # Add the user to the team if they aren't already on it.
      unless team.member?(this_user)
        result     = team.add_member(this_user, adder: current_user)
        error_code = result.status if result.error?
      end

      # Add/remove the user's team maintainer status if requested.
      if error_code.nil?
        if team_role == "maintainer"
          begin
            team.promote_maintainer(this_user)
          rescue Team::Roles::MemberRequiredError
            error_code = :not_team_member
          rescue Organization::DirectMemberRequiredError
            error_code = :unaffiliated
          end
        elsif team_role == "member" && team.maintainer?(this_user)
          team.demote_maintainer(this_user)
        end
      end
    elsif team.organization.at_seat_limit? && !team.organization.pending_invitation_for(this_user)
      # The user is not affiliated with the org yet,
      # doesn't have a pending invitation, and there are no seats available
      # for them, so we need the "no seat" error.
      error_code = Team::AddMemberStatus::NO_SEAT.status
    else
      # The user is not affiliated with the org yet, so we need to create or
      # update an invitation.
      begin
        invitation = team.organization.invite(this_user, inviter: current_user, teams: [team])
        invitation.add_team(team, inviter: current_user, role: team_role)
      rescue OrganizationInvitation::InvalidError
        error_code = :invalid
      rescue ActiveRecord::RecordInvalid => e
        # We're getting duplicate team errors on race conditions when
        # someone submits two identical requests simultaneously. We 201 in
        # this case to indicate that the resource has already been created.
        #
        # For more context, see:
        # - https://github.com/github/github/issues/39606
        # - https://github.com/github/github/pull/39674
        if e.record && e.record.errors[:team_id].present?
          success_status = 201
        else
          error_code = :invalid
        end
      end
    end

    if error_code.present?
      error = translate_error(error_code, team.organization, @documentation_url)
      deliver_error(422, error)
    else
      deliver :team_membership_hash, { team: team, user: this_user }, status: success_status
    end
  end

  # get a team membership
  get "/organizations/:org_id/team/:team_id/memberships/:username" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#get-team-membership-for-a-user"
    team = find_team!
    user = this_user

    control_access :get_team_membership,
      resource: team,
      team: team,
      member: user,
      organization: team.organization,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    if team.membership_state_of(user, immediate_only: false) == :inactive
      deliver_error 404
    else
      deliver :team_membership_hash, { team: team, user: user}
    end
  end

  # remove a team membership
  delete "/organizations/:org_id/team/:team_id/memberships/:username" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the team-membership.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("team-membership", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/teams#remove-team-membership-for-a-user"
    team = find_team!
    user = this_user

    forbidden_message = if team&.externally_managed?
      EXTERNAL_MANAGEMENT_RESTRICTION_ERROR % tenant_provider(team)
    elsif user == current_user
      "You need at least admin:org scope to remove yourself from this team."
    else
      "You must be an organization owner or team maintainer to remove a team membership."
    end

    set_forbidden_message forbidden_message
    control_access :delete_team_membership,
      resource: team.organization,
      team: team,
      organization: team.organization,
      member: user,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    if team.member?(user)
      team.remove_member(user)
    elsif invitation = team.pending_invitation_for(user)
      invitation.remove_team(team)
    end

    deliver_empty(status: 204)
  end

  private

  # Finds the Team defined in the URL request and
  # sets the current_org if found.
  #
  # Returns a Team instance.
  def find_team(param_name: :team_id, org_param_name: :org_id)
    @team ||= Team.find_by(id: int_id_param!(key: param_name), organization_id: int_id_param!(key: org_param_name))
    @current_org ||= @team.try(:organization)
    @team
  end

  def org_invitation_sendable?(org, user)
    !org.at_seat_limit? && logged_in? && !org.direct_or_team_member?(user)
  end

  # Private: Translate a Team::AddMemberStatus error code to an API 422 response.
  #
  # error_code        - A Symbol error code.
  # org               - An Organization.
  # documentation_url - String url for documentation.
  #
  # Returns a Hash.
  def translate_error(error_code, org, documentation_url)
    response = {
      message: "Validation Failed",
      errors: [{ code: error_code, field: :user, resource: :TeamMember }],
      documentation_url: documentation_url,
    }

    case error_code
    when :blocked
      response[:message] = "User is blocked. #{GitHub.support_link_text}."
    when :dupe
      response[:message] = "User is already a member."
    when :org
      response[:message] = "Cannot add an organization as a member."
    when :no_seat
      response[:message] = "You must purchase at least one more seat to add this user as a member."
      response[:documentation_url] = "https://github.com/organizations/#{org}/settings/billing/seats"
    when :pending_cycle_no_seat
      response[:message] = "You must cancel your pending seat downgrade."
      response[:documentation_url] = "https://github.com/organizations/#{org}/settings/billing"
    when :no_2fa
      response[:message] = "User doesn't satisfy the two-factor authentication requirements for this organization."
    when :unaffiliated
      response[:message] = "User isn't a member of this organization. Please invite them first."
    when :not_team_member
      response[:message] = "User isn't a member of this team. Please add them to the team first."
      response[:documentation_url] = "/rest/reference/teams#add-or-update-team-membership-for-a-user"
    when :already_org_admin
      response[:message] = "User is already an admin of this organization, so they can't be promoted to a team maintainer."
    when :restricted_org
      response[:message] = ::TradeControls::Notices::Plaintext.organization_account_restricted
      response[:documentation_url] = GitHub.trade_controls_help_url
    end

    response
  end

  def can_list_all_teams?
    enabled = GitHub.enterprise_only_api_enabled?
    enabled && access_allowed?(:list_all_teams, allow_integrations: false, allow_user_via_integration: false)
  end

  def can_list_all_repos?
    enabled = GitHub.enterprise_only_api_enabled?
    enabled && access_allowed?(:list_all_team_repos, allow_integrations: false, allow_user_via_integration: false)
  end

  def tenant_provider(team)
    team.organization.team_sync_tenant.provider_label
  end
end
