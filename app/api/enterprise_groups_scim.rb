# rubocop:disable Style/FrozenStringLiteralComment

class Api::EnterpriseGroupsScim < Api::App
  include Api::App::SCIMHelpers

  allow_media SCIM::CONTENT_TYPE

  before do
    # Bypass IP allow list enforcement for calls to these endpoints since we do
    # not require an allow list to include the IP addresses of an IdP.
    bypass_allowed_ip_enforcement
  end

  get "/scim/v2/enterprises/:enterprise_id/Groups" do
    enterprise = find_enterprise!
    deliver_error!(404) unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#list-provisioned-scim-groups-for-an-enterprise"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    # This does not currently include any group ExternalIdentity's that are not linked to an
    # Organization, which is a case we don't currently support, but may need to in the future
    identities = enterprise.saml_provider.external_identities.group_identities

    if params[:filter]
      identities = begin
          identities.scim_filter(params[:filter])
        rescue SCIM::Filter::InvalidFilterError => e
          # https://tools.ietf.org/html/rfc7644#section-3.12
          deliver_scim_error!(400, scim_type: "invalidFilter", detail: e.message)
        end
    end

    results = paginated_results(identities)

    GitHub::PrefillAssociations.for_external_identities(results.resources)
    deliver_scim(:enterprise_groups_hash, results, excluded_attributes: params[:excludedAttributes])
  end

  get "/scim/v2/enterprises/:enterprise_id/Groups/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)
    deliver_error!(404) unless enterprise.saml_sso_enabled?

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#get-scim-provisioning-information-for-an-enterprise-group"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    external_identity = enterprise.saml_provider.external_identities.group_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) if external_identity.nil?

    GitHub::PrefillAssociations.for_group_external_identity(external_identity)
    deliver_scim(:enterprise_group_hash, external_identity, excluded_attributes: params[:excludedAttributes])
  end

  put "/scim/v2/enterprises/:enterprise_id/Groups/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#set-scim-information-for-a-provisioned-enterprise-group"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    external_identity = enterprise.saml_provider.external_identities.group_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) if external_identity.nil?
    organization = external_identity.user
    deliver_scim_error!(500, detail: "An unknown error occurred") if organization.nil?

    json = receive_with_schema("scim-group", "put")
    json_members = Array(json["members"])
    json_guids = json_members.map { |member| member["value"] }

    identities = enterprise
      .saml_provider
      .external_identities
      .where(guid: json_guids)

    results = apply_identity_operation \
      enterprise, current_user, identities, build_operation("add", organization)

    # if we're passed an empty members array we'll ignore it for now. tl:dr; in the method comment
    if json_members.any?
      cleanup_organization_membership(enterprise, organization, identities, current_user)
    end

    # allow results.empty? as we're currently bypassing requests with an empty `members` value
    if results.empty? || results.all? { |result| result.success? }
      GitHub::PrefillAssociations.for_group_external_identity(external_identity)
      deliver_scim(:enterprise_group_hash, external_identity)
    else
      # Something went wrong. Scim doesn't specify how to handle general errors
      # so let's just return a generic 500. This should probably be a 422 but SCIM doesn't allow
      # for that https://tools.ietf.org/html/rfc7644#section-3.12
      deliver_scim_error!(500, detail: results.first.error_messages.join(", "))
    end
  end

  # update a group attribute
  patch "/scim/v2/enterprises/:enterprise_id/Groups/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#update-an-attribute-for-a-scim-enterprise-group"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    json = receive_with_schema("scim-group", "patch")

    external_identity = enterprise.saml_provider.external_identities.group_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) if external_identity.nil?
    organization = external_identity.user
    deliver_scim_error!(500, detail: "An unknown error occurred") if organization.nil?

    results = []

    removed_external_identity_ids = []
    Array(json["Operations"]).each do |scim_operation|
      scim_op = scim_operation.dig("op")&.downcase
      # this is the IdP keeping us posted on the most recent name of the SCIM Group which also happens to need
      # to match the GitHub Organization name. We'll need to do something here when we have a UI to pick
      # a SCIM Group to use to control Organization membership.
      next if scim_op == "replace" && !scim_operation.dig("value").is_a?(Array) && scim_operation.dig("value", "displayName").present?

      if scim_operation.dig("path").start_with?("members")
        # these cases are intentionally WET
        case scim_op
        when "add"
          guids = scim_operation.fetch("value") { [] }.map { |member| member["value"] }
          identities = enterprise
            .saml_provider
            .external_identities
            .where(guid: guids)

          results.concat \
            apply_identity_operation(enterprise, current_user, identities, build_operation("add", organization))
        when "replace"
          guids = scim_operation.fetch("value") { [] }.map { |member| member["value"] }

          # find and update all enterprise external identities not in the list :boom:
          identities_to_remove = enterprise
            .saml_provider
            .external_identities
            .unlinked_or_users
            .where.not(guid: guids)

          remove_operation = build_operation("remove", organization)

          results.concat \
            apply_identity_operation(enterprise, current_user, identities_to_remove, remove_operation)
          removed_external_identity_ids += identities_to_remove.map(&:id)

          # add `organization.login` to `groups` for identities matching guids
          add_operation = build_operation("add", organization)
          identities_to_add = enterprise
            .saml_provider
            .external_identities
            .where(guid: guids)

          results.concat \
            apply_identity_operation(enterprise, current_user, identities_to_add, add_operation)

          # similar to PUT, if we're passed an empty array of members (so no guids) skip removal of outstanding
          # organization members
          cleanup_organization_membership(enterprise, organization, identities_to_add, current_user) if guids.any?
        when "remove"
          guids = if scim_operation.key?("value")
            scim_operation.fetch("value") { [] }.map { |object| object.dig("value") }
          else
            # an optional syntax
            [scim_operation["path"].scan(/members\[value eq \"(\S+)\"\]/).first&.first]
          end

          identities = enterprise
            .saml_provider
            .external_identities
            .unlinked_or_users
            .where(guid: guids)

          remove_operation = build_operation("remove", organization)
          results.concat \
            apply_identity_operation(enterprise, current_user, identities, remove_operation)
          removed_external_identity_ids += identities.map(&:id)
        else
          # Something went wrong. SCIM doesn't specify how to handle general errors
          # so let's just return a generic 500.
          deliver_scim_error!(500, detail: "Unknown op")
        end
      end
    end

    if results.all? { |result| result.success? }
      GitHub::PrefillAssociations.for_group_external_identity(external_identity)
      deliver_scim(:enterprise_group_hash, external_identity)
    else
      # Something went wrong. Scim doesn't specify how to handle general errors
      # so let's just return a generic 500.
      deliver_scim_error!(500, detail: "An unknown error occurred")
    end
  end

  # provision an external identity for a group
  post "/scim/v2/enterprises/:enterprise_id/Groups" do
    enterprise = find_enterprise!
    unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)
      deliver_error!(500, message: "This Enterprise account does not support membership provisioning.")
    end

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#provision-a-scim-enterprise-group-and-invite-users"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    json = receive_with_schema("scim-group", "create")
    organization = enterprise.organizations.find_by(login: json["displayName"])
    if organization.nil?
      # return success and an empty result list, if matching Organization could not be found -
      # see https://tools.ietf.org/html/rfc7644#section-3.4.2
      halt deliver_scim(:enterprise_groups_hash, paginated_results(ExternalIdentity.none), status: 201)
    end

    group_data = Platform::Provisioning::SCIMGroupData.load(json)
    options = {
      target: enterprise,
      user_id: organization.id,
      user_data: group_data,
      mapper: Platform::Provisioning::SCIMGroupMapper,
    }

    results = []
    results[0] = Platform::Provisioning::IdentityProvisioner.provision(**options)

    if results[0].success?
      json_members = Array(json["members"])
      json_guids = json_members.map { |member| member["value"] }

      identities = enterprise
        .saml_provider
        .external_identities
        .where(guid: json_guids)

      results += apply_identity_operation \
        enterprise, current_user, identities, build_operation("add", organization)

      cleanup_organization_membership(enterprise, organization, identities, current_user)
    end

    if results.all? { |result| result.success? }
      GitHub::PrefillAssociations.for_group_external_identity(results[0].external_identity)
      deliver_scim(:enterprise_group_hash, results[0].external_identity, status: 201)
    elsif results[0].invalid_identity_error?
      deliver_scim_error!(409, scim_type: "uniqueness")
    else
      # Something unexpected happened. SCIM doesn't define a general error so
      # we'll just respond with a 500.
      GitHub::Logger.log \
        error: results[0].errors.first.reason,
        error_messages: results[0].error_messages,
        enterprise_id: enterprise.id
      deliver_scim_error!(500, detail: results[0].errors.full_messages.join(" "))
    end
  end

  # deprovision a group
  delete "/scim/v2/enterprises/:enterprise_id/Groups/:external_identity_guid" do
    enterprise = find_enterprise!
    deliver_error!(404) unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(enterprise)

    @route_owner = "@github/admin-experience"
    @documentation_url = "/rest/reference/enterprise-admin#delete-a-scim-group-from-an-enterprise"

    control_access :enterprise_scim_writer,
      resource: enterprise,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: enterprise)
    deliver_error!(error_code, message: error_msg) if error_code

    # linters
    receive_with_schema("scim-group", "delete")

    external_identity = enterprise.saml_provider.external_identities.group_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) if external_identity.nil?
    organization = external_identity.user
    deliver_scim_error!(500, detail: "An unknown error occurred") if organization.nil?

    results = cleanup_organization_membership(enterprise,
                                              organization,
                                              ExternalIdentity.none,
                                              current_user,
                                              external_identities_only: true)
    external_identity.destroy

    if results.all? { |result| result.success? }
      deliver_empty status: 204
    else
      # Something went wrong. SCIM doesn't specify how to handle general errors
      # so let's just return a generic 500.
      detail = results.any? ? results.first.error_messages.join(", ") : "An unknown error occurred"
      deliver_scim_error!(500, detail: detail)
    end
  end

  private

  # applies the SCIM::Operation `operation` passed in to the list of `identities`
  #
  # enterprise - Enterprise where SAML is enabled
  # current_user - user performing the action
  # identities — an Enumerable of ExternalIdentity records to be updated
  # operation — a SCIM::Operation directive created by `build_operation`
  # provisioning_scheme - are we applying the operation to SCIM or SAML identities?
  #
  # returns an Array of Platform::Provisioning::Result instances
  def apply_identity_operation(enterprise, current_user, identities, operation,
                               provisioning_scheme: :scim)
    identities.map do |identity|
      mapper, user_data = case provisioning_scheme
      when :scim
       [Platform::Provisioning::ScimMapper, identity.scim_user_data]
      when :saml
       [Platform::Provisioning::SamlMapper, identity.saml_user_data]
      end

      patched_user_data = SCIM::Operation.apply(user_data, operation)
      options = {
        target: enterprise,
        user_data: patched_user_data,
        inviter_id: current_user.id,
        mapper: mapper,
        identity: identity,
      }

      # If the request is being performed by an app
      # we need to pass along the `IntegrationInstallation`
      # record as the inviter instead of the `Bot`.
      if current_user.bot?
        options[:inviter_id]   = current_user.installation.id
        options[:inviter_type] = IntegrationInstallation
      end

      Platform::Provisioning::IdentityProvisioner.provision_and_invite(**options)
    end
  end

  # Public: helper method to clean up organization's membership after a SCIM operation.
  #   Does the following:
  #    - removes the group attribute for this organization from the user_data of ExternalIdentity's
  #      that still belong to this Enterprise to
  #    - removes members who were not provisioned by the IdP
  #    - cancels invitations to members who were not provisioned by the IdP
  #
  # for now, explicitly skip removal of users when the payload is empty. Okta will send multiple
  # requests when an IdP admin forces a group sync. First they'll send an empty array, second the
  # members. For now we need to ignore the first request otherwise original provisioning/setup of
  # organizations won't get past the first API call which would remove everyone (including the admin(s))
  #
  # enterprise - parent enterprise where SAML is defined
  # organization — the organization that should have members removed
  # identities - ExternalIdentity's representing the current member list, according to the IdP
  # actor - used for the audit log entry for cancelling invitations
  # external_identities_only - only cleans up users who have external identities if true,
  #     removes/un-invites any users who don't have external identities otherwise
  #
  # Returns: nothing (we suppress the "Can't remove the last admin" exception by
  # passing the `allow_last_admin_removal` flag)
  def cleanup_organization_membership(enterprise, organization, identities, actor,
                                      external_identities_only: false)
    member_identities = enterprise.saml_provider.external_identities_for_organization(organization)
    identities_to_remove = member_identities.where.not(id: identities.map(&:id))
    remove_operations = build_remove_operations(organization, identities_to_remove)

    results = remove_operations[:scim].map do |remove_operation|
      apply_identity_operation(enterprise, actor,
        identities_to_remove.provisioned_by(:scim), remove_operation)
    end.flatten

    results += remove_operations[:saml].map do |remove_operation|
      apply_identity_operation(enterprise, actor,
        identities_to_remove.provisioned_by(:saml), remove_operation, provisioning_scheme: :saml)
    end.flatten

    unless external_identities_only
      org_member_ids = organization.member_ids - member_identities.map(&:user_id)
      User.where(id: org_member_ids).map do |member|
        organization.remove_member(member, allow_last_admin_removal: true)
      end

      organization.pending_invitations.where(external_identity_id: nil).each do |invitation|
        invitation.cancel(actor: actor)
      end
    end

    results
  end

  # Public: helper method for building the objects that we'll need to fully clean up group
  # membership information for ExternalIdentity's.
  # An ExternalIdentity is connected to a group (organization) if it has a 'group'
  # ExternalIdentityAttribute for a group, whose value references the organization.
  # Possible combinations:    attribute name        value
  #             Okta SCIM:       groups             organization login
  # Okta SCIM (alternate):       groups             organization identity external_id
  #             Okta SAML:       groups             organization login
  #              AAD SCIM:       groups             organization login
  #              AAD SAML:       http://schemas.microsoft.com/ws/2008/06/identity/claims/groups
  #                                                 organization identity external_id
  # organization - organization that the identities are being removed from
  # identities_to_remove - identities we need to remove
  #
  # Returns: Hash of operations that SCIM::Operation.apply can use to remove all references to
  # the organization for each of the identities specified.
  def build_remove_operations(organization, identities_to_remove)
    operations = { scim: [], saml: [] }
    if identities_to_remove.provisioned_by(:scim).any?
      operations[:scim] = [
        build_operation("remove", organization),
        build_operation_with_value("remove",
          organization.external_identity.scim_user_data.external_id)
      ]
    end
    if identities_to_remove.provisioned_by(:saml).any?
      operations[:saml] = [
        build_operation("remove", organization),
        build_operation_with_value("remove",
          organization.external_identity.scim_user_data.external_id,
          groups_path: "http://schemas.microsoft.com/ws/2008/06/identity/claims/groups")
      ]
    end

    operations
  end

  # helper method for building an object which causes an op to be applied to an external
  # identity's `groups` metadata
  #
  # op - the SCIM::Operation to perform, `add`, `remove`, or `replace`
  # organization - the organization whose login should be added, removed, or replaced
  # groups_path - value to use for the groups path
  #
  # Returns: Hash suitable for SCIM::Operation.apply
  def build_operation(op, organization, groups_path_value: "groups")
    build_operation_with_value(op, organization.login, groups_path: groups_path_value)
  end

  def build_operation_with_value(op, group_value, groups_path: "groups")
    {
      "op" => op,
      "path" => groups_path,
      "value" => {
        groups_path => [{
          "value" => group_value,
        }],
      },
      "meta" => {
        "groups_path_key" => groups_path,
      }
    }
  end
end
