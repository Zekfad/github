# frozen_string_literal: true

class Api::UserEmails < Api::App
  # List a User's email addresses
  get "/user/emails" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-email-addresses-for-the-authenticated-user"
    control_access :list_user_emails,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    emails = current_user.emails.order("id ASC")
    emails = paginate_rel(emails)

    GitHub::PrefillAssociations.fill_email_roles(emails)

    deliver :user_email_hash, emails
  end

  # List the authenticated user's public email addresses
  get "/user/public_emails" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-public-email-addresses-for-the-authenticated-user"
    control_access :list_user_emails,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    emails = current_user.email_roles.public.includes(:email).map(&:email).compact
    emails = emails.paginate(pagination)

    GitHub::PrefillAssociations.fill_email_roles(emails)

    deliver :user_email_hash, emails
  end

  # Create (add) email address(es) to a User
  # https://developer.github.com/v3/users/emails/#add-email-addresses
  post "/user/emails" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#add-an-email-address-for-the-authenticated-user"
    control_access :add_user_emails,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if !current_user.change_email_enabled?
      deliver_update_denied_using_ldap_sync! "Email addresses must be added in the #{GitHub.auth.name} Identity Provider."
    end

    data = receive
    emails = data.is_a?(Hash) ? data["emails"] : Array(data)

    # This can be removed once we begin validation using a JSON schema
    message = "Emails must be an Array of String values."
    if emails.blank?
      deliver_error! 422, message: message
    end
    if bogus = emails.detect { |email| !email.is_a?(String) }
      deliver_error! 422, message: message, errors: [api_error(:User, :email, :invalid, value: bogus)]
    end

    # Introducing strict validation of the user-email.add-emails
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # TODO: replace `receive` with `receive_with_schema`
    # see: https://github.com/github/ecosystem-api/issues/1555
    _ = receive_with_schema("user-email", "add-emails", skip_validation: true)

    emails.each do |email|
      saved = current_user.add_email(email)
      deliver_error! 422 unless saved
    end

    GitHub::PrefillAssociations.fill_email_roles(current_user.emails)

    deliver :user_email_hash, current_user.emails, status: 201
  end

  # Delete (remove) email address(es) from a User
  delete "/user/emails" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#delete-an-email-address-for-the-authenticated-user"
    control_access :delete_user_emails,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if !current_user.change_email_enabled?
      deliver_update_denied_using_ldap_sync! "Email addresses must be removed in the #{GitHub.auth.name} Identity Provider."
    end

    data = receive
    emails = data.is_a?(Hash) ? data["emails"] : Array(data)

    # This can be removed once we begin validation using a JSON schema
    message = "Emails must be an Array of String values."
    if emails.blank?
      deliver_error! 422, message: message
    end
    if bogus = emails.detect { |email| !email.is_a?(String) }
      deliver_error! 422, message: message, errors: [api_error(:User, :email, :invalid, value: bogus)]
    end
    # Introducing strict validation of the user-email.delete-emails
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # TODO: replace `receive` with `receive_with_schema`
    # see: https://github.com/github/ecosystem-api/issues/1555
    _ = receive_with_schema("user-email", "delete-emails", skip_validation: true)

    emails.each do |email|
      if current_user.emails.size == 1
        deliver_error! 422,
          message: "Cannot delete last email address",
          errors: [api_error(:User, :email, :invalid, value: email)]
      end

      unless current_user.remove_email(email)
        deliver_error! 404
      end
    end

    deliver_empty(status: 204)
  end

  # Toggle email visibility
  patch "/user/email/visibility" do
    @route_owner = "@github/profiles"
    unless GitHub.stealth_email_enabled?
      message = "Email visibility cannot be toggled in this environment."
      deliver_error! 422, message: message
    end

    @documentation_url = "/rest/reference/users#set-primary-email-visibility-for-the-authenticated-user"
    control_access :toggle_email_visibility,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    receive_with_schema("user-email", "update-visibility")

    email = current_user.primary_user_email

    if email.toggle_visibility
      GitHub::PrefillAssociations.fill_email_roles([email])
      deliver :user_email_hash, [email]
    else
      deliver_error!(422)
    end
  end
end
