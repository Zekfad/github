# frozen_string_literal: true

# Public: The Api::MultiThrottler implements the Api::Throttler interface by
# proxying calls to multiple underlying Throttler instances. The most restrictive
# rate limit is returned.
class Api::MultiThrottler
  def initialize(throttlers)
    @throttlers = throttlers
  end

  def rate!
    rate_min(@throttlers) { |throttler| throttler.rate! }
  end

  def check
    rate_min(@throttlers) { |throttler| throttler.check }
  end

  def remove!
    @throttlers.each do |throttler|
      throttler.remove!
    end
  end

  def rate_min(throttlers, &block)
    min_rate = nil
    min_scaled_rate = nil

    throttlers.each do |throttler|
      rate = block.call(throttler)

      time_remaining = rate.expires_at - Time.now.utc
      # While unlikely, we should avoid potential divide-by-zero conditions
      time_remaining = 1.second unless time_remaining > 1.second

      # Not all rates use the same time period. Normalize them.
      scaled_remaining = 1.0 * rate.remaining / time_remaining

      if min_rate.nil? || scaled_remaining < min_scaled_rate
        min_rate = rate
        min_scaled_rate = scaled_remaining
      end
    end

    min_rate
  end
end
