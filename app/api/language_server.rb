# frozen_string_literal: true

class Api::LanguageServer < Api::App
  map_to_service :code_navigation

  before do
    deliver_error! 404 if GitHub.enterprise?
  end

  HEADER_BACKEND = "X-CodeNav-Backend"
  HEADER_FORCE_BACKEND = "HTTP_X_FORCE_CODE_NAV_BACKEND"
  HEADER_BACKEND_ALEPH = "aleph"
  HEADER_BACKEND_RICH_NAV = "rich_nav"

  post "/lsp/:repository_id/:commit_oid/textDocument/definition" do
    @route_owner = "@github/semantic-code"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    repo = find_repo!
    control_access :lsp_get_definition,
      repo: repo,
      allow_integrations: false,
      allow_user_via_integration: true

    deliver_error! 404 unless GitHub.flipper[:aleph_lsp_rest_api].enabled?(repo)

    data = receive_with_schema("language-server", "definition")

    res = GitHub::Aleph.text_document_definition(
      repo: repo,
      commit_oid: params[:commit_oid],
      path: lsp_uri_to_path(data["textDocument"]["uri"]),
      row: data["position"]["line"].to_i,
      col: data["position"]["character"].to_i,
      backend: use_backend,
    )

    if res.nil? || res.error
      headers[HEADER_BACKEND] = use_backend.to_s.downcase
      deliver_error!(500, message: "failed to find definition", errors: [res&.error].reject(&:nil?))
    end

    headers[HEADER_BACKEND] = res.data.backend.to_s.downcase
    deliver_raw(res.data)
  end

  post "/lsp/:repository_id/:commit_oid/textDocument/references" do
    @route_owner = "@github/semantic-code"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    repo = find_repo!
    control_access :lsp_get_references,
      repo: repo,
      allow_integrations: false,
      allow_user_via_integration: true

    deliver_error! 404 unless GitHub.flipper[:aleph_lsp_rest_api].enabled?(repo)

    data = receive_with_schema("language-server", "references")

    res = GitHub::Aleph.text_document_references(
      repo: repo,
      commit_oid: params[:commit_oid],
      path: lsp_uri_to_path(data["textDocument"]["uri"]),
      row: data["position"]["line"].to_i,
      col: data["position"]["character"].to_i,
      backend: use_backend,
    )

    if res.nil? || res.error
      headers[HEADER_BACKEND] = use_backend.to_s.downcase
      deliver_error!(500, message: "failed to find references", errors: [res&.error].reject(&:nil?))
    end

    headers[HEADER_BACKEND] = res.data.backend.to_s.downcase
    deliver_raw(res.data)
  end

  private

  def use_backend
    @use_backend ||= begin
      case request.env[HEADER_FORCE_BACKEND].to_s.downcase
      when HEADER_BACKEND_RICH_NAV
        :RICH_NAV
      when HEADER_BACKEND_ALEPH
        :ALEPH
      else
        :AUTO
      end
    end
  end

  # convert various LSP URIs to just repo relative file path.
  def lsp_uri_to_path(uri)
    uri = URI(uri)
    uri.path
  end
end
