# rubocop:disable Style/FrozenStringLiteralComment

class Api::Admin::OrgsManager < Api::Admin
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  # Create an organization
  post "/admin/organizations" do
    @route_owner = "@github/teams-and-orgs"
    @documentation_url = "/user/rest/reference/enterprise-admin#create-an-organization"
    data = receive_with_schema("organization", "create")

    admin_user = User.find_by_login(data["admin"])
    deliver_error! 422, message: "Admin user could not be found" unless admin_user.present?

    begin
      result = Organization::Creator.perform \
        admin_user,
        GitHub::Plan.default_plan,
        { login: data["login"] }
      org = result.organization

      if result.success?
        org.update profile_name: data["profile_name"]
        deliver :organization_hash, org, status: 201
      else
        if result.error_message.present?
          deliver_error 422, message: result.error_message
        else
          deliver_error 422, errors: org.errors
        end
      end
    rescue
      deliver_error 422, errors: "Could not create organization #{data['login']}."
    end
  end

  # Rename an organization
  verbs :patch, :post, "/admin/organization/:org_id" do
    @route_owner = "@github/teams-and-orgs"
    @documentation_url = "/user/rest/reference/enterprise-admin#update-an-organization-name"
    org = find_org!
    deliver_error!(404) if org.trusted_oauth_apps_owner?

    data = receive(Hash)
    attributes = attr(data, :login)
    if org.rename(attributes[:login])
      message = "Job queued to rename organization. It may take a few minutes to complete."
      url     = api_url("/organizations/#{org.id}")
      response["location"] = url
      deliver_raw(
        {
          message: message,
          url: url,
        },
        status: 202,
      )
    else
      deliver_error! 422, errors: org.errors
    end
  end
end
