# rubocop:disable Style/FrozenStringLiteralComment

class Api::Admin::Webhooks < Api::Admin
  areas_of_responsibility :api, :webhook, :enterprise_only

  get "/admin/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/user/rest/reference/enterprise-admin#list-global-webhooks"

    hooks = paginate_rel(GitHub.global_business.hooks)
    GitHub::PrefillAssociations.for_hooks(hooks)
    deliver :global_hook_hash, hooks
  end

  get "/admin/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/user/rest/reference/enterprise-admin#get-a-global-webhook"

    hook = find_hook!
    GitHub::PrefillAssociations.for_hooks([hook])
    deliver :global_hook_hash, hook, full: true, last_modified: calc_last_modified(hook)
  end

  post "/admin/hooks/:hook_id/pings" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/user/rest/reference/enterprise-admin#ping-a-global-webhook"

    hook = find_hook!
    hook.ping
    deliver_empty(status: 204)
  end

  post "/admin/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/user/rest/reference/enterprise-admin#create-a-global-webhook"

    data = receive(Hash)
    hook = Hook.new(
      name: data["name"],
      events: Array(data.fetch("events", "*")),
      active: true,
      config: data["config"],
      installation_target: GitHub.global_business,
    )
    hook.track_creator(current_user)
    if hook.save
      deliver :global_hook_hash, hook, status: 201, full: true
    else
      deliver_error 422, errors: hook.errors, documentation_url: @documentation_url
    end
  end

  verbs :post, :patch, "/admin/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/user/rest/reference/enterprise-admin#update-a-global-webhook"

    hook = find_hook!
    data = receive(Hash)
    attributes = attr(data, :active, :config)
    if (events = Array(data["events"])).present?
      attributes[:events] = events
    end
    if hook.update attributes
      deliver :global_hook_hash, hook
    else
      deliver_error 422,
        errors: hook.errors,
        documentation_url: @documentation_url
    end
  end

  delete "/admin/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-a-global-webhook"

    hook = find_hook!
    deliver_error! 422, errors: hook.errors unless hook.destroy
    deliver_empty status: 204
  end

  private
    def find_hook!
      GitHub.global_business.hooks.find_by_id(int_id_param!(key: :hook_id)) || deliver_error!(404)
    end

end
