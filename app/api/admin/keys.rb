# frozen_string_literal: true

class Api::Admin::Keys < Api::Admin
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  # Get all keys
  get "/admin/keys" do
    @route_owner = "@github/iam"
    @documentation_url = "/user/rest/reference/enterprise-admin#list-public-keys"
    scope = filter_and_sort(PublicKey.verified)
    keys = paginate_rel(scope)

    GitHub::PrefillAssociations.for_public_keys(keys)

    deliver :public_key_hash, keys, detail: true
  end

  # Delete a key
  delete "/admin/keys/:key_id" do
    @route_owner = "@github/iam"
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-a-public-key"
    key = record_or_404(PublicKey.find_by_id(int_id_param!(key: :key_id)))

    key.delete
    deliver_empty status: 204
  end

  private

  def filter_and_sort(scope)
    # filter by accessed_at
    if (since = time_param!(:since)).present?
      scope = scope.accessed_since(since.getlocal)
    end

    if (sort = params[:sort]).present?
      direction = params[:direction] || "asc"
      scope = scope.sorted_by sort, direction
    end

    scope
  end
end
