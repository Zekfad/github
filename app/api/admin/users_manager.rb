# rubocop:disable Style/FrozenStringLiteralComment

class Api::Admin::UsersManager < Api::Admin
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  # Create user. The provided login should match what the auth mechanism provides.
  post "/admin/users" do
    deliver_error!(404) if GitHub.private_instance?

    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#create-a-user"
    data = receive(Hash)

    user = User.new_with_random_password(data["login"])

    user.email = data["email"]

    if user.valid?
      user.save
      deliver :user_hash, user, status: 201
    else
      deliver_error 422, errors: user.errors
    end
  end

  # Create or reset a GitHub site administrator 'impersonation' token
  post "/admin/user/:user_id/authorizations" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#create-an-impersonation-oauth-token"
    data = receive_with_schema("impersonation-token", "create")

    user = find_user!
    deliver_error!(404) if user.ghost?

    app_id = GitHub.enterprise_admin_oauth_app_id
    deliver_error!(404) unless app_id
    accesses = user.oauth_accesses

    if access = accesses.find_by_application_id(app_id)
      access.set_scopes(data["scopes"].reject { |e| e == "site_admin" })
      deliver :oauth_access_hash, access, status: 200, token: access.reset_token
    else
      token, hashed_token = OauthAccess.random_token_pair
      access = user.oauth_accesses.build(
        application_id: app_id,
        application_type: "OauthApplication",
        token_last_eight: token.last(8),
        hashed_token: hashed_token,
        scopes: Array(data["scopes"].reject { |e| e == "site_admin" }))
      if !(access.valid? && access.save)
        deliver_error 422, errors: access.errors
      else
        deliver :oauth_access_hash, access, status: 201, token: token
      end
    end
  end

  # Delete GitHub site administrator 'impersonation' token for specified user
  delete "/admin/user/:user_id/authorizations" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-an-impersonation-oauth-token"
    app_id = GitHub.enterprise_admin_oauth_app_id
    deliver_error!(404) unless app_id

    user = find_user!
    accesses = user.oauth_accesses
    access = accesses.find_by_application_id(app_id)
    deliver_error!(404) unless access
    access.delete if access
    deliver_empty status: 204
  end

  # Rename a user
  verbs :patch, :post, "/admin/user/:user_id" do
    deliver_error!(404) if GitHub.private_instance?

    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#update-the-username-for-a-user"
    user = find_user!

    if user.organization?
      deliver_error! 404,
        message: "#{user.login} is an organization. You can only rename " \
                    "users with this API."
    end

    if user.system_account?
      deliver_error! 404,
        message: "This account cannot be renamed because it is a system account that " \
                   "is important for GitHub to function properly."
    end

    data = receive(Hash)
    attributes = attr(data, :login)
    if user.rename(attributes[:login])
      message = "Job queued to rename user. It may take a few minutes to complete."
      url     = api_url("/user/#{user.id}")
      response["location"] = url
      deliver_raw(
        {
         message: message,
         url: url,
        },
        status: 202,
      )
    else
      deliver_error! 422,
        errors: user.errors
    end
  end

  # Delete a user
  delete "/admin/user/:user_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-a-user"
    user = find_user!
    deliver_error!(404) if user.ghost?
    if current_user == user
      deliver_error! 403,
        message: "You can't delete yourself. " \
          "You'll have to convince another admin to do that for you."
    end
    if user.organization?
      deliver_error! 404,
        message: "#{user.login} is an organization. You can only delete " \
                    "organizations via the site admin dashboard."
    end
    # This intentionally doesn't use async_destroy so that all the
    # before_destroy callbacks will be run and stop e.g. the last
    # admin of an organization being deleted.
    unless user.destroy
      deliver_error! 422,
        errors: user.errors
    end

    deliver_empty status: 204
  end
end
