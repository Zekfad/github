# rubocop:disable Style/FrozenStringLiteralComment

class Api::Admin::PreReceiveEnvironments < Api::Admin
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  # Get all environments
  get "/admin/pre-receive-environments" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#list-pre-receive-environments"

    scope = sort(PreReceiveEnvironment.scoped)
    environments = paginate_rel(scope)
    GitHub::PrefillAssociations.for_pre_receive_environments(environments)
    deliver :pre_receive_environment_hash, environments
  end

  get "/admin/pre-receive-environments/:pre_receive_environment_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#get-a-pre-receive-environment"

    environment = find_pre_receive_environment!(param_name: :pre_receive_environment_id)
    deliver :pre_receive_environment_hash, environment
  end

  post "/admin/pre-receive-environments" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#create-a-pre-receive-environment"

    data = receive_with_schema("pre-receive-environment", "create")
    accepted_attributes = attr(data, :name, :image_url)
    environment = PreReceiveEnvironment.new(accepted_attributes)
    environment.save
    deliver_error! 422, errors: environment.errors if environment.errors.present?
    deliver :pre_receive_environment_hash, environment, status: 201
  end

  verbs :patch, :post, "/admin/pre-receive-environments/:pre_receive_environment_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#update-a-pre-receive-environment"

    data = receive_with_schema("pre-receive-environment", "update")
    accepted_attributes = attr(data, :image_url, :name)
    environment = find_pre_receive_environment!(param_name: :pre_receive_environment_id)
    environment.update(accepted_attributes)
    deliver_error! 422, errors: environment.errors if environment.errors.present?
    deliver :pre_receive_environment_hash, environment
  end

  get "/admin/pre-receive-environments/:pre_receive_environment_id/downloads/latest" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#get-the-download-status-for-a-pre-receive-environment"

    environment = find_pre_receive_environment!(param_name: :pre_receive_environment_id)
    deliver :pre_receive_environment_download_hash, environment
  end

  post "/admin/pre-receive-environments/:pre_receive_environment_id/downloads" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#start-a-pre-receive-environment-download"

    environment = find_pre_receive_environment!(param_name: :pre_receive_environment_id)
    environment.queue_download
    deliver_error! 422, errors: environment.errors if environment.errors.present?
    deliver :pre_receive_environment_download_hash, environment, status: 202
  end

  delete "/admin/pre-receive-environments/:pre_receive_environment_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-a-pre-receive-environment"

    environment = find_pre_receive_environment!(param_name: :pre_receive_environment_id)
    deliver_error! 422, errors: environment.errors unless environment.can_destroy? && environment.destroy
    deliver_empty status: 204
  end

  private

  def sort(scope)
    return scope unless params[:sort].present? || params[:direction].present?
    direction = params[:direction] || "asc"
    scope.sorted_by(params[:sort], direction)
  end
end
