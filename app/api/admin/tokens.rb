# frozen_string_literal: true

class Api::Admin::Tokens < Api::Admin
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  # Get all tokens
  get "/admin/tokens" do
    @route_owner = "@github/ecosystem-api"
    # TODO https://github.com/github/internal-developer.github.com/issues/5004
    @documentation_url = "/user/rest/reference/enterprise-admin#list-personal-access-tokens"

    tokens = paginate_rel(OauthAccess.personal_tokens.order(:accessed_at))

    GitHub::PrefillAssociations.for_oauth_accesses(tokens)

    deliver :oauth_access_hash, tokens, detail: true
  end

  # Delete a token
  delete "/admin/tokens/:token_id" do
    @route_owner = "@github/ecosystem-api"
    # TODO https://github.com/github/internal-developer.github.com/issues/5004
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-a-personal-access-token"
    token = record_or_404(OauthAccess.find_by_id(int_id_param!(key: :token_id)))

    if token.id == current_user&.oauth_access&.id
      deliver_error!(403, message: "Cannot delete token being used to access this API.")
    end

    token.destroy_with_explanation(:site_admin)

    deliver_empty status: 204
  end
end
