# rubocop:disable Style/FrozenStringLiteralComment

class Api::Languages < Api::App
  areas_of_responsibility :languages, :api

  get "/languages" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    control_access :apps_audited, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    languages = Linguist::Language.all.map do |l|
      {
        name: l.name,
        aliases: l.aliases,
      }
    end
    deliver_raw languages
  end
end
