# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client/self_hosted_runners"
require "github/launch_client"

# Register self hosted runners for Actions
# This is used by the Action Runner only (https://github.com/actions/runner).
class Api::ActionsRunnerRegistration < Api::App
  areas_of_responsibility :api

  map_to_service :actions_experience

  include Api::App::GrpcHelpers
  include GitHub::LaunchClient

  def attempt_login
    @current_user = nil

    input_token = Api::RequestCredentials.token_from_scheme(env, "remoteauth")
    @data = receive(Hash, required: false)
    deliver_error! 422 unless @data

    @runner_owner = find_runner_owner(@data["url"])

    deliver_error! 404 unless @runner_owner

    scope = @runner_owner.runner_creation_token_scope
    token = User.verify_signed_auth_token(token: input_token, scope: scope)

    if !token.valid?
      if token.expired?
        deliver_error! 401, message: "Token expired."
      else
        deliver_error! 404
      end
    end

    @current_user = token.user
    @remote_token_auth = true
  end

  post "/actions/runner-registration" do
    @route_owner = "@github/c2c-actions-experience-reviewers"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error!(404) unless GitHub.actions_enabled?
    deliver_error!(422) unless @runner_owner

    if @runner_owner.is_a?(Organization)
      control_access :register_actions_runner_org,
        resource: @runner_owner,
        user: @current_user,
        allow_integrations: true,
        allow_user_via_integration: true,
        enforce_oauth_app_policy: true

    elsif @runner_owner.is_a?(Business)
      deliver_error!(404) unless GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?

      control_access :register_actions_runner_business,
        resource: @runner_owner,
        user: @current_user,
        allow_integrations: false,
        allow_user_via_integration: false,
        enforce_oauth_app_policy: true
    elsif @runner_owner.is_a?(Repository)
      control_access :register_actions_runner_repo,
        resource: @runner_owner,
        user: @current_user,
        allow_integrations: true,
        allow_user_via_integration: true
    else
      if GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
        deliver_error!(422, message: "Invalid URL, must be an Enterprise, Organization or Repository.")
      else
        deliver_error!(422, message: "Invalid URL, must be an Organization or Repository.")
      end
    end

    # Result is a GitHub::Launch::Services::Selfhostedrunners::RegisterRunnerResponse,
    # which contains `token` and url which represent a short-lived token from
    # the Actions Service system and the AZP URL the runner can self-register
    # at.
    is_remove_runner_event = @data["runner_event"] == "remove"

    result = rescue_from_grpc_errors("SelfHostedRunners") do
      if is_remove_runner_event
        SelfHostedRunners.get_runner_removal_token(@runner_owner, actor: @current_user, instrument: true)
      else
        SelfHostedRunners.get_runner_registration_token(@runner_owner, actor: @current_user, instrument: true)
      end
    end

    validate_result!(result)
    deliver :actions_runner_registration_hash, {
      url: result.url,
      token: result.token,
      token_schema: result.token_schema,
    }
  end

  private

  def find_runner_owner(url)
    return nil unless url.present?

    uri = URI(url)
    unless GitHub.enterprise?
      deliver_error!(422, message: "Must use a #{GitHub.host_name} URL") if uri.host != GitHub.host_name
    end

    _, part_1, part_2, remaining = uri.path.split("/", 3)

    # return early if URL is longer than we expected
    return nil if remaining

    if part_1 == "enterprises"
      Business.find_by(slug: part_2)
    elsif part_1 && part_2
      Repository.nwo("#{part_1}/#{part_2}")
    else
      Organization.find_by(login: part_1)
    end
  end

  def validate_result!(result)
    unless result
      Failbot.report(StandardError.new("no response from register_runner"), launch_selfhostedrunners: GitHub.launch_selfhostedrunners)
      deliver_error!(503, message: "Runner register service unavailable")
    end
  end
end
