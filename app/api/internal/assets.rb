# rubocop:disable Style/FrozenStringLiteralComment

class Api::Internal::Assets < Api::Internal
  areas_of_responsibility :data_infrastructure, :api

  require_api_version "smasher"

  post "/internal/assets/archives" do
    @route_owner = "@github/data-infrastructure"
    body = verify_and_receive(Hash, required: true) || {}
    ids = Array(body["ids"]).first(100)

    archives = Asset::Archive.all_deleteable(ids).
      map do |arc|
        {
          path_prefix: arc.alambic_path_prefix,
          oid: arc.asset_oid,
        }
      end

    deliver_raw archives
  end
end
