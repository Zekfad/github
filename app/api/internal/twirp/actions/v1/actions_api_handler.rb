# frozen_string_literal: true

require "github-proto-actions"

class Api::Internal::Twirp < ::Api::Internal
  module Actions
    module V1
      # Handles Actions-specific business logic.
      class ActionsAPIHandler < Api::Internal::Twirp::Handler
        ALLOWED_CLIENTS = %w(launch).freeze

        ALLOWED_FEATURES = %w(
          actions_disable_scheduled_workflows
          actions_disable_status_postbacks
        ).freeze


        # Public: Implentation of CheckGlobalFeature RPC, allowing clients to
        # retrieve a feature's enabled/disabled status globally.
        #
        # req - The Twirp request as a
        # GitHub::Proto::Actions::V1::CheckGlobalFeatureRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response as a Hash.
        def check_global_feature(req, env)
          maybe_error = ensure_allowed_feature(env)
          return maybe_error if maybe_error.is_a?(Twirp::Error)

          feature_or_error = get_feature_flag(req)

          return feature_or_error if feature_or_error.is_a?(Twirp::Error)
          {
            is_enabled: feature_or_error.fully_enabled?,
          }
        end

        # See Api::Internal::Twirp::Handler#require_user_token?
        #
        # This handler does not require a user token; feature management is
        # currently handled with service-level access control.
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        # Private: Builds the service instance for this handler.
        #
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Actions::V1::ActionsAPIService.new(self)
        end

        def get_feature_flag(req)
          feature = FlipperFeature.find_by(name: req.feature)
          unless feature
            return Twirp::Error.invalid_argument("unknown feature", argument: "feature")
          end
          feature
        end

        def ensure_allowed_feature(env)
          feature = env[:input].feature
          if feature.empty?
            Twirp::Error.invalid_argument("must be provided", argument: "feature")
          elsif !ALLOWED_FEATURES.include?(feature)
            Twirp::Error.invalid_argument("feature #{feature} not currently supported",
                                          argument: "feature")
          end
        end
      end
    end
  end
end
