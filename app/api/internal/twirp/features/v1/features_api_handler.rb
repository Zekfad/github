# frozen_string_literal: true

require "github-proto-features"

class Api::Internal::Twirp < ::Api::Internal
  module Features
    module V1
      # Manages feature flipping/checking.
      class FeaturesAPIHandler < Api::Internal::Twirp::Handler

        # Currently limited to these clients, identified by the HMAC key
        # used to sign requests.
        #
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        # for more information.
        ALLOWED_CLIENTS = %w(
          dependabot_api
          launch
        ).freeze

        # Feature management is currently limited to these features. For enforcement, see:
        # - FeaturesAPIHandler#build_service
        # - FeaturesAPIHandler#ensure_allowed_feature
        ALLOWED_FEATURES = %w(
          actions_disable_scheduled_workflows
          actions_disable_status_postbacks
          chat
        ).freeze

        KNOWN_ACTOR_TYPES = [
          :TYPE_USER, :TYPE_BUSINESS, :TYPE_TEAM, :TYPE_REPOSITORY, :TYPE_ORGANIZATION
        ].freeze

        VALID_FEATURE_ACTOR_TYPES = [:TYPE_USER, :TYPE_REPOSITORY, :TYPE_ORGANIZATION].freeze

        ACTORS_HARD_LIMIT = 100

        # Public: Implementation of the CheckActorsFeature RPC, allowing clients
        # to retrieve a feature's enabled/disabled status for specified actors.
        #
        # req - The Twirp request as a GitHub::Proto::Features::V1::CheckActorsFeatureRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash.
        def check_actors_feature(req, env)
          feature_or_error = get_feature_flag(req)
          return feature_or_error if feature_or_error.is_a?(Twirp::Error)

          maybe_error = validate_actors(req)
          return maybe_error if maybe_error

          actors = find_actors(req)
          results = build_actor_feature_results(actors, feature: feature_or_error)
          results = order_actor_feature_results_by_given_ids(results, req.actors)

          { results: results }
        end

        # Public: Implementation of the CheckActorFeature RPC, allowing clients
        # to retrieve a feature's enabled/disabled status for a specific actor.
        #
        # req - The Twirp request as a GitHub::Proto::Features::V1::CheckActorFeatureRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash.
        def check_actor_feature(req, env)
          with_actor_and_feature(req, env) do |actor, feature|
            {
              actor: actor_response_field(actor),
              is_enabled: feature.enabled?(actor),
            }
          end
        end

        # Public: Implementation of the EnableActorFeature RPC, allowing clients
        # to enable a feature for a specified actor.
        #
        # req - The Twirp request as a GitHub::Proto::Features::V1::EnableActorFeatureRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash.
        def enable_actor_feature(req, env)
          with_actor_and_feature(req, env) do |actor, feature|
            feature.enable(actor)

            { actor: actor_response_field(actor) }
          end
        end

        # Public: Implementation of the DisableActorFeature RPC, allowing clients
        # to disable a feature for a specified actor.
        #
        # req - The Twirp request as a GitHub::Proto::Features::V1::DisableActorFeatureRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash.
        def disable_actor_feature(req, env)
          with_actor_and_feature(req, env) do |actor, feature|
            feature.disable(actor)

            { actor: actor_response_field(actor) }
          end
        end

        # See Api::Internal::Twirp::Handler#require_user_token?
        #
        # This handler does not require a user token; feature management is
        # currently handled with service-level access control.
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        def order_actor_feature_results_by_given_ids(results, actors)
          actor_identifiers = actors.map do |actor|
            "#{actor.type}#{actor.id_string.presence || actor.id}"
          end

          results.sort_by do |result|
            type = result[:actor][:type]
            id_string = result[:actor][:id_string]
            id = result[:actor][:id]

            actor_identifier_candidate1 = "#{type}#{id_string}"
            actor_identifier_candidate2 = "#{type}#{id}"

            actor_identifiers.index(actor_identifier_candidate1) ||
              actor_identifiers.index(actor_identifier_candidate2)
          end
        end

        def build_actor_feature_results(actors, feature:)
          actors.map do |actor|
            {
              actor: actor_response_field(actor),
              is_enabled: feature.enabled?(actor),
            }
          end
        end

        def validate_actors(req)
          actors = req.actors

          if actors.size > ACTORS_HARD_LIMIT
            return Twirp::Error.invalid_argument("must have a length <= #{ACTORS_HARD_LIMIT}",
              argument: "actors")
          end

          actors.each do |actor|
            if actor.id == 0 && actor.id_string.empty?
              return Twirp::Error.invalid_argument("one must be provided",
                arguments: %w(actor.id actor.id_string))
            end

            if actor.type == :TYPE_INVALID
              return Twirp::Error.invalid_argument("invalid type", argument: "actor.type")
            end

            unless KNOWN_ACTOR_TYPES.include?(actor.type)
              return Twirp::Error.invalid_argument("unknown type #{actor.type.inspect}",
                argument: "actor.type")
            end

            unless VALID_FEATURE_ACTOR_TYPES.include?(actor.type)
              return Twirp::Error.invalid_argument(
                "cannot use the features API with an actor of type #{actor.type}",
                argument: "actor.type",
              )
            end
          end

          nil
        end

        def find_actors(req)
          users = find_user_actors(req)
          orgs = find_organization_actors(req)
          repos = find_repository_actors(req)
          users + orgs + repos
        end

        def find_user_actors(req)
          user_ids = []
          user_logins = []

          req.actors.select { |actor| actor.type == :TYPE_USER }.each do |actor|
            if actor.id > 0
              user_ids << actor.id
            elsif actor.id_string.present?
              user_logins << actor.id_string
            end
          end

          users = []
          users = users.concat(User.where(id: user_ids)) if user_ids.present?
          users = users.concat(User.where(login: user_logins)) if user_logins.present?
          users
        end

        def find_organization_actors(req)
          org_ids = []
          org_logins = []

          req.actors.select { |actor| actor.type == :TYPE_ORGANIZATION }.each do |actor|
            if actor.id > 0
              org_ids << actor.id
            elsif actor.id_string.present?
              org_logins << actor.id_string
            end
          end

          orgs = []
          orgs = orgs.concat(Organization.where(id: org_ids)) if org_ids.present?
          orgs = orgs.concat(Organization.where(login: org_logins)) if org_logins.present?
          orgs
        end

        def find_repository_actors(req)
          repo_ids = []
          repo_nwos = []

          req.actors.select { |actor| actor.type == :TYPE_REPOSITORY }.each do |actor|
            if actor.id > 0
              repo_ids << actor.id
            elsif actor.id_string.present?
              repo_nwos << actor.id_string
            end
          end

          repos = []
          if repo_ids.present?
            # Preload owner relation so we can populate id_string in #actor_response_field
            repos = repos.concat(Repository.includes(:owner).where(id: repo_ids))
          end
          if repo_nwos.present?
            # Preload owner relation so we can populate id_string in #actor_response_field
            repos = repos.concat(Repository.includes(:owner).with_names_with_owners(repo_nwos))
          end
          repos
        end

        # Private: Find the specified actor.
        #
        # req - The Twirp request; one of:
        #       GitHub::Proto::Features::V1::CheckActorFeatureRequest,
        #       GitHub::Proto::Features::V1::EnableActorFeatureRequest,
        #       GitHub::Proto::Features::V1::DisableActorFeatureRequest.
        #
        # Returns a User, Business, or Team if the actor is found, nil if the
        # actor isn't found, and a Twirp::Error if invalid conditions have been
        # provided.
        def find_actor(req)
          if req.actor.id == 0 && req.actor.id_string.empty?
            return Twirp::Error.invalid_argument("one must be provided",
              arguments: %w(actor.id actor.id_string))
          end

          case req.actor.type
          when :TYPE_INVALID
            Twirp::Error.invalid_argument("invalid type", argument: "actor.type")
          when :TYPE_USER
            find_user_actor(req.actor)
          when :TYPE_BUSINESS
            Twirp::Error.invalid_argument("cannot use the features API with a business actor",
              argument: "actor.type")
          when :TYPE_TEAM
            Twirp::Error.invalid_argument("cannot use the features API with a team actor",
              argument: "actor.type")
          when :TYPE_REPOSITORY
            find_repository_actor(req.actor)
          when :TYPE_ORGANIZATION
            find_organization_actor(req.actor)
          else
            Twirp::Error.invalid_argument("unknown type #{req.actor.type.inspect}",
              argument: "actor.type")
          end
        end

        def find_repository_actor(params)
          if params.id > 0
            Repository.find_by(id: params.id)
          elsif params.id_string.present?
            Repository.with_name_with_owner(params.id_string)
          end
        end

        def find_organization_actor(params)
          if params.id > 0
            Organization.find_by(id: params.id)
          elsif params.id_string.present?
            Organization.find_by_login(params.id_string)
          end
        end

        # Private: Find a user actor, if any, matching the provided conditions.
        #
        # params - The actor conditions as a GitHub::Proto::Features::V1::Actor
        #
        # Returns a User if the actor is found, nil otherwise.
        def find_user_actor(params)
          if params.id > 0
            User.find_by(id: params.id)
          elsif params.id_string.present?
            User.find_by_login(params.id_string)
          end
        end

        # Private: Builds the service instance for this handler.
        #
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          service = GitHub::Proto::Features::V1::FeaturesAPIService.new(self)
          service.before do |rack_env, env|
            ensure_allowed_feature(env)
          end
          service
        end

        # Private: Ensure the provided feature name is allowed.
        #
        # Note that this currently assumes all RPCs in the service handler
        # receive a "feature" argument. See build_service for how this is
        # called.
        #
        # env - The Twirp environment as a Hash.
        #
        # Returns a Twirp::Error if the feature isn't allowed, which halts the
        # request, and nothing if the feature is allowed, letting the request
        # continue.
        def ensure_allowed_feature(env)
          feature = env[:input].feature
          if feature.empty?
            Twirp::Error.invalid_argument("must be provided", argument: "feature")
          elsif !ALLOWED_FEATURES.include?(feature)
            Twirp::Error.invalid_argument("feature #{feature} not currently supported",
              argument: "feature")
          end
        end

        # Private: Actor information for serialization in the response.
        #
        # actor - the User, Organization, or Repository actor
        #
        # Returns the actor information as a Hash.
        def actor_response_field(actor)
          if actor.is_a?(User) && actor.user?
            { id: actor.id, id_string: actor.login, type: :TYPE_USER }
          elsif actor.is_a?(Organization) && actor.organization?
            { id: actor.id, id_string: actor.login, type: :TYPE_ORGANIZATION }
          elsif actor.is_a?(Repository)
            { id: actor.id, id_string: actor.name_with_owner, type: :TYPE_REPOSITORY }
          else
            { id: actor.id, type: :TYPE_INVALID }
          end
        end

        # Private: Attempt to retrieve the appropriate actor and feature
        # information for this request, executing the provided block or
        # returning an error.
        #
        # This is a convenience to abstract away some common query and error
        # checking concerns from RPC methods.
        #
        # req - The Twirp request; one of:
        #       GitHub::Proto::Features::V1::CheckActorFeatureRequest,
        #       GitHub::Proto::Features::V1::EnableActorFeatureRequest,
        #       GitHub::Proto::Features::V1::DisableActorFeatureRequest.
        #
        # env - The Twirp environment as a Hash.
        #
        # Returns the result of the provided block when actor and feature
        # information is successfully retrieved, otherwise returns the
        # appropriate Twirp::Error.
        def with_actor_and_feature(req, env, &block)
          actor = find_actor(req)
          if actor.is_a?(Twirp::Error)
            return actor
          elsif actor.nil?
            return Twirp::Error.invalid_argument("unknown actor", argument: "actor")
          end

          feature_or_error = get_feature_flag(req)
          return feature_or_error if feature_or_error.is_a?(Twirp::Error)

          yield actor, feature_or_error
        end

        def get_feature_flag(req)
          feature = FlipperFeature.find_by(name: req.feature)
          unless feature
            return Twirp::Error.invalid_argument("unknown feature", argument: "feature")
          end
          feature
        end

      end
    end
  end
end
