# frozen_string_literal: true

class Api::Internal::Twirp
  module Hooks
    module V1

      # Public: Generate the config for a hook.
      #
      # hook - A Hook
      #
      # Returns a Hash suitable for use as a GitHub::Proto::Hooks::V1::Config
      def self.config(hook)
        hook_config = hook.config

        {
          url: hook_config["url"],
          content_type: hook_config["content_type"] || "",
          is_ssl_verified: hook_config["insecure_ssl"] == "1",
        }
      end
    end
  end
end
