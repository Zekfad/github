# frozen_string_literal: true

# Simple HMAC-based service-to-service access control.
#
# Note: this is a stopgap measure until we have better ways to identify services.
module Api::Internal::Twirp::ClientAccess
  # Convenient client names used for testing
  TEST_CLIENT_NAMES = %w(allowed otherallowed disallowed)

  # Public: Adds a before hook to the service instance to check the requesting service (client)
  # for the current handler/RPC.
  #
  # service - The Twirp::Service subclass instance.
  # handler - The Api::Internal::Twirp::Handler subclass instance.
  #
  # Returns nothing.
  def self.install(service, handler)
    service.prepend do |rack_env, env|
      if (client_key = rack_env[:request_hmac_key])
        process(client_key, handler, env)
      else
        # It shouldn't be possible to reach this point, but we return a Twirp
        # error just in case.
        return Twirp::Error.permission_denied("a valid request HMAC was not provided", {})
      end
    end
  end

  def self.process(client_key, handler, env)
    client_name = GitHub.api_internal_twirp_hmac_settings[client_key]
    if !allow_client?(client_name, handler, env)
      return Twirp::Error.permission_denied("client not supported", name: client_name)
    end
  end

  def self.allow_client?(client_name, handler, env)
    if GitHub.twirp_supports_test_clients? && TEST_CLIENT_NAMES.include?(client_name)
      client_name == "allowed"
    else
      handler.allow_client?(client_name, env)
    end
  end
end
