# frozen_string_literal: true

# Token-based access restrictions for Twirp services on behalf of a user.
module Api::Internal::Twirp::UserAccess

  # The HTTP request header where the token is provided (uppercase Rack
  # notation style with "HTTP" prefix).
  AUTHORIZATION_HEADER = "HTTP_X_TWIRP_AUTHORIZATION"

  # The HTTP request header where the token scope is provided (uppercase Rack
  # notation style with "HTTP" prefix).
  SCOPE_HEADER = "HTTP_X_TWIRP_SCOPE"

  # Public: Adds a before hook to the service instance; it checks for a user
  # token at the handler instance's request.
  #
  # service - The Twirp::Service subclass instance.
  # handler - The Api::Internal::Twirp::Handler subclass instance.
  #

  # Returns nothing.
  def self.install(service, handler)
    service.prepend do |rack_env, env|
      process(rack_env[AUTHORIZATION_HEADER], rack_env[SCOPE_HEADER], env)
      if handler.require_user_token?(env)
        ensure_current_user(env)
      end
    end
  end

  # Private: Check to see if a user token provided a user ID.
  #
  # env - The Twirp environment as a Hash.
  #
  # Returns a Twirp::Error if a user ID wasn't found in the environment,
  # otherwise return nothing.
  def self.ensure_current_user(env)
    unless env[:user_id]
      return Twirp::Error.unauthenticated("a valid user token was not provided")
    end
  end
  private_class_method :ensure_current_user

  # Private: Runs the access check, setting the current user ID and next authorization
  # HTTP header as a side-effect if successful.
  #
  # token_header - The authorization header as a String.
  # scope_header - The scope header as a String.
  # env          - The Twirp environment as a Hash.
  #
  # Returns the user ID, or a Twirp::Error in the event that access isn't granted.
  def self.process(token_header, scope_header, env)
    return unless token_header.present? && scope_header.present?

    result = extract_user_id(token_header, scope_header, env)

    # When the user ID is successfully extracted from the provided authorization
    # header, put it into the Twirp environment and provide a fresh token in a
    # response header.
    if result.is_a?(Integer)
      env[:user_id] = result
    end

    result
  end
  private_class_method :process

  # Private: Extracts the user ID from the authorization and scope headers.
  #
  # token_header - The authorization header as a String.
  # scope_header - The scope header as a String.
  # env          - The Twirp environment as a Hash.
  #
  # Returns the user ID, or a Twirp::Error if the token is invalid or expired.
  def self.extract_user_id(token_header, scope_header, env)
    token = token_header.split(/\s+/).last
    parsed_token = GitHub::Authentication::SignedAuthToken.instrument(
      GitHub::Authentication::SignedAuthToken::Session.verify(token: token, scope: scope_header),
    )
    return Twirp::Error.unauthenticated("invalid token") if parsed_token.nil?
    return Twirp::Error.unauthenticated("invalid token: #{parsed_token.reason}") unless parsed_token.valid?

    parsed_token.user.id
  end
  private_class_method :extract_user_id

end
