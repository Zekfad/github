# frozen_string_literal: true

require "github-proto-insights"

class Api::Internal::Twirp
  module Insights
    module V1
      class PullRequestsAPIHandler < Api::Internal::Twirp::Handler
        # Limited to the Insights client, identified by the HMAC key used to sign requests.
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        ALLOWED_CLIENTS = %w(insights).freeze

        LIST_PULL_REQUESTS_DEFAULT_LIMIT = 1000

        # Internal: Implementation of the FindUpdatedPullRequestsByTimeRequest RPC, retrieving a list
        # of pull requests for a given repository id, which are updated in given time frame.
        #
        # req - The Twirp request as a GitHub::Proto::Insights::V1::FindUpdatedPullRequestsByTimeRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash suitable for use as a
        # GitHub::Proto::Insights::V1::FindUpdatedPullRequestsByTimeRequest, or a Twirp::Error.
        def find_updated_pull_requests_by_time(req, env)

          if req.repository_id.zero?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "repository_id")
          end

          if req.from_time.nil?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "from_time")
          end

          limit = req.limit.zero? ? LIST_PULL_REQUESTS_DEFAULT_LIMIT : req.limit
          sort_order = req.is_desc_order ? "updated_at desc" : "updated_at asc"

          if req.to_time.nil?
            pull_requests = PullRequest.where(repository_id: req.repository_id)
                                .where("updated_at >= ?", protobuf_timestamp_to_time(req.from_time))
                                .order(sort_order).limit(limit)
          else
            pull_requests = PullRequest.where(repository_id: req.repository_id, updated_at: protobuf_timestamp_to_time(req.from_time)..protobuf_timestamp_to_time(req.to_time))
                                .order(sort_order).limit(limit)
          end

          GitHub::PrefillAssociations.prefill_associations(pull_requests, [:reviews, :review_requests])
          {
              pull_requests: build_pull_requests_list(pull_requests),
          }
        end

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Insights::V1::PullRequestsAPIService.new(self)
        end

        def to_protobuf_timestamp_if_present(time)
          if time && time.is_a?(ActiveSupport::TimeWithZone)
            return Google::Protobuf::Timestamp.new(seconds: time.to_i, nanos: time.nsec)
          end
          if time && time.is_a?(Integer)
            return Google::Protobuf::Timestamp.new(seconds: time)
          end
        end

        def protobuf_timestamp_to_time(timestamp)
          timestamp.present? ? Time.at(timestamp.nanos * 10 ** -9 + timestamp.seconds) : nil
        end

        # Private: Converts an array of Review Objects to Hash Object
        def build_review_list(pr)
          pr.reviews.map do |review|
            {
                id: review.id,
                pull_request_id: review.pull_request_id,
                user_id: review.user_id,
                state: review.state,
                head_sha: review.head_sha,
                body: review.body,
                formatter: review.formatter,
                user_hidden: review.user_hidden,
                is_comment_hidden: review.comment_hidden,
                comment_hidden_reason: review.comment_hidden_reason,
                comment_hidden_classifier: review.comment_hidden_classifier,
                created_at: to_protobuf_timestamp_if_present(review.created_at),
                updated_at: to_protobuf_timestamp_if_present(review.updated_at),
                submitted_at: to_protobuf_timestamp_if_present(review.submitted_at),
            }.compact
          end
        end

        # Private: Converts an array of Review Request Objects to Hash Object
        def build_review_request_list(pr)
          pr.review_requests.map do |review|
            {
                id: review.id,
                reviewer_id: review.reviewer_id,
                pull_request_id: review.pull_request_id,
                created_at: to_protobuf_timestamp_if_present(review.created_at),
                updated_at: to_protobuf_timestamp_if_present(review.updated_at),
                reviewer_type: review.reviewer_type,
                dismissed_at: to_protobuf_timestamp_if_present(review.dismissed_at),
            }.compact
          end
        end

        # Private: Convert an array of Pull Request objects to the shape Twirp responses expect.
        #
        # prs - The array of Pull Request objects.
        #
        # Returns an array of Hash objects with user data that matches the
        # Twirp definition.
        def build_pull_requests_list(prs)
          prs.map do |pr|
            {
                id: pr.id,
                base_sha: pr.base_sha,
                head_sha: pr.head_sha,
                repository_id: pr.repository_id,
                user_id: pr.user_id,
                created_at: to_protobuf_timestamp_if_present(pr.created_at),
                updated_at: to_protobuf_timestamp_if_present(pr.updated_at),
                base_repository_id: pr.base_repository_id,
                head_repository_id: pr.head_repository_id,
                base_ref: pr.base_ref,
                head_ref: pr.head_ref,
                merged_at: pr.merged_at,
                base_user_id: pr.base_user_id,
                head_user_id: pr.head_user_id,
                is_mergeable: pr.mergeable,
                merge_commit_sha: pr.merge_commit_sha,
                contributed_at: to_protobuf_timestamp_if_present(pr.contributed_at_timestamp),
                contributed_at_offset: pr.contributed_at_offset,
                fork_collab_state: pr.fork_collab_state,
                user_hidden: pr.user_hidden,
                base_sha_on_merge: pr.base_sha_on_merge,
                is_work_in_progress: pr.work_in_progress,
                reviews: build_review_list(pr),
                review_requests: build_review_request_list(pr)
            }.compact
          end
        end
      end
    end
  end
end
