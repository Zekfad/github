# frozen_string_literal: true

require "github-proto-packageregistry"

class Api::Internal::Twirp
  module Packageregistry
    module V1
      class OrgApiHandler < Api::Internal::Twirp::Handler
        # Limited to the Package Registry client, identified by the HMAC key used to sign requests.
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        ALLOWED_CLIENTS = %w(package_registry).freeze

        def get_org_id(req, env)
          org_or_user = User.find_by_name(req.name)
          if org_or_user.nil?
            return Twirp::Error.not_found("org/user name not found")
          end
          {
              org_id: org_or_user.id
          }
        end

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # Internal: See Api::Internal::Twirp::Handler#allow_client?
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private
        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Packageregistry::V1::OrgAPIService.new(self)
        end
      end
    end
  end
end
