# frozen_string_literal: true

module Api::Internal::Twirp::Packageregistry::Access
  # Public: Adds a before hook to the service instance to check the requesting service (client)
  # for the current handler/RPC.
  #
  # service - The Twirp::Service subclass instance.
  # handler - The Api::Internal::Twirp::Handler subclass instance.
  #
  # Returns nothing.
  #
  # The Package Registry middleware collects the Authorization, X-Github-Request-ID and Remote-addr headers
  # and populates env with this data, for the handler to use
  def self.install(service, handler)
    service.prepend do |rack_env, env|
      if (auth = rack_env["HTTP_X_TWIRP_AUTHORIZATION"]) && (req_id = rack_env["HTTP_X_GITHUB_REQUEST_ID"]) && (real_ip = rack_env["HTTP_X_REAL_IP"])
        process(auth, req_id, real_ip, env)
      else
        #TODO this error isn't returned properly (comes back as "unexpected return" / LocalJumpError), find out why
        return Twirp::Error.permission_denied("One or more of 'X-Twirp-Authorization', 'X-Github-Request-Id' or 'X-Real-IP' headers not provided", {})
      end
    end
  end

  def self.process(authorization, request_id, real_ip, env)
    env[:authorization] = authorization
    env[:request_id] = request_id
    env[:real_ip] = real_ip
    env[:user_agent] = "package_registry"
    env[:path] = "/internal/twirp/github.packageregistry.v1.LoginAPI/ValidateTokenScopes"
  end
end
