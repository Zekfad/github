# frozen_string_literal: true

require "github-proto-secretscanning"

class Api::Internal::Twirp
  module Secretscanning
    module V1
      class OauthAccessAPIHandler < SecretScanningAPIHandler
        def revoke_oauth_access(req, env)
          candidates = req.candidates&.to_a

          unless candidates.present?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "candidates")
          end

          accesses = OauthAccess.for_tokens(candidates.map(&:token))
          revoked_count = 0

          candidates.each do |candidate|
            if access = accesses[candidate.token]
              revoke_and_notify(candidate, access)
              revoked_count += 1
            end
          end

          { revoked_count: revoked_count }
        end

        private

        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Secretscanning::V1::OauthAccessAPIService.new(self)
        end

        def revoke_and_notify(candidate, access)
          access.destroy_with_explanation(:token_scan)
          AccountMailer.access_token_leaked(access.user, nil, candidate.url, true).deliver_later
        end
      end
    end
  end
end
