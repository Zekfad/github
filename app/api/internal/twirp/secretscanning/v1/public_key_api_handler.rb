# frozen_string_literal: true

require "github-proto-secretscanning"

class Api::Internal::Twirp
  module Secretscanning
    module V1
      class PublicKeyAPIHandler < SecretScanningAPIHandler
        include GitHub::TokenScanning::TokenScanningPostProcessingHelper

        def unverify_public_key(req, env)
          candidates = req.candidates&.to_a

          unless candidates.present?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "candidates")
          end

          private_keys = candidates.map(&:private_key)
          fingerprints = ssh_key_fingerprints(private_keys)
          public_keys = public_keys_for_fingerprints(fingerprints.values.compact)

          unverified_count = 0

          candidates.each do |candidate|
            fingerprint = fingerprints[candidate.private_key]
            key = public_keys[fingerprint]
            unverified_count += 1 if unverify_and_notify(key, candidate)
          end

          { unverified_count: unverified_count }
        end

        private

        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Secretscanning::V1::PublicKeyAPIService.new(self)
        end

        def unverify_and_notify(key, candidate)
          return false unless key.present?
          return false if public_key_already_unverified?(key)

          unverify_and_notify_public_key(key, false, candidate.url)
          true
        end
      end
    end
  end
end
