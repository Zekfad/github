# frozen_string_literal: true

class Api::Internal::Twirp
  module Secretscanning
    module V1
      class SecretScanningAPIHandler < Api::Internal::Twirp::Handler
        ALLOWED_CLIENTS = ["token_scanning_service"].freeze

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # Internal: See Api::Internal::Twirp::Handler#allow_client?
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end
      end
    end
  end
end
