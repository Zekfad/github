# frozen_string_literal: true

require "github-proto-users"

class Api::Internal::Twirp < ::Api::Internal
  module Users
    module V1

      # Convert SignedAuthToken tokens to user identities.
      class TokensAPIHandler < Api::Internal::Twirp::Handler

        # Currently limited to these clients, identified by the HMAC key
        # used to sign requests.
        #
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        # for more information.
        ALLOWED_CLIENTS = %w().freeze

        # Public: Implementation of the IdentifyTokenUser Twirp RPC.
        #
        # req - The Twirp request as a GitHub::Proto::Users::V1::IdentifyTokenUserRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash suitable for use in a
        # GitHub::Proto::Users::V1::IdentifyTokenUserResponse.
        def identify_token_user(req, env)
          if env[:user_id]
            user = User.find_by(id: env[:user_id])
            {
              user: token_user_identity(user),
            }
          else
            {}
          end
        end

        # See Api::Internal::Twirp::Handler#require_user_token?
        #
        # This handler requires a token since its entire function is converting
        # from a token to a user.
        def require_user_token?(env)
          true
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        # Private: Builds the service instance for this handler.
        #
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Users::V1::TokensAPIService.new(self)
        end

        def token_user_identity(user)
          return nil unless user
          {
            id: user.id,
            login: user.login,
            name: user.safe_profile_name,
            avatar_url: user.primary_avatar_url,
            type: V1.user_type_enum(user),
          }
        end
      end
    end
  end
end
