# frozen_string_literal: true

# Adds additional request lifecycle hooks to services
module Api::Internal::Twirp::ServiceHooks

  # Prepend a before hook.
  #
  # Useful to ensure general hooks aren't run after handler-specific
  # hooks.
  def prepend(&block)
    @before.unshift(block)
  end
end
