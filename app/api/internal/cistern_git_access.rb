# rubocop:disable Style/FrozenStringLiteralComment

class Api::Internal::CisternGitAccess < Api::Internal
  statsd_tag_actions "/internal/cistern/:api_version/repositories/:repo_id/git/dereference/*"
  statsd_tag_actions "/internal/cistern/:api_version/repositories/:repo_id/git/trees/*"
  statsd_tag_actions "/internal/cistern/:api_version/repositories/:repo_id/git/blobs/:sha"
  statsd_tag_actions "/internal/cistern/:api_version/repositories/:repo_id/git/refs/*"

  require_api_version "searchinternalauth"

  # p99 tree size is around 3M per https://github.com/github/code-search/issues/1069
  # but until we roll out some additional changes to Cistern we need to stay at 10MB
  # https://github.com/github/availability/issues/1336
  MAX_TREE_LIMIT = 10.megabyte.freeze
  LEGACY_MAX_TREE_LIMIT = 70.megabyte.freeze

  GITRPC_CONNECT_TIMEOUT = 1 # as seconds
  GITRPC_REQUEST_TIMEOUT = 6 # as seconds
  CONTROLLER_TIMEOUT     = GITRPC_CONNECT_TIMEOUT + GITRPC_REQUEST_TIMEOUT + 1

  MAX_BLOB_SIZE = 100.megabytes

  include Api::App::ContentHelpers

  ERROR_CLASS_HEADER = "X-Cistern-API-Error-Class"
  ERROR_DETAILS_HEADER = "X-Cistern-API-Error-Details"

  # wire up the controller-level global timeout handler for Cistern interal API calls
  before do
    # CONTROLLER_TIMEOUT is applied in GitHub.request_timeout(env)
    # https://github.com/github/github/blob/master/lib/github/config.rb#L596
    GitHub::TimeoutMiddleware.notify(env, CisternTimeoutHandler.new(self))
  end

  # Require Request-HMAC header for all these APIs.
  def require_request_hmac?
    true
  end

  # Skip rate limits.
  def check_whitelist_for_request_limiting
    GitHub::Limiters::Middleware.skip_limit_checks(env)
  end

  # Get the commit for a ref or a commit ID. This is a combination of Get Ref and Get Commit.
  get "/internal/cistern/:api_version/repositories/:repo_id/git/dereference/*" do
    @route_owner = "@github/code-search"
    begin
      api_dereference
    rescue StandardError => exc
      needs_love_error!(exc)
    end
  end

  def api_dereference
    repo = load_repo_by_id(params[:repo_id])
    perform_dereference(repo)
  end

  # Get the contents of a tree object by repo_id
  get "/internal/cistern/:api_version/repositories/:repo_id/git/trees/*" do
    @route_owner = "@github/code-search"
    begin
      api_trees
    rescue StandardError => exc
      needs_love_error!(exc)
    end
  end

  def api_trees
    repo = load_repo_by_id(params[:repo_id])
    if params.fetch(:api_version, "v2") == "v2" # legacy
      perform_get_tree(repo, LEGACY_MAX_TREE_LIMIT)
    else
      perform_get_tree(repo, MAX_TREE_LIMIT)
    end
  end

  # Get the contents of a blob object by repo_id
  get "/internal/cistern/:api_version/repositories/:repo_id/git/blobs/:sha" do
    @route_owner = "@github/code-search"
    begin
      api_blobs
    rescue StandardError => exc
      needs_love_error!(exc)
    end
  end

  def api_blobs
    repo = load_repo_by_id(params[:repo_id])
    if params.fetch(:api_version, "v2") == "v2" # legacy
      perform_get_blob(repo, false)
    else
      perform_get_blob(repo, true)
    end
  end

  # Get the contents of a ref object
  get "/internal/cistern/:api_version/repositories/:repo_id/git/refs/*" do
    @route_owner = "@github/code-search"
    begin
      api_refs
    rescue GitRPC::Timeout
      timeout_error!
    rescue StandardError => exc
      needs_love_error!(exc)
    end
  end

  def api_refs
    repo = load_repo_by_id(params[:repo_id])
    perform_get_ref(repo)
  end

  get "/internal/cistern/:api_version/repositories/:repo_id/token" do
    @route_owner = "@github/code-search"
    begin
      api_token
    rescue StandardError => exc
      needs_love_error!(exc)
    end
  end

  def api_token
    repo = load_repo_by_id(params[:repo_id])
    perform_get_token(repo)
  end

  private

  # Look up a commit by commit ID (ffffffffffffffffffffffffffffffffffffffff) or fully-qualified reference name (refs/heads/master).
  #
  # Delivers an error if not found.
  def perform_dereference(repo)
    name = params[:splat].join("/").b

    begin
      commit = commit_by_oid(repo, name) || commit_for_ref(repo, name)

      if commit.nil?
        commit_not_found_error!
      end

      deliver :commit_hash, commit, repo: repo, last_modified: calc_last_modified(commit)
    rescue GitRPC::Timeout
      timeout_error!
    rescue GitRPC::ObjectMissing
      object_missing_error!
    rescue GitRPC::InvalidObject
      object_invalid_error!
    end
  end

  # Return the commit for the OID.
  #
  # Returns nil if not an OID.
  #
  # Raises GitRPC::ObjectMissing if the commit is not found.
  def commit_by_oid(repo, oid)
    return nil unless oid =~ /\A[a-f0-9]{40}\z/i
    repo.commits.find(oid)
  end

  # Return the commit for the ref name.
  #
  # Returns nil if not fully-qualified or if the ref cannot be found.
  #
  # Raises GitRPC::ObjectMissing if the commit is not found.
  def commit_for_ref(repo, name)
    return nil unless name.starts_with?("refs/")
    repo.commit_for_ref(name)
  end

  def perform_get_tree(repo, tree_size_limit = MAX_TREE_LIMIT)
    begin
      sha = repo.ref_to_sha(params[:splat].first)
      sha_not_found_error! unless sha
      truncated = false

      if params[:recursive]

        if params.fetch(:api_version, "v2") == "v4"
          result = rpc_with_timeout(repo).ls_tree(sha, long: true, recurse: true, show_trees: true, byte_limit: tree_size_limit)
        else
          result = repo.rpc.ls_tree(sha, long: true, recurse: true, show_trees: true, byte_limit: tree_size_limit)
        end
        tree_too_large_error! if result["truncated"]

        entries = []
        result["entries"].each do |entry|
          entries << TreeEntry.from_ls_tree(repo, entry, from_collection: true)
        end
      else
        _oid, entries, truncated = repo.tree_entries(sha, "", recursive: params[:recursive], limit: 100_000, skip_size: false)
      end
      tree_empty_error! if entries.empty?
      GitHub.dogstats.histogram("search_git_trees#{params[:recursive] && "_recursive"}.entry_count", entries.size, tags: ["via:search_api"])

      tree = Api::GitTrees::Tree.new(sha, entries, truncated)
      deliver :api_git_trees_tree_hash, tree,
        repo: repo,
        last_modified: calc_last_modified(repo)
    rescue GitRPC::Timeout
      timeout_error!
    rescue GitRPC::ObjectMissing
      object_missing_error!
    rescue GitRPC::InvalidObject
      object_invalid_error!
    # Doing this here isn't great. The recursive fetching should probably be pushed back down into
    # TreeListable#tree_entries so it can raise the same GitRPC::InvalidObject for both cases.
    rescue GitRPC::CommandFailed => boom
      raise unless boom.message.include?("fatal: not a tree object")

      tree_not_tree_error!
    end
  end

  def perform_get_blob(repo, with_headers_check = false)
    begin
      if with_headers_check
        # fetch blob headers first https://github.com/github/code-search/issues/1109

        if params.fetch(:api_version, "v2") == "v4"
          obj_headers = rpc_with_timeout(repo).read_object_headers([sha_param!(:sha)]).first
        else
          obj_headers = repo.rpc.read_object_headers([sha_param!(:sha)]).first
        end

        commit_not_found_error! if obj_headers.nil?
        commit_not_commit_error! if obj_headers["type"] != "blob"
        blob_too_large_error! if obj_headers["size"] > MAX_BLOB_SIZE
      end
      # fetch the bytes
      blob = repo.blob_by_oid(sha_param!(:sha))
    rescue GitRPC::Timeout
      timeout_error!
    rescue GitRPC::ObjectMissing
      object_missing_error!
    rescue GitRPC::InvalidObject
      object_invalid_error!
    end

    blob_too_large_error! if blob.size > MAX_BLOB_SIZE

    if medias.api_param?(:raw)
      deliver_raw blob.raw_data, content_type: blob.content_type
    else
      deliver :grit_blob_hash, blob, repo: repo
    end
  end

  def perform_get_ref(repo)
    name = build_name_from_params

    if ref = repo.extended_refs.find(name)
      deliver :ref_hash, ref, last_modified: calc_last_modified(repo)
    else
      ref_not_found_error!
    end
  end

  def perform_get_token(repo)
    data = {
      proto: "http",
      member: "gitauth-full-trust:cistern",
    }
    expiration = 1.hour.from_now.utc
    token = GitHub::Authentication::GitAuth::SignedAuthToken.generate(
      repo: repo, scope: "read", data: data, expires: expiration,
    )

    deliver_raw(
      {
        id: repo.id,
        network_id: repo.network_id,
        owner: repo.owner.login,
        name: repo.name,
        nwo: repo.nwo,
        token: token,
        expires_at: expiration.iso8601,
      },
    )
  end

  # A GitRPC object including an overriden timeout. For Cistern code cache.
  # Sum of timeout values should not exceed dotcom request timeout.
  #   :request_timeout - seconds until GitRPC request should timeout
  #   :connect_timeout - seconds before connection attempt should timeout
  def rpc_with_timeout(repo, request_timeout = GITRPC_REQUEST_TIMEOUT, connect_timeout = GITRPC_CONNECT_TIMEOUT)
    return @timed_rpc if defined?(@timed_rpc)

    @timed_rpc ||=
      GitRPC.new(
        "dgit:/",
        delegate: GitHub::DGit::Delegate::Repository.new(repo.network_id, repo.id, repo.no_dgit_shard_path),
        timeout: request_timeout,
        connect_timeout: connect_timeout,
    )

    @timed_rpc.content_key = "gitrpc:#{repo.network_cache_key}"
    @timed_rpc.repository_key = "gitrpc:#{repo.repository_cache_key}"
    @timed_rpc.options[:info] = {
      user_id: GitHub.context[:actor_id],
      real_ip: GitHub.context[:actor_ip],
      repo_id: repo.id,
      repo_name: repo.name_with_owner,
    }

    @timed_rpc
  end

  def build_name_from_params
    ("refs/" + params[:splat].join("/")).b
  end

  def load_repo_by_id(repo_id)
    if repo_id.blank?
      deliver_error! 400, message: "params[:repo_id] is empty"
    end
    @repo_id = repo_id

    current_repo = ActiveRecord::Base.connected_to(role: :reading) do
      Repository.find_by(id: repo_id)
    end

    ensure_repository_exists_and_ingest_enabled(current_repo)
  end

  def ensure_repository_exists_and_ingest_enabled(current_repo)
    if current_repo.nil?
      repo_not_found_error!
    end

    if current_repo.empty?
      repo_is_empty_error!
    end

    unless current_repo.cistern_ingest_enabled?
      repo_ingest_not_enabled_error!
    end

    current_repo
  end

  def needs_love_error!(exception)
    options = {
      message: "Unhandled error occured: #{exception.message}"
    }
    headers = {
      ERROR_CLASS_HEADER => "UNHANDLED_ERROR",
      ERROR_DETAILS_HEADER => exception.class.to_s,
    }

    log_data = {
      controller: "Api::Internal::CisternGitAccess",
      request_id: env["HTTP_X_GITHUB_REQUEST_ID"],
      error_class: exception.class.to_s,
      error_message: exception.message
    }
    if @repo_id.present?
      log_data["repo_id"] = @repo_id
    end
    GitHub::Logger.log(log_data)

    deliver_error_with_headers! 500, options, headers
  end

  def tree_not_tree_error!
    options = {
      message: "Invalid object requested. SHA must identify a commit or a tree."
    }
    headers = {
      ERROR_CLASS_HEADER => "TREE_NOT_TREE_ERROR"
    }
    deliver_error_with_headers! 422, options, headers
  end

  def tree_empty_error!
    options = {
      message: "Git tree is empty."
    }
    headers = {
      ERROR_CLASS_HEADER => "TREE_EMPTY_ERROR"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def commit_not_found_error!
    options = {
      message: "Could not dereference ref."
    }
    headers = {
      ERROR_CLASS_HEADER => "COMMIT_NOT_FOUND_ERROR"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def commit_not_commit_error!
    options = {
      message: "Requested object is not a commit."
    }
    headers = {
      ERROR_CLASS_HEADER => "COMMIT_NOT_COMMIT"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def sha_not_found_error!
    options = {
      message: "SHA not found using repo.ref_to_sha."
    }
    headers = {
      ERROR_CLASS_HEADER => "SHA_NOT_FOUND_ERROR"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def ref_not_found_error!
    options = {
      message: "Could not find ref in repo.extended_refs."
    }
    headers = {
      ERROR_CLASS_HEADER => "REF_NOT_FOUND_ERROR"
    }
    deliver_error_with_headers! 404, options, headers
  end

  # IMPORTANT! Called by CisternTimeoutHandler, see comments there!
  def controller_timeout_error!
    options = {
      message: "Requested operation resulted in controller timeout exceeding #{CONTROLLER_TIMEOUT} seconds"
    }
    headers = {
      ERROR_CLASS_HEADER => "CONTROLLER_TIMEOUT"
    }
    deliver_error_with_headers! 502, options, headers
  end

  def timeout_error!
    options = {
      message: "Requested operation resulted in timeout on GitRPC side - https://github.com/github/github/blob/master/vendor/gitrpc/lib/gitrpc/error.rb."
    }
    headers = {
      ERROR_CLASS_HEADER => "GitRPC::Timeout"
    }
    deliver_error_with_headers! 502, options, headers
  end

  def object_missing_error!
    options = {
      message: "Requested object was not found."
    }
    headers = {
      ERROR_CLASS_HEADER => "GitRPC::ObjectMissing"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def object_invalid_error!
    options = {
      message: "Invalid object requested"
    }
    headers = {
      ERROR_CLASS_HEADER => "GitRPC::InvalidObject"
    }
    deliver_error_with_headers! 422, options, headers
  end

  def repo_is_empty_error!
    options = {
      message: "Git Repository is empty."
    }
    headers = {
      ERROR_CLASS_HEADER => "REPO_EMPTY_ERROR"
    }
    deliver_error_with_headers! 409, options, headers
  end

  def repo_not_found_error!
    options = {
      message: "repository does not exist"
    }
    headers = {
      ERROR_CLASS_HEADER => "REPO_NOT_FOUND_ERROR"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def repo_ingest_not_enabled_error!
    options = {
      message: "cistern ingest not enabled for this repository"
    }
    headers = {
      ERROR_CLASS_HEADER => "REPO_INGEST_NOT_ENABLED_ERROR"
    }
    deliver_error_with_headers! 404, options, headers
  end

  def blob_too_large_error!
    options = {
      message: "This API returns blobs up to 100 MB in size. "\
               "The requested blob is too large to fetch via the API, "\
               "but you can always clone the repository via Git in order to obtain this blob.",
      errors: [api_error(:Blob, :data, :too_large)],
      documentation_url: "/v3/git/blobs/#get-a-blob"
    }
    headers = {
      ERROR_CLASS_HEADER => "BLOB_TOO_LARGE_ERROR"
    }
    deliver_error_with_headers! 403, options, headers
  end

  def tree_too_large_error!
    options = {
      message: "The requested tree or subtree exceeded the maximum byte size #{MAX_TREE_LIMIT}",
    }
    headers = {
      ERROR_CLASS_HEADER => "TREE_TOO_LARGE_ERROR"
    }
    deliver_error_with_headers! 403, options, headers
  end

  def deliver_error_with_headers!(http_status, options = {}, headers = {})
    datadog_tags = [
      "http_status:#{http_status}"
    ]
    datadog_tags << "error_class:#{headers[ERROR_CLASS_HEADER]}" if headers.key?(ERROR_CLASS_HEADER)

    GitHub.dogstats.increment("api.internal.cistern_git_access.error", tags: datadog_tags)
    @meta.merge!(headers)
    deliver_error! http_status, options
  end

  # This handles behavior specific to Cistern controller timeouts (not GitRPC specific ones)
  #
  # IMPORANT! This handler is unlikely to fire as expected since the
  # GitHub::TimeoutMiddleware has to exit the process rather than
  # send responses. In the (probably rare) event that the notifier
  # able to push a response out before the process sending it for
  # us is killed, this will ensure we still get a 502 on req timeout.
  class CisternTimeoutHandler
    def initialize(controller)
      @controller = controller
    end

    def timeout(_env)
      @controller.controller_timeout_error!
    end
  end
end
