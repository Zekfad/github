# frozen_string_literal: true

class Api::Internal::Repositories < Api::Internal
  include Api::App::JobEnqueueHelpers

  statsd_tag_actions "/internal/repositories/:repository_id/git/pushes"
  statsd_tag_actions "/internal/repositories/:repository_id/wiki/git/pushes"

  # Internal: Given a repo identifier, returns the repo id, network id and nwo.
  get "/internal/repositories/:repository_id" do
    @route_owner = "@github/pe-repos"

    repo = if params[:include_hidden] == "true"
      find_repo_include_hidden!
    else
      find_repo!
    end

    deliver :internal_repository_hash, repo
  end

  # Internal: After a successful push to a Git repository happened, this endpoint
  # is notified about who updated which refs over what protocol.
  post "/internal/repositories/:repository_id/git/pushes" do
    @route_owner = "@github/pe-repos"
    repo = find_repo!
    data = receive_with_schema("git-push", "create")
    notices = []
    decode_push_data!(data)

    enqueue_push_job(repo, data)

    if data["ref_updates"].any?
      notices = [new_pr_notice(repo, data), vulnerability_notice(repo, data)].select(&:present?)
    else
      Failbot.report(ArgumentError.new("Should not reach post-receive hooks without ref_updates"))
    end

    if notices.any?
      notice = notices.join("\n")
      deliver :internal_git_push_output, notice, status: 202
    else
      deliver_empty status: 202
    end
  end

  # Internal: After a successful push to a repository's wiki Git repository happened, this endpoint
  # is notified about who updated which refs over what protocol.
  post "/internal/repositories/:repository_id/wiki/git/pushes" do
    @route_owner = "@github/pe-repos"
    repo = find_repo!

    data = receive_with_schema("git-push", "create")
    decode_push_data!(data)

    enqueue_push_job(repo.unsullied_wiki, data)

    deliver_empty status: 202
  end

  # Internal: After successful password authentication, this endpoint is notified periodically
  # to warn the user of the upcoming deprecation.
  post "/internal/repositories/:repository_id/git/password_auth" do
    @route_owner = "@github/prodsec"
    body = verify_and_receive(Hash, required: true) || {}

    AccountMailer.git_password_auth_deprecation(
      user_id: body["user_id"],
      repository_id: params["repository_id"],
      user_agent: body["user_agent"],
      resource_type: body["resource_type"],
    ).deliver_later

    deliver_empty status: 202
  end

  # Internal: Behaves like find_repo! but returns disabled/spammy repos.
  def find_repo_include_hidden!
    repo = find_repo
    repo&.network_broken?
    repo = nil unless repo&.active?
    record_or_404(repo)
  rescue Repository::NetworkMissingError => e
    Failbot.report e
    deliver_disabled_repo_error!(repo)
  end

  def new_pr_notice(repo, data)
    return if data["ref_updates"].size != 1

    first_ref = data["ref_updates"].first
    before = first_ref["before_oid"]
    ref = first_ref["refname"]
    if first_head_push?(before, ref) && !default_ref_push?(ref, repo)
      ref_name = ref["refs/heads/".length..-1]
      notice = "\nCreate a pull request for '#{ref_name}' on GitHub by visiting:\n     #{GitHub.url}/#{repo.nwo}/pull/new/#{Api::LegacyEncode.encode(ref_name)}\n\n"
    end

    notice
  end

  def vulnerability_notice(repo, data)
    user = User.find_by(login: data["pusher"])
    return if user.nil?
    return unless user.wants_vulnerability_cli_notifications?
    return unless repo.vulnerability_alerts_visible_to?(user)

    vuln_count = repo.visible_vulnerability_alerts.count
    return if vuln_count.zero?

    first_ref = data["ref_updates"].first
    ref = first_ref["refname"]
    return if ref_is_tag?(ref)

    GlobalInstrumenter.instrument("repository.push_vulnerability_notification", {
      repository: repo,
      actor: user,
    })

    counts_by_severity = repo.alert_count_by_severity(actor: user)
    vulnerabilities = "#{vuln_count} #{'vulnerability'.pluralize(vuln_count)}"
    vuln_breakdown = vuln_count_string(counts_by_severity)

    alerts_url =
      if vuln_count == 1
        repo.visible_vulnerability_alerts.first&.permalink(include_host: true)
      else
        urls = GitHub::Application.routes.url_helpers
        url_params = { host: GitHub.url, repository: repo, user_id: repo.owner }

        if GitHub.dedicated_alerts_ui_enabled?
          urls.network_alerts_url(url_params)
        elsif GitHub.dependency_graph_enabled?
          urls.network_dependencies_url(url_params)
        else
          # We shouldn't get here but the repository URL is a good failsafe.
          urls.repository_url(url_params)
        end
      end

    "\nGitHub found #{vulnerabilities} on #{repo.nwo}'s default branch (#{vuln_breakdown}). To find out more, visit:\n     #{alerts_url}\n\n"
  end

  def first_head_push?(before_oid, current_ref)
    GitHub::NULL_OID == before_oid && current_ref.start_with?("refs/heads/")
  end

  def default_ref_push?(ref, repo)
    default_ref = get_default_ref(repo)
    ref == default_ref || !repo.rpc.rev_parse(default_ref)
  end

  def get_default_ref(repo)
    branch = repo.default_branch ||= "master"
    "refs/heads/#{branch}"
  end

  def externally_accessible?
    false
  end

  def require_request_hmac?
    true
  end

  def authenticated_for_private_mode?
    true
  end

  # Relax user-agent requirements for internal API endpoints used by babeld
  def user_agent_allows_access_to_garage_hosts?
    request.user_agent =~ /#{GitHub.current_sha}|^babeld\/.*/
  end

  def vuln_count_string(counts)
    sevs = Vulnerability::SEVERITIES.reverse
    count_strings = sevs.map do |severity|
      if counts[severity]
        "#{counts[severity]} #{severity}"
      end
    end

    count_strings.compact.join(", ")
  end

  def ref_is_tag?(ref)
    ref.to_s.start_with?("refs/tags/")
  end
end
