# rubocop:disable Style/FrozenStringLiteralComment

class Api::Internal::MediaApp < Api::Internal
  areas_of_responsibility :lfs, :api

  require_api_version "galactus"

  post "/internal/media/transitions" do
    @route_owner = "@github/lfs"
    body = verify_and_receive(Hash, required: true) || {}
    transition_id = body["transition_id"].to_i
    blob_id = body["blob_id"].to_i
    op = body["operation"].to_s

    transition = Media::Transition.find_by_id(transition_id) if transition_id > 0

    if !transition
      deliver_error! 400, message: "Media Transition #{transition_id.inspect} does not exist"
    end

    blob = Media::Blob.find_by_id(blob_id) if blob_id > 0
    case transition.verify_blob(op, blob)
    when :operation
      deliver_error! 400, message: "Media Transition #{transition_id.inspect} was not #{op.inspect}"
    when :not_blob, :wrong_network
      deliver_error! 400, message: "Media Transition #{transition_id.inspect} does not match blob #{blob_id.inspect}"
    end

    deliver_raw(
      oid: blob.oid,
      old_path_prefix: blob.alambic_path_prefix,
      new_path_prefix: "media/#{transition.repository_network_id}",
    )
  end
end
