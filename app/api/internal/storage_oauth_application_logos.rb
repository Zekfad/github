# rubocop:disable Style/FrozenStringLiteralComment

# Implements the API that Alambic uses for serving or accepting avatar
# uploads through the Alambic storage cluster. Used on GitHub Enterprise only.
#
# https://github.com/github/alambic/tree/master/docs/assets
class Api::Internal::StorageOauthApplicationLogos < Api::Internal::StorageUploadable
  areas_of_responsibility :avatars, :api, :enterprise_only
  require_api_version "smasher"

  def self.enforce_private_mode?
    false
  end

  get "/internal/storage/oauth_logos/:guid" do
    @route_owner = "@github/avatars"
    logo = ActiveRecord::Base.connected_to(role: :reading) { OauthApplicationLogo.find_by_guid(params[:guid].to_s) }
    control_access :read_user_files, file: logo, allow_integrations: false, allow_user_via_integration: false
    GitHub::PrefillAssociations.for_oauth_logos([logo])
    deliver :internal_storage_hash, logo, env: request.env
  end

  post "/internal/storage/oauth_logos" do
    @route_owner = "@github/avatars"
    control_access :write_oauth_app_logo, allow_integrations: false, allow_user_via_integration: false
    validate_uploadable OauthApplicationLogo
  end

  post "/internal/storage/oauth_logos/verify" do
    @route_owner = "@github/avatars"
    control_access :write_oauth_app_logo, allow_integrations: false, allow_user_via_integration: false
    create_uploadable OauthApplicationLogo
  end

  def verify_user(meta)
    OauthApplicationLogo.storage_verify_token(@asset_token, meta)
  end
end
