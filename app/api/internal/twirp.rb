# frozen_string_literal: true

class Api::Internal::Twirp < ::Api::Internal
  require_dependency "api/internal/twirp/features/v1/features_api_handler"
  require_dependency "api/internal/twirp/repositories/v1/repositories_api_handler"
  require_dependency "api/internal/twirp/support/v1/abuse_reports_api_handler"
  require_dependency "api/internal/twirp/users/v1"
  require_dependency "api/internal/twirp/users/v1/following_api_handler"
  require_dependency "api/internal/twirp/users/v1/tokens_api_handler"
  require_dependency "api/internal/twirp/users/v1/users_api_handler"
  require_dependency "api/internal/twirp/users/v1/businesses_api_handler"
  require_dependency "api/internal/twirp/auditlogs/v1/auditlogs_api_handler"
  require_dependency "api/internal/twirp/hooks/v1/hooks_api_handler"
  require_dependency "api/internal/twirp/support/v1/users_api_handler"
  require_dependency "api/internal/twirp/policy/v1/repository_api_handler"
  require_dependency "api/internal/twirp/insights/v1/pull_requests_api_handler"
  require_dependency "api/internal/twirp/insights/v1/issues_api_handler"
  require_dependency "api/internal/twirp/insights/v1/issue_events_api_handler"
  require_dependency "api/internal/twirp/secretscanning/v1/public_key_api_handler"
  require_dependency "api/internal/twirp/actions/v1/actions_api_handler"

  ##
  # Path pattern
  PATH_REGEX = /\A(?:\/api)?\/internal\/twirp\//i.freeze

  def self.acceptable_media_types
    ["application/protobuf"]
  end

  ##
  # Mount services for handlers

  register Mount

  mount ::Api::Internal::Twirp::Features::V1::FeaturesAPIHandler
  mount ::Api::Internal::Twirp::Repositories::V1::RepositoriesAPIHandler
  mount ::Api::Internal::Twirp::Support::V1::AbuseReportsApiHandler
  mount ::Api::Internal::Twirp::Users::V1::FollowingAPIHandler
  mount ::Api::Internal::Twirp::Users::V1::TokensAPIHandler
  mount ::Api::Internal::Twirp::Users::V1::UsersAPIHandler
  mount ::Api::Internal::Twirp::Users::V1::BusinessesAPIHandler
  mount ::Api::Internal::Twirp::Auditlogs::V1::AuditlogsAPIHandler
  mount ::Api::Internal::Twirp::Hooks::V1::HooksAPIHandler
  mount ::Api::Internal::Twirp::Support::V1::UsersAPIHandler
  mount ::Api::Internal::Twirp::Insights::V1::PullRequestsAPIHandler
  mount ::Api::Internal::Twirp::Insights::V1::IssuesAPIHandler
  mount ::Api::Internal::Twirp::Insights::V1::IssueEventsAPIHandler
  mount ::Api::Internal::Twirp::Actions::V1::ActionsAPIHandler

  # Registry handlers bypass UserAccess and load their own middleware
  mount ::Api::Internal::Twirp::Packageregistry::V1::LoginApiHandler, install: [
      ::Api::Internal::Twirp::ClientAccess,
      ::Api::Internal::Twirp::Packageregistry::Access
  ]

  mount ::Api::Internal::Twirp::Packageregistry::V1::OrgApiHandler, install: [
      ::Api::Internal::Twirp::ClientAccess,
  ]

  mount ::Api::Internal::Twirp::Policy::V1::RepositoryAPIHandler, install: [
      ::Api::Internal::Twirp::ClientAccess,
  ]

  mount ::Api::Internal::Twirp::Secretscanning::V1::PublicKeyAPIHandler, install: [
    ::Api::Internal::Twirp::ClientAccess,
  ]
  mount ::Api::Internal::Twirp::Secretscanning::V1::OauthAccessAPIHandler, install: [
    ::Api::Internal::Twirp::ClientAccess,
  ]

  ##
  # ::Api::Internal access control checks

  def externally_accessible?
    false
  end

  def require_request_hmac?
    true
  end
end
