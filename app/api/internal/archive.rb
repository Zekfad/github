# frozen_string_literal: true

class Api::Internal::Archive < Api::Internal::GitContent
  rate_limit_as Api::RateLimitConfiguration::ARCHIVE_FAMILY

  # Request comes from:
  #
  #   https://github.com/{user}/{repo}/archive/{commitish}.{ext}
  #
  get "/internal/archive" do
    @route_owner = "@github/pe-repos"
    deliver_error! 404, message: "Missing path parameter" unless params[:path]
    params[:path] = params[:path][1..-1] if params[:path][0] == "/"
    params[:user], params[:repo], params[:arc_type], params[:splat] = params[:path].scrub.split(/\//, 4)
    deliver_error! 404, message: "Bogus path" unless (params[:user] && params[:repo] && params[:arc_type] && params[:splat])
    params[:splat] = [params[:splat].gsub(/\/+\Z/, "")]

    @api_stats_key = :archive

    # "gist" isn't a valid username, don't try to use it to find a gist
    gist_owner_param = :user unless params[:user] == "gist"
    repo_or_gist = find_repo || find_gist(param_name: :repo, owner_param: gist_owner_param)
    attempt_remote_token_login GitRepository::ArchiveCommand.token_scope(repo_or_gist)
    verify_not_spammy

    # validate repo_or_gist
    repository_or_gist_or_404 repo_or_gist

    # Bypass IP allow list enforcement to this endpoint called from codeload
    # for public repositories.
    bypass_allowed_ip_enforcement if repo_or_gist.is_a?(Repository) && repo_or_gist.public?

    if repo_or_gist.is_a?(Gist)
      control_access :get_gist_contents, resource: repo_or_gist, allow_integrations: true, allow_user_via_integration: true
    else
      control_access :get_contents, resource: repo_or_gist, allow_integrations: true, allow_user_via_integration: true
    end

    if hash = repository_archive_hash(repo_or_gist)
      deliver_raw hash
    else
      deliver_error 404, message: "Bad archive command for #{repo_or_gist.name_with_owner}/#{h content_path.inspect}/#{h params[:arc_type].inspect}"
    end
  end

  def repository_archive_hash(repo_or_gist)
    actor_id = logged_in? ? current_user.id : 0
    actor_token = if logged_in? && repo_or_gist.is_a?(Repository)
      current_user.signed_auth_token(expires: 1.hour.from_now,
                                     scope: Media.auth_scope(repo_or_gist.id, "download"))
    else
      nil
    end
    repo_or_gist.archive_command(content_path, params[:arc_type],
                                 preferred_dc: params[:dc], actor_id: actor_id,
                                 actor_token: actor_token).to_hash
  rescue ArgumentError
  end

  def fingerprint_rate_limit_configuration
    {
      max_tries: GitHub.api_archive_fingerprint_rate_limit,
      ttl: rate_limit_configuration.duration,
      amount: increment_rate_limit_amount,
    }
  end
end
