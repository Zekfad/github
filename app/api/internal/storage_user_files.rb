# rubocop:disable Style/FrozenStringLiteralComment

# Implements the API that Alambic uses for serving or accepting issue
# attachments through the Alambic storage cluster. Used on GitHub Enterprise
# only.
#
# https://github.com/github/alambic/tree/master/docs/assets
class Api::Internal::StorageUserFiles < Api::Internal::StorageUploadable
  areas_of_responsibility :code_collab, :api
  require_api_version "smasher"

  def self.enforce_private_mode?
    false
  end

  def deliver_user_file(guid)
    file = ActiveRecord::Base.connected_to(role: :reading) { UserAsset.find_by_guid(guid) }
    control_access :read_user_files, file: file, allow_integrations: false, allow_user_via_integration: false
    GitHub::PrefillAssociations.for_user_assets([file])
    deliver :internal_storage_hash, file, env: request.env
  end

  get "/internal/storage/user/:user_id/files/:guid" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    deliver_user_file params[:guid]
  end

  # Handles legacy enterprise paths in GHE 2.4 and below.
  # https://uploads.ghe.io/github-enterprise-assets/0000/0126/0000/1147/2a517e68-b60f-11e5-817f-de93ec03543a.png
  get "/internal/storage/github-enterprise-assets/:a/:b/:c/:d/:guid" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    deliver_user_file params[:guid].to_s.split(".").first
  end

  post "/internal/storage/user/:user_id/files" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    owner = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(params[:user_id].to_i) }
    control_access :write_user_files, owner: owner, allow_integrations: false, allow_user_via_integration: false
    validate_uploadable UserAsset
  end

  post "/internal/storage/user/:user_id/files/verify" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    owner = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(params[:user_id].to_i) }
    control_access :write_user_files, owner: owner, allow_integrations: false, allow_user_via_integration: false
    create_uploadable UserAsset
  end

  def verify_user(meta)
    UserAsset.storage_verify_token(@asset_token, meta)
  end
end
