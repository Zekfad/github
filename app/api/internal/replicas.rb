# frozen_string_literal: true

class Api::Internal::Replicas < Api::Internal
  ERROR_REPLICA_EXISTS = "Replica exists on host."
  ERROR_INVALID_ENTITY_SPEC = "Invalid spokes entity spec."
  ERROR_REPLICA_NOT_FOUND = "No replica on host."
  ERROR_HOST_TOO_BUSY = "Host is too busy."

  post "/internal/spokes/entities/:entity_type/:entity_id/replicas" do
    @route_owner = "@github/pe-repos"

    data = receive(Hash)
    type = params["entity_type"]
    id   = params["entity_id"].to_i
    host = data["host"]

    case type
    when "gist"
      if GitHub::DGit::Routing.all_hosts_for_gist(id).include?(host)
        deliver_error!(409, message: ERROR_REPLICA_EXISTS)
      end
      ctx = GitHub::DGit::Maintenance::GistMaintenanceContext.new
    when "network"
      if GitHub::DGit::Routing.all_hosts_for_network(id).include?(host)
        deliver_error!(409, message: ERROR_REPLICA_EXISTS)
      end
      ctx = GitHub::DGit::Maintenance::NetworkMaintenanceContext.new
    else
      deliver_error!(422, message: ERROR_INVALID_ENTITY_SPEC)
    end

    unless ctx.ok_to_queue_job?(host)
      deliver_error!(429, message: ERROR_HOST_TOO_BUSY)
    end

    ctx.enqueue_create_replica(id, data["host"])
    deliver_raw({status: "replica creation scheduled for #{type}/#{id}"}, status: 202)
  end

  post "/internal/spokes/entities/:entity_type/:entity_id/replicas/:host/repair" do
    @route_owner = "@github/pe-repos"

    type = params["entity_type"]
    id   = params["entity_id"].to_i
    host = params["host"]

    # A spec for a repo is in the form <network id>/<repository id>
    network_id = params["entity_type"].to_i
    if network_id > 0
      type = "repo"

      # A wiki spec is in the form of <network_id>/<repository_id>.wiki
      if params["entity_id"].end_with? ".wiki"
        type = "wiki"
      end
    end

    # We only need a context for ok_to_queue_job? It would seem like the base
    # MaintenanceContext would be fine but the metrics it generates requires an
    # "entity". That "entity" will be network for everything besides gists.
    case type
    when "gist"
      ctx = GitHub::DGit::Maintenance::GistMaintenanceContext.new
    else
      ctx = GitHub::DGit::Maintenance::NetworkMaintenanceContext.new
    end

    deliver_error!(429, message: ERROR_HOST_TOO_BUSY) unless ctx.ok_to_queue_job?(host, increment: true)

    case type
    when "gist"
      GitHub::DGit::Maintenance.repair_gist_replica(id, host)
    when "repo"
      GitHub::DGit::Maintenance.repair_repo_replica(id, network_id, host, GitHub::DGit::RepoType::REPO)
    when "wiki"
      GitHub::DGit::Maintenance.repair_repo_replica(id, network_id, host, GitHub::DGit::RepoType::WIKI)
    when "network"
      GitHub::DGit::Maintenance.repair_network_replica(id, host)
    else
      deliver_error!(422, message: ERROR_INVALID_ENTITY_SPEC)
    end

    deliver_raw({status: "replica repair scheduled for #{type} #{id}"}, status: 200)
  end

  patch "/internal/spokes/entities/:entity_type/:entity_id/replicas/:src_host" do
    @route_owner = "@github/pe-repos"

    type = params["entity_type"]
    id = params["entity_id"].to_i
    src_host = params["src_host"]

    data = receive(Hash)
    dst_host = data["dst_host"]

    existing_hosts = []
    case type
    when "gist"
      existing_hosts = GitHub::DGit::Routing.all_hosts_for_gist(id)
      ctx = GitHub::DGit::Maintenance::GistMaintenanceContext.new
    when "network"
      existing_hosts = GitHub::DGit::Routing.all_hosts_for_network(id)
      ctx = GitHub::DGit::Maintenance::NetworkMaintenanceContext.new
    else
      deliver_error!(422, message: ERROR_INVALID_ENTITY_SPEC)
    end

    unless existing_hosts.include?(src_host)
      deliver_error!(404, message: ERROR_REPLICA_NOT_FOUND)
    end
    if existing_hosts.include?(dst_host)
      deliver_error!(409, message: ERROR_REPLICA_EXISTS)
    end

    unless ctx.ok_to_queue_job?(src_host) || ctx.ok_to_queue_job?(dst_host)
      deliver_error!(429, message: ERROR_HOST_TOO_BUSY)
    end

    ctx.enqueue_move_replica(id, src_host, dst_host)
    deliver_raw({status: "replica move scheduled for #{type}/#{id}"}, status: 202)
  end

  delete "/internal/spokes/entities/:entity_type/:entity_id/replicas/:host" do
    @route_owner = "@github/pe-repos"

    type = params["entity_type"]
    id = params["entity_id"].to_i
    host = params["host"]

    existing_hosts = []
    case type
    when "gist"
      existing_hosts = GitHub::DGit::Routing.all_hosts_for_gist(id)
      ctx = GitHub::DGit::Maintenance::GistMaintenanceContext.new
    when "network"
      existing_hosts = GitHub::DGit::Routing.all_hosts_for_network(id)
      ctx = GitHub::DGit::Maintenance::NetworkMaintenanceContext.new
    else
      deliver_error!(422, message: ERROR_INVALID_ENTITY_SPEC)
    end

    unless existing_hosts.include?(host)
      deliver_error!(404, message: ERROR_REPLICA_NOT_FOUND)
    end

    unless ctx.ok_to_queue_job?(host)
      deliver_error!(429, message: ERROR_HOST_TOO_BUSY)
    end

    ctx.enqueue_destroy_replica(id, host, inhibit_repairs: true)
    deliver_raw({ status: "replica deletion scheduled for #{type}/#{id}"}, status: 202)
  end

  def externally_accessible?
    false
  end

  def require_request_hmac?
    true
  end

  def authenticated_for_private_mode?
    true
  end
end
