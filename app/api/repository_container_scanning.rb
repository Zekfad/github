# frozen_string_literal: true

class Api::RepositoryContainerScanning < Api::App
  areas_of_responsibility :api

  # Handle a status report
  post "/repositories/:repository_id/container-scanning/check-run" do
    @route_owner = "@github/containerscrew"
    @documentation_url = Platform::NotDocumentedBecause::EXPERIMENTAL
    repo = ActiveRecord::Base.connected_to(role: :reading) { find_repo! }
    # This endpoint is only exposed when reached from Actions
    return deliver_error!(404) unless current_integration&.launch_github_app? || current_integration&.launch_lab_github_app?

    control_access :create_check_run_for_container_scan,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: false

    data = receive_with_schema("container-scanning-check-run", "create")
    data[:repository] = repo.name_with_owner

    response = GitHub::Scanitizer.upload_scan_result(data)
    if response.nil?
      deliver_empty status: 200
    else
      if response.status == 200
        deliver_raw response.body
      else
        deliver_error!(response.status, message: response.body)
      end
    end
  end
end
