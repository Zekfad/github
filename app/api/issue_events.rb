# rubocop:disable Style/FrozenStringLiteralComment
require "scientist"

class Api::IssueEvents < Api::App
  include Api::Issues::EnsureIssuesEnabled

  # Get Events for an Issue
  get "/repositories/:repository_id/issues/:issue_number/events" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#list-issue-events"
    repo = find_repo!
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))

    control_access :list_issue_events_for_issue,
      repo: repo,
      resource: issue,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_issues_enabled_or_pr! repo, issue

    requested_event_types = ::IssueEvent::VALID_EVENTS
    if event_types_to_exclude(repo).include?(:projects)
      requested_event_types -= ::IssueEvent::PROJECT_EVENTS
    end

    if !issue.pull_request?
      requested_event_types -= ::IssueEvent::PULL_REQUEST_EVENTS
    end

    requested_issue_event_type_names = requested_event_types.map do |event_type|
      ::IssueEvent.column_to_platform_type_name(event_type)
    end

    events = Platform::Loaders::Timeline::Placeholders::IssueEvent.load(
      issue.id,
      current_user,
      visible_events_only: false,
      requested_issue_event_type_names: requested_issue_event_type_names,
    ).then { |placeholders|
      pager = paginate_rel(placeholders.sort_by(&:sort_key))
      Promise.all(pager.map { |placeholder| placeholder.async_value }).then do |values|
        pager.replace(values)
      end
    }.sync

    GitHub::PrefillAssociations.for_issue_events(events)
    deliver :issue_event_hash, events
  end

  statsd_tag_actions "/repositories/:repository_id/issues/events"

  # Get Events for a Repository
  get "/repositories/:repository_id/issues/events" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#list-issue-events-for-a-repository"
    control_access :list_issue_events_for_repo, repo: repo = find_repo!, allow_integrations: false, allow_user_via_integration: false
    cap_paginated_entries!

    events = repo.issue_events
    if repo.has_issues?
      events = events.from([Arel.sql("`#{IssueEvent.table_name}` FORCE INDEX (index_issue_events_on_repository_id_event)")])
      events = paginate_rel(events.reorder("issue_events.id DESC"))
    else
      events = events.from([Arel.sql("`#{IssueEvent.table_name}` FORCE INDEX (index_issue_events_on_repo_id_issue_id_and_event)")])
      events = events.pull_requests
      # here we use different join strategies for the count and the page load.
      # for the count, we add an extra restriction to issues so that issues can be used efficiently as driver table
      # while for the page load we continue to use issue_events as the driver table
      relation = events.reorder("issue_events.id DESC").limit(pagination[:per_page]).page(pagination[:page])
      relation.total_entries = events.unscoped.from(
        events.unscope(:order, :select, :from).select("1 as one").
          where("issues.repository_id = issue_events.repository_id").
          limit(pagination[:total_entries])
        ).count
      events = relation
    end
    GitHub.dogstats.time "prefill", tags: ["via:api", "action:issues_events_list"] do
      GitHub::PrefillAssociations.for_issue_events(events)
      deliver :issue_event_hash, events, issues: issue_hash_from(events, repo), filter_project_events: repo.owner.is_a?(User)
    end
  end

  # Get a single Issue Event
  get "/repositories/:repository_id/issues/events/:event_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#get-an-issue-event"
    repo = find_repo!
    event = repo.issue_events.find_by_id(int_id_param!(key: :event_id))
    deliver_error!(404) unless event && event.issue && event.visible_to?(current_user)

    control_access :get_issue_event,
      repo: repo,
      resource: event,
      allow_integrations: true,
      allow_user_via_integration: false

    ensure_issues_enabled_or_pr! repo, event.issue
    GitHub::PrefillAssociations.for_issue_events([event])

    deliver :issue_event_hash, event, issues: issue_hash_from([event], repo), last_modified: calc_last_modified(event)
  end

  private

  # Fetches the Issues for the given events.
  #
  # events - An Array of IssueEvent instances.
  # repo   - An optional Repository instance.
  #
  # Returns a Hash of ID => <Issue>.
  def issue_hash_from(events, repo = events.first.repository)
    issue_ids = events.map { |ev| ev.issue_id }
    issue_ids.uniq!
    issue_ids.compact!
    return {} if issue_ids.blank?

    issues = repo.issues.where(id: issue_ids)
    GitHub::PrefillAssociations.for_issues(issues)
    Reaction::Summary.prefill(issues)

    serialize_options = {
      repo: repo,
      mime_params: medias.api_params,
    }

    if (mimes = request.accept).present?
      serialize_options[:accept_mime_types] = mimes
    end

    issues.inject({}) do |memo, issue|
      memo.update issue.id =>
        Api::Serializer.issue_hash(issue, serialize_options)
    end
  end

  def event_types_to_exclude(repo)
    [].tap do |types|
      types << :projects unless access_allowed?(:list_projects,
        owner: repo,
        organization: repo.organization,
        resource: repo,
        allow_integrations: true,
        allow_user_via_integration: false)
    end
  end
end
