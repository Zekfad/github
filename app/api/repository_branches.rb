# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryBranches < Api::App
  map_to_service :ref, only: [
    "GET /repositories/:repository_id/branches",
    "GET /repositories/:repository_id/branches/*",
    "POST /repositories/:repository_id/merges"
  ]
  map_to_service :branch_protection_rule, only: [
    "GET /repositories/:repository_id/branches/*/protection",
    "PUT /repositories/:repository_id/branches/*/protection",
    "DELETE /repositories/:repository_id/branches/*/protection",
    "GET /repositories/:repository_id/branches/*/push_control",
    "GET /repositories/:repository_id/branches/*/protection/required_status_checks",
    "PATCH /repositories/:repository_id/branches/*/protection/required_status_checks",
    "DELETE /repositories/:repository_id/branches/*/protection/required_status_checks",
    "GET /repositories/:repository_id/branches/*/protection/required_status_checks/contexts",
    "PUT /repositories/:repository_id/branches/*/protection/required_status_checks/contexts",
    "POST /repositories/:repository_id/branches/*/protection/required_status_checks/contexts",
    "DELETE /repositories/:repository_id/branches/*/protection/required_status_checks/contexts",
    "GET /repositories/:repository_id/branches/*/protection/restrictions",
    "DELETE /repositories/:repository_id/branches/*/protection/restrictions",
    "GET /repositories/:repository_id/branches/*/protection/restrictions/users",
    "PUT /repositories/:repository_id/branches/*/protection/restrictions/users",
    "POST /repositories/:repository_id/branches/*/protection/restrictions/users",
    "DELETE /repositories/:repository_id/branches/*/protection/restrictions/users",
    "GET /repositories/:repository_id/branches/*/protection/restrictions/teams",
    "PUT /repositories/:repository_id/branches/*/protection/restrictions/teams",
    "POST /repositories/:repository_id/branches/*/protection/restrictions/teams",
    "DELETE /repositories/:repository_id/branches/*/protection/restrictions/teams",
    "GET /repositories/:repository_id/branches/*/protection/restrictions/apps",
    "PUT /repositories/:repository_id/branches/*/protection/restrictions/apps",
    "POST /repositories/:repository_id/branches/*/protection/restrictions/apps",
    "DELETE /repositories/:repository_id/branches/*/protection/restrictions/apps",
    "GET /repositories/:repository_id/branches/*/protection/required_pull_request_reviews",
    "PATCH /repositories/:repository_id/branches/*/protection/required_pull_request_reviews",
    "DELETE /repositories/:repository_id/branches/*/protection/required_pull_request_reviews",
    "GET /repositories/:repository_id/branches/*/protection/required_signatures",
    "POST /repositories/:repository_id/branches/*/protection/required_signatures",
    "DELETE /repositories/:repository_id/branches/*/protection/required_signatures",
    "GET /repositories/:repository_id/branches/*/protection/enforce_admins",
    "POST /repositories/:repository_id/branches/*/protection/enforce_admins",
    "DELETE /repositories/:repository_id/branches/*/protection/enforce_admins"
]

  # list branches for a repository
  get "/repositories/:repository_id/branches" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-branches"

    control_access :list_branches,
                   resource: repo = find_repo!,
                   allow_integrations: true,
                   allow_user_via_integration: true,
                   approved_integration_required: false


    branches = repo.heads.to_a
    GitHub::PrefillAssociations.for_branches(repo, branches)

    protection_status = parse_bool(params[:protected])

    if protection_status == true
      branches = branches.select(&:protected?)
    elsif protection_status == false
      branches = branches.reject(&:protected?)
    end

    branches = branches.paginate(pagination)

    can_read_branch_protections = if repo.plan_supports?(:protected_branches)
      access_allowed?(:read_branch_protection, {
        resource: repo,
        allow_integrations: true,
        allow_user_via_integration: true,
      })
    else
      false
    end

    if can_read_branch_protections
      deliver :short_branch_with_protection_hash, branches
    else
      deliver :short_branch_hash, branches
    end
  end

  # merge one branch into another
  post "/repositories/:repository_id/merges" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#merge-a-branch"
    repo = find_repo!
    control_access :merge_branch, resource: repo, forbid: repo.public?, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    data = receive_with_schema("merge", "create-legacy")

    base, head, message = data.values_at("base", "head", "commit_message")

    base_ref = repo.heads.find(base)

    deliver_error!(404,
      message: "Base does not exist",
      documentation_url: @documentation_url) unless base_ref

    ensure_changed_files_writable!(repo, GitHub::Diff.new(repo, base, head))

    begin
      merge_commit, error, error_message = base_ref.merge(current_user, head, {
        commit_message: message,
        reflog_data: request_reflog_data("branch merge api"),
      })

      if merge_commit
        deliver(:git_commit_hash, merge_commit, status: 201)
      else
        case error
        when :no_such_head
          deliver_error!(404,
            message: "Head does not exist",
            documentation_url: @documentation_url)
        when :already_merged
          deliver_empty(status: 204)
        when :merge_conflict, :protected_branch_update_error
          deliver_error(409,
            message: error_message,
            documentation_url: @documentation_url)
        else
          deliver_error(500,
            message: "There was a problem performing the merge",
            documentation_url: @documentation_url)
        end
      end
    rescue Git::Ref::HookFailed => e
      deliver_error 422,
      message: "Could not merge because a Git pre-receive hook failed.\n\n#{e.message}",
      documentation_url: @documentation_url
    end
  end

  # Get protection status
  get "/repositories/:repository_id/branches/*/protection" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-branch-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)

    GitHub::PrefillAssociations.for_protected_branches([ref.protected_branch])
    deliver(:protected_branch_hash, ref)
  end

  def extract_required_status_checks(data)
    return if data.nil?

    data.slice("strict", "contexts").symbolize_keys!
  end

  def extract_required_pull_request_reviews(data)
    return if data["required_pull_request_reviews"].nil?

    object = data["required_pull_request_reviews"]

    require_preview(:multiple_required_reviewers) if object.key?("required_approving_review_count")

    keys = ["dismissal_restrictions", "dismiss_stale_reviews", "require_code_owner_reviews", "required_approving_review_count"]

    object.slice(*keys).symbolize_keys!
  end

  def extract_restrictions(data)
    return if data.nil?

    {
      users: data["users"],
      teams: data["teams"],
      integrations: data["apps"],
    }
  end

  # Enable branch protection
  put "/repositories/:repository_id/branches/*/protection" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#update-branch-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists!(ref)

    data = receive_with_schema("protected-branch", "enable-legacy")

    required_signatures = if medias.api_preview?(:required_signatures)
      data["required_signatures"]
    elsif data.include?("required_signatures")
      require_preview(:required_signatures)
    end

    begin
      protected_branch_params = {
        creator: current_user,
        required_status_checks: extract_required_status_checks(data["required_status_checks"]),
        required_pull_request_reviews: extract_required_pull_request_reviews(data),
        required_signatures: required_signatures,
        enforce_admins: data["enforce_admins"],
        restrictions: extract_restrictions(data["restrictions"]),
      }

      protected_branch_params[:required_linear_history] = data["required_linear_history"]
      protected_branch_params[:block_force_pushes] = !data["allow_force_pushes"]
      protected_branch_params[:block_deletions] = !data["allow_deletions"]

      ref.protected_branch = GitHub::SchemaDomain.allowing_cross_domain_transactions do
        repo.protect_branch(ref.name, **protected_branch_params)
      end

      deliver(:protected_branch_hash, ref)
    rescue ProtectedBranch::OnlyOrgsHaveAuthorizedActors
      deliver_error 422,
        errors: ["Only organization repositories can have users and team restrictions"]
    rescue ProtectedBranch::TooManyPermittedActors => e
      deliver_error 422, errors: [e.message]
    rescue ActiveRecord::RecordInvalid => e
      deliver_error 422, errors: e.record.errors
    end
  end

  # Disable branch protection
  delete "/repositories/:repository_id/branches/*/protection" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the protected-branch.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("protected-branch", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-branch-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    ref.protected_branch.destroy if ref.protected_by_exact_rule?

    deliver_empty(status: 204)
  end

  # Can the current user push to a branch?
  get "/repositories/:repository_id/branches/*/push_control" do
    @route_owner = "@github/pe-repos"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    require_preview(:push_control) unless push_control_allowed?

    repo = find_repo!

    control_access :list_branches, resource: repo, allow_integrations: true, allow_user_via_integration: true

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/push_control")
    ref ||= Git::Ref.new(repo, params[:splat].first, nil, "refs/heads/")

    deliver :branch_pushability_hash, ref
  end

  # Get required status check settings
  get "/repositories/:repository_id/branches/*/protection/required_status_checks" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-status-checks-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)
    ensure_required_status_checks_enabled!(ref.protected_branch)

    GitHub::PrefillAssociations.for_protected_branches([ref.protected_branch])
    deliver(:protected_branch_required_status_checks_hash, ref)
  end

  # Update required status check settings
  patch "/repositories/:repository_id/branches/*/protection/required_status_checks" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#update-status-check-potection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_required_status_checks_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "update-required-status-checks-legacy")

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.update_required_status_checks(**extract_required_status_checks(data))
      protected_branch.save!
    end

    GitHub::PrefillAssociations.for_protected_branches([protected_branch])
    deliver(:protected_branch_required_status_checks_hash, ref)
  end

   # Disable required status checks
   delete "/repositories/:repository_id/branches/*/protection/required_status_checks" do
     @route_owner = "@github/pe-repos"

     # Introducing strict validation of the protected-branch.remove-required-status-checks
     # JSON schema would cause breaking changes for integrators
     # skip_validation until a rollout strategy can be determined
     # see: https://github.com/github/ecosystem-api/issues/1555
     receive_with_schema("protected-branch", "remove-required-status-checks", skip_validation: true)

     @documentation_url = "/rest/reference/repos#remove-status-check-protection"
     repo = find_repo!
     ensure_plan_supports_protected_branches!(repo)

     ref = repo.heads.find(params[:splat].first)
     pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks")

     control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
     ensure_user_can_update_branch_protection!(repo, current_user)
     ensure_branch_exists_and_is_protected!(ref)
     ensure_required_status_checks_enabled!(ref.protected_branch)

     with_protected_branch_for_modification(ref) do |protected_branch|
       protected_branch.clear_required_status_checks
       protected_branch.save!
     end

     deliver_empty(status: 204)
   end

  # Get required status check contexts
  get "/repositories/:repository_id/branches/*/protection/required_status_checks/contexts" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-all-status-check-contexts"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks/contexts")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)
    ensure_required_status_checks_enabled!(ref.protected_branch)

    deliver(:raw, ref.protected_branch.required_status_checks.map(&:context))
  end

  # Replace required status check contexts
  put "/repositories/:repository_id/branches/*/protection/required_status_checks/contexts" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#set-status-check-contexts"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks/contexts")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_required_status_checks_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "replace-required-status-checks-contexts-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.replace_status_contexts(data)
      end

      deliver(:raw, protected_branch.required_status_checks.map(&:context))
    rescue ActiveRecord::RecordInvalid => e
      deliver_error 422, errors: e.record.errors
    end
  end

  # Add required status check contexts
  post "/repositories/:repository_id/branches/*/protection/required_status_checks/contexts" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#add-status-check-contexts"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks/contexts")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_required_status_checks_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "add-required-status-checks-contexts-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.create_required_status_checks(data)
      end

      deliver(:raw, protected_branch.required_status_checks.map(&:context))
    rescue ActiveRecord::RecordInvalid => e
      deliver_error 422, errors: e.record.errors
    end
  end

  # Delete required status check contexts
  delete "/repositories/:repository_id/branches/*/protection/required_status_checks/contexts" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#remove-status-check-contexts"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_status_checks/contexts")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_required_status_checks_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "remove-required-status-checks-contexts-legacy")

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.destroy_required_status_checks(data)
    end

    deliver(:raw, protected_branch.required_status_checks.map(&:context))
  end

  # Get push restrictions settings
  get "/repositories/:repository_id/branches/*/protection/restrictions" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    GitHub::PrefillAssociations.for_protected_branches([ref.protected_branch])
    deliver(:protected_branch_restrictions_hash, ref)
  end

  # Disable push restrictions
  delete "/repositories/:repository_id/branches/*/protection/restrictions" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the protected-branch.remove-restrictions
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("protected-branch", "remove-restrictions", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.clear_restrictions
      protected_branch.save!
    end

    deliver_empty(status: 204)
  end

  # Get list of users restricted to pushing
  get "/repositories/:repository_id/branches/*/protection/restrictions/users" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-users-with-access-to-the-protected-branch"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/users")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    deliver(:simple_user_hash, ref.protected_branch.authorized_users)
  end

  # Replace list of users restricted to pushing
  put "/repositories/:repository_id/branches/*/protection/restrictions/users" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#set-user-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/users")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "replace-user-restrictions-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.update_restrictions(users: data)
      end

      deliver(:simple_user_hash, protected_branch.authorized_users)
    rescue ProtectedBranch::TooManyPermittedActors => err
      deliver_error 422, errors: [err.message]
    end
  end

  # Add list of users restricted to pushing
  post "/repositories/:repository_id/branches/*/protection/restrictions/users" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#add-user-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/users")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "add-user-restrictions-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.add_users_to_restrictions(data)
      end

      deliver(:simple_user_hash, protected_branch.authorized_users)
    rescue ProtectedBranch::TooManyPermittedActors => err
      deliver_error 422, errors: [err.message]
    end
  end

  # Remove list of users restricted to pushing
  delete "/repositories/:repository_id/branches/*/protection/restrictions/users" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#remove-user-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/users")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "remove-user-restrictions-legacy")

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.remove_users_from_restrictions(data)
    end

    deliver(:simple_user_hash, protected_branch.authorized_users)
  end

  # Get list of teams restricted to pushing
  get "/repositories/:repository_id/branches/*/protection/restrictions/teams" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-teams-with-access-to-the-protected-branch"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/teams")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    deliver(:team_hash, ref.protected_branch.authorized_teams_with_preloaded_org)
  end

  # Replace list of teams restricted to pushing
  put "/repositories/:repository_id/branches/*/protection/restrictions/teams" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#set-team-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/teams")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "replace-team-restrictions-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.update_restrictions(teams: data)
      end

      deliver(:team_hash, protected_branch.authorized_teams_with_preloaded_org)
    rescue ProtectedBranch::TooManyPermittedActors => err
      deliver_error 422, errors: [err.message]
    end
  end

  # Add list of teams restricted to pushing
  post "/repositories/:repository_id/branches/*/protection/restrictions/teams" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#add-team-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/teams")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "add-team-restrictions-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.add_teams_to_restrictions(data)
      end

      deliver(:team_hash, protected_branch.authorized_teams_with_preloaded_org)
    rescue ProtectedBranch::TooManyPermittedActors => err
      deliver_error 422, errors: [err.message]
    end
  end

  # Remove list of teams restricted to pushing
  delete "/repositories/:repository_id/branches/*/protection/restrictions/teams" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#remove-team-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/teams")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "remove-team-restrictions-legacy")

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.remove_teams_from_restrictions(data)
    end

    deliver(:team_hash, protected_branch.authorized_teams_with_preloaded_org)
  end

  # Get list of apps restricted to pushing
  get "/repositories/:repository_id/branches/*/protection/restrictions/apps" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-apps-with-access-to-the-protected-branch"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/apps")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    integrations = ref.protected_branch.authorized_integrations
    GitHub::PrefillAssociations.for_integrations(integrations)

    deliver(:integration_hash, integrations)
  end

  # Replace list of apps restricted to pushing
  put "/repositories/:repository_id/branches/*/protection/restrictions/apps" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#set-app-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/apps")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "replace-app-restrictions-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.update_restrictions(integrations: data)
      end

      integrations = protected_branch.authorized_integrations
      GitHub::PrefillAssociations.for_integrations(integrations)

      deliver(:integration_hash, integrations)
    rescue ProtectedBranch::TooManyPermittedActors => err
      deliver_error 422, errors: [err.message]
    end
  end

  # Add list of apps restricted to pushing
  post "/repositories/:repository_id/branches/*/protection/restrictions/apps" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#add-app-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/apps")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "add-app-restrictions-legacy")

    begin
      protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
        protected_branch.add_integrations_to_restrictions(data)
      end

      integrations = protected_branch.authorized_integrations
      GitHub::PrefillAssociations.for_integrations(integrations)

      deliver(:integration_hash, integrations)
    rescue ProtectedBranch::TooManyPermittedActors => err
      deliver_error 422, errors: [err.message]
    end
  end

  # Remove list of apps restricted to pushing
  delete "/repositories/:repository_id/branches/*/protection/restrictions/apps" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#remove-app-access-restrictions"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/restrictions/apps")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)
    ensure_push_restrictions_enabled!(ref.protected_branch)

    data = receive_with_schema("protected-branch", "remove-app-restrictions-legacy")

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.remove_integrations_from_restrictions(data)
    end

    integrations = protected_branch.authorized_integrations
    GitHub::PrefillAssociations.for_integrations(integrations)

    deliver(:integration_hash, integrations)
  end

  # Get pull request review enforcement settings
  get "/repositories/:repository_id/branches/*/protection/required_pull_request_reviews" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-pull-request-review-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_pull_request_reviews")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)

    GitHub::PrefillAssociations.for_protected_branches([ref.protected_branch])
    deliver(:protected_branch_pull_request_reviews_hash, ref)
  end

  # Update pull request review enforcement settings
  patch "/repositories/:repository_id/branches/*/protection/required_pull_request_reviews" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#update-pull-request-review-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_pull_request_reviews")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    data = receive_with_schema("protected-branch", "update-pull-request-review-enforcement-legacy")

    deliver_error!(422, message: "Dismissal restrictions are supported only for repositories owned by an organization.") if data["dismissal_restrictions"] && !repo.in_organization?

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.enable_required_pull_request_reviews(
        dismiss_stale_reviews: data["dismiss_stale_reviews"],
        require_code_owner_reviews: data["require_code_owner_reviews"],
        required_approving_review_count: medias.api_preview?(:multiple_required_reviewers) ? data["required_approving_review_count"] : 1,
        dismissal_restrictions: data["dismissal_restrictions"],
      )
    end

    GitHub::PrefillAssociations.for_protected_branches([protected_branch])
    deliver(:protected_branch_pull_request_reviews_hash, ref)
  end

  # Remove pull request review enforcement settings
  delete "/repositories/:repository_id/branches/*/protection/required_pull_request_reviews" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the protected-branch.remove-pr-review-enforcement
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("protected-branch", "remove-pr-review-enforcement", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-pull-request-review-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_pull_request_reviews")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.clear_required_pull_request_reviews
      protected_branch.save
    end

    GitHub::PrefillAssociations.for_protected_branches([protected_branch])

    deliver_empty(status: 204)
  end

  # Get required signatures setting
  get "/repositories/:repository_id/branches/*/protection/required_signatures" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-commit-signature-protection"
    require_preview(:required_signatures)

    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_signatures")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)

    GitHub::PrefillAssociations.for_protected_branches([ref.protected_branch])
    deliver(:protected_branch_required_signatures_hash, ref)
  end

  # Enable required signatures setting
  post "/repositories/:repository_id/branches/*/protection/required_signatures" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-commit-signature-protection"
    require_preview(:required_signatures)

    receive_with_schema("protected-branch", "enable-required-signatures")

    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_signatures")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.enable_required_signatures

      unless protected_branch.save
        deliver_error! 422, errors: protected_branch.errors
      end
    end

    GitHub::PrefillAssociations.for_protected_branches([protected_branch])
    deliver(:protected_branch_required_signatures_hash, ref)
  end

  # Disable required signatures setting
  delete "/repositories/:repository_id/branches/*/protection/required_signatures" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#delete-commit-signature-protection"
    require_preview(:required_signatures)

    # Introducing strict validation of the protected-branch.remove-required-signatures
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("protected-branch", "remove-required-signatures", skip_validation: true)

    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/required_signatures")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    with_protected_branch_for_modification(ref) do |protected_branch|
      protected_branch.clear_required_signatures

      unless protected_branch.save
        deliver_error! 422, errors: protected_branch.errors
      end
    end

    deliver_empty(status: 204)
  end

  # Get admin enforcement settings
  get "/repositories/:repository_id/branches/*/protection/enforce_admins" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-admin-branch-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/enforce_admins")

    control_access :read_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_branch_exists_and_is_protected!(ref)

    GitHub::PrefillAssociations.for_protected_branches([ref.protected_branch])
    deliver(:protected_branch_admin_enforced_hash, ref)
  end

  # Update admin enforcement settings
  post "/repositories/:repository_id/branches/*/protection/enforce_admins" do
    @route_owner = "@github/pe-repos"
    receive_with_schema("protected-branch", "enable-admin-enforcement")

    @documentation_url = "/rest/reference/repos#set-admin-branch-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/enforce_admins")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    protected_branch = with_protected_branch_for_modification(ref) do |protected_branch|
      unless protected_branch.update(admin_enforced: true)
        deliver_error! 422, errors: protected_branch.errors
      end
    end

    GitHub::PrefillAssociations.for_protected_branches([protected_branch])
    deliver(:protected_branch_admin_enforced_hash, ref)
  end

  # Remove admin enforcement settings
  delete "/repositories/:repository_id/branches/*/protection/enforce_admins" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the protected-branch.remove-admin-enforcement
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("protected-branch", "remove-admin-enforcement", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-admin-branch-protection"
    repo = find_repo!
    ensure_plan_supports_protected_branches!(repo)

    ref = repo.heads.find(params[:splat].first)
    pass if ref.nil? && ref_with_suffix_exists?(repo, suffix: "/protection/enforce_admins")

    control_access :update_branch_protection, resource: repo, allow_integrations: true, allow_user_via_integration: true
    ensure_user_can_update_branch_protection!(repo, current_user)
    ensure_branch_exists_and_is_protected!(ref)

    with_protected_branch_for_modification(ref) do |protected_branch|
      unless protected_branch.update(admin_enforced: false)
        deliver_error! 422, errors: protected_branch.errors
      end
    end

    deliver_empty(status: 204)
  end

  # get a single branch and the commit it points to
  # uses splat route to accommodate branches with slashes
  get "/repositories/:repository_id/branches/*" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-a-branch"
    control_access :list_branches, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    ref = repo.heads.find(params[:splat].first)

    if ref && ref.exist?
      deliver(:branch_with_protection_hash, ref)
    else
      deliver_error(404,
        message: "Branch not found",
        documentation_url: @documentation_url)
    end
  end

  private

  def ref_with_suffix_exists?(repo, suffix:)
    repo.heads.find("#{params[:splat].first}#{suffix}")
  end

  # Execute the given block with a ProtectedBranch instance that is
  # suitable for modification.
  #
  # This method is supposed to help bridging compatibility between
  # "legacy" branch protections and "new-style" branch protection rules.
  #
  # When given a `Ref` for a branch for which to update protection settings,
  # this method checks whether protection is currently provided by a
  # wildcard rule or a branch-specific rule. For wildcard rules, the rule will
  # be copied to a branch-specific rule with all associated settings
  # in place, and all further protection setting modifications will be
  # performed on this newly created rule. For exact rules, the existing
  # rule will be modified directly.
  #
  # This ensures that all modifications made via the branch protections API are
  # always scoped to just a single branch, and that even partial modifications
  # end up with the same protection settings.
  #
  # ref - a `Ref` for the branch for which the protection should be changed.
  #
  # Returns the modified ProtectedBranch.
  def with_protected_branch_for_modification(ref)
    raise ArgumentError.new("The ref must have a protected branch") unless ref.protected?

    ProtectedBranch.transaction do
      Ability.transaction do
        protected_branch = if ref.protected_by_exact_rule?
          ref.protected_branch
        else
          ref.protected_branch = ref.protected_branch.deep_copy_as!(name: ref.name, creator: current_user)
        end

        yield protected_branch

        protected_branch
      end
    end
  end

  def ensure_branch_exists!(ref)
    if ref.nil? || !ref.exists?
      deliver_error!(404, message: "Branch not found")
    end
  end

  def ensure_branch_exists_and_is_protected!(ref)
    ensure_branch_exists!(ref)

    if ref.protected_branch.nil?
      deliver_error!(404, message: "Branch not protected")
    end
  end

  def ensure_required_status_checks_enabled!(protected_branch)
    unless protected_branch.required_status_checks_enabled?
      deliver_error! 404, message: "Required status checks not enabled"
    end
  end

  def ensure_push_restrictions_enabled!(protected_branch)
    unless protected_branch.has_authorized_actors?
      deliver_error! 404, message: "Push restrictions not enabled"
    end
  end

  def ensure_plan_supports_protected_branches!(repo)
    unless repo.plan_supports?(:protected_branches)
      deliver_error!(403, message: "Upgrade to GitHub Pro or make this repository public to enable this feature.")
    end
  end

  def ensure_user_can_update_branch_protection!(repo, user)
    unless repo.can_update_protected_branches?(user)
      deliver_error!(403, message: "Protected branch updating is disabled on this repository.")
    end
  end

  def ensure_required_pull_request_reviews_enabled!(protected_branch)
    unless protected_branch.pull_request_reviews_enabled?
      deliver_error! 404, message: "Pull request review enforcement not enabled"
    end
  end

  def push_control_allowed?
    medias.api_version?(REST::Previews.get(:push_control).media_version)
  end
end
