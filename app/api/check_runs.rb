# frozen_string_literal: true

class Api::CheckRuns < Api::App

  statsd_tag_actions "/repositories/:repository_id/check-runs"
  statsd_tag_actions "/repositories/:repository_id/check-runs/:check_run_id"

  class SilentTruncationError < RuntimeError
    def needs_redacting?
      true
    end
  end

  # List check runs for a specific commit
  get "/repositories/:repository_id/commits/*/check-runs" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#list-check-runs-for-a-git-reference"
    require_preview(:checks)

    repo = find_repo!

    control_access :read_check_run,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    ref = params[:splat].first
    commit = find_commit!(repo, ref, @documentation_url)
    sha = commit.sha
    return deliver_error 404 if sha.blank?

    check_runs = CheckRun.order("id DESC")

    allowed_filter_params = ["latest", "all"]
    filter = params[:filter]
    filter = "latest" unless allowed_filter_params.include?(filter)

    if filter == "latest"
      check_runs = check_runs.latest_for_sha_and_repository_id(sha, repo.id)
    else
      check_runs = check_runs.for_sha_and_repository_id(sha, repo.id)
    end
    if id = params[:app_id].presence
      check_runs = check_runs.for_app_id(id)
    end
    if name = params[:check_name].presence
      check_runs = check_runs.where(name: name).or(check_runs.where(display_name: name))
    end
    if status = params[:status].presence
      check_runs = check_runs.where(status: CheckRun.statuses[status])
    end
    check_runs = paginate_rel(check_runs)

    GitHub::PrefillAssociations.for_check_runs(check_runs)
    deliver :check_runs_hash, { check_runs: check_runs, total_count: check_runs.total_entries }
  end

  # List check runs in a check suite
  get "/repositories/:repository_id/check-suites/:check_suite_id/check-runs" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#list-check-runs-in-a-check-suite"
    require_preview(:checks)

    repo = find_repo!

    control_access :read_check_run,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    check_suite = CheckSuite.find_by(id: params[:check_suite_id])
    record_or_404(check_suite)
    deliver_error! 404 if repo.id != check_suite.repository_id

    allowed_filter_params = ["latest", "all"]
    filter = params[:filter]
    filter = "latest" unless allowed_filter_params.include?(filter)

    if filter == "latest"
      check_runs = check_suite.latest_check_runs
    end

    if filter == "all"
      check_runs = check_suite.check_runs.order("id DESC")
    end

    if name = params[:check_name].presence
      check_runs = check_runs.where(name: name).or(check_runs.where(display_name: name))
    end
    if status = params[:status].presence
      check_runs = check_runs.where(status: CheckRun.statuses[status])
    end
    check_runs = paginate_rel(check_runs)

    GitHub::PrefillAssociations.for_check_runs(check_runs)
    deliver :check_runs_hash, { check_runs: check_runs, total_count: check_runs.total_entries }, compare_payload_to: :check_runs_candidate
  end

  CheckRunsQuery = PlatformClient.parse  <<~'GRAPHQL'
    query($checkSuiteId: ID!, $limit: Int!, $numericPage: Int, $checkType: CheckRunType, $checkName: String, $status: CheckStatusState) {
      node(id: $checkSuiteId) {
        ... on CheckSuite {
          checkRuns(first: $limit, numericPage: $numericPage, filterBy: { checkType: $checkType, checkName: $checkName, status: $status }) {
            totalCount
            edges {
              node {
                ...Api::Serializer::ChecksDependency::CheckRunFragment
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def check_runs_candidate(check_runs, _ = {})
    repo = find_repo!
    check_suite = CheckSuite.find_by(id: params[:check_suite_id], repository_id: repo.id)

    filter = params[:filter].blank? ? params[:filter] : params[:filter].upcase

    status = params[:status].blank? ? params[:status] : params[:status].upcase

    variables = {
      "checkSuiteId" => check_suite.global_relay_id,
      "limit" => per_page,
      "numericPage" => pagination[:page],
      "checkType" => filter,
      "checkName" => params[:check_name],
      "status" => status,
    }

    results = platform_execute(CheckRunsQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error!({
        errors: results.errors.all,
        resource: "CheckRun",
        documentation_url: @documentation_url,
      })
    end

    graphql_check_runs = results.data.node.check_runs

    paginator.collection_size = graphql_check_runs.total_count
    Api::Serializer.serialize(:graphql_check_runs_hash, check_runs: graphql_check_runs.edges.map(&:node), total_count: graphql_check_runs.total_count)
  end

  # Get a check run.
  get "/repositories/:repository_id/check-runs/:check_run_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#get-a-check-run"
    require_preview(:checks)

    repo = find_repo!
    check_run = CheckRun.includes(:check_suite).where(id: params[:check_run_id]).first
    record_or_404(check_run)

    if repo.id != check_run.check_suite.repository_id
      deliver_error! 404
    end

    control_access :read_check_run,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    GitHub::PrefillAssociations.for_check_runs([check_run])
    deliver :check_run_hash, check_run, compare_payload_to: :check_run_candidate
  end

  CheckRunQuery = PlatformClient.parse  <<~'GRAPHQL'
    query($checkRunId: ID!) {
      node(id: $checkRunId) {
        ... on CheckRun {
          ...Api::Serializer::ChecksDependency::CheckRunFragment
        }
      }
    }
  GRAPHQL

  def check_run_candidate(check_run, _ = {})
    variables = {
      "checkRunId" => check_run.global_relay_id,
    }

    results = platform_execute(CheckRunQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error!({
        errors: results.errors.all,
        resource: "CheckRun",
        documentation_url: @documentation_url,
      })
    end

    graphql_check_run = results.data.node

    Api::Serializer.serialize(:graphql_check_run_hash, graphql_check_run)
  end

  # Create a check run for a specific commit.
  post "/repositories/:repository_id/check-runs" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#create-a-check-run"
    require_preview(:checks)

    repo = find_repo!
    # Return forbidden message about authentication, when the user could otherwise admin the repo
    if can_access_repo?(repo)
      set_forbidden_message "You must authenticate via a GitHub App.".freeze
    end

    control_access :write_check_run, resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    data = receive_with_schema("check-run", "create-legacy")

    data["head_sha"] = find_commit!(repo, data["head_sha"], @documentation_url).oid
    data["status"] = "completed" if data["conclusion"]

    check_run_attrs = [
      :status, :conclusion, :details_url,
      :started_at, :completed_at, :external_id, :name
    ]
    output_attrs = [:title, :summary, :text]

    check_run_data = attr(data, *check_run_attrs)
    check_run_data = check_run_data.merge(attr(data["output"] || {}, *output_attrs))
    truncator = CheckRun::Truncator.new(check_run_data["text"])
    check_run_data["text"] = truncator.truncate

    check_run = CheckRun.new check_run_data

    image_attrs = [:alt, :image_url, :caption]
    images_data_source = (data["output"] || {})["images"] || []
    images_data_source.each do |image|
      image_data = attr(image, *image_attrs)
      check_run.images << image_data
    end

    annotation_attrs = [
      :path, :annotation_level, :message,
      :start_line, :end_line, :start_column, :end_column,
      :raw_details, :title
    ]
    annotations_data_source = (data["output"] || {})["annotations"] || []
    annotations_data_source.each do |annotation|
      annotation_data = attr(annotation, *annotation_attrs)
      check_run.annotations.build annotation_data
    end

    check_run.actions = prepare_actions(data["actions"])

    check_suite = find_or_create_check_suite(head_sha: data["head_sha"], repo: repo)
    check_run.check_suite_id = check_suite.id

    if check_run.save
      if truncator.truncated?
        truncator.report_silent_truncation!(
          check_run: check_run,
          repo: repo,
          integration: current_integration,
          type: "a new",
        )
      end
      GitHub::PrefillAssociations.for_check_runs([check_run])
      deliver :check_run_hash, check_run, status: 201
    else
      if truncator.truncated?
        truncator.report_silent_truncation!(
          check_run: check_run,
          repo: repo,
          integration: current_integration,
          type: "a new",
        )
      end
      deliver_error 422,
        errors: check_run.errors,
        documentation_url: @documentation_url
    end
  end

  # Update a check run
  patch "/repositories/:repository_id/check-runs/:check_run_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#update-a-check-run"
    require_preview(:checks)

    repo = find_repo!
    # Return forbidden message about authentication, when the user could otherwise admin the repo
    if can_access_repo?(repo)
      set_forbidden_message "You must authenticate via a GitHub App.".freeze
    end

    control_access :write_check_run,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    check_run = CheckRun.includes(:check_suite).where(id: params[:check_run_id]).first
    record_or_404(check_run)
    if repo.id != check_run.check_suite.repository_id
      deliver_error! 404
    end

    if current_integration&.id != check_run.check_suite.github_app_id
      deliver_error!(403, message: "Invalid app_id `#{check_run.check_suite.github_app_id}` - check run can only be modified by the GitHub App that created it.")
    end

    data = receive_with_schema("check-run", "update-legacy")

    check_run.name         = data["name"] if data["name"]
    check_run.details_url  = data["details_url"] if data["details_url"]
    check_run.conclusion   = data["conclusion"] if data["conclusion"]
    check_run.started_at   = data["started_at"] if data["started_at"]
    check_run.completed_at = data["completed_at"] if data["completed_at"]
    check_run.external_id  = data["external_id"] if data["external_id"]

    if data["conclusion"]
      check_run.status = "completed"
    elsif data["status"]
      check_run.status = data["status"]
    end

    output_attrs = [:title, :summary, :images, :text, :annotations]
    output_data  = attr(attr(data, :output)["output"], *output_attrs) || {}

    check_run.title   = output_data["title"] if output_data["title"]
    check_run.summary = output_data["summary"] if output_data["summary"]

    truncator = CheckRun::Truncator.new(output_data["text"])
    check_run.text = truncator.truncate
    if truncator.truncated?
      truncator.report_silent_truncation!(
        check_run: check_run,
        repo: repo,
        integration: current_integration,
        type: "an existing",
      )
    end

    image_attrs = [:alt, :image_url, :caption]
    images_data_source = output_data["images"] || []
    images_data_source.each do |image|
      image_data = attr(image, *image_attrs)
      check_run.images << image_data
    end

    annotation_attrs = [
      :path, :annotation_level, :message,
      :start_line, :end_line, :start_column, :end_column,
      :raw_details, :title
    ]

    annotations_data_source = output_data["annotations"] || []
    annotations_data_source.each do |annotation|
      annotation_data = attr(annotation, *annotation_attrs)
      check_run.annotations.build annotation_data
    end

    check_run.actions = prepare_actions(data["actions"])

    if check_run.save
      GitHub::PrefillAssociations.for_check_runs([check_run])
      deliver :check_run_hash, check_run
    else
      deliver_error 422,
        errors: check_run.errors,
        documentation_url: @documentation_url
    end
  end

  get "/repositories/:repository_id/check-runs/:check_run_id/annotations" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#list-check-run-annotations"
    require_preview(:checks)

    repo = find_repo!
    check_run = CheckRun.includes(:check_suite).where(id: params[:check_run_id]).first
    record_or_404(check_run)

    # TODO: denormalize repository ID or optimize this lookup
    if repo.id != check_run.check_suite.repository_id
      deliver_error! 404
    end

    control_access :read_check_run,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    GitHub::PrefillAssociations.for_check_runs([check_run])

    annotations = paginate_rel(check_run.annotations)
    deliver :check_annotation_hash, annotations, compare_payload_to: :check_annotation_candidate
  end

  CheckAnnotationQuery = PlatformClient.parse  <<~'GRAPHQL'
    query($checkRunId: ID!, $limit: Int!, $numericPage: Int) {
      node(id: $checkRunId) {
        ... on CheckRun {
          annotations(first: $limit, numericPage: $numericPage) {
            totalCount
            edges {
              node {
                ...Api::Serializer::ChecksDependency::CheckAnnotationFragment
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def check_annotation_candidate(annotations, _ = {})
    check_run = CheckRun.includes(:check_suite).where(id: params[:check_run_id]).first

    variables = {
      "checkRunId" => check_run.global_relay_id,
      "limit" => per_page,
      "numericPage" => pagination[:page],
    }

    results = platform_execute(CheckAnnotationQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error!({
        errors: results.errors.all,
        resource: "CheckAnnotation",
        documentation_url: @documentation_url,
      })
    end

    graphql_check_annotations = results.data.node.annotations

    Api::Serializer.serialize(:graphql_check_annotations_hash, { check_annotations: graphql_check_annotations.edges.map(&:node) })
  end

  private

  # Can they see that the repo exists?
  def can_access_repo?(repo)
    return true if repo.public?
    repo.pullable_by?(current_user) && scope?(current_user, "repo")
  end

  def find_or_create_check_suite(head_sha:, repo:)
    check_suite = Checks::Service.find_or_create_check_suite(
      github_app_id: current_integration.id,
      head_sha: head_sha,
      repo: repo,
    )
    return check_suite if check_suite.valid?

    deliver_error! 422,
      errors: check_suite.errors,
      documentation_url: @documentation_url
  end

  def prepare_actions(data)
    data ||= []
    data.map do |action|
      CheckRunAction.new(action["label"], action["identifier"], action["description"])
    end
  end
end
