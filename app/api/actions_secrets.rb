# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client/credz"
require "github/launch_client"

class Api::ActionsSecrets < Api::App
  include Api::App::EarthsmokeKeyHelper
  include Api::App::GrpcHelpers
  include GitHub::LaunchClient

  map_to_service :actions_experience

  # For Local development, you need bin/server running and github/launch running (credz service)

  # Get public key for encrypting secrets
  get "/repositories/:repository_id/actions/secrets/public-key" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-repository-public-key"

    repo = find_repo!

    control_access :read_actions_secrets,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    public_key = if GitHub.enterprise?
      { key_identifier: 1, key: GitHub.actions_secrets_public_key }
    else
      generate_earthsmoke_key_payload(name: Platform::EarthsmokeKeys::CUSTOM_TASKS, scope: repo.global_relay_id, count: 1).first
    end

    payload = { key_id: public_key[:key_identifier], key: public_key[:key] }

    deliver_raw(payload)
  end

  # Get the name and timestamps of all secrets set for the Repository
  get "/repositories/:repository_id/actions/secrets" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#list-repository-secrets"

    repo = find_repo!

    control_access :read_actions_secrets,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    result = rescue_from_grpc_errors("Secrets") do
      Credz.list_credentials(app: GitHub.launch_github_app, owner: repo, actor: current_user)
    end

    validate_listing!(result)

    # We map to an Array so pagination works
    secrets = paginate_rel(result.credentials.map { |cred| cred })

    deliver :actions_secrets_hash, { secrets: secrets, total_count: secrets.total_entries }
  end

  # Get a single secret
  get "/repositories/:repository_id/actions/secrets/:name" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-repository-secret"

    repo = find_repo!

    control_access :read_actions_secrets,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    result = rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(app: GitHub.launch_github_app, owner: repo, actor: current_user, key: params[:name])
    end

    deliver_error! 404 unless result

    deliver :actions_secret_hash, result.credential
  end

  # Delete a secret
  delete "/repositories/:repository_id/actions/secrets/:name" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#delete-a-repository-secret"

    repo = find_repo!

    control_access :write_actions_secrets,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    receive_with_schema("actions-secret", "delete-secret")

    result = rescue_from_grpc_errors("Secrets") do
      Credz.delete_credential(app: GitHub.launch_github_app, owner: repo, actor: current_user, key: params[:name])
    end

    deliver_error! 404 unless result

    deliver_empty status: (result.success ? 204 : 404)
  end

   # Store a secret for the given App on repository for a write user.
  put "/repositories/:repository_id/actions/secrets/:name" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#create-or-update-a-repository-secret"

    repo = find_repo!

    control_access :write_actions_secrets,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    data = receive_with_schema("actions-secret", "set-secret")
    value = data["encrypted_value"]
    key_identifier = data["key_id"]

    validation = Credz.validate_secret(params[:name], value)
    unless validation.succeeded?
      deliver_error! 422, message: validation.error, documentation_url: @documentation_url
    end

    value = if GitHub.enterprise?
      decrypt_enterprise_value(value)
    else
      pack_earthsmoke_value(key_identifier, value)
    end

    encoded_value = Base64.strict_encode64(value)

    # result is a GitHub::Launch::Services::Credz::StoreResponse
    result = rescue_from_grpc_errors("Secrets") do
      Credz.store_credential \
        app:   GitHub.launch_github_app,
        owner:  repo,
        actor: current_user,
        key:   params[:name],
        value: encoded_value
    end

    validate_result!(result)
    validate_storage!(result)

    status = 201
    if result.updated
      status = 204
    end

    deliver_empty(status: status)
  end

  private

  def validate_result!(result)
    unless result
      Failbot.report(StandardError.new("no response from store_credential"), launch_credz: GitHub.launch_credz)
      deliver_error!(503, message: "Secrets unavailable. Please try again later.")
    end
  end

  def validate_storage!(result)
    unless result.stored
      Failbot.report(StandardError.new("store_credential did not store"), launch_credz: GitHub.launch_credz)
      deliver_error!(500, message: "Secret was not stored")
    end
  end

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_credz: GitHub.launch_credz)
      deliver_error!(503, message: "Secrets unavailable. Please try again later.")
    end
  end

  def pack_earthsmoke_value(key_identifier, value)
    begin
      value = Base64.strict_decode64(value)
    rescue ArgumentError
      deliver_error! 422,
        message: "Provided value is not a valid base64 value.",
        documentation_url: @documentation_url
    end

    begin
      # validate that the public key provided is truly the latest
      key = GitHub.earthsmoke.low_level_key(Platform::EarthsmokeKeys::CUSTOM_TASKS)
      version = key.export.key_versions.first
      earthsmoke_latest_key_identifier = version.id

      unless key_identifier == earthsmoke_latest_key_identifier.to_s
        deliver_error! 422,
          message: "Provided key `#{key_identifier}` is not the latest version available. Call `GET /actions/secrets/public-key` and resign the data using the latest key.",
          documentation_url: @documentation_url
      end

      Earthsmoke::Embedding.embed(earthsmoke_latest_key_identifier, value)
    rescue ::Earthsmoke::UnavailableError, ::Earthsmoke::TimeoutError
      deliver_error! 503,
        message: "Signing and storage unavailable. Please try again in a few minutes.",
        documentation_url: @documentation_url
    end
  end

  def decrypt_enterprise_value(value)
    box = RbNaCl::Boxes::Sealed.from_private_key(Base64.decode64(GitHub.actions_secrets_private_key))
    box.decrypt(Base64.strict_decode64(value))
  end
end
