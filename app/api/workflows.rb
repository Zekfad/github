# frozen_string_literal: true

class Api::Workflows < Api::App
  map_to_service :actions_experience

  # Get all workflows for a repository.
  get "/repositories/:repository_id/actions/workflows" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-repository-workflows"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    workflows = paginate_rel(repo.workflows.not_deleted)

    deliver :workflows_hash, { workflows: workflows, total_count: workflows.total_entries }
  end

  # Get a workflow.
  get "/repositories/:repository_id/actions/workflows/:workflow_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-workflow"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    workflow = find_workflow!(repo)
    deliver :workflow_hash, workflow
  end

  # Get the billable information for a workflow
  get "/repositories/:repository_id/actions/workflows/:workflow_id/timing" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-workflow-usage"

    deliver_error!(404) if GitHub.enterprise?

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    workflow = find_workflow!(repo)
    owner = repo.owner
    starting_at = owner.first_day_in_metered_cycle.beginning_of_day
    ending_at = owner.first_day_in_next_metered_cycle.beginning_of_day

    lines = workflow.billing_usage_line_items(starting_at, ending_at)
    environments = lines.map { |line| line.job_runtime_environment }.uniq
    payload = {
      billable: {}
    }
    environments.each do |environment|
      payload[:billable][environment] = billing_timing_for_environment(lines, environment)
    end

    deliver_raw(payload)
  end

  # Trigger a workflow_dispatch event
  post "/repositories/:repository_id/actions/workflows/:workflow_id/dispatches" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#create-a-workflow-dispatch-event"

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow = find_workflow!(repo)
    data = receive_with_schema("workflow", "dispatch")
    ref = find_ref!(repo, data["ref"])

    parsed_workflow = Actions::ParsedWorkflow.parse_from_yaml(repo, workflow.path, ref.qualified_name)

    deliver_error! 422, message: "Workflow does not have 'workflow_dispatch' trigger" unless parsed_workflow&.has_workflow_dispatch_trigger?

    provided_inputs = data["inputs"]
    if provided_inputs && provided_inputs.to_json.length > MYSQL_TEXT_FIELD_LIMIT
      deliver_error! 422, message: "inputs are too large."
    end

    begin
      inputs = parsed_workflow.process_inputs(provided_inputs)
    rescue ArgumentError => e
      deliver_error! 422, message: e.message
    end

    repo.dispatch_workflow_event(
      current_user.id,
      workflow.path,
      ref.qualified_name,
      inputs
    )

    deliver_empty(status: 204)
  end

  # Enable a workflow
  put "/repositories/:repository_id/actions/workflows/:workflow_id/enable" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error!(404) unless GitHub.flipper[:actions_disabled_workflows_api].enabled?(current_integration || current_user)

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow = find_workflow!(repo)
    begin
      workflow.enable(current_user)
      deliver_empty(status: 204)
    rescue Actions::Workflow::CannotBeEnabledError
      deliver_error!(403, message: "Unable to enable a workflow that is not active.")
    end
  end

  # Disable a workflow
  put "/repositories/:repository_id/actions/workflows/:workflow_id/disable" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error!(404) unless GitHub.flipper[:actions_disabled_workflows_api].enabled?(current_integration || current_user)

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow = find_workflow!(repo)
    begin
      workflow.disable(current_user)
      deliver_empty(status: 204)
    rescue Actions::Workflow::NotActiveError
      deliver_error!(403, message: "Unable to disable a workflow that is not active.")
    end
  end

  private

  def billing_timing_for_environment(lines, environment)
    env_lines = lines.select { |line| line.job_runtime_environment == environment }
    milliseconds = env_lines.sum do |line_item|
      line_item.duration_in_milliseconds
    end
    { total_ms: milliseconds }
  end

  def find_ref!(repo, ref)
    repo_ref = repo.refs.find(ref)
    deliver_error! 422, message: "No ref found for: #{ref}" unless repo_ref

    repo_ref
  rescue GitRPC::InvalidObject, GitRPC::ObjectMissing
    deliver_error! 422, message: "No ref found for: #{ref}"
  end

  def find_workflow!(repo)
    workflow = repo.workflows.find_from_id_or_filename(params[:workflow_id])
    deliver_error!(404) unless workflow

    workflow
  end
end
