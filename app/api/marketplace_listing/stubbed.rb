# frozen_string_literal: true

class Api::MarketplaceListing::Stubbed < Api::App
  areas_of_responsibility :api, :marketplace
  include Api::App::MarketplaceListingHelpers

  get "/marketplace_listing/stubbed/plans" do
    @route_owner = "@github/marketplace-eng"
    @documentation_url = "/rest/reference/apps#list-plans-stubbed"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    deliver_raw Marketplace::StubbedResponse.plans
  end

  get "/marketplace_listing/stubbed/accounts/:account_id" do
    @route_owner = "@github/marketplace-eng"
    @documentation_url = "/rest/reference/apps#get-a-subscription-plan-for-an-account-stubbed"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    deliver_raw Marketplace::StubbedResponse.organization_subscription_item
  end

  get "/marketplace_listing/stubbed/plans/:plan_id/accounts" do
    @route_owner = "@github/marketplace-eng"
    @documentation_url = "/rest/reference/apps#list-accounts-for-a-plan-stubbed"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    deliver_raw Marketplace::StubbedResponse.subscription_items
  end
end
