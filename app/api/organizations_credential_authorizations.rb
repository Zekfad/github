# frozen_string_literal: true

class Api::OrganizationsCredentialAuthorizations < Api::App

  # GET a list of whitelisted credentials for an org
  get "/organizations/:organization_id/credential-authorizations" do
    @documentation_url = "/rest/reference/orgs#list-saml-sso-authorizations-for-an-organization"
    @route_owner       = "@github/iam"
    @accepted_scopes   = %w(read:org)

    org = find_org!

    control_access :whitelisted_org_credentials_reader,
      resource: org,
      user: current_user,
      organization: org,
      allow_integrations: true,
      allow_user_via_integration: true

    credential_auths = paginate_rel(
      Organization::CredentialAuthorization.by_organization(organization: org).active.excluding_applications,
    )

    GitHub::PrefillAssociations.for_credential_authorizations(credential_auths)

    deliver :credential_authorizations_hash, credential_auths
  end

  # revoke whitelisted access of a credential from an org
  delete "/organizations/:organization_id/credential-authorizations/:credential_auth_id" do
    @documentation_url = "/rest/reference/orgs#remove-a-saml-sso-authorization-for-an-organization"
    @route_owner       = "@github/iam"
    @accepted_scopes   = %w(admin:org)

    receive_with_schema("credential-authorization", "delete", skip_validation: true)

    org = find_org!

    whitelisted_credential = Organization::CredentialAuthorization.where(id: params[:credential_auth_id]).first

    deliver_error! 404 unless whitelisted_credential.present?

    control_access :whitelisted_org_credentials_writer,
      resource: org,
      user: current_user,
      organization: org,
      allow_integrations: true,
      allow_user_via_integration: true

    Organization::CredentialAuthorization.revoke(
      organization: org,
      credential: whitelisted_credential.credential,
      actor: current_user,
    )

    deliver_empty status: 204
  end
end
