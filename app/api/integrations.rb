# rubocop:disable Style/FrozenStringLiteralComment

# Provides endpoints for requests that are authenticated as the integration
# (i.e., requests using an assertion token signed with the integration's private
# key). For example, to obtain an access token for an installation, the request
# must be authenticated using an assertion token signed with the integration's
# private key.
class Api::Integrations < Api::App
  areas_of_responsibility :api, :webhook

  DEPRECATION_FLIPPER_KEY = :api_integrations_access_tokens_deprecation_emails
  DEPRECATION_EMAIL_TTL   = 2.weeks
  DEPRECATION_STATS_KEY   = "api.integrations.access_tokens.deprecated_endpoints.email_sent"
  PREVENT_EMAIL_FLIPPER_KEY = :opt_out_of_api_integrations_access_tokens_deprecation_emails

  statsd_tag_actions "/app/installations/:installation_id/access_tokens"
  statsd_tag_actions "/app/global/access_tokens"

  # Internal: Overrides default auth scheme to only support Integration Bearer
  # Assertions.
  def attempt_login
    attempt_login_from_integration
  end

  # Internal: Overrides default private mode authentication check so that
  # integrations can make authenticated requests
  def authenticated_for_private_mode?
    attempt_login_from_integration && current_integration.present?
  end

  before do
    GitHub.context.push(auth: "jwt")
    Audit.context.push(auth: "jwt")

    log_data[:auth] = "jwt"
  end

  # Temporarily disable rate limiting for now.
  #
  # TODO: Implement proper rate limiting for these endpoints. Please see
  # https://github.com/github/platform/issues/560 for more context.
  rate_limit_as nil

  get "/app" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#get-the-authenticated-app"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    GitHub::PrefillAssociations.for_integrations([current_integration])
    # We pass current_integration here as an extra option so that from the serializer
    # we can expose sensitive fields like installations_count when authenticated as the app
    deliver :integration_hash, current_integration, current_integration: current_integration
  end

  get "/app/installations" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#list-installations-for-the-authenticated-app"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    installations = GitHub.tracer.with_span("Api::Integrations::get_app_installations") do |query_span|
      scope = GitHub.tracer.with_span("build_scope") do |scope_span|
        scope = params[:outdated].present? ? current_integration.outdated_installations : current_integration.installations
        filter(scope.most_recent)
      end

      paginated_installations = GitHub.tracer.with_span("paginate") do |paginate_span|
        paginate_rel(scope).load
      end

      GitHub::PrefillAssociations.for_integration_installations(
        paginated_installations,
        integration: current_integration,
      )

      paginated_installations
    end

    deliver :installation_hash, installations
  end

  get "/app/installation-requests" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/v3/apps/#find-installation-requests"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    scope = current_integration.pending_installation_requests
    scope = filter(scope.most_recent)

    pending_requests = paginate_rel(scope)

    GitHub::PrefillAssociations.for_integration_installation_requests(pending_requests)
    deliver :integration_installation_request_hash, pending_requests
  end

  get "/integration/installations" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED

    if GitHub.flipper[:github_apps_brown_out].enabled?
      # TODO: Update this with the brown out blog post
      # @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
      deliver_error!(missing_repository_status_code)
    end

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    deliver_redirect! api_url("/app/installations"), status: permanent_redirect_status_code
  end

  get "/app/installations/:installation_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#get-an-installation-for-the-authenticated-app"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    installation = find_installation!
    GitHub::PrefillAssociations.for_integration_installations(
      [installation],
      integration: current_integration,
    )

    deliver :installation_hash, installation
  end

  delete "/app/installations/:installation_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#delete-an-installation-for-the-authenticated-app"
    receive_with_schema("installation", "delete")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    installation = find_installation!
    installation.uninstall(actor: current_integration.owner)

    deliver_empty(status: 204)
  end

  put "/app/installations/:installation_id/suspended" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#suspend-an-app-installation"
    receive_with_schema("installation", "suspend")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    installation = find_installation!
    installation.suspend!

    deliver_empty(status: 204)
  end

  delete "/app/installations/:installation_id/suspended" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#unsuspend-an-app-installation"
    receive_with_schema("installation", "unsuspend")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    installation = find_installation!
    installation.unsuspend!

    deliver_empty(status: 204)
  end

  get "/integration/installations/:installation_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED

    if GitHub.flipper[:github_apps_brown_out].enabled?
      # TODO: Update this with the brown out blog post
      # @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
      deliver_error!(missing_repository_status_code)
    end

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    deliver_redirect! api_url("/app/installations/#{params[:installation_id]}"), status: permanent_redirect_status_code
  end

  post "/app/installations/:installation_id/access_tokens" do # rubocop:disable GitHub/DuplicateRoutesAreDefinedTogether
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#create-an-installation-access-token-for-an-app"

    enforce_internal_access!

    # Introducing strict validation of the installation-token.create
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("installation-token", "create", skip_validation: true)

    attrs = [:permissions, :repositories, :repository_ids]
    data  = attr(data, *attrs)

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    GitHub.tracer.with_span("Api::Integrations::post_app_installation_access_tokens") do |span|
      ActiveRecord::Base.connected_to(role: :reading) do
        installation = find_installation!
        span.set_tag("integration", current_integration.slug)

        repository_ids = fetch_repository_ids(data, installation.target)

        # ScopedIntegrationInstallation can be created using the repositories,
        # permissions or both parameters
        if should_generate_scoped_token?(repository_ids, data)
          GitHub.dogstats.increment("api.integrations.access_tokens.create.scoped_token", tags: ["connect:#{connect_request?}"])
          result = generate_scoped_token(installation, repository_ids, data[:permissions])
          deliver_error!(422, message: result.error) if result.failed?
          installation = result.installation
        end

        record, token = AuthenticationToken.create_for(installation)
        options = { status: 201, token: token }

        if respond_with_lightweight_authentication_token?
          span.set_tag("lightweight_payload", true)

          stats_key = "api_integrations.lightweight_authentication_token_response"
          GitHub.dogstats.increment(stats_key, tags: ["integration:#{current_integration.slug}"])
          return deliver(:lightweight_authentication_token_hash, record, options)
        end

        if record.authenticatable.is_a?(ScopedIntegrationInstallation)
          with_master_or_replica(installation.permissions) do
            options[:permissions] = installation.permissions
            options[:single_file] = installation.single_file_name if single_file_permission?(installation)
            options[:repositories] = installation.repositories if scoped_token_repository_param?(data)
          end

          if options[:repositories].present?
            GitHub.tracer.with_span("prefill_associations_for_repositories") do |_|
              GitHub::PrefillAssociations.for_repositories(options[:repositories])
            end

            GitHub.tracer.with_span("prefill_licenses_for_repositories") do |_|
              GitHub::PrefillAssociations.fill_licenses(options[:repositories])
            end
          end
        else
          options[:permissions] = installation.permissions
          options[:single_file] = installation.single_file_name if single_file_permission?(installation)
        end

        options[:repository_selection] = repository_selection(installation)

        deliver :authentication_token_hash, record, options
      end
    end
  end

  post "/app/installations/:installation_id/access_tokens/scoped" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    unless ::Apps::Internal.capable?(:per_pull_request_permissions, app: current_integration)
      deliver_error!(404)
    end

    enforce_internal_access!

    data = receive_with_schema("scoped-installation-token", "create")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    GitHub.tracer.with_span("Api::Integrations::post_app_installation_access_tokens_scoped") do |span|
      ActiveRecord::Base.connected_to(role: :reading) do
        installation = find_installation!
        span.set_tag("integration", current_integration.slug)

        GitHub.dogstats.increment("api.integrations.access_tokens.create.scoped_token")
        result = ScopedIntegrationInstallation::V2::Creator.perform(installation, data)

        if result.failed?
          deliver_error!(422, message: result.error)
        end

        record, token = AuthenticationToken.create_for(result.installation)
        span.set_tag("lightweight_payload", true)

        stats_key = "api_integrations.lightweight_authentication_token_response"
        GitHub.dogstats.increment(stats_key, tags: ["integration:#{current_integration.slug}"])

        deliver(:lightweight_authentication_token_hash, record, status: 201, token: token)
      end
    end
  end

  patch "/app/installations/:installation_id/access_tokens" do # rubocop:disable GitHub/DuplicateRoutesAreDefinedTogether
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    unless ::Apps::Internal.capable?(:extend_access_token_expiry, app: current_integration)
      deliver_error!(404)
    end

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    data = receive_with_schema("installation-token", "update")
    ActiveRecord::Base.connected_to(role: :reading) do
      installation = find_installation!
      record = AuthenticationToken
        .active
        .with_unhashed_token(data["token"])
        .first

      deliver_error!(404) unless record
      if installation != record.authenticatable
        deliver_error!(404) unless installation.children.include?(record.authenticatable)
      end

      result = AuthenticationToken.extend_expires_at(record, data["expires_at"])

      if result.success?
        deliver :lightweight_authentication_token_hash, record, token: data["token"], status: 200
      else
        deliver_error!(422, message: result.message)
      end
    end
  end

  post "/app/global/access_tokens" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    enforce_internal_access!

    unless ::Apps::Internal.capable?(:installed_globally, app: current_integration)
      deliver_error!(404)
    end

    unless GitHub.flipper[:global_apps].enabled?(current_integration)
      deliver_error!(404)
    end

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    data = receive_with_schema("global-app-token", "create")

    target = User.find_by_id(data["target_id"])
    record_or_404(target)

    repository_ids = data.fetch("repository_ids", [])
    repositories = repository_ids.present? ? Repository.where(id: repository_ids) : :all

    permissions = data.fetch("permissions", {})

    result = SiteScopedIntegrationInstallation::Creator.perform(
      current_integration, target, repositories: repositories, permissions: permissions
    )

    if result.failed?
      deliver_error!(422, message: result.error)
    end
    installation = result.installation
    record, token = AuthenticationToken.create_for(installation)

    # After saving the new token we want to set the last write timestamp in the
    # cache, so the api DatabaseSelection can use the write DB for newly
    # created tokens and avoid issues due to replication lag.
    DatabaseSelector::LastOperations.from_token(token).update_last_write_timestamp

    deliver :lightweight_authentication_token_hash, record, status: 201, token: token
  end

  post "/installations/:installation_id/access_tokens" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED

    # This helper won't set the header in Enterprise
    deprecated(
      deprecation_date: Date.iso8601("2020-04-15"),
      sunset_date: Date.iso8601("2020-10-01"),
      info_url: "#{GitHub.developer_help_url}/changes/2020-04-15-replacing-create-installation-access-token-endpoint",
      alternate_path_url: "/app/installations/#{params[:installation_id]}/access_tokens"
    )

    unless current_integration.deprecated_endpoint_enabled?
      @documentation_url = "/changes/2020-04-15-replacing-create-installation-access-token-endpoint/"
      deliver_error!(missing_repository_status_code)
    end

    maybe_deliver_deprecation_email

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    installation = find_installation!
    record, token = AuthenticationToken.create_for(installation)

    deliver :authentication_token_hash, record, status: 201, token: token
  end

  get "/organizations/:organization_id/installation" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#get-an-organization-installation-for-the-authenticated-app"

    control_access :apps_audited,
      resource: org = find_org!,
      allow_integrations: false,
      allow_user_via_integration: false

    installation = find_installation_on_user!(org)

    GitHub::PrefillAssociations.for_integration_installations(
      [installation],
      integration: current_integration,
    )
    deliver :installation_hash, installation
  end

  get "/repositories/:repository_id/installation" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#get-a-repository-installation-for-the-authenticated-app"

    # The `this_repo` finder helper only applies in the
    # cases where the path is using the name with owner as
    # part of the path.
    repo = params.key?("repository_id") ? current_repo : this_repo

    control_access :apps_audited,
      resource: repo,
      allow_integrations: false,
      allow_user_via_integration: false

    installation = find_installation_for_repository!(repo)

    GitHub::PrefillAssociations.for_integration_installations(
      [installation],
      integration: current_integration,
    )
    deliver :installation_hash, installation
  end

  get "/user/:user_id/installation" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#get-a-user-installation-for-the-authenticated-app"

    control_access :apps_audited,
      resource: user = find_user!,
      allow_integrations: false,
      allow_user_via_integration: false

    installation = find_installation_on_user!(user)

    GitHub::PrefillAssociations.for_integration_installations(
      [installation],
      integration: current_integration,
    )
    deliver :installation_hash, installation
  end

  # Fetch the deliveries of the hook associated with the app
  get "/app/hook/deliveries" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    hook = current_integration.hook
    record_or_404(hook)

    safe_params = params.slice(:since, :until, :per_page, :status, :status_codes,
                               :events, :redelivery, :guid, :cursor, :repo_id, :installation_id)


    if safe_params[:repo_id]
      repo = Repository.find_by(id: safe_params[:repo_id])
      deliver_error!(404) unless repo
      deliver_error!(404) unless current_integration.installations.with_repository(repo).present?
    end

    if safe_params[:installation_id] &&
        !current_integration.installations.exists?(id: safe_params[:installation_id])
      deliver_error!(404)
    end

    status, body = Hookshot::Client.for_parent(hook.hookshot_parent_id).deliveries_for_hook(hook.id, safe_params.except(:per_page).merge(limit: per_page(safe_params["per_page"])))

    case status
    when 200
      build_cursor_based_links(body["page_info"], per_page(safe_params["per_page"]))
      deliver_raw body["deliveries"]
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  # Get a single delivery for the hook associated with the app
  get "/app/hook/deliveries/:delivery_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    hook = current_integration.hook
    record_or_404(hook)

    safe_params = { include_payload: !!params[:full] }
    status, body = Hookshot::Client.for_parent(hook.hookshot_parent_id).delivery_for_hook(params[:delivery_id], hook.id, safe_params)

    case status
    when 200
      deliver_raw body
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  # Trigger a new delivery attempt
  post "/app/hook/deliveries/:delivery_id/attempts" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    hook = current_integration.hook
    record_or_404(hook)

    hook_client  = Hookshot::Client.for_parent(hook.hookshot_parent_id)
    status, body = hook_client.create_redelivery_attempt(int_id_param!(key: :delivery_id), hook.id, hook.config_attributes)

    case status
    when 202
      deliver_empty status: 202
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  private

  def enforce_internal_access!
    return if GitHub.enterprise?

    should_enforce_access = ::Apps::Internal.capable?(
      :enforce_internal_access_on_token_generation, app: current_integration
    )

    if should_enforce_access && !GitHub.internal_api_role?
      GitHub.dogstats.increment(
        "api_integrations.enforced_internal_access",
        tags: ["integration:#{current_integration.slug}"],
      )
      deliver_error!(403)
    end
  end

  def filter(scope)
    if (since = time_param!(:since))
      scope = scope.since(since.getlocal)
    end

    scope
  end

  def find_installation!(installation_id = params[:installation_id])
    installation = current_integration.installations.find_by_id(installation_id)
    record_or_404(installation)
  end

  def find_installation_for_repository!(repository)
    installation = IntegrationInstallation.with_repository(repository).find_by(integration: current_integration)
    record_or_404(installation)
  end

  def find_installation_on_user!(user)
    installation = current_integration.installations.find_by(target: user)
    record_or_404(installation)
  end

  def fetch_repository_ids(data, installation_target)
    GitHub.tracer.with_span("Api::Integrations#fetch_repository_ids") do |span|
      repository_ids = []

      # Unfortunately, this type-assertion is needed until we turn back
      # on the validation for receive_with_schema
      if data[:repository_ids] && data[:repository_ids].is_a?(Array)
        repository_ids << data[:repository_ids]
      end

      if data[:repositories] && data[:repositories].is_a?(Array)
        found_repository_ids = installation_target.repositories.where(name: data[:repositories]).pluck(:id)

        if found_repository_ids.count != data[:repositories].uniq.count
          deliver_error!(422, message: ScopedIntegrationInstallation::Permissions::Result::HUMAN_READABLE_REASONS[:repositories_not_available_to_target])
        end

        repository_ids << found_repository_ids
      end

      span.set_tag("repository_ids.count", repository_ids.count)
      repository_ids.flatten.map(&:to_i).uniq
    end
  end

  def per_page(limit)
    return DEFAULT_PER_PAGE unless limit
    [limit.to_i, MAX_PER_PAGE].min
  end

  def should_generate_scoped_token?(repository_ids, data)
    repository_ids.any? || data[:permissions]
  end

  def scoped_token_repository_param?(data)
    data[:repositories] || data[:repository_ids]
  end

  def repository_selection(installation)
    installation.installed_on_all_repositories? ? "all" : "selected"
  end

  def single_file_permission?(installation)
    installation.permissions.present? && installation.permissions.keys.include?("single_file")
  end

  # Internal: Redirect the request to the repository's new location.
  #
  # block - A block that returns the path to use for the redirect. The block
  #         receives the following arguments to assist in constructing the
  #         redirect path:
  #         :path            - A String representing the full path for this
  #                            request.
  #         :requested_nwo   - A String representing the repository
  #                            name-with-owner that this request is attempting
  #                            to access.
  #         :redirected_repo - The relocated Repository (that previously existed
  #                            at the requested name-with-owner location).
  #
  # Halts with a redirect if the requested repository has relocated (e.g., if it
  #   has been renamed, if it has been transferred, etc.).
  # Halts with a 404 if the request lacks permission to access the relocated
  #   repository.
  # Returns nothing.
  def redirect_to_new_repo_location_or_404!(&block)
    redirected_repo = find_redirected_repo_from_path

    env[GitHub::Routers::Api::ThisRepositoryKey] = redirected_repo

    if access_allowed?(:follow_repo_redirect, resource: redirected_repo, current_integration: current_integration, allow_integrations: true, allow_user_via_integration: true)
      redirect_path =
        block.call(request.fullpath, repo_nwo_from_path, redirected_repo)

      deliver_redirect! \
        api_url(redirect_path), status: permanent_redirect_status_code
    else
      deliver_error!(missing_repository_status_code)
    end
  end

  def generate_scoped_token(installation, repository_ids, permissions)
    GitHub.tracer.with_span("Api::Integrations#generate_scoped_token") do |span|
      repositories = if repository_ids.empty? && installation.installed_on_all_repositories?
       span.set_tag("repositories", "all")
       :all
      elsif repository_ids.empty?
        span.set_tag("repositories", "installation.repositories")
        installation.repositories
      else
        span.set_tag("repositories", "selected_repositories")
        Repository.with_ids(repository_ids)
      end

      ScopedIntegrationInstallation::Creator.perform(
        installation, repositories: repositories, permissions: permissions
      )
    end
  end

  def respond_with_lightweight_authentication_token?
    ::Apps::Internal.capable?(:can_receive_lightweight_access_token_response, app: current_integration)
  end


  # Internal: wrap the given block by connecting to either the primary or read
  # replicas based on whether the given permissions exist.
  #
  # Scoped installation tokens are written to the database so we should
  # break out of the read-from-replica connection to make sure we use the
  # primary to read our own write.
  #
  # Returns nothing.
  def with_master_or_replica(permissions)
    return unless block_given?

    role = permissions&.empty? ? :writing : :reading

    GitHub.dogstats.increment("api.integrations.access_tokens.create.with_master_or_replica", tags: ["role:#{role}"])

    ActiveRecord::Base.connected_to(role: role) do
      yield
    end
  end

  def maybe_deliver_deprecation_email
    return unless response.successful?
    return unless GitHub.flipper[DEPRECATION_FLIPPER_KEY].enabled?
    return if GitHub.flipper[PREVENT_EMAIL_FLIPPER_KEY].enabled?(current_integration)
    return if current_integration.connect_app?

    if GitHub::ActionRestraint.perform?(
         "deprecated_api_integrations_access_tokens_endpoint",
         interval: DEPRECATION_EMAIL_TTL,
         app_id:   current_integration.id,
       )

      if current_integration.owner.is_a?(Organization)
        OrganizationMailer.api_integrations_access_tokens_deprecation(current_integration, time: Time.now).deliver_later
      else
        AccountMailer.api_integrations_access_tokens_deprecation(current_integration, time: Time.now).deliver_later
      end

      GitHub.dogstats.increment(DEPRECATION_STATS_KEY)
    end
  end
end
