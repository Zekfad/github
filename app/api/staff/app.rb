# frozen_string_literal: true

class Api::Staff::App < Api::App
  areas_of_responsibility :stafftools, :api

  # Access to staff APIs is limited to the staff_api scope.
  # To generate a token with this scope, use:
  # curl -u $username -d '{"scopes":["site_admin"],"note":"test"}'
  # -X POST https://api.github.com/authorizations
  # Individual actions and endpoints can be further restricted
  # using role based authentication, where request method and
  # the specific endpoint are used to determine access
  before do
    control_access :staff_api, request_method: env["REQUEST_METHOD"], route_pattern: Api::App.route_pattern(env), allow_integrations: false, allow_user_via_integration: false
  end

  private

  def audit_log_results(phrase, per_page: nil, page: nil)
    return Search::Results.empty if phrase.blank?

    options = {
      phrase:       phrase,
      current_user: current_user,
    }
    options[:per_page] = per_page if per_page.present?
    options[:page] = page if page.present?

    query = Audit::Driftwood::Query.new_stafftools_query(options)
    query.execute
  end
end
