# frozen_string_literal: true

class Api::Staff::Coupons < Api::Staff::App
  unless GitHub.enterprise?

    # APIs for the Education app
    # https://github.com/github/education-web/

    # Retrieve the coupon from a User or Organization. Responds with a 404 if no
    # coupon is applied.
    get "/staff/user/:user_id/coupon" do
      @route_owner = Platform::NoOwnerBecause::UNAUDITED
      skip_rate_limit!
      user_or_org = find_user!
      coupon_redemption = user_or_org.coupon_redemption
      if coupon_redemption
        GitHub::PrefillAssociations.for_coupons([coupon_redemption])
        deliver :coupon_redemption_hash, coupon_redemption,
          last_modified: calc_last_modified(coupon_redemption)
      else
        deliver_error 404
      end
    end

    # Apply a coupon to a User or Organization. Responds with a 409 if they
    # already have a coupon, or a 422 if the coupon cannot be applied.
    put "/staff/user/:user_id/coupon" do
      @route_owner = Platform::NoOwnerBecause::UNAUDITED
      skip_rate_limit!
      user_or_org = find_user!
      if user_or_org.coupon_redemption
        deliver_error! 409, message: "Only one coupon per account"
      else
        data = attr(receive(Hash), :code)
        if coupon_redemption = user_or_org.redeem_coupon(data["code"],
          allow_reuse: true, actor: current_user)
          deliver :coupon_redemption_hash, coupon_redemption,
            last_modified: calc_last_modified(coupon_redemption)
        else
          deliver_error 422
        end
      end
    end

    # Remove the coupon from a User or Organization. Responds with a 204 if
    # successfully removed, or a 404 if there isn't one to be removed.
    delete "/staff/user/:user_id/coupon" do
      @route_owner = Platform::NoOwnerBecause::UNAUDITED
      skip_rate_limit!
      user_or_org = find_user!
      if user_or_org.expire_active_coupon quiet: params[:quiet]
        deliver_empty(status: 204)
      else
        deliver_error! 404
      end
    end

    # Create a coupon. The request must include a JSON
    # body with these attributes:
    #
    # discount   - Monthly discount. E.g. "$7".
    # group      - A label for simple classification.
    # note       - Information about why the coupon was created.
    # code       - (optional) The code users will enter to redeem the coupon.
    #              Default is a 7 digit hex code.
    # duration   - (optional) Number of days that the discount will be applied.
    #              Default is 30 days.
    # limit      - (optional) How many times the coupon can be redeemed.
    #              Default is 1 redemption.
    # expires_at - (optional) The time at which the coupon expires.
    #              Default is 1 year from now.
    #
    # Responds with 200 on success, or 422 if the data is invalid.
    post "/staff/coupons" do
      @route_owner = Platform::NoOwnerBecause::UNAUDITED
      skip_rate_limit!
      data = receive_with_schema("coupon", "create")
      data["expires_at"] ||= (Date.today + 1.year).to_time.iso8601
      data["expires_at"] = parse_time!(data.delete("expires_at"))

      coupon = ::Coupon.new(data)
      if coupon.save
        deliver :coupon_hash, coupon, last_modified: calc_last_modified(coupon)
      else
        deliver_error! 422, errors: coupon.errors
      end
    end
  end
end
