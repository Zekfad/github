# frozen_string_literal: true

class Api::OrganizationInvitations < Api::App
  areas_of_responsibility :orgs, :api

  get "/organizations/:organization_id/invitations" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-pending-organization-invitations"
    deliver_error! 404 if GitHub.bypass_org_invites_enabled?

    org = find_org!

    set_forbidden_message "You must be an admin to view organization pending invitations."
    control_access :read_org_invitations,
      resource: org,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: true

    invitations = paginate_rel(org.pending_invitations)

    GitHub::PrefillAssociations.for_organization_pending_invitations(invitations)
    deliver :invitation_hash, invitations
  end

  InviteToOrganizationQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($organizationId: ID!, $email: String, $inviteeId: ID, $role: OrganizationInvitationRole, $teamIds: [ID!]) {
      inviteToOrganization(input: {organizationId: $organizationId, email: $email, inviteeId: $inviteeId, role: $role, teamIds: $teamIds}) {
        invitation {
          ...Api::Serializer::OrganizationsDependency::OrganizationInvitationFragment
        }
      }
    }
  GRAPHQL

  # Create a new org invitation
  post "/organizations/:organization_id/invitations" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#create-an-organization-invitation"
    deliver_error! 404 if GitHub.bypass_org_invites_enabled?

    set_forbidden_message "You must be an admin to create an invitation to an organization."

    org = find_org!
    control_access :create_organization_invitation, resource: org, challenge: true, allow_integrations: true, allow_user_via_integration: true
    attributes = attr(receive_with_schema("organization-invitation", "create-legacy"), :email, :invitee_id, :role, :team_ids)

    if invitee_id = attributes[:invitee_id]
      invitee = User.find_by_id(invitee_id)
      unless invitee.present?
        deliver_error! 422, message: "Invalid invitee_id", documentation_url: @documentation_url
      end
    end

    teams = org.teams.where(id: attributes[:team_ids])

    results = platform_execute(InviteToOrganizationQuery, variables: {
      organizationId: org.global_relay_id,
      email: attributes[:email],
      inviteeId: invitee&.global_relay_id,
      role: attributes[:role]&.upcase,
      teamIds: teams.map(&:global_relay_id),
    })

    if results.errors.all.any?
      deprecated_deliver_graphql_error(
        errors: results.errors.all,
        resource: "OrganizationInvitation",
        documentation_url: @documentation_url,
      )
    else
      deliver :graphql_invitation_hash, results.data.invite_to_organization.invitation, status: 201
    end
  end

  OrganizationInvitationTeamsQuery = PlatformClient.parse <<-'GRAPHQL'
    query($invitationId: ID!, $pageSize: Int!, $page: Int, $includeFullTeamDetails: Boolean!) {
      node(id: $invitationId) {
        ... on OrganizationInvitation {
          teams(first: $pageSize, numericPage: $page) {
            totalCount
            nodes {
              ...Api::Serializer::OrganizationsDependency::SimpleTeamFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # list teams on an org invitation
  get "/organizations/:organization_id/invitations/:invitation_id/teams" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-organization-invitation-teams"
    deliver_error! 404 if GitHub.bypass_org_invites_enabled?

    set_forbidden_message "You must be an admin to list the teams off an organization invitation."

    org = find_org!
    invitation = record_or_404(org.pending_invitations.find_by_id(params[:invitation_id]))

    control_access :list_organization_invitation_teams, resource: org, challenge: true, allow_integrations: true, allow_user_via_integration: true

    results = platform_execute(OrganizationInvitationTeamsQuery, variables: {
      includeFullTeamDetails: false,
      invitationId: invitation.global_relay_id,
      pageSize: pagination[:per_page],
      page: pagination[:page],
    })

    set_pagination_headers(collection_size: results.data.node.teams.total_count)

    deliver :graphql_team_hash, results.data.node.teams.nodes
  end
end
