# frozen_string_literal: true

class Api::EnterpriseRunners < Api::App
  map_to_service :actions_runners

  # Create a registration token for enterprise-level runners
  post "/enterprises/:enterprise_id/actions/runners/registration-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("runner", "create-enterprise-runner-registration-token")

    expires_at = 1.hour.from_now
    scope = enterprise.runner_creation_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)
    deliver_raw({ token: token, expires_at: expires_at }, status: 201)
  end

  # Delete an enterprise-level runner
  delete "/enterprises/:enterprise_id/actions/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("runner", "delete-enterprise-runner")

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.delete_runner(enterprise, params[:runner_id].to_i, actor: current_user)
    end

    if resp.call_succeeded?
      deliver_empty status: 204
    else
      deliver_error! 500, message: "Failed to delete the specified runner, it may be actively running a job"
    end
  end

  # Create a remove token for enterprise-level runners
  post "/enterprises/:enterprise_id/actions/runners/remove-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("runner", "create-enterprise-runner-remove-token")

    expires_at = 1.hour.from_now
    scope = enterprise.runner_deletion_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)
    deliver_raw({ token: token, expires_at: expires_at }, status: 201)
  end

  # List runner downloads
  get "/enterprises/:enterprise_id/actions/runners/downloads" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_downloads(enterprise)
    end

    downloads = resp&.value&.downloads || []
    downloads_hashes = downloads.map do |download|
      download_hash(download)
    end
    deliver_raw(downloads_hashes)
  end

  # Get all runners
  get "/enterprises/:enterprise_id/actions/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_runners(enterprise)
    end

    validate_listing!(resp&.value&.runners)

    # We map to an Array so pagination works
    runners = paginate_rel(resp&.value&.runners.map { |runner| runner })
    deliver :actions_runners_hash, { runners: runners, total_count: runners.total_entries, current_user: current_user }
  end

  # Get a runner
  get "/enterprises/:enterprise_id/actions/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.get_runner(enterprise, params[:runner_id].to_i)
    end

    runner = resp&.value&.runner
    deliver_error! 404 unless runner

    deliver :actions_runner_hash, runner, { current_user: current_user }
  end

  private

  def download_hash(download)
    { os: download.os, architecture: download.architecture, download_url: download.download_url, filename: download.filename }
  end

  def enterprise_runners_enabled?
    GitHub.actions_enabled? && (
      GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) ||
      GitHub.enterprise?
    )
  end

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_selfhostedrunners: GitHub.launch_selfhostedrunners)
      deliver_error!(503, message: "Runners unavailable. Please try again later.")
    end
  end

  def ensure_tenant!(enterprise)
    result = GrpcHelper.rescue_from_grpc_errors("EnterpriseTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(enterprise)
    end
    deliver_error! 500 unless result
  end
end
