# frozen_string_literal: true

class Api::Artifacts < Api::App
  map_to_service :actions_experience

  # Get all artifacts for a run.
  get "/repositories/:repository_id/actions/runs/:run_id/artifacts" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-workflow-run-artifacts"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    deliver_error!(404) unless repo.id == workflow_run.check_suite.repository_id

    artifacts = paginate_rel(workflow_run.artifacts.includes(check_suite: :repository))

    deliver :artifacts_hash, { artifacts: artifacts, total_count: artifacts.total_entries }
  end

  # Get an artifact.
  get "/repositories/:repository_id/actions/artifacts/:artifact_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-an-artifact"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    artifact = Artifact.includes(:check_suite).find_by(id: params[:artifact_id])
    record_or_404(artifact)

    deliver_error!(404) unless repo.id == artifact.check_suite.repository_id

    deliver :artifact_hash, artifact
  end

  # Get all artifacts for a repository.
  get "/repositories/:repository_id/actions/artifacts" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-artifacts-for-a-repository"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    artifacts = Artifact.includes(:check_suite).where(check_suites: { repository: repo }).order(id: :desc)
    artifacts = paginate_rel(artifacts)

    deliver :artifacts_hash, { artifacts: artifacts, total_count: artifacts.total_entries }
  end

  # Download the zip archive of an artifact.
  get "/repositories/:repository_id/actions/artifacts/:artifact_id/zip" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#download-an-artifact"

    repo = find_repo!

    control_access :read_actions_downloads,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      forbid_message: "You must have the actions scope to download artifacts.",
      allow_integrations: true,
      allow_user_via_integration: true

    artifact = Artifact.includes(:check_suite).find_by(id: params[:artifact_id])
    record_or_404(artifact)

    deliver_error!(404) unless repo.id == artifact.check_suite.repository_id

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::ExchangeURLRequest.new({
        unauthenticated_url: artifact.source_url,
        repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repo.global_relay_id),
        resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_DOWNLOAD_ARTIFACT,
      })

      GitHub.launch_artifacts_exchange_for_check_suite(artifact.check_suite).exchange_url(grpc_request)
    end

    if result.call_succeeded?
      redirect result.value.authenticated_url
    else
      deliver_error! 500, message: "Failed to generate URL to download artifact"
    end

  end

  # Delete an artifact.
  delete "/repositories/:repository_id/actions/artifacts/:artifact_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#delete-an-artifact"

    repo = find_repo!

    receive_with_schema("artifact", "delete-artifact")

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      forbid_message: "You must have push access to this repository.",
      allow_integrations: true,
      allow_user_via_integration: true

    artifact = Artifact.find_by(id: int_id_param!(key: :artifact_id))
    record_or_404(artifact)

    deliver_error!(404) unless repo.id == artifact.check_suite.repository_id

    artifact.destroy
    deliver_empty status: 204
  end
end
