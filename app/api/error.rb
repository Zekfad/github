# frozen_string_literal: true

class Api::Error < StandardError; end
