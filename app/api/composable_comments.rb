# frozen_string_literal: true

class Api::ComposableComments < Api::App

  # Create a composable comment
  post "/repositories/:repository_id/issues/:issue_number/composable_comments" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:interactions_api)

    repo  = find_repo!

    issue = repo.issues.find_by_number int_id_param!(key: :issue_number)
    record_or_404(issue)

    control_access :create_composable_comment,
      repo: repo,
      resource: issue,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver_error!(404) unless GitHub.flipper[:interactions_api].enabled?(current_integration.owner)

    data = receive_with_schema("composable-comment", "create")

    composable_comment = ComposableComment.new(
      issue: issue,
      integration: current_integration,
      components: data["components"],
      notifications: data["notifications"],
    )

    if composable_comment.save
      # Preload AR objects ahead of serializer
      # I tried using GitHub::PrefillAssociations.for_composable_comments
      # but for some reason that was duplicating each record, super weird
      composable_comment.components

      GlobalInstrumenter.instrument("composable_comments.create",
        app: current_integration,
        issue: issue,
        composable_comment: composable_comment,
        repository: repo,
      )

      deliver(:composable_comment_hash, composable_comment, status: 201)
    else
      deliver_error 422, errors: composable_comment.errors, documentation_url: @documentation_url
    end
  end

  # Update a composable comment
  patch "/repositories/:repository_id/issues/composable_comments/:composable_comment_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:interactions_api)

    repo = find_repo!

    composable_comment = ComposableComment.find_by(id: int_id_param!(key: :composable_comment_id))
    record_or_404(composable_comment)

    control_access :update_composable_comment,
      resource: composable_comment,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver_error!(404) unless GitHub.flipper[:interactions_api].enabled?(current_integration.owner)

    data = receive_with_schema("composable-comment", "update")

    previous_components = composable_comment.components.to_json
    composable_comment.components = data["components"]

    if composable_comment.save
      # Preload AR objects ahead of serializer
      # I tried using GitHub::PrefillAssociations.for_composable_comments
      # but for some reason that was duplicating each record, super weird
      composable_comment.components

      GlobalInstrumenter.instrument("composable_comments.update",
        app: current_integration,
        issue: composable_comment.issue,
        composable_comment: composable_comment,
        previous_components: previous_components,
        repository: repo,
      )

      deliver(:composable_comment_hash, composable_comment)
    else
      deliver_error 422, errors: composable_comment.errors, documentation_url: @documentation_url
    end
  end

  # Delete a composable comment
  delete "/repositories/:repository_id/issues/composable_comments/:composable_comment_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:interactions_api)

    repo = find_repo!

    composable_comment = ComposableComment.find_by(id: int_id_param!(key: :composable_comment_id))
    record_or_404(composable_comment)

    control_access :delete_composable_comment,
      resource: composable_comment,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver_error!(404) unless GitHub.flipper[:interactions_api].enabled?(current_integration.owner)

    GlobalInstrumenter.instrument("composable_comments.delete",
      app: current_integration,
      issue: composable_comment.issue,
      composable_comment: composable_comment,
      repository: repo,
    )

    composable_comment.destroy

    deliver_empty status: 204
  end
end
