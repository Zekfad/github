# rubocop:disable Style/FrozenStringLiteralComment

module Api
  module Limiters
    class ElapsedTimeByAuthenticationFingerprint < GitHub::Limiters::MemcachedWindow
      REQUEST_STARTED_AT = "github.limiters.request_started_at"

      def initialize(max:)
        super("time-based", limit: max)
      end

      def start(request)
        request.env[REQUEST_STARTED_AT] = Time.now
        super
      end

      def record_finish(request)
        increment_counter(request)
      end

      protected

      def key(request)
        Api::Middleware::RequestAuthenticationFingerprint.get(request.env).to_s
      end

      # The cost should be the number of milliseconds this request has taken.
      def cost(request)
        Integer((Time.now - request.env[REQUEST_STARTED_AT]) * 1_000)
      end
    end
  end
end
