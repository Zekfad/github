# rubocop:disable Style/FrozenStringLiteralComment

module Api
  module Limiters
    class ElapsedAuthenticatedTimeByPath < ElapsedTimeByAuthenticationFingerprint
      attr_reader :path

      def initialize(max:, path:)
        super(max: max)

        @path = path
      end

      protected

      def increment_counter(request)
        super if path == request.path_info
      end

      # Internal: has the request met or exceeded the limit?
      def at_limit?(request)
        super if path == request.path_info
      end

      def key(request)
        [super, path.parameterize].join(":")
      end
    end
  end
end
