# rubocop:disable Style/FrozenStringLiteralComment

require "digest/md5"

module Api
  module Limiters
    # This limiter is intialized with a maximum number of concurrent requests to
    # allow as well as a TTL.
    #
    # The TTL-based window can lead to slightly odd edge cases. If the limit
    # is 10 requests:
    #
    # * A request starts (and perhaps finishes), initializing the window key
    # * 10 requests begin
    # * Subsequent requests are blocked
    # * The window key expires
    # * New requests reinitialize and increment the key from 0
    # * The previous 10 requests now finish, decrementing the counter to a
    #   minimum value of 0 even though several requests are still being processed.
    #
    # At this point, for remainder of the TTL window, the limiter will allow
    # extra requests. The current implementation is about as simple as it gets,
    # but it can be adjusted in the future if this edge case shows up more often
    # than we think it will, or has a negative effect.
    #
    # A potential solution is to change from using a TTL on a fixed key, to a
    # time-based key (based on request timestamp).
    class ConcurrentAuthenticationFingerprint < GitHub::Limiters::MemcachedWindow
      include GitHub::Middleware::Constants

      RESET_DURATION = 1 # inform clients that they can retry nearly immediately

      def initialize(ttl:, max:)
        super("concurrent-authentication-fingerprint", ttl: ttl, limit: max)
      end

      def record_start(request)
        increment_counter(request)
      end

      def record_finish(request)
        decrement_counter(request)
      end

      def duration
        RESET_DURATION
      end

      protected

      def key(request)
        Api::Middleware::RequestAuthenticationFingerprint.get(request.env).to_s
      end

    end
  end
end
