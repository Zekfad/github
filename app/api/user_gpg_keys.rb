# frozen_string_literal: true

class Api::UserGpgKeys < Api::App
  # List a User's GPG keys
  get "/user/gpg_keys" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/users#list-gpg-keys-for-the-authenticated-user"
    control_access :list_gpg_keys,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    keys = current_user.gpg_keys.primary_keys.with_subkeys.with_emails
    keys = paginate_rel(keys)
    GitHub::PrefillAssociations.fill_gpg_emails(keys)
    deliver(:gpg_key_hash, keys)
  end

  # Get a GPG key
  get "/user/gpg_keys/:gpg_key_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/users#get-a-gpg-key-for-the-authenticated-user"

    key = GpgKey.with_subkeys.where(id: int_id_param!(key: :gpg_key_id)).first
    control_access :read_gpg_key,
      resource: key,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    GitHub::PrefillAssociations.fill_gpg_emails(key)
    deliver(:gpg_key_hash, key)
  end

  # Create (add) GPG key to a User
  post "/user/gpg_keys" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/users#create-a-gpg-key-for-the-authenticated-user"
    control_access :add_gpg_key,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if !current_user.change_gpg_key_enabled?
      deliver_update_denied_using_ldap_sync! "GPG keys must be added in the #{GitHub.auth.name} Identity Provider"
    end

    data = receive_with_schema("gpg-key", "create")

    armored_public_key = data["armored_public_key"].to_s
    key = current_user.gpg_keys.create_from_armored_public_key(
      armored_public_key,
    )

    if !key.new_record?
      deliver(:gpg_key_hash, key, status: 201)
    else
      deliver_error(422,
        errors: key.errors,
        documentation_url: "/v3/users/gpg_keys",
      )
    end
  end

  # Delete (remove) GPG key
  delete "/user/gpg_keys/:gpg_key_id" do
    @route_owner = "@github/identity"
    receive_with_schema("gpg-key", "delete")

    @documentation_url = "/rest/reference/users#delete-a-gpg-key-for-the-authenticated-user"

    key = GpgKey.where(id: int_id_param!(key: :gpg_key_id)).first
    control_access :remove_gpg_key,
      resource: key,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if !current_user.change_gpg_key_enabled?
      deliver_update_denied_using_ldap_sync! "GPG keys must be removed in the #{GitHub.auth.name} Identity Provider."
    end

    if key.subkey?
      message = "Subkeys cannot be deleted directly. Delete the primary key "\
        "instead."
      deliver_error(
        422,
        message: message,
        documentation_url: "/v3/users/gpg_keys",
      )
    else
      key.destroy
      deliver_empty(status: 204)
    end
  end

  get "/user/:user_id/gpg_keys" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/users#list-gpg-keys-for-a-user"
    control_access :read_user_public, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    user = find_user!
    keys = user.gpg_keys.primary_keys.with_subkeys.with_emails
    keys = paginate_rel(keys)
    GitHub::PrefillAssociations.fill_gpg_emails(keys)

    deliver(:gpg_key_hash, keys)
  end
end
