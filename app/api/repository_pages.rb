# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryPages < Api::App
  areas_of_responsibility :pages, :api

  def validate_source_fields(repo, source, pages_any_branch, set_default_ref_name = false)
    # Extract and check validity of (new) source value. Our current source handling code doesn't do this for us.
    source_ref_name = source["branch"] if !source.nil? && source.is_a?(Hash)
    if (!source_ref_name.nil? && !pages_any_branch && !Page::VALID_LEGACY_BRANCHES.include?(source_ref_name))
      deliver_error!(400, message: "Invalid source/branch value")
    end
    source_subdir = source["path"] if !source.nil? && source.is_a?(Hash)
    if (!source_subdir.nil? && !Page::VALID_SUBDIRS.include?(source_subdir))
      deliver_error!(400, message: "Invalid source/path value")
    end

    # Set default for the branch name
    if (set_default_ref_name && source_ref_name.nil?)
      source_ref_name = "master" if !pages_any_branch && repo.is_user_pages_repo?
      source_ref_name = repo.default_branch if pages_any_branch && repo.is_user_pages_repo?
      source_ref_name = "gh-pages" if !repo.is_user_pages_repo?
    end

    # Set default subdir
    if (source_subdir.nil?)
      source_subdir = "/"
    end

    return source_ref_name, source_subdir
  end

  def set_source(pages_any_branch, page, source, source_ref_name, source_subdir)
    # Set the source
    if !source.nil? && source.is_a?(String) && !pages_any_branch
      return page.set_source(source)
    elsif !source.nil? && source.is_a?(String) && pages_any_branch
      case source
      when "master"
        return page.set_source(nil, "master", "/")
      when "master /docs"
        return page.set_source(nil, "master", "/docs")
      when "gh-pages"
        return page.set_source(nil, "gh-pages", "/")
      end
    elsif !source_ref_name.nil? && !source_subdir.nil? && pages_any_branch
      return page.set_source(nil, source_ref_name, source_subdir)
    elsif !source_ref_name.nil? && !source_subdir.nil? && !pages_any_branch
      if (source_subdir == "/")
        return page.set_source(source_ref_name)
      else
        return page.set_source(source_ref_name + " " + source_subdir)
      end
    end

    # No source is set, consider we still saved the page successfully
    true
  end

  # get page info
  get "/repositories/:repository_id/pages" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#get-a-github-pages-site"
    repo = find_repo!
    control_access :read_pages, resource: repo, allow_integrations: true, allow_user_via_integration: true
    deliver_error!(404) unless page = repo.page
    deliver :page_hash, page, {
      repo: repo,
      last_modified: calc_last_modified(repo),
      html_url: repo.gh_pages_url,
    }
  end

  # create pages site for this repo
  post "/repositories/:repository_id/pages" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#create-a-github-pages-site"
    require_preview(:pages_toggle)
    repo = find_repo!
    control_access :write_pages,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    if repo.page
      deliver_error!(409, message: "GitHub Pages is already enabled.")
    end
    if repo.private? && !repo.plan_supports?(:pages)
      deliver_error!(422, message: "Your current plan does not support GitHub Pages for this repository.")
    end

    data = receive_with_schema("page", "create")

    page = repo.build_page

    # Check if :pages_any_branch is enabled
    pages_any_branch = GitHub.flipper[:pages_any_branch].enabled?(page.async_owner.sync)

    # Extract and check validity of (new) source value.
    source_ref_name, source_subdir = validate_source_fields(repo, data["source"], pages_any_branch, set_default_ref_name: true)

    # Set the source
    saved = page.save && set_source(pages_any_branch, page, nil, source_ref_name, source_subdir)

    unless saved
      deliver_error!(422, message: "Unable to save", errors: page.errors)
    end

    if repo.heads.include?(source_ref_name)
      repo.rebuild_pages(current_user)
    else
      deliver_error!(422,
        message: "The #{repo.pages_branch} branch must exist before GitHub Pages can be built.",
      )
    end

    deliver :page_hash, page.reload, {
      status: 201,
      repo: repo,
      last_modified: calc_last_modified(repo),
      html_url: repo.gh_pages_url,
    }
  end

  # delete pages site for this repo
  delete "/repositories/:repository_id/pages" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#delete-a-github-pages-site"
    require_preview(:pages_toggle)
    repo = find_repo!
    control_access :write_pages,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    record_or_404(repo.page)

    receive_with_schema("page", "delete")
    is_user_pages_repo = repo.is_user_pages_repo?
    pages_any_branch = GitHub.flipper[:pages_any_branch].enabled?(repo.page.async_owner.sync)
    if pages_any_branch && (is_user_pages_repo || (!is_user_pages_repo && repo.has_gh_pages_branch?))
      deliver_error!(422, message: "Deactivating GitHub pages for this repository is not allowed.")
    elsif !repo.page.destroy
      deliver_error!(422, message: "Unable to deactivate GitHub pages for this repository.", errors: repo.page.errors)
    end

    deliver_empty(status: 204)
  end

  # modify page info
  put "/repositories/:repository_id/pages" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#update-information-about-a-github-pages-site"
    repo = find_repo!
    control_access :write_pages,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true
    page = repo.page
    record_or_404(page)

    data = receive_with_schema("page", "update-legacy")
    attributes = attr(data, :cname, :source, :public)

    # Check if :pages_any_branch is enabled
    pages_any_branch = GitHub.flipper[:pages_any_branch].enabled?(page.async_owner.sync)

    # Extract the source attribute (may be a string or a hash)
    source = attributes[:source]

    # Check validity of (legacy) source value. Our current source handling code doesn't do this for us.
    if !source.nil? && source.is_a?(String) && !Page::VALID_SOURCES.include?(source)
      deliver_error!(400, message: "Invalid source value")
    end

    # Extract and check validity of (new) source value.
    source_ref_name, source_subdir = validate_source_fields(repo, source, pages_any_branch) if source.is_a?(Hash)

    # If set pages visbility to private, but plan does not support private pages, return bad request.
    if GitHub.flipper[:private_pages].enabled?(current_user) && attributes.key?(:public) && !attributes[:public] && !repo.plan_supports?(:private_pages)
      deliver_error!(422, message: "Current plan does not support private GitHub Pages")
    end

    # Check that custom domains are enabled.
    if attributes.key?(:cname) && !GitHub.pages_custom_cnames?
      deliver_error!(400, message: "Custom domains are not available for GitHub Pages")
    end

    # Check that no custom validation error with the custom domain is present.
    # If a cname is written which is invalid, a before_validation hook clears it from the db silently.
    # This is not ideal behaviour. We want to ensure errors are surfaced to users.
    if attributes.key?(:cname) && page.cname_error(attributes[:cname])
      deliver_error!(400, message: "Invalid cname", errors: [page.cname_error(attributes[:cname])])
    end

    # Set the source (if it was set to something)
    if !source.nil?
      unless set_source(pages_any_branch, page, source, source_ref_name, source_subdir)
        deliver_error!(422, errors: page.errors)
      end
    end

    page.write_cname(attributes[:cname].to_s, current_user) if attributes.key?(:cname)

    # Switch pages visibilities
    if GitHub.flipper[:private_pages].enabled?(current_user) && attributes.key?(:public) && page.public != attributes[:public]
      page.update_attribute(:public, attributes[:public])
      # Publish visibility change event to Hydro
      GlobalInstrumenter.instrument "pages.visibility_change", {
        actor: current_user,
        page: page,
        public: page.public
      }
    end

    if page.save
      deliver_empty(status: 204)
    else
      deliver_error!(422, errors: page.errors)
    end
  end

  # build page; not currently exposed
  post "/repositories/:repository_id/pages/builds" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#request-a-github-pages-build"

    receive_with_schema("page", "build")

    repo = find_repo!
    control_access :build_pages, resource: repo, allow_integrations: false, allow_user_via_integration: true

    error_message = "The repository does not have a GitHub Pages site. See #{GitHub.developer_help_url}/v3/repos/pages/"
    deliver_error! 403, message: error_message unless repo.has_gh_pages?

    repo.rebuild_pages(current_user)
    pending = {status: "queued", url: "#{@current_url}/latest"}
    deliver_raw pending, status: 201
  end

  # get page builds
  get "/repositories/:repository_id/pages/builds" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#list-github-pages-builds"
    repo = find_repo!
    control_access :read_pages, resource: repo, allow_integrations: true, allow_user_via_integration: true

    page = repo.page
    record_or_404(page)

    builds = paginate_rel(page.builds)

    pusher_ids = builds.map { |b| b.pusher_id.to_i }
    pusher_ids.uniq!
    pusher_ids.delete 0
    users = (pusher_ids.present? ?
      User.where(id: pusher_ids) :
      []).index_by { |u| u.id }
    builds.each do |build|
      build.pusher = users[build.pusher_id]
    end

    deliver :page_build_hash, builds, repo: repo
  end

  # get latest page build
  get "/repositories/:repository_id/pages/builds/latest" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#get-latest-pages-build"
    repo = find_repo!
    control_access :read_pages, resource: repo, allow_integrations: true, allow_user_via_integration: true
    deliver_error!(404) unless page = repo.page
    deliver_error!(404) unless build = page.builds.first
    deliver :page_build_hash, build, repo: repo, last_modified: calc_last_modified(build)
  end

  # get single page build
  get "/repositories/:repository_id/pages/builds/:build_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#get-github-pages-build"
    repo = find_repo!
    control_access :read_pages, resource: repo, allow_integrations: true, allow_user_via_integration: true
    deliver_error!(404) unless page = repo.page
    deliver_error!(404) unless build = page.builds.find_by_id(int_id_param!(key: :build_id))
    deliver :page_build_hash, build, repo: repo, last_modified: calc_last_modified(build)
  end
end
