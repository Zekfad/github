# frozen_string_literal: true

module Api::Serializer::OrganizationTeamSyncDependency
  # Creates a Hash to be serialized to JSON.
  #
  # group - group returned from group-syncer
  # Returns a Hash if the group is present.
  def group_hash(group, options = {})
    {
      group_id: group.id,
      group_name: group.name,
      group_description: group.description,
    }
  end

  # mapping - group_mapping
  # Returns a Hash if the mapping is present.
  def mapping_hash(mapping, options = {})
    {
      group_id: mapping.group_id,
      group_name: mapping.group_name,
      group_description: mapping.group_description,
    }
  end

end
