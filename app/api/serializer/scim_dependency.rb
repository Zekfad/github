# frozen_string_literal: true

module Api::Serializer::ScimDependency

  # Creates a SCIM error hash to be serialized to JSON.
  #
  # error - A SCIM::ErrorResponse instance.
  #
  # Returns a Hash.
  def scim_error_hash(error, options = {})
    {
      schemas: [SCIM::ERROR_SCHEMA],
      status: error.status,
      scimType: error.scim_type,
      detail: error.detail,
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # identity - ExternalIdentity instance.
  #
  # Returns a Hash if the ExternalIdentity exists, or nil.
  def scim_identity_hash(identity, options = {})
    return nil if !identity

    scim_data = identity.scim_user_data

    attributes = identity_attributes_hash(
      identity,
      url_path: "/scim/v2/organizations/#{ identity.target.login }/Users/#{ identity.guid }",
      is_active: !identity.destroyed?,
    )

    scim_data.as_json(skip_groups: true).merge(attributes)
  end

  # Creates a Hash for an enterprise identity to be serialized to JSON.
  #
  # identity - ExternalIdentity instance.
  #
  # Returns a Hash if the ExternalIdentity exists, or nil.
  def enterprise_scim_identity_hash(identity, options = {})
    return nil unless identity

    scim_data = identity.scim_user_data

    attributes = identity_attributes_hash(
      identity,
      url_path: "/scim/v2/enterprises/#{ identity.target.slug }/Users/#{ identity.guid }",
      is_active: !identity.destroyed? && scim_data.active?,
    )

    # TODO: review when implementing SCIM Groups endpoint
    attributes[:groups] = scim_groups_array(
      Platform::Provisioning::GroupsUserDataWrapper.new(scim_data).groups_attributes
    )

    scim_data.as_json(skip_groups: true).merge(attributes)
  end

  # Creates a Hash for a SCIM group.
  #
  # identity - ExternalIdentity instance.
  # options  - Hash of options. Supported options include:
  #            - :excluded_attributes - String of comma-separated values
  #              representing the names of attributes that should be excluded
  #              in the returned Hash.
  #
  # Returns a Hash if the ExternalIdentity exists, or nil.
  def enterprise_group_hash(identity, options = {})
    return nil unless identity

    scim_data = identity.scim_group_data

    result = {
      schemas: [SCIM::GROUP_SCHEMA],
      id: identity.guid,
      externalId: scim_data.external_id,
      displayName: scim_data.display_name,
      members: enterprise_scim_member_array(identity),
      meta: {
        resourceType: "Group",
        created: identity.created_at,
        lastModified: identity.updated_at,
        location: url("/scim/v2/enterprises/#{ identity.target.slug }/Groups/#{ identity.guid }"),
      }
    }

    # Filter the response if the :excluded_attributes option is provided
    if options[:excluded_attributes].present?
      excluded_attributes = options[:excluded_attributes].split(",").map do |attr|
        attr = attr.strip.to_sym
        # Never let the :schemas or :id attributes be excluded
        next if attr == :schemas || attr == :id
        attr
      end.compact

      excluded_attributes.each do |attr|
        result.delete(attr)
      end
    end

    result
  end

  def enterprise_scim_member_array(identity)
    members = identity.prefilled_group_members
    members ||= begin
      identity.provider.external_identities_for_organization(identity.user).
        provisioned_by(:scim).
        includes(:identity_attribute_records)
    end
    members.map do |user_identity|
      user_scim_data = user_identity.scim_user_data
      {
        :value => user_identity.guid,
        :$ref => url("/scim/v2/enterprises/#{ identity.target.slug }/Users/#{ user_identity.guid }"),
        :display => (user_scim_data.display_name || user_identity.saml_user_data.name_id).to_s,
      }
    end
  end

  def enterprise_groups_hash(identities, options = {})
    scim_list_hash(identities) do |resource|
      enterprise_group_hash(resource, options)
    end
  end

  def scim_identities_hash(results, options = {})
    scim_list_hash(results) do |resource|
      scim_identity_hash(resource)
    end
  end

  def enterprise_scim_identities_hash(results, options = {})
    scim_list_hash(results) do |resource|
      enterprise_scim_identity_hash(resource)
    end
  end

  private

  def identity_attributes_hash(identity, url_path:, is_active:)
    {
      schemas: [SCIM::USER_SCHEMA],
      id: identity.guid,
      active: is_active,
      meta: {
        resourceType: "User",
        created: identity.created_at,
        lastModified: identity.updated_at,
        location: url(url_path),
      },
    }
  end

  # Internal: Transforms the Array of groups as stored internally separated into
  # a SCIM-compliant structure
  def scim_groups_array(groups)
    groups.map do |group|
      group.slice(:value)
    end
  end

  # Internal: generates a SCIM list type result with pagination
  def scim_list_hash(results)
    {
      schemas: [SCIM::LIST_SCHEMA],
      totalResults: results.total_results,
      itemsPerPage: results.items_per_page,
      startIndex: results.start_index,
      Resources: results.resources.map do |resource|
        yield(resource)
      end,
    }
  end
end
