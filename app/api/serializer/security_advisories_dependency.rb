# frozen_string_literal: true

module Api::Serializer::SecurityAdvisoriesDependency
  def security_advisory_hash(advisory, options = {})
    return nil unless advisory

    # Coerce to the correct subclass.
    if advisory.class == Vulnerability
      advisory = advisory.becomes(SecurityAdvisory)
    end

    # Just in case!
    return nil unless advisory.disclosed?

    {
      ghsa_id: advisory.ghsa_id,
      summary: advisory.summary,
      description: advisory.description,
      severity: advisory.severity,
      identifiers: advisory.identifiers,
      references: advisory.references,
      published_at: time(advisory.published_at),
      updated_at: time(advisory.updated_at),
      withdrawn_at: time(advisory.withdrawn_at),
      vulnerabilities: advisory.vulnerabilities.map { |vulnerability|
        security_vulnerability_hash(vulnerability)
      },
    }
  end

  def security_vulnerability_hash(vulnerability, options = {})
    return nil unless vulnerability

    # Coerce to the correct subclass.
    if vulnerability.class == VulnerableVersionRange
      vulnerability = vulnerability.becomes(SecurityVulnerability)
    end

    {
      package: vulnerability.package,
      severity: vulnerability.severity,
      vulnerable_version_range: vulnerability.vulnerable_version_range,
      first_patched_version: vulnerability.first_patched_version,
    }
  end
end
