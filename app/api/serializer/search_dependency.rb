# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::SearchDependency
  # Public
  def label_search_result_hash(results, options = {})
    item_serializer = lambda do |result|
      Api::Serializer.serialize(:search_label_hash, result, options)
    end

    search_result_hash(results, item_serializer, options)
  end

  # Public
  def user_search_result_hash(results, options = {})
    item_serializer = lambda do |result|
      resource = result["_model"]
      Api::Serializer.serialize(:search_user_hash, resource, options)
    end

    search_result_hash(results, item_serializer, options)
  end

  # Public
  def topic_search_result_hash(results, options = {})
    item_serializer = lambda do |result|
      Api::Serializer.serialize(:search_topic_hash, result, options)
    end

    search_result_hash(results, item_serializer, options)
  end

  # Public
  def repo_search_result_hash(results, options = {})
    return nil unless results

    repos = results.results.map { |result| result["_model"] }
    # It's necessary to prefill internal repo associations here because the
    # `disabled?` check during serialization loads it in order to look for
    # an OFAC flag on the business.
    GitHub::PrefillAssociations.for_repositories(repos, internal: options.current_user&.businesses&.any?)

    item_serializer = lambda do |result|
      resource = result["_model"]
      Api::Serializer.serialize(:repository_hash, resource, options)
    end

    search_result_hash(results, item_serializer, options)
  end

  # Public
  def issue_search_result_hash(results, options = {})
    return nil unless results

    issues = results.results.map { |result| result["_model"] }
    GitHub::PrefillAssociations.for_issues(issues)
    Reaction::Summary.prefill(issues)
    options.search = true

    item_serializer = lambda do |result|
      resource = result["_model"]
      Api::Serializer.serialize(:issue_hash, resource, options)
    end

    search_result_hash(results, item_serializer, options)
  end

  # Public
  def code_search_result_hash(results, options = {})
    item_serializer = lambda do |result|
      Api::Serializer.serialize(:code_search_result_item_hash, result, options)
    end

    search_result_hash(results, item_serializer, options)
  end

  def commit_search_result_hash(results, options = {})
    item_serializer = lambda do |result|
      Api::Serializer.serialize(:commit_search_result_item_hash, result)
    end

    search_result_hash(results, item_serializer, options)
  end

  # Internal
  def search_result_hash(results, item_serializer, options = {})
    return nil unless results

    item_hashes = results.results.map do |result|
      hash = item_serializer.call(result)
      # Don't publish the actual score here, because that can be used
      # to detect terms in _private_ documents in the index
      # https://github.com/github/github/issues/133923#issue-557092962
      hash[:score] = 1.0
      hash[:text_matches] = text_matches(result) if options[:highlight]
      hash
    end

    {
      total_count: results.total,
      incomplete_results: results.timed_out,
      items: item_hashes,
    }
  end

  # Internal
  def code_search_result_item_hash(result, options)
    options = Api::SerializerOptions.from(options)
    repo = result["_model"]
    file_info = result["_source"]
    file_path = file_info["path"]
    blob_sha = file_info["blob_sha"]
    commit_sha = file_info["commit_sha"]

    file_name = file_info["filename"]
    file_path = [file_path, file_name].compact.join("/")

    hash = {
      name: file_name,
      path: file_path,
      sha: blob_sha,
      url: encode(url("/repositories/#{repo.id}/contents/#{file_path}?ref=#{commit_sha}")),
      git_url: url("/repositories/#{repo.id}/git/blobs/#{blob_sha}"),
      html_url: encode("#{repo.permalink}/blob/#{commit_sha}/#{file_path}"),
      repository: simple_repository_hash(result["_model"], options),
    }

    if options.accepts_version?("extended-search-results")
      file_size = file_info["file_size"]
      language = file_info["language"]
      last_modified_at = file_info["timestamp"]
      highlights_line_numbers = line_numbers(result)
      hash.update \
        file_size: file_size,
        line_numbers: highlights_line_numbers,
        language: language,
        last_modified_at: time(last_modified_at)
    end
    hash
  end

  def commit_search_result_item_hash(result, options)
    options = Api::SerializerOptions.from(options)

    repo = result["_model"]
    doc = result["_source"]

    commit = Commit.new(repo, {
      "oid" => doc["hash"],
    })

    {
      url: url("/repos/#{repo.nwo}/commits/#{doc["hash"]}"),
      sha: doc["hash"],
      node_id: commit.global_relay_id,
      html_url: "#{repo.permalink}/commit/#{doc["hash"]}",
      comments_url: url("/repos/#{repo.nwo}/commits/#{doc["hash"]}/comments"),
      commit: {
        url: url("/repos/#{repo.nwo}/git/commits/#{doc["hash"]}"),
        author: {
          date: doc["author_date"],
          name: doc["author_name"],
          email: doc["author_email"],
        },
        committer: {
          date: doc["committer_date"],
          name: doc["committer_name"],
          email: doc["committer_email"],
        },
        message: doc["message"],
        tree: {
          url: url("/repos/#{repo.nwo}/git/trees/#{doc["tree_hash"]}"),
          sha: doc["tree_hash"],
        },
        comment_count: result["_comment_count"],
      },
      author: user_hash(User.find_by_id(doc["author_id"]), content_options(options)),
      committer: user_hash(User.find_by_id(doc["committer_id"]), content_options(options)),
      parents: doc["parent_hashes"].map do |hash|
        {
          url: url("/repos/#{repo.nwo}/commits/#{hash}"),
          html_url: "#{repo.permalink}/commit/#{hash}",
          sha: hash,
        }
      end,
      repository: simple_repository_hash(repo, options),
    }
  end

  # Internal: Converts an array of results from the index into an array of hashes.
  # See http://git.io/eTeKGA for context
  def array_to_hash(source)
    result = []
    source.each do |a|
      result << { "text" => a }
    end
    result
  end

  # Internal: Serialize the highlights for the given search result in the
  # format appropriate for use in the 'text_matches' property of the JSON
  # response.
  #
  # result - The Hash containing a single search result.
  #
  # Returns an Array of Hashes.
  def text_matches(result)
    field_names_and_matches = result.fetch("highlight", [])

    field_names_and_matches.map do |field_name, matches|
      matches.map do |match|
        Api::Serializer::TextMatch.to_hash(result, field_name, match)
      end
    end.flatten.compact.uniq
  end

  # Takes the given string and replaces `\r` and `\r\n` with a single `\n`
  # character. Trailing whitepsace is also removed.
  #
  # str - The String to clean up.
  #
  # Returns a new string.
  #
  def cleanup_line_endings(str)
    str = str.rstrip
    str.gsub!(/\r\n?/m, "\n")
    str
  end

  # Internal: This method will return the Range of line numbers in the file
  # where the highlight is found.
  #
  # Returns a Range of line numbers in the file.
  #
  def line_numbers(result)
    file = cleanup_line_endings(result["_source"]["file"])
    highlights = result["highlight"]["file"]
    ln = []

    return ln if highlights.nil?

    highlights.each do |highlight|
      clean = cleanup_line_endings(highlight[:text])
      reg = Regexp.new(Regexp.escape(clean))

      char_offset = file.index(reg)
      if !char_offset.nil?
        first_line = file.slice(0, char_offset).count("\n")
        last_line = first_line + clean.count("\n")

        range = first_line..last_line
        ln << range
      end
    end

    ln
  end
end
