# frozen_string_literal: true

module Api::Serializer::ActionsRunnersDependency
  # Creates a hash to be serialized to JSON
  #
  # data - Must be a hash with token and url keys
  #
  def actions_runner_registration_hash(data, options = {})
    {
      token: data[:token],
      token_schema: data[:token_schema],
      url: data[:url],
    }
  end

  def actions_runners_hash(data, options = {})
    data[:runners] ||= []
    runner_hashes = data[:runners].map do |runner|
      actions_runner_hash(runner, options)
    end

    {
      total_count: data[:total_count],
      runners: runner_hashes,
    }
  end

  def actions_runner_hash(runner, options = {})
    payload = { id: runner.id, name: runner.name, os: runner.os, status: runner.status, busy: runner.current_parallelism != 0 || runner.assigned_request.present? }

    if GitHub.flipper[:actions_api_runner_labels].enabled?(options[:current_user])
      payload[:labels] = runner.labels.map do |label|
        actions_runner_label_hash(label)
      end
    end

    payload
  end

  LABEL_TYPE_MAPPING = {
    "user" => "custom",
    "system" => "read-only"
  }

  def actions_runner_label_hash(label, _options = {})
    { id: label.id, name: label.name, type: LABEL_TYPE_MAPPING[label.type] }
  end

  def actions_enterprise_runner_groups_hash(data, options = {})
    data[:runner_groups] ||= []
    runner_group_hashes = data[:runner_groups].map do |runner_group|
      actions_enterprise_runner_group_hash({ runner_group: runner_group, enterprise: data[:enterprise] })
    end

    {
      total_count: data[:total_count],
      runner_groups: runner_group_hashes,
    }
  end

  def actions_enterprise_runner_group_hash(data, options = {})
    runner_group = data[:runner_group]
    enterprise = data[:enterprise]

    runner_group_hash = actions_runner_group_hash(runner_group, options)
    return nil unless runner_group_hash

    runner_group_hash.tap do |hash|
      if runner_group.visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED
        hash[:selected_organizations_url] = url("#{enterprise_runner_group_path(enterprise, runner_group)}/organizations", options)
      end

      hash[:runners_url] = url("#{enterprise_runner_group_path(enterprise, runner_group)}/runners", options)
    end
  end

  def actions_org_runner_groups_hash(data, options = {})
    data[:runner_groups] ||= []
    runner_group_hashes = data[:runner_groups].map do |runner_group|
      actions_org_runner_group_hash({ runner_group: runner_group, org: data[:org] })
    end

    {
      total_count: data[:total_count],
      runner_groups: runner_group_hashes,
    }
  end

  def actions_org_runner_group_hash(data, options = {})
    runner_group = data[:runner_group]
    org = data[:org]

    runner_group_hash = actions_runner_group_hash(runner_group, options)
    return nil unless runner_group_hash

    runner_group_hash.tap do |hash|
      if runner_group.visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED
        hash[:selected_repositories_url] = url("#{org_runner_group_path(org, runner_group)}/repositories", options)
      end

      hash[:runners_url] = url("#{org_runner_group_path(org, runner_group)}/runners", options)

      hash[:inherited] = runner_group.owner_id.present? && runner_group.owner_id.global_id != ""
    end
  end

  def actions_runner_group_organizations_hash(data, options = {})
    organization_hashes = (data[:organizations] || []).map do |organization|
      organization_hash(organization, options)
    end

    {
      total_count: data[:total_count],
      organizations: organization_hashes
    }
  end

  def actions_runner_group_repositories_hash(data, options = {})
    repository_hashes = (data[:repositories] || []).map do |repository|
      simple_repository_hash(repository, options)
    end

    {
      total_count: data[:total_count],
      repositories: repository_hashes,
    }
  end

  private

  def actions_runner_group_hash(runner_group, _options = {})
    return nil unless runner_group

    {
      id: runner_group.id,
      name: runner_group.name,
      visibility: GitHub::LaunchClient::RunnerGroups::TO_VISIBILITY_MAP[runner_group.visibility],
      default: runner_group.is_default || runner_group.id == 1,
    }
  end

  def enterprise_runner_group_path(enterprise, runner_group)
    "/enterprises/#{enterprise.to_param}/actions/runner-groups/#{runner_group.id}"
  end

  def org_runner_group_path(org, runner_group)
    "/orgs/#{org.name}/actions/runner-groups/#{runner_group.id}"
  end
end
