# frozen_string_literal: true

module Api::Serializer::VulnerabilitiesDependency


  VULNERABILITY_ATTRIBUTES_FOR_ENTERPRISE = %w(
    classification
    created_at
    description
    external_identifier
    external_reference
    ghsa_id
    id
    cve_id
    white_source_id
    platform
    published_at
    severity
    status
    withdrawn_at
  ).freeze

  VULNERABLE_VERSION_RANGE_ATTRIBUTES_FOR_ENTERPRISE = %w(
    affects
    ecosystem
    fixed_in
    id
    requirements
  ).freeze

  def vulnerability_hash(vulnerability, options = {})
    return unless vulnerability

    hash = vulnerability.attributes.slice(*VULNERABILITY_ATTRIBUTES_FOR_ENTERPRISE)

    hash[:vulnerable_version_ranges] = vulnerability.vulnerable_version_ranges.map do |range|
      range.attributes.slice(*VULNERABLE_VERSION_RANGE_ATTRIBUTES_FOR_ENTERPRISE)
    end

    hash.symbolize_keys
  end
end
