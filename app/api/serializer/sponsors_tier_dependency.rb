# frozen_string_literal: true

module Api::Serializer::SponsorsTierDependency
  # Creates a Hash to be serialized to JSON.
  #
  # sponsors_tier - SponsorsTier instance
  #
  # Returns a Hash if the SponsorsTier exists, or nil.
  def sponsors_tier_hash(sponsors_tier)
    return nil unless sponsors_tier

    {
      node_id: sponsors_tier.global_relay_id,
      created_at: time(sponsors_tier.created_at),
      description: sponsors_tier.description,
      monthly_price_in_cents: sponsors_tier.monthly_price_in_cents,
      monthly_price_in_dollars: sponsors_tier.monthly_price_in_dollars,
      name: sponsors_tier.name,
    }
  end
end
