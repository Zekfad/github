# frozen_string_literal: true

module Api::Serializer::CouponDependency
  # Creates a Hash to be serialized to JSON.
  #
  # coupon_redemption  - CouponRedemption instance
  # options - Hash
  #
  # Returns a Hash if the coupon_redemption exists, or nil.
  def coupon_redemption_hash(coupon_redemption, options = nil)
    return nil if !coupon_redemption
    coupon = coupon_redemption.coupon
    {
      code: coupon.code,
      discount: coupon.human_discount,
      expires_at: time(coupon_redemption.expires_at),
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # coupon  - Coupon instance
  # options - Hash
  #
  # Returns a Hash if the coupon exists, or nil.
  def coupon_hash(coupon, _ = nil)
    return nil if !coupon
    {
      code: coupon.code,
      discount: coupon.human_discount,
      expires_at: time(coupon.expires_at),
    }
  end
end
