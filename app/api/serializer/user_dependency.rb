# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::UserDependency
  EMPLOYEE_IDS_REFRESH = 1.hour

  def simple_user_hash(user, options = {})
    return nil if !user

    options = Api::SerializerOptions.from(options)
    base_path = Api::LegacyEncode.encode("/users/#{user.login}", /\[|\]/)
    hash = {
      login: user.login,
      id: user.id,
      node_id: user.global_relay_id,
      avatar_url: avatar(user),
      gravatar_id: "",
      url: url(base_path, options),
      html_url: user.permalink,
      followers_url: url("#{base_path}/followers", options),
      following_url: url("#{base_path}/following{/other_user}", options),
      gists_url: url("#{base_path}/gists{/gist_id}", options),
      starred_url: url("#{base_path}/starred{/owner}{/repo}", options),
      subscriptions_url: url("#{base_path}/subscriptions", options),
      organizations_url: url("#{base_path}/orgs", options),
      repos_url: url("#{base_path}/repos", options),
      events_url: url("#{base_path}/events{/privacy}", options),
      received_events_url: url("#{base_path}/received_events", options),
      type: user[:type],
      site_admin: instance_admin?(user),
    }

    hash[:ldap_dn] = user.ldap_dn if user.ldap_mapped?

    hash
  end

  def search_user_hash(user, options = {})
    return nil if !user

    hash = simple_user_hash(user, options)
    options = Api::SerializerOptions.from(options)

    if options.accepts_version?("extended-search-results")
      profile = user.profile

      hash.update \
        name: fetch_field(profile, :name),
        location: fetch_field(profile, :location),
        email: user.publicly_visible_email(logged_in: !options[:exclude_email]),
        hireable: fetch_field(profile, :hireable),
        bio: fetch_field(profile, :bio),
        public_repos: user.repository_counts.public_repositories,
        public_gists: user.repository_counts.public_gists,
        followers: user.followers_count,
        following: user.following_count,
        created_at: time(user.created_at),
        updated_at: time(user.updated_at)
    end
    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # user    - User instance.
  # options - Hash
  #           :repo - Repository instance that the user is a collaborator on.
  #
  # Returns a Hash if the User and Repository exist, or nil.
  def collaborator_hash(user, options = {})
    options = Api::SerializerOptions.from(options)
    repo    = options[:repo]

    return nil unless user.present?
    return nil unless repo.present?

    hash = simple_user_hash(user, options)
    hash.update(requested_identity_attrs(user, options: options))
    hash.update(permissions: permissions_hash(repo, user: user))

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # user    - User instance.
  # options - Hash
  #           :full          - Boolean specifying we want the extended output.
  #           :private       - Boolean specifying we want the private output.
  #           :business_plus - Boolean specifying we want the business plan output.
  #           :exclude_email - Boolean specifying supressing the user's profile email.
  #
  # Returns a Hash if the User exists, or nil.
  def user_hash(user, options = {})
    return nil if !user

    hash = simple_user_hash(user, options)
    options = Api::SerializerOptions.from(options)

    include_details =  options[:full] || options[:private]

    if include_details
      profile = options[:profile] || user.profile
      hash.update \
        name: fetch_field(profile, :name),
        company: fetch_field(profile, :company),
        blog: fetch_field(profile, :blog).to_s,
        location: fetch_field(profile, :location),
        email: user.publicly_visible_email(logged_in: !options[:exclude_email]),
        hireable: fetch_field(profile, :hireable),
        bio: fetch_field(profile, :bio), # DEPRECATED: Will be removed in API v4.
        twitter_username: fetch_field(profile, :twitter_username)
    end

    hash.update(requested_identity_attrs(user, options: options))

    if include_details
      hash.update \
        public_repos: user.repository_counts.public_repositories,
        public_gists: user.repository_counts.public_gists,
        followers: user.followers_count,
        following: user.following_count,
        created_at: time(user.created_at),
        updated_at: time(user.updated_at)

      hash[:suspended_at] = user.suspended_at if GitHub.enterprise?
    end

    if options[:private]
      hash.update \
        private_gists: user.repository_counts.private_gists,
        total_private_repos: user.repository_counts.private_repositories,
        owned_private_repos: user.repository_counts.owned_private_repositories,
        disk_usage: user.disk_usage,
        collaborators: user.collaborators_count,
        two_factor_authentication: user.two_factor_authentication_enabled?

      if !GitHub.enterprise?
        hash.update(business_plus: user.business_plus?) if options[:business_plus]

      end
    end

    if (options[:private] || options[:plan]) && !GitHub.enterprise?
      hash.update \
        plan: {
          name: user.plan.display_name,
          space: user.plan.space / 1.kilobyte,
          collaborators: 0,
          private_repos: user.plan.repos,
        }
    end

    hash
  end

  HovercardFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Hovercard {
      contexts {
        message
        octicon
      }
    }
  GRAPHQL

  def graphql_hovercard_hash(hovercard, options = {})
    hovercard = HovercardFragment.new(hovercard)

    {
      contexts: hovercard.contexts.map { |context|
        {
          message: context.message,
          octicon: context.octicon,
        }
      },
    }
  end

  SimpleHumanUserFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Node {
      id
      ...on RepositoryOwner {
        login
        databaseId
        url
        avatarUrl
      }
      ...on User {
        isSiteAdmin
        isEmployee
        ldapDN
      }
    }
  GRAPHQL

  SimpleUserFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Node {
      id
      ...on RepositoryOwner {
        login
        databaseId
        url
        avatarUrl
      }
      ...on User {
        isSiteAdmin
        isEmployee
        ldapDN
      }
      ...on Bot {
        login
        databaseId
        url
        avatarUrl
      }
      ...on Mannequin {
        login
        databaseId
        url
        avatarUrl
      }
    }
  GRAPHQL

  def graphql_simple_user_hash(user, options = {})
    options = Api::SerializerOptions.from(options)
    user = SimpleUserFragment.new(user)

    return unless user

    make_simple_graphql_user_hash(user, options)
  end

  def graphql_simple_human_user_hash(user, options = {})
    options = Api::SerializerOptions.from(options)
    user = SimpleHumanUserFragment.new(user)

    return unless user

    make_simple_graphql_user_hash(user, options, false)
  end

  FullUserFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on User {
      ...Api::Serializer::UserDependency::SimpleUserFragment

      name
      company
      websiteUrl
      twitterUsername
      location
      email
      isHireable
      bio

      publicRepos: repositories(privacy:PUBLIC, affiliations:[OWNER]) {
        totalCount
      }

      publicGists: gists(privacy:PUBLIC) {
        totalCount
      }

      followers {
        totalCount
      }

      following {
        totalCount
      }

      createdAt
      updatedAt

      suspendedAt

      plan {
        displayName
        allowedDiskSize
        allowedPrivateRepositoriesCount
      }
    }
  GRAPHQL

  def graphql_full_user_hash(user, options = {})
    user = FullUserFragment.new(user)
    hash = graphql_simple_user_hash(user, options)

    hash.update(
      name:         fetch_field(user, :name),
      company:      fetch_field(user, :company),
      blog:         fetch_field(user, :website_url).to_s,
      location:     fetch_field(user, :location),
      email:        fetch_field(user, :email),
      hireable:     fetch_field(user, :is_hireable),
      bio:          fetch_field(user, :bio),
      public_repos: user.public_repos.total_count,
      public_gists: user.public_gists.total_count,
      followers:    user.followers.total_count,
      following:    user.following.total_count,
      created_at:   time(user.created_at),
      updated_at:   time(user.updated_at),
      twitter_username: fetch_field(user, :twitter_username),
    )

    # FIXME: Should this be time(user.suspended_at)? That's not how the old
    # REST implementation did it.
    hash[:suspended_at] = user.suspended_at if GitHub.enterprise?

    if options[:plan] && !GitHub.enterprise?
      hash[:plan] = {
        name: user.plan.display_name,
        space: user.plan.allowed_disk_size,
        collaborators: 0,
        private_repos: user.plan.allowed_private_repositories_count,
      }
    end

    hash
  end

  PrivateUserFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on User {
      ...Api::Serializer::UserDependency::FullUserFragment

      privateGists: gists(privacy:SECRET) {
        totalCount
      }

      totalPrivateRepos: repositories(privacy:PRIVATE, affiliations:[OWNER]) {
        totalCount
      }

      ownedPrivateRepos: repositories(isFork:false, privacy:PRIVATE, affiliations:[OWNER], isLocked: false) {
        totalCount
      }

      diskUsageRepos: repositories(isFork:false, affiliations:[OWNER]) {
        totalDiskUsage
      }

      collaboratorsCount

      hasTwoFactorAuthenticationEnabled

      plan {
        displayName
        allowedDiskSize
        allowedPrivateRepositoriesCount
      }
    }
  GRAPHQL

  def graphql_private_user_hash(user, options = {})
    hash = graphql_full_user_hash(user, options)

    hash.update(
      private_gists:              user.private_gists.total_count,
      total_private_repos:        user.total_private_repos.total_count,
      owned_private_repos:        user.owned_private_repos.total_count,
      disk_usage:                 user.disk_usage_repos.total_disk_usage,
      collaborators:              user.collaborators_count,
      two_factor_authentication:  user.has_two_factor_authentication_enabled,
    )

    unless GitHub.enterprise?
      hash[:plan] = {
        name: user.plan.display_name,
        space: user.plan.allowed_disk_size,
        collaborators: 0,
        private_repos: user.plan.allowed_private_repositories_count,
      }
    end

    hash
  end

  # Internal: Determine whether the API response can safely honor the
  # `user-identity` media type parameter.
  #
  # If the user profile is not already loaded (i.e., "prefilled"), then the API
  # method probably doesn't officially support the `user-identity` parameter.
  # Prefilling is essential for any API methods that can return a list of users.
  # Failure to prefill the profile would lead to an n+1 query loading the
  # profile for each user in the list.
  #
  # user - A User instance.
  #
  # Returns a Boolean.
  def supports_user_identity_param?(user)
    user.association(:profile).loaded?
  end

  # Builds an uber-minimal User hash to use when autocompleting users.
  #
  # user    - User instance.
  # options - Hash of valid Api::Serializer options
  #
  # Returns a Hash if the User exists, or nil.
  def mentionable_user_hash(user, options = {})
    return nil if !user

    profile = user.profile
    {
      login: user.login,
      name: profile && profile.name,
      email: user.public_attribution_email,
      avatar_url: avatar(user),
    }
  end

  # Builds a contributor hash based on raw data returned from
  # Repository#contributors
  #
  # contributor - The [User|Hash, Integer] pair. Integer is contributions count.
  # options - Hash of valid Api::Serializer options
  #
  # Returns a Hash if the user is present, or nil.
  def contributor_hash(contributor, options = {})
    user, count = contributor
    hash = case user
    when User
      user_hash(user, content_options(options))
    when Hash
      user.merge(type: "Anonymous")
    else
      raise ArgumentError, "expected user to be User or Hash"
    end

    hash.update(contributions: count)
  end

  # Creates a Hash to be serialized to JSON.
  #
  # email   - UserEmail instance
  # options - Hash
  #
  # Returns a Hash if the email exists, or nil.
  def user_email_hash(email, options = {})
    return nil if !email

    options = Api::SerializerOptions.from(options)
    if options.wants_beta_media_type?
      # < v3 deprecated
      email.email
    else
      {}.tap do |email_hash|
        email_hash[:email]    = email.email
        email_hash[:primary]  = email.primary?
        email_hash[:verified] = GitHub.email_verification_enabled? ? email.verified? : true
        email_hash[:visibility] = email.primary? ? email.visibility : nil

        if options.full
          email_hash[:user] = user_hash(email.user, private: true, business_plus: true)
        end
      end
    end
  end

  # Creates a Hash to be serialized to JSON.
  #
  # public_key - PublicKey instance
  # options    - Hash
  #
  # Returns a Hash if the public_key exists, or nil.
  def public_key_hash(public_key, options = {})
    return nil if !public_key
    options = Api::SerializerOptions.from(options)
    hash = {
      id: public_key.id,
      key: public_key.key,
    }

    if options.simple
      hash
    else
      url_suffix = if public_key.repository.present?
        "/repos/#{public_key.repository.nwo}/keys/#{public_key.id}"
      else
        "/user/keys/#{public_key.id}"
      end

      hash.update \
        url: url(url_suffix, options),
        title: public_key.title,
        verified: public_key.verified?,
        created_at: time(public_key.created_at),
        read_only: public_key.read_only?
    end

    if options.detail
      hash.update \
        last_used: public_key.accessed_at,
        user_id: public_key.user_id,
        repository_id: public_key.repository_id
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # gpg_key - GpgKey instance
  # options - Hash
  #
  # Returns a Hash if the gpg_key exists, or nil.
  def gpg_key_hash(gpg_key, options = {})
    return nil if !gpg_key

    subkeys = gpg_key.subkeys.map { |sk| gpg_key_hash(sk, options) }
    emails = gpg_key.emails.map do |email|
      {email: email.email, verified: gpg_key.allowed_email?(email.email)}
    end

    {
      id: gpg_key.id,
      primary_key_id: gpg_key.primary_key_id,
      key_id: gpg_key.hex_key_id,
      raw_key: gpg_key.raw_key,
      public_key: Base64.strict_encode64(gpg_key.public_key),
      emails: emails,
      subkeys: subkeys,
      can_sign: gpg_key.can_sign,
      can_encrypt_comms: gpg_key.can_encrypt_comms,
      can_encrypt_storage: gpg_key.can_encrypt_storage,
      can_certify: gpg_key.can_certify,
      created_at: gpg_key.created_at,
      expires_at: gpg_key.expires_at,
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # access - OauthAccess.
  #
  # Returns a Hash if the OauthAccess exists, or nil.
  def oauth_access_hash(access, options = nil)
    return nil if !access
    options = Api::SerializerOptions.from(options)
    hash = {
      id: access.id,
      url: url("/authorizations/#{access.id}"),
      app: oauth_application_hash(access.safe_app),
      token: options[:token].to_s,
      hashed_token: access.hashed_token(hex: true),
      token_last_eight: access.token_last_eight,
      note: access.description,
      note_url: access.note_url,
      created_at: time(access.created_at),
      updated_at: time(access.updated_at),
      scopes: access.scopes,
      fingerprint: access.fingerprint,
    }

    hash[:user] = simple_user_hash(access.user, content_options(options)) if options && options[:user]

    if (installation = options[:installation])
      hash[:installation] = scoped_installation_hash(installation, content_options(options))
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # authorization - OauthAuthorization.
  #
  # Returns a Hash if the OauthAuthorization exists, or nil.
  def oauth_authorization_hash(authorization, options = nil)
    return nil if !authorization
    hash = {
      id: authorization.id,
      url: url("/applications/grants/#{authorization.id}"),
      app: oauth_application_hash(authorization.safe_app),
      created_at: time(authorization.created_at),
      updated_at: time(authorization.updated_at),
      scopes: authorization.scopes,
    }

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # access - OauthApplication.
  #
  # Returns a Hash if the OauthApplication exists, or nil.
  def oauth_application_hash(app, options = nil)
    return nil if !app
    {
      name: app.name,
      url: app.url,
      client_id: app.key,
    }
  end

  # Determine if a given user is an instance admin, for serializing the
  # site_admin field on user resources.
  #
  # An instance admin is defined as:
  #
  # GitHub.com: staff users
  # GitHub Enterprise: site admins
  #
  # Returns a Boolean.
  def instance_admin?(user)
    return false unless user.user?

    user.site_admin_without_two_factor_check? || github_employee?(user)
  end

  private
  # Fetch the value of a field
  #
  # obj - e.g. a Profile
  # name - name of the field to lookup
  #
  # Returns the value of the field, or nil, if the value is nil or empty
  def fetch_field(obj, name)
    obj && obj.send(name).presence
  end

  # Get the identity attributes for the user if they were requested
  #
  # user    - The user whose identity information should be added to the hash.
  # options - API options that we'll use to check whether or not the identity
  #           info was requested.
  #
  # Returns a hash containing the user's identity attributes, or an empty hash
  # if identity was not requested.
  def requested_identity_attrs(user, options:)
    if options.accepts_param?(:'user-identity') && supports_user_identity_param?(user)
      { name: user.profile_name, email: user.profile_email }
    else
      {}
    end
  end

  # Check if a user is in the cached list of GitHub employees.
  #
  # user - a User object.
  #
  # Returns a Boolean.
  def github_employee?(user)
    return false unless GitHub.require_employee_for_site_admin?
    github_employee_user_ids.include?(user.id)
  end

  # Internal: Cache GitHub employees team user IDs for faster serialization of
  # site_admin field on user resources.
  #
  # Returns an Array of integers.
  def github_employee_user_ids
    return @github_employee_user_ids if defined?(@github_employee_user_ids)

    return [] unless GitHub.require_employee_for_site_admin?
    key = "github_employee_ids"
    @github_employee_user_ids = GitHub.cache.fetch(key, ttl: EMPLOYEE_IDS_REFRESH) do
      if team = GitHub::FeatureFlag.employees_team
        team.member_ids
      else
        []
      end
    end
  end

  # Convert a User fragment into a hash
  # user graphQL fragment object (SimpleUserFragment or SimpleHumanUserFragment)
  # include_nonhumans Boolean - should we include nonhuman fields (Bots and Mannequins)
  # Returns a hash
  def make_simple_graphql_user_hash(user, options = {}, include_nonhumans = true)
    login = user.login

    if include_nonhumans && user.is_a?(Api::App::PlatformTypes::Bot)
      # See https://github.com/github/github/issues/93032#issuecomment-407761734
      login = "#{login}#{Bot::LOGIN_SUFFIX}"
    end

    base_path = Api::LegacyEncode.encode("/users/#{login}", /\[|\]/)
    html_url  = user.url.to_s

    if include_nonhumans && user.is_a?(Api::App::PlatformTypes::Mannequin)
      ghost_user_hash = simple_user_hash(User.ghost, content_options(options))
      # See https://github.com/github/github/issues/93032#issuecomment-407761734
      base_path = Api::LegacyEncode.encode("/users/#{ghost_user_hash[:login]}", /\[|\]/)
      html_url = ghost_user_hash[:url].to_s
    end

    avatar_url = user.avatar_url.to_s
    avatar_url += "?" unless avatar_url["?"]

    hash = {
      login:               login,
      id:                  user.database_id,
      node_id:             user.id,
      avatar_url:          avatar_url,
      gravatar_id:         "",
      url:                 url(base_path, options),
      html_url:            html_url,
      followers_url:       url("#{base_path}/followers", options),
      following_url:       url("#{base_path}/following{/other_user}", options),
      gists_url:           url("#{base_path}/gists{/gist_id}", options),
      starred_url:         url("#{base_path}/starred{/owner}{/repo}", options),
      subscriptions_url:   url("#{base_path}/subscriptions", options),
      organizations_url:   url("#{base_path}/orgs", options),
      repos_url:           url("#{base_path}/repos", options),
      events_url:          url("#{base_path}/events{/privacy}", options),
      received_events_url: url("#{base_path}/received_events", options),
      type:                user.__typename,
    }

    if user.is_a?(Api::App::PlatformTypes::User)
      if user.is_site_admin || user.is_employee
        hash[:site_admin] = true
      else
        hash[:site_admin] = false
      end
    else
      hash[:site_admin] = false
    end

    # FIXME: It'd be nice not to fetch user.ldap_dn at all if GitHub.auth.ldap?
    # is false. Right now we're doing the work to fetch that field and then
    # throwing it away.
    if user.is_a?(Api::App::PlatformTypes::User) && GitHub.auth.ldap? && user.ldap_dn?
      hash[:ldap_dn] = user.ldap_dn
    end

    hash
  end
end
