# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::RepositoriesDependency
  def disabled_repository_hash(repo, options = {})
    return nil if !repo

    message   = "Repository access blocked"

    if repo.access.disabled?
      reason    = repo.access.disabling_reason
      timestamp = repo.access.disabled_at
    elsif repo.network.nil?
      timestamp = repo.updated_at
    elsif repo.network_broken?
      reason    = "broken"
      timestamp = repo.network.updated_at
    elsif repo.trade_restricted?
      message = repo.trade_restriction_api_error_message(repo.actor)
      timestamp = repo.updated_at
    end

    valid_reasons = GitRepositoryAccess::REASONS - ["broken"]
    reason = "unavailable" unless valid_reasons.include?(reason)

    url = if GitHub.enterprise?
      "#{GitHub.enterprise_admin_help_url}/guides/installation/troubleshooting/"
    elsif reason == "dmca"
      repo.access.dmca_url
    else
      "#{GitHub.scheme}://#{GitHub.host_name}/tos"
    end

    response = {
                message: message,
                block: {
                  reason: reason,
                  created_at: time(timestamp),
                  html_url: url,
                },
               }

    response
  end

  def repository_identifier_hash(repo)
    return nil unless repo && repo.network.present?

    {
      id: repo.id,
      node_id: repo.global_relay_id,
      name: repo.name,
      full_name: repo.full_name,
      private: repo.private?,
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # repo    - Repository instance.
  # options - Hash
  #           :include_timestamps - Optional Boolean specifying whether to
  #                                 include repo timestamp information.
  #
  # Returns a Hash if the Repository exists, or nil.
  def simple_repository_hash(repo, options = {})
    return nil unless repo && repo.network.present?
    options = Api::SerializerOptions.from(options)
    repo_api_path = "/repos/#{repo.name_with_owner}"
    url = url(repo_api_path, options)

    h = {
      owner: user_hash(repo.owner, content_options(options)),
      html_url: repo.permalink,
      description: repo.description,
      fork: repo.fork?,
      url: url,
    }

    if options[:include_timestamps]
      h.update \
        created_at: time(repo.created_at),
        updated_at: time(repo.updated_at),
        pushed_at: time(repo.pushed_at)
    end

    repository_identifier_hash(repo).merge(h)
    .merge(repository_url_fields(url, repo_api_path, options))
  end

  SimpleRepositoryFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Repository {
      id
      name
      databaseId
      networkPresent
      isPrivate
      description
      permalink(includeHost: true)
      isFork
      owner {
        login
        ...Api::Serializer::UserDependency::SimpleUserFragment
      }
    }
  GRAPHQL

  def graphql_simple_repository_hash(repo, options = {})
    repo = SimpleRepositoryFragment.new(repo)
    return nil unless repo && repo.network_present
    options = Api::SerializerOptions.from(options)
    name_with_owner = "#{repo.owner.login}/#{repo.name}"
    repo_api_path = "/repos/#{name_with_owner}"
    url = url(repo_api_path, options)

    {
      id: repo.database_id,
      node_id: repo.id,
      name: repo.name,
      full_name: name_with_owner,
      owner: graphql_simple_user_hash(repo.owner, content_options(options)),
      private: repo.is_private,
      html_url: repo.permalink.to_s,
      description: repo.description,
      fork: repo.is_fork,
      url: url,
    }.merge(repository_url_fields(url, repo_api_path, options))
  end

  def repository_url_fields(url, repo_api_path, options)
    {
      url: url,
      forks_url: url("#{repo_api_path}/forks", options),
      keys_url: url("#{repo_api_path}/keys{/key_id}", options),
      collaborators_url: url("#{repo_api_path}/collaborators{/collaborator}", options),
      teams_url: url("#{repo_api_path}/teams", options),
      hooks_url: url("#{repo_api_path}/hooks", options),
      issue_events_url: url("#{repo_api_path}/issues/events{/number}", options),
      events_url: url("#{repo_api_path}/events", options),
      assignees_url: url("#{repo_api_path}/assignees{/user}", options),
      branches_url: url("#{repo_api_path}/branches{/branch}", options),
      tags_url: url("#{repo_api_path}/tags", options),
      blobs_url: url("#{repo_api_path}/git/blobs{/sha}", options),
      git_tags_url: url("#{repo_api_path}/git/tags{/sha}", options),
      git_refs_url: url("#{repo_api_path}/git/refs{/sha}", options),
      trees_url: url("#{repo_api_path}/git/trees{/sha}", options),
      statuses_url: url("#{repo_api_path}/statuses/{sha}", options),
      languages_url: url("#{repo_api_path}/languages", options),
      stargazers_url: url("#{repo_api_path}/stargazers", options),
      contributors_url: url("#{repo_api_path}/contributors", options),
      subscribers_url: url("#{repo_api_path}/subscribers", options),
      subscription_url: url("#{repo_api_path}/subscription", options),
      commits_url: url("#{repo_api_path}/commits{/sha}", options),
      git_commits_url: url("#{repo_api_path}/git/commits{/sha}", options),
      comments_url: url("#{repo_api_path}/comments{/number}", options),
      issue_comment_url: url("#{repo_api_path}/issues/comments{/number}", options),
      contents_url: url("#{repo_api_path}/contents/{+path}", options),
      compare_url: url("#{repo_api_path}/compare/{base}...{head}", options),
      merges_url: url("#{repo_api_path}/merges", options),
      archive_url: url("#{repo_api_path}/{archive_format}{/ref}", options),
      downloads_url: url("#{repo_api_path}/downloads", options),
      issues_url: url("#{repo_api_path}/issues{/number}", options),
      pulls_url: url("#{repo_api_path}/pulls{/number}", options),
      milestones_url: url("#{repo_api_path}/milestones{/number}", options),
      notifications_url: url("#{repo_api_path}/notifications{?since,all,participating}", options),
      labels_url: url("#{repo_api_path}/labels{/name}", options),
      releases_url: url("#{repo_api_path}/releases{/id}", options),
      deployments_url: url("#{repo_api_path}/deployments", options),
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # repo    - Repository instance.
  # options - Hash
  #           :full - Boolean specifying we want the extended output.
  #
  # Returns a Hash if the Repository exists, or nil.
  def repository_hash(repo, options = {})
    return nil if !repo
    options = Api::SerializerOptions.from(options)

    # Fix for #37741. These attributes require a routed repo, but since
    # this serializer method is used in lists, we can't just bail on the
    # whole repo.
    default_branch = "master"

    begin
      default_branch = repo.default_branch
    rescue GitRPC::RepositoryOffline => boom
      Failbot.push app: "github-unrouted"
      Failbot.report boom
    end

    return nil unless hash = simple_repository_hash(repo, options)

    hash.update \
      created_at: time(repo.created_at),
      updated_at: time(repo.updated_at),
      pushed_at: time(repo.pushed_at),
      git_url: repo.gitweb_url,
      ssh_url: repo.ssh_url,
      clone_url: repo.clone_url,
      svn_url: repo.svn_url,
      homepage: repo.homepage,
      size: repo.disk_usage.to_i,
      stargazers_count: repo.stargazer_count,
      watchers_count: repo.stargazer_count,
      language: repo.primary_language_name,
      has_issues: repo.has_issues?,
      has_projects: repo.repository_projects_enabled?,
      has_downloads: repo.has_downloads?,
      has_wiki: repo.has_wiki?,
      has_pages: repo.page.present?,
      forks_count: repo.forks_count,
      mirror_url: repo.mirror ? repo.mirror.url : nil,
      archived: repo.archived?,
      disabled: repo.disabled?,
      open_issues_count: repo.open_issues_count,
      license: license_hash(repo.license)

    if options.accepts_version?(:"mercy-preview")
      hash[:topics] = repo.topic_names
    end

    if options.accepts_version?(:"baptiste-preview")
      hash[:is_template] = repo.template?
    end

    if options.accepts_version?(:"nebula-preview")
      hash[:visibility] = repo.visibility
    end

    if options.accepts_version?("extended-search-results") && options[:license].nil?
      hash[:license] = license_hash(repo.license)
    end

    # DEPRECATED: These attributes will be removed in API v4.
    hash.update \
      forks: hash[:forks_count],
      open_issues: hash[:open_issues_count],
      watchers: hash[:watchers_count]

    hash[:default_branch] = default_branch

    # DEPRECATED: Will be removed in API v4. Use default_branch instead.
    hash[:master_branch] = default_branch if options.wants_beta_media_type?

    if options[:current_user].present?
      hash[:permissions] = permissions_hash(repo, user: options[:current_user])
    end

    if options[:code_of_conduct]
      hash[:code_of_conduct] = code_of_conduct_hash(repo.code_of_conduct, options)
    end

    if options[:generate_temp_clone_token]
      hash[:temp_clone_token] = repo.temp_clone_token(options[:current_user])
    end

    if options[:show_merge_settings]
      hash[:allow_squash_merge] = repo.squash_merge_allowed?
      hash[:allow_merge_commit] = repo.merge_commit_allowed?
      hash[:allow_rebase_merge] = repo.rebase_merge_allowed?
      hash[:delete_branch_on_merge] = repo.delete_branch_on_merge?
    end

    if GitHub.anonymous_git_access_enabled?
      hash[:anonymous_access_enabled] = repo.anonymous_git_access_enabled?
    end

    hash
  end

  # GraphQL versions of permissions_hash

  RepositoryCollaboratorEdgePermissionFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on RepositoryCollaboratorEdge {
      permission
    }
  GRAPHQL

  RepositoryPermissionFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Repository {
      permission: viewerPermission
    }
  GRAPHQL

  def graphql_permissions_hash(object, options = {})
    raise ArgumentError unless object

    permission = if object.is_a? Api::App::PlatformTypes::Repository
      RepositoryPermissionFragment.new(object).permission
    else
      RepositoryCollaboratorEdgePermissionFragment.new(object).permission
    end

    if permission
      {
        pull: %w(READ WRITE ADMIN).include?(permission),
        push: %w(WRITE ADMIN).include?(permission),
        admin: %w(ADMIN).include?(permission),
      }
    else
      {
        pull: false,
        push: false,
        admin: false,
      }
    end
  end

  RepositoryFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Repository {
      ...Api::Serializer::RepositoriesDependency::SimpleRepositoryFragment

      visibility

      createdAt
      updatedAt
      pushedAt

      homepageUrl # homepage
      primaryLanguage {
        name
      } # language
      diskUsage # size
      defaultBranch

      hasIssuesEnabled # has_issues
      hasProjectsEnabled # has_projects
      hasWikiEnabled # has_wiki
      hasDownloads # has_downloads
      hasPages # has_pages

      squashMergeAllowed
      rebaseMergeAllowed
      mergeCommitAllowed
      deleteBranchOnMerge

      isArchived
      isDisabled
      isTemplate

      issues(states: OPEN) {
        totalCount
      } # open_issues_count (deprecated: open_issues)
      pullRequests(states: OPEN) {
        totalCount
      } # open_issues_count (deprecated: open_issues)
      stargazers {
        totalCount # stargazers_count (deprecated: watchers, watchers_count)
      }
      forks {
        totalCount # forks_count (deprecated: forks)
      }

      cloneUrl
      mirrorUrl
      gitUrl
      sshUrl
      svnUrl

      licenseInfo {
        ...Api::Serializer::LicensesDependency::SimpleLicenseFragment
      }

      codeOfConduct {
        ...Api::Serializer::CodesOfConductDependency::PartialCodeOfConductFragment
      }

      ...Api::Serializer::RepositoriesDependency::RepositoryPermissionFragment

      hasAnonymousAccessEnabled
      tempCloneToken
    }
  GRAPHQL

  def graphql_repository_hash(repo, options = {})
    return nil unless repo

    repo = RepositoryFragment.new(repo)
    options = Api::SerializerOptions.from(options)

    hash = graphql_simple_repository_hash(repo, options)
    return if hash.nil?

    hash.update \
      created_at: time(repo.created_at),
      updated_at: time(repo.updated_at),
      pushed_at: time(repo.pushed_at),
      git_url: repo.git_url.to_s,
      ssh_url: repo.ssh_url.to_s,
      clone_url: repo.clone_url.to_s,
      svn_url: repo.svn_url.to_s,
      homepage: repo.homepage_url&.to_s,
      size: repo.disk_usage,
      stargazers_count: repo.stargazers.total_count,
      watchers_count: repo.stargazers.total_count,
      language: repo.primary_language&.name,
      has_issues: repo.has_issues_enabled?,
      has_projects: repo.has_projects_enabled?,
      has_downloads: repo.has_downloads?,
      has_wiki: repo.has_wiki_enabled?,
      has_pages: repo.has_pages?,
      forks_count: repo.forks.total_count,
      mirror_url: repo.mirror_url&.to_s,
      archived: repo.is_archived?,
      disabled: repo.is_disabled?,
      open_issues_count: repo.issues.total_count + repo.pull_requests.total_count,
      license: graphql_simple_license_hash(repo.license_info)

    hash.update \
      forks: hash[:forks_count],
      open_issues: hash[:open_issues_count],
      watchers: hash[:watchers_count]

    hash[:default_branch] = repo.default_branch

    if options[:code_of_conduct]
      hash[:code_of_conduct] = graphql_code_of_conduct_hash(repo.code_of_conduct, options)
    end

    if options[:current_user]
      hash[:permissions] = graphql_permissions_hash(repo, options)
    end

    if options[:generate_temp_clone_token]
      hash[:temp_clone_token] = repo.temp_clone_token
    end

    if options.accepts_version?(:"baptiste-preview")
      hash[:is_template] = repo.is_template?
    end

    if options.accepts_version?(:"nebula-preview")
      hash[:visibility] = repo.visibility
    end

    if options[:show_merge_settings]
      hash[:allow_squash_merge] = repo.squash_merge_allowed?
      hash[:allow_merge_commit] = repo.merge_commit_allowed?
      hash[:allow_rebase_merge] = repo.rebase_merge_allowed?
      hash[:delete_branch_on_merge] = repo.delete_branch_on_merge?
    end

    if GitHub.anonymous_git_access_enabled?
      hash[:anonymous_access_enabled] = repo.has_anonymous_access_enabled?
    end

    hash
  end

  def full_repository_hash(repo, options)
    return nil if !repo

    options = Api::SerializerOptions.from(options)

    return nil unless hash = repository_hash(repo, options)

    if options.accepts_version?(:"baptiste-preview")
      hash[:template_repository] = if (template = repo.template_repository)
        repository_hash(template, options)
      end
    end

    if org = repo.organization
      hash[:organization] = user_hash(org, content_options(options))
    end

    if parent = repo.parent
      hash[:parent] = repository_hash(parent)
    end

    if (root = repo.root) && root.id != repo.id
      hash[:source] = repository_hash(root, content_options(options))
    end

    hash.update \
      network_count: repo.network_count,
      subscribers_count: repo.watchers_next_count

    hash
  end

  ExtendedRepositoryFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Repository {
      ...Api::Serializer::RepositoriesDependency::RepositoryFragment

      owner {
        __typename
      }

      watchers {
        totalCount
      }

      network {
        repositories {
          totalCount
        }
      }

      templateRepository {
        ...Api::Serializer::RepositoriesDependency::RepositoryFragment
      }
    }
  GRAPHQL

  def graphql_full_repository_hash(repo, options)
    repo = ExtendedRepositoryFragment.new(repo)
    options = Api::SerializerOptions.from(options)

    hash = graphql_repository_hash(repo, options)

    if options.accepts_version?(:"baptiste-preview")
      hash[:template_repository] = if (template = repo.template_repository)
        graphql_repository_hash(template, options)
      end
    end

    if repo.owner.is_a? Api::App::PlatformTypes::Organization
      hash.update organization: hash[:owner]
    end

    hash.update \
      subscribers_count:  repo.watchers.total_count,
      network_count:      repo.network.repositories.total_count

    hash
  end

  # Returns a permissions hash, suitable for use as `hash[:permissions]` for
  # `repository_hash`.
  #
  # repo    - Repository instance.
  # options - Hash
  #           :user - User instance for computing permissions values
  #
  # Returns a Hash with :admin, :push, and :pull keys with boolean values.
  def permissions_hash(repo, options)
    raise ArgumentError if options[:user].nil?

    action_fixnum = Ability.actions[repo.access_level_for(options[:user])] || -1

    {
      admin: (action_fixnum >= Ability.actions[:admin]),
      push: (action_fixnum >= Ability.actions[:write]),
      pull: (action_fixnum >= Ability.actions[:read]),
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # comment - CommitComment instance.
  # options - Hash
  #           :repo - Optional Repository instance.
  #
  # Returns a Hash if the CommitComment exists, or nil.
  def commit_comment_hash(comment, options = {})
    return nil if !comment

    options = Api::SerializerOptions.from(options)
    repo = repo_path(options, comment)

    hash = {
      url: url("/repos/#{repo}/comments/#{comment.id}", options),
      html_url: "#{comment.url}#commitcomment-#{comment.id}",
      id: comment.id,
      node_id: comment.global_relay_id,
      user: user_hash(comment.user, content_options(options)),
      position: comment.position,
      line: comment.line,
      path: comment.path,
      commit_id: comment.commit_id,
      created_at: time(comment.created_at),
      updated_at: time(comment.updated_at),
      author_association: comment.author_association(options[:current_user]).to_s,
    }.update(mime_body_hash(comment, options))

    if options.accepts_preview?(:reactions)
      hash[:reactions] = reactions_rollup(comment, url("/repos/#{repo}/comments/#{comment.id}/reactions", options))
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # diff_entry - GitHub::Diff::Entry instance.
  # options    - Hash
  #              :repo - Optional Repository instance.
  #
  # Returns a Hash if the GitHub::Diff::Entry exists, or nil.
  def condensed_diff_entry_hash(diff_entry, options = {})
    return nil if !diff_entry

    options = Api::SerializerOptions.from(options)
    repo = repo_path(options, diff_entry)

    hash = {
      sha: diff_entry.b_blob || diff_entry.a_blob,
      filename: diff_entry.path,
      status: diff_entry.status_label,
      additions: diff_entry.additions,
      deletions: diff_entry.deletions,
      changes: diff_entry.changes,
      blob_url: diff_url(repo, diff_entry, "blob"),
      raw_url: diff_url(repo, diff_entry, "raw"),
      contents_url: diff_contents_url(repo, diff_entry),
    }

    if diff_entry.text.try(:length).to_i > 0 && !diff_entry.binary_text?
      hash[:patch] = diff_entry.unicode_text
    end

    if diff_entry.renamed?
      hash[:previous_filename] = diff_entry.a_path
    end

    hash
  end

  alias github_diff_entry_hash condensed_diff_entry_hash

  PatchFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Patch {
      text
      unicodeText
      status
      linesAdded
      linesDeleted
      linesChanged
      isBinary
      newTreeEntry {
        oid
        path
      }
      oldTreeEntry {
        oid
        path
      }
      blobUrl
      rawUrl
      contentsUrl
    }
  GRAPHQL

  def graphql_condensed_diff_entry_hash(diff_entry, options = {})
    diff_entry = PatchFragment.new(diff_entry)
    return nil if !diff_entry

    hash = {
      sha: diff_entry.new_tree_entry&.oid || diff_entry.old_tree_entry&.oid,
      filename: diff_entry.new_tree_entry&.path || diff_entry.old_tree_entry&.path,
      status: diff_entry.status.downcase,
      additions: diff_entry.lines_added,
      deletions: diff_entry.lines_deleted,
      changes: diff_entry.lines_changed,
      blob_url: diff_entry.blob_url.to_s,
      raw_url: diff_entry.raw_url.to_s,
      contents_url: diff_entry.contents_url.to_s,
    }

    if diff_entry.text.try(:length).to_i > 0 && !diff_entry.is_binary
      hash[:patch] = diff_entry.unicode_text
    end

    if diff_entry.status == "RENAMED"
      hash[:previous_filename] = diff.old_tree_entry&.path
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # comparison - GitHub::Comparison instance.
  # options    - Hash
  #              :repo - Optional Repository instance.
  #
  # Returns a Hash if the GitHub::Comparison exists, or nil.
  def github_comparison_hash(comparison, options = {})
    return nil if !comparison

    options = Api::SerializerOptions.from(options)
    repo = repo_path(options, comparison)

    diffs = comparison.diffs
    entries = diffs.to_a

    all_commits = comparison.commits + [
      comparison.base_commit,
      comparison.merge_base_commit,
    ]

    Commit.prefill_verified_signature(all_commits, repo)

    {
      url: url("/repos/#{repo}/compare/#{comparison.base}...#{comparison.head}", options),
      html_url: comparison.to_url,
      permalink_url: comparison.to_permalink_url,
      diff_url: comparison.to_diff_url,
      patch_url: comparison.to_patch_url,
      base_commit: git_commit_hash(comparison.base_commit, options),
      merge_base_commit: git_commit_hash(comparison.merge_base_commit, options),
      status: comparison.status,
      ahead_by: comparison.ahead_by,
      behind_by: comparison.behind_by,
      total_commits: comparison.total_commits,
      commits: commits_array(comparison.commits, options),
      files: entries.map { |entry| condensed_diff_entry_hash(entry, options) },
    }
  end

  # Hashify a community profile in preparation for JSON serialization.
  #
  # community_profile - A repository's community profile
  # options - Unused.
  #
  # Returns a hash.
  def community_profile_hash(community_profile, options = {})
    repository = community_profile.repository
    readme = repository.preferred_readme
    contributing = repository.preferred_contributing
    license = repository.license
    detected_code_of_conduct = community_profile.code_of_conduct
    issue_template = community_profile.issue_template
    pr_template = community_profile.pr_template
    hashed_code_of_conduct, hashed_contributing, hashed_license, hashed_readme, hashed_issue_template, hashed_pr_template = nil, nil, nil, nil, nil, nil

    if detected_code_of_conduct
      hashed_code_of_conduct = code_of_conduct_hash(detected_code_of_conduct, options)
    end

    # Gets a contributing hash
    if community_profile.contributing?
      hashed_contributing = {
        url: url("/repos/#{repository.name_with_owner}/contents/#{contributing.path}"),
        html_url: preferred_file_url(type: :contributing, repository: contributing.repository),
      }
    end

    # Gets the license hash
    if license
      hashed_license = license_hash(license).except(:featured)
      hashed_license[:html_url] = preferred_file_url(type: :license, repository: repository)
    end

    # Gets a readme hash
    if community_profile.readme?
      hashed_readme = {
        url: url("/repos/#{repository.name_with_owner}/contents/#{readme.path}"),
        html_url: preferred_file_url(type: :readme, repository: repository),
      }
    end

    # Gets an issue template hash
    if !!issue_template
      hashed_issue_template = {
        url: url("/repos/#{issue_template.repository.name_with_owner}/contents/#{issue_template.path}"),
        html_url: preferred_file_url(type: :issue_template, repository: issue_template.repository),
      }
    end

    # Gets a pull request template hash
    if !!pr_template
      hashed_pr_template = {
        url: url("/repos/#{pr_template.repository.name_with_owner}/contents/#{pr_template.path}"),
        html_url: preferred_file_url(type: :pull_request_template, repository: pr_template.repository),
      }
    end

    {
      health_percentage: community_profile.health_percentage,
      description: repository.description,
      documentation: community_profile.documentation_url,
      files: {
        code_of_conduct: hashed_code_of_conduct,
        contributing: hashed_contributing,
        issue_template: hashed_issue_template,
        pull_request_template: hashed_pr_template,
        license: hashed_license,
        readme: hashed_readme,
      },
      updated_at: time(community_profile.updated_at),
    }
  end

  # Hashify a repository invitation in preparation for JSON serialization.
  #
  # invitation - A repository invitation
  # options - Unused.
  #
  # Returns a hash.
  def repository_invitation_hash(invitation, options = {})
    {
      id: invitation.id,
      node_id: invitation.global_relay_id,
      repository: simple_repository_hash(invitation.repository),
      invitee: user_hash(invitation.invitee, content_options(options)),
      inviter: user_hash(invitation.inviter, content_options(options)),
      permissions: invitation.permission_string,
      created_at: time(invitation.created_at),
      url: url("/user/repository_invitations/#{invitation.id}"),
      html_url: invitation.permalink,
    }
  end

  # Hashify a repository invitation in preparation for JSON serialization.
  #
  # invitation - A repository invitation
  # options - Unused.
  #
  # Returns a hash.
  def fgp_repository_invitation_hash(invitation, options = {})
    {
      id: invitation.id,
      node_id: invitation.global_relay_id,
      repository: simple_repository_hash(invitation.repository),
      invitee: user_hash(invitation.invitee, content_options(options)),
      inviter: user_hash(invitation.inviter, content_options(options)),
      permissions: invitation.permission_string,
      created_at: time(invitation.created_at),
      url: url("/user/repository_invitations/#{invitation.id}"),
      html_url: invitation.permalink,
    }
  end

  # Create a Hash of a user's permission level in preparation for JSON
  # serialization.
  #
  # permission - A String permission level (see Repository#access_level_for)
  # options - Expects a :user key mapping to a User instance.
  #
  # Returns a Hash.
  def repository_collaborator_permission(permission, options = {})
    {
      permission: permission,
      user: user_hash(options[:user], content_options(options)),
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # download - Download instance.
  # options  - Hash
  #            :repo - Optional Repository instance.
  #
  # Returns a Hash if the GitHub::Comparison exists, or nil.
  def download_hash(download, options = {})
    return nil if !download

    options = Api::SerializerOptions.from(options)
    repo = repo_path(options, download)
    hash = {
      url: url("/repos/#{repo}/downloads/#{download.id}", options),
      id: download.id,
      html_url: download.full_url,
      name: download.name,
      description: download.description,
      created_at: time(download.timestamp),
      size: download.size * 1.kilobyte,
      download_count: download.hits,
      content_type: download.content_type,
    }

    if upload_hash = options[:upload_hash]
      hash.merge! upload_hash
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # release - Release instance.
  # options - Hash
  #           :repo - Optional Repository instance.
  #           :assets - Optional Hash of Integer Release ID => ReleaseAsset.
  #
  # Returns a Hash if the Release exists, or nil.
  def release_hash(release, options = {})
    return nil if !release

    options = Api::SerializerOptions.from(options)

    repo = options[:repo_path] ||= repo_path(options, release)
    repo_api_path = "/repos/#{repo}"
    release_path = "#{repo_api_path}/releases/#{release.id}"

    asset_options = options.merge(release: release, repo_path: repo)
    assets = if all_assets = options[:assets]
      all_assets[release.id] || []
    else
      release.uploaded_assets
    end

    GitHub::PrefillAssociations.asset_release(release, assets) if assets.present?

    h = {
      url: url(release_path, options),
      assets_url: url("#{release_path}/assets", options),
      upload_url: "#{GitHub.api_upload_prefix}#{release_path}/assets{?name,label}",
      html_url: release.permalink,
      id: release.id,
      node_id: release.global_relay_id,
      tag_name: release.exposed_tag_name,
      target_commitish: release.target_commitish,
      name: release.name,
      draft: release.draft?,
      author: simple_user_hash(release.author, content_options(options)),
      prerelease: release.prerelease?,
      created_at: time(release.created_at),
      published_at: time(release.published_at),
      assets: assets.map { |a| release_asset_hash(a, asset_options) },
      tarball_url: nil,
      zipball_url: nil,
    }

    h.update mime_body_hash(release, options)

    if release.published?
      h.update \
        tarball_url: url("#{repo_api_path}/tarball/#{release.tag_name}", options),
        zipball_url: url("#{repo_api_path}/zipball/#{release.tag_name}", options)
    end

    h
  end

  # Creates a Hash to be serialized to JSON.
  #
  # asset   - ReleaseAsset instance
  # options - Hash
  #           :repo - Optional Repository instance.
  #           :release - Optional Release instance.
  #
  # Returns a Hash if the ReleaseAsset exists, or nil.
  def release_asset_hash(asset, options = {})
    return nil if !asset

    options = Api::SerializerOptions.from(options)
    repo = repo_path(options, asset.release)
    asset_path = "/repos/#{repo}/releases/assets/#{asset.id}"

    {
      url: url(asset_path, options),
      id: asset.id,
      node_id: asset.global_relay_id,
      name: asset.name,
      label: asset.label,
      uploader: simple_user_hash(asset.uploader, content_options(options)),
      content_type: asset.downloadable_content_type,
      state: asset.state,
      size: asset.size,
      download_count: asset.downloads,
      created_at: time(asset.created_at),
      updated_at: time(asset.updated_at),
      browser_download_url: asset.permalink,
    }
  end

  # Creates a Hash to be serialized to JSON.
  #
  # content - Api::RepositoryContents::ContentWrapper instance
  # options - Hash
  #           :repo - The Repository containing the content.
  #           :full - The Boolean specifying whether to include the extended
  #                   output.
  #
  # Returns a Hash if the ContentWrapper exists, or nil.
  def content_hash(content, options = {})
    options = Api::SerializerOptions.from(options)

    hash = if content.submodule?
      submodule_content_hash(content, options)
    elsif content.blob?
      blob_content_hash(content, options)
    else
      tree_content_hash(content, options)
    end

    # Backwards compatibility
    hash[:_links] = {
      self: hash[:url],
      git: hash[:git_url],
      html: hash[:html_url],
    }

    hash
  end

  # Internal: Creates a Hash containing the elements that are common amongst all
  # JSON responses provided for "content" (i.e., trees, blobs, submodules).
  #
  # common  - Api::RepositoryContents::ContentWrapper instance representing
  # options - Hash
  #           :repo - The Repository containing the content.
  #
  # Returns a Hash if the ContentWrapper exists, or nil.
  def common_content_hash(content, options = {})
    return nil if content.nil?

    repo = options[:repo]
    repo_nwo = repo_path(options, content)

    ref = options[:ref] ||= repo.default_branch

    hash = {
      name: content.name,
      path: content.path,
      sha: content.sha,
      size: content.size,
      url: encoded_content_url("/repos/#{repo_nwo}/contents/", content.path, {ref: ref.b}),
      html_url: nil,
      git_url: nil,
      download_url: nil,
    }

    hash
  end

  # Internal: Creates a Hash to be serialized to JSON.
  #
  # tree    - Api::RepositoryContents::ContentWrapper instance representing a tree
  # options - Hash
  #           :repo - The Repository containing the content.
  #           :full - The Boolean specifying whether to include the extended
  #                   output.
  #
  # Returns a Hash if the ContentWrapper exists, or nil.
  def tree_content_hash(tree, options = {})
    return nil if tree.nil?

    hash = common_content_hash(tree, options)

    hash[:type] = "dir"

    repo = options[:repo]
    ref = options[:ref]

    hash[:html_url] = encoded_html_url("#{repo.permalink}/tree/#{ref.b}/", tree.path)

    repo_nwo = repo_path(options, tree)
    hash[:git_url]  = url("/repos/#{repo_nwo}/git/trees/#{tree.sha}")

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # tree    - Api::RepositoryContents::ContentWrapper instance representing a tree
  # options - Hash of options for #tree_content_hash
  #
  # Returns a Hash if the ContentWrapper exists, or nil.
  def tree_object_content_hash(tree, options = {})
    return nil if tree.nil?

    _id, tree_entries, _truncated = tree.repository.tree_entries(tree.sha, "", skip_size: false)
    content_hash(tree, options).merge!(
      entries: tree_entries.map do |entry|
        entry.path_prefix = tree.path
        content_hash(entry, options.merge(full: false))
      end,
    )
  end

  # Internal: Creates a Hash to be serialized to JSON.
  #
  # blob    - A TreeEntry instance.
  # options - Hash
  #           :repo - The Repository containing the content.
  #           :full - The Boolean specifying whether to include the extended
  #                   output.
  #
  # Returns a Hash if the ContentWrapper exists, or nil.
  def blob_content_hash(blob, options = {})
    options = Api::SerializerOptions.from(options)
    return nil if blob.nil?

    hash = common_content_hash(blob, options)

    hash[:type] = blob.unresolvable_symlink? ? "symlink" : "file"

    repo = options[:repo]
    ref = options[:ref]

    hash[:html_url] = encoded_html_url("#{repo.permalink}/blob/#{ref.b}/", blob.path)
    hash[:download_url] = if blob.git_lfs?
      TreeEntry::RenderHelper.lfs_blob_url(options.current_user, repo, ref, blob.path)
    else
      TreeEntry::RenderHelper.raw_blob_url(options.current_user, repo, ref, blob.path)
    end

    repo_nwo = repo_path(options, blob)
    hash[:git_url] = url("/repos/#{repo_nwo}/git/blobs/#{blob.sha}")

    if options[:full]
      if blob.unresolvable_symlink?
        hash[:target] = blob.data.to_s
      else
        if symlink = blob.symlink_target
          blob = symlink
        end
        hash[:content] = encode_base64(blob.data.to_s, stats_key: "git_blob")
        hash[:size] = blob.size
        hash[:encoding] = "base64"
      end
    end

    hash
  end

  # Internal: Creates a Hash to be serialized to JSON.
  #
  # submodule - Api::RepositoryContents::ContentWrapper instance representing a
  #             submodule
  # options   - Hash
  #             :repo - The Repository containing the content.
  #             :full - The Boolean specifying whether to include the extended
  #                     output.
  #
  # Returns a Hash if the ContentWrapper exists, or nil.
  def submodule_content_hash(submodule, options = {})
    return nil if submodule.nil?

    hash = common_content_hash(submodule, options)

    hash[:type] = "submodule"

    if submodule.submodule_hosted_on_github?
      repo_nwo = "#{submodule.submodule_user}/#{submodule.submodule_repo}"
      hash[:html_url] = html_url("/#{repo_nwo}/tree/#{submodule.sha}")
      hash[:git_url] = url("/repos/#{repo_nwo}/git/trees/#{submodule.sha}")
    end

    # DEPRECATED: This exists for backwards compatibility. When this API first
    # shipped, v3 mistakenly returned the type as 'file' when providing the
    # summary info for a submodule. This else clause is here only to avoid
    # breaking any v3 API consumers that might expect have been built to expect
    # this buggy behavior.
    #
    # TODO In v4, remove this line so that we always return the type as
    # 'submodule'.
    hash[:type] = "file" if !options[:full]

    if options[:full]
      hash[:submodule_git_url] = submodule.submodule_git_url
    end

    hash
  end

  def page_hash(page, options = {})
    options = Api::SerializerOptions.from(options)
    repo_path = repo_path(options, page)
    hash = {
      url: url("/repos/#{repo_path}/pages", options),
      status: page.status,
      cname: page.cname,
      custom_404: page.four_oh_four,
      html_url: options[:html_url],
      source: {
        branch: page.source_branch,
        path: page.source_dir,
      },
    }

    hash
  end

  def page_build_hash(build, options = {})
    options = Api::SerializerOptions.from(options)
    repo = repo_path(options, build)
    {
      url: url("/repos/#{repo}/pages/builds/#{build.id}", options),
      status: build.status,
      error: { message: build.error },
      pusher: user_hash(build.pusher, content_options(options)),
      commit: build.commit,
      duration: build.duration.to_i,
      created_at: time(build.created_at),
      updated_at: time(build.updated_at),
    }
  end

  # Public: Get a hash for content CRUD response
  #
  # input - hash containing :content and :commit keys
  #
  # Contains a summary of commit and content info
  #
  # Returns a Hash
  def contents_crud_hash(input, options = {})
    options = Api::SerializerOptions.from(options)

    content = input[:content]
    commit  = input[:commit]

    content_summary = if content
      content_hash(content, options.merge(ref: input[:ref]))
    end
    commit_summary  = Api::Serializer.commit_hash(commit, options)

    {
      content: content_summary,
      commit: commit_summary,
    }
  end

  # Public: Build a short hash of a branch
  #
  # input - a Ref
  #
  # Contains the name, head SHA, and location of the ref.
  #
  # Returns a hash
  def short_branch_hash(ref, options = {})
    sha        = ref.target_oid
    commit_url = url("/repos/#{ref.repository.name_with_owner}/commits/#{sha}") if sha

    {
      name: ref.name,
      commit: {
        sha: sha,
        url: commit_url,
      },
      protected: ref.protected?,
    }
  end

  # Public: Same as short_branch_hash, but includes branch protection information
  #
  # ref - A Ref (must be a branch)
  #
  # Returns a Hash
  def short_branch_with_protection_hash(ref, options = {})
    short_branch_hash(ref, options).merge({
      protection: branch_protection_hash(ref, options),
      protection_url: url(protected_branch_base_path(ref), options),
    })
  end

  # Public: Returns a hash describing what is required for the current user to push to the branch.
  #
  # Example:
  #
  # {
  #   pattern: "release-*",
  #   required_signatures: true,              # (false if user is admin and not admin-enforced)
  #   required_status_checks: ["travis-ci"],  # (empty array if user is admin and not admin-enforced)
  #   required_approving_review_count: 3,     # (0 if user is admin and not admin-enforced)
  #   required_linear_history: true,          # (false if user is admin and not admin-enforced)
  #   allow_actor: true,                      # (false if there are restrictions and user is not on list. Always true for admins, etc. True if there are no restrictions.)
  #   allow_deletions: false,                 # this has no admin override
  #   allow_force_pushes: false               # this has no admin override
  # }
  def branch_pushability_hash(ref, options = {})
    options = Api::SerializerOptions.from(options)
    user = options[:current_user]

    protected_branch = ref.protected_branch || unprotected_branch(ref)

    required_statuses = if protected_branch.required_status_checks_enforced_for?(actor: user)
      protected_branch.required_status_checks.map { |check| check.context }
    else
      []
    end
    review_count = if protected_branch.required_review_policy_enforced_for?(actor: user)
      protected_branch.required_approving_review_count
    else
      0
    end

    short_branch_hash(ref, options).merge(
      pattern: protected_branch.name,
      required_signatures: protected_branch.required_signatures_enforced_for?(actor: user),
      required_status_checks: required_statuses,
      required_approving_review_count: review_count,
      required_linear_history: protected_branch.required_linear_history_enforced_for?(actor: user),
      allow_actor: protected_branch.push_authorized?(user),
      allow_deletions: !protected_branch.block_deletions_enabled?,
      allow_force_pushes: !protected_branch.block_force_pushes_enabled?,
    )
  end

  def unprotected_branch(ref)
    ProtectedBranch.new(repository: ref.repository).tap do |branch|
      branch.clear_blocked_deletions
      branch.clear_blocked_force_pushes
    end
  end

  RepositoryInteractionAbilityFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Repository {
      interactionAbility {
        limit
        origin
        expiresAt
      }
    }
  GRAPHQL

  def graphql_repository_interaction_ability_hash(repo, options = {})
    repo = RepositoryInteractionAbilityFragment.new(repo)

    interaction = repo.interaction_ability

    return {} if interaction.limit == "NO_LIMIT"

    {
      limit: interaction.limit.downcase,
      origin: interaction.origin.downcase,
      expires_at: time(interaction.expires_at),
    }
  end
end
