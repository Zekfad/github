# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::PullsDependency
  class InvalidIssueAssociation < RuntimeError ; end

  # Creates a Hash to be serialized to JSON.
  #
  # pull    - PullRequest instance
  # options - Hash
  #           :full    - Boolean specifying we want the extended output.
  #           :private - Boolean specifying we want the private output.
  #           :hook    - Boolean specifying if called from webhooks
  #
  # Returns a Hash if the org exists, or nil.
  def pull_request_hash(pull, options = {})
    return nil if !pull

    options = Api::SerializerOptions.from(options)

    repo       = repo_path(options, pull)
    pull_url   = url("/repos/#{repo}/pulls/#{pull.number}", options)
    issue_url  = url("/repos/#{repo}/issues/#{pull.number}", options)
    status_url = url("/repos/#{repo}/statuses/#{pull.head_sha}", options)
    review_comment_url = url("/repos/#{repo}/pulls/comments{/number}", options)

    # Add tracing to help debug PRs being created without issues,
    # see #40102 for more details.
    if pull.updated_at.nil? || pull.issue.nil? || pull.issue.updated_at.nil?
      error = InvalidIssueAssociation.new("Invalid PullRequest/Issue association")
      extra = {
        pull: pull,
        pull_valid: pull.valid?,
        pull_saved: pull.persisted?,
        pull_errors: pull.errors.full_messages.to_sentence,
        issue: pull.issue,
      }
      if issue = pull.issue
        extra.merge!({
          issue_valid: issue.valid?,
          issue_saved: issue.persisted?,
          issue_errors: issue.errors.full_messages.to_sentence,
        })
      end
      Failbot.report_trace(error, extra)
    end

    updated_at = pull.updated_at > pull.issue.updated_at ?
      time(pull.updated_at) :
      time(pull.issue.updated_at)
    hash       = {
      url: pull_url,
      id: pull.id,
      node_id: pull.global_relay_id,
      html_url: pull.url,
      diff_url: "#{pull.url}.diff",
      patch_url: "#{pull.url}.patch",
      issue_url: issue_url,
      number: pull.number,
      state: pull.issue.state,
      locked: pull.issue.locked?,
      title: pull.title,
      user: user_hash(pull.safe_user, content_options(options)),
      body: pull.body,
      created_at: time(pull.created_at),
      updated_at: updated_at,
      closed_at: time(pull.closed_at),
      merged_at: time(pull.merged_at),
      merge_commit_sha: pull.merge_commit_sha, # DEPRECATED: Will be
                                                      # removed in API v4.
      assignee: user_hash(pull.issue.assignee, content_options(options)),
      assignees: pull.issue.assignees.map { |assignee| simple_user_hash(assignee, content_options(options)) },
      requested_reviewers: pull.review_requests.pending.users.map { |reviewer| simple_user_hash(reviewer, content_options(options)) },
      requested_teams: pull.review_requests.pending.teams.map { |team| team_hash(team, content_options(options)) },
      labels: pull.issue.labels.map { |l| label_hash(l, options.merge(repo: repo)) },
      milestone: milestone_hash(pull.issue.milestone, repo: repo),
      draft: pull.draft?,
      commits_url: pull_url + "/commits",
      review_comments_url: pull_url + "/comments",
      review_comment_url: review_comment_url,
      comments_url: issue_url + "/comments",
      statuses_url: status_url,
      head: {
        label: pull.head_label(username_qualified: true),
        ref: pull.head_ref_name,
        sha: pull.head_sha,
        user: user_hash(pull.head_user, content_options(options)),
        repo: repository_hash(pull.head_repository, options_for_repo_hash(options)),
      },
      base: {
        label: pull.base_label(username_qualified: true),
        ref: pull.base_ref_name,
        sha: pull.base_sha,
        user: user_hash(pull.base_user, content_options(options)),
        repo: repository_hash(pull.base_repository, options_for_repo_hash(options)),
      },
      _links: {
        self: {href: pull_url},
        html: {href: pull.url},
        issue: {href: issue_url},
        comments: {href: issue_url + "/comments"},
        review_comments: {href: pull_url + "/comments"},
        review_comment: {href: review_comment_url},
        commits: {href: pull_url + "/commits"},
        statuses: {href: status_url},
      },
      author_association: pull.author_association(options[:current_user]).to_s,
    }.update(mime_body_hash(pull, options))

    hash[:active_lock_reason] = pull.active_lock_reason

    if options[:full]
      hash.update({
        merged: pull.merged?,
        mergeable: pull.currently_mergeable?,
        rebaseable: pull.currently_mergeable? && pull.rebase_safe?,
        mergeable_state: pull.merge_state(viewer: options[:current_user]).status,
        merged_by: user_hash(pull.merged_by, content_options(options)),
        comments: pull.issue.comments.filter_spam_for(options[:current_user]).count,
        review_comments: pull.review_comments.count,
        maintainer_can_modify: pull.fork_collab_granted?,
      })

      hash.update pull_request_diff_stats_hash(pull)
    end

    if options.accepts_version?("night-shift")
      merge_state = pull.merge_state(viewer: options[:current_user])
      decision = merge_state.pull_request_review_policy_decision
      hash.update(
        mergeability_requirements: {
          required_reviews: {
            fulfilled: decision.policy_fulfilled,
            summary: decision.reason.summary,
            message: decision.reason.message,
          },
        },
      )
    end

    hash
  end

  PullRequestFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on PullRequest {
      id
      databaseId
      url
      number
      title
      state
      locked
      body
      createdAt
      updatedAt
      closedAt
      mergedAt
      merged
      mergeable
      additions
      deletions
      changedFiles
      maintainerCanModify
      mergeStateStatus
      canBeRebased
      activeLockReason
      isDraft

      mergedBy {
        ...Api::Serializer::UserDependency::SimpleUserFragment
      }

      commits{
        totalCount
      }

      comments {
        totalCount
      }

      reviews(first:100) {
        nodes {
          comments {
            totalCount
          }
        }
      }

      author {
        ...Api::Serializer::UserDependency::SimpleUserFragment
      }

      authorAssociation

      repository {
        nameWithOwner
        owner {
          login
          ...Api::Serializer::UserDependency::SimpleUserFragment
        }
        ...Api::Serializer::RepositoriesDependency::RepositoryFragment
      }

      milestone {
        ...Api::Serializer::IssuesDependency::MilestoneFragment
      }

      mergeCommit {
        oid
      }

      labels(last:100) {
        nodes {
          ...Api::Serializer::IssuesDependency::LabelFragment
        }
      }

      assignees(last:100) {
        nodes {
          ...Api::Serializer::UserDependency::SimpleUserFragment
        }
      }

      reviewRequests(last:100) {
        nodes {
          requestedReviewer {
            ... on User {
              ...Api::Serializer::UserDependency::SimpleUserFragment
            }

            ... on Team {
              ...Api::Serializer::OrganizationsDependency::TeamFragment
            }
          }
        }
      }

      headRefOid
      headRefName
      headRef {
        repository {
          nameWithOwner
          ...Api::Serializer::RepositoriesDependency::RepositoryFragment

          owner {
            login
            ...Api::Serializer::UserDependency::SimpleUserFragment
          }
        }
      }

      baseRefOid
      baseRefName
      baseRef {
        repository {
          nameWithOwner
          ...Api::Serializer::RepositoriesDependency::RepositoryFragment

          owner {
            login
            ...Api::Serializer::UserDependency::SimpleUserFragment
          }
        }
      }

      baseRepository {
        nameWithOwner
        ...Api::Serializer::RepositoriesDependency::RepositoryFragment

        owner {
          login
          ...Api::Serializer::UserDependency::SimpleUserFragment
        }
      }

      ...Api::Serializer::GraphqlHelperDependency::MimeBodyFragment
    }
  GRAPHQL

  RefFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Ref {
      name
      target {
        oid
      }
      repository {
        databaseId
        permalink(includeHost: true)
        name
        nameWithOwner
      }
    }
  GRAPHQL

  MinimalPullRequestFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on PullRequest {
      permalink
      databaseId
      number

      repository {
        nameWithOwner
      }

      headRef {
        ...Api::Serializer::PullsDependency::RefFragment
      }

      baseRef {
        ...Api::Serializer::PullsDependency::RefFragment
      }
    }
  GRAPHQL

  def graphql_pull_request_hash(pull, options = {})
    pull = PullRequestFragment.new(pull)
    return unless pull

    options = Api::SerializerOptions.from(options)

    nwo         = pull.repository.name_with_owner
    pull_url    = url("/repos/#{nwo}/pulls/#{pull.number}", options)
    issue_url   = url("/repos/#{nwo}/issues/#{pull.number}", options)
    status_url  = url("/repos/#{nwo}/statuses/#{pull.head_ref_oid}", options)
    review_comment_url = url("/repos/#{nwo}/pulls/comments{/number}", options)

    review_requests_by_type = pull.review_requests.nodes.group_by do |request|
      request.requested_reviewer.__typename
    end

    requested_reviewers = (review_requests_by_type["User"] || []).map(&:requested_reviewer)
    requested_teams     = (review_requests_by_type["Team"] || []).map(&:requested_reviewer)

    head_label = "#{pull.head_ref.repository.owner.login}:#{pull.head_ref_name}"
    base_label = "#{pull.repository.owner.login}:#{pull.base_ref_name}"

    review_comment_count = pull.reviews.nodes.sum do |review|
      review.comments.total_count
    end

    mergeable = case pull.mergeable
    when "MERGEABLE"
      true
    when "CONFLICTING"
      false
    else
      nil
    end

    user_hash = if pull.author.present?
      graphql_simple_user_hash(pull.author, content_options(options))
    else
      simple_user_hash(User.ghost, content_options(options))
    end

    hash = {
      url: pull_url,
      id: pull.database_id,
      node_id: pull.id,
      html_url: pull.url.to_s,
      diff_url: "#{pull.url}.diff",
      patch_url: "#{pull.url}.patch",
      issue_url: issue_url,
      number: pull.number,
      state: pull.state.downcase,
      locked: pull.locked,
      title: pull.title,
      user: user_hash,
    }.update(graphql_mime_body_hash(pull, options))

    hash.update \
      created_at: time(pull.created_at),
      updated_at: time(pull.updated_at),
      closed_at: time(pull.closed_at),
      merged_at: time(pull.merged_at),
      merge_commit_sha: pull.merge_commit&.oid,
      assignee: graphql_simple_user_hash(pull.assignees.nodes.first, content_options(options)),
      assignees: pull.assignees.nodes.map { |assignee| graphql_simple_user_hash(assignee, content_options(options)) },
      requested_reviewers: requested_reviewers.map { |reviewer| graphql_simple_user_hash(reviewer, content_options(options)) },
      requested_teams: requested_teams.map { |team| graphql_team_hash(team, content_options(options)) },
      labels: pull.labels.nodes.map { |label| graphql_label_hash(label, options) },
      milestone: graphql_milestone_hash(pull.milestone),
      draft: pull.is_draft,
      commits_url: pull_url + "/commits",
      review_comments_url: pull_url + "/comments",
      review_comment_url: review_comment_url,
      comments_url: issue_url + "/comments",
      statuses_url: status_url,
      head: {
        label: head_label,
        ref: pull.head_ref_name,
        sha: pull.head_ref_oid,
        user: graphql_simple_user_hash(pull.head_ref.repository.owner, content_options(options)),
        repo: graphql_repository_hash(pull.head_ref.repository, content_options(options)),
      },
      base: {
        label: base_label,
        ref: pull.base_ref_name,
        sha: pull.base_ref_oid,
        user: graphql_simple_user_hash(pull.base_repository.owner, content_options(options)),
        repo: graphql_repository_hash(pull.base_repository, content_options(options)),
      },
      _links: {
        self: {href: pull_url},
        html: {href: pull.url.to_s},
        issue: {href: issue_url},
        comments: {href: issue_url + "/comments"},
        review_comments: {href: pull_url + "/comments"},
        review_comment: {href: review_comment_url},
        commits: {href: pull_url + "/commits"},
        statuses: {href: status_url},
      },
      author_association: pull.author_association

    hash[:active_lock_reason] = pull.active_lock_reason

    if options[:full]
      hash.update({
        merged: pull.merged,
        mergeable: mergeable,
        rebaseable: mergeable && pull.can_be_rebased,
        mergeable_state: pull.merge_state_status.downcase,
        merged_by: graphql_simple_user_hash(pull.merged_by, content_options(options)),
        comments: pull.comments.total_count,
        review_comments: review_comment_count,
        maintainer_can_modify: pull.maintainer_can_modify,
      })

      hash.update graphql_pull_request_diff_stats_hash(pull)
    end

    hash
  end

  def minimal_pull_request_hash(pull, options = {})
    return nil if !pull

    options = Api::SerializerOptions.from(options)


    {}.tap do |h|
      h[:url] = url("/repos/#{pull.repository.name_with_owner}/pulls/#{pull.number}", options)
      h[:id]  = pull.id
      h[:number] = pull.number
      h[:head] = {
        ref: pull.head_ref_name,
        sha: pull.head_sha,
        repo: {
          id:   pull.head_repository.id,
          url:  url("/repos/#{pull.head_repository.name_with_owner}"),
          name: pull.head_repository.name,
        },
      }
      h[:base] = {
        ref: pull.base_ref_name,
        sha: pull.base_sha,
        repo: {
          id:   pull.base_repository.id,
          url:  url("/repos/#{pull.base_repository.name_with_owner}"),
          name: pull.base_repository.name,
        },
      }
    end
  end

private
  def pull_request_diff_stats_hash(pull)
    total_commits = 0
    additions     = 0
    deletions     = 0
    changed_files = 0

    if pull.historical_comparison.valid?
      begin
        total_commits = pull.total_commits
        additions     = pull.additions
        deletions     = pull.deletions
        changed_files = pull.changed_files
      rescue GitRPC::CommandFailed, Repository::CommandFailed => boom
        Failbot.report boom, app: "github-user"
      end
    end

    {
      commits: total_commits,
      additions: additions,
      deletions: deletions,
      changed_files: changed_files,
    }
  end

  def options_for_repo_hash(options)
    return options if options[:hook]
    content_options(options)
  end

  def graphql_pull_request_diff_stats_hash(pull)
    {
      commits: pull.commits.total_count,
      additions: pull.additions,
      deletions: pull.deletions,
      changed_files: pull.changed_files,
    }
  end

  def graphql_minimal_pull_request_hash(pull)
    pull = MinimalPullRequestFragment.new(pull)
    head = RefFragment.new(pull.head_ref)
    base = RefFragment.new(pull.base_ref)
    {
      url: url("/repos/#{pull.repository.name_with_owner}/pulls/#{pull.number}", {}),
      id: pull.database_id,
      number: pull.number,
      head: {
        ref: head.name,
        sha: head.target.oid,
        repo: {
          id: head.repository.database_id,
          url: url("/repos/#{head.repository.name_with_owner}"),
          name: head.repository.name,
        },
      },
      base: {
        ref: base.name,
        sha: base.target.oid,
        repo: {
          id: base.repository.database_id,
          url: url("/repos/#{base.repository.name_with_owner}"),
          name: base.repository.name,
        },
      },
    }
  end
end
