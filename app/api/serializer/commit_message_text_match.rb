# frozen_string_literal: true

module Api::Serializer
  class CommitMessageTextMatch
    include TextMatch

    SUPPORTED_FIELD_NAMES = %w(message).freeze

    def object_type
      "CommitMessage"
    end

    def object_url_suffix
      "/repos/#{search_result["_model"].owner.name}/#{search_result["_model"].name}/commits/#{search_result["_source"]["hash"]}"
    end

    def property
      field_name
    end
  end
end
