# frozen_string_literal: true

module Api::Serializer::RateLimitDependency
  def rate_limit_statuses_hash(rate_limit_statuses, options = {})
    resources_hash = {}
    rate_limit_statuses.map do |family, rate_limit_status|
      rate_hash = {
        limit: rate_limit_status.max_tries,
        remaining: rate_limit_status.remaining,
        reset: rate_limit_status.expires_at.to_i,
      }

      resources_hash[family] = rate_hash
    end

    {
      resources: resources_hash,

      # DEPRECATED: In v4, remove the line below. The top-level `rate` object
      # should not be part of the v4 response.
      rate: resources_hash[Api::RateLimitConfiguration::DEFAULT_FAMILY],
    }
  end
end
