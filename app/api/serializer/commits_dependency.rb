# frozen_string_literal: true

module Api::Serializer::CommitsDependency
  CommitFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Commit {
      oid
      tree {
        oid
      }
      message
      committedDate
      author {
        name
        email
      }
      committer {
        name
        email
      }
    }
  GRAPHQL

  def graphql_simple_commit_hash(commit)
    return nil unless commit
    commit = CommitFragment.new(commit)

    {
      id:        commit.oid,
      tree_id:   commit.tree.oid,
      message:   commit.message,
      timestamp: time(commit.committed_date),
      author: {
        name: commit.author.name,
        email: commit.author.email,
      },
      committer: {
        name:  commit.committer.name,
        email: commit.committer.email,
      },
    }
  end

  def simple_commit_hash(commit)
    return nil unless commit
    {
      id:        commit.oid,
      tree_id:   commit.tree_oid,
      message:   commit.message,
      timestamp: time(commit.committed_date),
      author: {
        name: commit.author_name,
        email: commit.author_email,
      },
      committer: {
        name:  commit.committer_name,
        email: commit.committer_email,
      },
    }
  end
end
