# frozen_string_literal: true

module Api::Serializer::ContentReferencesDependency
  # Creates a hash to be serialized to JSON
  #
  # content_reference - An instance of the ContentReference model
  #
  def content_reference_hash(content_reference, options = {})
    {
      id: content_reference.id,
      node_id: content_reference.global_relay_id,
      reference: content_reference.reference,
    }
  end

  # Creates a hash to be serialized to JSON
  #
  # attachment - An instance of the ContentReferenceAttachment model
  #
  def content_reference_attachment_hash(attachment, options = {})
    {
      id: attachment.id,
      node_id: attachment.global_relay_id,
      title: attachment.title,
      body: attachment.body,
    }
  end
end
