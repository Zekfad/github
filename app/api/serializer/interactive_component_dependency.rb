# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::InteractiveComponentDependency
  def interactive_component_hash(interactive_component, options = {})
    {
      type: "interactive",
      external_id: interactive_component.external_id,
      elements: interactive_component.parsed_elements,
    }
  end

  def markdown_component_hash(markdown_component, options = {})
    {
      type: "markdown",
      text: markdown_component.body,
    }
  end

  def composable_comment_hash(composable_comment, options = {})
    {}.tap do |h|
      h[:id] = composable_comment.id
      h[:node_id] = composable_comment.global_relay_id
      h[:issue] = {
        "id": composable_comment.issue.id,
        "node_id": composable_comment.issue.global_relay_id,
        "number": composable_comment.issue.number,
      }
      h[:components] = composable_comment.components.map do |component|
        case component.class.to_s
        when "InteractiveComponent"
          interactive_component_hash(component)
        when "MarkdownComponent"
          markdown_component_hash(component)
        end
      end
      h[:created_at] = time(composable_comment.created_at)
      h[:updated_at] = time(composable_comment.updated_at)
    end
  end
end
