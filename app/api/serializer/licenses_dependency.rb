# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::LicensesDependency
  # Public: Build the hash of a license
  #
  # input - a License
  # options - :full - whether to return the full hash. Defaults to false
  #
  # Contains the numeric ID, the SPDX-compliant key, and the human-readable name
  #
  # Returns a hash
  def license_hash(license, options = {})
    return nil unless license
    options = Api::SerializerOptions.from(options)
    url = license.pseudo_license? ? nil : url("/licenses/#{license.key}", options)

    hash = {
      key: license.key,
      name: license.name,
      spdx_id: license.spdx_id,
      url: url,
      node_id: license.global_relay_id,
    }

    return hash unless options[:full]

    hash.update({
      html_url: license.url,
      description: license.description,
      implementation: license.how,
      permissions: license.permissions,
      conditions: license.conditions,
      limitations: license.limitations,
      body: license.body,
      featured: license.featured?,
    })
  end

  def license_content_hash(license, options = {})
    return nil unless license
    options = Api::SerializerOptions.from(options)
    hash = content_hash(license, options)
    options[:full] = false
    hash[:license] = license_hash(options[:repo].license, options)

    hash
  end

  SimpleLicenseFragment = Api::App::PlatformClient.parse  <<-'GRAPHQL'
    fragment on License {
      id
      key
      name
      spdxId
      url
      pseudoLicense
    }
  GRAPHQL

  def graphql_simple_license_hash(license, options = {})
    return nil unless license
    options = Api::SerializerOptions.from(options)
    license = SimpleLicenseFragment.new(license)
    url = license.pseudo_license ? nil : url("/licenses/#{license.key}", options)

    {
      key: license.key,
      name: license.name,
      spdx_id: license.spdx_id,
      url: url,
      node_id: license.id,
    }
  end

  FullLicenseFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on License {
      ...Api::Serializer::LicensesDependency::SimpleLicenseFragment
      featured
      url
      description
      implementation
      permissions {
        key
      }
      conditions {
        key
      }
      limitations {
        key
      }
      body
    }
  GRAPHQL

  def graphql_full_license_hash(license, options = {})
    return nil unless license
    options = Api::SerializerOptions.from(options)
    license = FullLicenseFragment.new(license)
    hash = graphql_simple_license_hash(license, options)

    hash.update({
      featured: license.featured,
      html_url: license.url.to_s,
      description: license.description,
      implementation: license.implementation,
      permissions: license.permissions.map(&:key),
      conditions: license.conditions.map(&:key),
      limitations: license.limitations.map(&:key),
      body: license.body,
    })
  end
end
