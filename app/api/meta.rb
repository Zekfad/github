# rubocop:disable Style/FrozenStringLiteralComment

class Api::Meta < Api::App
  areas_of_responsibility :api

  include Api::App::EarthsmokeKeyHelper

  get "/meta" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/reference/meta#get-github-meta-information"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    payload = {
      verifiable_password_authentication: GitHub.auth.verifiable?,
    }

    if !GitHub.enterprise?
      payload.merge!(ssh_key_fingerprints: GitHub.ssh_host_key_fingerprints,
        hooks: GitHub.hook_ips, web: GitHub.web_ips,
        api: GitHub.api_ips, git: GitHub.git_ips,
        pages: GitHub.pages_a_record_ips, importer: GitHub.porter_worker_ips)
    else
      payload[:installed_version] = GitHub.version_number
    end

    deliver_raw payload
  end

  get "/meta/public_keys/webhooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    deliver_earthsmoke_key(name: Platform::EarthsmokeKeys::HOOKSHOT_SIGNING, feature_flag: :public_key_webhook_signing)
  end

  # this endpoint can be deprecated once docs changes reflecting the new endpoint is out.
  get "/meta/public_keys/token_scanning" do
    @route_owner = "@github/dsp-token-scanning"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    deliver_earthsmoke_key(name: GitHub::TokenScanning::ThirdPartyTokenProcessor::KEY_NAME, feature_flag: nil)
  end

  get "/meta/public_keys/secret_scanning" do
    @route_owner = "@github/dsp-token-scanning"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    deliver_earthsmoke_key(name: GitHub::TokenScanning::ThirdPartyTokenProcessor::KEY_NAME, feature_flag: nil)
  end

  get "/meta/public_keys/custom_tasks" do
    @route_owner = "@github/c2c-actions-experience-reviewers"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    deliver_earthsmoke_key(name: Platform::EarthsmokeKeys::CUSTOM_TASKS, feature_flag: nil)
  end
end
