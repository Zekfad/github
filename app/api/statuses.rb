# rubocop:disable Style/FrozenStringLiteralComment

class Api::Statuses < Api::App
  areas_of_responsibility :commit_statuses, :api
  statsd_tag_actions "/repositories/:repository_id/statuses/:sha"

  # Create a Status
  #
  # Statuses are meant to be immutable.  If a SHA has a
  # changed status, the integrator should just create a new status.
  #
  # To create a status, you must have push rights to the repo. For OAuth access,
  # you must also have the appropriate scopes.
  post "/repositories/:repository_id/statuses/:sha" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/repos#create-a-commit-status"
    repo = find_repo!
    control_access :write_status, resource: repo, allow_integrations: true, allow_user_via_integration: true
    authorize_content(repo, :create)

    GitHub.dogstats.time("status.time", tags: ["action:create"]) do
      # Introducing strict validation of the status.create
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # see: https://github.com/github/ecosystem-api/issues/1555
      data = receive_with_schema("status", "create", skip_validation: true)
      data = attr(data, :state, :description, :target_url, :context)

      GitHub.dogstats.increment("status.context", tags: ["action:create"]) if data["context"].present?

      data["sha"] = params[:sha]

      find_commit!(repo, params[:sha], @documentation_url)
      data[:oauth_application_id] = current_app.id if current_app
      status, _error = Statuses::Service.create_status(repo_id: repo.id, data: data, user_id: current_user.id)

      GitHub::PrefillAssociations.for_commit_statuses([status])

      if !status.new_record?
        GitHub.dogstats.increment("status", tags: ["action:create", "valid:true"])
        deliver :status_hash, status, status: 201
      else
        GitHub.dogstats.increment("status", tags: ["action:create", "valid:false"])
        deliver_error 422,
          errors: status.errors,
          documentation_url: @documentation_url
      end
    end
  end

  # Get statuses for a SHA, branch, or tag name.
  get "/repositories/:repository_id/statuses/*" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/repos#list-commit-statuses-for-a-reference"
    ref = params[:splat].first
    control_access :read_status,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    sha = repo.ref_to_sha(ref)
    return deliver_error 404 if sha.blank?

    commit_statuses = Statuses::Service.paginated_statuses(
      repo_id: repo.id,
      sha: sha,
      per_page: pagination[:per_page],
      page: pagination[:page],
    )
    GitHub::PrefillAssociations.for_commit_statuses(commit_statuses)

    deliver :status_hash, commit_statuses
  end

  # Get combined status for a given SHA, branch, or tag name.
  get "/repositories/:repository_id/status/*" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/repos#get-the-combined-status-for-a-specific-reference"
    ref = params[:splat].first
    repo = find_repo!
    control_access :read_status,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    sha = repo.ref_to_sha(ref)
    unless sha
      deliver_error!(404, message: "Ref not found", documentation_url: @documentation_url)
    end

    statuses = Statuses::Service.current_for_shas(repository_id: repo.id, shas: sha)
    combined_status = CombinedStatus.new(repo, sha, statuses: statuses, check_runs: [])
    combined_status.paginate(pagination)

    deliver :combined_status_hash, combined_status
  end

  private

  def authorize_content(authorizable, operation = :create)
    authorization = ContentAuthorizer.authorize(current_user, :status, operation, repo: authorizable)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end
end
