# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationsScim < Api::App
  include Api::App::SCIMHelpers

  allow_media SCIM::CONTENT_TYPE

  before do
    # Bypass IP allow list enforcement for calls to these endpoints since we do
    # not require an allow list to include the IP addresses of an IdP.
    bypass_allowed_ip_enforcement
  end

  # GET a list of users
  get "/scim/v2/organizations/:organization_id/Users" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/scim#list-scim-provisioned-identities"

    control_access :org_scim_reader,
      resource: organization = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: organization)
    deliver_error!(error_code, message: error_msg) if error_code

    identities = organization.saml_provider.external_identities.provisioned_by(:scim)

    if params[:filter]
      identities = begin
          identities.scim_filter(params[:filter])
        rescue SCIM::Filter::InvalidFilterError => e
          # https://tools.ietf.org/html/rfc7644#section-3.12
          deliver_scim_error!(400, scim_type: "invalidFilter", detail: e.message)
        end
    end

    results = paginated_results(identities)

    GitHub::PrefillAssociations.for_external_identities(results.resources)
    deliver_scim(:scim_identities_hash, results)
  end

  # get details about a specific user
  get "/scim/v2/organizations/:organization_id/Users/:external_identity_guid" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/scim#get-scim-provisioning-information-for-a-user"

    control_access :org_scim_reader,
      resource: organization = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: organization)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = organization.saml_provider.external_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    GitHub::PrefillAssociations.for_external_identities([@external_identity])
    deliver_scim(:scim_identity_hash, @external_identity)
  end

  # provision an external identity
  post "/scim/v2/organizations/:organization_id/Users" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/scim#provision-and-invite-a-scim-user"

    control_access :manage_org_users,
      resource: organization = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: organization)
    deliver_error!(error_code, message: error_msg) if error_code

    json = receive_with_schema("scim-user", "provision")

    user_data = Platform::Provisioning::ScimUserData.load(json)

    mapper = if GitHub.flipper[:org_idp_migrations].enabled?(organization)
      Platform::Provisioning::SCIMMapperWithMigrationSupport
    else
      Platform::Provisioning::ScimMapper
    end

    options = {
      organization_id: organization.id,
      user_data: user_data,
      inviter_id: current_user.id,
      mapper: mapper,
    }

    # If the request is being performed by an app
    # we need to pass along the `IntegrationInstallation`
    # record as the inviter instead of the `Bot`.
    if current_user.can_have_granular_permissions?
      options[:inviter_id]   = current_user.installation.id
      options[:inviter_type] = IntegrationInstallation
    end

    result = Platform::Provisioning::OrganizationIdentityProvisioner.provision_and_invite(**options)

    if result.success?
      GitHub::PrefillAssociations.for_external_identities([result.external_identity])
      deliver_scim(:scim_identity_hash, result.external_identity, status: 201)
    elsif result.invalid_identity_error?
      deliver_scim_error!(409, scim_type: "uniqueness")
    else
      GitHub::Logger.log({error: result.errors.first.reason,
                          error_messages: result.error_messages,
                          organization_id: organization.id})
      deliver_scim_error!(500, detail: result.errors.full_messages.join(" "))
    end
  end

  put "/scim/v2/organizations/:organization_id/Users/:external_identity_guid" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/scim#set-scim-information-for-a-provisioned-user"

    control_access :manage_org_users,
      resource: organization = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: organization)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = organization.saml_provider.external_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    json = receive_with_schema("scim-user", "update")
    user_data = Platform::Provisioning::ScimUserData.load(json)

    result = if json["active"] == false
      Platform::Provisioning::OrganizationIdentityProvisioner.deprovision \
        organization_id: organization.id,
        identity_guid: params[:external_identity_guid],
        actor_id: current_user.id
    else
      Platform::Provisioning::OrganizationIdentityProvisioner.provision \
        organization_id: organization.id,
        identity_guid: params[:external_identity_guid],
        user_data: user_data,
        mapper: Platform::Provisioning::ScimMapper,
        inviter_id: current_user.id
    end

    if result.success?
      GitHub::PrefillAssociations.for_external_identities([result.external_identity])
      deliver_scim(:scim_identity_hash, result.external_identity)
    elsif result.identity_not_found_error?
      deliver_resource_not_found!(params[:external_identity_guid])
    elsif result.forbidden_error?
      deliver_scim_error!(403, detail: result.error_messages.join("\n"))
    else
      # Something went wrong. Scim doesn't specify how to handle general errors
      # so let's just return a generic 500.
      deliver_scim_error!(500)
    end
  end

  # update a user attribute
  patch "/scim/v2/organizations/:organization_id/Users/:external_identity_guid" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/scim#update-an-attribute-for-a-scim-user"

    control_access :manage_org_users,
      resource: organization = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: organization)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = organization.saml_provider.external_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    patched_user_data = @external_identity.scim_user_data

    json = receive_with_schema("scim-user", "patch")
    json["Operations"].each do |op|
      if op["op"] == "remove" && op["path"].blank?
        deliver_scim_error!(400, scimType: "noTarget")
      elsif op["op"] == "replace" && op["value"].is_a?(Hash) && op["value"]["active"] == false
        # Deprovisioning request
        result = Platform::Provisioning::OrganizationIdentityProvisioner.deprovision \
          organization_id: organization.id,
          identity_guid: params[:external_identity_guid],
          actor_id: current_user.id

        if result.success?
          GitHub::PrefillAssociations.for_external_identities([result.external_identity])
          halt deliver_scim(:scim_identity_hash, result.external_identity)
        else
          deliver_scim_error!(403, detail: result.error_messages.join("\n"))
        end
      else
        patched_user_data = SCIM::Operation.apply(patched_user_data, op)
      end
    end

    result = Platform::Provisioning::OrganizationIdentityProvisioner.provision \
      organization_id: organization.id,
      identity_guid: params[:external_identity_guid],
      user_data: patched_user_data,
      mapper: Platform::Provisioning::ScimMapper

    if result.success?
      GitHub::PrefillAssociations.for_external_identities([result.external_identity])
      deliver_scim(:scim_identity_hash, result.external_identity)
    elsif result.forbidden_error?
      deliver_scim_error!(403, detail: result.error_messages.join("\n"))
    else
      # Something went wrong. Scim doesn't specify how to handle general errors
      # so let's just return a generic 500.
      deliver_scim_error!(500)
    end
  end

  # deprovision a user from the org
  delete "/scim/v2/organizations/:organization_id/Users/:external_identity_guid" do
    @documentation_url = "/rest/reference/scim#delete-a-scim-user-from-an-organization"

    @route_owner = "@github/iam"

    # Introducing strict validation of the scim-user.deprovision
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("scim-user", "deprovision", skip_validation: true)


    control_access :manage_org_users,
      resource: organization = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    error_code, error_msg = scim_processing_error_code?(target: organization)
    deliver_error!(error_code, message: error_msg) if error_code

    @external_identity = organization.saml_provider.external_identities.
      find_by_guid(params[:external_identity_guid])
    deliver_resource_not_found!(params[:external_identity_guid]) unless @external_identity

    result = Platform::Provisioning::OrganizationIdentityProvisioner.deprovision \
      organization_id: organization.id,
      identity_guid: params[:external_identity_guid],
      actor_id: current_user.id

    if result.success?
      deliver_empty status: 204
    elsif result.forbidden_error?
      deliver_scim_error!(403, detail: result.error_messages.join("\n"))
    else
      deliver_scim_error!(500, detail: result.error_messages.join("\n"))
    end
  end
end
