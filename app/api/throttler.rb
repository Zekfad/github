# frozen_string_literal: true
# Public: The Api::Throttler is a factory for RateLimiter that allows us to
# create and configure instances for use within an API endpoint. Sub-class will
# likely want to define their own key and options logic for customization.
class Api::Throttler
  attr_reader :options, :key

  # Public: Initialize a Api::Throttler.
  #
  # args -
  #        key     - The memcache key used to define the rate limit
  #        options - Hash of `RateLimiter` options for defining the limit.
  def initialize(key, options = {})
    @key = key
    @options = options
    @runway = options[:runway] || 0
    @max_tries = options[:max_tries] || RateLimiter::DEFAULT_MAX_TRIES
    @ttl = options[:ttl] || RateLimiter::DEFAULT_TTL
    @amount = options[:amount] || RateLimiter::DEFAULT_AMOUNT
    @distributed_redis = options[:redis] || GitHub.rate_limiter_redis
  end

  # Increases the current rate limit status for `@key` by `@amount`
  #
  # @return [RateLimiter]
  def rate!
    tries, ttl = with_circuit_breaking(action: :rate) do
      shard = @distributed_redis.shard_for(@key).write
      shard.eval(RATE_SCRIPT, [@key], [@amount, @ttl])
    end
    legacy_rate_limit_result(tries, ttl)
  end

  # Gets the current rate limit status (number of tries and expiration)
  # without incrementing any counters
  #
  # @return [RateLimiter]
  def check
    @check ||= begin
      tries, ttl = with_circuit_breaking(action: :check) do
        read_only_shard = @distributed_redis.shard_for(@key).read_only
        read_only_shard.eval(CHECK_SCRIPT, [@key])
      end
      legacy_rate_limit_result(tries || 0, ttl || @ttl)
    end
  end

  def remove!
    @distributed_redis.shard_for(@key).write.del(@key)
  end

  private

  def legacy_rate_limit_result(tries, ttl)
    expiration = Time.now.utc + ttl
    hard_max_tries = @max_tries + @runway
    at_limit = tries >= hard_max_tries
    remain = if tries > @runway
      # this client has consumed the runway; warn them.
      [(hard_max_tries - tries), 0].max
    else
      # This client still has runway; don't tell how much,
      # but show that the full rate limit is still ahead of them.
      @max_tries
    end

    RateLimiter.new(
      # Legacy rate limiter took only the last part of the key for a "unique key"
      key.split(":").last,
      tries,
      at_limit,
      # This is the number of tries we _tell_ people they have;
      # it ignores the runway before reaching the soft limit.
      @max_tries,
      # The number of tries _after_ the runway;
      # If the client hasn't reached the runway, this is the full `@max_tries`.
      remain,
      expiration,
    )
  end

  # Safe way to incr + expire
  # See https://redis.io/commands/incr#pattern-rate-limiter-2
  RATE_SCRIPT = <<~LUA
    local current = redis.call("incrby", KEYS[1], ARGV[1])

    if current == tonumber(ARGV[1]) then
        redis.call("expire", KEYS[1], ARGV[2])
    end

    local ttl = redis.call("ttl", KEYS[1])

    return { current, ttl }
  LUA

  # Getting both the value and the expiration
  # of key as needed by our algorithm needs to be ran
  # in an atomic way, hence the script.
  CHECK_SCRIPT = <<~LUA
    local tries = redis.call("get", KEYS[1])
    local ttl = redis.call("ttl", KEYS[1])

    return { tonumber(tries), ttl }
  LUA


  def with_circuit_breaking(action:, &block)
    breaker = self.class.circuit_breaker(action: action)

    if !breaker.allow_request?
      GitHub.dogstats.increment("redis_rate_limiter.#{action}.circuit_open")
      return [0, @ttl]
    end

    res = block.call
    breaker.success

    res
  rescue *GitHub::Config::Redis::REDIS_DOWN_EXCEPTIONS, ::Redis::BaseError => e
    # On Redis errors, we want to report the failure, but we fail open when it comes to rate limits.
    # Users should not be impacted.
    breaker.failure
    GitHub.dogstats.increment "redis_rate_limiter.#{action}.errors", tags: ["error:#{e.class.name}"]
    Failbot.report!(e)
    return [0, @ttl]
  end

  class << self
    def circuit_breaker(action:)
      @circuit_breakers ||= {}
      return @circuit_breakers[action] if @circuit_breakers.key?(action)

      options = {
        # seconds after tripping circuit before allowing retry
        sleep_window_seconds: 5,
        # number of requests that must be made within a statistical window
        # before open/close decisions are made using stats
        request_volume_threshold: 5,
        # % of "marks" that must be failed to trip the circuit
        error_threshold_percentage: 50,
        # number of seconds in the statistical window
        window_size_in_seconds: 30,
        # size of buckets in statistical window
        bucket_size_in_seconds: 5,
      }

      options[:instrumenter] = GitHub if GitHub.respond_to?(:instrument)

      @circuit_breakers[action] = Resilient::CircuitBreaker.get("rate_limiter_#{action}", options)
    end
  end
end
