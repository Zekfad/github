# rubocop:disable Style/FrozenStringLiteralComment

class Api::Porter < Api::App
  areas_of_responsibility :porter, :api

  before do
    deliver_error!(404) unless GitHub.porter_available?
  end

  CustomErrors = {
    Porter::ApiClient::ErrorRateLimit::Error => "error_rate_limit".freeze,
    Porter::ApiClient::ClientRequestLimit::Error => "internal_request_limit".freeze,
    Faraday::TimeoutError => "timeout_error".freeze,
  }.freeze

  error *CustomErrors.keys do
    if code = CustomErrors[env["sinatra.error"].class]
      @meta["X-GitHub-Error-Class"] = code
      GitHub.dogstats.increment "porter", tags: ["via:api", "status:503", "code:#{code}"]
    end
    deliver_error! 503,
      message: "The import API is temporarily unavailable."
  end

  # start an import
  put "/repositories/:repository_id/import" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#start-an-import"
    control_access :manage_import_writer, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: true


    # Introducing strict validation of the import.start
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("import", "start", skip_validation: true)

    proxy_errors do
      response = porter.start_import(data)

      current_repo.porter_started!
      deliver :porter_import_hash, response, repo: repo,
        # Porter responds with 201 or 200, depending on whether the import is new or was already present.
        status: porter.last_response.status
    end
  end

  # stop an import
  delete "/repositories/:repository_id/import" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#cancel-an-import"

    receive_with_schema("import", "delete")

    control_access :manage_import_writer, resource: find_repo!, allow_integrations: false, allow_user_via_integration: true

    proxy_errors do
      porter.stop_import
      current_repo.porter_stopped!
      deliver_empty(status: 204)
    end
  end

  # get import progress
  get "/repositories/:repository_id/import" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#get-an-import-status"
    control_access :manage_import_reader, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: true

    proxy_errors do
      response = porter.import_status
      deliver :porter_import_hash, response, repo: repo
    end
  end

  # get the author list
  get "/repositories/:repository_id/import/authors" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#get-commit-authors"
    control_access :manage_import_reader, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: true

    proxy_errors do
      response = porter.authors(params.slice("since"))
      deliver :porter_author_hash, response, repo: repo
    end
  end

  # set the mapped author information
  patch "/repositories/:repository_id/import/authors/:author_id" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#map-a-commit-author"
    control_access :manage_import_writer, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: true

    data = receive_with_schema("import", "update-author")

    proxy_errors do
      response = porter.update_author(params[:author_id], data)

      deliver :porter_author_hash, response, repo: repo
    end
  end

  # "yes, use git-lfs to push my large files"
  # or
  # "no, do not use git-lfs on this project"
  patch "/repositories/:repository_id/import/lfs" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#update-git-lfs-preference"
    control_access :manage_import_writer, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: true

    data = receive(Hash, required: false) || {}

    # Fill in missing data.
    data["committer"] ||= {}
    data["committer"]["name"] ||= current_user.git_author_name
    data["committer"]["email"] ||= current_user.git_author_email
    data["committer"]["time"] ||= Time.now.to_i
    data["committer"]["time_offset"] ||= current_user.time_zone.utc_offset

    proxy_errors do
      response = porter.set_lfs_preference(data)

      # Introducing strict validation of the import.update-lfs-preference
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # TODO: replace `receive` with `receive_with_schema`
      # see: https://github.com/github/ecosystem-api/issues/1555
      _ = receive_with_schema("import", "update-lfs-preference", skip_validation: true)

      deliver :porter_import_hash, response, repo: repo
    end
  end

  # restart import or update auth/project choice for existing import
  patch "/repositories/:repository_id/import" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#update-an-import"
    control_access :manage_import_writer, resource: repo = find_repo!, allow_integrations: false, allow_user_via_integration: true

    data = receive_with_schema("import", "update")

    proxy_errors do
      response = porter.update_import(data)

      deliver :porter_import_hash, response, repo: repo,
        # Porter responds with 200 because the import was already present.
        status: porter.last_response.status
    end
  end

  # get the large files list
  get "/repositories/:repository_id/import/large_files" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#get-large-files"
    control_access :manage_import_reader, resource: find_repo!, allow_integrations: false, allow_user_via_integration: true

    proxy_errors do
      response = porter.large_files(pagination)
      deliver :porter_large_files_hash, response["entries"]
    end
  end

  private

  def rate_limit_configuration
    # Only put new import requests into the SOURCE_IMPORT_FAMILY
    # This should be rolled back once the google code migration storm is over.
    if request.put?
      Api::RateLimitConfiguration.for(
        Api::RateLimitConfiguration::SOURCE_IMPORT_FAMILY,
        self,
      )
    else
      super
    end
  end

  # An API client for porter.
  def porter
    @porter ||=
      Porter::ApiClient.new \
        current_user: current_user,
        current_repository: find_repo,
        set_import_started: false,
        send_import_status: false,
        site_admin: Api::Serializer.instance_admin?(current_user)
  end

  # Rescue errors and deliver an appropriate error.
  def proxy_errors
    yield
  rescue Porter::ApiClient::Error => e
    if e.status >= 500
      Failbot.report!(e)
    end
    if e.status == 429
      deliver_error! 403, message: "Rate Limit Exceeded"
    elsif message = e.public_error_message
      deliver_error! e.status, message: message
    else
      deliver_error! e.status
    end
  end
end
