# frozen_string_literal: true

class Api::UserFollowers < Api::App
  # List a User's followers
  get "/user/:user_id/followers" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-followers-of-a-user"
    control_access :read_user_public,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    users = paginate_rel(find_user!.followers.not_suspended.filter_spam_for(current_user))

    deliver :user_hash, users
  end

  # List the authenticated User's followers
  get "/user/followers" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-followers-of-the-authenticated-user"
    control_access :read_user_followers,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    users = paginate_rel(current_user.followers.not_spammy)
    deliver :user_hash, users
  end

  # List who a User is following
  get "/user/:user_id/following" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-the-people-a-user-follows"
    control_access :read_user_public,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    users = paginate_rel(find_user!.following.not_suspended.filter_spam_for(current_user))

    deliver :user_hash, users
  end

  # List users the authenticated User is following
  get "/user/following" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-the-people-the-authenticated-user-follows"
    control_access :read_user_following,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    users = paginate_rel(current_user.following.not_spammy)
    deliver :user_hash, users
  end

  # Get if the authenticated User is following a User
  get "/user/following/:username" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#check-if-a-person-is-followed-by-the-authenticated-user"
    control_access :read_user_following,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    other_user = this_user

    if !other_user.spammy? && current_user.following?(other_user)
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Check if a user is following another user
  get "/user/:user_id/following/:target_user" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#check-if-a-user-follows-another-user"
    control_access :read_user_public,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    user = find_user!
    target_user = User.find_by_login(params[:target_user])

    if !target_user ||
      (user.spammy? && user != current_user) ||
      (target_user.spammy? && target_user != current_user) ||
      !user.following?(target_user)

      deliver_error 404
    else
      deliver_empty(status: 204)
    end
  end

  # Follow a User
  put "/user/following/:username" do
    @route_owner = "@github/profile"

    # Introducing strict validation of the user.follow
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("user", "follow", skip_validation: true)

    @documentation_url = "/rest/reference/users#follow-a-user"
    control_access :follow,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    following = Following.new(user: current_user, following: this_user)
    if following.creation_rate_limited?
      deliver_error! 429,
        message: "You can't perform that action at this time",
        errors: [api_error(:User, :following, :invalid, value: "rate limit exceeded")]
    else
      current_user.follow(this_user, context: "api")
      deliver_empty(status: 204)
    end
  end

  # Unfollow a User
  delete "/user/following/:username" do
    @route_owner = "@github/profile"
    receive_with_schema("user", "unfollow")

    @documentation_url = "/rest/reference/users#unfollow-a-user"
    control_access :unfollow,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user.unfollow(this_user, context: "api")

    deliver_empty(status: 204)
  end
end
