# rubocop:disable Style/FrozenStringLiteralComment

class Api::Applications < Api::App
  areas_of_responsibility :ecosystem_apps
  DEPRECATION_FLIPPER_KEY = :api_applications_deprecation_emails
  DEPRECATION_EMAIL_TTL   = 2.weeks
  DEPRECATION_STATS_KEY   = "api.applications.deprecated_endpoints.email_sent"

  GH_APPS_DEPRECATION_FLIPPER_KEY = :api_applications_gh_apps_deprecation_emails

  # https://github.com/github/ecosystem-apps/issues/743
  PREVENT_EMAIL_FLIPPER_KEY = :opt_out_of_api_applications_deprecation_emails

  before do
    @accepted_scopes = []
  end

  after do
    maybe_deliver_deprecation_email
  end

  # This API endpoint provides access for applications to check tokens without
  # hitting the rate limit for failed login attempts. Because this
  # endpoint deals with OAuth tokens themselves, it is only accessible via
  # Basic Authentication using application credentials.
  #
  # By overridding #attempt_login, we disable login via Basic Auth using
  # *user* credentials, and we enable login via Basic Auth using OAuth
  # application credentials.
  def attempt_login
    @current_user = nil
    deliver_error!(404) unless request_credentials.login_password_present?

    reject_for_bad_credentials! unless current_app
  end

  # Public: Determines whether the request is sufficiently authenticated to
  # access the API when running in Private Mode (i.e., if the request is
  # authenticated as an OAuth app).
  #
  # Returns true if the request is authenticated; false otherwise.
  def authenticated_for_private_mode?
    current_app
  end

  # [Deprecated] Check a token using application credentials
  get "/applications/:client_id/tokens/:access_token" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#check-an-authorization"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    mark_endpoint_as_deprecated

    # base the DB selection on the token param instead of the
    # HTTP_AUTHORIZATION header token
    token = params[:access_token]
    token_operations = DatabaseSelector::LastOperations.from_token(token)

    DatabaseSelector.instance.read_from_database(last_operations: token_operations, called_from: :api_applications) do
      ensure_oauth_creds_match_request!
      access = find_token_for_oauth_app_from_params
      GitHub::PrefillAssociations.for_oauth_accesses([access])
      deliver :oauth_access_hash, access, user: true, token: token
    end
  end

  # [New] Check a token using application credentials
  post "/applications/:client_id/token" do # rubocop:disable GitHub/DuplicateRoutesAreDefinedTogether
    @route_owner = "@github/ecosystem-apps"
    # TODO: update docs ref
    @documentation_url = "/rest/reference/apps#check-a-token"

    data = receive_with_schema("authorization", "check-token")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    token = data["access_token"]
    ensure_oauth_creds_match_request!
    access = find_token_for_oauth_app_from_body(data)
    GitHub::PrefillAssociations.for_oauth_accesses([access])
    deliver :oauth_access_hash, access, user: true, token: token
  end

  # Delete all app tokens using application credentials
  delete "/applications/:client_id/tokens" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    ensure_oauth_creds_match_request!
    deliver_error!(403, message: "Forbidden") if token_revocation_forbidden?
    deliver_error!(410, message: "Gone via the API. You can still revoke all tokens via the settings page for this application.")
  end

  # [Deprecated] Reset OauthAccess#token using application credentials
  post "/applications/:client_id/tokens/:access_token" do
    @route_owner = "@github/ecosystem-apps"

    # Introducing strict validation of the authorization.reset JSON schema
    # would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("authorization", "reset", skip_validation: true)

    @documentation_url = "/rest/reference/apps#reset-an-authorization"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    mark_endpoint_as_deprecated

    ensure_oauth_creds_match_request!
    access = find_token_for_oauth_app_from_params

    # Older tokens will have a code set that is not needed.
    access.code = nil
    GitHub::PrefillAssociations.for_oauth_accesses([access])
    deliver :oauth_access_hash, access, user: true, token: access.reset_token
  end

  # [New] Reset OauthAccess#token using application credentials
  patch "/applications/:client_id/token" do # rubocop:disable GitHub/DuplicateRoutesAreDefinedTogether
    @route_owner = "@github/ecosystem-apps"
    # TODO: update docs ref
    @documentation_url = "/rest/reference/apps#reset-a-token"

    data = receive_with_schema("authorization", "reset-token")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    ensure_oauth_creds_match_request!
    access = find_token_for_oauth_app_from_body(data)

    # Older tokens will have a code set that is not needed.
    access.code = nil
    GitHub::PrefillAssociations.for_oauth_accesses([access])
    deliver :oauth_access_hash, access, user: true, token: access.reset_token
  end

  # [Deprecated] Delete an app token using application credentials
  delete "/applications/:client_id/tokens/:access_token" do
    @route_owner = "@github/ecosystem-apps"

    # Introducing strict validation of the authorization.revoke JSON schema
    # would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("authorization", "revoke", skip_validation: true)

    @documentation_url = "/rest/reference/apps#revoke-an-authorization-for-an-application"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    mark_endpoint_as_deprecated

    ensure_oauth_creds_match_request!
    access = find_token_for_oauth_app_from_params
    access.destroy_with_explanation(:oauth_application)
    deliver_empty status: 204
  end

  # [New] Delete an app token using application credentials
  delete "/applications/:client_id/token" do
    @route_owner = "@github/ecosystem-apps"
    # TODO: update docs ref
    @documentation_url = "/rest/reference/apps#delete-an-app-token"

    data = receive_with_schema("authorization", "revoke-token")
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    ensure_oauth_creds_match_request!
    access = find_token_for_oauth_app_from_body(data)
    access.destroy_with_explanation(:oauth_application)
    deliver_empty status: 204
  end

  # [Deprecated] Delete an app authorization using application credentials
  delete "/applications/:client_id/grants/:access_token" do
    @route_owner = "@github/ecosystem-apps"

    # Introducing strict validation of the authorization-grant.delete-for-app
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("application-grant", "delete-for-app", skip_validation: true)

    @documentation_url = "/rest/reference/apps#revoke-a-grant-for-an-application"

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false
    mark_endpoint_as_deprecated

    ensure_oauth_creds_match_request!
    authorization = find_token_for_oauth_app_from_params.authorization
    authorization.destroy_with_explanation(:oauth_application)
    deliver_empty status: 204
  end

  # [New] Delete an app authorization using application credentials
  delete "/applications/:client_id/grant" do
    @route_owner = "@github/ecosystem-apps"
    # TODO: update docs ref
    @documentation_url = "/rest/reference/apps#delete-an-app-authorization"

    data = receive_with_schema("application-grant", "delete-for-app-token")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    ensure_oauth_creds_match_request!
    authorization = find_token_for_oauth_app_from_body(data).authorization
    authorization.destroy_with_explanation(:oauth_application)
    deliver_empty status: 204
  end

  post "/applications/:client_id/token/scoped" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/v3/oauth_authorizations/#scope-a-grant-for-an-application"

    data = receive_with_schema("authorization", "scope-token")

    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    ensure_oauth_creds_match_request!
    access = find_token_for_oauth_app_from_body(data)

    target = User.find_by(id: data["target_id"])
    record_or_404(target)

    new_access, error = current_app.grant_scoped_access_from(access, target, permissions: data["permissions"], repository_ids: data["repository_ids"])

    if error
      case error[:error]
      when :feature_not_enabled
        deliver_error!(415, message: error[:error_description])
      when :invalid_token
        deliver_error!(401, message: error[:error_description])
      when :token_creation_failure
        deliver_error!(500, message: error[:error_description])
      else
        deliver_error!(403, message: error[:error_description])
      end
    end

    token, _ = new_access.redeem
    installation = new_access.installation

    GitHub::PrefillAssociations.for_oauth_accesses([new_access])

    case installation
    when SiteScopedIntegrationInstallation
      GitHub::PrefillAssociations.for_site_scoped_integration_installations([installation], integration: current_app)
    else
      GitHub::PrefillAssociations.for_scoped_integration_installations([installation], integration: current_app)
    end

    deliver :oauth_access_hash, new_access, user: true, token: token, installation: installation
  end

  private

  # Internal: Overrides the default behavior for finding the current app. Finds
  # the OauthApplication/Integration (if any) associated with the current request based
  # on the client ID and client secret provided via Basic Auth.
  #
  # Returns an OauthApplication/Integration or nil.
  def find_current_app
    if (oauth_app = current_app_via_authorization_header)
      oauth_app
    elsif (github_app = current_integration_via_authorization_header)
      github_app
    end
  end

  def current_integration_via_authorization_header
    Integration.find_by(
      key: request_credentials.login,
      secret: request_credentials.password,
    )
  end

  def oauth_creds_match_request?
    return false unless current_app

    SecurityUtils.secure_compare(params[:client_id], current_app.key)
  end

  # See https://github.com/github/github/issues/33359
  # We're temporarily disabling token revocation for our
  # own apps until we can deprecate the entire endpoint.
  def token_revocation_forbidden?
    current_app && current_app.github_owned?
  end

  def find_token_for_oauth_app_from_params
    find_access_token(params[:access_token])
  end

  def find_token_for_oauth_app_from_body(body)
    find_access_token(body["access_token"])
  end

  def find_access_token(token)
    if (access = current_app.accesses.find_by_token(token))
      return access if access.user.can_authenticate_via_oauth?
    end

    deliver_error!(404)
  end

  def ensure_oauth_creds_match_request!
    deliver_error!(404) unless oauth_creds_match_request?
  end

  def mark_endpoint_as_deprecated
    # This helper won't set the header in Enterprise
    deprecated(
      deprecation_date: Date.iso8601("2020-02-14"),
      sunset_date: Date.iso8601("2021-05-05"),
      info_url: "#{GitHub.developer_blog_url}/2020-02-14-deprecating-oauth-app-endpoint",
    )

    @deprecated_endpoint = true
  end

  def deprecated_endpoint?
    defined?(@deprecated_endpoint) && @deprecated_endpoint
  end

  def maybe_deliver_deprecation_email
    return unless response.successful?
    return unless deprecated_endpoint?
    return if GitHub.flipper[PREVENT_EMAIL_FLIPPER_KEY].enabled?(current_app)

    # REF: https://github.com/github/ecosystem-api/issues/1807#issuecomment-593662031
    if current_app.is_a?(Integration)
      return unless GitHub.flipper[GH_APPS_DEPRECATION_FLIPPER_KEY].enabled?(current_app)
    else
      return unless GitHub.flipper[DEPRECATION_FLIPPER_KEY].enabled?(current_app)
    end

    app_type = current_app.class.to_s.underscore
    if GitHub::ActionRestraint.perform?(
         "deprecated_api_applications_endpoints",
         interval: DEPRECATION_EMAIL_TTL,
         app_type: app_type,
         app_id:   current_app.id,
       )

      if current_app.owner.is_a?(Organization)
        OrganizationMailer.api_applications_endpoints_deprecation(current_app, time: Time.now).deliver_later
      else
        AccountMailer.api_applications_endpoints_deprecation(current_app, time: Time.now).deliver_later
      end

      GitHub.dogstats.increment(DEPRECATION_STATS_KEY, tags: ["app_type:#{app_type}"])
    end
  end
end
