# frozen_string_literal: true

class Api::InteractionLimits < Api::App
  areas_of_responsibility :api, :community_and_safety
  include Scientist

  before do
    deliver_error!(404) unless GitHub.interaction_limits_enabled?
  end

  GetInteractionAbilityQuery = PlatformClient.parse <<-'GRAPHQL'
    query($repositoryId: ID!) {
      node(id: $repositoryId) {
        ... on Repository {
          ...Api::Serializer::RepositoriesDependency::RepositoryInteractionAbilityFragment
        }
      }
    }
  GRAPHQL

  # Access the interaction ability settings for a repository
  get "/repositories/:repository_id/interaction-limits" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/interactions#get-interaction-restrictions-for-a-repository"
    repo = find_repo!

    control_access :read_repository_interaction_limits, resource: repo, allow_integrations: true, allow_user_via_integration: true, forbid: true

    if repo.private?
      deliver_error! 405, errors: "Interaction limits cannot be set for private repositories."
    end

    variables = { repositoryId: repo.global_relay_id }

    results = platform_execute(GetInteractionAbilityQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deliver_graphql_error! errors: results.errors, resource: "Repository"
    end

    deliver :graphql_repository_interaction_ability_hash, results.data.node
  end

  SetInteractionAbilityQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($repositoryId: ID!, $limit: RepositoryInteractionLimit!) {
      setRepositoryInteractionLimit(input: { repositoryId: $repositoryId, limit: $limit}) {
        repository {
          ...Api::Serializer::RepositoriesDependency::RepositoryInteractionAbilityFragment
        }
      }
    }
  GRAPHQL

  # Set the interaction ability settings for a repository
  put "/repositories/:repository_id/interaction-limits" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/interactions#set-interaction-restrictions-for-a-repository"
    repo = find_repo!

    control_access :set_repository_interaction_limits,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: true

    if repo.private?
      deliver_error! 405, errors: "Interaction limits cannot be set for private repositories."
    end

    if repo.owner.organization? && RepositoryInteractionAbility.has_active_limits?(:organization, repo.owner)
      deliver_error! 409, errors: "You cannot set repository level interaction limits when an organization level limit is enabled."
    end

    data = receive_with_schema("interaction-limit", "update-for-repository")
    limit = Platform::Enums::Base.convert_string_to_enum_value(data["limit"])
    variables = { repositoryId: repo.global_relay_id, limit: limit }

    results = platform_execute(SetInteractionAbilityQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deliver_graphql_error! errors: results.errors, resource: "SetRepositoryInteractionAbility"
    end

    deliver :graphql_repository_interaction_ability_hash, results.data.set_repository_interaction_limit.repository
  end

  # Disable any active interaction limit for a repository
  delete "/repositories/:repository_id/interaction-limits" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/interactions#remove-interaction-restrictions-for-a-repository"
    repo = find_repo!

    control_access :set_repository_interaction_limits,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: true

    receive_with_schema("interaction-limit", "delete-for-repository")

    if repo.private?
      deliver_error! 405, errors: "Interaction limits cannot be set for private repositories."
    end

    if repo.owner.organization? && RepositoryInteractionAbility.has_active_limits?(:organization, repo.owner)
      deliver_error! 409, errors: "You cannot set repository level interaction limits when an organization level limit is enabled."
    end

    variables = { repositoryId: repo.global_relay_id, limit: "NO_LIMIT" }

    results = platform_execute(SetInteractionAbilityQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deliver_graphql_error! errors: results.errors, resource: "SetRepositoryInteractionAbility"
    end

    deliver_empty status: 204
  end

  GetOrgInteractionAbilityQuery = PlatformClient.parse <<-'GRAPHQL'
    query($organizationId: ID!) {
      node(id: $organizationId) {
        ... on Organization {
          ...Api::Serializer::OrganizationsDependency::OrganizationInteractionAbilityFragment
        }
      }
    }
  GRAPHQL

  # Access the interaction ability settings for an organization
  get "/organizations/:organization_id/interaction-limits" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/interactions#get-interaction-restrictions-for-an-organization"
    org = find_org!

    control_access :read_organization_interaction_limits, resource: org, allow_integrations: true, allow_user_via_integration: true, forbid: true

    variables = { organizationId: org.global_relay_id }

    results = platform_execute(GetOrgInteractionAbilityQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deliver_graphql_error! errors: results.errors, resource: "Organization"
    end

    deliver :graphql_organization_interaction_ability_hash, results.data.node
  end

  SetOrgInteractionAbilityQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($organizationId: ID!, $limit: RepositoryInteractionLimit!) {
      setOrganizationInteractionLimit(input: { organizationId: $organizationId, limit: $limit}) {
        organization {
          ...Api::Serializer::OrganizationsDependency::OrganizationInteractionAbilityFragment
        }
      }
    }
  GRAPHQL

  # Set the interaction ability settings for an organization
  put "/organizations/:organization_id/interaction-limits" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/interactions#set-interaction-restrictions-for-an-organization"
    org = find_org!

    control_access :set_organization_interaction_limits, resource: org, allow_integrations: true, allow_user_via_integration: true, forbid: true

    data = receive_with_schema("interaction-limit", "update-for-organization")
    limit = Platform::Enums::Base.convert_string_to_enum_value(data["limit"])
    variables = { organizationId: org.global_relay_id, limit: limit }

    results = platform_execute(SetOrgInteractionAbilityQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deliver_graphql_error! errors: results.errors, resource: "SetOrganizationInteractionAbility"
    end

    deliver :graphql_organization_interaction_ability_hash, results.data.set_organization_interaction_limit.organization
  end

  # Disable any active interaction limit for an organization
  delete "/organizations/:organization_id/interaction-limits" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/interactions#remove-interaction-restrictions-for-an-organization"
    org = find_org!

    control_access :set_organization_interaction_limits, resource: org, allow_integrations: true, allow_user_via_integration: true, forbid: true

    receive_with_schema("interaction-limit", "delete-for-organization")

    variables = { organizationId: org.global_relay_id, limit: "NO_LIMIT" }

    results = platform_execute(SetOrgInteractionAbilityQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deliver_graphql_error! errors: results.errors, resource: "SetOrganizationInteractionAbility"
    end

    deliver_empty status: 204
  end
end
