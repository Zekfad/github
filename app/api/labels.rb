# rubocop:disable Style/FrozenStringLiteralComment

class Api::Labels < Api::App
  module Helpers
    # Fetch all Labels from the given data. Labels that do not exist will be created
    # on-the-fly. Returns early with a 422 if input is not a String or Hash with a
    # value for 'name'.
    #
    # issue - The Issue that is being updated.
    # data  - An Array of Hashes or String names, usually pulled from the body
    #         of the API request.
    #
    # Returns an Array of Labels.
    def labels_for(repo, data)
      return [] if Array(data).compact.blank?

      begin
        label_names = label_names_from_input(data)
      rescue
        deliver_error! 400,
          message: "Invalid Labels: #{data.inspect}.  Must be an Array of strings: ['bug', 'ui']",
          documentation_url: "/v3/issues/labels/"
      end

      if label_names.blank? || label_names.any?(&:blank?)
        deliver_error! 422,
          errors: [api_error(:Label, :name, :missing_field)]
      end

      labels = repo.find_labels_by_name(label_names).to_a
      existing_label_names = labels.map { |l| l.name.downcase }

      Label.transaction do
        label_names.each do |name|
          unless existing_label_names.include?(name.downcase)
            new_label = Label.create!(repository: repo, name: name)
            new_label.instrument_creation(context: "api")
            labels << new_label
          end
        end
      end

      labels
    rescue ActiveRecord::RecordInvalid => e
      deliver_error! 422,
        errors: [api_error(:Label, :name, :invalid, value: e.record.name)]
    end

    # Internal: Parse label names from API input
    #
    # data - Array where each element is a label name
    #        String or a label Hash.
    #
    # Returns an Array of label name Strings.
    def label_names_from_input(data)
      return nil unless data.respond_to?(:map)

      data.map do |input|
        case input
        when Hash   then input["name"]
        when String then input
        end
      end
    end

    def label_attributes_from_input(data)
      if data.is_a?(Hash)
        return data["labels"].map { |label| { "name" => label } }
      end

      data.map do |input|
        case input
        when Hash   then input
        when String then { "name" => input }
        end
      end
    end
  end

  include Helpers, Api::Issues::EnsureIssuesEnabled
  include Scientist

  # List all Labels for this Repository
  get "/repositories/:repository_id/labels" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-labels-for-a-repository"
    repo = current_repo
    control_access :list_repo_labels,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    labels = repo.labels
    labels = paginate_rel(labels)
    deliver :label_hash, labels, repo: repo
  end

  # Create a Label
  post "/repositories/:repository_id/labels" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#create-a-label"

    control_access :create_repo_label, repo: repo = current_repo, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    data = receive_with_schema("label", "create-legacy")
    allowed_attributes = [:name, :color, :description]
    label = repo.labels.build attr(data, *allowed_attributes)

    if label.save
      label.instrument_creation(context: "api")
      deliver :label_hash, label, status: 201, repo: repo
    else
      deliver_error 422, errors: label.errors
    end
  end

  LabelQuery = PlatformClient.parse <<-'GRAPHQL'
    query($label_id: ID!){
      node(id: $label_id){
        ... on Label{
          ...Api::Serializer::IssuesDependency::LabelFragment
        }
      }
    }
  GRAPHQL

  # Get a single Label
  # use :splat so sinatra can parse weird tags with periods in them.
  get "/repositories/:repository_id/labels/*" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#get-a-label"
    control_access :get_repo_label,
      repo: repo = current_repo,
      allow_integrations: true,
      allow_user_via_integration: true

    label_name = label_id
    label = repo.labels.find_by_name(label_name)

    deliver :label_hash, label,
      repo: repo,
      last_modified: calc_last_modified(label),
      compare_payload_to: :label_candidate
  end

  def label_candidate(label, options)
    variables = { label_id: label.global_relay_id }

    results = platform_execute(LabelQuery, variables: variables)
    label = results.data.node

    Api::Serializer.serialize(:graphql_label_hash, label, options)
  end

  # Update a Label
  # use :splat so sinatra can parse weird tags with periods in them.
  verbs :patch, :post, "/repositories/:repository_id/labels/*" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#update-a-label"
    control_access :update_repo_label, repo: repo = current_repo, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    id    = label_id
    data  = receive_with_schema("label", "update-legacy")

    if label = repo.labels.find_by_name(id)
      allowed_attributes = [:name, :color, :description]
      attributes = attr(data, *allowed_attributes)
      attributes[:name] ||= attr(data, :new_name)[:new_name] if data["new_name"]

      if label.update(attributes)
        deliver :label_hash, label, repo: repo
      else
        deliver_error 422, errors: label.errors
      end
    else
      deliver_error 404
    end
  end

  # Delete a label
  delete "/repositories/:repository_id/labels/*" do
    @route_owner = "@github/pe-issues-projects"

    # Introducing strict validation of the label.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("label", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/issues#delete-a-label"
    control_access :delete_repo_label, repo: repo = current_repo, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    id = label_id
    if label = repo.labels.find_by_name(id)
      label.destroy
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  IssueLabelsQuery = PlatformClient.parse <<-'GRAPHQL'
    query($issueId: ID!, $limit: Int!, $numericPage: Int) {
      node(id: $issueId) {
        ... on Issue {
          lastModifiedAt: updatedAt
          labels(first: $limit, numericPage: $numericPage) {
            totalCount
            nodes {
              lastModifiedAt: updatedAt
              ...Api::Serializer::IssuesDependency::LabelFragment
            }
          }
        }
        ... on PullRequest {
          labels(first: $limit, numericPage: $numericPage) {
            totalCount
            nodes {
              lastModifiedAt: updatedAt
              ...Api::Serializer::IssuesDependency::LabelFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # List labels on the Issue
  get "/repositories/:repository_id/issues/:issue_number/labels" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-labels-for-an-issue"
    repo = current_repo
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))

    control_access :list_issue_labels,
      repo: repo,
      resource: issue,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    ensure_issues_enabled_or_pr! repo, issue

    variables = {
      issueId: issue.global_relay_id,
      limit: pagination[:per_page] || DEFAULT_PER_PAGE,
      numericPage: pagination[:page],
    }
    results = platform_execute(IssueLabelsQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Issue",
        documentation_url: @documentation_url,
      })
    else
      labels  = results.data.node.labels
      nodes   = labels.nodes.map { |n| Api::Serializer::IssuesDependency::LabelFragment.new(n) }

      last_modified = calc_last_modified([issue, *nodes])

      paginator.collection_size = labels.total_count
      deliver :graphql_label_hash, labels.nodes, last_modified: last_modified
    end
  end

  AddIssueLabelsQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: AddOrCreateLabelsToLabelableInput!) {
      addOrCreateLabelsToLabelable(input: $input) {
        errors {
          ...Api::Serializer::ValidationErrorFragment
        }
        labelableRecord {
          labels(first: 100) {
            nodes {
              ...Api::Serializer::IssuesDependency::LabelFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # Add Labels to an Issue
  post "/repositories/:repository_id/issues/:issue_number/labels" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#add-labels-to-an-issue"
    repo = current_repo
    issue  = repo.issues.find_by_number(int_id_param!(key: :issue_number))
    record_or_404(issue)

    control_access :add_label,
      repo: repo,
      resource: issue,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_repo_writable!(repo)
    ensure_issues_enabled_or_pr! repo, issue

    label_attributes = label_attributes_from_input(receive_with_schema("label", "add-legacy"))

    input_variables = {
      "input" => {
        "labelableId"      => issue.global_relay_id,
        "labels"           => label_attributes,
        "clientMutationId" => request_id,
      },
    }

    results = platform_execute(AddIssueLabelsQuery, variables: input_variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error! errors: results.errors, resource: "Label"
    elsif has_graphql_mutation_errors?(results)
      deliver_graphql_mutation_errors! results, input_variables: input_variables, resource: "Label"
    end

    deliver :graphql_label_hash, results.data.add_or_create_labels_to_labelable.labelable_record.labels.nodes
  end

  RemoveIssueLabelsMutation = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: RemoveLabelsFromLabelableInput!) {
      removeLabelsFromLabelable(input: $input)
      {
        errors {
          ...Api::Serializer::ValidationErrorFragment
        }
        labelable {
          labels(first: 100) {
            nodes {
              ...Api::Serializer::IssuesDependency::LabelFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # Remove a Label from an Issue
  delete "/repositories/:repository_id/issues/:issue_number/labels/*" do
    @route_owner = "@github/pe-issues-projects"

    # Introducing strict validation of the label.remove-one
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("label", "remove-one", skip_validation: true)

    @documentation_url = "/rest/reference/issues#remove-a-label-from-an-issue"
    repo = current_repo
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))
    record_or_404(issue)

    control_access :remove_label,
      repo: repo,
      resource: issue,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_repo_writable!(repo)
    ensure_issues_enabled_or_pr! repo, issue

    label_name = params[:splat].shift
    label = issue.labels.find_by_name(label_name)

    deliver_error!(404, message: "Label does not exist") if label.nil?

    input_variables = {
      input: {
        labelableId: issue.global_relay_id,
        labelIds: [label.global_relay_id],
        clientMutationId: request_id,
      },
    }

    results = platform_execute(RemoveIssueLabelsMutation, variables: input_variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error! errors: results.errors, resource: "Label"
    elsif has_graphql_mutation_errors?(results)
      deliver_graphql_mutation_errors! results, input_variables: input_variables, resource: "Label"
    end

    deliver :graphql_label_hash, results.data.remove_labels_from_labelable.labelable.labels.nodes
  end

  ReplaceIssueLabelsQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: ReplaceLabelsForLabelableInput!) {
      replaceLabelsForLabelable(input: $input) {
        errors {
          ...Api::Serializer::ValidationErrorFragment
        }
        labelableRecord {
          labels(first: 100) {
            nodes {
              ...Api::Serializer::IssuesDependency::LabelFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # Replace all Labels for an Issue
  put "/repositories/:repository_id/issues/:issue_number/labels" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#set-labels-for-an-issue"
    repo  = current_repo
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))
    record_or_404(issue)

    control_access :replace_all_labels,
      repo: repo,
      resource: issue,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_repo_writable!(repo)
    ensure_issues_enabled_or_pr! repo, issue

    label_attributes = label_attributes_from_input(receive_with_schema("label", "replace-legacy"))

    input_variables = {
      "input" => {
        "labelableId"      => issue.global_relay_id,
        "labels"           => label_attributes,
        "clientMutationId" => request_id,
      },
    }

    results = platform_execute(ReplaceIssueLabelsQuery, variables: input_variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error! errors: results.errors, resource: "Label"
    elsif has_graphql_mutation_errors?(results)
      deliver_graphql_mutation_errors! results, input_variables: input_variables, resource: "Label"
    end

    deliver :graphql_label_hash, results.data.replace_labels_for_labelable.labelable_record.labels.nodes
  end

  ClearIssueLabelsQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($issueId: ID!, $clientMutationId: String!) {
      clearLabelsFromLabelable(input: {
        labelableId: $issueId
        clientMutationId: $clientMutationId
      })
      {
        errors {
          ...Api::Serializer::ValidationErrorFragment
        }
      }
    }
  GRAPHQL

  # Remove all Labels from an Issue
  delete "/repositories/:repository_id/issues/:issue_number/labels" do
    @route_owner = "@github/pe-issues-projects"

    # Introducing strict validation of the label.delete-all-from-issue
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("label", "delete-all-from-issue", skip_validation: true)

    @documentation_url = "/rest/reference/issues#remove-all-labels-from-an-issue"
    repo  = current_repo
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))
    record_or_404(issue)

    control_access :remove_all_labels,
      repo: repo,
      resource: issue,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_repo_writable!(repo)
    ensure_issues_enabled_or_pr! repo, issue

    input_variables = {
      "issueId"          => issue.global_relay_id,
      "clientMutationId" => request_id,
    }

    results = platform_execute(ClearIssueLabelsQuery, variables: input_variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error! errors: results.errors, resource: "Label"
    elsif has_graphql_mutation_errors?(results)
      deliver_graphql_mutation_errors! results, input_variables: input_variables, resource: "Label"
    end

    deliver_empty(status: 204)
  end

  # Get Labels for every Issue in a Milestone
  get "/repositories/:repository_id/milestones/:milestone_number/labels" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-labels-for-issues-in-a-milestone"
    repo = current_repo
    milestone = repo.milestones.find_by_number(int_id_param!(key: :milestone_number))
    control_access :list_milestone_issue_labels, repo: repo, resource: milestone, allow_integrations: true, allow_user_via_integration: true

    deliver :label_hash, paginate_rel(milestone.labels), repo: repo
  end

private
  def label_id
    params[:splat].shift
  end
end
