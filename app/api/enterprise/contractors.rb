# frozen_string_literal: true

class Api::Enterprise::Contractors < Api::App
  areas_of_responsibility :enterprise_only, :api

  # List contractor attestations with user account.
  get "/enterprise/contractors" do
    deliver_error! 404 unless GitHub.restrict_contractors_from_default_access_to_internal_repos?

    @route_owner = "@github/iam"
    @documentation_url = Platform::NotDocumentedBecause::EXPERIMENTAL
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    limit = params.fetch(:limit, 50).to_i
    page = params.fetch(:page, 1).to_i

    ids = EnterpriseAttestation.contractor_ids(limit: limit, page: page)
    users = User.with_ids(ids).select(:id, :login)

    results = users.map do |user|
      contractor_user_hash(user, contractor: true)
    end

    deliver_raw(results)
  end

  # Add contractor attestion for given User account.
  put "/enterprise/contractors/:user_id" do
    deliver_error! 404 unless GitHub.restrict_contractors_from_default_access_to_internal_repos?

    @route_owner = "@github/iam"
    @documentation_url = Platform::NotDocumentedBecause::EXPERIMENTAL
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    user = find_user!

    EnterpriseAttestation.set(user.id, contractor: true)

    deliver_empty status: 204
  end

  # Remove contractor attestion for given User account.
  delete "/enterprise/contractors/:user_id" do
    deliver_error! 404 unless GitHub.restrict_contractors_from_default_access_to_internal_repos?

    @route_owner = "@github/iam"
    @documentation_url = Platform::NotDocumentedBecause::EXPERIMENTAL
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    user = find_user!

    EnterpriseAttestation.set(user.id, contractor: false)

    deliver_empty status: 204
  end

  # Get contractor attestation for given User account.
  get "/enterprise/contractors/:user_id" do
    deliver_error! 404 unless GitHub.restrict_contractors_from_default_access_to_internal_repos?

    @route_owner = "@github/iam"
    @documentation_url = Platform::NotDocumentedBecause::EXPERIMENTAL
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    user = find_user!

    contractor = EnterpriseAttestation.contractor?(user.id)
    result = contractor_user_hash(user, contractor: contractor)

    deliver_raw(result)
  end

  private

  def find_user!
    user =
      case params[:user_id]
      when /\A\d+\z/
        User.find_by(id: params[:user_id])
      else
        User.find_by(login: params[:user_id])
      end
    deliver_error!(404, {}) unless user.present?
    user
  end

  def contractor_user_hash(user, contractor:)
    {
      id: user.id,
      login: user.login,
      contractor: contractor,
    }
  end

end
