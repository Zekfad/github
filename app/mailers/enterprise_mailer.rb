# frozen_string_literal: true

class EnterpriseMailer < ApplicationMailer
  self.mailer_name = "mailers/enterprise"

  helper :application

  layout "layouts/primer_layout"

  # Invites a user into a private-mode GitHub Enterprise Server instance.
  def invite_user(user, password_reset_link, password_reset_expires)
    time = Time.parse(password_reset_expires)
    @user               = user
    @reset_link         = password_reset_link
    @hours_until_expiry = ((time - Time.now) / 1.hour).round
    @url                = GitHub.url

    premail(
      to: user_email(user),
      subject: "[GitHub] Welcome to your GitHub Enterprise Server instance",
    )
  end

  # Invites the first administrator to a GitHub Private Instance.
  def invite_ghpi_admin(user, password_reset_link, password_reset_expires)
    time = Time.parse(password_reset_expires)
    @user               = user
    @reset_link         = password_reset_link
    @hours_until_expiry = ((time - Time.now) / 1.hour).round
    @url                = GitHub.url

    premail(
      to: user_email(user),
      subject: "[GitHub] Welcome to your GitHub Private Instance",
    )
  end
end
