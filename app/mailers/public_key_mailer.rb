# frozen_string_literal: true

class PublicKeyMailer < ApplicationMailer
  self.mailer_name = "mailers/public_key"

  def weak_key(key)
    @owner = key.owner
    @key = key
    recipients = user_or_admin_recipients(@owner)
    subject =
      if @key.repository_key?
        "Weak deploy key deleted from #{@key.repository.nwo}"
      else
        "Weak SSH key deleted from your account"
      end

    mail(
      to: recipients[:to],
      bcc: recipients[:bcc],
      from: github,
      subject: subject,
    )
  end
end
