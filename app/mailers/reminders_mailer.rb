# frozen_string_literal: true

class RemindersMailer < ApplicationMailer
  self.mailer_name = "mailers/reminders"

  def waitlist_acceptance(membership)
    subject = "You're in! Get started with scheduled reminders 💖"

    settings = GitHub.newsies.settings(membership.actor)
    contact_email = user_email(membership.actor, settings.email(membership.member).address)

    @membership = membership
    @has_pull_reminders_installed = IntegrationInstallation.where(target: membership.member, integration: Apps::Internal.integration(:pull_panda)).exists?

    mail(
      from: github_noreply,
      to: contact_email,
      subject: subject,
    )
  end
end
