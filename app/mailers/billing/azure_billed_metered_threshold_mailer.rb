# frozen_string_literal: true

module Billing
  class AzureBilledMeteredThresholdMailer < ApplicationMailer
    self.mailer_name = "mailers/billing/azure_billed_metered_threshold"

    def approaching_limit(recipients:, threshold:, billable:, limit_in_subunits:)
      @billable_name = billable.name
      @threshold = threshold
      @limit_in_units = limit_in_subunits / 100

      mail(
        from: github,
        bcc: recipients.map(&:email),
        subject: "[GitHub] At #{threshold}% of paid usage services for #{billable.name}",
      )
    end

    def over_limit(recipients:, billable:)
      @billable_name = billable.name

      mail(
        from: github,
        bcc: recipients.map(&:email),
        subject: "[GitHub] At 100% of paid usage services for #{billable.name}",
      )
    end
  end
end
