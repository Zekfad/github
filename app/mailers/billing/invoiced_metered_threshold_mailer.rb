# frozen_string_literal: true

module Billing
  class InvoicedMeteredThresholdMailer < ApplicationMailer
    self.mailer_name = "mailers/billing/invoiced_metered_threshold"

    layout "layouts/primer_layout"

    def approaching_limit(recipients:, threshold:, billable:)
      @billable_name = billable.name
      @threshold = threshold
      @billing_page_url = billable.organization? ? "#{GitHub.url}/organizations/#{billable}/settings/billing" : "#{GitHub.url}/enterprises/#{billable}/settings/billing"

      @subject = "[GitHub] At #{threshold}% of paid usage services for #{billable.name}"
      @footer_text = "You are receiving this because you are an administrator or Billing Manager for the #{billable.name} account."

      if GitHub.flipper[:metered_billing_html_emails].enabled?(billable)
        premail(
          from: github,
          bcc: recipients.map(&:email),
          subject: @subject,
          template_name: "approaching_limit_v2"
        )
      else
        mail(
          from: github,
          bcc: recipients.map(&:email),
          subject: @subject
        )
      end
    end

    def over_limit(recipients:, threshold:, billable:)
      @billable_name = billable.name
      @threshold = threshold
      @billing_page_url = billable.organization? ? "#{GitHub.url}/organizations/#{billable}/settings/billing" : "#{GitHub.url}/enterprises/#{billable}/settings/billing"

      @subject = "[GitHub] At #{threshold}% of paid usage services for #{billable.name}"
      @footer_text = "You are receiving this because you are an administrator or Billing Manager for the #{billable.name} account."

      if GitHub.flipper[:metered_billing_html_emails].enabled?(billable)
        premail(
          from: github,
          bcc: recipients.map(&:email),
          subject: @subject,
          template_name: "over_limit_v2"
        )
      else
        mail(
          from: github,
          bcc: recipients.map(&:email),
          subject: @subject
        )
      end
    end
  end
end
