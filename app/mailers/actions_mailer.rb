# frozen_string_literal: true

class ActionsMailer < ApplicationMailer
  self.mailer_name = "mailers/actions"
  helper :bundle
  helper :mailer_bundle

  layout "layouts/primer_layout"

  def example_email(user)
    @user = user
    @subject = "[GitHub] Actions email example"
    @footer_text = "You are receiving this email because you are a really special person ♥️"

    premail(
      from: github_noreply,
      to: user_email(user),
      subject: @subject
    )
  end

  def workflow_inactivity(user, workflow)
    @workflow = workflow
    @repo = workflow.repository
    @user = user
    @subject = "[GitHub] The \"#{@workflow.name}\" workflow in #{@repo.nwo} will be disabled soon"
    @footer_text = "You are receiving this because you are the owner of the #{@repo.nwo} repository."
    @threshold = Actions::Workflow::REPOSITORY_INACTIVITY_THRESHOLD
    @url = "#{GitHub.url}/#{@repo.nwo}/actions?query=#{CGI::escape("workflow:\"#{@workflow.name}\"")}"

    premail(
      from: github_noreply,
      to: user_email(user),
      subject: @subject,
      workflow: @workflow,
      repo: @repo
    )
  end
end
