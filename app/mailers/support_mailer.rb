# frozen_string_literal: true

class SupportMailer < ApplicationMailer
  self.mailer_name = "mailers/support"

  def contact(subject, body, halp_metadata)
    unless GitHub.enterprise?
      headers["X-Halp-Metadata"] = halp_metadata.to_json
    end

    sender = halp_metadata[:email] || github
    sender = "\"#{halp_metadata[:name]}\" <#{sender}>" if halp_metadata[:name]

    bcc = if halp_metadata[:spam]
      if halp_metadata[:hit_honeypot]
        # Spam positive and honeypot positive
        ["logs+halpspampozhoneypoz@github.com"]
      else
        # Spam positive and honeypot negative
        ["logs+halpspampozhoneyneg@github.com"]
      end
    else
      if halp_metadata[:hit_honeypot]
        # Spam negative and honeypot positive
        ["logs+halpspamneghoneypoz@github.com"]
      else
        # Spam negative and honeypot negative
        ["logs+halpspamneghoneyneg@github.com"]
      end
    end
    bcc = [] if GitHub.enterprise?

    mail(
      from: sender,
      to: halp,
      bcc: bcc,
      subject: subject || "",
      body: body || "",
    )
  end

  def ada_init(user)
    @user = user

    mail(
      from: github,
      to: user_email(user),
      bcc: [bcc_log, GitHub.support_email],
      subject: "AdaInitiative and #{user.login} private repositories - action required",
    )
  end
end
