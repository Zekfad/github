# frozen_string_literal: true

class BillingNotificationsMailer < ApplicationMailer
  self.mailer_name = "mailers/billing_notifications"

  layout "layouts/primer_layout"

  include BillingSettingsHelper
  helper :billing_settings

  # Public: Email a user a payment receipt.
  #
  # user        - The User that was billed.
  # transaction - A Billing::BillingTransaction (or equivalent).
  #
  # Returns an ActionMailer Mail object.
  def receipt(user, billing_transaction)
    @user    = user
    @receipt = ::Billing::Receipt.new(billing_transaction)
    @extra   = @user.try(:billing_extra)

    attachments[@receipt.pdf_filename] = {
      mime_type: "application/pdf",
      content: @receipt.to_pdf,
    }

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Payment Receipt for #{@user.login}",
    )
  end

  # Public: Email to bcc_log about a payment receipt.
  #
  # user   - The User that was billed.
  # amount - Integer dollar amount billed.
  # date   - Time when this receipt occurred (optional, default: Time.now).
  #
  # Returns an ActionMailer Mail object.
  def receipt_bcc(user, billing_transaction)
    @user    = user
    @receipt = ::Billing::Receipt.new(billing_transaction)
    @extra   = @user.try(:billing_extra)

    mail(
      from: github_noreply,
      to: bcc_log,
      subject: "[GitHub] Payment Receipt (#{user.billing_email} - #{user.braintree_customer_id})",
      template_name: "receipt",
    )
  end

  def self.send_receipt_bcc?
    Helpers.bcc_log.any?
  end

  # Public: Email a receipt for a refund.
  #
  # user                   - The User/Organization that was billed.
  # purchased_on           - The date of purchase
  # refund_amount_in_cents - Integer amount in cents of the refund.
  # refunded_at            - The refund transaction timestamp, as Time
  #
  # Returns an ActionMailer Mail object.
  def refund(user, purchased_on, refund_amount_in_cents, instrument, refunded_at = nil)
    refunded_at = refunded_at || Time.now
    @user = user
    @sale_date = purchased_on.to_s(:long)
    @refunded_at = refunded_at.to_s(:long)
    @refund_amount = Billing::Money.new(refund_amount_in_cents).dollars
    @instrument = instrument

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Refund Receipt",
    )
  end

  def free_trial_will_end_soon(user, subscription_item_change)
    @user = user
    @subscription_item_change = subscription_item_change
    @marketplace_listing_plan_name = subscription_item_change.subscribable.name
    @marketplace_listing_name = subscription_item_change.subscribable.listing.name

    recipients = user_or_billing_recipients(user)

    mail(
      from:       github_noreply,
      to:         recipients[:to],
      bcc:        recipients[:bcc].concat(bcc_log),
      subject:    "[GitHub] Your free trial for #{@marketplace_listing_name} is ending soon",
      categories: "free-trial-ending-reminder",
    )
  end

  def coupon_will_expire_soon(user, redemption)
    @user = user
    @redemption = redemption
    @coupon = redemption.coupon
    @payment_amount = Billing::Money.new(user.undiscounted_payment_amount * 100).format(no_cents_if_whole: true)

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Coupon expiration reminder",
      categories: "coupon-expiration-reminder",
    )
  end

  # Public: Email a reminder about an expiring credit card.
  #
  # user - The User/Organization to remind.
  #
  # Returns an ActionMailer Mail object.
  def credit_card_will_expire_soon(user)
    @user = user
    @card_info = if @user.payment_method.present? && @user.payment_method.last_four.present?
      " (#{@user.payment_method.card_type} - #{@user.payment_method.formatted_number})"
    else
      ""
    end

    @on_file_text = if @user.organization?
      "for the @#{@user.login} organization"
    else
      "on your GitHub account"
    end

    subject = "[GitHub] Credit card expiring soon"
    subject += " for #{@user.safe_profile_name}" if @user.organization?

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: subject,
      categories: "card-expiration-reminder",
    )
  end

  # Public: Email accounting that a yearly plan has been upgraded/downgraded.
  #
  # user          - The User that changed their plan.
  # old_plan_name - The previous Plan they had.
  # new_plan_name - The new Plan they have moved to.
  # delta_cost    - The calculated delta cost we charged/refunded the user.
  #
  # Returns an ActionMailer Mail object.
  def yearly_plan_change(user, old_plan, new_plan, delta_cost)
    @billing_date  = user.billed_on.to_s
    @user          = user
    @new_plan      = new_plan
    @old_plan_name = old_plan.display_name.capitalize
    @new_plan_name = new_plan.display_name.capitalize
    @delta_cost    = delta_cost

    mail(
      from: github_noreply,
      to: "ar@github.com, #{GitHub.support_email}",
      bcc: bcc_log,
      subject: "#{user.login} changed their yearly plan",
    )
  end

  # Public: Email alerting a user that their annual plan will be billed soon
  #
  # user - The User that will be billed
  #
  # Returns an ActionMailer Mail object.
  def yearly_billing_notice(user)
    @billing_date = user.next_billing_date.to_s(:long)
    @user         = user

    recipients = user_or_billing_recipients(user)

    mail(
      from:    github_noreply,
      to:      recipients[:to],
      bcc:     recipients[:bcc],
      subject: "[GitHub] Annual Billing Alert for @#{@user.login}",
    )
  end

  def cc_expired_failure(user)
    @user = user
    @metered_billing_failure_text = metered_billing_failure_text(user)

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Your credit card has expired",
    )
  end

  def cc_failure(user, message)
    @user = user
    @message = message
    @metered_billing_failure_text = metered_billing_failure_text(user)

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] We had a problem billing your account",
    )
  end

  def paypal_failure(user, message)
    @user = user
    @message = message
    @metered_billing_failure_text = metered_billing_failure_text(user)

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] We had a problem billing your account",
    )
  end

  def no_payment_failure(user)
    @user = user
    @message = message
    @metered_billing_failure_text = metered_billing_failure_text(user)

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] We had a problem billing your account",
    )
  end

  def coupon_expired_failure(user, education_coupon: false)
    @user = user
    @education_coupon = user.coupon&.education_coupon? || education_coupon

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Your coupon has expired",
    )
  end

  def process_resource_usage_level(level, owner, product = nil)
    case level
    when Billing::UsageNotifications::LEVEL_ERROR
      error_resources_usage(owner, product)
    when Billing::UsageNotifications::LEVEL_WARN
      warn_resources_usage(owner, product)
    when Billing::UsageNotifications::LEVEL_INFO
      info_resources_usage(owner, product)
    end
  end

  def info_resources_usage(owner, product)
    @owner = owner
    @notifier = Billing::UsageNotifications.new(owner, product: product)

    @subject = "#{@notifier.info_resources_mail_subject} for the #{account_description(owner)}"
    @footer_text = "You are receiving this because you are an administrator or Billing Manager for an organization within the #{account_description(owner)}."

    recipients = build_metered_billing_recipients_list(owner)

    if GitHub.flipper[:metered_billing_html_emails].enabled?(owner)
      premail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc].concat(bcc_log),
        subject: "[GitHub] " + @subject,
        template_name: "info_resources_usage_v2"
      )
    else
      mail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc].concat(bcc_log),
        subject: "[GitHub] " + @subject,
        template_name: "info_resources_usage"
      )
    end
  end

  def warn_resources_usage(owner, product)
    @owner = owner
    @notifier = Billing::UsageNotifications.new(owner, product: product)

    @subject = "#{@notifier.warn_resources_mail_subject} for the #{account_description(owner)}"
    @footer_text = "You are receiving this because you are an administrator or Billing Manager for an organization within the #{account_description(owner)}."

    recipients = build_metered_billing_recipients_list(owner)

    if GitHub.flipper[:metered_billing_html_emails].enabled?(owner)
      premail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc].concat(bcc_log),
        subject: "[GitHub] " + @subject,
        template_name: "warn_resources_usage_v2"
      )
    else
      mail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc].concat(bcc_log),
        subject: "[GitHub] " + @subject,
        template_name: "warn_resources_usage"
      )
    end
  end

  def error_resources_usage(owner, product)
    @owner = owner
    @notifier = Billing::UsageNotifications.new(owner, product: product)

    @subject = "#{@notifier.error_resources_mail_subject} for the #{account_description(owner)}"
    @footer_text = "You are receiving this because you are an administrator or Billing Manager for an organization within the #{account_description(owner)}."

    recipients = build_metered_billing_recipients_list(owner)

    if GitHub.flipper[:metered_billing_html_emails].enabled?(owner)
      premail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc].concat(bcc_log),
        subject: "[GitHub] " + @subject,
        template_name: "error_resources_usage_v2"
      )
    else
      mail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc].concat(bcc_log),
        subject: "[GitHub] " + @subject,
        template_name: "error_resources_usage"
      )
    end
  end

  def marketplace_failure(user, message)
    @user = user
    @message = message

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] We had a problem billing your account",
    )
  end

  def never_entered_dunning_failure(user, message)
    @user = user
    @message = message

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] We had a problem billing your account",
    )
  end

  def over_billing_attempts_limit_failure(user, message)
    @user = user
    @message = message

    recipients = user_or_billing_recipients(user)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] We had a problem billing your account",
    )
  end

  private

  # Generates a recipient list for metered billing emails
  #
  # We want these sent to both billing managers and admins. Primarily because admins
  # have more context around Actions/Packages/Storage usage and billing managers may not.
  #
  # User accounts: just the user
  # Org owned accounts: all admins + billing managers
  # Enterprise accounts: all child org admins + enterprise billing managers
  #
  # Emails are sent on BCC to protect private email addresses from other Org users.
  def build_metered_billing_recipients_list(owner)
    if owner.is_a? Business
      billing_emails = billing_emails(owner)

      recipients = {to: [], bcc: []}
      owner.organizations.each do |org|
        recipients.merge!(build_admin_recipients_list(org, org.admins)) do |key, oldval, newval|
          newval + oldval
        end
      end

      recipients[:bcc] << billing_emails
      recipients[:bcc].compact!

      return recipients
    end

    if owner.user?
      return user_or_billing_recipients(owner)
    end

    if owner.organization?
      billing_emails = billing_emails(owner)
      recipients = build_admin_recipients_list(owner, owner.admins)

      recipients[:bcc] << billing_emails
      recipients[:bcc].compact!

      return recipients
    end
  end

  def metered_billing_failure_text(owner)
    if Billing::MeteredBillingPermission.new(owner).paid_overages_restricted_by_owner_payment_issue?
      "Actions and Packages usage is restricted to the included amounts for your plan."
    else
      "If our next billing attempt fails, Actions and Packages usage will be restricted to the included amounts for your plan."
    end
  end

  def account_description(owner)
    if owner.is_a?(Business)
      "#{owner.name} Enterprise account"
    else
      "#{owner.login} account"
    end
  end
  helper_method :account_description
end
