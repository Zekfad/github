# frozen_string_literal: true

class TradeControlsMailer < ApplicationMailer
  self.mailer_name = "mailers/trade_controls"

  helper :trade_controls, :text

  default from: -> { github_trade_appeals }
  default return_path: -> { github_trade_appeals }
  default subject: "GitHub and Trade Controls"

  def individual_actor_restricted(user)
    mail(to: user_email(user))
  end

  def individual_actor_reactivated(user)
    mail(to: user_email(user))
  end

  def organization_restricted(org)
    mail(to: (admin_emails(org) | billing_emails(org)))
  end

  def organization_reactivated(org)
    mail(to: (admin_emails(org) | billing_emails(org)))
  end

  def organization_scheduled_pending_enforcement(org)
    @organization = org
    @appeal_url = GitHub.org_pending_enforcement_appeals_url
    @privacy_url = GitHub.privacy_statement_url
    mail(to: (admin_emails(org) | billing_emails(org)), subject: "Urgent: Verify Your Information")
  end
end
