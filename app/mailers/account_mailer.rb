# frozen_string_literal: true

class AccountMailer < ApplicationMailer
  include ApplicationHelper

  # Necessary for the `settings_app_transfer_url` method
  include GitHub::RouteHelpers

  self.mailer_name = "mailers/account"

  helper :application # TODO: Remove once html_email_layout lives app/views/layouts
  helper :avatar
  helper :comments
  helper :email_link_tracking

  ACCOUNT_SECURITY_LAYOUT_ACTIONS = [
    :email_address_added,
    :email_address_removed,
    :oauth_authorization_notification,
    :configure_sms_fallback,
    :remove_sms_fallback,
    :two_factor_brute_force,
    :two_factor_recover,
    :unexpected_sign_in,
    :unexpected_incomplete_sign_in,
    :username_and_password_compromised,
    :weak_password_warning,
  ]

  TOKEN_SCANNING_SCOPE = {
    commit_scoped: "commit",
    repo_scoped: "repo",
    config_change_scoped: "config_change",
    config_deleted_scoped: "config_deleted",
    reconciliation_scoped: "path_reconciliation",
  }

  layout proc {
    case action_name.to_sym
    when :invited_to_org, :invited_to_org_by_email, :blocked_by_org, :attribution_invitation_notification
      "organization/organization_email_layout"
    when :invited_to_repo_by_email
      "repository/collab_email_layout"
    when *ACCOUNT_SECURITY_LAYOUT_ACTIONS
      "account/security"
    end
  }

  def new_password(user, email, link, hours_until_expiry, forced_reset)
    @link = link
    @hours_until_expiry = hours_until_expiry
    @forced_reset = forced_reset

    mail(
      to: user_email(user, email),
      subject: "[GitHub] Please reset your password",
    )
  end

  def organization_email_verification(email, requested_by:, redirect: nil)
    @requested_by = requested_by
    @organization = email.user
    @email        = email
    @cta_url      = organization_confirm_verification_email_url(@organization,
                                                                @email,
                                                                @email.verification_token,
                                                                redirect: redirect)
    @cta_tracking_url = ga_campaign_url(@cta_url,
                                        source: "verification-email",
                                        medium: "email",
                                        campaign: "github-email-verification",
                                        content: "html")

    premail(
      to: user_email(@organization, email.to_s),
      subject: "[GitHub] Please verify an email address for #{@organization}.",
    )

    # This header is used by our mailservers to ensure that verification mails
    # do not go through SendGrid. This makes it possible for the verification
    # mails to bypass SendGrid bounce suppressions.
    headers["X-GitHub-Verify"] = @organization.login
  end

  def download_everything(user, url)
    @user = user
    @url = url

    mail(
      from: github,
      to: user_email(@user),
      subject: "[GitHub] Your data export is ready to download",
    )
  end

  def delete_user(user, admins)
    @user = user.login

    recipients = build_admin_recipients_list(user, admins)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Account deletion",
    )
  end

  def delete_org(org, admins)
    @org_login = org.login
    @paid_account = org.plan.paid?
    @deleter = org.deleted_by

    recipients = build_admin_recipients_list(org, admins)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Organization deletion",
    )
  end

  def delete_private(user)
    @user         = user
    @deleter      = user.user? ? user : user.deleted_by
    @transactions = user.transactions.map do |transaction|
      old = transaction.old_plan.blank? ? "" : "#{transaction.old_plan.display_name.capitalize} -> "
      "#{transaction.timestamp} - #{transaction.action.camelize.upcase} #{old}#{transaction.current_plan.display_name.capitalize}"
    end
    @repos = user.repositories.map do |repo|
      "#{user}/#{repo}: #{repo.network_id}/#{repo.id}"
    end

    mail(
      to: bcc_log,
      subject: "[GitHub] Account deletion",
    )
  end

  # Sent when a public key is added to a user account.
  def public_key_added(key)
    @user = key.user
    @key = key

    mail(
      to: user_email(key.user),
      subject: "[GitHub] A new public key was added to your account",
    )
  end

  # Sent when a public key is updated.
  def public_key_updated(key)
    @user = key.user
    @key = key

    mail(
      to: user_email(key.user),
      subject: "[GitHub] A public key associated with your account was updated",
    )
  end

  # Sent when a user changes her password.
  def password_changed(user, reason)
    @user = user
    @raw_address = user.email

    to = @user.primary_user_email
    bcc = @user.emails.notifiable - [to]

    @explanation = case reason
    when "changed"
      "has changed"
    when "reset"
      "was reset"
    else
      raise ArgumentError, "Expected reason to be 'changed' or 'reset' but was #{reason}"
    end

    mail(
      to: user_email(@user, to.to_s),
      bcc: bcc.map { |email| user_email(@user, email.to_s) },
      subject: "[GitHub] Your password #{@explanation}",
      categories: "account-security,password-changed",
    )
  end

  def two_factor_stop(credential)
    @user = credential.user
    @number = credential.sms_number

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :two_factor_stop,
      subject: "[GitHub] SMS-based two-factor authentication disabled via STOP",
      categories: "account-security,two-factor-stop",
    )
  end

  def two_factor_enable(credential, reconfiguring)
    @user = credential.user
    @credential = credential
    @reconfiguring = reconfiguring

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :two_factor_enable,
      subject: "[GitHub] Please download your two-factor recovery codes",
      categories: "account-security,two-factor-enable",
    )
  end

  def two_factor_disable(user)
    @user = user

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :two_factor_disable,
      subject: "[GitHub] Two-factor authentication disabled",
      categories: "account-security,two-factor-disable",
    )
  end

  # An email sent to the user's password reset emails when a recovery code is
  # used to access a 2FA-enabled account
  #
  # user: the owner of the TwoFactorCredential
  def two_factor_recover(user, remaining_codes_count)
    @remaining_codes_count = remaining_codes_count
    @user = user
    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A recovery code was used to access your account",
      template_name: :two_factor_recover,
      categories: "account-security,two_factor_recover",
    )
  end

  def configure_sms_fallback(credential, new_number)
    @credential = credential
    @user = credential.user
    @new_number = new_number

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :configure_sms_fallback,
      subject: "[GitHub] A backup phone number has been added to your account",
      categories: "account-security,configure-sms-fallback",
    )
  end

  def remove_sms_fallback(credential, previous_number)
    @credential = credential
    @user = credential.user
    @previous_number = previous_number

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :remove_sms_fallback,
      subject: "[GitHub] A backup phone number has been removed from your account",
      categories: "account-security,remove-sms-fallback",
    )
  end

  def security_key_added(user)
    @user = user

    mail(
      from: github,
      to: user_email(@user),
      subject: "[GitHub] A security key was added to your account",
    )
  end

  def access_token_leaked(user, repository, url, in_public_repo)
    @user = user
    # Only include the URL if the repository is readable by the user
    # so private repository links are not leaked to users without access
    @url = url if repository.nil? || repository.readable_by?(user)
    @in_public_repo = in_public_repo

    mail(
      from: github,
      to: user_email(@user),
      subject: "[GitHub] OAuth access token found in commit",
    )
  end

  def third_party_token_leaked(repository, urls, token_type)
    recipients = user_or_admin_recipients(repository.owner)
    @urls = urls
    @token_type = token_type

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Third party token found in commit",
    )

    headers["References"]  = "<Token type: #{token_type}>"
  end

  def access_token_leaked_in_gist(user, url, in_public_repo)
    @user = user
    @url = url
    @in_public_repo = in_public_repo

    mail(
      from: github,
      to: user_email(@user),
      subject: "[GitHub] OAuth access token found in Gist",
    )
  end

  def ssh_private_key_leaked(user, url)
    @user = user
    @url = url

    mail(
      from: github,
      to: user_email(@user),
      subject: "[GitHub] SSH private key found in commit",
    )
  end

  def ssh_private_key_leaked_in_gist(user, url)
    @user = user
    @url = url

    mail(
      from: github,
      to: user_email(@user),
      subject: "[GitHub] SSH private key found in Gist",
    )
  end

  def token_scanning_summary(repo, results, users, scope)
    recipients = build_admin_recipients_list(repo.owner, users)
    @repo = repo
    @results = results
    @image_url = image_base_url
    @scope = scope

    subject = "[#{@repo.name_with_owner}] "
    case scope
    when TOKEN_SCANNING_SCOPE[:commit_scoped]
      subject += "Possible valid secrets found in commits"
    when TOKEN_SCANNING_SCOPE[:config_change_scoped]
      subject += "Possible secrets found after configuration change"
    when TOKEN_SCANNING_SCOPE[:config_deleted_scoped]
      subject += "Possible secrets found after configuration deletion"
    when TOKEN_SCANNING_SCOPE[:reconciliation_scoped]
      subject += "Possible secrets found in additional path locations"
    else
      subject += "Possible valid secrets found in repository"
    end

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: subject,
    )
  end

  def repository_transfer(xfer)
    @repo      = xfer.repository
    @requester = xfer.requester
    @target    = xfer.target
    @token     = xfer.token
    @private_repo_on_free_plan = @target.free_plan? && @repo.private?
    @private_collaborator_seats = @target.plan_limit(:collaborators, visibility: :private)

    recipients = user_or_admin_recipients(@target)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Repository transfer from @#{@requester.login} (#{@repo.nwo})",
    )
  end

  def immediate_repository_transfer(repository, requester, target, nwo_was)
    @repo      = repository
    @requester = requester
    @target    = target
    @nwo_was   = nwo_was

    recipients = user_or_admin_recipients(@target)

    mail(
        from: github,
        to: recipients[:to],
        bcc: recipients[:bcc],
        subject: "[GitHub] Repository transfer from @#{@requester.login} (#{@nwo_was})",
    )
  end

  def invited_to_org(invitation, invitation_token = nil)
    @invitation       = invitation
    @invitation_token = invitation_token
    @user             = invitation.invitee
    @inviter          = invitation.inviter
    @org              = invitation.organization
    @login_to_block   = invitation.show_inviter? ? @inviter.login : @org.login

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the noreply address if
    # not.
    reply_to = if invitation.show_inviter?
      user_email(invitation.inviter, allow_private: false) || github_noreply(invitation.inviter)
    else
      github_noreply
    end

    subject = if invitation.show_inviter?
      "[GitHub] @#{@inviter.login} has invited you to join the @#{@org.login} organization"
    else
      "[GitHub] You’re invited to join the @#{@org.login} organization"
    end

    premail(
      from: github_noreply,
      reply_to: reply_to,
      to: user_email(@user),
      subject: subject,
    )
  end

  def invited_to_org_by_email(invitation, invitation_token = nil)
    invitation.reset_token
    @invitation       = invitation
    @email            = invitation.email
    @inviter          = invitation.inviter
    @org              = invitation.organization
    @invitation_token = invitation.token
    @login_to_block   = invitation.show_inviter? ? @inviter.login : @org.login

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the noreply address if
    # not.
    reply_to = if invitation.show_inviter?
      user_email(invitation.inviter, allow_private: false) || github_noreply(invitation.inviter)
    else
      github_noreply
    end

    subject = if invitation.show_inviter?
      "[GitHub] @#{@inviter.login} has invited you to join the @#{@org.login} organization"
    else
      "[GitHub] You're invited to join the @#{@org.login} organization"
    end

    premail(
      from: github_noreply,
      reply_to: reply_to,
      to: @email,
      subject: subject,
    )
  end

  def org_invitation_canceled(invitation)
    @invitation = invitation
    @email = @invitation.email? ? @invitation.email : user_email(@invitation.invitee)
    @org = @invitation.organization

    mail(
      from: github,
      to: @email,
      subject: "[GitHub] Your invitation to #{@org.login} has been canceled",
    )
  end

  def org_invitation_failed(user, org, failures)
    @user = user
    @org = org
    begin
      @failures = JSON.parse(failures)
    rescue JSON::ParserError # Raise a clean error so we don't leak PII
      raise JSON::ParserError.new
    end
    @email_source = "org_invitation_failed"

    if @failures.length > 1
      subject = "We weren’t able to send some of your invitations"
    else
      subject = "One of your invitations didn't reach its recipient"
    end

    premail(
      from: github_noreply,
      to: user_email(user),
      subject: subject,
    )
  end

  def blocked_by_org(org, blocked_user, content, duration_string)
    @login = blocked_user.safe_profile_name
    @org = org
    @content_url = GitHub.url + content.async_path_uri.sync.to_s
    @coc_url = content.repository.code_of_conduct.url ? content.repository.code_of_conduct.url : nil
    subject = "[GitHub] You have been blocked from #{@org.login}"

    if duration_string
      @duration = duration_string
      subject += @duration
    end

    premail(
      from: github,
      reply_to: github,
      to: user_email(blocked_user),
      subject: subject,
    )
  end

  def application_transfer_request(xfer)
    @application = xfer.application
    @requester   = xfer.requester
    @target      = xfer.target
    @xfer        = xfer

    mail(
      from: github,
      bcc: admin_emails(@target),
      subject: "[GitHub] OAuth Application transfer from @#{@requester.login} (#{@application.name})",
    )
  end

  def integration_transfer_request(xfer)
    @integration = xfer.integration
    @requester   = xfer.requester
    @target      = xfer.target
    @xfer        = xfer

    mail(
      from: github,
      bcc: admin_emails(@target),
      subject: "[GitHub] Integration transfer from @#{@requester.login} (#{@integration.name})",
    )
  end

  def legacy_reply_token_bounce(email)
    mail(
      from: github,
      to: email.from_address,
      subject: "GitHub was unable to receive your email",
    )
  end

  # An email informing the user that their reply to comments from email
  # failed to be posted on GitHub.
  #
  # email  - An EmailReplyJob instance
  # reason - A symbol from EmailJob::NOTIFIABLE_ERRORS list.
  #
  def email_reply_unsuccessful(email, reason)
    @sender = email.sender
    @target = email.target
    @reason = reason

    subject = if email.subject =~ /\ARe:/i
      email.subject
    else
      "Re: #{email.subject}"
    end

    mail(
      from: github,
      to: email.from_address,
      subject: subject,
    )

    headers["In-Reply-To"] = email.message_id
    headers["References"]  = email.message_id
  end

  # An email sent to the user when they verify their first email address after signing up.
  # - Sent to users with a transactional email preference.
  # - Users with a marketing email preference receive the welcome series via MailChimp instead.
  #
  # email - a UserEmail
  #
  def welcome(email)
    @user = email.user

    mail(
      from: github_noreply,
      to: user_email(@user, email.to_s),
      subject: "[GitHub] Welcome to GitHub, @#{@user.login}!",
      categories: "welcome",
    )
  end

  # An email sent to the user when they add an email address to their account.
  # Avoids sending an email to the newly added address since an account without
  # any confirmed emails would include the newly created address in
  # #account_related_emails
  #
  # email - a UserEmail
  def email_address_added(email)
    @user = email.user
    @email = email

    # Remove the primary address (to) and the recently added address
    # since it's receiving a verification email already.
    bcc = @user.account_related_emails.map(&:to_s) - [email.to_s, @user.email]

    mail(
      to: user_email(@user),
      bcc: bcc.map { |address| user_email(@user, address.to_s) },
      subject: "[GitHub] An email address was added to your account.",
      categories: "account-security,email_address_added",
    )
  end

  # An email sent to the user when an email addressed is removed from an
  # account. We deliver to the address that was just removed and we BCC all
  # account-related emails.
  #
  # user - a User record
  # email - a String
  def email_address_removed(user, email)
    @user = user
    @email = email

    mail(
      # Deliver to recently removed address
      to: user_email(@user, @email),
      # BCC all other account-related emails
      bcc: @user.account_related_emails.map { |address| user_email(@user, address.to_s) },
      subject: "[GitHub] An email address was removed from your account.",
      categories: "account-security,email_address_removed",
     )
  end

  def two_factor_brute_force(user)
    @user = user

    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] Having trouble signing in with two-factor authentication?",
      template_name: :two_factor_brute_force,
      categories: "account-security,two-factor-brute-force",
    )
  end

  def api_basic_auth_deprecation(user, url, user_agent: nil)
    @user = user
    @url = url
    @user_agent = user_agent
    @time = Time.zone.now.to_s(:deprecation_mailer)

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :api_basic_auth_deprecation,
      subject: "[GitHub] Deprecation Notice",
     )
  end

  def api_basic_auth_app_upgrade(user, app_name, app_current_version, app_patched_version, current_sessions_affected)
    @user = user
    @app_name = app_name
    @app_current_version = app_current_version
    @app_patched_version = app_patched_version
    @current_sessions_affected = current_sessions_affected

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :api_basic_auth_app_upgrade,
      subject: "[GitHub] Action required: Update #{@app_name} to version #{@app_patched_version} to ensure continued use with github.com"
     )
  end

  def authorizations_grants_api_basic_auth_deprecation(route:, user:, user_agent:, blog_endpoint:)
    @route = route
    @user = user
    @user_agent = user_agent
    @blog_endpoint = blog_endpoint
    @time = Time.zone.now.to_s(:deprecation_mailer)

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :authorizations_grants_api_basic_auth_deprecation,
      subject: "[GitHub] Deprecation Notice",
    )
  end

  def app_owner_authorizations_grants_api_basic_auth_deprecation(route:, oauth_application:, user_agent:, blog_endpoint:)
    @route = route
    @oauth_application = oauth_application
    @user_agent = user_agent
    @blog_endpoint = blog_endpoint
    @user = oauth_application.owner

    recipients = user_or_admin_recipients(@user)
    return unless recipients[:bcc].any?

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Deprecation Notice",
      template_name: :app_owner_authorizations_grants_api_basic_auth_deprecation,
    )
  end

  def api_oauth_access_via_query_params_deprecation(oauth_access, time:, url:, user_agent:)
    @time       = time.to_s(:deprecation_mailer)
    @url        = url
    @user_agent = user_agent

    @template_name = if oauth_access.personal_access_token?
      @oauth_access = oauth_access
      @user = oauth_access.user

      :pat_api_oauth_access_via_query_params_deprecation
    else
      @application = oauth_access.application
      @user = @application.owner

      :app_api_oauth_access_via_query_params_deprecation
    end

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: @template_name,
      subject: "[GitHub API] Deprecation notice for authentication via URL query parameters",
    )
  end

  def api_oauth_credentials_via_params_deprecation(application, time:, url:, user_agent:)
    @application = application
    @user = application.owner

    @time       = time.to_s(:deprecation_mailer)
    @url        = url
    @user_agent = user_agent

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :api_oauth_credentials_via_params_deprecation,
      subject: "[GitHub API] Deprecation notice for authentication via URL query parameters",
    )
  end

  def api_applications_endpoints_deprecation(application, time:)
    @application = application
    @user = application.owner

    @time = time.to_s(:deprecation_mailer)

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :api_applications_endpoints_deprecation,
      subject: "[GitHub API] Deprecation notice for OAuth Application API",
    )
  end

  def git_password_auth_deprecation(user_id:, repository_id:, user_agent:, resource_type:)
    @user = User.find_by_id(user_id)
    repo = case resource_type
    when "repository", "wiki"
      Repository.find_by_id(repository_id)
    when "gist"
      Gist.find_by_id(repository_id)
    else
      raise ArgumentError, "Unrecognized resource type for git deprecation mailer: #{resource_type}"
    end

    @nwo = repo.name_with_owner
    @user_agent = user_agent
    @resource_type = resource_type

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :git_password_auth_deprecation,
      subject: "[GitHub] Deprecation Notice",
    )
  end

  def legacy_integration_event_deprecation(user, app_names)
    @user = user
    @app_names = app_names

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :legacy_integration_event_deprecation,
      subject: "[GitHub] Deprecation notice for GitHub Apps webhook events",
    )
  end

  def api_integrations_access_tokens_deprecation(application, time:)
    @application = application
    @user = application.owner

    @time = time.to_s(:deprecation_mailer)

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :api_integrations_access_tokens_deprecation,
      subject: "[GitHub API] Deprecation notice for GitHub Apps installation token creation API",
    )
  end

  private def unexpected_sign_in_explanation(reason)
    case reason
    when "unrecognized_location", "unrecognized_device_and_location"
      "location of the sign in"
    when "unrecognized_device"
      raise NotImplementedError, "Notifications for unknown devices are disabled"
    else
      raise ArgumentError, "Unknown unexpected login notification reason"
    end
  end

  # Notify users of logins from unexpected locations.
  #
  # authentication_record: the record that was created after a correct
  #  password was supplied.
  def unexpected_sign_in(authentication_record)
    @user = authentication_record.user
    @authentication_record = authentication_record
    @reason = unexpected_sign_in_explanation(authentication_record.flagged_reason)

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :unexpected_sign_in,
      subject: "[GitHub] Please review this sign in",
    )
  end

  def weak_password_warning(user, client, user_agent: nil, deadline: nil)
    @user = user
    @user_agent = user_agent
    @deadline = deadline
    @client = case client
    when "git"
      "Git"
    when "api"
      "the API"
    when "gist"
      "Gist"
    when "wiki"
      "GitHub Wikis"
    when "web"
      "Github.com"
    else
      raise ArgumentError.new("unknown client")
    end

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :weak_password_warning,
      subject: "[GitHub] Please change your password",
     )
  end

  # Notify users of a compromise containing their username and password
  #
  # user: The compromised user
  def username_and_password_compromised(user)
    @user = user
    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :username_and_password_compromised,
      subject: "[GitHub] Please change your password",
    )
  end

  # Notify users of partial two factor sign ins unexpected locations. Use the
  # email addresses at the time of sign in to prevent an attacker from changing
  # the email addresses on the account before the notification is delivered.
  #
  # authentication_record: the record that was created after a correct
  #   password was supplied.
  # emails: an array of email address strings that represent the notifiable
  #   addresses at the time of sign in.
  def unexpected_incomplete_sign_in(authentication_record, primary_email, bcc_emails)
    @user = authentication_record.user
    @reason = unexpected_sign_in_explanation(authentication_record.flagged_reason)
    @authentication_record = authentication_record

    mail(
      to: user_email(@user, primary_email),
      bcc: bcc_emails.map { |address| user_email(@user, address.to_s) },
      subject: "[GitHub] Please review this attempt to sign in",
     )
  end

  # When verified device enforcement is enabled, we deliver this email when a
  # sign in from an unverified device occurs. The code in this email is used
  # to complete the sign in
  #
  # device_name: the auto-generated device name based on the user agent
  # verification_code: the code that is used to complete sign in
  def verified_device_verification(user, device_name, verification_code)
    @user = user
    @device_name = device_name
    @verification_code = verification_code
    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :verified_device_verification,
      subject: "[GitHub] Please verify your device",
    )
  end

  # An email sent to the user's admin emails when a third party application
  # is granted access to non-private scopes
  #
  # authorization - the newly created/update OAuthAuthorization record
  # added_scopes - newly authorized, non-public scopes
  # user - the user that created the OAuthAuthorization
  def oauth_authorization_notification(authorization:, added_scopes:, previous_scopes:, is_new_record:, is_regenerated:)
    @authorization = authorization
    @user = @authorization.user
    @previous_scopes = previous_scopes
    @added_scopes = added_scopes

    if authorization.personal_access_authorization?
      if is_new_record
        new_personal_access_token
      elsif is_regenerated
        regenerated_personal_access_token
      else
        updated_personal_access_token
      end
    else
      @application_owner = if @authorization.application.owned_or_operated_by_github?
        "first-party GitHub"
      else
        "third-party"
      end
      if is_new_record
        new_oauth_authorization
      else
        updated_oauth_authorization
      end
    end
  end

  def blocked_from_org_notification(user, org, comment)
    premail(
      to: user_email(user),
      subject: "[GitHub] An organization has blocked you",
    )
  end

  def attribution_invitation_notification(invitation)
    @source = invitation.source
    @target = invitation.target
    @inviter = invitation.creator
    @org = invitation.owner

    mail(
      to: user_email(@target),
      subject: "[GitHub] You have been invited to claim contributions in #{@org.login}",
     )
  end

  private def new_oauth_authorization
    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A #{@application_owner} OAuth application has been added to your account",
      template_name: :new_oauth_authorization,
      categories: "account-security,new-oauth-authorization",
    )
  end

  private def updated_oauth_authorization
    @previous_scopes_message = previous_scopes_message
    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A previously authorized #{@application_owner} OAuth application has been granted additional scopes",
      template_name: :updated_oauth_authorization,
      categories: "account-security,updated-oauth-authorization",
    )
  end

  private def new_personal_access_token
    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A personal access token has been added to your account",
      template_name: :new_personal_access_token,
      categories: "account-security,new-personal-access-token",
    )
  end

  private def regenerated_personal_access_token
    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A personal access token has been regenerated for your account",
      template_name: :regenerated_personal_access_token,
      categories: "account-security,new-personal-access-token",
    )
  end

  private def updated_personal_access_token
    @previous_scopes_message = previous_scopes_message

    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A personal access token has been granted additional scopes",
      template_name: :updated_personal_access_token,
      categories: "account-security,updated-personal-access-token",
    )
  end

  private def previous_scopes_message
    if @previous_scopes.any?
      "had #{@previous_scopes.to_sentence} #{pluralize_without_number(@previous_scopes.count, "scope")}"
    else
      "did not have any scopes"
    end
  end
end
