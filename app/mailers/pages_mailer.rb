# frozen_string_literal: true

class PagesMailer < ApplicationMailer
  self.mailer_name = "mailers/pages"

  def build_failure(pusher, repository, error, build = nil)
    return if staff_build?(pusher)
    @error = error
    @build = build
    @repository = repository
    mail(
      from: github,
      to: recipient_email(pusher, repository),
      subject: "[#{repository.name_with_owner}] Page build failure",
      message_id: "<#{repository.name_with_owner}/page-#{message_id_suffix build}@#{GitHub.host_name}>",
    )
  end

  def build_warning(pusher, repository, warning, build = nil)
    return if staff_build?(pusher)
    @warning = warning
    @build = build
    @repository = repository
    mail(
      from: github,
      to: recipient_email(pusher, repository),
      subject: "[#{repository.name_with_owner}] Page build warning",
      message_id: "<#{repository.name_with_owner}/page_warning-#{message_id_suffix build}@#{GitHub.host_name}>",
    )
  end

  private

  def message_id_suffix(build)
    if build
      build.id
    else
      "0-#{Time.now.to_i}"
    end
  end

  def recipient_email(pusher, repository)
    user_email(pusher, GitHub.newsies.email(pusher, repository.owner).value)
  end

  # If the build is a staff-initiated build (e.g., via stafftools),
  # Supress build warnings and errors, rather than sending them to Halp
  def staff_build?(pusher)
    GitHub.guard_audit_log_staff_actor? && (User.staff_user == pusher)
  end
end
