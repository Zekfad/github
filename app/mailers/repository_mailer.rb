# frozen_string_literal: true

class RepositoryMailer < ApplicationMailer
  self.mailer_name = "mailers/repository"

  helper :avatar
  helper :octicons_caching
  layout "repository/collab_email_layout", only: [:collab_invited]

  def private_repository_deleted(repository)
    @repository_name = repository.name_with_owner

    mail(
      to: user_email(repository.owner),
      subject: "#{repository.name_with_owner} deleted",
    )
  end

  def private_fork_deleted(repo_nwo:, parent_nwo:, repo_owner:)
    @repository_name = repo_nwo
    @parent_repo_name = parent_nwo

    mail(
      to: user_email(repo_owner),
      subject: "fork of #{@parent_repo_name} has been deleted",
    )
  end

  def collab_invited(invitation)
    invitation.reset_token
    @invitation = invitation
    @inviter = invitation.inviter
    @invitee = invitation.invitee
    @email = invitation.email? ? invitation.email : @invitee.email
    @repository = invitation.repository
    @signature = notification_signature(@repository.permalink)

    email = GitHub.newsies.settings(@invitee).email(@repository.organization || :global).address if @invitee.present?

    premail(
      from: github_noreply(@inviter),
      to: @email,
      subject: "#{@inviter} invited you to #{@repository.name_with_owner}",
    )
  end

  # Sent to all org admins when a deploy key is added to a repository.
  def deploy_key_added(key)
    @key = key
    recipients = user_or_admin_recipients(key.repository.owner)

    mail(
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] A new public key was added to #{key.repository.name_with_owner}",
    )
  end

  # Sent to all org admins when a deploy key is updated.
  def deploy_key_updated(key)
    @key = key
    recipients = user_or_admin_recipients(key.repository.owner)

    mail(
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] A public key associated with #{key.repository.name_with_owner} was updated",
      template_name: "deploy_key_added",
    )
  end

  def dmca_takedown_notice(repository, url)
    @repository = repository
    @takedown_url = url
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] DMCA takedown notice for #{repository.name_with_owner}",
    )
  end

  def country_block_notice(repository, block, url)
    @repository = repository
    @country_block_description = GitRepositoryBlock::COUNTRY_BLOCK_DESCRIPTIONS[block]
    @country_block_url = url
    @countries = GitRepositoryBlock::COUNTRY_BLOCK_TYPES[block]
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Repository blocked in certain countries",
    )
  end

  def admin_disabled_notice(repository, instructions = nil)
    @repository = repository
    @instructions = instructions
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Repository access disabled for #{repository.name_with_owner}",
    )
  end

  def size_disabled_notice(repository, instructions = nil)
    @repository = repository
    @instructions = instructions
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Repository access disabled for #{repository.name_with_owner}",
    )
  end

  def tos_disabled_notice(repository, instructions = nil)
    @repository = repository
    @instructions = instructions
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Repository access disabled for #{repository.name_with_owner}",
    )
  end

  def sensitive_data_disabled_notice(repository, instructions = nil)
    @repository = repository
    @instructions = instructions
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Repository access disabled for #{repository.name_with_owner}",
    )
  end

  def trademark_disabled_notice(repository, instructions = nil)
    @repository = repository
    @instructions = instructions
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] Repository access disabled for #{repository.name_with_owner}",
    )
  end

  def ssh_deploy_private_key_leaked(repository, url)
    @repository = repository
    @url = url
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] SSH private deploy key found in commit",
    )
  end

  def ssh_deploy_private_key_leaked_in_gist(repository, url)
    @repository = repository
    @url = url
    recipients = user_or_admin_recipients(repository.owner)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "[GitHub] SSH private deploy key found in Gist",
    )
  end
end
