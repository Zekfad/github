# frozen_string_literal: true

class DelegatedAccountRecoveryMailer < ApplicationMailer
  include GitHub::RecurringMailer
  ACCOUNT_RECOVERY_HELP_URL = "/articles/adding-a-fallback-authentication-method-with-recover-accounts-elsewhere"

  self.mailer_name = "mailers/delegated_account_recovery"

  def recovery_token_used(user, provider_origin)
    @user = user
    @provider_origin = provider_origin

    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A recovery token from your account has been used",
      template_name: :recovery_token_used,
    )
  end

  def recovery_token_stored(user, provider_origin)
    @user = user
    @provider_origin = provider_origin

    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] A recovery token has been added to your account",
      template_name: :recovery_token_stored,
    )
  end

  def recovery_token_failed_attempt(user, provider_origin)
    @user = user
    @provider_origin = provider_origin

    mail_to_primary_bcc_remaining_account_related_emails(
      subject: "[GitHub] An attempt to use a recovery token for your account was unsuccessful",
      template_name: :recovery_token_failed_attempt,
    )
  end
end
