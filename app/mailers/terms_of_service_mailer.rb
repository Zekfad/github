# frozen_string_literal: true

class TermsOfServiceMailer < ApplicationMailer
  self.mailer_name = "mailers/terms_of_service"

  # This mailer is intended for sending out periodic one-off notices
  # to our users regarding updates to our terms of service.
  def updated_terms_notification(user)
    mail(
      from: github_noreply,
      to: user_email(user),
      subject: "Updates to GitHub Terms of Service",
      categories: "terms-of-service-update",
    )
  end
end
