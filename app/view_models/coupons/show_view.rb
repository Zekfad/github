# frozen_string_literal: true

module Coupons
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

    attr_reader :coupon, :selected_account

    def discount_amount
      interval = coupon.discount > 1 ? " per month" : ""
      text = "#{coupon.human_discount}#{interval} off"
      text
    end

    def discount_duration
      coupon.human_duration
    end

    # Returns the most sensible user login for a given coupon.
    #
    # user   - User that is redeeming the coupon
    # coupon - Da Coupon
    #
    # Returns a user id as a String
    def default_user_login(user, coupon)
      coupon.user_only?? user.login : ""
    end

    # Returns the most sensible plan for a given coupon.
    #
    # user   - User that is redeeming the coupon
    # coupon - Da Coupon
    #
    # Returns the plan name String
    def default_plan(user, coupon)
      coupon.plan? ? coupon.plan.name : GitHub.default_plan_name
    end

    def redeem_button_disabled
      "disabled" if !coupon.user_only? || !coupon.plan?
    end

    def account_selected(account)
      "selected" if selected_account == account
    end
  end
end
