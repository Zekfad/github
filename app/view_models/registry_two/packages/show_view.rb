# frozen_string_literal: true

module RegistryTwo
  module Packages
    class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :owner, :package, :package_versions, :package_version, :package_download_counts

      DOWNLOAD_POINTS_OFFSET = 200 / 29

      def download_points
        return @download_points if @download_points

        max_val = package_download_counts.day_counts.max
        min_val = package_download_counts.day_counts.min
        divisor = max_val == min_val ? 1.0 : max_val - min_val
        @download_points = package_download_counts.day_counts.reverse.each_with_index.map { |value, ix|
          "#{ix * DOWNLOAD_POINTS_OFFSET},#{2+(22.0*(value - min_val)/(divisor))}"
        }.join(" ")
      end
    end
  end
end
