# frozen_string_literal: true

module RepositoryCodeScanning
  class AnnotationView < ResultView
    delegate :result, :related_locations, :has_code_paths, to: :response
    attr_reader :pull

    def head_ref
      return @head_ref if defined? @head_ref
      return if pull.nil?
      # NOTE: This code assumes that we never analyze a merge commit, but always refer to the head commit of a PR.
      # This is enforced by the upload action https://github.com/github/codeql-action/blob/ec154779ac71b6def748d0faa75ef9eb6d4893b3/src/util.ts#L203
      # but might not be true in other cases.
      @head_ref = pull.merge_ref.sub("merge", "head")
    end
  end
end
