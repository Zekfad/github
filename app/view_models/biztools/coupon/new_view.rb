# frozen_string_literal: true

module Biztools
  module Coupon
    class NewView
      include ActionView::Helpers::TagHelper
      include ActionView::Helpers::FormTagHelper

      def all_plans
        plans  = GitHub::Plan.non_free_user_plans
        plans += GitHub::Plan.all_non_free_org_plans[0..4]
        plans.map do |plan|
          ["#{plan.name.capitalize} (#{plan.repos})", plan.name]
        end
      end
    end
  end
end
