# frozen_string_literal: true

module Biztools
  module Coupon
    class ShowView < View

      attr_reader :state

      delegate :redeemed_count, to: :coupon

      def initialize(coupon, state: "active")
        @coupon = coupon
        @state = state
      end

      def active_state?
        state == "active"
      end

      def expired_state?
        state == "expired"
      end

      def human_details
        super coupon
      end

      def expires(user)
        user.coupon_redemption.expires_at.to_date
      end

      def coupon_note
        helpers.auto_link(
          coupon.note,
          mode=:urls,
          link_attr=nil,
          skip_tags=nil,
        )
      end

      private

      attr_reader :coupon
    end
  end
end
