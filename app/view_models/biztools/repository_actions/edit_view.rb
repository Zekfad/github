# frozen_string_literal: true

module Biztools
  module RepositoryActions
    class EditView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include PlatformHelper

      attr_reader :data, :action

      Query = parse_query <<~'GRAPHQL'
        fragment Root on Query {
          repositoryAction(id: $id) {
            id
            name
            isListed
            regularCategories {
              name
              isFilter
            }
            filterCategories {
              name
              isFilter
            }
          }
          marketplaceCategories {
            name
            isFilter
          }
        }
      GRAPHQL

      def initialize(data:, current_user: nil, user_session: nil)
        @data = Query::Root.new(data)
        @action = @data.repository_action
      end

      def regular_categories
        @regular_categories ||= data.marketplace_categories.reject(&:is_filter).map(&:name)
      end

      def filter_categories
        @filter_categories ||= @data.marketplace_categories.select(&:is_filter).map(&:name)
      end

      def selected_regular_categories
        @selected_regular_categories ||= @action.regular_categories.map(&:name)
      end

      def selected_filter_categories
        @selected_filter_categories ||= @action.filter_categories.map(&:name)
      end

      def action_primary_category
        @action_primary_category ||= selected_regular_categories.first
      end

      def action_secondary_category
        @action_secondary_category ||= selected_regular_categories.second
      end

      def action_id
        action.id
      end

      def action_name
        action.name
      end
    end
  end
end
