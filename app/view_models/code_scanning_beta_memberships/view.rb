# frozen_string_literal: true

module CodeScanningBetaMemberships
  class View < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :membership, :adminable_accounts, :selected_account, :survey

    def selected?(member)
      selected_account == member
    end

    def already_enabled?
      selected_account.advanced_security_public_beta_enabled?
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def membership_exists?
      @membership_exists ||= EarlyAccessMembership.code_scanning_waitlist.exists?(member: selected_account)
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def member_type(member)
      member&.is_a?(Business) ? "Business" : "User"
    end

    def target_member_description
      return unless membership&.member

      if membership.member.is_a?(Business)
        "your business #{membership.member.name}"
      elsif membership.member.organization?
        "your organization @#{membership.member.login}"
      else
        "your personal account"
      end
    end
  end
end
