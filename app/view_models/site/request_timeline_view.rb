# frozen_string_literal: true

class Site::RequestTimelineView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  FAMILIES = [
    [:memcached, /\.memcached\z/, "#18987D", "#8CCCBE"],
    [:sql, /\.active_record\z/, "#468EE5", "#A3C7F2"],
    [:redis, /\.redis\z/, "#FF595E", "#FFACAE"],
    [:gitrpc, /\.gitrpc\z/, "#6A4A95", "#B5A5CA"],
    [:html_pipeline, /\.html_pipeline\z/, "#D59D05", "#EACE82"],
    [:views, /\.action_view\z/, "#8B572A", "#C5AB94"],

    # This must be last.
    [:other, //, "#555", "#CCC"],
  ].freeze

  # A single row in the timeline
  class Row
    BRIEF_LIMIT_MS = 1

    # ActiveSupport::Notification::Event for this row
    attr_reader :event
    # String representing the line of code that generated the event
    attr_reader :location
    # Unique Integer ID for this row
    attr_reader :id
    # Integer ID of this row's parent row, or nil if no parent
    attr_accessor :parent_id

    # Array of Strings (possibly syntax-highlighted) representing the event's
    # payload.
    attr_accessor :payload_lines

    def initialize(event, location, id)
      @event = event
      @location = location
      @id = id
    end

    # The URL for the line of code that generated this row's event.
    #
    # Returns a URL String.
    def location_url
      match = location.match(%r{\A#{Rails.root}/([^:]+):(\d+)})
      path, lineno = match.captures
      "https://github.com/github/github/blob/#{GitHub.current_sha}/#{path}#L#{lineno}"
    end

    # The method and line of code that generated this event, cleaned for
    # display purposes.
    #
    # Returns a String.
    def cleaned_location
      location
        .sub(%r{\A#{Rails.root}/}, "")
        .sub(/_erb__[_\d]+'\z/, "_erb'")
    end

    def event_family
      FAMILIES.find { |_, pattern, _| event.name =~ pattern }
    end

    # A String containing more information about the event.
    def event_detail
      case event.name
      when "start_processing.action_controller"
        "#{event.payload[:controller]}##{event.payload[:action]}"
      when "sql.active_record"
        event.payload[:name]
      when "render_template.action_view", "render_partial.action_view"
        event.payload[:identifier].sub(%r{\A#{Rails.root}/}, "")
      when /(?<!get_multi)\.(cache|memcached)\z/
        event.payload[:args].first
      when "remote_call.gitrpc"
        "call #{event.payload[:name]}"
      when "call_pipeline.html_pipeline"
        event.payload[:pipeline]
      when "call_filter.html_pipeline"
        event.payload[:filter]
      end
    end

    # A String with information about the event's payload. You probably want
    # #payload_lines instead to get a syntax highlighted version.
    def payload_string
      case event.name
      when "sql.active_record".freeze
        event.payload[:sql]
      when "call_pipeline.html_pipeline".freeze
        event.payload.slice(:filters).pretty_inspect
      when "call_filter.html_pipeline".freeze
        event.payload.slice(:pipeline).pretty_inspect
      when /\.redis\z/
        event.payload.slice(:cmd).pretty_inspect
      else
        event.payload.pretty_inspect
      end
    end

    def payload_syntax_scope
      if event.name == "sql.active_record".freeze
        "source.sql".freeze
      else
        "source.ruby".freeze
      end
    end

    # Is this event a "brief" event?
    def brief?
      event.duration < BRIEF_LIMIT_MS
    end

    MAXIMUM_HIGHLIGHTED_PAYLOADS = 256
    HIGHLIGHTING_TIMEOUT_SECONDS = 1

    def self.highlight_payloads(rows)
      contents = []
      scopes = []
      rows.each do |row|
        contents << row.payload_string
        scopes << row.payload_syntax_scope
      end

      highlighted_contents =
        GitHub::Colorize.highlight_many(scopes[0, MAXIMUM_HIGHLIGHTED_PAYLOADS],
                                                 contents[0, MAXIMUM_HIGHLIGHTED_PAYLOADS],
                                                 timeout: HIGHLIGHTING_TIMEOUT_SECONDS)

      rows.each_with_index do |row, index|
        row.payload_lines = highlighted_contents[index] || [contents[index]]
      end
    end
  end

  def families
    FAMILIES
  end

  # The rows to display in the timeline.
  #
  # Each row corresponds to a single event.
  #
  # Returns an Array of Rows.
  def rows
    @rows ||= rows!
  end

  private

  def rows!
    rows = GitHub.request_tracer.events
      .sort_by { |e, _| [e.time, e.duration] }
      .each_with_index
      .map { |(event, location), i| Row.new(event, location, i) }
    parent_stack = []
    rows.each do |row|
      while parent_row = parent_stack.last
        if parent_row.event.time <= row.event.time && row.event.end <= parent_row.event.end
          row.parent_id = parent_stack.last.id
          break
        end
        parent_stack.pop
      end
      parent_stack << row
    end

    Row.highlight_payloads(rows)

    rows.each(&:freeze)
    rows
  end
end
