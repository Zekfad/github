# frozen_string_literal: true

module Site
  class ContactView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::DateHelper
    include ApplicationHelper

    attr_reader :subject
    attr_reader :referer # [sic]
    attr_reader :reported_user
    attr_reader :reported_app
    attr_reader :reported_action
    attr_reader :flavor
    attr_reader :content_url
    attr_reader :classifier
    attr_reader :from_desktop_app
    attr_accessor :session

    GitHub.contact_form_flavors.each_key do |f|
      define_method("#{f.underscore}?") { f == flavor }
    end

    def flavor # rubocop:disable Lint/DuplicateMethods
      @flavor || "default"
    end

    def title
      GitHub.contact_form_flavors[flavor]
    end

    def show_contact_hint?
      from_api_docs? || from_help? || from_desktop_app?
    end

    def show_block_user_prompt?
      current_user && reported_user && !current_user.blocking?(reported_user) && !reported_user.organization?
    end

    def show_generic_support_hint?
      default?
    end

    def human_name
      return unless logged_in?
      current_user.profile_name
    end

    def has_emails?
      return false unless logged_in?
      emails.present?
    end

    # List all contactable, non-stealthy emails.
    def emails
      @contact_view_emails ||= current_user.emails.contactable
    end

    def abuse_report?
      return false if GitHub.enterprise?
      return false if from_jobs? || from_developer_program? || from_api_docs?
      flavor == "report-abuse"
    end

    def content_report?
      return false if GitHub.enterprise?
      return false if from_jobs? || from_developer_program? || from_api_docs?
      flavor == "report-content"
    end

    def hide_subject?
      false
    end

    def comment_label
      if abuse_report? || content_report?
        "What would you like to report?"
      else
        "How can we help?"
      end
    end

    def comment_placeholder
      if from_developer_program?
        "Tell us all about your integration!"
      elsif from_api_docs?
        "Please describe exactly what you’re trying to accomplish with the API."
      elsif abuse_report? || content_report?
        "Please provide as much detail as possible about the account or behavior you are reporting. It's especially helpful to include specific examples in the form of URLs" + (logged_in? ? " or screenshots." : ".")
      else
        "Please describe exactly what you’re trying to accomplish."
      end
    end

    def comment_class
      "input-block js-comment-field js-quick-submit"
    end

    def comment_template
      ContactRequest.template(flavor)
    end

    def from_help?
      @from_help ||= referer =~ %r{\Ahttps?://help.github.com/}
    end

    # Create a humanized representation of this help article title,
    # based on the referer.
    #
    # Returns a string.
    def help_article_humanized
      title = referer.split("/").last.tr("-", " ")

      # In the rare case we have non alphanum or spaces (a user-faked url), use a default
      title =~ /\A[\w\s]+\z/ ? title.humanize : "An article on help.github.com"
    end

    def from_developer_program?
      @from_developer_program ||= referer =~ /^https?:\/\/developer\.github\.com\/program/
    end

    def from_api_docs?
      return false if from_developer_program?
      @from_api_docs ||= referer =~ /^https?:\/\/developer\.github\.com/
    end

    def api_article
      return "API - root" if referer =~ /^https?:\/\/developer\.github\.com$/
      return "API - v3" if referer =~ /^https?:\/\/developer\.github\.com\/v3\/$/
      path = referer.gsub(/^https?:\/\/developer\.github\.com\/v3\//, "API - ")
      path.gsub(/\/$/, "")
    end

    def from_guides?
      @from_guides ||= referer =~ /^https?:\/\/guides\.github\.com/
    end

    def guides_path
      return "Guides - root" if referer =~ /^https?:\/\/guides\.github\.com$/
      path = referer.gsub(/^https?:\/\/guides\.github\.com\//, "Guides - ")
      path.gsub(/\/$/, "")
    end

    def from_education?
      @from_education ||= referer =~ /^https?:\/\/education(-staging)?\.github(app)?\.com(\/|$)/
    end

    def from_desktop_app?
      @from_desktop_app
    end

    def business_plus_link
      if current_user && current_user.business_plus?
        "https://enterprise.githubsupport.com/hc/en-us/requests/new?github_product=cloud"
      else
        "https://enterprise.githubsupport.com/hc/en-us/requests/new"
      end
    end

    def premium_support_link
      "https://premium.githubsupport.com"
    end

    def default_inbox
      return if GitHub.enterprise?
      if from_education?
        "education"
      elsif from_jobs?
        "jobs"
      elsif abuse_report? || content_report? || sensitive_content?
        "terms-of-service"
      elsif from_desktop_app?
        "desktop"
      end
    end

    def default_inbox?
      default_inbox.present?
    end

    def project_host_name
      GitHub.enterprise? ? GitHub.host_name : "GitHub"
    end

    def show_captcha?
      return @show_captcha if defined?(@show_captcha)
      @show_captcha ||= begin
        if !logged_in? &&
            Octocaptcha.new(session, page: :report_abuse).show_captcha? &&
            (flavor == "report-abuse" || flavor == "report-content")
          true
        else
          false
        end
      end
    end

    def form_class
      show_captcha? ? "mt-4 js-octocaptcha-parent"
                    : "mt-4"
    end

    def form_submit_button_class
      show_captcha? ? "btn btn-primary js-octocaptcha-form-submit"
                    : "btn btn-primary"
    end

    private

    def from_jobs?
      @from_jobs ||= referer =~ /^https?:\/\/jobs\.github\.com/
    end
  end
end
