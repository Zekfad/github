# frozen_string_literal: true

class Site::KeyboardShortcuts < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include UrlHelper
  attr_reader :is_admin

  def shortcuts(section)
    if keyboard_shortcuts.has_key?(section)
      keyboard_shortcuts[section].reject do |shortcut|
        !is_admin && shortcut[:admin]
      end
    else
      []
    end
  end

  private

  def keyboard_shortcuts
    {
      global: [
        {
          keys: ["s", "/"],
          label: "Focus search bar",
          join: "or",
        },
        {
          keys: ["g", "n"],
          label: "Go to notifications",
        },
        {
          keys: ["g", "d"],
          label: "Go to dashboard",
        },
        {
          keys: ["?"],
          label: "Bring up this help dialog",
        },
        {
          keys: ["j"],
          label: "Move selection down",
        },
        {
          keys: ["k"],
          label: "Move selection up",
        },
        {
          keys: ["x"],
          label: "Toggle selection",
        },
        {
          keys: ["o", "enter"],
          label: "Open selection",
          join: "or",
        },
        {
          keys: ["shift a"],
          label: "Open site admin dashboard",
          admin: true,
        },
        {
          keys: ["\\"],
          label: "Open site admin modal",
          admin: true,
        },
      ],
      repositories: [
        {
          keys: ["ctrl /"],
          label: "Focus secondary search bar",
        },
        {
          keys: ["g", "c"],
          label: "Go to Code",
        },
        {
          keys: ["g", "i"],
          label: "Go to Issues",
        },
        {
          keys: ["g", "p"],
          label: "Go to Pull Requests",
        },
        {
          keys: ["g", "b"],
          label: "Go to Projects",
        },
        {
          keys: ["g", "w"],
          label: "Go to Wiki",
        },
      ],
      code_editor: [
        {
          keys: ["ctrl shift p"],
          label: "Preview changes",
        },
        {
          keys: ["ctrl /"],
          label: "Toggle line comment",
        },
      ],
      source_code: [
        {
          keys: ["t"],
          label: "Activates the file finder",
        },
        {
          keys: ["l"],
          label: "Jump to line",
        },
        {
          keys: ["w"],
          label: "Switch branch/tag",
        },
        {
          keys: ["y"],
          label: "Expand URL to its canonical form",
        },
        {
          keys: ["i"],
          label: "Show/hide all inline notes",
        },
        {
          keys: ["b"],
          label: "Open blame",
        },
      ],
      issues: [
        {
          keys: ["ctrl enter"],
          label: "Submit comment",
        },
        {
          keys: ["ctrl shift enter"],
          label: "Submit comment and close issue",
        },
        {
          keys: ["ctrl shift p"],
          label: "Preview comment",
        },
        {
          keys: ["c"],
          label: "Create issue",
        },
        {
          keys: ["u"],
          label: "Filter by author",
        },
        {
          keys: ["a"],
          label: "Filter by or edit assignees",
        },
        {
          keys: ["l"],
          label: "Filter by or edit labels",
        },
        {
          keys: ["p"],
          label: "Filter by or edit projects",
        },
        {
          keys: ["m"],
          label: "Filter by or edit milestones",
        },
        {
          keys: ["r"],
          label: "Reply (quoting selected text)",
        },
        {
          keys: ["ctrl ."],
          label: "Open saved replies",
        },
        {
          keys: ["ctrl 1", "ctrl 9"],
          label: "Insert saved reply (with open saved replies)",
          join: "-",
        },
      ],
      commits: [
        {
          keys: ["ctrl enter"],
          label: "Submit comment",
        },
        {
          keys: ["escape"],
          label: "Close form",
        },
        {
          keys: ["p"],
          label: "Parent commit",
        },
        {
          keys: ["o"],
          label: "Other parent commit",
        },
      ],
      commit_list: [
        {
          keys: ["y"],
          label: "Expand URL to its canonical form",
        },
      ],
      dashboards: [
        {
          keys: ["g", "i"],
          label: "Go to your issues",
        },
        {
          keys: ["g", "p"],
          label: "Go to your pull requests",
        },
      ],
      notifications: [
        {
          keys: ["e", "I", "y"],
          label: "Mark as read",
          join: "/",
        },
        {
          keys: ["shift m"],
          label: "Mute thread",
        },
      ],
      notifications_v2: [
        {
          keys: ["e"],
          label: "Mark as done",
          join: "/",
        },
        {
          keys: ["shift i"],
          label: "Mark as read",
          join: "/",
        },
        {
          keys: ["shift u"],
          label: "Mark as unread",
          join: "/",
        },
        {
          keys: ["shift m"],
          label: "Unsubscribe",
        },
      ],
      moving_columns: [
        {
          keys: ["enter", "space"],
          label: "Start moving the focused column",
        },
        {
          keys: ["escape"],
          label: "Cancel the move in progress",
        },
        {
          keys: ["enter"],
          label: "Complete the move in progress",
        },
        {
          keys: ["←", "h"],
          label: "Move column to the left",
          join: "/",
        },
        {
          keys: ["ctrl ←", "ctrl h"],
          label: "Move column to the leftmost position",
        },
        {
          keys: ["→", "l"],
          label: "Move column to the right",
          join: "/",
        },
        {
          keys: ["ctrl →", "ctrl l"],
          label: "Move column to the rightmost position",
        },
      ],
      project_cards: [
        {
          keys: ["d"],
          label: "Open the issue or pull request associated with the focused card in the sidebar",
        },
      ],
      pull_request_list: [
        {
          keys: ["o", "enter"],
          label: "Open pull request",
        },
      ],
      pull_request_conversation: [
        {
          keys: ["ctrl enter"],
          label: "Submit comment",
        },
        {
          keys: ["ctrl shift enter"],
          label: "Submit comment and close or open pull request",
        },
        {
          keys: ["ctrl shift p"],
          label: "Preview comment",
        },
        {
          keys: ["q"],
          label: "Request reviewers",
        },
        {
          keys: ["a"],
          label: "Filter by or edit assignees",
        },
        {
          keys: ["l"],
          label: "Filter by or edit labels",
        },
        {
          keys: ["p"],
          label: "Filter by or edit projects",
        },
        {
          keys: ["m"],
          label: "Filter by or edit milestones",
        },
        {
          keys: ["r"],
          label: "Reply (quoting selected text)",
        },
        {
          keys: ["ctrl ."],
          label: "Open saved replies",
        },
        {
          keys: ["ctrl 1", "ctrl 9"],
          label: "Insert saved reply (with open saved replies)",
          join: "-",
        },
        {
          keys: ["Alt"],
          label: "Toggle visibility of all collapsed review comments instead of just the current one",
          hold: true,
        },
      ],
      pull_request_files_changed: [
        {
          keys: ["c"],
          label: "Open commits list",
        },
        {
          keys: ["t"],
          label: "Open files list",
        },
        {
          keys: ["n"],
          label: "Next commit",
        },
        {
          keys: ["p"],
          label: "Previous commit",
        },
        {
          keys: ["ctrl enter"],
          label: "Start a review",
        },
        {
          keys: ["ctrl shift enter"],
          label: "Add single comment",
        },
        {
          keys: ["Alt"],
          label: "Collapse or expand all files instead of just the current one",
          hold: true,
        },
      ],
      network_graph: [
        {
          keys: ["←", "h"],
          label: "Scroll left",
        },
        {
          keys: ["→", "l"],
          label: "Scroll right",
        },
        {
          keys: ["↑", "k"],
          label: "Scroll up",
        },
        {
          keys: ["↓", "j"],
          label: "Scroll down",
        },
        {
          keys: ["t"],
          label: "Toggle visibility of the head labels",
        },
        {
          keys: ["shift ←", "shift h"],
          label: "Scroll all the way left",
          join: "/",
        },
        {
          keys: ["shift →", "shift l"],
          label: "Scroll all the way right",
          join: "/",
        },
        {
          keys: ["shift ↑", "shift k"],
          label: "Scroll all the way up",
          join: "/",
        },
        {
          keys: ["shift ↓", "shift j"],
          label: "Scroll all the way down",
          join: "/",
        },
      ],
      moving_a_card: [
        {
          keys: ["enter", "space"],
          label: "Start moving the focused card",
        },
        {
          keys: ["escape"],
          label: "Cancel the move in progress",
        },
        {
          keys: ["enter"],
          label: "Complete the move in progress",
        },
        {
          keys: ["↓", "j"],
          label: "Move card down",
          join: "/",
        },
        {
          keys: ["ctrl ↓", "ctrl j"],
          label: "Move card to the bottom of the column",
          join: "/",
        },
        {
          keys: ["↑", "k"],
          label: "Move card up",
          join: "/",
        },
        {
          keys: ["ctrl ↑", "ctrl k"],
          label: "Move card to the top of the column",
          join: "/",
        },
        {
          keys: ["←", "h"],
          label: "Move card to the bottom of the column on the left",
        },
        {
          keys: ["shift ←", "shift h"],
          label: "Move card to the top of the column on the left",
        },
        {
          keys: ["ctrl ←", "ctrl h"],
          label: "Move card to the bottom of the leftmost column",
        },
        {
          keys: ["ctrl shift ←", "ctrl shift h"],
          label: "Move card to the top of the leftmost column",
        },
        {
          keys: ["→", "l"],
          label: "Move card to the bottom of the column on the right",
          join: "/",
        },
        {
          keys: ["shift →", "shift l"],
          label: "Move card to the top of the column on the right",
        },
        {
          keys: ["ctrl →", "ctrl l"],
          label: "Move card to the bottom of the rightmost column",
        },
        {
          keys: ["ctrl shift →", "ctrl shift l"],
          label: "Move card to the top of the rightmost column",
        },
      ],
      checks: [
        {
          keys: ["c"],
          label: "Open the commit menu",
        },
      ],
    }
  end
end
