# frozen_string_literal: true

module Site
  class Stats
    include ActionView::Helpers::NumberHelper
    include ActionView::Helpers::TagHelper

    def line_profiler_enabled?
      GitHub::LineProfilerMiddleware.enabled?
    end

    def flamegraph_enabled?
      GitHub::FlamegraphMiddleware.enabled?
    end

    def allocation_tracer_enabled?
      GitHub::AllocationTracerMiddleware.enabled?
    end

    attr_reader :request, :element_id_suffix

    def initialize(options = {})
      @request = options[:request]
      @element_id_suffix = options[:element_id_suffix] || ""
    end

    def enterprise?
      GitHub.enterprise?
    end

    def single_instance?
      GitHub.single_instance?
    end

    def xhr_stats
      nil
    end

    def process_stats
      @process_stats ||= @request.env[Rack::ProcessUtilization::ENV_KEY]
    end

    def site_stats
      @site_stats ||= GitHub::Stats.site(enterprise?)
    end
    attr_writer :site_stats if Rails.test?

    def process_stat_tracked?(sym)
      process_stats && process_stats.send("track_#{sym}?")
    end

    def gc_info
      process_stats.gc_info
    end

    def process_stat_visible?(sym)
      queries, time = process_stats.send("#{sym}_stats")
      time > 0.001
    end

    def process_stat(sym)
      format_stats process_stats.send(sym)
    end

    def server_status(sym)
      if site_stats
        classes = site_stats[sym] ? "dot active" : "dot inactive"
        content_tag(:span, "", class: classes) # rubocop:disable Rails/ViewModelHTML
      end
    end

    def format_stats(stats)
      queries, time = stats
      "#{format_time time} / #{queries}"
    end

    def gc_time
      format_time gc_info.time
    end

    def response_time_stats
      if sec = response_time_secs
        format_time sec
      end
    end

    def response_time_secs
      if start_time = request.env["process.request_start"]
        ("%.5f" % (Time.now - start_time)).to_f
      end
    end

    def response_time_ms
      if secs = response_time_secs
        number_with_precision (secs * 1000), precision: 0
      end
    end

    def render_stats?
      process_stat_tracked?(:render) && ActionView::Template.template_trace_enabled
    end

    def render_stats_visible?
      process_stats.render_stats.total_time > 0
    end

    def render_time
      format_time process_stats.render_stats.total_time
    end

    def render_trace_html
      count = 0
      process_stats.render_stats.root.children_html.gsub(/'template'/) {
        count += 1
        count.even? ? "'template even'" : "'template odd'"
      }.html_safe # rubocop:disable Rails/OutputSafety
    end

    def cpu_stats
      cpu, idle, real = process_stats.cpu_stats
      format_time (cpu + idle)
    end

    def cpu_stats_detail
      cpu, idle, real = process_stats.cpu_stats
      "#{format_time cpu} CPU + #{format_time idle} idle"
    end

    def obj_allocated
      "#{number_with_delimiter process_stats.gc_info.allocations} allocations"
    end

    def ar_stats?
      process_stat_tracked?(:activerecord) && process_stats.activerecord_stats[0] > 0
    end

    def ar_objs
      ar_objs, ar_types = process_stats.activerecord_stats
      width = ar_objs.to_s.length
      rows = ar_types.sort_by { |n, o| -o }.map { |n, o| sprintf("%#{width}d | %s\n", o, n) }
      sprintf("%#{width}d | AR objects\n\n%s", ar_objs, rows.join(""))
    end

    def es_loader_stats?
      process_stat_tracked?(:es) && Elastomer::QueryStats.instance.any?
    end

    def es_loader_stats
      stats = Elastomer::QueryStats.instance

      path_width = stats.map(&:url_path).map(&:length).max
      cluster_width = stats.map(&:cluster).map(&:length).max
      rows = stats.map do |stat|
        sprintf("%5s | %5s | %5s | %#{cluster_width}s | %#{path_width}s\n",
          stat.time,
          stat.timed_out,
          stat.total,
          stat.cluster,
          stat.url_path,
        )
      end

      sprintf("Elasticsearch URL\n\n%s", rows.join(""))
      sprintf("%5s | %5s | %5s | %#{cluster_width}s | %#{path_width}s\n%s", "Time", "T-Out", "Hits", "Cluster", "Path", rows.join(""))
    end

    def graphql_loader_stats?
      process_stat_tracked?(:graphql) && Platform::LoaderTracker.loaders.any?
    end

    def graphql_loader_stats
      loaders = Platform::LoaderTracker.loaders

      fetch_count = loaders.values.map(&:length).sum
      fetch_duration_ms = loaders.values.flat_map(&:sum).sum

      width = loaders.values.map(&:length).max.to_s.length

      rows = loaders.sort_by do |(loader, durations)|
        -durations.length
      end.map do |(loader, durations)|
        sprintf("%#{width}d | %s (%0.2fms)\n", durations.length, loader, durations.sum)
      end

      sprintf("%#{width}d | Batch Loader fetches (%0.2fms)\n\n%s", fetch_count, fetch_duration_ms, rows.join(""))
    end

    def query_cache_hits
      ar_objs, ar_types, hits = process_stats.activerecord_stats
      hits
    end

    def pending_jobs
      number_with_delimiter site_stats[:pending_jobs]
    end

    def pending_jobs_raw
      site_stats[:pending_jobs]
    end

    def job_stats?
      enterprise? || Rails.env.development?
    end

    def gitrpc_call_stats
      call_stats = GitRPCLogSubscriber.rpc_call_stats
      return "" if call_stats.empty?
      rows = call_stats
        .sort_by { |command, stats| -stats.time }
        .map     { |command, stats| sprintf("%7.3f | %7d | %s\n", stats.time, stats.count, command) }
      sprintf("%7s | %7s | %s\n%s", "Seconds", "Count", "Command", rows.join(""))
    end

    def gitrpc_trace
      _, _, rpc_calls = process_stats.gitrpc_stats

      rpc_calls
    end

    def redis_trace
      _, _, redis_queries = process_stats.redis_stats

      redis_queries
    end

    def exceptions?
      Rails.development? && (most_recent_exception > 1.hour.ago)
    end

    def most_recent_exception
      @most_recent_exception ||=
        begin
          stat = File.stat(GitHub.failbot_log_path)
          return Time.at(0) if stat.size == 0
          stat.mtime
        rescue Errno::ENOENT
          Time.at(0)
        end
    end

    def controller_key
      @controller_key ||= request.params[:controller].parameterize
    end

    def action_key
      @action_key ||= request.params[:action].parameterize
    end

    def glb_hosts
      hostnames = process_stats.glb_via.map { |row| row["hostname"] }

      # include the fe/k8s node as the termination point
      hostnames.push(Socket.gethostname)

      hostnames.join("\n")
    end

    def glb_regions
      regions = process_stats.glb_via.map { |row| row["region"] }

      # include the k8s cluster name, or unicorn region, as the termination point
      if GitHub.kube?
        regions.push(GitHub.kubernetes_cluster_name || "k8s")
      else
        regions.push(GitHub.server_region)
      end

      # we should never have a region loop, so duplicate regions should be neighboring entries
      # this means the regions list will show each region _change_, rather than each hop's region
      regions = regions.uniq

      safe_join(regions, " \u2192 ")
    end

    def allocation_interval
      target_samples = 6000
      interval = (process_stats.gc_info.allocations.to_f / target_samples).to_i
      interval > 0 ? interval : 1
    end

    def flamegraph_wall_path
      # We want to try to collect about 1000 samples independent of the
      # duration of the request. We attempt this by guessing an interval based
      # on current request's response time. It doesn't need to be perfect.
      # The interval is a period in microseconds.
      target_samples = 1000
      flamegraph_interval = (response_time_secs * 1_000_000 / target_samples).to_i

      "#{flamegraph_base_path}&flamegraph_interval=#{flamegraph_interval}"
    end

    def flamegraph_object_path
      # We want to try to collect about 1000 samples independent of the
      # number of objects allocated.
      # The interval represents tracking every 1/N allocations
      target_samples = 1000
      flamegraph_interval = (process_stats.gc_info.allocations.to_f / target_samples).to_i

      "#{flamegraph_base_path}&flamegraph_mode=object&flamegraph_interval=#{flamegraph_interval}"
    end

    private
      def flamegraph_base_path
        "/stafftools/graphs/flamegraph?url=#{EscapeUtils.escape_url(request.fullpath)}"
      end

      def kube_backend?(hostname)
        # Kubernetes Pods have a hostname set to the pod name, which should
        # match this (potentially fragile) regex
        hostname =~ /\A[\w-]+-\d+-[0-9a-z]+\Z/ && GitHub.kube?
      end

      def format_time(time)
        time = time * 1000
        if time >= 1000
          "%.1fs" % (time / 1000)
        else
          "%.0fms" % time
        end
      end
  end
end
