# frozen_string_literal: true

class Businesses::PendingMembersView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

  attr_reader :invitations, :business

  EMAIL_INVITATION = "EMAIL"
  USER_INVITATION = "USER"

  def initialize(**args)
    super(args)
    @business = args[:business]
    @invitations = args[:invitations]
  end

  def invitations_rollup
    invitations.each_with_object({}) do |invitation, rollup|
      key = rollup.keys.find { |keyed_invite| rollup_invites?(keyed_invite, invitation) }
      key ||= invitation

      rollup[key] ||= { logins: [] }

      rollup[key][:logins] << invitation.organization.login
    end
  end

  private

  def rollup_invites?(invite, other)
    return false unless invitation_type(invite) == invitation_type(other)

    invitation_type(invite) == EMAIL_INVITATION ?
      invite.email == other.email :
      invite.invitee.login == other.invitee.login
  end

  def invitation_type(invitation)
    invitation.email.present? ? EMAIL_INVITATION : USER_INVITATION
  end
end
