# frozen_string_literal: true

module Businesses
  class PeopleView < Businesses::QueryView
    # query filters defined for QueryView
    attr_reader :role, :organizations, :deployment, :license

    def organization_member_link(link_id)
      return urls.user_path(link_id) if GitHub.single_business_environment?
      urls.organizations_enterprise_user_account_path(link_id)
    end

    def organization_member_link_data_options(member)
      return {} unless member

      case member
      when ::User
        helpers.hovercard_data_attributes_for_user(member)
      when ::BusinessUserAccount
        helpers.hovercard_data_attributes_for_business_user_account(member)
      end
    end

    def membership_count_label(org_count, server_count)
      parts = []
      parts << helpers.pluralize(org_count, "organization") if org_count > 0
      parts << helpers.pluralize(server_count, "server") if server_count > 0

      parts.to_sentence
    end

    def filter_map
      BusinessesHelper::MEMBERS_QUERY_FILTERS
    end

    def enterprise_organizations_for(business:, member:, viewer:)
      case member
      when ::BusinessUserAccount
        member.enterprise_organizations(viewer)
      when ::User
        if GitHub.single_business_environment?
          member.organizations_visible_to(viewer)
        else
          business_user_account = business.user_accounts.find_by(user: member)
          return Organization.none if business_user_account.blank?
          business_user_account.enterprise_organizations(viewer)
        end
      else
        Organization.none
      end
    end
  end
end
