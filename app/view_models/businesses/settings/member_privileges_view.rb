# frozen_string_literal: true

class Businesses::Settings::MemberPrivilegesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business, :params

  def initialize(**args)
    super(args)
    @business = args[:business]
  end

  # "Base permissions" setting helpers

  def default_repository_permissions_change_form_path
    urls.update_default_repository_permission_enterprise_path(business)
  end

  def default_repository_permissions_select_list
    @select_list ||= [
      {
        heading: "No policy",
        description: "Organizations choose default repository permissions for their members.",
        value: "no_policy",
        selected: default_repository_permissions_value == "no_policy",
      },
      {
        heading: "None",
        description: "Organization members will only be able to clone and pull public repositories.",
        value: "none",
        selected: default_repository_permissions_value == "none",
      },
      {
        heading: "Read",
        description: "Organization members will be able to clone and pull all organization repositories.",
        value: "read",
        selected: default_repository_permissions_value == "read",
      },
      {
        heading: "Write",
        description: "Organization members will be able to clone, pull, and push all organization repositories.",
        value: "write",
        selected: default_repository_permissions_value == "write",
      },
      {
        heading: "Admin",
        description: "Organization members will be able to clone, pull, push, and add new collaborators to all organization repositories.",
        value: "admin",
        selected: default_repository_permissions_value == "admin",
      },
    ]
  end

  def default_repository_permissions_value
    if !business.default_repository_permission_policy?
      return "no_policy"
    end

    case business.default_repository_permission
    when :admin
      "admin"
    when :write
      "write"
    when :read
      "read"
    when :none
      "none"
    end
  end

  def default_repository_permissions_button_text
    default_repository_permissions_selected_option[:heading]
  end

  def default_repository_permissions_selected_option
    default_repository_permissions_select_list.find { |s| s[:selected] }
  end

  def updating_default_repository_permissions?
    business.updating_default_repository_permission?
  end

  def default_repository_permission_setting_organizations_business_path
    urls.default_repository_permission_organizations_settings_enterprise_path(business)
  end

  # "Repository visibility change" setting helpers

  def repo_visibility_change_form_path
    urls.update_members_can_change_repo_visibility_enterprise_path(business)
  end

  def repo_visibility_change_select_list
    @repo_visibility_change_select_list ||= [
      {
        heading: "No policy",
        description: "Organizations choose whether to allow members with admin permissions to change repository visibilities.",
        value: "no_policy",
        selected: repo_visibility_change_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Organizations always allow members with admin permissions to change repository visibilities.",
        value: "enabled",
        selected: repo_visibility_change_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Organizations never allow members with admin permissions to change repository visibilities.",
        value: "disabled",
        selected: repo_visibility_change_value == "disabled",
      },
    ]
  end

  def repo_visibility_change_value
    if !business.members_can_change_repo_visibility_policy?
      "no_policy"
    elsif business.members_can_change_repo_visibility?
      "enabled"
    else
      "disabled"
    end
  end

  def repo_visibility_change_button_text
    repo_visibility_change_selected_option[:heading]
  end

  def repo_visibility_change_input_value
    repo_visibility_change_selected_option[:value]
  end

  def repo_visibility_change_selected_option
    repo_visibility_change_select_list.find { |s| s[:selected] }
  end

  def members_can_change_repository_visibility_setting_organizations_business_path
    urls.members_can_change_repository_visibility_organizations_settings_enterprise_path(business)
  end

  # "Repository forking" setting helpers

  def allow_private_repository_forking_form_path
    urls.update_allow_private_repository_forking_enterprise_path(business)
  end

  def allow_private_repository_forking_select_list
    @allow_private_repository_forking_select_list ||= [
      {
        heading: "No policy",
        description: "Organizations choose whether to allow private and internal repositories to be forked.",
        value: "no_policy",
        selected: allow_private_repository_forking_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Organizations always allow private and internal repositories to be forked.",
        value: "enabled",
        selected: allow_private_repository_forking_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Organizations never allow private or internal repositories to be forked.",
        value: "disabled",
        selected: allow_private_repository_forking_value == "disabled",
      },
    ]
  end

  def allow_private_repository_forking_value
    if !business.allow_private_repository_forking_policy?
      "no_policy"
    elsif business.allow_private_repository_forking?
      "enabled"
    else
      "disabled"
    end
  end

  def allow_private_repository_forking_button_text
    allow_private_repository_forking_selected_option[:heading]
  end

  def allow_private_repository_forking_input_value
    allow_private_repository_forking_selected_option[:value]
  end

  def allow_private_repository_forking_selected_option
    allow_private_repository_forking_select_list.find { |s| s[:selected] }
  end

  def allow_private_repository_forking_setting_organizations_business_path
    urls.allow_private_repository_forking_organizations_settings_enterprise_path(business)
  end

  # Members can update protected branches setting helpers

  def members_can_update_protected_branches_select_list
    @update_branch_protection_select_list ||= [
      {
        heading: "No policy",
        description: "Organization administrators choose whether to allow updating protected branches settings.",
        value: "no_policy",
        selected: members_can_update_protected_branches_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Members with admin permissions will be able to update protected branches.",
        value: "enabled",
        selected: members_can_update_protected_branches_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Only business owners can update protected branches.",
        value: "disabled",
        selected: members_can_update_protected_branches_value == "disabled",
      },
    ]
  end

  def members_can_update_protected_branches_value
    if !business.members_can_update_protected_branches_policy?
      "no_policy"
    elsif business.members_can_update_protected_branches?
      "enabled"
    else
      "disabled"
    end
  end

  def members_can_update_protected_branches_button_text
    members_can_update_protected_branches_selected_option[:heading]
  end

  def members_can_update_protected_branches_input_value
    members_can_update_protected_branches_selected_option[:value]
  end

  def members_can_update_protected_branches_selected_option
    members_can_update_protected_branches_select_list.find { |s| s[:selected] }
  end

  def members_can_update_protected_branches_form_path
    urls.settings_members_can_update_protected_branches_enterprise_path(business)
  end

  def members_can_update_protected_branches_setting_organizations_business_path
    urls.members_can_update_protected_branches_organizations_settings_enterprise_path(business)
  end

  # Members Can Delete Issues setting helpers

  def members_can_delete_issues_select_list
    @delete_issues_select_list ||= [
      {
        heading: "No policy",
        description: "Organization administrators choose whether to allow issue deletes.",
        value: "no_policy",
        selected: members_can_delete_issues_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Members with admin permissions will be able to delete issues.",
        value: "enabled",
        selected: members_can_delete_issues_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Only organization owners can delete issues.",
        value: "disabled",
        selected: members_can_delete_issues_value == "disabled",
      },
    ]
  end

  def members_can_delete_issues_value
    if !business.members_can_delete_issues_policy?
      "no_policy"
    elsif business.members_can_delete_issues?
      "enabled"
    else
      "disabled"
    end
  end

  def members_can_delete_issues_button_text
    members_can_delete_issues_selected_option[:heading]
  end

  def members_can_delete_issues_input_value
    members_can_delete_issues_selected_option[:value]
  end

  def members_can_delete_issues_selected_option
    members_can_delete_issues_select_list.find { |s| s[:selected] }
  end

  def members_can_delete_issues_form_path
    urls.settings_members_can_delete_issues_enterprise_path(business)
  end

  def members_can_delete_issues_setting_organizations_business_path
    urls.members_can_delete_issues_organizations_settings_enterprise_path(business)
  end

  def action_execution_capability_select_list
    @action_execution_capability_select_list ||= [
      {
        heading: "No policy",
        description: "This setting delegates all decisions for Actions to the individual organization administrators.",
        value: "no_policy",
        selected: action_execution_capability_value == "no_policy",
      },
      {
        heading: "Enable local & third party Actions across all organizations",
        description: "This allows all repositories to execute any Action, whether the code for the Action exists within the same repository,
        same organization, or a repository owned by a third party.",
        value: "all",
        selected: action_execution_capability_value.match?("all"),
      },
      {
        heading: "Enable local Actions only across all organizations.",
        description: "This allows all repositories to execute any Action, as long as the code for the Action exists within the same repository.",
        value: "local_actions",
        selected: action_execution_capability_value.match?("local"),
      },
      {
        heading: "Disable Actions for all organizations.",
        description: "This disallows any Action from running on any repository in any organization.",
        value: "disabled",
        selected: action_execution_capability_value == "disabled",
      },
    ]
  end

  def action_execution_capability_value
    return "no_policy" unless business.action_execution_capabilities_policy?
    business.action_execution_capabilities && business.action_execution_capabilities.downcase
  end

  def action_execution_capability_form_path
    urls.update_action_execution_capability_enterprise_path(business)
  end

  def action_execution_capability_button_text
    action_execution_capability_selected_option[:heading]
  end

  def action_execution_capability_input_value
    action_execution_capability_selected_option[:value]
  end

  def action_execution_capability_selected_option
    action_execution_capability_select_list.find { |s| s[:selected] }
  end

  def action_execution_capability_setting_organizations_business_path
    urls.action_execution_capability_organizations_settings_enterprise_path(business)
  end

  # Members Can Delete or Transfer Repositories setting helpers

  def members_can_delete_repos_select_list
    @delete_repos_select_list ||= [
      {
        heading: "No policy",
        description: "Organization administrators choose whether to allow repository deletes and transfers.",
        value: "no_policy",
        selected: members_can_delete_repos_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Members with admin permissions will be able to delete or transfer repositories.",
        value: "enabled",
        selected: members_can_delete_repos_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Only organization owners can delete or transfer repositories.",
        value: "disabled",
        selected: members_can_delete_repos_value == "disabled",
      },
    ]
  end

  def members_can_delete_repos_value
    if !business.members_can_delete_repositories_policy?
      "no_policy"
    elsif business.members_can_delete_repositories?
      "enabled"
    else
      "disabled"
    end
  end

  def members_can_delete_repos_button_text
    members_can_delete_repos_selected_option[:heading]
  end

  def members_can_delete_repos_input_value
    members_can_delete_repos_selected_option[:value]
  end

  def members_can_delete_repos_selected_option
    members_can_delete_repos_select_list.find { |s| s[:selected] }
  end

  def members_can_delete_repos_form_path
    urls.settings_members_can_delete_repositories_enterprise_path(business)
  end

  def members_can_delete_repositories_setting_organizations_business_path
    urls.members_can_delete_repositories_organizations_settings_enterprise_path(business)
  end

  # Members Can Create Repositories setting helpers
  def members_can_create_repositories_no_policy_checked?
    !business.members_can_create_repositories_policy?
  end

  def members_can_create_repositories_disabled_checked?
    !members_can_create_repositories_no_policy_checked? && !any_repo_creation_enabled?
  end

  def members_can_create_repositories_allowed_checked?
    any_repo_creation_enabled?
  end

  private def any_repo_creation_enabled?
    all_disabled = !members_can_create_public_repos_checked? && !members_can_create_private_repos_checked?
    # This logic can move to the model once the backward-compatible GraphQL API goes away.
    all_disabled = all_disabled && !members_can_create_internal_repos_checked?
    !all_disabled
  end

  def members_can_create_public_repos_checked?
    return nil unless business.members_can_create_repositories_policy?
    business.members_can_create_public_repositories?
  end

  def members_can_create_private_repos_checked?
    return nil unless business.members_can_create_repositories_policy?
    business.members_can_create_private_repositories?
  end

  def members_can_create_internal_repos_checked?
    return nil unless business.members_can_create_repositories_policy?
    business.members_can_create_internal_repositories?
  end

  def members_can_create_repos_form_path
    urls.settings_members_can_create_repositories_enterprise_path(business)
  end

  def repository_creation_setting_organizations_business_path
    urls.repository_creation_organizations_settings_enterprise_path(business)
  end

  # Members Can invite outside collaborators setting helpers
  def members_can_invite_outside_collaborators_select_list
    @invite_outside_collaborators_select_list ||= [
      {
        heading: "No policy",
        description: "Organization administrators choose whether to allow members to invite outside collaborators.",
        value: "no_policy",
        selected: members_can_invite_outside_collaborators_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Members can invite outside collaborators.",
        value: "enabled",
        selected: members_can_invite_outside_collaborators_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Only organization owners can invite outside collaborators.",
        value: "disabled",
        selected: members_can_invite_outside_collaborators_value == "disabled",
      },
    ]
  end

  def members_can_invite_outside_collaborators_value
    if !business.members_can_invite_outside_collaborators_policy?
      "no_policy"
    elsif business.members_can_invite_outside_collaborators?
      "enabled"
    else
      "disabled"
    end
  end

  def members_can_invite_outside_collaborators_button_text
    members_can_invite_outside_collaborators_selected_option[:heading]
  end

  def members_can_invite_outside_collaborators_input_value
    members_can_invite_outside_collaborators_selected_option[:value]
  end

  def members_can_invite_outside_collaborators_selected_option
    members_can_invite_outside_collaborators_select_list.find { |s| s[:selected] }
  end

  def members_can_invite_outside_collaborators_form_path
    urls.settings_members_can_invite_outside_collaborators_enterprise_path(business)
  end

  def members_can_invite_collaborators_setting_organizations_business_path
    urls.members_can_invite_collaborators_organizations_settings_enterprise_path(business)
  end
end
