# frozen_string_literal: true

class Businesses::Settings::SecurityView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

  attr_reader :business, :params

  def initialize(**args)
    super(args)

    @business = args[:business]
    @params = args[:params] || {}
    @current_user = args[:current_user]
  end

  def two_factor_requirement_form_disabled?
    !GitHub.auth.two_factor_org_requirement_allowed? ||
      (!two_factor_requirement_enabled? && blocking_orgs_count > 0)
  end

  def disabled_form_reason
    if !GitHub.auth.two_factor_authentication_enabled?
      "Built-in two-factor authentication is disabled on your instance."
    elsif GitHub.auth.builtin_auth_fallback?
      "Two-factor authentication can't be enforced when both built-in and #{GitHub.auth.name} users are allowed."
    elsif !current_user.two_factor_authentication_enabled?
      link = helpers.link_to("enabled on your personal account", urls.settings_user_security_path)
      helpers.safe_join(["Two-factor authentication must be ", link, " to require it for the enterprise."])
    elsif blocking_orgs_count > 0
      count = blocking_orgs_count
      "Enforcing two-factor authentication would remove all admins from the #{blocking_orgs_names} #{"organization".pluralize(count)}."
    end
  end

  def blocking_orgs
    @blocking_orgs ||= organizations_blocking_enabling_two_factor.limit(10)
  end

  def blocking_orgs_count
    @blocking_orgs_count ||= organizations_blocking_enabling_two_factor.count
  end

  def blocking_orgs_names
    names = blocking_orgs.flat_map(&:name)
    extra = blocking_orgs_count - names.size
    names << "and #{extra} more" if extra > 0
    message = names.to_sentence
  end

  def disable_submit_button?
    two_factor_requirement_form_disabled? ||
      business.updating_two_factor_requirement?
  end

  def two_factor_requirement_disabled_and_needs_confirmation?
    !two_factor_requirement_enabled? && business.affiliated_users_with_two_factor_disabled_exist?
  end

  def two_factor_requirement_enabled?
    business.two_factor_requirement_enabled?
  end

  def two_factor_required_setting_organizations_business_path
    urls.two_factor_required_organizations_settings_enterprise_path(business.slug)
  end

  def saml_provider_params
    @saml_provider_params ||= params.fetch(:saml, {})
  end

  def saml_testing_params
    @saml_testing_params ||= params.fetch(:saml_testing, {})
  end

  def current_external_identity
    @current_external_identity ||= params.fetch(:current_external_identity, nil)
  end

  def saml_settings_saved?
    business.saml_provider&.sso_url&.present? &&
      business.saml_provider&.idp_certificate&.present?
  end

  def show_saml_settings?
    saml_settings_saved? || saml_provider_params.present?
  end

  def show_recovery_prompt?
    !!business.saml_provider&.recovery_codes_available?
  end

  def sso_url
    saml_provider_params[:sso_url] || business.saml_provider&.sso_url&.to_s
  end

  def issuer
    saml_provider_params[:issuer] || business.saml_provider&.issuer
  end

  def idp_certificate
    saml_provider_params[:idp_certificate] || business.saml_provider&.idp_certificate&.to_s
  end

  def okta_team_sync?
    provider_type&.to_sym == :okta && GitHub.flipper[:okta_team_sync].enabled?(business)
  end

  def saml_testing?
    !!params[:test_settings]
  end

  def saml_test_failure?
    saml_testing? && saml_testing_params[:failure]
  end

  def saml_test_success?
    saml_testing? && saml_testing_params[:success]
  end

  def saml_test_errors
    return unless saml_testing?

    saml_testing_params[:message] || "invalid settings"
  end

  DEFAULT_SIGNATURE_METHOD = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"
  SIGNATURE_METHOD_OPTIONS = {
    "RSA-SHA1": "http://www.w3.org/2001/04/xmldsig-more#rsa-sha1",
    "RSA-SHA256": "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
    "RSA-SHA384": "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384",
    "RSA-SHA512": "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512",
  }

  def signature_method
    saml_provider_params[:signature_method] ||
      business.saml_provider&.signature_method&.to_s ||
      DEFAULT_SIGNATURE_METHOD
  end

  def signature_method_label
    SIGNATURE_METHOD_OPTIONS.key(signature_method) ||
      SIGNATURE_METHOD_OPTIONS.key(DEFAULT_SIGNATURE_METHOD)
  end

  def saml_signature_method_options
    SIGNATURE_METHOD_OPTIONS
  end

  DEFAULT_DIGEST_METHOD = "http://www.w3.org/2001/04/xmlenc#sha256"
  DIGEST_METHOD_OPTIONS = {
    "SHA1": "http://www.w3.org/2000/09/xmldsig#sha1",
    "SHA256": "http://www.w3.org/2001/04/xmlenc#sha256",
    "SHA384": "http://www.w3.org/2001/04/xmlenc#sha384",
    "SHA512": "http://www.w3.org/2001/04/xmlenc#sha512",
  }

  def digest_method
    saml_provider_params[:digest_method] ||
      business.saml_provider&.digest_method&.to_s ||
      DEFAULT_DIGEST_METHOD
  end

  def digest_method_label
    DIGEST_METHOD_OPTIONS.key(digest_method) ||
      DIGEST_METHOD_OPTIONS.key(DEFAULT_DIGEST_METHOD)
  end

  def saml_digest_method_options
    DIGEST_METHOD_OPTIONS
  end

  def saml_identity_provider_setting_organizations_business_path
    urls.saml_identity_provider_organizations_settings_enterprise_path(business.slug)
  end

  def show_ssh_cas?
    GitHub.ssh_enabled?
  end

  def show_ip_whitelisting?
    GitHub.ip_whitelisting_available?
  end

  def show_team_sync_settings?
    saml_settings_saved?
  end

  def supported_team_sync_provider?
    team_sync_provider_candidate&.supported_for_business?(business)
  end

  def disallow_team_sync_setup?
    current_external_identity.nil?
  end

  def enable_team_sync_settings?
    saml_settings_saved? &&
    supported_team_sync_provider?
  end

# TODO : Get teams in a business
  def team_sync_status
    "Teams managed in #{team_sync_provider_type_label}."
  end

  def team_sync_activity_link
    query = { q: "actor:github-team-synchronization[bot] action:team.add_member action:team.remove_member" }
    helpers.link_to("View activity", urls.settings_audit_log_enterprise_path(business, query))
  end

  IDENTITY_PROVIDER_OPTIONS = {
    "unknown" => "Unknown",
    "azuread" => "Azure AD",
    "okta" => "Okta",
  }

  def show_enable_team_sync_button?
    ::TeamSync::SetupFlow::STARTING_STATES.include?(team_sync_setup_flow.status) ||
      team_sync_setup_flow.status.nil?
  end

  def show_team_sync_assignment_review_available?
    ::TeamSync::SetupFlow::APPROVAL_STATE == team_sync_setup_flow.status
  end

  def team_sync_enabled?
    team_sync_setup_flow.tenant.team_sync_enabled?
  end

  def team_sync_provider_type_label
    IDENTITY_PROVIDER_OPTIONS[provider_type]
  end

  def team_sync_provider_id
    team_sync_setup_flow.provider_id
  end

  def idp_beta?
    team_sync_provider_candidate&.beta?
  end

  def show_orgs_with_saml_configured?
    !GitHub.private_instance_user_provisioning?
  end

  def saml_sso_url
    return urls.login_url(host: GitHub.host_name) if GitHub.private_instance_user_provisioning?
    urls.business_idm_sso_enterprise_url(business, host: GitHub.host_name)
  end

  def saml_sso_path
    return urls.login_path if GitHub.private_instance_user_provisioning?
    urls.business_idm_sso_enterprise_path(business.slug)
  end

  def saml_consume_url
    return urls.saml_consume_url(host: GitHub.host_name) if GitHub.private_instance_user_provisioning?
    urls.idm_saml_consume_enterprise_url(business, host: GitHub.host_name)
  end

  def saml_consume_path
    return urls.saml_consume_path if GitHub.private_instance_user_provisioning?
    urls.idm_saml_consume_enterprise_path(business.slug)
  end

  private

  # This is the potential team sync provider, based on the SAML issuer
  def team_sync_provider_candidate
    @team_sync_provider_candidate ||= ::TeamSync::Provider.detect(issuer: issuer)
  end

  # Derive type from SAML issuer (instead of tenants that may have
  # been created from older SAML configurations).
  def provider_type
    team_sync_provider_candidate&.type
  end

  def team_sync_setup_flow
    ::TeamSync::SetupFlow.new(business: business, actor: @current_user)
  end

  def organizations_blocking_enabling_two_factor
    business
      .organizations_can_enable_two_factor_requirement(false)
      .order("login ASC")
  end
end
