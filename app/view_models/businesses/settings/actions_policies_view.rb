# frozen_string_literal: true

class Businesses::Settings::ActionsPoliciesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business, :query, :current_page

  PAGE_SIZE = 25

  ALL_ENTITIES = ::Actions::PolicyResolver::ALL_ENTITIES
  SELECTED_ENTITIES = ::Actions::PolicyResolver::SELECTED_ENTITIES
  DISABLED = Configurable::ActionExecutionCapabilities::DISABLED

  ALL_ACTIONS = Configurable::ActionExecutionCapabilities::ALL_ACTIONS
  LOCAL_ACTIONS = Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS

  ALL_ACTIONS_FOR_SELECTED = Configurable::ActionExecutionCapabilities::ALL_ACTIONS_FOR_SELECTED
  LOCAL_ONLY_FOR_SELECTED = Configurable::ActionExecutionCapabilities::LOCAL_ONLY_FOR_SELECTED

  def action_policy_list
    [
      GitHub::Menu::ButtonComponent.new(
        text: "Enable for all organizations",
        description: "All organizations, including any created in the future, may use GitHub Actions.",
        replace_text: "Enable for all organizations",
        checked: ::Actions::PolicyChecker.enabled_for_all_entities?(current_policy),
        name: "policy",
        value: ALL_ENTITIES,
        type: "submit",
      ),
      GitHub::Menu::ButtonComponent.new(
        text: "Enable for specific organizations",
        description: "Only specifically-selected organizations may use GitHub Actions.",
        replace_text: "Enable for specific organizations",
        checked: selected_orgs_only?,
        name: "policy",
        value: SELECTED_ENTITIES,
        type: "submit",
      ),
      GitHub::Menu::ButtonComponent.new(
        text: "Disabled",
        description: "No organizations may use GitHub Actions.",
        replace_text: "Disabled",
        checked: disabled?,
        name: "policy",
        value: DISABLED,
        type: "submit",
      ),
    ]
  end

  def action_type_policy_list
    [
      GitHub::Menu::ButtonComponent.new(
        text: "No policy",
        description: "This delegates the decision to use local and third-party Actions to the individual organization administrators.",
        replace_text: "No policy",
        checked: disable_action_type_selection? || ::Actions::PolicyChecker.all_action_types_allowed?(current_policy),
        name: "policy",
        value: selected_orgs_only? ? ALL_ACTIONS_FOR_SELECTED : ALL_ACTIONS,
        disabled: disable_action_type_selection?,
        type: "submit",
      ),
      GitHub::Menu::ButtonComponent.new(
        text: "Enable local Actions only",
        description: "This allows organizations to execute any Action as long as the code for the Action exists within their own organization.",
        replace_text: "Enable local Actions only",
        checked: ::Actions::PolicyChecker.local_actions_only?(current_policy),
        name: "policy",
        value: selected_orgs_only? ? LOCAL_ONLY_FOR_SELECTED : LOCAL_ACTIONS,
        disabled: disable_action_type_selection?,
        type: "submit",
      ),
    ]
  end

  def current_policy
    @_current_policy ||= business.action_execution_capabilities
  end

  def show_org_list?
    selected_orgs_only?
  end

  def disable_action_type_selection?
    disabled?
  end

  def show_fork_pr_workflows_policy?
    !business.all_action_executions_disabled?
  end

  def orgs
    return @orgs if defined?(@orgs)

    orgs = Organization.
      includes(business_membership: [:business]).
      where(business_organization_memberships: { business_id: business.id }).
      order(login: :asc)


    if query
      orgs = orgs.where("login LIKE :query", { query: "%#{query}%" })
    end

    @orgs = orgs.paginate(page: current_page, per_page: PAGE_SIZE)
  end

  def actions_allowed?(organization)
    allowed_orgs.include?(organization.id)
  end

  private

  def selected_orgs_only?
    ::Actions::PolicyChecker.enabled_for_selected_entities_only?(current_policy)
  end

  def disabled?
    ::Actions::PolicyChecker.disabled?(current_policy)
  end

  def allowed_orgs
    return @_allowed_orgs if defined?(@_allowed_orgs)
    @_allowed_orgs = business.actions_allowed_entities
  end
end
