# frozen_string_literal: true

class Businesses::BillingSettings::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include GitHub::Application.routes.url_helpers

  attr_reader :business

  delegate :organizations, to: :business

  def organizations_asset_statuses
    @_organizations_asset_statuses ||= organizations.each_with_object([]) do |organization, statuses|
      asset_status = organization.asset_status || organization.build_asset_status
      next if asset_status.bandwidth_usage.zero? && asset_status.storage_usage.zero?

      statuses << AssetStatusUsage.new(
        organization,
        asset_status: asset_status,
      )
    end
  end

  def number_of_organizations_without_asset_usage
    @_number_of_organizations_without_asset_usage ||= organizations.count - organizations_asset_statuses.count
  end

  def organizations_actions_usage
    @_organizations_actions_usage ||= organizations.each_with_object([]) do |organization, usages|
      usage = Billing::ActionsUsage.new(business, owner: organization)
      next if usage.total_private_minutes_used.zero?

      usages << ActionsUsage.new(
        organization,
        usage: usage,
      )
    end
  end

  def number_of_organizations_without_actions_usage
    @_number_of_organizations_without_actions_usage ||=  organizations.count - organizations_actions_usage.count
  end

  def organizations_packages_usage
    @_organizations_packages_usage ||= organizations.each_with_object([]) do |organization, usages|
      usage = Billing::PackageRegistryUsage.new(business, owner: organization)
      next if usage.paid_gigabytes_used.zero?

      usages << PackageUsage.new(organization, usage: usage)
    end
  end

  def number_of_organizations_without_packages_usage
    @_number_of_organizations_without_packages_usage ||=  organizations.count - organizations_packages_usage.count
  end

  def organizations_storage_usage
    @_organizations_storage_usage ||= organizations.each_with_object([]) do |organization, usages|
      usage = Billing::SharedStorageUsage.new(business, owner: organization)
      next if usage.estimated_monthly_private_megabytes.zero?

      usages << SharedStorageUsage.new(organization, usage: usage)
    end
  end

  def number_of_organizations_without_storage_usage
    @_number_of_organizations_without_storage_usage ||=  organizations.count - organizations_storage_usage.count
  end

  def invoices
    @_invoices ||= Billing::ZuoraInvoice.invoices_for_account(business.customer.zuora_account_id)
      .select(&:posted?)
      .reject(&:suppress_from_customer_view?)
      .sort_by(&:invoice_date)
      .reverse
  rescue Zuorest::HttpError, Faraday::Error
    @_invoices = []
  end

  def invoice
    @_invoice ||= invoices.first
  end

  def show_remaining_actions_balance?
    !GitHub.single_business_environment? && business.pays_github_directly? && prepaid_usage_calculator.total_purchased_refills_in_cents > 0
  end

  def refill_amount_remaining
    Billing::Money.new(prepaid_usage_calculator.amount_remaining_in_cents)
  end

  def total_purchased_refills
    Billing::Money.new(prepaid_usage_calculator.total_purchased_refills_in_cents)
  end

  def show_warning?
    prepaid_usage_calculator.amount_remaining_in_cents < 0 && !show_alert?
  end

  def show_alert?
    prepaid_usage_calculator.over_prepaid_limit?
  end

  def refill_used_percentage
    prepaid_usage_calculator.percent_used
  end

  def included_actions_private_minutes_used
    actions_usage.included_private_minutes_used
  end

  def included_actions_private_minutes
    actions_usage.included_private_minutes
  end

  def package_registry_used_bandwidth
    package_registry_usage.paid_gigabytes_used
  end

  def package_registry_included_bandwidth
    business.plan.package_registry_included_bandwidth
  end

  def shared_storage_included_gigabytes
    to_gigabytes(business.plan.shared_storage_included_megabytes)
  end

  def shared_storage_estimated_monthly_private_gigabytes
    to_gigabytes(Billing::SharedStorageUsage.new(business).estimated_monthly_private_megabytes)
  end

  def navigation_tabs
    @_tabs ||= begin
      tabs = [{ name: "Usage", path: settings_billing_enterprise_path(business) }]
      tabs << { name: "Cost Management", path: settings_billing_tab_enterprise_path(business, :cost_management) } if show_cost_management_tab?
      tabs << { name: "Past Invoices", path: settings_billing_tab_enterprise_path(business, :past_invoices) } if show_past_invoices_tab?
      tabs
    end
  end

  def actions_and_packages_budget
    @_actions_and_packages_budget ||= business.budget_for(product: :shared)
  end

  def codespaces_enabled?
    GitHub.flipper[:billing_github_codespaces_for_orgs].enabled?(business)
  end

  def codespaces_budget
    @_codespaces_budget ||= business.budget_for(product: :codespaces)
  end

  def show_alert_for_missing_azure_id?
    agreement = business.enterprise_agreements.active.first
    agreement.present? && agreement.azure_subscription_id.nil?
  end

  def missing_azure_id_message
    "Usage of GitHub Actions, Packages, and Shared Storage past your included allotments is not set up for your account. \
    Contact sales if you wish to enable this feature."
  end

  def show_invoice_overview?
    !business.reseller_customer? && business.invoiced? && invoice.present?
  end
  alias_method :show_past_invoices_tab?, :show_invoice_overview?

  def show_cost_management_tab?
    show_metered_billing_configuration? || show_billing_privileges?
  end

  def show_billing_privileges?
    business.owner?(current_user) && business.can_self_serve? && !business.invoiced?
  end

  def show_metered_billing_configuration?
    Billing::Budget.configurable?(business)
  end

  private

  def package_registry_usage
    @_package_registry_usage ||= Billing::PackageRegistryUsage.new(business)
  end

  def actions_usage
    @_actions_usage ||= Billing::ActionsUsage.new(business)
  end

  def shared_storage
    @_shared_storage ||= Billing::SharedStorageUsage.new(business)
  end

  def to_gigabytes(megabytes)
    (megabytes.megabytes / 1.gigabyte.to_f).round(2)
  end

  def prepaid_usage_calculator
    @_prepaid_usage_calculator ||= Billing::PrepaidUsageCalculator.new(business)
  end
end
