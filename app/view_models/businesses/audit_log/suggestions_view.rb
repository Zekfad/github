# frozen_string_literal: true

class Businesses::AuditLog::SuggestionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business

  def actions
    AuditLogEntry.business_action_names
  end

  def users
    @users ||= if GitHub.single_business_environment?
      User.where(type: "User")
    else
      User.none
    end
  end

  def organizations
    @organizations ||= if GitHub.single_business_environment?
      Organization.all
    else
      Organization.none
    end
  end
end
