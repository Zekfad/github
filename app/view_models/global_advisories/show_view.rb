# frozen_string_literal: true

module GlobalAdvisories
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    areas_of_responsibility :security_advisories

    attr_reader :advisory

    def repository_advisory
      advisory.repository_advisory
    end

    def has_readable_repository_advisory?
      return @has_readable_repository_advisory if defined? @has_readable_repository_advisory

      @has_readable_repository_advisory = advisory.repository_advisory.present? &&
        advisory.repository_advisory.readable_by?(current_user)
    end

    def repository
      repository_advisory&.repository
    end

    def has_repository?
      has_readable_repository_advisory? && repository.present?
    end

    def advisory_title
      if advisory.summary.present?
        advisory.summary
      else
        helpers.truncate(advisory.description, length: 60)
      end
    end

    def advisory_description
        advisory.description_with_references
    end

    def render_description
      GitHub::Goomba::MarkdownPipeline.to_html(advisory_description)
    rescue EncodingError, TypeError => error
      Failbot.report(error, advisory: advisory.id, body: advisory_description)
      nil
    end

    def safe_repository_advisory_publisher
      @safe_repository_advisory_publisher ||= repository_advisory.publisher || User.ghost
    end

    def first_vulnerable_version_range
      advisory.vulnerable_version_ranges.first
    end

    def grouped_vulnerable_verson_ranges
      advisory.vulnerable_version_ranges.group_by { |range|
        [range.ecosystem, range.affects]
      }.sort_by { |(ecosystem, affects), ranges|
        affects
      }
    end

    def show_credits?
      return @show_credits if defined? @show_credits

      @show_credits =
        GitHub.flipper[:advisory_credits_ui].enabled? &&
          credits.any?
    end

    def credits
      return @credits if @credits

      advisory_credits = advisory.credits.accepted.preload(:recipient)

      @credits = RepositoryAdvisories::CreditView.for_advisory_credits(advisory_credits, current_user: current_user, viewer_can_manage: false)
    end
  end
end
