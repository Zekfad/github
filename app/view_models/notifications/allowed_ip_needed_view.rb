# frozen_string_literal: true

module Notifications
  class AllowedIpNeededView
    attr_reader :list

    # Represents a notifications list.
    #
    # repository - A notifications list ActiveRecord object (e.g. Repository, Team).
    # total_notifications - An Integer number of total notifications for this list.
    def initialize(list, total_notifications)
      @list = list
      @total_notifications = total_notifications
    end

    # Public: Returns the String path for the list.
    def name_with_owner
      @list.name_with_owner
    end

    # Public: Returns the Integer number of total notifications for the list.
    def total
      @total_notifications
    end

    def organization
      list.owner
    end
  end
end
