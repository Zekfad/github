# frozen_string_literal: true

module Notifications
  class ThreadSubscriptionView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ThreadSubscriptionMethods
    attr_reader :list
    attr_reader :thread
    attr_reader :display_explanation_text

    def show_notifications_header?
      # Not shown for issues and PRs because #custom_notifications_supported_for_thread? is true, so we show the
      # header plus the 'Customize' button. Not shown for gists and commits because they're not displayed in a
      # sidebar where a 'Notifications' header makes sense.
      thread.is_a?(Discussion)
    end

    def websocket_url
      @attributes[:websocket_url] || urls.notifications_thread_subscription_path(
        repository_id: list_id,
        thread_id: thread_id,
        thread_class: thread_class,
      )
    end

    def form_path
      @attributes[:form_path] || urls.notifications_thread_subscribe_path
    end
  end
end
