# frozen_string_literal: true

class Notifications::CheckSuiteConclusionView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  GREEN = "open".freeze
  RED = "closed".freeze
  NEUTRAL = "none".freeze
  RED_STATES = StatusCheckRollup::FAILURE_STATES + StatusCheckRollup::INCOMPLETE_STATES - [StatusCheckRollup::STALE]
  NEUTRAL_STATES = [StatusCheckRollup::NEUTRAL, StatusCheckRollup::SKIPPED, StatusCheckRollup::STALE]

  def initialize(conclusion)
    @conclusion = conclusion.downcase
  end

  def adjective
    StatusCheckRollup.adjective_state(conclusion)
  end

  # We're just recycling existing class names here from SummaryView to get the
  # appropriate green, red, or neutral coloring for the notification icon.
  def summary_state
    if conclusion == StatusCheckRollup::SUCCESS
      GREEN
    elsif RED_STATES.include?(conclusion)
      RED
    else
      NEUTRAL
    end
  end

  def text_color
    case conclusion
    when StatusCheckRollup::SUCCESS
      "text-green"
    when StatusCheckRollup::ACTION_REQUIRED, StatusCheckRollup::FAILURE, StatusCheckRollup::TIMED_OUT
      "text-red"
    else
      "text-gray-light"
    end
  end

  # Uses the same icons as we'd expect to see on the Checks pages.
  def icon
    case conclusion
    when StatusCheckRollup::NEUTRAL
      "primitive-square"
    when StatusCheckRollup::STALE
      "issue-reopened"
    when StatusCheckRollup::SUCCESS
      "check"
    when StatusCheckRollup::FAILURE, StatusCheckRollup::TIMED_OUT
      "x"
    when StatusCheckRollup::CANCELLED
      "stop"
    when StatusCheckRollup::ACTION_REQUIRED
      "alert"
    when StatusCheckRollup::SKIPPED
      "skip"
    end
  end

  private

  attr_reader :conclusion
end
