# frozen_string_literal: true

module Notifications
  class RepositoryView < ListView

    # Overrides Notifications::ListView#list_owner.
    def list_owner
      @list.owner
    end

    # Overrides Notifications::ListView#more_url.
    def more_url
      query = {page: :next} if current_list?
      @index_view.build_url(@list, query)
    end
  end
end
