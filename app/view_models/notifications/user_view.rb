# frozen_string_literal: true

module Notifications
  class UserView < ListView
    # Overrides Notifications::ListView#name.
    def name
      @list.login
    end

    # Overrides Notifications::ListView#list_owner.
    def list_owner
      # A user doesn't have an owner, so we return the user.
      @list
    end

    # Overrides Notifications::ListView#more_url.
    def more_url
      query = { list_type: list_type }
      query[:page] = :next if current_list?
      @index_view.build_url(@list, query)
    end
  end
end
