# frozen_string_literal: true

module Issues
  class FormButtonsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlatformHelper

    attr_reader :issue, :current_user, :issue_node, :addl_btn_classes

    FormButtonClosable = parse_query <<-'GRAPHQL'
      fragment on IssueOrPullRequest {
        ... on Closable {
          closed
          viewerCanClose
        }
      }
    GRAPHQL

    def initialize(**args)
      super(args)
      @issue = args[:issue]
      @current_user = args[:current_user]
      @issue_node = FormButtonClosable.new(args[:issue_node])
      @addl_btn_classes = args[:addl_btn_classes] || []
    end

    def pull_request?
      issue_node.is_a?(PlatformTypes::PullRequest)
    end

    def show_close_button?
      !issue_node.closed? &&
        issue_node.viewer_can_close?
    end

    def show_reopen_button?
      issue_node.closed?
    end

    def can_reopen?
      issue.reopenable_by?(current_user)
    end

    def cannot_reopen_with_reason?
      pull_request? &&
        issue.reopenable_by?(current_user, issue_only: true) &&
        not_reopenable_reason
    end

    def not_reopenable_reason
      return unless pull_request?
      return @reason if defined?(@reason)
      @reason = issue.pull_request.not_reopenable_reason
    end

    def noun
      if pull_request?
        "pull request"
      else
        "issue"
      end
    end

    def btn_classes(custom_classes: [])
      classes = base_btn_classes + addl_btn_classes
      classes += custom_classes
      classes.join(" ")
    end

    def base_btn_classes
      ["btn", "js-comment-and-button", "js-quick-submit-alternative"]
    end
  end
end
