# frozen_string_literal: true

module RepositoryAdvisories
  class IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :current_repository, :page

    PAGE_SIZE = 10

    # If the current_user is not authorized to manage advisories
    # we coerce the view state to "published" as that is the only
    # state they are ever permitted to view.
    def state
      @state ||= (collaboration_authorized? && attributes[:state]) || "published"
    end

    def advisories(per_page: PAGE_SIZE)
      current_advisories.paginate(page: page, per_page: per_page)
    end

    def draft?
      state == "draft"
    end

    def published?
      state == "published"
    end

    def closed?
      state == "closed"
    end

    def state_icon
      case state
      when "draft" then "shield"
      when "closed" then "shield-x"
      when "published" then "shield-check"
      end
    end

    def total_draft_count
      @total_draft_count ||= available_advisories.open.count
    end

    def total_published_count
      @total_published_count ||= available_advisories.published.count
    end

    def total_closed_count
      @total_closed_count ||= available_advisories.closed.count
    end

    def advisory_management_authorized?
      return @advisory_management_authorized if defined? @advisory_management_authorized

      @advisory_management_authorized =
        current_repository.advisory_management_authorized_for?(current_user)
    end

    alias_method :show_new_button?, :advisory_management_authorized?

    def collaboration_authorized?
      return @collaboration_authorized if defined? @collaboration_authorized

      @collaboration_authorized = advisory_management_authorized? ||
        total_draft_count > 0 ||
        total_closed_count > 0
    end

    alias_method :show_draft?, :collaboration_authorized?
    alias_method :show_closed?, :collaboration_authorized?

    private

    def current_advisories
      if draft?
        available_advisories.open
      elsif published?
        available_advisories.published
      elsif closed?
        available_advisories.closed
      else
        RepositoryAdvisory.none
      end
    end

    def available_advisories
      @available_advisories ||= current_repository.repository_advisories.
        available_to(current_user).
        newest_first
    end
  end
end
