# frozen_string_literal: true

module RepositoryAdvisories
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repository, :advisory, :form_advisory

    delegate :title,
             :ghsa_id,
             :author,
             :publisher,
             :created_at,
             :publishable?,
             :form_filled_out?,
             :published?,
             :published_at,
             :closed?,
             :closed_at,
             :recently_touched_branches,
             :pull_requests,
             :workspace_repository,
             :cve_id,
             :cve_url,
             :package,
             :ecosystem,
             :workspace_clean?,
             :cve_request_locked?,
             :cve_request_unlocks_at,
             :cve_request_pending?,
             to: :advisory

    def admin?
      return @is_admin if defined? @is_admin
      @is_admin = advisory.adminable_by?(current_user)
    end

    def collaborator?
      return @is_collaborator if defined? @is_collaborator
      @is_collaborator = advisory.writable_by?(current_user)
    end

    def readable?
      return @is_readable if defined? @is_readable
      @is_readable = advisory.readable_by?(current_user)
    end

    def draft?
      advisory.open?
    end

    def affected_versions
      return "Required" if missing_affected_versions?

      advisory.affected_versions
    end

    def missing_affected_versions?
      advisory.affected_versions.blank?
    end

    def missing_description?
      advisory.description.blank?
    end

    def description
      return @description if defined? @description

      @description = begin
        context = { current_user: current_user, entity: repository }
        context[:organization] = organization if organization
        html_content = GitHub::Goomba::MarkdownPipeline.to_html(advisory.description, context)
        html_content = GitHub::Goomba::NoReferrerPipeline.to_html(html_content) unless published?
        html_content.html_safe  # rubocop:disable Rails/OutputSafety
      rescue EncodingError, TypeError => e
        Failbot.report(e, repository: repository.id, advisory: advisory.id)
        nil
      end
    end

    # We used to treat the RepositoryAdvisory#body attribute like a comment,
    # and so sometimes we have to check for it, eg see repos/advisories/_body
    def body_present?
      !advisory.body.nil?
    end

    def body
      return @body if defined? @body

      @body = begin
        context = { current_user: current_user, entity: repository }
        context[:organization] = organization if organization
        html_content = GitHub::Goomba::MarkdownPipeline.to_html(advisory.body, context)
        html_content = GitHub::Goomba::NoReferrerPipeline.to_html(html_content) unless published?
        html_content.html_safe  # rubocop:disable Rails/OutputSafety
      rescue EncodingError, TypeError => e
        Failbot.report(e, repository: repository.id, advisory: advisory.id)
        nil
      end
    end

    def severity
      @severity ||= advisory.severity
    end

    def state
      advisory.state == "open" ? "draft" : advisory.state
    end

    def patches
      return "None" if no_patches?

      advisory.patches
    end

    def no_patches?
      advisory.patches.blank?
    end

    def state_icon
      case state
      when "published" then "check"
      when "closed" then "x"
      else "git-pull-request"
      end
    end

    def state_badge_icon
      case state
      when "draft" then "shield"
      when "closed" then "shield-x"
      when "published" then "shield-check"
      else "shield"
      end
    end

    def state_text_color
      case state
      when "draft" then "gray-light"
      when "closed" then "red"
      when "published" then "green"
      end
    end

    def state_color
      case state
      when "draft" then "default"
      when "closed" then "red"
      when "published" then "green"
      end
    end

    def workspace
      advisory.workspace_repository
    end

    def workspace_creating?
      workspace.creating?
    end

    def workspace_path
      urls.advisory_workspace_path(repository.owner, repository, advisory)
    end

    def default_url
      workspace.http_url
    end

    def viewer_authored?
      current_user ? current_user.id == advisory.author_id : false
    end

    def viewer_published?
      current_user ? current_user.id == advisory.publisher_id : false
    end

    def pending_credit_for_viewer
      return nil unless current_user
      return @pending_credit if defined? @pending_credit

      @pending_credit = advisory.credits.pending.find_by(recipient_id: current_user.id)
    end

    def timeline_items
      @timeline_items ||= advisory.timeline_for(current_user).select do |item|
        # Let's make doubly sure that the current user can view these timeline
        # items before sending them to the view.
        item.readable_by?(current_user)
      end
    end

    def admin_users
      @admin_users ||= repository.members(action: :admin)
    end

    def unique_collaborating_users
      @unique_collaborating_users ||= advisory.collaborating_users - admin_users - [advisory.owner]
    end

    def admin_teams
      return @admin_teams if defined?(@admin_teams)
      return [] unless organization

      @admin_teams = organization.visible_teams_for(current_user).where(id: Ability.where(
        actor_type: "Team",
        subject_id: repository.id,
        subject_type: "Repository",
        priority: Ability.priorities[:direct],
        action: Ability.actions[:admin],
      ).pluck(:actor_id))
    end

    def unique_collaborating_teams
      @unique_collaborating_teams ||= advisory.collaborating_teams - admin_teams
    end

    def organization
      return advisory.owner if advisory.owner.organization?

      nil
    end

    def show_credit_action_button?(timeline_item)
      return false unless current_user
      return false unless timeline_item == latest_credit_event_for_viewer

      viewer_credit = advisory.credits.find_by(recipient_id: current_user.id)
      return false unless viewer_credit

      (timeline_item.credit_accepted? && viewer_credit.accepted?) ||
        (timeline_item.credit_declined? && viewer_credit.declined?)
    end

    def show_workspace_box?
      draft? && collaborator?
    end

    def show_merge_box?
      draft? && collaborator? && workspace.present?
    end

    def show_publish_box?
      collaborator? && (draft? || cve_requestable?)
    end

    def cve_requestable?
      !closed? && form_filled_out? && cve_id.blank? && !cve_request_locked?
    end

    def locked_advisory?
      advisory.cve_request_locked?
    end

    # Viewer permissions
    #
    # The methods below use viewer_may_* and viwer_can_* naming.
    #
    # A viewer_may_* method signifies that the current user is *allowed* to
    # perform an action, regardless of whether that action is currently
    # possible.
    #
    # A viewer_can_* method typically combines the current_user's permission
    # (viewer_may_*) with a check of the data state.
    #
    # For example, it's possible that the current user *may* publish the
    # advisory, but the user *cannot* actually publish the advisory because we
    # haven't collected all of the required information yet. The fact that the
    # user *may* publish allows us to show the publish button. The fact that
    # they *cannot* means that button will be disabled.

    def viewer_can_manage_collaborators?
      admin?
    end

    def viewer_can_create_workspace?
      workspace.blank? && viewer_may_create_workspace?
    end

    def viewer_may_create_workspace?
      admin?
    end

    def viewer_can_publish?
      publishable? && viewer_may_publish?
    end

    def viewer_may_publish?
      admin?
    end

    def viewer_can_request_cve?
      cve_requestable? && viewer_may_request_cve?
    end

    def viewer_may_request_cve?
      admin?
    end

    def show_comment_and_close_box?
      viewer_can_comment? || viewer_can_close? || viewer_can_reopen?
    end

    def viewer_can_comment?
      !published? && viewer_may_comment?
    end

    def viewer_may_comment?
      collaborator?
    end

    def viewer_can_close?
      draft? && viewer_may_close?
    end

    def viewer_may_close?
      admin?
    end

    def viewer_can_reopen?
      closed? && viewer_may_reopen?
    end

    def viewer_may_reopen?
      admin?
    end

    def show_github_review_info?
      !closed? && collaborator?
    end

    def viewer_can_edit_title?
      !locked_advisory? && viewer_may_edit_title?
    end

    def viewer_may_edit_title?
      collaborator?
    end

    def show_state_badge?
      collaborator?
    end

    def show_comment_count?
      collaborator? || comment_count > 0
    end

    def comment_count
      return @comment_count if defined?(@comment_count)
      @comment_count =
        timeline_items.count { |item| item.is_a?(RepositoryAdvisoryComment) }

      # Remove whenever we migrate away RepositoryAdvisory#body comments.
      if body_present?
        @comment_count += 1
      end

      @comment_count
    end

    def viewer_may_manage_credits?
      return @viewer_may_manage_credits if defined? @viewer_may_manage_credits

      @viewer_may_manage_credits =
        GitHub.flipper[:advisory_credits_ui].enabled?(repository) &&
          collaborator?
    end

    def show_credits?
      return @show_credits if defined? @show_credits

      @show_credits =
        GitHub.flipper[:advisory_credits_ui].enabled?(repository) &&
          credits.any?
    end

    def credits
      return @credits if @credits

      advisory_credits = advisory.credits.preload(:recipient)
      advisory_credits = advisory_credits.accepted unless viewer_may_manage_credits?

      @credits = CreditView.for_advisory_credits(advisory_credits, current_user: current_user, viewer_can_manage: viewer_may_manage_credits?)
    end

    def form_credits
      @form_credits ||= CreditView.for_advisory_credits(form_advisory.credits, current_user: current_user, viewer_can_manage: viewer_may_manage_credits?)
    end

    def link_to_global_advisory?
      GitHub.global_advisories_enabled? && advisory.vulnerability&.globally_available?
    end

    def publish_url
      urls.publish_repository_advisory_path(repository.owner, repository, advisory)
    end

    def request_cve_url
      urls.request_cve_repository_advisory_path(repository.owner, repository, advisory)
    end

    def show_edit_form_on_load?
      form_advisory.errors.any?
    end

    def after_initialize
      @form_advisory = @advisory

      # The page to see an advisory has its details as well as a form to edit
      # those values. When you submit an invalid edit (e.g. with an invalid
      # field), the view has to discern between the original valid data (that
      # appears in the details) and the modified invalid data (that appears in
      # the edit form, using @form_advisory).
      if @advisory.persisted? && @advisory.errors.any?
        @advisory = RepositoryAdvisory.find(@advisory.id)
      end
    end

    def show_advisory_credits_feature_popover?
      logged_in? && collaborator? && !current_user.dismissed_notice?("feature_repository_advisory_credits")
    end

    private

    def credit_event_for_viewer?(timeline_item)
      timeline_item.is_a?(RepositoryAdvisoryEvent) &&
        timeline_item.credit_event? &&
        current_user&.id == timeline_item.actor_id
    end

    def latest_credit_event_for_viewer
      return nil unless current_user

      if defined? @latest_credit_event_for_viewer
        return @latest_credit_event_for_viewer
      end

      @latest_credit_event_for_viewer =
        timeline_items.reverse_each.detect do |timeline_item|
          credit_event_for_viewer?(timeline_item)
        end
    end
  end
end
