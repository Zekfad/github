# frozen_string_literal: true

module Branches
  class ListView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repository
    attr_reader :search_query

    def initialize(*)
      super

      eager_load!
    end

    def template_name
      raise NotImplementedError
    end

    def search_mode?
      search_query.present?
    end

    def all_branches
      raise NotImplementedError
    end

    def can_push?
      return @can_push if defined?(@can_push)

      @can_push = logged_in? && repository.pushable_by?(current_user)
    end

    def nav_link(label, view_name, options)
      options = options.dup
      options[:class] ||= ""
      options[:class] += " selected" if view_name == selected_view

      helpers.link_to label,
        urls.branches_path(repository, view: view_name),
        options
    end

    def search_path
      urls.branches_path(repository, view: :all)
    end

  private
    attr_reader :head_commits, :author_email_map, :pull_requests, :branch_has_open_pull_as_base

    def eager_load!
      branches = all_branches

      @head_commits = load_head_commits(branches)
      @ahead_behind = load_ahead_behind(head_commits)
      @author_email_map = load_authors(head_commits.values)
      Commit.prefill_combined_statuses(head_commits.values, repository)
      @pull_requests = load_pull_requests(branches)
      @branch_has_open_pull_as_base = load_branch_has_open_pull_as_base(branches)
    end

    def base_branch
      @base_branch ||= begin
        name = repository.default_branch
        if b = head_commits[name.b]
          b.oid
        else
          repository.heads.find(name.b).sha
        end
      end
    end

    def load_ahead_behind(commits)
      others = commits.values.map(&:oid)
      others.delete(base_branch)

      repository.rpc.ahead_behind(base_branch, others)
    end

    def ahead_behind(oid)
      (oid == base_branch) ? [0, 0] : @ahead_behind[oid]
    end

    def load_head_commits(branches)
      refs = branches.map(&:ref)
      head_commits_by_sha = repository.commits.find(refs.map(&:sha)).index_by(&:sha)
      Hash[refs.map { |ref| [ref.ref, head_commits_by_sha[ref.sha]] }]
    end

    def load_authors(commits)
      emails = commits.map(&:author_email).uniq
      emails = emails.reject { |email| UserEmail.generic_domain?(email) }
      User.find_by_emails(emails)
    end

    def load_pull_requests(branches)
      PullRequest.
        includes(:issue).
        where(
          head_repository_id: repository.id,
          base_repository_id: repository.id,
          head_ref: branches.flat_map { |branch|
            # github/github ref names are fully qualified. Pull requests in
            # other repos omit the "refs/heads/" prefix. See #20196.
            [branch.ref.name, branch.ref.qualified_name]
          },
        ).select { |pull|
        pull.head_sha == head_commits[pull.raw_head_ref_name].oid
      }.index_by { |pull|
        pull.head_ref_name
      }
    end

    def load_branch_has_open_pull_as_base(branches)
      # Remove the default branch, if present, as the query below has
      # poor performance for branches with large cardinality in the table
      # and the default branch isn't deletable, anyway.
      branches = branches.dup
      branches.reject! { |branch| branch.ref.default_branch? }
      return {} if branches.empty?

      # Remove any protected branches, as they are likely to be have high
      # cardinality in the table as base branches and the query below has
      # poor performance for branches with large cardinality and protected
      # branches aren't deletable, anyway.
      protected_branches = ProtectedBranch.for_repository_with_branch_names(
        repository,
        branches.map(&:ref).map(&:name)
      )
      protected_branches.transform_keys!(&:b)
      branches.reject! { |branch| protected_branches[branch.ref.name.b] }
      return {} if branches.empty?

      refs = branches.map { |branch| GitHub::SQL::BINARY(branch.ref.name) }
      refs_as_base = PullRequest.github_sql.results <<-SQL, repository_id: repository.id, refs: refs
        SELECT pr.base_ref
        FROM pull_requests pr
        JOIN issues i
          ON pr.id = i.pull_request_id
          AND i.state = 'open'
        WHERE
          pr.head_repository_id = :repository_id
          AND pr.base_repository_id = :repository_id
          AND pr.base_ref IN :refs
          AND pr.user_hidden = 0
        GROUP BY pr.base_ref
      SQL
      refs_as_base.flatten!
      trues = Array.new(refs_as_base.count, true)
      Hash[refs_as_base.zip(trues)]
    end

    def create_item_views(branches)
      branches.map { |branch| create_item_view(branch) }
    end

    def create_item_view(branch)
      commit = head_commits.fetch(branch.ref.name)
      user = author_email_map.fetch(commit.author_email, nil)
      pull = pull_requests.fetch(branch.ref.name, nil)
      has_pull_as_base = branch_has_open_pull_as_base.fetch(branch.ref.name, false)
      ab = ahead_behind(commit.oid)
      max_diverged = @ahead_behind.map { |_, (ahead, behind)| ahead + behind }.max

      BranchView.new(
        ref: branch.ref,
        commit: commit,
        user: user,
        repository: repository,
        pull_request: pull,
        has_open_pull_request_as_base: has_pull_as_base,
        can_push: can_push?,
        ahead_behind: ab,
        current_user: current_user,
        max_diverged: max_diverged,
      )
    end
  end
end
