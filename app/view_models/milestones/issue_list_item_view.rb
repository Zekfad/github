# frozen_string_literal: true

module Milestones
  class IssueListItemView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :issue

    # The PR or the issue object for this item.
    def subject
      issue.pull_request? ? issue.pull_request : issue
    end

    # Which type of icon should we display for this particular issue?
    #
    # Returns a String.
    def icon
      if issue.state == "locked"
        "lock"
      elsif issue.pull_request
        "git-pull-request"
      elsif issue.open?
        "issue-opened"
      else
        "issue-closed"
      end
    end

    def type_name
      if issue.pull_request?
        "pull request"
      else
        "issue"
      end
    end

    def status
      if issue.pull_request? && issue.pull_request.merged?
        "merged"
      else
        issue.open? ? "open" : "closed"
      end
    end

    # Does this item have a task list associated with it?
    #
    # Returns a Boolean.
    def task_list?
      subject.task_list?
    end

    def comment_count
      if issue.pull_request?
        issue.pull_request.total_comments
      else
        issue.issue_comments_count
      end
    end

    def comments?
      !comment_count.zero?
    end
  end
end
