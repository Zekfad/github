# frozen_string_literal: true

module Settings
  class OrgsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

    # Public: Return a Membership for each associated organization, collapsing
    # multiple assocations for one organization into a single Membership and
    # preferring the membership that includes the most information.
    #
    # Returns an Array of memberships.
    def memberships
      repo_count_by_organization_id = ::Repository.with_organization.where({
        id: current_user.associated_repository_ids(including: [:direct]),
      }).group(:organization_id).count

      organizations_by_id = ::Organization.where(id: repo_count_by_organization_id.keys).index_by(&:id)

      memberships = {}

      repo_count_by_organization_id.each do |organization_id, count|
        organization = organizations_by_id[organization_id]
        memberships[organization] = count if organization
      end

      cumulative = memberships.map { |organization, repository_count|
        Membership.new(current_user, organization, repository_count: repository_count)
      } + current_user.organizations.map { |organization|
        Membership.new(current_user, organization)
      } + current_user.billing_manager_organizations.map { |organization|
        Membership.new(current_user, organization, role: :billing_manager)
      }

      cumulative.reject { |membership| membership.organization.deleted? }.uniq { |membership| membership.name }.sort_by(&:name)
    end

    # Provides a membership description and delete confirmation method for a
    # user and organization assocation as well as the organization, number of
    # direct repo memberships in the organization, and organization name.
    class Membership
      include GitHub::Application.routes.url_helpers

      def initialize(user, organization, repository_count: 0, role: nil)
        @user = user
        @repository_count = repository_count
        @organization = organization
        @name = organization.login
        @role = role || determine_role
      end

      attr_reader :organization, :repository_count, :name, :user, :role

      def description
        case role
        when :owner
          "owner"
        when :member
          "member"
        when :outside_collaborator
          "outside collaborator on #{repository_count} #{repository_count > 1 ? 'repositories' : 'repository'}"
        when :member_and_outside_collaborator
          "member and collaborator on #{repository_count} #{repository_count > 1 ? 'repositories' : 'repository'}"
        when :billing_manager
          "billing manager"
        end
      end

      def delete_confirmation_message
        message = ["Are you positive you want to leave #{organization}?"]

        message << \
          case role
          when :owner, :member, :member_and_outside_collaborator
            "You will lose access to all repositories and teams."
          when :outside_collaborator
            "You will lose access to all repositories."
          when :billing_manager
            "You will lose access to billing settings."
          end

        message.join(" ")
      end

      def org_settings_path
        case role
        when :owner
          settings_org_profile_path(organization)
        when :billing_manager
          settings_org_billing_path(organization)
        end
      end

      def show_settings_button?
        role == :owner || role == :billing_manager
      end

      private

      def determine_role
        return :owner if organization.adminable_by?(user)
        return :member if repository_count == 0

        if organization.direct_or_team_member?(user)
          :member_and_outside_collaborator
        else
          :outside_collaborator
        end
      end
    end
  end
end
