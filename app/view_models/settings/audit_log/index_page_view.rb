# frozen_string_literal: true

module Settings
  module AuditLog
    class IndexPageView < ::AuditLog::IndexPageView
      def page_title
        "Security log"
      end

      def tips
        @tips ||= Settings::AuditLog::TipView.new(user: current_user)
      end

      def search_path(options = {})
        query = if options.present?
          { q: build_query_param(options) }
        else
          {}
        end

        urls.settings_user_audit_log_path(query)
      end

      def suggestions_path
        urls.settings_user_audit_log_suggestions_path
      end

      def export_path
        urls.settings_user_audit_log_export_path(format: :json)
      end

      def filters_partial
        "settings/audit_log/search_filters"
      end

      def normalize(entries)
        AuditLogEntry.for_users(entries)
      end

      private

      # Private: The organization scoped audit log ElasticSearch query.
      #
      # Returns Hash
      def es_query
        {
          current_user: current_user,
          actor_id: current_user.id,
          user_id: current_user.id,
          phrase: query,
          aggregations: false,
          page: page,
          after: after,
          before: before,
        }
      end

      def search_query
        @search_query ||= Audit::Driftwood::Query.new_user_query(es_query)
      end
    end
  end
end
