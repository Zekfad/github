# frozen_string_literal: true

class Settings::AuditLog::SuggestionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  def actions
    AuditLogEntry.user_action_names
  end

  def repositories
    @repositories ||= current_user.visible_repositories_for(current_user)
  end
end
