# frozen_string_literal: true

class Settings::Organization::ProjectsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization

  def organization_projects_policy?
    organization.organization_projects_policy?
  end

  def organization_projects_enabled?
    organization.organization_projects_enabled?
  end

  def organization_projects_enabled_text
    organization.organization_projects_enabled? ? "enabled" : "disabled"
  end

  def organization_projects_label_class
    return "text-gray-light" if organization_projects_policy?
  end

  def organization_projects_label_text
    return "Enable projects for the organization" unless organization_projects_policy?

    helpers.safe_join([
      helpers.octicon("shield-lock"),
      "Organization projects have been",
      helpers.link_to("#{organization_projects_enabled_text} by enterprise administrators", GitHub.business_accounts_help_url) + ".",
    ], " ")
  end

  def repository_projects_policy?
    organization.repository_projects_policy?
  end

  def repository_projects_enabled?
    organization.repository_projects_enabled?
  end

  def repository_projects_enabled_text
    organization.repository_projects_enabled? ? "enabled" : "disabled"
  end

  def repository_projects_label_class
    return "text-gray-light" if repository_projects_policy?
  end

  def repository_projects_label_text
    return "Enable projects for all repositories" unless repository_projects_policy?

    helpers.safe_join([
      helpers.octicon("shield-lock"),
      "Repository projects have been",
      helpers.link_to("#{repository_projects_enabled_text} by enterprise administrators", GitHub.business_accounts_help_url) + ".",
    ], " ")
  end

  def disable_save_button?
    organization_projects_policy? && repository_projects_policy?
  end
end
