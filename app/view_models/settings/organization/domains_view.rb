# frozen_string_literal: true

module Settings
  module Organization
    class DomainsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include PlatformHelper

      DomainsViewQuery = parse_query <<-'GRAPHQL'
        fragment on Organization {
          notificationRestrictionsSupported: planSupports(feature: RESTRICT_NOTIFICATION_DELIVERY)
          verified_domains: organizationDomains(isVerified: true) {
            totalCount
          }
          adminInfo {
            hasNotificationDeliveryRestrictionEnabled
            membersWithoutVerifiedNotificationEmail(first: 100) {
              totalCount
            }
          }
        }
      GRAPHQL

      def after_initialize
        @org = DomainsViewQuery.new(@org)
      end

      attr_reader :org

      def show_notification_restrictions?
        org.verified_domains.total_count > 0 &&
        org.notification_restrictions_supported?
      end

      def notification_restrictions_enabled?
        org.admin_info.has_notification_delivery_restriction_enabled
      end

      def render_notification_restriction_confirmation?
        !notification_restrictions_enabled? &&
        org.admin_info.members_without_verified_notification_email.total_count > 0
      end
    end
  end
end
