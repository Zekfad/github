# frozen_string_literal: true

module Mobile
  module Discussions
    class CommentView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include CommentsHelper
      include PlatformHelper

      attr_reader :comment

      CommentFragment = parse_query <<-'GRAPHQL'
        fragment on Comment {
          body
          bodyHTML
          bodyVersion
          lastEditedAt
          publishedAt
          spammy

          ...CommentsHelper::CommentFragment

          author {
            login
            resourcePath

            ... on Bot {
              isDeltaforce
            }

            ...AvatarHelper::AvatarFragment::Actor
          }

          ... on Reactable {
            viewerCanReact
          }

          ... on Updatable {
            viewerCanUpdate
          }

          ... on Deletable {
            viewerCanDelete
          }

          ... on IssueComment {
            databaseId

            repository {
              owner { login }
              name
            }
          }

          ... on Minimizable {
            isMinimized
            minimizedReason
          }

          ... on PullRequestReviewComment {
            updateResourcePath
            thread
            pullRequest {
              state
              viewerCanApplySuggestion
              headRef
              headRepository
            }
          }

          ... on CommitComment {
            updateResourcePath
          }

          ... on GistComment {
            databaseId

            gist {
              name
              owner { login }
            }
          }
        }
      GRAPHQL

      def initialize(comment)
        @comment = CommentFragment.new(comment)
      end

      def spammy?
        comment.spammy?
      end

      def minimized_reason
        comment.minimized_reason
      end

      def author
        comment.author
      end

      def author_login
        author.login
      end

      def author_resource_path
        author.resource_path
      end

      def last_edited_at
        comment.last_edited_at
      end

      def published_at
        comment.published_at
      end

      def comment_body_html
        comment.body_html
      end

      def comment_body
        comment.body
      end

      def comment_body_version
        comment.body_version
      end

      def update_resource_path
        comment.update_resource_path.to_s
      end

      def head_repository
        comment.pull_request.head_repository
      end

      def head_ref
        comment.pull_request.head_ref
      end

      def author_is_dependabot?
        author.is_a?(PlatformTypes::Bot) && author.is_deltaforce
      end

      def dependabot_owner
        GitHub.dependabot_github_app.owner
      end

      def subject_dom_id
        @subject_dom_id ||= graphql_comment_dom_id(comment)
      end

      def permalink_dom_id
        @permalink_dom_id ||= "#{subject_dom_id}-permalink"
      end

      def minimized?
        return @minimized if defined?(@minimized)
        @minimized = comment.is_a?(PlatformTypes::Minimizable) && comment.is_minimized?
      end

      def updatable?
        return @updatable if defined?(@updatable)
        @updatable = comment.is_a?(PlatformTypes::Updatable) && comment.viewer_can_update?
      end

      def reactable?
        return @reactable if defined?(@reactable)
        @reactable = comment.is_a?(PlatformTypes::Reactable)
      end

      def deletable?
        return @deletable if defined?(@deletable)
        @deletable = comment.is_a?(PlatformTypes::Deletable) && comment.viewer_can_delete?
      end

      def suggested_changes_enabled?
        return @suggested_changes_enabled if defined?(@suggested_changes_enabled)
        @sugested_changes_enabled = pull_request_review_comment? &&
          comment.pull_request.state == "OPEN" &&
          head_repository && head_ref &&
          comment.pull_request.viewer_can_apply_suggestion?
      end

      def pull_request_review_comment?
        return @pull_request_review_comment if defined?(@pull_request_review_comment)
        @pull_request_review_comment = comment.is_a?(PlatformTypes::PullRequestReviewComment)
      end

      def commit_comment?
        return @commit_comment if defined?(@commit_comment)
        @commit_comment = comment.is_a?(PlatformTypes::CommitComment)
      end

      def update_path
        @update_path ||= if comment.is_a?(PlatformTypes::IssueComment)
          urls.issue_comment_path(
            comment.repository.owner.login,
            comment.repository.name,
            comment.database_id,
          )
        elsif comment.is_a?(PlatformTypes::GistComment)
          urls.update_user_gist_comment_path(
            # Temporary fix for https://github.com/github/github/issues/88653
            comment.gist.owner&.login || "anonymous",
            comment.gist.name,
            comment.database_id,
          )
        else
          comment.update_resource_path.to_s
        end
      end

      def field_name
        @field_name ||= if comment.is_a?(PlatformTypes::IssueComment)
          "issue_comment"
        elsif comment.is_a?(PlatformTypes::GistComment)
          "gist_comment"
        elsif comment.is_a?(PlatformTypes::CommitComment)
          "commit_comment"
        elsif comment.is_a?(PlatformTypes::PullRequestReviewComment)
          "pull_request_review_comment"
        end
      end
    end
  end
end
