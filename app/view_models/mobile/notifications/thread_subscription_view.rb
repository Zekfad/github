# frozen_string_literal: true

module Mobile
  module Notifications
    class ThreadSubscriptionView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include ::Notifications::ThreadSubscriptionMethods

      attr_reader :list
      attr_reader :thread

      def form_path
        @attributes[:form_path] || urls.notifications_thread_subscribe_path
      end
    end
  end
end
