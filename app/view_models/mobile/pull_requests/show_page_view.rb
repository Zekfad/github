# frozen_string_literal: true

class Mobile::PullRequests::ShowPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Mobile::TabsHelper

  ALLOWED_TABS = %w(discussion commits files)

  attr_reader :pull, :diffs, :tab, :visible_timeline_items, :showing_full_timeline, :pull_node

  def commits
    @commits ||= pull.changed_commits
  end

  # Public: The currently selected tab on the issue list.
  #
  # Can be either be 'discussion' or 'commits'.
  #
  # Returns a string.
  def current_tab
    @current_tab ||= ALLOWED_TABS.include?(tab) ? tab : "discussion"
  end
end
