# frozen_string_literal: true

# We can generalize this to view any queue as we go forward
module Devtools::Spamurai
  class PatternsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::TagHelper
    include BasicHelper

    DEFAULT_PER_PAGE = 40
    DEFAULT_SORT_KEY = :recently_matched

    attr_reader :filtered_state, :selected_target, :selected_sort, :per_page, :current_page

    def patterns
      @patterns ||= filtered_patterns(filtered_scope).paginate \
        per_page: (per_page || DEFAULT_PER_PAGE),
        page: current_page
    end

    def has_patterns?
      patterns.any?
    end

    def total_active_patterns
      filtered_patterns.active.count
    end

    def total_inactive_patterns
      filtered_patterns.inactive.count
    end

    def sanitized_sort_key
      @sanitized_sort_key ||= if selected_sort && sort_options.keys.include?(selected_sort.to_sym)
        selected_sort.to_sym
      else
        DEFAULT_SORT_KEY
      end
    end

    # What classes should we show as valid filter targets?
    # For now, let's select them from the existing data
    def filterable_target_classes
      @filterable_target_classes ||= SpamPattern.select("DISTINCT(class_name)").pluck(:class_name).sort
    end

    # Options hash to be used in filter links
    def filter_options_hash(overrides = {})
      {
        state: filtered_state,
        target: selected_target,
        sort: sanitized_sort_key,
      }.merge overrides
    end

    def active_selected
      filtered_state == :active
    end

    def inactive_selected
      filtered_state == :inactive
    end

    # Public: Retrieve possible spammers from the queue
    #
    # This method also drops any non-existant (aka deleted) logins
    # from the PSQ whenever they are found.
    #
    # Returns an Array of Users
    def users
      return @users if defined? @users

      logins = queue.peek [limit, queue.size].min

      @users = User.where(login: logins).includes(:profile).to_a

      # Not every login on the queue is valid any more. Clear'em out
      dead_logins = logins - @users.map { |u| u.login.downcase }
      queue.drop_these(dead_logins) if dead_logins.any?

      @users
    end

    def has_users?
      users.count > 0
    end

    def user_count
      users.count
    end

    def total_user_count
      queue.size
    end

    def sort_options
      {
        newest: {
          description: "Newest", sql: "id DESC"
        },
        oldest: {
          description: "Oldest", sql: "id ASC"
        },
        most_matched: {
          description: "Most matched", sql: "match_count DESC"
        },
        least_matched: {
          description: "Least matched", sql: "match_count ASC"
        },
        most_false_positives: {
          description: "Most false positives", sql: "false_positives DESC"
        },
        least_false_positives: {
          description: "Least false positives", sql: "false_positives ASC"
        },
        highest_false_percentage: {
          description: "Highest false positive %", sql: Arel.sql("((false_positives / match_count) * 100) DESC")
        },
        lowest_false_percentage: {
          description: "Lowest false positive %", sql: Arel.sql("((false_positives / match_count) * 100) ASC")
        },
        recently_matched: {
          description: "Recently matched", sql: "last_matched_at DESC"
        },
        least_recently_matched: {
          description: "Least recently matched", sql: "last_matched_at ASC"
        },
      }
    end

    private
    def filtered_scope
      if filtered_state == :inactive
        SpamPattern.inactive
      else
        SpamPattern.active
      end
    end

    def filtered_patterns(scope = nil)
      scope = SpamPattern if scope.nil?

      # Filter on target class if valid one provided
      scope = scope.where(class_name: selected_target) if valid_selected_target?

      sort = sort_options[sanitized_sort_key][:sql]
      scope.order(sort)
    end

    def valid_selected_target?
      selected_target && filterable_target_classes.include?(selected_target)
    end
  end
end
