# frozen_string_literal: true

# Methods shared between OauthTokens::TokenView and Settings::SshKeyView
# currently.
#
# Depends on presence of the following where included:
# - sso_organizations
# - credential_map
# - credential
# - return_to_path
module CredentialAuthorizationMethods
  def show_sso_ready_badge?
    sso_organizations.any?
  end

  def sso_ready?
    credential_map.values.any? { |authorization| authorization.try(:active?) }
  end

  def icon(credential)
    return unless credential

    icon = credential.revoked? ? "x" : "check"
    css_class = credential.revoked? ? "text-red" : "text-green"

    helpers.octicon(icon, class: css_class)
  end

  def authorize_path(org)
    target = org.external_identity_session_owner

    request_token = ::Organization::CredentialAuthorization.generate_request \
      organization: org, target: target, credential: credential, actor: current_user

    case target
    when ::Organization
      urls.org_idm_sso_path(target, authorization_request: request_token, return_to: return_to_path)
    when ::Business
      urls.business_idm_sso_enterprise_path(target, authorization_request: request_token, return_to: return_to_path)
    end
  end
end
