# frozen_string_literal: true

module Discussions
  class ReferenceView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :reference

    def actor
      reference.actor
    end

    def source
      reference.source
    end

    def target
      reference.target
    end

    def source_noun
      noun_for(source)
    end

    def target_noun
      noun_for(target)
    end

    def referenced_at
      reference.referenced_at
    end

    def cross_entity?
      source.entity != target.entity
    end

    # Public: Whether to show this reference
    #
    # If the user can see (read) the source entity, show
    #   (unless the source is an issue on a repo that has issues disabled)
    # If anything is missing (source, source entity), don't show
    #
    # Returns Boolean
    def show?
      return @show if defined?(@show)
      @show = reference.visible_to?(current_user)
    end

    # Public: Whether to point out this reference is "private"
    #
    # If you're showing a reference and the source is not public, point out
    # that not everyone will be able to see this reference. Not necessary
    # for same-entity references (if you can see this, you can see the source).
    #
    # Returns Boolean
    def show_private?
      show? && cross_entity? && !source.entity.public?
    end

    def state
      return unless source.is_a?(Issue)
      return "merged" if source.pull_request? && source.pull_request.merged?
      source.state
    end

    def state?
      !!state
    end

    def task_summary
      return unless source.respond_to?(:task_list?)
      return unless source.task_list?
      TaskLists::SummaryView.new(source.task_list_summary).text
    end

    def source_number
      source.respond_to?(:number) ? source.number : source.id
    end

    def anchor
      return unless noun = noun_for(source)

      ["ref",
        noun.gsub(/\s+/, ""),
        source.id,
      ].join("-")
    end

    def pending_fix?
      return false unless target.respond_to?(:state) && target.state == "open"
      return false unless source.is_a?(Issue) && source.pull_request?

      pull = source.pull_request
      return false if pull.merged? || pull.closed?

      GitHub.dogstats.time("discussions.reference_view", tags: ["action:pending_fix"]) do
        pull.close_issues.include?(target)
      end
    end

    private

    def noun_for(thing)
      case thing
      when Issue
        thing.pull_request? ? "pull request" : "issue"
      end
    end
  end
end
