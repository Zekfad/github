# frozen_string_literal: true

module Admin
  module User
    module SecurityHelper
      def audit_log_query
        keys = %w{actor_id user_id}
        keys << "org_id" if user.organization?
        query = keys.reverse.map { |k| "#{k}:#{user.id}" }.join " OR "
        "(#{query})"
      end

      def audit_log_path(query = audit_log_query)
        urls.stafftools_audit_log_path(query: query)
      end
    end
  end
end
