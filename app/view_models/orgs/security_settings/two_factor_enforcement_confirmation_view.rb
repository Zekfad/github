# frozen_string_literal: true

module Orgs
  module SecuritySettings
    class TwoFactorEnforcementConfirmationView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :organization

      delegate :affiliated_users_with_two_factor_disabled_scopes,
        to: :organization

      def users_with_two_factor_disabled_count
        affiliated_users_with_two_factor_disabled_scopes.
          inject(Set.new) do |user_ids, scope|
            user_ids.merge(scope.pluck(:id))
          end.size
      end

      def users_with_two_factor_disabled(limit: 100)
        return @users_with_two_factor_disabled if defined?(@users_with_two_factor_disabled)
        users_with_two_factor_disabled = Set.new
        remainder = limit

        affiliated_users_with_two_factor_disabled_scopes.each do |scope|
          users = scope.limit(remainder)
          users_with_two_factor_disabled.merge users
          remainder = remainder - users.size
          break if remainder <= 0
        end

        @users_with_two_factor_disabled = users_with_two_factor_disabled.to_a
      end

      def private_forks_count_for(user)
        @private_fork_counts ||= Repository.organization_member_private_forks(
          organization, users_with_two_factor_disabled).group(:owner_id).count

        @private_fork_counts[user.id] || 0
      end

      def show_private_forks_count_for?(user)
        private_forks_count_for(user) > 0
      end
    end
  end
end
