# frozen_string_literal: true

class Orgs::OverviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Users::OrganizationEnforcementMethods
  include TextHelper
  include RichwebHelper
  include PlatformHelper
  include IntegrationManagerHelper
  include GitHub::Application.routes.url_helpers

  attr_reader :organization, :breadcrumb, :selected_nav_item, :show_new_business_org_message,
              :finished_migration, :rate_limited

  def page_title
    organization.safe_profile_name
  end

  def show_header?
    [
      organization_location, organization_blog, organization_profile_email(logged_in: logged_in?)
    ].any?(&:present?)
  end

  def organization_login
    organization.login
  end

  def organization_avatar(avatarSize, avatarClass)
    helpers.avatar_for(organization, avatarSize, itemprop: "image", class: avatarClass)
  end

  def organization_avatar_url(avatarSize)
    helpers.avatar_url_for(organization, avatarSize)
  end

  def organization_location
    organization.profile_location
  end

  def organization_blog
    organization.profile_blog
  end

  def show_organization_twitter?
    organization.profile_twitter_username.present?
  end

  def organization_twitter_username
    organization.profile_twitter_username
  end

  def organization_twitter_url
    organization.profile&.twitter_url
  end

  def organization_profile_email(logged_in: false)
    organization.publicly_visible_email(logged_in: logged_in)
  end

  # Add the appropriate classes for the org header's meta data.
  def meta_classes(logged_in: false)
    classes = []
    classes << "has-location" if organization_location.present?
    classes << "has-blog"     if organization_blog.present?
    classes << "has-email"    if organization_profile_email(logged_in: logged_in).present?
    classes.join " "
  end

  def organization_developer_program_member?
    organization.developer_program_member?
  end

  def pending_invitation
    @pending_invitation ||= organization.pending_invitation_for(current_user)
  end

  def business_invitations
    organization.business_invitations_acceptable_by(current_user)
  end

  def show_role?
    pending_invitation && pending_invitation.billing_manager?
  end

  def accept_invitation_path
    if pending_invitation.billing_manager?
      urls.org_show_pending_billing_manager_invitation_path(organization)
    else
      urls.org_show_invitation_path(organization)
    end
  end

  def discussions_count
    @discussions_count ||= discussions_scope.count
  end

  def repositories_count
    @repositories_count ||= repositories_scope.count
  end

  def public_repositories_count
    @public_repositories_count ||= organization.public_repositories.count
  end

  def show_admin_stuff?
    adminable_by_current_user?
  end

  def show_apps_manager_stuff?
    manages_any_integration?(user: current_user, organization: organization)
  end

  # Public: Should we show 2fa information
  #
  # Returns a boolean.
  def show_2fa?
    show_admin_stuff? && GitHub.auth.two_factor_authentication_enabled?
  end

  def show_saml_sso?
    show_admin_stuff? && organization.saml_sso_present?
  end

  def manager_org_settings_path
    if billing_manager?
      return urls.settings_org_billing_path(organization)
    end
  end

  def manage_org_apps_settings_path
    urls.settings_org_apps_path(organization)
  end

  # Should a link be shown to the org settings page for a billing
  # manager?
  def show_manager_stuff?
    billing_manager?
  end

  def hide_people?
    billing_manager? && !direct_or_team_member?
  end

  def show_teams?
    direct_or_team_member?
  end

  def show_packages?
    PackageRegistryHelper.show_packages?
  end

  def show_discussions?
    GitHub.flipper[:discussions_org_list].enabled?(organization)
  end

  def show_policies?
    return false unless direct_or_team_member?
    organization.policies_enabled_for?(current_user)
  end

  def show_projects?
    organization.organization_projects_enabled?
  end

  def show_new_team_button?
    organization.can_create_team?(current_user)
  end

  def show_import_teams_button?
    GitHub.ldap_sync_enabled? && adminable_by_current_user?
  end

  def discussions_scope
    organization.discussion_posts.visible_to(current_user)
  end

  def repositories_scope
    @repositories_scope ||= organization.visible_repositories_for(
      current_user,
      associated_repository_ids: associated_repository_ids,
    ).where(owner_id: organization.id)
  end

  def viewer_invited?
    pending_invitation.present?
  end

  def viewer_opted_in?
    return false unless current_user
    !OrganizationInvitation::OptOut.opted_out?(org: organization, invitee: current_user)
  end

  def show_pending_team_change_parent_requests?
    false
  end

  # Public: Checks if this page is being rendered to a user who's come through the
 # Business > New Organization workflow. We pass a URL param that should trigger a plain text
  # success flash message to be rendered
  def show_new_business_org_message?
    show_new_business_org_message && organization.business
  end

  def per_seat_pricing_model
    Billing::PlanChange::PerSeatPricingModel.new \
      organization,
      seats: organization.seats,
      plan_duration: organization.plan_duration,
      new_plan: organization.plan
  end

  def can_invite_users?
    !organization.at_seat_limit? || organization.business.present?
  end

  def show_finished_migration_help?
    !!finished_migration
  end

  def show_org_membership_banner?
    return unless logged_in?
    return if current_user.dismissed_notice?("org_membership_banner")

    # Only show the banner to non-owners.
    organization.direct_member?(current_user) && !adminable_by_current_user?
  end

  def failed_invitations
    if defined? @failed_invitations
      return @failed_invitations
    end
    @failed_invitations = organization.active_failed_invitations
  end

  def pending_non_manager_invitations
    if defined? @pending_non_manager_invitations
      return @pending_non_manager_invitations
    end
    @pending_non_manager_invitations = organization.pending_non_manager_invitations.
        includes(invitee: :profile)
  end

  # Public: Should we show the failed invitations section?
  #
  # Returns a boolean.
  def show_failed_invitations?
    failed_invitations.any? && adminable_by_current_user?
  end

  # Public: Should we show the pending invitations section?
  #
  # Returns a boolean.
  def show_pending_invitations?
    pending_non_manager_invitations.any? && adminable_by_current_user?
  end

  # Public: Should we show invitations?
  #
  # Returns a boolean.
  def show_invitations?
    show_pending_invitations? || show_failed_invitations?
  end

  def manage_seats_path
    organization.has_downgradable_seats? ? remove_org_seats_path(organization) : org_seats_path(organization)
  end

  private

  # Internal: Is the current organization adminable by the current user?
  # Memoized to prevent database roundtrips.
  #
  # Returns a Boolean.
  def adminable_by_current_user?
    return @adminable_by_current_user if defined? @adminable_by_current_user
    @adminable_by_current_user = organization.adminable_by?(current_user)
  end

  # Internal: Is the current user a direct or team member of the current
  # organization. Memoized to prevent database roundtrips.
  #
  # Returns a Boolean.
  def direct_or_team_member?
    return @direct_or_team_member if defined? @direct_or_team_member
    @direct_or_team_member = organization.direct_or_team_member?(current_user)
  end

  # Internal: Is the current user a billing_manager of the current
  # organization. Memoized to prevent repeat database roundtrips.
  #
  # Returns a Boolean.
  def billing_manager?
    return @is_billing_manager if defined? @is_billing_manager
    @is_billing_manager = organization.billing_manager?(current_user)
  end

  # Internal: Memoized copy of all repository IDs associated with the current
  # user.
  #
  # Returns an Array of Integers.
  def associated_repository_ids
    return [] unless current_user
    @associated_repository_ids ||= current_user.associated_repository_ids
  end
end
