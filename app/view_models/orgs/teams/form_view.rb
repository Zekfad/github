# frozen_string_literal: true

module Orgs
  module Teams
    class FormView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :organization
      attr_reader :query
      attr_reader :team
      attr_reader :team_entity
      attr_reader :org
      attr_reader :making_new_team
      attr_reader :team_params

      include ActionView::Helpers::TextHelper
      include PlatformHelper

      TeamFragment = parse_query <<-'GRAPHQL'
        fragment on Team {
          avatarUrl(size: 400)
          combinedSlug
          databaseId
          id
          name
          slug
          description
          discussions {
            totalCount
          }
          privacy
          parentTeam {
            name
            slug
            id
          }
          hasChildTeams
          resourcePath
          isLegacyAdminTeam
          migrateLegacyAdminTeamResourcePath
          canBeExternallyManaged
          outboundParentRequests: pendingTeamChangeParentRequests(first: 1, direction: OUTBOUND_CHILD_INITIATED) {
            edges {
              node {
                cancelResourcePath
                parentTeam{
                  id
                  name
                }
              }
            }
          }
          ...Views::Orgs::Teams::LdapGroup::Team
          ...Views::Orgs::Teams::DeleteTeamDialog::Team
        }
      GRAPHQL

      OrgFragment = parse_query <<-'GRAPHQL'
        fragment on Organization {
          name
          viewerCanAdminister
          totalTeams: teams(privacy: VISIBLE) {
            totalCount
          }
        }
      GRAPHQL

      def initialize(options)
        super(options)

        @team_params = {} if team_params.nil?
        @team = team && TeamFragment.new(@team)
        @view_model_org = OrgFragment.new(@org)
      end

      # Public: Should the team name field be autofucused?
      #
      # Returns a boolean.
      def autofocus_name_field?
        making_new_team?
      end

      # Public: Should the submit button be disabled when the view is first
      # loaded?
      #
      # This is true when it's a new team form, because the form will start
      # out blank, which is invalid. Once the user starts typing a name, the
      # client-side validation JS will enable the button.
      #
      # Returns a boolean.
      def disable_submit_button_on_load?
        making_new_team?
      end

      # Public: Get the method to use for the form.
      #
      # Returns a symbol (:post or :put).
      def form_method
        making_new_team? ? :post : :put
      end

      # Public: Get the text to show in the header.
      #
      # Returns a string.
      def header_text
        making_new_team? ? "Create new team" : "Team settings"
      end

      # Public: Should the team delete button be shown?
      #
      # Should only show when editing a non-Owners team.
      #
      # Returns a boolean.
      def show_delete_button?
        editing_existing_team?
      end

      # Public: Should the change permissions button be shown?
      #
      # Should only show when editing a team.
      #
      # Returns a boolean.
      def show_change_permissions_button?
        editing_existing_team?
      end

      def submit_button_text
        making_new_team? ? "Create team" : "Save changes"
      end

      def team_database_id
        team&.database_id
      end

      def team_name_note
        if making_new_team?
          "You’ll use this name to mention this team in conversations."
        else
          "Changing the team name will break past @mentions."
        end
      end

      def not_deletable?
        team.has_child_teams? && !@view_model_org.viewer_can_administer
      end

      def parent_team_id
        if team_params["parentTeam"].present?
          team_params["parentTeam"]["id"]
        elsif team&.parent_team
          team.parent_team.id
        elsif requested_parent_team
          requested_parent_team&.node&.parent_team&.id
        else
          nil
        end
      end

      def parent_team_name
        if team_params["parentTeam"].present?
          team_params["parentTeam"]["name"]
        elsif team.present? && team.parent_team
          team.parent_team.name
        elsif requested_parent_team
          requested_parent_team_name
        else
          nil
        end
      end

      def team_avatar_url
        team&.avatar_url
      end

      def team_name
        team_params["name"] || team&.name
      end

      def team_combined_slug
        team&.combined_slug
      end

      def team_slug
        team_params["slug"] || team&.slug
      end

      def team_description
        team_params["description"] || team&.description
      end

      def team_privacy
        team_params["privacy"] || team&.privacy
      end

      def team_team_path
        team.resource_path.to_s
      end

      def original_team_slug
        team&.slug
      end

      def organization_name
        @view_model_org.name
      end

      def page_title_text
        if making_new_team?
          "Create new team · #{organization_name}"
        else
          "Edit #{team_name} team settings · #{organization_name}"
        end
      end

      def original_team_visibility
        team&.privacy
      end

      def team_count
        @view_model_org.total_teams.total_count
      end

      def allow_team_avatar_uploads?
        !making_new_team
      end

      def show_specific_destruction_warnings
        team.has_child_teams? || team.discussions.total_count > 0
      end

      def destruction_warnings
        return unless show_specific_destruction_warnings

        discussion_count = team.discussions.total_count
        warnings = [
          discussion_count > 0 ? "#{pluralize(discussion_count, "discussion post")}" : nil,
          team.has_child_teams? ? "the following child teams:" : nil,
        ].compact.join(" and ")
        "Deleting this team will delete #{warnings}"
      end

      def current_parent_team
        team&.parent_team || team_params["parentTeam"]
      end

      def requested_parent_team
        team&.outbound_parent_requests&.edges&.first
      end

      def requested_parent_team_name
        requested_parent_team&.node&.parent_team&.name
      end

      def requested_parent_team_cancel_path
        requested_parent_team&.node&.cancel_resource_path
      end

      def legacy_admin_team?
        team&.is_legacy_admin_team
      end

      def migrate_legacy_admin_team_resource_path
        team&.migrate_legacy_admin_team_resource_path
      end

      def team_can_be_externally_managed?
        if making_new_team?
          organization.show_team_sync_feature?
        else
          team.can_be_externally_managed?
        end
      end

      private

      # Internal: Are we editing an existing team rather than making a new one?
      #
      # Returns a boolean.
      def editing_existing_team?
        !making_new_team?
      end

      # Internal: Are we making a new team rather than editing an existing one?
      #
      # Returns a boolean.
      def making_new_team?
        making_new_team
      end
    end
  end
end
