# frozen_string_literal: true

class Orgs::Teams::IndexPageView < Orgs::OverviewView
  include GitHub::Application.routes.url_helpers
  include UrlHelper
  # View model attributes
  attr_reader :organization, :page, :query, :graphql_org

  include PlatformHelper

  OrgFragment = parse_query <<-'GRAPHQL'
    fragment on Organization {
      name
      teamsResourcePath
      newTeamResourcePath
      importOrganizationResourcePath
      teamsToolbarActionsResourcePath
      dismissTeamsBannerResourcePath
      viewerIsAMember
      viewerCanAdminister
      viewerCanCreateTeams
      teams(first: $first, last: $last, before: $before, after: $after, membersFilter: $membersFilter, userLogins: $userLogins, privacy: $privacy, orderBy: { field: NAME, direction: ASC }, query: $query, rootTeamsOnly: $nestedList) {
        pageInfo {
          ...ApplicationHelper::CursorPaginate
        }
        edges {
          cursor
          node {
            id
            ...Views::Orgs::Teams::Team::Team
          }
        }
      }

      allTeams: teams {
        totalCount
      }
    }
  GRAPHQL

  def initialize(**args)
    super(args)

    @g_org = args[:graphql_org] && OrgFragment.new(args[:graphql_org])
  end

  def page_title
    "Teams · #{g_org.name}"
  end

  def show_visibility_toggle?
    true
  end

  def teams
    @teams ||= g_org.teams.edges.map { |t| t.node }
  end

  def teams_page_info
    g_org.teams.page_info
  end

  # This method is called parent_teams because it represents relative root teams
  # Relative to a team - parent_teams are that team's direct children (excludes other descendants) (Orgs::Teams::TeamsPageView#parent_teams)
  # Relative to an org - parent_teams are teams in the org that don't have parents (root teams) (Orgs::Teams::IndexPageView#parent_teams)
  alias_method :parent_teams, :teams
  alias_method :parent_teams_page_info, :teams_page_info

  def any_teams?
    g_org.all_teams.total_count > 0
  end

  def heading_text
    teams_count = g_org.all_teams.total_count
    teams_count_text = helpers.pluralize(teams_count, "team", "teams")
    "#{teams_count_text} in the #{g_org.name} organization"
  end

  def search_path
    g_org.teams_resource_path.to_s
  end

  def teams_toolbar_actions_path
    g_org.teams_toolbar_actions_resource_path.to_s
  end

  def new_team_path
    g_org.new_team_resource_path.to_s
  end

  def import_organization_path
    g_org.import_organization_resource_path.to_s
  end

  def dismiss_teams_banner_path
    g_org.dismiss_teams_banner_resource_path.to_s
  end

  def can_move_teams?
    false
  end

  def can_create_teams?
    g_org.viewer_can_create_teams
  end

  def search?
    query.present?
  end

  def show_admin_stuff?
    g_org.viewer_can_administer
  end

  def show_bulk_actions?
    g_org.viewer_can_administer
  end

  def show_import_teams_button?
    GitHub.ldap_sync_enabled? && g_org.viewer_can_administer
  end

  # to tell pagination which controller to use when writing the url
  def controller_action
    "index"
  end

  def show_org_teams_banner?
    !any_teams? || (g_org.viewer_is_a_member && !current_user.dismissed_notice?("org_teams_banner"))
  end

  def show_disabled_child_team_button?
    false
  end

  private

  def g_org
    @g_org
  end
end
