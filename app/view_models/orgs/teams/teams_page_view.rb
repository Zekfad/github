# frozen_string_literal: true

class Orgs::Teams::TeamsPageView < Orgs::OverviewView
  include GitHub::Application.routes.url_helpers
  include UrlHelper
  # View model attributes
  attr_reader :page, :query, :graphql_team, :org, :team

  include PlatformHelper

  TeamFragment = parse_query <<-'GRAPHQL'
    fragment on Team {
      name
      slug
      id
      databaseId
      locallyManaged
      viewerCanAdminister
      resourcePath
      newTeamResourcePath
      teamsResourcePath
      teamsToolbarActionsResourcePath
      hasChildTeams

      inboundParentRequests: pendingTeamChangeParentRequests(first: $first, direction: INBOUND_PARENT_INITIATED) @include(if: $isMaintainer) {
        totalCount
      }

      inboundChildRequests: pendingTeamChangeParentRequests(first: $first, direction: INBOUND_CHILD_INITIATED) @include(if: $isMaintainer) {
        totalCount
      }

      childTeams(immediateOnly: $immediateOnly, first: $first, last: $last, before: $before, after: $after, membersFilter: $membersFilter, userLogins: $userLogins, orderBy: { field: NAME, direction: ASC }, query: $query) {
        totalCount
        pageInfo {
          ...ApplicationHelper::CursorPaginate
        }
        edges {
          cursor
          node {
            id
            ...Views::Orgs::Teams::Team::Team
          }
        }
      }
    }
  GRAPHQL

  OrgFragment = parse_query <<-'GRAPHQL'
    fragment on Organization {
      name
      viewerIsAMember
      viewerCanAdminister
      viewerCanCreateTeams
      importOrganizationResourcePath
      dismissTeamsBannerResourcePath
    }
  GRAPHQL

  def initialize(**args)
    super(args)

    @g_team = args[:graphql_team] && TeamFragment.new(args[:graphql_team])
    @g_org = args[:org] && OrgFragment.new(args[:org])
  end

  def show_visibility_toggle?
    false
  end

  def page_title
    "#{g_team.name} · #{g_org.name} Teams List"
  end

  def teams
    @teams ||= g_team.child_teams.edges.map { |t| t.node }
  end

  def teams_page_info
    g_team.child_teams.page_info
  end

  # This method is called parent_teams because it represents relative root teams
  # Relative to a team - parent_teams are that team's direct children (excludes other descendants) (Orgs::Teams::TeamsPageView#parent_teams)
  # Relative to an org - parent_teams are teams in the org that don't have parents (root teams) (Orgs::Teams::IndexPageView#parent_teams)
  alias_method :parent_teams, :teams
  alias_method :parent_teams_page_info, :teams_page_info

  def any_teams?
    g_team.has_child_teams?
  end

  def heading_text
    teams_count = g_team.child_teams.total_count
    "#{helpers.pluralize(teams_count, "team", "teams")} in the #{g_team.name} team"
  end

  def show_admin_stuff?
    g_team.viewer_can_administer
  end

  def show_bulk_actions?
    g_org.viewer_can_administer
  end

  def show_import_teams_button?
    GitHub.ldap_sync_enabled? && g_org.viewer_can_administer
  end

  # to tell pagination which controller to use when writing the url
  def controller_action
    "teams"
  end

  def search_path
    g_team.teams_resource_path.to_s
  end

  def teams_toolbar_actions_path
    g_team.teams_toolbar_actions_resource_path.to_s
  end

  def new_team_path
    "#{g_team.new_team_resource_path}?parent_team=#{g_team.slug}"
  end

  def import_organization_path
    g_org.import_organization_resource_path.to_s
  end

  def dismiss_teams_banner_path
    g_org.dismiss_teams_banner_resource_path.to_s
  end

  def can_move_teams?
    g_team.viewer_can_administer && g_team.locally_managed
  end

  def can_create_teams?
    g_org.viewer_can_create_teams && g_team.locally_managed
  end

  def search?
    query.present?
  end

  def show_org_teams_banner?
    !any_teams? || (g_org.viewer_is_a_member && !current_user.dismissed_notice?("org_teams_banner"))
  end

  def show_disabled_child_team_button?
    !can_create_teams?
  end

  def show_pending_team_change_parent_requests?
    show_admin_stuff? && inbound_request_count > 0
  end

  def current_team_id
    g_team.id
  end

  def inbound_request_count
    g_team.inbound_parent_requests.total_count + g_team.inbound_child_requests.total_count
  end

  private

  def g_team
    @g_team
  end

  def g_org
    @g_org
  end
end
