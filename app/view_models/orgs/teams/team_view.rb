# frozen_string_literal: true

class Orgs::Teams::TeamView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :parent_team_slug, :indent_level, :show_bulk_actions, :graphql_team

  def checkbox_indent
    level = indent_level.to_i
    "indent-#{level}" if level > 0
  end

  def avatar_range
    case indent_level
    when nil then 0..7
    when 0..2 then 0..7
    when 3..5 then 0..5
    when 6..8 then 0..3
    when 9..11 then 0..1
    else nil
    end
  end

  def avatar_width
    case indent_level
    when nil then "width-0"
    when 0..2 then "width-0"
    when 3..5 then "width-1"
    when 6..8 then "width-2"
    when 9..11 then "width-3"
    else nil
    end
  end

  def parent_team_class
    if child_team?
      "js-child-team bg-gray-light"
    else #parent team
      "root-team"
    end
  end

  def team_avatar_class
    "shortened-teams-avatars"
  end

  def bg_color_class
    if child_team?
      "bg-gray-light"
    else
      ""
    end
  end

  def child_team?
    parent_team_slug.present?
  end

  def show_bulk_actions?
    !!show_bulk_actions
  end
end
