# frozen_string_literal: true

class Orgs::Policies::View < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  def resource_type_label(item)
    case item.resource_type
    when :RESOURCE_REPOSITORY then "repository"
    when :RESOURCE_TEAM then "team"
    when :RESOURCE_ORGANIZATION then "organization"
    else "unknown"
    end
  end

  def resource_type_icon(item)
    case item.resource_type
    when :RESOURCE_REPOSITORY then "repo"
    when :RESOURCE_TEAM then "people"
    when :RESOURCE_ORGANIZATION then "organization"
    else "alert"
    end
  end

  def hovercard_attributes(resource)
    attrs = case resource
    when Repository
      helpers.hovercard_data_attributes_for_repository(resource)
    when Team
      helpers.hovercard_data_attributes_for_team(resource)
    when Organization
      helpers.hovercard_data_attributes_for_org(login: resource.login)
    end

    helpers.safe_data_attributes(attrs) if attrs.present?
  end
end
