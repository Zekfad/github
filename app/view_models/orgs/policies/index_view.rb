# frozen_string_literal: true

class Orgs::Policies::IndexView < Orgs::Policies::View
  attr_reader :current_organization, :policy_service_response, :resource_type_filter, :status_filter, :query

  def show_blank_slate?
    policies.empty? && (status_filter.empty? || status_filter == :POLICY_STATUS_UNKNOWN)
  end

  def policies
    return [] if policy_service_response.policies.blank?

    @policies ||= begin
      policy_service_response.policies.map { |policy| Policy.new(policy) }
    end
  end

  def no_policies_due_to_filter?
    policies.empty? && status_filter.present?
  end

  def selected_resource_type_filter
    query.resource.present? ? query.resource.capitalize : "All"
  end

  def resource_type_filters
    ["repository", "team", "organization"].map { |resource|
      {
        label: resource.capitalize,
        selected: query.qualifier_selected?(name: :resource, value: resource),
        url: urls.org_policies_path(current_organization, query: query.toggle_qualifier(name: :resource, value: resource)),
      }
    }.unshift({
      label: "All",
      selected: !query.contains_qualifier?(name: :resource),
      url: urls.org_policies_path(current_organization, query: query.replace_qualifier(name: :resource , value: nil)),
    })
  end

  def compliant_policy_count
    policy_service_response.total_compliant_policy_count
  end

  def noncompliant_policy_count
    policy_service_response.total_noncompliant_policy_count
  end

  def resource_type
    policies.first.resource_type
  end

  def total_policy_count
    policies.length
  end

  def compliance_ratio
    return 0 if total_policy_count == 0

    (compliant_policy_count).to_f/total_policy_count
  end

  def compliance_percentage
    (compliance_ratio * 100).round
  end

  def noncompliance_percentage
    100 - compliance_percentage
  end

  class Policy
    delegate :natural_id,
    :name,
    :id,
    :org_id,
    :description,
    :path,
    :commit_oid,
    :is_malformed,
    :repository_id,
    :compliant_outcome_count,
    :noncompliant_outcome_count,
    :compliance_ratio,
    :resource_type, to: :@remote_policy

    def initialize(remote_policy)
      @remote_policy = remote_policy
    end

    def status
      case @remote_policy.status
      when :EVALUATION_SUCCESS then "success"
      when :EVALUATION_FAILURE then "failure"
      when :EVALUATION_UNKNOWN then "unknown"
      when :EVALUATION_ERROR then "error"
      end
    end

    def compliance_percentage
      (compliance_ratio * 100).round
    end

    def compliant?
      status == "success"
    end

    def noncompliant?
      !compliant?
    end

    def malformed?
      is_malformed == true
    end

    def well_formed?
      !malformed?
    end

    def has_evaluation?
      status != "unknown"
    end

    def evaluation_has_no_outcomes?
      compliant_outcome_count == 0 && noncompliant_outcome_count == 0
    end
  end
end
