# frozen_string_literal: true

class Orgs::Policies::ShowView < Orgs::Policies::View
  attr_reader :current_organization, :policy_service_response, :policy_service_unavailable, :page, :per_page, :filter, :order_by

  delegate :name, :description, :natural_id, :commit_oid, :compliant_outcome_count, :noncompliant_outcome_count,
    to: :policy, prefix: true

  def show_blank_slate?
    policy.compliant_outcome_count.zero? && policy.noncompliant_outcome_count.zero? && policy_well_formed?
  end

  def no_outcomes_due_to_filter?
    outcomes.empty? && policy_well_formed? && filter.present?
  end

  def policy
    policy_service_response.policy
  end

  def policy_triggered_at
    policy.triggered_at&.to_time
  end

  def policy_evaluated_at
    policy.evaluated_at&.to_time
  end

  def policy_updated_at
    policy.updated_at&.to_time
  end

  def policy_triggered?
    policy_triggered_at.present?
  end

  def policy_evaluated?
    policy_evaluated_at.present?
  end

  def policy_never_evaluated?
    !policy_evaluated?
  end

  def policy_triggered_after_most_recent_evaluation?
    policy_triggered? && policy_evaluated? && policy_evaluated_at < policy_triggered_at
  end

  def policy_has_running_evaluation?
    policy_triggered? &&
      (policy_never_evaluated? || policy_triggered_after_most_recent_evaluation?)
  end

  def waiting_for_policy_evaluation_to_complete?
    policy_has_running_evaluation? && (Time.current - policy_triggered_at) < 10.minutes
  end

  def evaluation_error?
    policy.status == :EVALUATION_ERROR || policy.error_message.present?
  end

  def policy_malformed?
    policy.is_malformed
  end

  def policy_well_formed?
    !policy_malformed?
  end

  def policy_has_outcomes?
    outcomes.present?
  end

  def policy_short_commit_oid
    policy_commit_oid[0..7]
  end

  def repository
    current_organization.repositories.find_by_id(policy.repository_id)
  end

  def compliance_percentage
    (policy.compliance_ratio * 100).round
  end

  def noncompliance_percentage
    (100 - compliance_percentage)
  end

  def teams_by_id
    @teams_by_id ||= begin
      team_ids = []
      policy_service_response.outcomes.each do |outcome|
        team_ids << outcome.resource_id if outcome.resource_type == :RESOURCE_TEAM
      end

      if team_ids.empty?
        {}
      else
        current_organization.visible_teams_for(current_user).with_ids(team_ids).index_by(&:id)
      end
    end
  end

  def repositories_by_id
    @repositories_by_id ||= begin
      repository_ids = []
      policy_service_response.outcomes.each do |outcome|
        repository_ids << outcome.resource_id if outcome.resource_type == :RESOURCE_REPOSITORY
      end

      if repository_ids.empty?
        {}
      else
        repository_ids = current_user.associated_repository_ids(repository_ids: repository_ids)
        current_organization.repositories.with_ids(repository_ids).index_by(&:id)
      end
    end
  end

  def org_by_id
    @organizations_by_id ||= begin
      org_ids = []
      policy_service_response.outcomes.each do |outcome|
        org_ids << outcome.resource_id if outcome.resource_type == :RESOURCE_ORGANIZATION && outcome.resource_id == current_organization.id
      end

      if org_ids.empty?
        {}
      else
        { current_organization.id => current_organization }
      end
    end
  end

  def resource_for_outcome(outcome)
    case outcome.resource_type
    when :RESOURCE_REPOSITORY
      repositories_by_id[outcome.resource_id]
    when :RESOURCE_TEAM
      teams_by_id[outcome.resource_id]
    when :RESOURCE_ORGANIZATION
      org_by_id[outcome.resource_id]
    end
  end

  def outcomes
    return [] if policy_service_response.outcomes.blank?

    @outcomes ||= begin
      policy_service_response.outcomes.map do |outcome|
        OutcomeListItem.new(outcome, resource: resource_for_outcome(outcome))
      end
    end
  end

  def outcomes_count
    case filter
    when "compliant"
      return policy.compliant_outcome_count
    when "noncompliant"
      return policy.noncompliant_outcome_count
    else
      return total_outcome_count
    end
  end

  def total_outcome_count
    policy.compliant_outcome_count + policy.noncompliant_outcome_count
  end

  def show_pagination?
    outcomes_count > per_page
  end

  def paginated_outcomes
    @results ||= WillPaginate::Collection.create(page, per_page, outcomes_count) do |pager|
      pager.replace(outcomes || [])
    end
  end

  def outcome_path(resource)
    resource_name = case resource
      when Team then resource.slug
      when Repository then resource.name
      when Organization then resource.login
    end

    urls.org_outcome_path(org: current_organization, policy_natural_id: policy_natural_id, resource_name: resource_name)
  end

  class OutcomeListItem
    attr_reader :resource, :remote_outcome

    delegate :resource_id, :resource_type, to: :remote_outcome

    def initialize(remote_outcome, resource:)
      @remote_outcome = remote_outcome
      @resource = resource
    end

    def evaluated_at
      remote_outcome.evaluated_at.to_time
    end

    def resource_name
      resource.present? ? resource.name : "<missing resource>"
    end

    def repository_stargazer_count
      resource.present? ? resource.stargazer_count : 0
    end

    def repository_open_issues_count
      resource.present? ? resource.open_issues_count : 0
    end

    def status_label
      compliant? ? "successful" : "failed"
    end

    def compliant?
      remote_outcome.status == :SUCCESS
    end

    def repository_resource?
      remote_outcome.resource_type == :RESOURCE_REPOSITORY
    end

    def non_compliant?
      !compliant?
    end
  end
end
