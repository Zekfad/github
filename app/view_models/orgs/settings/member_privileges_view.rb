# frozen_string_literal: true

class Orgs::Settings::MemberPrivilegesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization

  def default_repository_permissions_select_list
    @select_list ||= [
        {
            heading: "None",
            description: "Members will only be able to clone and pull public repositories. To give a member additional access, you’ll need to add them to teams or make them collaborators on individual repositories.",
            value: "none",
            selected: organization.default_repository_permission == :none,
        },
        {
            heading: "Read",
            description: "Members will be able to clone and pull all repositories.",
            value: "read",
            selected: organization.default_repository_permission == :read,
        },
        {
            heading: "Write",
            description: "Members will be able to clone, pull, and push all repositories.",
            value: "write",
            selected: organization.default_repository_permission == :write,
        },
        {
            heading: "Admin",
            description: "Members will be able to clone, pull, push, and add new collaborators to all repositories.",
            value: "admin",
            selected: organization.default_repository_permission == :admin,
        },
    ]
  end

  def default_repository_permissions_button_text
    default_repository_permissions_selected_option[:heading]
  end

  def default_repository_permissions_input_value
    default_repository_permissions_selected_option[:value]
  end

  def default_repository_permissions_selected_option
    default_repository_permissions_select_list.find { |s| s[:selected] }
  end

  def default_repository_permissions_member_count
    organization.members_count
  end

  def default_repository_permissions_repository_count
    @repository_count ||= organization.repositories.count
  end

  def default_repository_permissions_need_confirmation?
    default_repository_permissions_repository_count > 0 && default_repository_permissions_member_count > 0
  end

  def members_can_change_repo_visibility_business_policy_set?
    organization.members_can_change_repo_visibility_policy?
  end

  # used to disable inputs in the UI. if a business policy is set then the org owner can't change it.
  def members_can_change_repo_visibility_disabled?
    members_can_change_repo_visibility_business_policy_set?
  end

  def members_can_change_repo_visibility_policy_action
    return nil unless members_can_change_repo_visibility_business_policy_set?

    organization.members_can_change_repo_visibility? ? "enabled" : "disabled"
  end

  def members_can_invite_outside_collaborators_policy_action
    organization.members_can_invite_outside_collaborators? ? "enabled" : "disabled"
  end

  def allow_forking_repo_label
    organization.supports_internal_repositories? ? "private and internal" : "private"
  end

  def allow_forking_repo_description
    organization.supports_internal_repositories? ? "private, internal, and public" : "private and public"
  end

  # Whether the option cannot be set at the org level.
  # Only unable to be set when a policy exists at the business level.
  def allow_private_repository_forking_disabled?
    organization&.business&.allow_private_repository_forking_policy?
  end

  def allow_private_repository_forking_policy_action
    return nil unless allow_private_repository_forking_disabled?

    organization.allow_private_repository_forking? ? "enabled" : "disabled"
  end

  def show_discussion_creation_setting?
    !GitHub.enterprise? && organization.repositories.any?
  end

  def can_delete_repos_form_path
    urls.organization_members_can_delete_repositories_path(organization)
  end

  def can_delete_issues_form_path
    urls.organization_members_can_delete_issues_path(organization)
  end

  def display_commenter_full_name_path
    urls.organization_display_commenter_full_name_path(organization)
  end

  def readers_can_create_discussions_path
    urls.organization_readers_can_create_discussions_path(organization)
  end

  def can_update_protected_branches_form_path
    urls.organization_members_can_update_protected_branches_path(organization)
  end

  def can_create_teams_form_path
    urls.organization_members_can_create_teams_path(organization)
  end

  def members_can_delete_repositories_action
    organization.members_can_delete_repositories? ? "enabled" : "disabled"
  end

  def members_can_delete_issues_action
    organization.members_can_delete_issues? ? "enabled" : "disabled"
  end

  def display_commenter_full_name_action
    organization.display_commenter_full_name_setting_enabled? ? "enabled" : "disabled"
  end

  def members_can_update_protected_branches_action
    organization.members_can_update_protected_branches? ? "enabled" : "disabled"
  end

  # Repo creation policy

  def members_can_create_repositories_policy_set?
    organization.members_can_create_repositories_policy?
  end

  def members_can_create_public_repos_disabled?
    members_can_create_repositories_policy_set? ||
      (!organization.can_restrict_only_public_repo_creation? && organization.members_can_create_private_repositories?)
  end

  def members_can_create_private_repos_disabled?
    members_can_create_repositories_policy_set?
  end

  def members_can_create_internal_repos_disabled?
    members_can_create_repositories_policy_set?
  end

  def show_internal_repo_creation_option?
    organization.supports_internal_repositories?
  end

  def private_only_policy_allowed?
    organization.can_restrict_only_public_repo_creation?
  end


  def repo_creation_privileges_doc_link
    helpers.link_to(
      "Why is this option disabled?",
       GitHub.help_url + "/en/articles/restricting-repository-creation-in-your-organization",
       {
        hidden: !members_can_create_public_repos_disabled? || members_can_create_repositories_policy_set?,
        class: "js-public-disabled-doc-link",
       },
    )
  end

  # Repo deletion policy

  def members_can_delete_repositories_policy_set?
    organization.members_can_delete_repositories_policy?
  end

  def members_can_delete_issues_policy_set?
    organization.members_can_delete_issues_policy?
  end

  def members_can_invite_outside_collaborators_policy_set?
    organization.members_can_invite_outside_collaborators_policy?
  end

  def members_can_update_protected_branches_policy_set?
    organization.members_can_update_protected_branches_policy?
  end

  # Action execution capability AKA Actions permissions

  def show_actions_permissions?
    actions_permissions.visible?
  end

  def actions_permissions
    @_actions_permissions ||= Orgs::Settings::ActionsPermissions.new(organization)
  end

  # Dependency insights

  def can_restrict_dependency_insights?
    organization.dependency_insights_enabled_for?(current_user)
  end

  def members_can_view_dependency_insights_set?
    organization.members_can_view_dependency_insights_policy?
  end

  def members_can_view_dependency_insights_policy_action
    organization.members_can_view_dependency_insights? ? "enabled" : "disabled"
  end

  # Custom roles

  def affected_custom_roles(action)
    custom_role_count = dependant_custom_roles_count(action)

    "#{custom_role_count} custom #{'role'.pluralize(custom_role_count)}"
  end

  def affected_members(action)
    member_count = dependant_custom_role_members_count(action)

    "#{member_count} #{'member'.pluralize(member_count)}"
  end

  def affected_repositories(action)
    repository_count = dependant_custom_role_repositories_count(action)

    "#{repository_count} #{'repository'.pluralize(repository_count)}"
  end

  private

  def dependant_custom_roles_count(action)
    dependant_custom_roles(action).count
  end

  def dependant_custom_role_members_count(action)
    UserRole
    .select(:actor_id).distinct
    .where(actor_type: "User", actor_id: org_member_ids, target_type: "Repository", role_id: dependant_custom_roles(action)).count
  end

  def dependant_custom_role_repositories_count(action)
    UserRole
    .select(:target_id).distinct
    .where(actor_type: "User", actor_id: org_member_ids, target_type: "Repository", role_id: dependant_custom_roles(action)).count
  end

  def dependant_custom_roles(action)
    Role.lower_custom_role_ids(action: action, org: organization)
  end

  def org_member_ids
    return @org_member_ids if defined?(@org_member_ids)
    @org_member_ids = organization.member_ids
  end

  # returns a hash with values for members_can_create_repos_select_list
  def members_can_create_repo_settings(val)
    {
      all: {
        heading: "Public and private repositories",
        description: "Members will be able to create public and private repositories.",
        value: "all",
      },
      private: {
        heading: "Private repositories",
        description: "Members will be able to create only private repositories.",
        value: "private",
      },
      none: {
        heading: "Disabled",
        description: "Members will not be able to create public or private repositories.",
        value: "none",
      },
    }[val]
  end
end
