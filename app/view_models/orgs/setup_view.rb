# frozen_string_literal: true

class Orgs::SetupView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include ActionView::Helpers::TagHelper

  attr_reader :plan, :organization, :collect_payment_for_trial

  INDUSTRY_OPTIONS = [
    ["", ""],
    "Agriculture & Mining",
    "Business Services",
    "Computers & Electronics",
    "Consumer Services",
    "Education",
    "Energy & Utilities",
    "Financial Services",
    "Food & Beverage",
    "Government",
    "Healthcare",
    "Manufacturing",
    "Media & Entertainment",
    "Not For Profit",
    "Real Estate & Construction",
    "Retail",
    "Software & Internet",
    "Telecommunications",
    "Transportation & Storage",
    "Travel, Recreation, and Leisure",
    "Wholesale & Distribution",
    ["Other", { classes: "js-enterprise-trial-industry"}],
  ]
  EMPLOYEE_SIZE = [["", ""], "0-50", "51-1,000", "1,001-3,000", "3,001-5,000", "5,000+"]
  BILLING_EMAIL_LABELS = { business: "Billing", business_plus: "Work", free: "Contact"}
  DISPLAY_NAME_LABELS = { business: "Team", business_plus: "Enterprise trial", free: "team", enterprise: "Organization"}
  GA_NAME_LABELS = { business: "Team", business_plus: "Enterprise Cloud", free: "Team for Open Source", enterprise: "Enterprise"}

  def ga_plan_name
   GA_NAME_LABELS[plan.name.to_sym]
  end

  def ga_format_input_tracking(label)
    "Signup funnel setup org,form input,text:#{ga_plan_name} - #{label};"
  end

  def description_text
   is_enterprise_cloud_trial? ? "Try GitHub Enterprise Cloud free for 14 days." : nil
  end

  def action_text
    "Set up your #{DISPLAY_NAME_LABELS[plan.name.to_sym]}"
  end

  def button_text
    skip_payment_collection? ? "Next" : "Next: Payment details"
  end

  def form_path(referral_params = {})
    if skip_payment_collection?
      urls.organizations_path
    elsif collect_payment_for_trial?
      urls.org_collect_payment_path
    else
      urls.org_signup_billing_path(referral_params)
    end
  end

  def form_method
    if skip_payment_collection? || collect_payment_for_trial?
      :post
    else
      :get
    end
  end

  def skip_payment_collection?
    return true unless GitHub.billing_enabled?
    return false if collect_payment_for_trial?
    return false if plan.business?

    true
  end

  def collect_payment_for_trial?
    is_enterprise_cloud_trial? && collect_payment_for_trial
  end

  def collect_billing_email?
    !plan.business? && GitHub.billing_enabled?
  end

  def billing_email_label
    "#{BILLING_EMAIL_LABELS[plan.name.to_sym]} email"
  end

  def is_enterprise_cloud_trial?
    plan.business_plus?
  end

  def login_errors?
    organization&.errors && organization.errors[:login].any?
  end

  def login_value
    organization&.login
  end

  def organization_name_hint
    login_errors? || organization.nil? ? "name" : organization&.name
  end

  def email_errors
    organization&.errors && organization.errors[:billing_email]
  end
end
