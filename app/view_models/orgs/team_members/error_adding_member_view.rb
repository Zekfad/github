# frozen_string_literal: true

module Orgs
  module TeamMembers
    class ErrorAddingMemberView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :member, :email, :error, :team

      def safe_profile_name_or_email
        (member && member.safe_profile_name) || email
      end

      def member_without_login?
        member && member.login.blank?
      end

      def login_or_email
        (member && member.login) || email
      end
    end
  end
end
