# frozen_string_literal: true

class Orgs::Metrics::LanguageBarView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization, :language_analysis

  # Public: Returns the most used languages for all of this organization's
  # repositories, private and public, sorted by weight.
  def top_languages
    @top_languages ||= language_analysis.language_summary.
      map { |lang, percentage|
        [Language[lang], percentage]
      }
  end

  def top_languages_cache_key
    ["v1:top_languages", organization.cache_key]
  end

  # This is a proxy class for Linguist::Language.
  # It ensures that "Other" is used for any unrecognized language,
  # (replete with color); and ensures that any known language has a default
  # color (gray) such that the view need not repeatedly handle these fallbacks.
  class Language < DelegateClass(Linguist::Language)
    DEFAULT_COLOR = "#ccc"
    OTHER_COLOR = "#ededed"

    OTHER = Linguist::Language.new(name: "Other", color: OTHER_COLOR)

    def self.[](name)
      new(Linguist::Language[name] || OTHER)
    end

    def color
      super || DEFAULT_COLOR
    end
  end
end
