# frozen_string_literal: true

class Orgs::Invitations::ShowPendingPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :invitation, :invitation_token, :current_external_identity_session
  delegate :organization, :inviter, to: :invitation

  def logout_url
    GitHub.auth.logout_url
  end

  def two_factor_auth_disclosure_url
    "#{GitHub.help_url}/articles/securing-your-account-with-two-factor-authentication-2fa"
  end

  def audit_log_disclosure_url
    "#{GitHub.help_url}/articles/reviewing-the-audit-log-for-your-organization/#search-based-on-the-action-performed"
  end

  def invitation_disclosure_article_url
    "#{GitHub.help_url}/articles/permission-levels-for-an-organization"
  end

  def two_factor_auth_settings_permit_joining?
    organization.two_factor_requirement_met_by?(current_user)
  end

  def sso_required_for_joining?
    # If SAML is enabled on the parent Enterprise, and provisioning is turned on, force user
    # through SSO regardless, so the IdP can validate the invitation. If SAML Provisioning is
    # enabled on the Enterprise, then the SAML assertion we'll receive during SSO will include
    # a `groups` attribute with a list of organizations this user is authorized to be a member
    # of. If this organization isn't in the authorized list, the invitation will be
    # automatically cancelled.
    return true if organization.saml_enabled_on_business? &&
      organization.business.saml_provider.saml_provisioning_enabled?
    return false unless organization.external_identity_session_owner.saml_sso_enforced? ||
      invitation.external_identity.present?

    # if a valid external identity session was provided, don't force user to SSO
    # a second time
    current_external_identity_session.nil?
  end

  def idm_saml_initiate_url
    if organization.business&.saml_sso_enabled?
      urls.idm_saml_initiate_enterprise_path(organization.business, invitation_token: invitation_token)
    else
      urls.org_idm_saml_initiate_path(organization, invitation_token: invitation_token)
    end
  end
end
