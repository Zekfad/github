# frozen_string_literal: true

class Orgs::Invitations::InviteeSuggestionsView < AutocompleteView
  # this pattern is similar to User::EMAIL_REGEX but is modified to find any and all valid email addresses in the query
  BULK_EMAIL_PATTERN = /([^@\s.\0][^@\s\0]*@\[?[a-z0-9.-]+\]?)[,\s]?/i

  def suggested_invitees
    suggestions.map do |user|
      [user, SuggestedInvitee.new(organization, user, current_user)]
    end
  end

  def bulk_email_query?
    return false unless bulk_email_invites_enabled?
    email_addresses.length > 1
  end

  def email_addresses
    @email_addresses ||= query.scan(BULK_EMAIL_PATTERN).flatten.uniq
  end

  # Overrides `AutocompleteView#org_members_only?` to include check for org
  # belongs to a business and has no seats remaining to scope results to
  # business members
  def org_members_only?
    org_members_only || business_members_only?
  end

  def business_members_only?
    return false unless organization.business.present?
    (!GitHub.single_business_environment? && organization.available_seats.zero?)
  end

  def email_match?(user)
    email_query? && user.profile.email == query
  end

  def email_query_autocomplete_label
    return query unless bulk_email_invites_enabled?

    n = email_addresses.length

    if n > 1
      "#{n} #{"email address".pluralize(n)}"
    else
      email_addresses.first
    end
  end

  def bulk_email_invites_enabled?
    GitHub.flipper[:bulk_email_invites].enabled?(current_user)
  end

  class SuggestedInvitee
    include OcticonsCachingHelper

    def initialize(organization, user, inviter)
      @user_direct_or_team_member = organization.direct_or_team_member?(user)
      @blocked_from_org = organization.blocking?(user)
      @blocked_warning = inviter.blocking?(user)
      @valid = Organization::InviteStatus.new(organization, user, inviter: inviter).valid?

      # bypass_org_invites_enabled? currently is aliased to enterprise?
      # On enterprise there are no org invites and instead org admins directly
      # add users to the org
      if GitHub.bypass_org_invites_enabled?
        @two_factor_not_enabled_error = !organization.two_factor_requirement_met_by?(user)
        @valid = @valid && !@two_factor_not_enabled_error
      end
    end

    attr_accessor :valid, :two_factor_not_enabled_error, :user_direct_or_team_member, :blocked_from_org, :blocked_warning

    # Public: Should the suggestion item for the specified user be enabled?
    #
    # Returns a boolean.
    alias_method :item_enabled?, :valid

    # Public: Can the specified user be invited to the organization?
    #
    # Returns a boolean.
    alias_method :invitable?, :valid

    # Public: Does the user meet the organization's two factor auth requirement?
    #
    # Returns a boolean.
    alias_method :two_factor_not_enabled_error?, :two_factor_not_enabled_error

    # Public: Is the user a direct or team member of the organization?
    #
    # Returns a boolean.
    alias_method :user_direct_or_team_member?, :user_direct_or_team_member

    # Public: Is the invitee blocked by the organization
    #
    # Returns a boolean.
    alias_method :blocked_from_org?, :blocked_from_org

    # Public: Is the invitee blocked by the inviter of the organization
    #
    # Returns a boolean.
    alias_method :blocked_warning?, :blocked_warning

    # Public: Get text explaining why the user cannot be invited
    #
    # Returns a string (empty if the user *is* invitable).
    def uninvitable_reason_text
      case
      when user_direct_or_team_member?
        "Already in this organization"
      when two_factor_not_enabled_error?
        "User needs to enable two-factor authentication"
      when blocked_from_org?
        "This user is blocked by the organization"
      else
        ""
      end
    end

    # Public: Get the text explaining what a user will be warned about before inviting
    #
    # Returns a string or nil if there is not a reason
    def warning_reason_text
      case
      when blocked_warning?
        "You have blocked this user. Proceed anyway?"
      else
        nil
      end
    end

    # Public: Get the CSS octicon classes to use in the suggestion item
    #
    # Returns a string.
    def octicon_html
      octicon(octicon_class, class: "float-right") if octicon_class
    end

    def octicon_class
      case
      when two_factor_not_enabled_error? || blocked_from_org?
        nil
      when invitable? || blocked_warning?
        "plus"
      else
        "check"
      end
    end
  end
end
