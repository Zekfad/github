# frozen_string_literal: true

class Orgs::Invitations::PendingMembersView < Orgs::OverviewView
  attr_reader :organization, :page, :rate_limited

  def initialize(**args)
    super(args)
    @context = args[:context] || :modal
    @invitations = args[:invitations]
    @organization = args[:organization]
  end

  def page_title
    "People · #{organization.safe_profile_name}"
  end

  def invitations
    modal? ?
      @invitations.take(Orgs::People::IndexPageView::MAX_MODAL_INVITATIONS) :
      @invitations.paginate(page: page, per_page: 30)
  end

  def total_count
    @invitations.length
  end

  def table_css_class
    modal? ?
      "scrollable-overlay" :
      "table-list-bordered"
  end

  def show_admin_stuff?
    organization.adminable_by?(current_user)
  end

  private

  def modal?
    @context == :modal
  end
end
