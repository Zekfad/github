# frozen_string_literal: true

class Orgs::Repositories::TeamSelectionView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization, :page, :team_ids, :ldap_mapped_teams_selectable

  TEAM_SUGGESTIONS_PAGE_SIZE = 10

  # Which teams are available to this user
  # to offer access to the repo at new organization destination
  #
  # Returns array of Team objects
  def teams
    if paginate?
      @teams = teams_scope.paginate(page: page, per_page: TEAM_SUGGESTIONS_PAGE_SIZE)
    else
      @teams = teams_scope.limit(TEAM_SUGGESTIONS_PAGE_SIZE)
    end
    @teams
  end

  def is_team_selected?(team_id)
    team_ids&.include?(team_id.to_s)
  end

  def ldap_teams?
    teams.any?(&:ldap_mapped?)
  end

  def selected_team_ids
    return "" unless team_ids
    team_ids.map(&:to_i).uniq.join(",")
  end

  def total_teams_count
    @count ||= teams_scope.count
  end

  def paginate?
    total_teams_count > TEAM_SUGGESTIONS_PAGE_SIZE
  end

  private

  def teams_scope
    @teams_scope ||= organization.visible_teams_for(current_user)
  end
end
