# frozen_string_literal: true

class Orgs::CreationView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :coupon, :per_seat_pricing_model, :plan, :organization,
    :annual_per_seat_pricing_model, :monthly_per_seat_pricing_model,
    :annual_business_plus_pricing_model, :monthly_business_plus_pricing_model

  def show_only_per_seat_plan?
    coupon && per_seat_pricing_model.final_price.zero?
  end

  def current_plan_duration
    per_seat_pricing_model.plan_duration
  end

  def current_plan_duration_adjective
    "#{per_seat_pricing_model.plan_duration}ly"
  end

  def available_plan_duration
    per_seat_pricing_model.monthly_plan? ? User::YEARLY_PLAN : User::MONTHLY_PLAN
  end

  def available_plan_duration_adjective
    "#{available_plan_duration}ly"
  end

  def button_text
    "Next: Customize your setup"
  end

  def form_path
    urls.organizations_path
  end
end
