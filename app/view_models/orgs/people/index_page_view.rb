# frozen_string_literal: true

class Orgs::People::IndexPageView < Orgs::OverviewView
  MAX_MODAL_INVITATIONS = 90
  MEMBERS_PER_PAGE = 30

  attr_reader :organization, :page, :query, :role

  def page_title
    "People · #{organization.safe_profile_name}"
  end

  def members
    return @members if defined? @members

    @members = found_members.paginate(page: page, per_page: MEMBERS_PER_PAGE)
  end

  def outside_collaborators_view
    Orgs::People::OutsideCollaboratorsView.new(
      organization: organization,
      page: page,
      query: query,
    )
  end

  def show_toolbar?
    !show_no_members_for_non_member?
  end

  def show_no_results?
    query.present? && no_members?
  end

  def show_no_members_for_non_member?
    no_members? &&
      !people_query.searching? &&
      !@role &&
      !organization.direct_or_team_member?(current_user)
  end

  def show_members?
    members.present?
  end

  def no_members?
    members.empty?
  end

  def show_buy_seats_link?
    organization.plan.per_seat? && !organization.has_unlimited_seats? && organization.adminable_by?(current_user)
  end

  def show_make_direct_members_help?
    !current_user.dismissed_notice?("make_direct_members") && organization.adminable_by?(current_user)
  end

  def show_org_membership_banner?
    return unless logged_in?
    return if current_user.dismissed_notice?("org_membership_banner")

    # Only show the banner to non-owners.
    organization.direct_member?(current_user) && !organization.adminable_by?(current_user)
  end

  # Public: Should we show the invitations section?
  #
  # Returns a boolean.
  def show_pending_invitations_view_more?
    pending_non_manager_invitations.size > MAX_MODAL_INVITATIONS
  end

  # Returns the `selected` class for the `select-menu-item` filter options
  # when the provided filter option matches the selected filter.
  def role_filter_select_class(filter)
    "selected" if role_filter_selected == filter
  end

  # Returns the `selected` class for the `select-menu-item` filter options
  # when the provided filter option matches the selected filter.
  def two_factor_filter_select_class(filter)
    "selected" if two_factor_filter_selected == filter
  end

  # Returns the `selected` class for the `select-menu-item` filter options
  # when the provided filter option matches the selected filter.
  def sso_filter_select_class(filter)
    "selected" if sso_filter_selected == filter
  end

  # Public: Returns a Hash of organization member IDs and an Array of their
  #         verified domain emails.
  #
  # Returns a Hash{Integer => Array[UserEmail]}
  def verified_domain_emails_for_members
    return @verified_domain_emails_for_members if defined?(@verified_domain_emails_for_members)

    member_ids = members.map(&:id)
    promises = members.map { |member| member.async_verified_domain_emails_for(organization) }
    results = Promise.all(promises).sync.flatten.group_by(&:user_id)

    @verified_domain_emails_for_members = member_ids.each_with_object(results) do |member_id, result|
      result[member_id] ||= []
    end
  end

  def members_count
    @members_count ||= found_members.size
  end

  private

  # Returns a Symbol for the queried role.
  def role_filter_selected
    @role_filter_selected ||= people_query.role_match || :all
  end

  # Returns a Symbol for the selected 2FA scope queried.
  def two_factor_filter_selected
    @two_factor_filter_selected ||=
      case
      when people_query.two_factor_enabled_scope?
        :enabled
      when people_query.two_factor_disabled_scope?
        :disabled
      else
        :all
      end
  end

  # Returns a Symbol for the selected SSO scope queried.
  def sso_filter_selected
    @sso_filter_selected ||=
      case
      when people_query.external_identity_linked_scope?
        :linked
      when people_query.external_identity_unlinked_scope?
        :unlinked
      else
        :all
      end
  end

  def people_query
    @people_query ||= Organization::People::Query.new(
      query: query,
      organization: organization,
      current_user: current_user,
      role: role,
    )
  end

  def found_members
    @found_members ||= begin
      users = Organization::People::Filter.new(query: people_query).call
      Organization::People::Search.new(query: people_query, users: users).call
    end
  end
end
