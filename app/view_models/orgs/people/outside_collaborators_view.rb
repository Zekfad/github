# frozen_string_literal: true

class Orgs::People::OutsideCollaboratorsView < Orgs::OverviewView

  TWO_FACTOR_DISABLED_QUERY = "two-factor:disabled"
  TWO_FACTOR_ENABLED_QUERY = "two-factor:enabled"

  # The amount of records to show per page
  PER_PAGE = 30

  # If there are more than this many pages of records, use next/prev links
  # instead of individual page links
  USE_NEXT_PREV_AFTER_PAGES = 50

  attr_reader :organization, :page, :query

  def two_factor_enabled_for_user?(user)
    user.two_factor_authentication_enabled?
  end

  def outside_collaborators
    return @outside_collaborators if defined?(@outside_collaborators)

    scope = add_two_factor_scope(organization.outside_collaborators)

    if cleaned_query.present?
      scope = scope.where(["users.login LIKE :query", { query: "%#{cleaned_query}%" }])
    end

    page_links_cutoff = USE_NEXT_PREV_AFTER_PAGES * PER_PAGE
    count_limit = [page, USE_NEXT_PREV_AFTER_PAGES].max * PER_PAGE + 1
    count = scope.limit(count_limit).count
    @has_too_many_pages = count > page_links_cutoff

    @outside_collaborators = scope.order(:login).paginate(page: page, per_page: PER_PAGE, total_entries: count)
  end

  def use_page_links?
    outside_collaborators # load data if not loaded
    !@has_too_many_pages
  end

  def repository_invitations_count
    @repository_invitations ||= organization.repository_invitations.size
  end

  def repositories_by_user_id_count(user_id)
    unless defined?(@repositories_by_user_id_count)
      @repositories_by_user_id_count = {}
    end
    if @repositories_by_user_id_count.has_key?(user_id)
      return @repositories_by_user_id_count[user_id]
    end
    @repositories_by_user_id_count[user_id] = organization.collaborating_repository_count_for([user_id])[user_id]
  end

  def show_no_results?
    query.present? && no_outside_collaborators?
  end

  # Returns the `selected` class for the `select-menu-item` filter options
  # when the provided filter option matches the selected filter.
  def two_factor_filter_select_class(filter)
    "selected" if two_factor_filter_selected == filter
  end

  def show_repository_invitations?
    GitHub.repo_invites_enabled? && repository_invitations_count > 0
  end

  private

  def no_outside_collaborators?
    outside_collaborators.empty?
  end

  def add_two_factor_scope(scope)
    if two_factor_disabled_scope?
      scope = scope.includes(:two_factor_credential).where("two_factor_credentials.id IS NULL").references(:two_factor_credential)
    end

    if two_factor_enabled_scope?
      scope = scope.includes(:two_factor_credential).where("two_factor_credentials.id IS NOT NULL").references(:two_factor_credential)
    end

    scope
  end

  def two_factor_disabled_scope?
    query.to_s.include?(TWO_FACTOR_DISABLED_QUERY) && organization.adminable_by?(current_user)
  end

  def two_factor_enabled_scope?
    query.to_s.include?(TWO_FACTOR_ENABLED_QUERY) && organization.adminable_by?(current_user)
  end

  # Returns the query with filters (like "two-factor:disabled") stripped out.
  def cleaned_query
    q = query.to_s.gsub(TWO_FACTOR_DISABLED_QUERY, "").strip
    q = q.to_s.gsub(TWO_FACTOR_ENABLED_QUERY, "").strip

    ActiveRecord::Base.sanitize_sql_like(q)
  end

  # Returns a Symbol for the selected 2FA scope queried.
  def two_factor_filter_selected
    @two_factor_filter_selected ||=
      case
      when two_factor_enabled_scope?
        :enabled
      when two_factor_disabled_scope?
        :disabled
      else
        :all
      end
  end
end
