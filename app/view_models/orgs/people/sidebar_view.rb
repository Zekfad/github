# frozen_string_literal: true

class Orgs::People::SidebarView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Orgs::People::RoleDescriptionMethods
  include Orgs::People::RoleNameMethods
  include Orgs::People::VerifiedEmailsMethods

  attr_reader :organization, :person, :repositories_count, :editable

  def role
    @role ||= Organization::Role.new(organization, person)
  end

  def repositories_count # rubocop:disable Lint/DuplicateMethods
    @repositories_count ||= organization.repositories_associated_with(person).size
  end

  def editable?
    !!editable
  end

  # Public: Is this member's org membership publicized?
  #
  # Returns a boolean.
  def public_member?
    organization.public_member?(person)
  end

  # Public: Can this user change this person's role?
  def can_change_role?
    return false unless editable?

    role.can_be_modified_by?(current_user) && !role.outside_collaborator?
  end

  # Public: Can this user publicize or conceal their membership in this org?
  def can_change_visibility?
    return false unless editable?

    if organization.public_member?(person)
      organization.can_conceal_memberships?(current_user, members: [person])
    else
      organization.can_publicize_memberships?(current_user, members: [person])
    end
  end

  def can_migrate_to_collaborator?
    repositories_count > 0
  end

  def show_billing_manager?
    GitHub.billing_enabled? && organization.billing_manager?(person)
  end

  # Public: Should the 2FA status for the person be shown? Only if 2FA is
  # enabled for the authentication system being used.
  #
  # Returns a Boolean
  def show_two_factor_status?
    GitHub.auth.two_factor_authentication_enabled?
  end

  def show_sso_status?
    organization.saml_sso_present?
  end

  def sso_status
    return "" unless show_sso_status?

    if organization.external_identity_session_owner.saml_provider.external_identities.linked_to(person).exists?
      "SAML identity linked"
    else
      "No SAML identity linked"
    end
  end
end
