# frozen_string_literal: true

module Orgs
  module People
    class MemberView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Orgs::People::RoleDescriptionMethods
      include Orgs::People::RoleNameMethods
      include Orgs::People::VerifiedEmailsMethods
      include BundledLicenseAssignmentHelper

      attr_reader :member
      attr_reader :organization
      attr_reader :verified_domain_emails

      def team_count
        organization.teams_for(member, viewer: current_user).size
      end

      def direct_or_team_member?
        return @direct_or_team_member if defined?(@direct_or_team_member)
        @direct_or_team_member = organization.direct_or_team_member?(current_user)
      end

      # Public: Is this member's org membership publicized?
      #
      # Returns a boolean.
      def public_member?
        organization.public_member?(member)
      end

      # Public: Should admin controls and info be shown in this view?
      #
      # Returns a boolean.
      def show_admin_stuff?
        organization.adminable_by?(current_user)
      end

      # Public: The URL representing this organization member.
      #
      # Returns a string path
      def member_url
        if show_admin_stuff?
          urls.org_person_path(organization, member)
        else
          urls.user_path(member)
        end
      end

      # Public: Should we show a warning about 2FA being disabled
      #
      # Returns a boolean.
      def member_two_factor_enabled?
         member.two_factor_authentication_enabled?
      end

      # Public: Should we show 2fa information
      #
      # Returns a boolean.
      def show_2fa?
        show_admin_stuff? && GitHub.auth.two_factor_authentication_enabled?
      end

      # Public: Should we display the verified domain emails for each user?
      #
      # Returns a Boolean.
      def show_verified_emails?
        show_admin_stuff? && super
      end

      # Public: The verified domain email for this user to display.
      #
      # Returns a String | nil
      def verified_domain_email
        verified_domain_emails.first
      end

      # Public: Should we show the link to view more verified domain emails
      #         for this user?
      #
      # Returns a Boolean.
      def show_more_emails_link?
        verified_domain_emails.count > 1
      end

      # Public: Can this user publicize or conceal their membership in this org?
      def can_change_visibility?
        if organization.public_member?(member)
          organization.can_conceal_memberships?(current_user, members: [member])
        else
          organization.can_publicize_memberships?(current_user, members: [member])
        end
      end

      # Public: Can we change the given license? Based on the organization's
      # business having a volume enterprise agreement.
      #
      # Returns a Boolean
      def can_change_license_type?
        organization&.business&.volume_licensing_enabled? && !bundled_license_assignments_enabled?(organization.business)
      end

      def enterprise_licenses
        @enterprise_licenses ||= Businesses::EnterpriseLicensesView.for_business(organization.business)
      end

      def volume_licenses
        @volume_licenses ||= Businesses::VolumeLicensesView.for_business(organization.business)
      end

      # Public: Return's this user's license for the current organization's
      # business
      #
      # Returns a UserLicense
      def user_license
        @_user_license ||= member.user_licenses.find_by(business: organization.business) || member.user_licenses.build
      end

      # Public: What's this member's role?
      #
      # Returns an Organization::Role.
      def role
        @role ||= Organization::Role.new(organization, member)
      end
    end
  end
end
