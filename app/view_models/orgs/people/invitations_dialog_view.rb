# frozen_string_literal: true

class Orgs::People::InvitationsDialogView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization, :redirect_to_path, :selected_invitations, :action_dialog

  CONJUGATED_VERB = {
    cancel: {
      present_participle: "Cancelling ",
      past: "cancelled",
      },
    delete: {
      present_participle: "Deleting ",
      past: "deleted",
      },
    retry: {
      present_participle: "Retrying ",
      past: "retried",
    },
  }

  def selected_invitation_ids
    selected_invitations.map(&:id)
  end
end
