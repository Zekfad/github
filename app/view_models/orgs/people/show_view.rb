# frozen_string_literal: true

class Orgs::People::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Orgs::People::RoleDescriptionMethods
  include Orgs::People::RoleNameMethods

  attr_reader :organization
  attr_reader :paginated_repositories
  attr_reader :person
  attr_reader :repositories_count

  def page_title
    "#{person}'s access for #{organization}"
  end

  def role
    @role ||= Organization::Role.new(organization, person)
  end

  def show_admin_notice?
    role.admin?
  end

  def show_search_form?
    !Organization::RepositoryFilter.too_many_repos?(repositories_count)
  end

  # Public: Should the 2FA status for the person be shown? Only if 2FA is
  # enabled for the authentication system being used.
  #
  # Returns a Boolean
  def show_two_factor_status?
    GitHub.auth.two_factor_authentication_enabled?
  end
end
