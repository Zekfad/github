# frozen_string_literal: true

module Gists
  # Controls the gist new/create view
  # eg https://github.com/gists/
  class EditPageView < NewPageView

    attr_reader :gist

    delegate :owner, :description, :visibility, :files_for_view, :user_param, to: :gist

    def page_title
      "Editing #{gist.title}"
    end

    # What objects does the form_for tag operate on?
    def form_objects
      [owner, gist]
    end

    # Should we show a warning banner?
    #
    # return false if this is an anonymous session
    # return false if the current_user isn't a site admin
    # return true if the current_user is not the owner of the gist
    def show_site_admin_edit_warning?
      return false unless logged_in?
      return false unless current_user.site_admin?
      current_user != owner
    end
  end
end
