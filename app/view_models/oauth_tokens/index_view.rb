# frozen_string_literal: true

class OauthTokens::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :tokens

  def sso_organizations
    @sso_organizations ||=
      Platform::Authorization::SAML.new(user: current_user).saml_organizations.to_a
  end

  # Public: Returns a nested list of credential authroizations first
  # grouped by tokens, then further grouped by the organization. These
  # groups may be sparsely populated.
  #
  # Example:
  #
  #   credential_map_by_token
  #   # => {
  #     #<OauthAccess id:1> => {
  #       #<Organization login: github> => #<CredentialAuthorization id:1>,
  #       #<Organization login: acme>   => nil
  #     },
  #     #<OauthAccess id:2> => {
  #       #<Organization login: github> => nil,
  #       #<Organization login: acme>   => #<CredentialAuthorization id:1>
  #     }
  #     #<OauthAccess id:3> => {
  #       #<Organization login: github> => nil,
  #       #<Organization login: acme>   => nil
  #     }
  #   }
  #
  # Returns a Hash.
  def credential_map_by_token
    @by_token ||= tokens.each_with_object({}) do |token, by_token|
      by_token[token] = sso_organizations.each_with_object({}) do |org, by_org|
        by_org[org] = credential_authorizations.find do |auth|
          auth.organization == org && auth.credential == token
        end
      end
    end
  end

  private

  def credential_authorizations
    tokens_ids = tokens.pluck(:id)
    @credential_authorizations ||= Organization::CredentialAuthorization.
      by_organization_credential(organization: sso_organizations, credential: tokens_ids).
      all
  end
end
