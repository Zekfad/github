# frozen_string_literal: true

module Users
  class MarketplaceNoticesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :pending_installations_notice

    def initialize(current_user:, user_session:)
      @current_user = current_user

      @pending_installations_notice = Marketplace::PendingInstallations::Notice.new \
        user_id: @current_user.id
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def triggered_order_previews?
      @triggered_order_previews ||= current_user.
        marketplace_order_previews.
        with_valid_listing_data.
        retargeting_notice_triggered.
        exists?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def dismissed_retargeting_notice?
      @dismissed_retargeting_notice ||= current_user.dismissed_notice?("marketplace_retargeting")
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def retargeting_notice_displayable?
      !dismissed_retargeting_notice? && triggered_order_previews?
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def triggered_pending_installations?
      @triggered_pending_installations ||= current_user.
        pending_marketplace_installations(only_notice_triggered: true).
        exists?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def pending_installations_notice_displayable?
      !pending_installations_notice.dismissed? && triggered_pending_installations?
    end
  end
end
