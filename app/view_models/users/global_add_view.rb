# frozen_string_literal: true

class Users::GlobalAddView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :repository, :this_user

  def show_org_links?
    return false unless organization
    return false if organization.new_record?
    organization.adminable_by? current_user
  end

  def show_new_issue_link?
    repository && repository.has_issues?
  end

  def show_new_collaborator_link?
    repository && repository.adminable_by?(current_user)
  end

  def show_this_repository?
    repository &&
    (show_new_issue_link? || show_new_collaborator_link?)
  end

  # Public: Show the import repository link?
  #
  # Returns true or false.
  def show_repository_import_link?
    GitHub.porter_available?
  end

  def organization
    attributes[:organization] || repository_organization || this_user_organization
  end

  private

  def repository_organization
    repository && repository.owner.organization? && repository.owner
  end

  def this_user_organization
    this_user && this_user.organization? && this_user
  end
end
