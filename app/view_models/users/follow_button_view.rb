# frozen_string_literal: true

module Users
  # This is a view model that provides the same interface as the
  # Views::Users::FollowButton::UserFragment
  class FollowButtonView
    attr_reader :user, :current_user

    # user         - The User you're viewing.
    # current_user - The User that's logged in.
    def initialize(user, current_user)
      @user = user
      @current_user = current_user
    end

    def viewer_can_follow?
      current_user.can_follow?(user)
    end

    def viewer_is_following?
      current_user.following?(user)
    end

    def is_viewer?
      user == current_user
    end

    def login
      user.login
    end
  end
end
