# frozen_string_literal: true

module Users
  # Controls the repositories view for a user,
  # eg https://github.com/github/repositories
  class RepositoriesView
    attr_reader :user, :current_user, :page

    INCLUDES = [:mirror, :primary_language, :owner, :parent, :topics]

    # user         - The User you're viewing.
    # current_user - The User that's logged in.
    # page         - The String page that you're looking at.
    def initialize(user, current_user, page = 1)
      @user = user
      @current_user = current_user
      @page = page
    end

    def me?
      user == current_user
    end

    def repos
      @repos ||= if user.organization?
        org_repos
      else
        user_repos
      end
    end

    def user_repos
      if me?
        user.repositories.includes(INCLUDES).order("name ASC").page(page)
      else
        user.public_repositories.includes(INCLUDES).order("name ASC").page(page)
      end
    end

    def org_repos
      (org = user).visible_repositories_for(current_user).includes(INCLUDES).order("name ASC").page(page)
    end
  end
end
