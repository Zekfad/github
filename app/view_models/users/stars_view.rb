# frozen_string_literal: true

module Users
  class StarsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :user, :viewer, :current_page, :sort, :direction, :language,
                :phrase

    SORT_OPTIONS = {
      "Recently starred" => ["created", "desc"],
      "Recently active"  => ["updated", "desc"],
      "Most stars"       => ["stars",   "desc"],
    }.freeze

    def initialize(**args)
      super(args)

      @sort ||= "created"

      default_direction = "desc"
      @direction ||= default_direction
      @direction = default_direction unless %w(asc desc).include?(@direction)
    end

    # Public: Returns true if the user is searching or filtering starred
    # repositories.
    def filtering?
      language.present? || phrase.present?
    end

    def current_sort
      SORT_OPTIONS.key([sort, direction]) || "Recently starred"
    end

    # Public: Whether or not the user is viewing their own profile.
    # Returns a Boolean.
    def me?
      user == viewer
    end

    # Public: Should a 'Language' select menu be shown to the user for filtering
    # repository stars?
    def show_language_filter?
      user_languages_and_counts.any?
    end

    # Public: The cached counts of the languages in a user's starred repos.
    #
    # Returns a hash of form {:language_name => count}.
    def user_languages_and_counts
      key = "user:repositories:starred_count:#{user.login}".dup
      key << ":public" unless me?
      GitHub.cache.fetch(key, ttl: 5.minutes) do
        user_languages!
      end
    end

    # Public: Returns alphabetized list of languages for the user's starred
    # repositories.
    def user_languages
      user_languages_and_counts.keys.sort
    end

    def selected_language
      language_names = user_languages
      index = language_names.map(&:downcase).index(language)
      index ? language_names[index] : "All languages"
    end

    def selected?(current_direction, current_sort)
      direction == current_direction && sort == current_sort
    end

    private

    # Private: The counts of the languages in a user's starred repos.
    #
    # Returns a hash of form {:language_name => count}.
    def user_languages!
      if me?
        user.starred_repositories_by_language
      else
        user.starred_public_repositories_by_language
      end
    end
  end
end
