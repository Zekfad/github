# frozen_string_literal: true

module Files
  # Controls the overview page for a repository.
  # eg https://github.com/mozilla/rust
  class OverviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include HydroHelper
    include LanguageHelper
    include PlatformHelper
    include TextHelper

    NUMBER_OF_BLOCKED_CONTRIBUTORS_TO_SHOW = 5

    # Owner ids for repositories that should not have the Used By button
    USED_BY_ORG_BLOCKLIST = [
      139426, # angular org
    ].freeze
    # repository ids that should not have the Used By button
    USED_BY_REPO_BLOCKLIST = [
      44124366, # yahoo/elide
    ].freeze

    attr_reader :user, :repository, :page_title, :repository_data,
                :source_url, :location, :community_contributors_enabled

    RepositoryFragment = parse_query <<-'GRAPHQL'
      fragment on Repository {
        isPrivate
        viewerCanPush
        hasListableAction
        listedAction {
          id
          slug
        }
        interactionAbility {
          origin
        }
        ...Views::Topics::List::Repository
        ...Views::Community::OrgInteractionLimitsBanner::Node
      }
    GRAPHQL

    def after_initialize
      @repository_data = RepositoryFragment.new(@repository_data)
    end

    # Can the person currently logged in
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def can_edit_metadata?
      @can_edit_metadata ||= begin
        return false if repository.locked_on_migration?
        return false if repository.archived?
        repository.can_edit_repo_metadata?(user)
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def show_used_by_sidebar?
      # Disable for popular orgs using scoped packages which are not currently
      # ingested in the dependency graph.
      # Revert once https://github.com/github/dependency-graph-api/issues/930
      # is implemented
      return false if USED_BY_ORG_BLOCKLIST.include?(repository.owner_id)
      # Disabled due to this issue https://github.com/github/customer-feedback/issues/2226
      return false if USED_BY_REPO_BLOCKLIST.include?(repository.id)

      repository.used_by_enabled?
    end

    # Generate an HTML formatted repository description. Also adds a link to
    # jump to the README if applicable.
    #
    # Returns the HTML description.
    def formatted_description
      @formatted_description ||= formatted_repo_description(repository)
    end

    # Should we show the description?
    def show_description?
      !repository.description.blank?
    end

    def any_topics?
      repository.applied_repository_topics.any?
    end

    def can_see_deployments?
      repository.can_see_deployments?(user)
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def can_add_topics?
      @can_add_topics ||= begin
        return false if repository.locked_on_migration?
        return false if repository.archived?
        repository.can_manage_topics?(user)
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def can_manage_topics?
      can_add_topics? && any_topics?
    end

    def any_mention_of_topics?
      can_add_topics? || any_topics?
    end

    # What's the website the user has linked to for this repository?
    #
    # Returns a String of the URL to autolink.
    def website
      repository.homepage.to_s
    end

    # Should we show the website?
    def show_website?
      !website.blank?
    end

    # Should we show the Read More link at the end of a repository's
    # description? A description and README should exist to show this.
    #
    # Returns boolean whether to show the read more link.
    def show_read_more?
      !repository.description.blank? && true # README_EXISTS
    end

    # Should we show the editing mode by default? We do this when there isn't
    # any content to display and the person has the correct permissions to
    # edit the description.
    def show_editing_meta?
      can_edit_metadata? && !(show_description? || show_website?)
    end

    def top_languages
      @top_languages ||= repository.top_languages_summarized
    end

    def show_languages?
      top_languages.any?
    end

    def show_community_profile?
      @show_community_profile ||= show_community_profile
    end

    def community_profile_health_percentage
      repository.community_profile.health_percentage
    end

    def packages_sidebar_section_enabled?
      return @packages_sidebar_section_enabled if defined?(@packages_sidebar_section_enabled)

      @packages_sidebar_section_enabled = repository.sidebar_section_enabled?(:packages)
    end

    def environments_sidebar_section_enabled?
      return @environments_sidebar_section_enabled if defined?(@environments_sidebar_section_enabled)

      @environments_sidebar_section_enabled = repository.sidebar_section_enabled?(:environments)
    end

    def releases_sidebar_section_enabled?
      return @releases_sidebar_section_enabled if defined?(@releases_sidebar_section_enabled)

      @releases_sidebar_section_enabled = repository.sidebar_section_enabled?(:releases)
    end

    def show_environments_sidebar?
      environments_sidebar_section_enabled? &&
      can_see_deployments?
    end

    def show_packages_sidebar?
      packages_sidebar_section_enabled? &&
      show_packages? &&
      (repository_packages_count > 0 || repository.writable_by?(current_user))
    end

    def show_packages?
      !repository.nil? && PackageRegistryHelper.show_packages?
    end

    def show_packages_popover?
      return false unless repository

      show_packages? &&
        !repository.archived? &&
        repository.writable_by?(current_user) &&
        !packages_onboarding_popover_dismissed? &&
        repository_packages_count == 0 &&
        !actions_banner_showing? # Actions banners have priority
    end

    # Public: The cached count of the repository's packages.
    #
    # Returns: Int
    def repository_packages_count
      key = "repository:packages:count:#{repository.id}"
      GitHub.cache.fetch(key, ttl: 4.hours) do
        repository.packages.joins(:package_versions).merge(Registry::PackageVersion.not_deleted).distinct.count
      end
    end

    # Public: The cached count of the repository's contributors.
    #
    # Returns: Int
    def contributor_count
      key = "repository:contributor:count:v1:#{repository.id}:#{repository.updated_at.to_i}"
      @contributor_count ||= GitHub.cache.fetch(key) do
        CommitContribution.contributors_count_for_repository(repository)
      end
    end

    # Public: The cached count of sponsorable links for the repository
    #
    # Returns: Int
    def sponsorable_count
      key = "repository:sponsorable:count:#{repository.id}:#{repository.updated_at.to_i}"
      @sponsorable_count ||= GitHub.cache.fetch(key) do
        repository.funding_links.sponsorable_users_ids.count +
          repository.funding_links.sponsorable_orgs_ids.count
      end
    end

    def latest_release
      return @latest_release if defined?(@latest_release)
      @latest_release = Release.latest_for(repository)
    end

    def release_count
      return @release_count if defined?(@release_count)
      @release_count = repository.tags.size
    end

    def latest_release_published_at
      latest_release.published_at || latest_release.created_at
    end

    # Public: Checks if the user has dismissed the packages_onboarding_prompt notice.
    #
    # Returns a Boolean.
    def packages_onboarding_popover_dismissed?
      return @packages_onboarding_popover_dismissed if defined?(@packages_onboarding_popover_dismissed)
      @packages_onboarding_popover_dismissed = user.dismissed_notice?("packages_onboarding_prompt")
    end

    def show_open_source_license?
      repository &&
        repository.license &&
        license_path.present? &&
        repository.license.spdx_id.present?
    end

    def blocked_contributors
      @blocked_contributors ||= [] unless show_blocked_contributors_warning?
      @blocked_contributors ||= repository.blocked_contributors_for(user)
      return [] if @blocked_contributors.empty?

      GitHub.instrument(
        "user.repo_visit_with_blocked_contributors",
        actor: current_user,
        repository: repository,
      )

      @blocked_contributors
    end

    def show_blocked_contributors_warning?
      return false unless user && repository
      return false unless user.show_blocked_contributors_warning?
      return false if repository.owner == user
      return false if repository.contributor_ids.include?(user.id)
      true
    end

    def license_path
      @license_path ||= repository.preferred_license.try(:path)
    end

    # Public: Checks to see if the org level repository interaction limit banner
    # should be displayed.
    #
    # Returns a Boolean.
    def show_interaction_limits_banner?
      return false if repository_data.is_private
      return false unless interactions = repository_data.interaction_ability

      interactions.origin == "ORGANIZATION"
    end

    def show_publish_action_banner?
      @show_publish_action_banner ||= show_publish_action_banner
    end

    def show_use_action_banner?
      @show_use_action_banner ||= show_use_action_banner
    end

    def action_slug
      repository_data.listed_action&.slug
    end

    def action_link_hydro_attrs
      payload = {
        repository_action_id: repository_data.listed_action&.id,
        source_url: source_url,
        location: location,
      }

      hydro_click_tracking_attributes("marketplace.action.click", payload)
    end

    private

    # Private: We want to give priority to any Actions popover or banner.
    # This helper returns true if:
    # - an Actions popover/banner is currently showing
    # - the Actions CI popover was dismissed less than 24 hours ago
    #
    # Returns: Boolean
    def actions_banner_showing?
      return true if show_publish_action_banner?
      return true if show_use_action_banner?

      dismissed_at = current_user.notice_dismissed_at("github_actions_ci_repository_banner")
      # Actions banner is not shown and has not been dismissed, safe to show other popovers
      return false if dismissed_at.nil?
      Time.now.utc - dismissed_at < 1.day
    end

    def show_publish_action_banner
      return false unless logged_in?
      return false if GitHub.enterprise?
      return false if current_user.dismissed_notice?(UserNotice::PUBLISH_ACTION_FROM_REPO_NOTICE)
      return false unless repository_data.viewer_can_push?
      return false unless repository_data.has_listable_action?
      return false if repository_data.listed_action

      true
    end

    def show_use_action_banner
      return false if GitHub.enterprise?
      return false unless repository_data.listed_action

      true
    end

    def show_community_profile
      return false unless GitHub.flipper[:community_profile_sidebar].enabled?(current_user)
      return false if repository.community_profile.nil?
      CommunityProfile.eligible_repository?(repository)
    end
  end
end
