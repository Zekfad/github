# frozen_string_literal: true

module Files
  # Controls the listing of a directory of files and subdirectories.
  class DirectoryPartialView
    include ActionView::Helpers::TagHelper
    include GitHub::Encoding
    include CodeNavigationHelper

    attr_reader :user, :repository, :directory, :path, :branch_or_tag_name, :commit_sha

    def initialize(user, repository, commit_sha, branch_or_tag_name, path, directory = nil)
      @user = user
      @repository = repository
      @commit_sha = commit_sha
      @branch_or_tag_name = branch_or_tag_name
      @path = path
      @directory = (directory || repository.directory(commit_sha, path))
    end

    def base_branch
      @base_branch ||= repository.base_branch(branch_or_tag_name, user)
    end

    def comparison
      @comparison ||= GitHub::Comparison.deprecated_build(repository, base_branch, commit_sha)
    end

    # Is there a pull request for this branch?
    #
    # Returns a PullRequest or nil.
    def pull_request
      @pull_request ||= begin
        pull = repository.pull_requests.for_branch(branch_or_tag_name).last
        pull && pull.open? ? pull : false
      end
    end

    delegate :has_readme?, to: :directory

    def readme
      directory.preferred_readme
    end

    # Is the readme editable by the current user?
    #
    # Returns a boolean.
    def edit_readme_enabled?
      has_readme? && repository.pushable_by?(user, ref: branch_or_tag_name)
    end

    def readme_name_for_display
      readme.name.force_encoding("utf-8").scrub!
    end

    def subdirectory?
      !@path.blank?
    end

    def render_directory_entries?
      directory.history_cached? || !directory.can_compute_history?
    end

    # Public: Can we load latest commit information. Only possible if we can
    # compute history for the directory.
    def can_load_latest_commit_information?
      directory.can_compute_history?
    end

    def upload_enabled?
      branch_or_tag_name.present?
    end
  end
end
