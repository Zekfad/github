# frozen_string_literal: true

class IntegrationInstallations::AccessGroupingsView < AccessGroupings::ShowView
  attr_reader :integration_installation

  delegate :target, :version, :integration, to: :integration_installation

  # Internal: Memoize the permissions.
  #
  # Returns the organization and repository permission Hash
  def permissions
    return @permissions if defined?(@permissions)
    @permissions = version.permissions_relevant_to(target)
  end

  # Internal: Memoize the permissions.
  #
  # Returns the user permission Hash
  def user_permissions
    @user_permissions ||= integration_installation.version.user_permissions
  end
end
