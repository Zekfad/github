# frozen_string_literal: true

class IntegrationInstallations::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :target, :page

  def installations
    @installations ||= begin
      installs = if target.is_a?(Repository)
        IntegrationInstallation.with_repository(target)
      else
        target.integration_installations
      end

      installs.
        third_party.
        includes(:integration).
        order("integrations.name asc").
        paginate(page: page)
    end
  end

  def update_permissions_path_for(installation)
    if can_update_installation_permissions(installation) == :is_admin
      urls.gh_permissions_update_request_settings_installation_path(installation)
    else
      urls.gh_edit_app_installation_permissions_path(installation.integration, installation)
    end
  end

  def review_request_path_for(installation_request)
    integration = installation_request.integration
    url_params  = { request_id: installation_request }

    if (installation = integration.installations.with_target(target).first)
      urls.settings_org_installation_path(target, installation, url_params)
    else
      url_params[:target_id] = target.id
      urls.gh_app_installation_permissions_path(integration, **url_params)
    end
  end

  def installation_requests
    return IntegrationInstallationRequest.none if target.is_a?(Repository)
    @installation_requests ||=
      target.integration_installation_requests.includes(:integration).order("integrations.name asc")
  end

  def can_configure_installation?(installation)
    installation.viewable_by?(current_user)
  end

  def configure_installation_as(installation)
    IntegrationInstallation::Permissions.check(
      installation: installation,
      actor: current_user,
      action: :view,
    ).reason
  end

  def can_review_pending_requests?
    return false unless target.is_a?(Organization)
    target.adminable_by?(current_user)
  end

  def automatic_installation_reason(installation)
    installation.integration_install_trigger.reason
  end

  def can_update_installation_permissions?(installation)
    update_permisions_for(installation).permitted?
  end

  def can_update_installation_permissions(installation)
    update_permisions_for(installation).reason
  end

  private

  def update_permisions_for(installation)
    IntegrationInstallation::Permissions.check(
      installation: installation,
      actor:        current_user,
      action:       :update_permissions,
      version:      installation.integration.latest_version,
    )
  end
end
