# frozen_string_literal: true

class Actions::EnterpriseRunnersView < Actions::RunnersView
  def show_labels?
    true
  end

  def settings_owner_type
    "enterprise"
  end

  def can_manage_runners?
    true
  end

  def add_runner_path
    urls.settings_actions_add_runner_enterprise_path(settings_owner)
  end

  def delete_runner_path(id:, os:)
    urls.settings_actions_delete_runner_modal_enterprise_path(settings_owner, id: id, os: os)
  end

  def force_remove_runner_path(id:)
    urls.settings_actions_delete_runner_enterprise_path(settings_owner, id: id)
  end

  def runners_path
    "" # Not implemented yet
  end

  def labels_path(runner_id:, selected_labels:)
    urls.settings_actions_runner_labels_enterprise_path(settings_owner, runner_id: runner_id, applied_labels: selected_labels)
  end

  def create_runner_group_path
    urls.settings_actions_create_runner_group_enterprise_path(settings_owner)
  end

  def update_runner_group_path(id:)
    urls.settings_actions_update_runner_group_enterprise_path(settings_owner, id: id)
  end

  def delete_runner_group_path(id:)
    urls.settings_actions_delete_runner_group_enterprise_path(settings_owner, id: id)
  end

  def runner_group_targets_path(id: nil)
    urls.settings_actions_runner_group_targets_enterprise_path(settings_owner, id: id)
  end

  def bulk_runner_groups_path
    urls.settings_actions_runner_group_bulk_actions_enterprise_path(settings_owner)
  end

  def update_runner_group_runners_path
    urls.settings_actions_update_runner_group_runners_enterprise_path(settings_owner)
  end

  def runner_groups_menu_path
    urls.settings_actions_runner_groups_menu_enterprise_path(settings_owner)
  end
end
