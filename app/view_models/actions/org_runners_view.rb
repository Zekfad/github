# frozen_string_literal: true

class Actions::OrgRunnersView < Actions::RunnersView
  def show_labels?
    true
  end

  def can_manage_runners?
    true
  end

  def add_runner_path
    urls.settings_org_actions_add_runner_path(settings_owner)
  end

  def delete_runner_path(id:, os:)
    urls.settings_org_actions_delete_runner_modal_path(settings_owner, id: id, os: os)
  end

  def force_remove_runner_path(id:)
    urls.settings_org_actions_delete_runner_path(settings_owner, id: id)
  end

  def runners_path
    urls.settings_org_actions_list_runners_path(settings_owner)
  end

  def labels_path(runner_id:, selected_labels:)
    urls.settings_org_actions_labels_path(settings_owner, runner_id: runner_id, applied_labels: selected_labels)
  end

  def bulk_actions_path
    urls.org_runner_bulk_actions_path(settings_owner)
  end

  def create_runner_group_path
    urls.settings_org_actions_create_runner_group_path(settings_owner)
  end

  def update_runner_group_path(id:)
    urls.settings_org_actions_update_runner_group_path(settings_owner, id: id)
  end

  def delete_runner_group_path(id:)
    urls.settings_org_actions_delete_runner_group_path(settings_owner, id: id)
  end

  def runner_group_targets_path(id: nil)
    urls.settings_org_actions_runner_group_targets_path(settings_owner, id: id)
  end

  def bulk_runner_groups_path
    urls.settings_org_actions_runner_group_bulk_actions_path(settings_owner)
  end

  def update_runner_group_runners_path
    urls.settings_org_actions_update_runner_group_runners_path(settings_owner)
  end

  def runner_groups_menu_path
    urls.settings_org_actions_runner_groups_menu_path(settings_owner)
  end

  def billing_path
    urls.settings_org_billing_path(settings_owner)
  end
end
