# frozen_string_literal: true

module Codespaces
  class NewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repository, :ref
    attr_initializable :vscs_target

    def form_ready?
      repository && ref
    end

    def codespace
      return unless form_ready?

      repository.codespaces.build(ref: ref&.name)
    end

    def vscs_target
      @vscs_target.to_sym if pickable_vscs_targets.include?(@vscs_target&.to_sym)
    end

    def vscs_target?(target)
      vscs_target == target.to_sym
    end

    def pickable_vscs_targets
      return [] unless repository && Codespaces::Policy.is_codespaces_developer?(current_user)
      return @pickable_vscs_targets if defined?(@pickable_vscs_targets)

      @pickable_vscs_targets = Set[default_vscs_target]
      GitHub::Config::VSCS_ENVIRONMENTS.each do |_, env|
        next if @pickable_vscs_targets.include?(env[:name])

        integration = Apps::Internal.integration(env[:integration_alias])
        next unless integration

        accessible_by_integration = Apps::Internal.repo_accessible_to_limited_app?(repository, app: integration)

        @pickable_vscs_targets << env[:name] if accessible_by_integration
      end

      @pickable_vscs_targets
    end

    def default_vscs_target
      Codespaces.vscs_target(current_user)
    end
  end
end
