# frozen_string_literal: true

module Codespaces
  class RepositorySelectView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    # Maximum number of user repos to query for and ultimate render the view with
    USER_REPO_LIMIT = 100

    attr_reader :selected_repository, :phrase, :remote_ip

    def selected?(repo)
      repo == selected_repository
    end

    def disabled?(repo)
      !Codespaces::Policy.creatable_by_user?(repo, current_user)
    end

    def repositories
      return @repositories if defined?(@repositories)

      if phrase.blank?
        @repositories = user_repositories
      else
        results = query.execute.results
        @repositories = results.map { |result| result["_model"] }
      end

      if selected_repository
        @repositories.delete(selected_repository)
        @repositories.unshift(selected_repository)
      end

      @repositories
    end

    private

    # user_repositories fetches all of the associated repositories with the current user
    # and ranks them based on contributions.
    def user_repositories
      ranked_repos = current_user.ranked_contributed_repositories(
        include_issue_comments: true,
        exclude_owned: false,
        since: 30.days.ago,
        viewer: current_user,
      )

      associated_ids = current_user.associated_repository_ids(including: [:owned, :direct]).take(USER_REPO_LIMIT)
      associated_repos = Repository.where(id: associated_ids).includes(:owner).limit(USER_REPO_LIMIT)

      superset = Set.new(ranked_repos.keys)

      # join ranked repos and associated repos deduping them in the process
      superset = superset | associated_repos.to_a

      # sort repos by their ranked if it exists, otherwise by their name with owner
      superset.sort_by { |repo| user_repo_rank(ranked_repos, repo) }.take(USER_REPO_LIMIT)
    end

    # user_repo_rank returns the ranking for a repo based on contributions, or
    # repo name with owner if no contributions are found
    def user_repo_rank(rankings, repo)
      ranking = if !Codespaces::Policy.creatable_by_user?(repo, current_user)
        # > 1, causes these repos to be at the bottom of the repo list
        1
      elsif rankings[repo]
        # negate so the highest :score causes `sort_by` to put most active repos at the top of the repo list
        -rankings[repo][:score]
      else
        # Repos from associated_repository_ids will have no score, they're in the middle
        0
      end

      [ranking, repo.nwo]
    end

    def query
      @query ||= Search::Queries::RepoQuery.new(
        current_user: current_user,
        remote_ip: remote_ip,
        phrase: phrase,
        include_forks: true
      )
    end
  end
end
