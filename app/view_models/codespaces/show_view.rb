# frozen_string_literal: true

module Codespaces
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :codespace, :passthrough_params, :github_token

    def origin
      @origin ||= if codespace.provisioned?
        urls.uri_origin(URI.parse(iframe_url))
      else
        config = Codespaces.vscs_target_config(codespace.owner)
        workbench_url = config[:workbench_url_pattern] % { id: "*" }
        urls.uri_origin(URI.parse(workbench_url))
      end
    end

    def owner
      codespace.owner.login
    end

    def name
      codespace.name
    end

    def codespace_id
      codespace.id
    end

    def repository_id
      codespace.repository.id
    end

    def repository_name
      codespace.repository.name
    end

    def repository_permalink
      codespace.repository.permalink
    end

    def guid
      codespace.guid
    end

    def iframe_url
      return nil unless codespace.provisioned?

      return @iframe_url if defined?(@iframe_url)

      pattern = codespace.vscs_target_config[:workbench_url_pattern]
      @iframe_url = (pattern % { id: EscapeUtils.escape_uri_component(codespace.guid) }).then do |url|
        passthrough_params.present? ? "#{url}?#{passthrough_params.to_query}" : url
      end
    end
  end
end
