# frozen_string_literal: true

module Diff
  # Controls the display of a diff hunk header
  class HunkHeaderPartialView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

    # An CodeLinesPartialView.
    attr_reader :lines_view_model

    # blob related properties.
    attr_reader :blob_sha, :path, :mode

    # The actual text for the hunk.
    attr_reader :hunk_text

    # Left and right starting line numbers for hunk header
    attr_reader :left_hunk_start, :right_hunk_start

    # Left and right hunk sizes
    attr_reader :left_hunk_size, :right_hunk_size

    # Don't allow expanstion from this hunk if false.
    attr_reader :expandable

    # The anchor for the file or diff discussion.
    attr_reader :anchor

    # The line numbers for the left and right sides of the diff.
    attr_reader :left, :right

    # Previous line numbers for left and right sides of the diff.
    attr_reader :last_left, :last_right

    # The hunk headers position in the original diff output.
    attr_reader :position

    # Determining if the repository is a wiki or not.
    attr_reader :in_wiki_context

    def after_initialize
      if !self.lines_view_model.nil?
        @blob_sha ||= self.lines_view_model.blob_sha
        @path ||= self.lines_view_model.path
        @mode ||= self.lines_view_model.mode
        @anchor ||= self.lines_view_model.anchor
        @expandable ||= self.lines_view_model.expandable?
      end

      # TODO: Should receive a parsed hunk header from upstream and assemble the
      # hunk text in the view rather than stringifying it and reparsing it here.
      if parsed_hunk_text = hunk_text.match(/@@ -(\d+),(\d+) \+(\d+),(\d+) /)
        @left_hunk_start  = parsed_hunk_text[1].to_i
        @left_hunk_size   = parsed_hunk_text[2].to_i
        @right_hunk_start = parsed_hunk_text[3].to_i
        @right_hunk_size  = parsed_hunk_text[4].to_i
      end
    end

    def expandable?
      self.expandable && !self.blob_sha.nil? && self.right > 0
    end
  end
end
