# frozen_string_literal: true

module Diff
  class ReviewCommentCodeLinesPartialView < CodeLinesPartialView
    # An PullRequestReviewThread
    attr_reader :thread

    # The number of lines skipped when these lines are for a review comment.
    attr_reader :excerpt_offset

    # The maximum number of adjacent insertion/deletion lines to include in the excerpt.
    attr_reader :max_context_lines

    attr_initializable :repository, :diff_entry, :path

    def after_initialize
      @repository ||= self.thread.repository

      @max_context_lines ||= 15
      @excerpt_offset ||= self.thread.excerpt_starting_offset(max_context_lines)
      @line_enumerator ||= self.thread.excerpt_line_enumerator(max_context_lines)

      @diff_entry ||= self.thread&.diff_entry
      @blob ||= (diff_entry && diff_entry.valid?) ? diff_blob(diff_entry) : nil

      @path ||= self.thread&.path
      @anchor ||= diff_discussion_anchor(self.thread)

      # The lines passed to #syntax_highlighted_line have positions relative to
      # the start of self.thread.diff_hunk. We need to convert those
      # positions to be relative to the start of self.thread.diff_entry
      # instead, which is used for syntax-highlighting the diff. So here we
      # compute where in the diff_entry the diff_hunk starts.
      @diff_hunk_start_index = if diff_entry
        first_line = diff_enum_peek(line_enumerator)
        if first_line && first_line_index = diff_entry.enumerator.find_index { |line| line.left == first_line.left && line.right == first_line.right }
          first_line_index - excerpt_offset
        end
      end
    end

    def syntax_highlighted_line(line)
      syntax_highlighted_lines[line.position + @diff_hunk_start_index]
    end

    def syntax_highlighting_available?
      @diff_hunk_start_index && super
    end
  end
end
