# frozen_string_literal: true

module Diff
  class FileReviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    extend UrlHelper

    attr_reader :pull_request
    attr_reader :path
    attr_reader :user
    attr_reader :reviewed

    def reviewed?
      if reviewed.nil?
        user_reviewed_file&.reviewed?(path)
      else
        reviewed
      end
    end

    def dismissed?
      user_reviewed_file&.dismissed?(path)
    end

    private def user_reviewed_file
      @user_reviewed_file ||= PullRequestUserReviews.new(pull_request, user)
    end
  end
end
