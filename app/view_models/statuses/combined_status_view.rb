# frozen_string_literal: true

class Statuses::CombinedStatusView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include StatusHelper
  attr_reader :combined_status, :simple_view, :commit, :required_status_decision_basis_commit

  # Optional PullRequest context used on merge form
  attr_reader :pull

  # Statuses sorted by state
  def statuses
    @statuses ||= begin
      combined_status.prefill
      combined_status.status_checks.sort_by(&:sort_order)
    end
  end

  def status_counts_by_context
    statuses.inject(Hash.new(0)) do |res, status|
      res[status.state] += 1
      res
    end
  end

  def all_succeeded?
    statuses.all? { |s| StatusCheckRollup::SUCCESS_STATES.include?(s.state) }
  end

  def all_failing?
    statuses.all? { |s| StatusCheckRollup::FAILURE_STATES.include?(s.state) }
  end

  def list_checks?
    statuses.any? { |status| status.state != "success" }
  end

  def pending?
    StatusCheckRollup::PENDING_STATES.include?(state)
  end

  def state
    combined_status.state
  end

  def status_required?(status)
    return @status_required[status] if defined?(@status_required)

    @status_required = if pull
      Promise.all(statuses.map { |status|
        status.async_required_for_pull_request?(pull).then do |is_required|
          [status, is_required]
        end
      }).sync.to_h
    else
      {}.tap { |hash| hash.default = false }
    end

    @status_required[status]
  end

  def graphql?
    false
  end
end
