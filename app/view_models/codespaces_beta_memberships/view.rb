# frozen_string_literal: true

module CodespacesBetaMemberships
  class View < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :membership, :survey

    def already_enabled?
      current_user.workspaces_enabled?
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def membership_exists?
      @membership_exists ||= EarlyAccessMembership.workspaces_waitlist.exists?(member: current_user)
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
