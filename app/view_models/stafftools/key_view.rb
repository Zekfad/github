# frozen_string_literal: true

module Stafftools
  class KeyView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :key

    def verifier_user_link
      if key.verifier
        helpers.link_to(key.verifier.login, urls.gh_stafftools_user_path(key.verifier))
      elsif key.verifier_id
        helpers.link_to("a deleted user",
          urls.stafftools_audit_log_path(query: "user_id:#{key.verifier_id}"))
      else
        "an unknown user"
      end
    end
  end
end
