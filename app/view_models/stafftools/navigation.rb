# frozen_string_literal: true

module Stafftools
  module Navigation

    def icon_nav(icon, text, warn = false)
      if warn
        span = helpers.octicon("alert", class: "error")
      else
        span = helpers.octicon(icon)
      end

      helpers.safe_join([span, text], " ")
    end

    def counter_title(text, count)
      "#{text} #{helpers.content_tag(:span, count, class: 'Counter')}" # rubocop:disable Rails/ViewModelHTML
    end

  end
end
