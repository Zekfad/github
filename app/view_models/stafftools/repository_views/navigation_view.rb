# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    class NavigationView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::Navigation

      attr_reader :repository

      def octicon
        if repository.fork?
          "repo-forked"
        elsif repository.mirror
          visibility_class = repository.public? ? "mirror" : "lock"
          "#{visibility_class}"
        else
          visibility_class = repository.public? ? "repo" : "lock"
          "#{visibility_class}"
        end
      end

      def show_importer?
        GitHub.porter_available?
      end

      def disk_label
        icon_nav "file-directory", "Disk", disk_warning?
      end

      private

      def disk_warning?
        !repository.availability.git?
      end
    end
  end
end
