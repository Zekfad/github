# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    class NetworkView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::Sentry

      attr_reader :repository

      def page_title
        "#{repository.name_with_owner} - Network"
      end

      def is_root?
        repository.network_root?
      end

      def root
        repository.root
      end

      def rooting_violates_repo_limit?
        repository.private? && root.private? && repository.owner.at_private_repo_limit?
      end

      def has_forks?
        !(repo_map.size == 1 || repo_map[nil].blank?)
      end

      def network
        repository.network
      end

      def has_parent_network?
        repository.network.parent.present?
      end

      def parent_network
        repository.network.parent
      end

      def child_network_maps
        network.children.map do |child|
          repo_map = child.full_network_tree
          [repo_map[nil].first, repo_map]
        end
      end

      def has_child_networks?
        network.children.present?
      end

      def has_related_networks?
        network.family_ids.length > 1
      end

      def attachable_repos
        RepositoryNetwork.family_root_repositories(network).where.not(id: network.root.id)
      end

      def first
        repo_map[nil].first
      end

      def family_network_maps
        @family_network_map ||= repository.network.family_network_trees
      end

      def repo_map
        @repo_map ||= repository.network.full_network_tree
      end

      def allow_extract?
        return unless repository.online?
        repository.enough_space_to_extract?
      end

      def allow_detach?
        return unless repository.online?
        repository.enough_space_to_detach?
      end

      def network_family_attach_enabled?
        GitHub.flipper[:network_family_attach].enabled?(current_user)
      end

      def allow_attach?
        return has_parent_network? && network.matches_parent_visibility? unless network_family_attach_enabled?
        has_related_networks?
      end

      def attach_button_text
        network_family_attach_enabled? ? "Attach" : "Reattach"
      end

      def attach_header_text
        network_family_attach_enabled? ? "Attach to related network" : "Reattach to parent network"
      end

      def attach_description_text
        if network_family_attach_enabled?
          allow_attach? ? "Attach this repository and all forks to a related network." : "This repository has no related networks."
        else
          return "This repository has no parent network." if !has_parent_network?
          return "Reattach this repository and all forks to parent network #{parent_network_link}." if allow_attach?
          "Cannot reattach this repository to parent network #{parent_network_link} because it has a different visibility."
        end
      end

      def root_repository_sentry_link
        sentry_query_link([Stafftools::Sentry::DOTCOM_PROJECT_ID], "job:ChangeNetworkRoot")
      end

      private

      def parent_network_link
        helpers.link_to(parent_network.root.name_with_owner, urls.gh_stafftools_repository_path(parent_network.root))
      end
    end
  end
end
