# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    class UploadsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

      attr_reader :repository

      def page_title
        "#{repository.name_with_owner} - Uploads"
      end

      def s3_files
        @s3_files ||= if GitHub.s3_uploads_enabled?
          Set.new GitHub.s3_primary_client.list_objects(
            bucket: GitHub.s3_environment_config[:bucket_name],
            prefix: "downloads/#{repository.name_with_owner}/",
          ).contents.map { |f| File.basename(f.key) }
        else
          []
        end
      end

      def s3_file?(name)
        s3_files.include?(name)
      end

    end
  end
end
