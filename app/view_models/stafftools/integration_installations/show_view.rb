# frozen_string_literal: true

class Stafftools::IntegrationInstallations::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :installation, :repositories, :user

  delegate :integration, :target, :rate_limit, :using_temporary_rate_limit?, to: :installation

  def installed_at
    installation.created_at
  end

  def page_title
    "#{ target } - #{ integration.name }"
  end

  def selected_link
    :installations
  end

  def ghec_rate_limit
    ghec_rate_limit_config.limit
  end

  def ghec_rate_limit_runway
    ghec_rate_limit_config.runway
  end

  private

  def ghec_rate_limit_config
    @ghec_rate_limit_config ||= begin
      context = Stafftools::RateLimitContext.new(
        nil,
        current_integration_installation: installation,
        request_owner: target,
      )

      Api::RateLimitConfiguration.for(
        Api::RateLimitConfiguration::DEFAULT_FAMILY,
        context,
      )
    end
  end
end
