# frozen_string_literal: true
class Stafftools::RemindersBetaSignup::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :memberships, :slack_installations, :pull_panda_installations

  def signups
    @signups ||= begin
      memberships.select(&:member).map do |membership|
        Signup.new(
          membership,
          slack_installations[membership.member],
          pull_panda_installations[membership.member],
        )
      end
    end
  end

  class Signup
    attr_reader :membership, :slack_installation, :pull_panda_installation
    def initialize(membership, slack_installation, pull_panda_installation)
      @membership = membership
      @slack_installation = slack_installation
      @pull_panda_installation = pull_panda_installation
    end

    def organization
      @organization ||= membership.member
    end

    def submitter
      @submitter ||= membership.actor || User.ghost
    end

    def submitted_at
      membership.created_at.to_date.iso8601
    end
  end
end
