# frozen_string_literal: true

class Stafftools::AzureMeteredBillingExtractsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  def monthly_extracts
    @monthly_extracts ||= monthly_objects.map do |object|
      ExtractFile.new(object: object)
    end
  end

  def quarterly_extracts
    @quarterly_extracts ||= quarterly_objects.map do |object|
      ExtractFile.new(object: object)
    end
  end

  def sales_extracts
    @sales_extracts ||= sales_objects.map do |object|
      ExtractFile.new(object: object)
    end
  end

  private

  def monthly_objects
    @monthly_objects ||= list_objects(prefix: "monthly/")
  end

  def quarterly_objects
    @quarterly_objects ||= list_objects(prefix: "quarterly/")
  end

  def sales_objects
    @sales_objects ||= list_objects(prefix: "sales/")
  end

  def list_objects(prefix:)
    Billing::Azure::ExtractStorage.
      list_objects(prefix: prefix).
      sort_by(&:key).
      reverse
  end

  class ExtractFile
    def initialize(object:)
      @object = object
    end

    def name
      @name ||= File.basename(object.key)
    end

    def link
      @link ||= Billing::Azure::ExtractStorage.generate_expiring_url(object_key: object.key)
    end

    private

    attr_reader :object
  end
end
