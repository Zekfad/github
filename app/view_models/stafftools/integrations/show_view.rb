# frozen_string_literal: true

class Stafftools::Integrations::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :integration
  attr_reader :hook_deliveries_query

  delegate :name, :hook, :created_at, :owner, :id, to: :integration

  def page_title
    "Owned Integrations - #{ name }"
  end

  def selected_link
    :integrations
  end

  def installations_count
    integration.installations.count
  end

  def has_automatic_installs?
    current_user_feature_enabled?(:automatic_app_installs) && integration_install_triggers.any?
  end

  def has_marketplace_listing?
    listing.present?
  end

  def listing
    @listing ||= Marketplace::Listing.non_sponsorable.for_integratables(nil, id).first
  end

  def integration_install_triggers
    @integration_install_triggers ||= IntegrationInstallTrigger.install_types.keys.map do |install_type|
      IntegrationInstallTrigger.latest(integration: integration, install_type: install_type)
    end.compact
  end

  def integration_install_trigger_string
    integration_install_triggers.map(&:install_type).to_sentence
  end

  def yaml_manifest
    YAML.dump(manifest_hash)
  end

  def json_manifest
    GitHub::JSON.encode(manifest_hash)
  end

  def manifest_hash
    {
      "name"                => "Manifest - #{name}"[0..30],
      "url"                 => "http://example.com",
      "hook_attributes"     => {"url" => "http://example.com/hooks"},
      "redirect_url"        => "http://example.com/manifest/creation/callback",
      "description"         => "Manifest App created from [#{name}](#{urls.stafftools_user_app_path(integration.owner, integration)})",
      "public"              => false,
      "default_events"      => integration.default_events,
      "default_permissions" => integration.default_permissions,
    }
  end

  def this_app_managers
    manager_ids = Permissions::Enumerator.actor_ids_with_permission(action: :manage_app, subject_id: integration.id)
    User.with_ids(manager_ids)
  end

  def organization_app_managers
    manager_ids = Permissions::Enumerator.actor_ids_with_permission(action: :manage_all_apps, subject_id: owner.id)
    User.with_ids(manager_ids)
  end

  def ip_whitelist_entries
    return @ip_whitelist_entries if defined?(@ip_whitelist_entries)
    @ip_whitelist_entries = IpWhitelistEntry.usable_for(integration).order(whitelisted_value: :asc)
  end

  def hook_active_status
    hook.active? ? "enabled" : "disabled"
  end
end
