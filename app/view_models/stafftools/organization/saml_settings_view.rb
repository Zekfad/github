# frozen_string_literal: true

module Stafftools
  module Organization
    class SamlSettingsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :organization, :page, :filter

      def linked_members
        @linked ||= organization.linked_saml_members.sort_by(&:login)
      end

      def unlinked_members
        @unlinked ||= organization.unlinked_saml_members.sort_by(&:login)
      end

      def scope_title
        "#{scope.capitalize} SAML members"
      end

      def scope
        filter == "unlinked" ? "unlinked" : "linked"
      end

      def scoped_members
        return @scoped if @scoped

        @scoped = case scope
        when "unlinked"
          unlinked_members
        else
          linked_members
        end

        @scoped = @scoped.paginate(page: page)
      end
    end
  end
end
