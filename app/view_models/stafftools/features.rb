# frozen_string_literal: true

module Stafftools
  module Features
    URL_HELPERS = Rails.application.routes.url_helpers

    def features_links(features, show_percentage_of_time:)
      features.sort_by(&:name)
        .map { |f|
          link = helpers.link_to(f.name, URL_HELPERS.devtools_feature_flag_path(f))

          if show_percentage_of_time && f.percentage_of_time_value > 0
            helpers.safe_join([link, " @ #{f.percentage_of_time_value}%"])
          else
            link
          end
        }
    end
  end
end
