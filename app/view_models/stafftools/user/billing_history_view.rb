# frozen_string_literal: true

module Stafftools
  module User
    class BillingHistoryView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :user
      attr_initializable :transactions

      def page_title
        "#{user.login} - Billing History"
      end

      def charges
        @charges ||= transactions.map do |txn|
          PaymentRecord.new txn, self
        end
      end

      def charges?
        charges.any?
      end

      def disputes
        @disputes ||= user.billing_disputes.order(created_at: :desc).map do |dispute|
          DisputeRecord.new(dispute)
        end
      end

      def disputes?
        disputes.any?
      end

      def refundable_charges?
        charges.any? &:refundable?
      end

      def confirmation_id(index)
        "confirm_send_receipt_#{index}"
      end

      def billing_email
        user ? user.billing_email : transactions.first.billing_email_address
      end

      def refund_path(charge)
        if charge.live_user.present?
          urls.stafftools_user_refund_transaction_path(user: charge.user_login, transaction_id: charge.transaction_id)
        else
          urls.stafftools_billing_transactions_refund_path(transaction_id: charge.transaction_id)
        end
      end

      private

      def transactions
        @transactions ||= user.billing_transactions.where("transaction_type != 'refund'").descending
      end
    end

    class PaymentRecord
      attr_reader :billing_transaction, :view

      delegate :amount,
               :asset_packs_delta,
               :asset_packs_total,
               :platform_url,
               :platform_name,
               :card_number,
               :card_type,
               :charged_back?,
               :created_at,
               :discount_in_cents,
               :incomplete?,
               :live_user,
               :line_items,
               :notes,
               :old_plan_name,
               :paypal_email,
               :paypal?,
               :plan_name,
               :refund,
               :refundable?,
               :seats_delta,
               :seats_total,
               :last_status,
               :success?,
               :transaction_id,
               :transaction_type,
               :user_login,
               :voided?,
               :was_refunded?,
        to: :billing_transaction

      delegate :urls,
               :helpers,
        to: :view

      def initialize(billing_transaction, view)
        @billing_transaction = billing_transaction
        @view = view
      end

      def plan
        GitHub::Plan.find(
          plan_name,
          effective_at: live_user&.plan_effective_at,
        )
      end

      def old_plan
        GitHub::Plan.find(
          old_plan_name,
          effective_at: live_user&.plan_effective_at,
        )
      end

      def plan_info
        case transaction_type
        when "prorate-charge"
          added_seats_text || subscription_item_text || "Upgrade to #{plan_display_name}"
        when "prorate-seat-charge"
          "+ #{helpers.pluralize(seats_delta, "seat")} (#{seats_total})"
        when "prorate-asset-pack-charge"
          "+ #{helpers.pluralize(asset_packs_delta, "data pack")} (#{asset_packs_total})"
        when "prorate-switch-to-seat-charge"
          "Switch to #{plan_display_name} (#{helpers.pluralize(seats_total, "seat")})"
        when "job-posting"
          "Job posting"
        else
          if switched_to_per_seat?
            "Switch to per-seat (#{seats_total})"
          else
            recurring_transaction_text
          end
        end
      end

      def plan_display_name
        (plan ? plan.display_name : plan_name)&.titleize&.strip
      end

      def per_seat?
        plan && plan.per_seat?
      end

      def added_seats_text
        if added_seats?
          "+ #{helpers.pluralize(seats_delta, "seat")} (#{seats_total})"
        end
      end

      def subscription_item_text
        if line_items.marketplace.present?
          item = line_items.marketplace.first
          "#{item.listing.name} #{item.subscribable.name}"
        end
      end

      def short_transaction_id
        transaction_id&.last(8)&.upcase
      end

      # Internal: Returns plan_info for recurring transaction
      #
      # Returns String
      def recurring_transaction_text
        text  = per_seat? ? "#{helpers.pluralize(seats_total, "seat")}" : plan_display_name

        return text unless usage_charges?
        "#{text} usage overages"
      end

      def added_seats?
        per_seat? && seats_delta > 0
      end

      # Internal: Whether transaction contains any usage charges
      #
      # Returns Boolean
      def usage_charges?
        usage_charged_line_items.any?
      end

      # Internal: Returns line items on transaction that include usage charges
      #
      # Returns [Billing::BillingTransaction::LineItem]
      def usage_charged_line_items
        line_items.usage.select { |line| line[:amount_in_cents] > 0 }
      end

      # Internal: Boolean if billing_transaction records a switch to per seat pricing.
      def switched_to_per_seat?
        per_seat? && (transaction_type == "prorate-switch-to-seat-charge" || (old_plan && !old_plan.per_seat?))
      end

      # Deprecated: Old school Transaction for this billing_transaction. Only
      # used to show a Stafftools note now.
      def legacy_transaction
        @legacy_transaction ||= Transaction.find_by_billing_transaction_id(billing_transaction.id)
      end

      def formatted_date(created_at = self.created_at)
        created_at.strftime("%Y-%m-%d")
      end

      def refund_or_void
        if billing_transaction.settled?
          "Refund"
        else
          "Void"
        end
      end

      def refund_at
        refund.created_at
      end

      def refund_amount
        Billing::Money.new(-refund.amount.cents)
      end

      def full_refund?
        refund.amount.abs == amount.abs
      end

      def payer_identifier
        paypal? ? paypal_email : card_number
      end

      def show_receipt_link?
        success? && !charged_back? && !zero_charge?
      end

      def zero_charge?
        amount.zero?
      end

      def active_coupon
        discount_in_cents && discount_in_cents > 0 && "#{Billing::Money.new(discount_in_cents).format} off"
      end

      def status_text
        was_refunded? ? "#{refund_or_void}ed" : last_status&.humanize
      end

      def style_class
        "bg-gray-light" if was_refunded? || !success?
      end
    end

    class DisputeRecord
      attr_reader :billing_dispute

      delegate :amount,
        :billing_transaction,
        :created_at,
        :platform,
        :platform_dispute_id,
        :refundable?,
        :response_due_by,
        to: :billing_dispute

      delegate :transaction_id, to: :billing_transaction

      def initialize(billing_dispute)
        @billing_dispute = billing_dispute
      end

      def dispute_url
        case platform.to_sym
        when :stripe
          "https://dashboard.stripe.com/payments/#{transaction_id}"
        end
      end

      def response_url
        case platform.to_sym
        when :stripe
          "https://dashboard.stripe.com/payments/#{transaction_id}/dispute"
        end
      end

      def transaction_url
        billing_transaction.platform_url
      end

      def platform_name
        platform.to_s.titleize
      end

      def transaction_platform_name
        billing_transaction.platform_name
      end

      def short_dispute_id
        platform_dispute_id.last(8)
      end

      def short_transaction_id
        transaction_id.last(8).upcase
      end

      def reason
        billing_dispute.reason.humanize
      end

      def status
        billing_dispute.status.humanize
      end

      def can_respond?
        return false if response_due_by.nil?

        %w[warning_needs_response needs_response].include?(billing_dispute.status)
      end

      def refund_or_void
        if billing_transaction.settled?
          "Refund"
        else
          "Void"
        end
      end
    end
  end
end
