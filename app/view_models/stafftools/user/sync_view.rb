# frozen_string_literal: true

module Stafftools
  module User
    class SyncView < Stafftools::LdapSyncView
      def sync_path
        sync_stafftools_user_ldap_path(subject)
      end

      def change_ldap_dn_path
        stafftools_user_ldap_path(subject)
      end

      def audit_log_path
        stafftools_audit_log_path(query: "user_id:#{subject.id} action:user.ldap_sync*")
      end

      def subject_label
        subject.login
      end

      def ldap_map_label
        "LDAP Entity"
      end
    end
  end
end
