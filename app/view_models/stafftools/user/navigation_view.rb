# frozen_string_literal: true

module Stafftools
  module User
    class NavigationView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::Navigation

      attr_reader :user

      def page_title
        "#{user.login} - Navigation"
      end

      def admin_alert
        user.spammy? || user.suspended? || org_transform?
      end

      def admin_title
        if user.spammy?
          counter_title("Admin", "Spammy")
        elsif user.suspended?
          counter_title("Admin", "Suspended")
        elsif org_transform?
          counter_title("Admin", "Org transform")
        else
          "Admin"
        end
      end

      def org_transform?
        !user.organization? && Organization.transforming?(user)
      end

      def billing_title
        plan = user.plan.to_s.humanize
        plan += " yearly" if user.plan_duration == User::YEARLY_PLAN
        counter_title("Billing", plan)
      end

      def show_security?
        return false if @user.organization?
        GitHub.stafftools_sessions_enabled? || GitHub.auth.two_factor_authentication_enabled?
      end

      def security_title
        if user.two_factor_authentication_enabled?
          counter_title("Security", "2FA")
        else
          "Security"
        end
      end

      def pub_repo_title
        count = helpers.number_with_delimiter user.repository_counts.public_repositories
        counter_title("Public repositories", count)
      end

      def disabled_repo_title
        count = helpers.number_with_delimiter user.repositories.where("disabled_at IS NOT NULL").size
        counter_title("Disabled repositories", count)
      end

      def priv_repo_title
        count = helpers.number_with_delimiter user.repository_counts.private_repositories
        counter_title("Private repositories", count)
      end

      def gists_title
        count = helpers.number_with_delimiter user.gists.are_public.count
        counter_title("Public gists", count)
      end

      def archived_gists_title
        "Archived public gists"
      end

      def repo_use
        used = user.private_repo_count_for_limit_check
        total = user.plan.repos
        "#{used} of #{total}"
      end

      def emails_alert
        return false unless user.user?
        !user.has_primary_email? ||
          (user.emails.user_entered_emails.verified.count.zero? &&
           GitHub.email_verification_enabled?)
      end

      def emails_title
        if !user.user?
          "Emails"
        elsif !user.has_primary_email?
          counter_title("Emails", "No primary email")
        else
          count = helpers.number_with_delimiter user.emails.user_entered_emails.count
          counter_title("Emails", count)
        end
      end

      def ssh_keys_alert
        user.public_keys.any? && keys_for_verification.empty?
      end

      def keys_for_verification
        keys = user.public_keys
        keys.select do |key|
          if key.verified?
            oauth_authorization = key.oauth_authorization
            oauth_authorization.nil? || (oauth_authorization.application && oauth_authorization.application.github_desktop?)
          else
            key.unverification_reason == "stale"
          end
        end
      end

      def ssh_keys_title
        count = helpers.number_with_delimiter keys_for_verification.count
        counter_title("SSH keys", count)
      end

      def show_user_interactions?
        GitHub.user_abuse_mitigation_enabled? || @user.user?
      end

    end
  end
end
