# frozen_string_literal: true

module Stafftools
  module User
    class BillingView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include ActionView::Helpers::TextHelper
      include Stafftools::StatusChecklist
      include Admin::User::SecurityHelper

      attr_reader :user
      attr_reader :params
      attr_reader :seat_change

      delegate :days_active, :days_remaining, :started_on, :expires_on,
        to: :enterprise_cloud_trial, prefix: :trial

      def user_metered_billing_eligible?
        user.plan_metered_billing_eligible?
      end

      def user_codespaces_eligible?
        user.plan_codespaces_eligible? && user.billing_github_codespaces_enabled?
      end

      def page_title
        "#{user.login} - Billing"
      end

      # Public: Show the current payment status of the account.
      #
      # Example:
      #
      # Charge failing. Will retry on 2000-11-11 and 2000-11-26
      # Charge failing. Will retry on 2000-11-26
      # Charge failing. No attempts remaining
      # Billing status: Unknown
      # Billing status: Good standing
      #
      # Returns an ActiveSupport::SafeBuffer (String)
      def payment_status
        if user.free_plan?
          status_li(:success, "Billing status: Free")
        elsif user.billing_attempts.zero?
          status_li(:success, "Billing status: Good standing")
        elsif user.paid_plan? && user.billing_attempts > 0 && user.billed_on
          tries = [0, ::User::BILLING_ATTEMPTS_LIMIT - user.billing_attempts].max
          dates = [15, 7]
            .take(tries)
            .map { |day| (user.billed_on + day.days).strftime("%F") }
            .reverse
            .to_sentence
          text   = "Charge failing. Will retry on #{dates}" unless dates.blank?
          text ||= "Charge failing. No attempts remaining"
          status_li(:error, text)
        else
          status_li(:neutral, "Billing status: Unknown")
        end
      end

      # Public: Returns the current trade restriction status on the user
      #
      # Returns an ActiveSupport::SafeBuffer (String)
      def trade_restriction_status
        return unless user.has_any_trade_restrictions?

        if scheduled_ofac_downgrade
          status_li(:error, "Trade restriction scheduled for #{scheduled_ofac_downgrade.downgrade_on}")
        elsif user.trade_restriction_finalized?
          status_li(:error, "Trade restriction finalized on #{user.trade_restriction_finalized_date}")
        else
          status_li(:error, "Trade restricted")
        end
      end

      def user_bill_cycle_day_text
        return "None" if !user.zuora_account? || user.customer_bill_cycle_day.nil?

        if user.customer_bill_cycle_day.zero?
          "Auto Set (Will be set on next invoice)"
        else
          "#{user.customer_bill_cycle_day.ordinalize} of the month"
        end
      end

      def metered_quota_reset_date
        user.first_day_in_next_metered_cycle
      end

      def calculated_metered_quota_reset_date
        user.advanced_metered_cycle_reset_date || (::GitHub::Billing.today + 1.month).beginning_of_month
      end

      def external_subscription_link
        if user.zuora_subscription?
          helpers.link_to(user.plan_subscription.zuora_subscription_number, zuora_subscription_url)
        else
          "None"
        end
      end

      def external_customer_account_link
        if user.zuora_account?
          helpers.link_to("View on Zuora", zuora_account_url)
        else
          "None"
        end
      end

      def lock_status_text
        if !user.disabled?
          "Unlocked"
        elsif !user.plan_supports?(:repos, visibility: :private)
          "Plan doesn't support private repos"
        elsif user.over_plan_limit?
          "Over the allotment of private repos"
        elsif user.has_billing_record?
          "Card declined"
        else
          "Coupon expired or staff locked"
        end
      end

      def lock_status(text)
        locked = case lock_status_text
        when "Card declined", "Coupon expired or staff locked"
          :error
        when "Unlocked"
          :success
        else
          :neutral
        end
        status_li locked, text
      end

      def gift_type
        if user.billing_type == "teacher"
          "Teacher/student group"
        elsif user.gift?
          "Gift"
        else
          "Normal account"
        end
      end

      def billing_audit_log_path
        query = "#{audit_log_query} AND " \
                "action:(braintree.* OR billing.* OR account.*)"
        audit_log_path query
      end

      def braintree_customer_url
        "#{GitHub.braintree_host}/merchants/%s/customers/%s" %
        [GitHub.braintree_merchant_id, user.braintree_customer_id.to_s]
      end

      def zuora_account_url
        "#{GitHub.zuora_host}/apps/CustomerAccount.do?method=view&id=%s" %
        [@user.customer.zuora_account_id]
      end

      def zuora_subscription_url
        "#{GitHub.zuora_host}/apps/Subscription.do?method=view&id=%s" %
        [@user.plan_subscription&.zuora_subscription_id]
      end

      def apple_iap_subscription?
        user.apple_iap_subscription?
      end

      def private_repos?
        user.owned_private_repositories.count > 0
      end

      def can_manual_charge?
        # Cannot bill someone with no card on file
        return false unless user.has_billing_record?

        # We can't charge someone who doesn't owe anything
        return false if user.payment_amount.zero?

        # If they've never paid us billed_on will be nil
        return true if user.billed_on.nil?

        # If the billed_on is in the past, they're fair game
        return true if user.billed_on <= Date.today

        # If they're having card issues, we may need to push a charge through
        return true if user.billing_attempts > 0

        # Otherwise, nooooope
        false
      end

      def manual_charge_explanation
        if can_manual_charge?
          "Force the payment method on file to be charged."
        elsif user.payment_amount.zero?
          "No payment due, active coupon covers the current plan."
        elsif user.has_valid_payment_method?
          "The #{user.friendly_payment_method_name} on file cannot be manually charged until #{user.billed_on}."
        else
          "There is no payment method on file to charge."
        end
      end

      def available_plans
        available_plans = if user.organization?
          GitHub::Plan.all_org_plans
        else
          [GitHub::Plan.free, GitHub::Plan.pro]
        end

        available_plans.map do |plan|
          if plan.per_seat?
            if plan.business_plus?
              ["#{plan.display_name(user.type).titleize} - Unlimited private repositories, SAML SSO, guaranteed uptime, and email support", plan.name]
            else
              ["#{plan.display_name(user.type).titleize} - Unlimited private repositories", plan.name]
            end
          else
            if plan.free_with_addons?
              ["#{plan.display_name(user.type).titleize} with addons", plan.name]
            else
              repo_count = user.organization? ? plan.org_repos : plan.repos
              ["#{plan.display_name(user.type).titleize} - #{repo_count} private repositories", plan.name]
            end
          end
        end
      end

      def plan_durations
        [
          ["Monthly", "month"],
          ["Yearly", "year"],
        ]
      end

      def is_gifted?
        user.gift?
      end

      def gift_types
        if user.billing_type == "teacher"
          [
            ["Normal account", "card"],
            ["Teacher or student group", "teacher"],
          ]
        else
          [
            ["Normal account", "card"],
            ["Gift account", "gift"],
          ]
        end
      end

      def plan_changes?
        plan_changes.any?
      end

      def plan_changes
        user.transactions.reverse
      end

      # Public: we warn staff who attempt to force a plan downgrade about potential custom roles which will be deleted.
      # This only applies when going from Enterprise plan (bussiness_plus) to any other target.
      def show_custom_role_downgrade_warning?
        user.organization? && user.plan.business_plus? && GitHub.flipper[:custom_roles].enabled?(user) && user.custom_roles.count > 0
      end

      def humanized_action_for_change(change)
        return "Changed" if change.action == "downgraded" || change.action == "upgraded"

        change.action.try(:camelize)
      end

      def billing_extra_summary
        if user.billing_extra.blank?
          "Not set"
        else
          helpers.truncate user.billing_extra.gsub(/\n/, " ")
        end
      end

      def coupon_redemptions?
        coupon_redemptions.any?
      end

      def coupon_redemptions
        @coupon_redemptions ||= begin
          r = user.coupon_redemptions + user.expired_coupons
          r.sort { |a, b| b.expires_at <=> a.expires_at }.map do |cr|
            [cr, cr.coupon]
          end
        end
      end

      def passed_code
        params[:coupon]
      end

      def coupon_codes
        ::Coupon.multi_use.active.sort_by(&:code)
      end

      # Renewal

      def renewal_amount
        Billing::Money.new(user.payment_amount * 100).format
      end

      def renewal_date
        user.new_billed_on(user.billed_on || Date.today)
      end

      def renewal_plan
        if user.plan.per_seat?
          helpers.pluralize(user.seats, "seat")
        else
          "#{user.plan.name.capitalize} plan"
        end
      end

      def show_perseat?
        user.organization?
      end

      def formatted_account_members_count
        helpers.pluralize(user.direct_members.size, "member")
      end

      def formatted_account_pending_invitations_count
        helpers.pluralize(user.pending_non_manager_invitations.size, "pending organization invitation")
      end

      def formatted_account_outside_collaborators_count
        "%s with private access" % [
          helpers.pluralize(
            user.collaborators_on_private_repositories_without_invitations.size,
            "outside collaborator",
          ),
        ]
      end

      def formatted_account_private_repository_invitations_counts
        total_invite_count  = user.private_repo_invitee_ids.size
        if total_invite_count > 0
          unique_invite_count = user.private_repo_non_collaborator_invitee_ids.size
          "%s unique / %s total private repository invitations" % [
            unique_invite_count,
            total_invite_count,
          ]
        else
          "0 private repository invitations"
        end
      end

      def current_term_end_date
        user.billed_on - 1.day if user.billed_on
      end

      def in_business
        user.business.present?
      end

      def plan_supports_unlimited_private_repos?
        limit = user.plan_limit(:repos, visibility: :private, fallback_to_free: user.disabled?)
        limit >= 9999
      end

      def show_prepaid_metered_usage_refills?
        Billing::PrepaidMeteredUsageRefill.enabled_for?(user)
      end

      def pending_cycle_abbr_title(change)
        if change.listable_is_sponsorable?
          "Sponsorship"
        else
          "Marketplace"
        end
      end

      def pending_cycle_abbr_prefix(change)
        change.listable_is_sponsorable? ? "SP" : "MP"
      end

      def scheduled_ofac_downgrade
        user.scheduled_ofac_downgrade
      end

      def in_enterprise_trial?
        enterprise_cloud_trial.active?
      end

      def can_extend_trial?
        enterprise_cloud_trial.can_extend_trial?
      end

      def trial_days_text
        if enterprise_cloud_trial.expired?
          "The trial expired #{pluralize(trial_days_remaining.abs, "days")} ago."
        else
          "The trial has been active for #{pluralize(trial_days_active, "days")} and has #{pluralize(trial_days_remaining, "days")} remaining."
        end
      end

      def trade_controls_restriction_type
        user.trade_controls_restriction.type
      end

      def package_registry_usage
        @_package_registry_usage ||= Billing::PackageRegistryUsage.new(user)
      end

      def shared_storage_usage
        @_shared_storage_usage ||= Billing::SharedStorageUsage.new(user)
      end

      def actions_usage
        @_actions_usage ||= Billing::ActionsUsage.new(user)
      end

      def codespaces_storage_usage
        @_codespaces_storage_usage ||= Billing::Codespaces::StorageUsage.new(user)
      end

      def codespaces_compute_usage
        @_codespaces_compute_usage ||= Billing::Codespaces::ComputeUsage.new(user)
      end

      def metered_billing_permissions
        @_metered_billing_permissions ||= Billing::MeteredBillingPermission.new(user)
      end

      def codespaces_permissions
        @_codespaces_permissions ||= Billing::CodespacesPermission.new(user)
      end

      private

      def enterprise_cloud_trial
        @enterprise_cloud_trial ||= Billing::EnterpriseCloudTrial.new(@user)
      end
    end
  end
end
