# frozen_string_literal: true

module Stafftools
  module User
    class KeysView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :user

      def page_title
        "#{user.login} - SSH Keys"
      end

      def keys?
        keys.any?
      end

      def keys
        user.public_keys
      end

      def old_fingerprint_cmd
        "ssh-keygen -lf path/to/key.pub"
      end

      def new_fingerprint_cmd
        "ssh-keygen -E md5 -lf path/to/key.pub"
      end
    end
  end
end
