# frozen_string_literal: true

module Stafftools
  module Sponsors
    module Members
      class HealthCheckView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
        include ActionView::Helpers::DateHelper

        attr_reader :check

        # Public: The slug of the criterion for this health check.
        #
        # Returns a String.
        def slug
          check.sponsors_criterion.slug
        end

        # Public: Indicates if the health check criterion is met.
        #         Certain checks are special cased here if it is important
        #         that we display in real time if a criterion is still met or not.
        #
        # Returns a Boolean.
        def met?
          case slug
          when SponsorsCriterion::Automated::SuspensionStatus.slug
            SponsorsCriterion::Automated::SuspensionStatus.new(
              sponsors_membership: check.sponsors_membership,
            ).async_met?.sync
          when SponsorsCriterion::Automated::SpammyStatus.slug
            SponsorsCriterion::Automated::SpammyStatus.new(
              sponsors_membership: check.sponsors_membership,
            ).async_met?.sync
          when SponsorsCriterion::Automated::OFACCompliance.slug
            SponsorsCriterion::Automated::OFACCompliance.new(
              sponsors_membership: check.sponsors_membership,
            ).async_met?.sync
          else
            check.met?
          end
        end

        # Public: The result text to display for this criterion.
        #
        # Returns a String.
        def text
          met? ? met_text : unmet_text
        end

        # Public: The text describing the criterion being evaluated.
        #
        # Returns a String.
        def description
          check.sponsors_criterion.description
        end

        # Public: The CSS class for the text for this health check.
        #
        # Returns a String.
        def text_class
          met? ? met_text_class : unmet_text_class
        end

        private

        # Private: The text to display if the criterion is met.
        #
        # Returns a String.
        def met_text
          case slug
          when SponsorsCriterion::Automated::AccountAge.slug
            account_age_text(check.value)
          when SponsorsCriterion::Automated::SuspensionStatus.slug
            "Not suspended"
          when SponsorsCriterion::Automated::SpammyStatus.slug
            "Not spammy"
          when SponsorsCriterion::Automated::BlockedThreshold.slug
            helpers.pluralize(check.value, "user")
          when SponsorsCriterion::Automated::AbuseReports.slug
            helpers.pluralize(check.value, "abuse report")
          when SponsorsCriterion::Automated::OFACCompliance.slug
            "Not flagged"
          when SponsorsCriterion::Automated::PublicContributions.slug
            helpers.pluralize(check.value, "public contribution")
          when SponsorsCriterion::Automated::FollowerThreshold.slug, SponsorsCriterion::Automated::FollowerThreshold::V1_SLUG
            helpers.pluralize(check.value, "follower")
          end
        end

        # Private: The text to display if the criterion is not met.
        #
        # Returns a String.
        def unmet_text
          case slug
          when SponsorsCriterion::Automated::AccountAge.slug
            account_age_text(check.value)
          when SponsorsCriterion::Automated::SuspensionStatus.slug
            "Suspended"
          when SponsorsCriterion::Automated::SpammyStatus.slug
            "Spammy"
          when SponsorsCriterion::Automated::BlockedThreshold.slug
            helpers.pluralize(check.value, "user")
          when SponsorsCriterion::Automated::AbuseReports.slug
            helpers.link_to(
              helpers.pluralize(check.value, "report"),
              urls.abuse_reports_stafftools_user_path(check.sponsors_membership.sponsorable.login),
              class: "#{unmet_text_class} text-underline",
            )
          when SponsorsCriterion::Automated::OFACCompliance.slug
            "Flagged"
          when SponsorsCriterion::Automated::PublicContributions.slug
            helpers.pluralize(check.value, "public contribution")
          when SponsorsCriterion::Automated::FollowerThreshold.slug, SponsorsCriterion::Automated::FollowerThreshold::V1_SLUG
            helpers.pluralize(check.value, "follower")
          end
        end

        # Private: The CSS class for the met text. Some criteria may want to have
        #          different text styling for their met state.
        #
        # Returns a String|nil.
        def met_text_class
          case slug
          when SponsorsCriterion::Automated::OFACCompliance.slug
            "text-green"
          end
        end

        # Private: The CSS class for the unmet text. Some criteria may want to have
        #          different text styling for their unmet state.
        #
        # Returns a String.
        def unmet_text_class
          case slug
          when SponsorsCriterion::Automated::OFACCompliance.slug
            "text-red"
          else
            "text-orange"
          end
        end

        # Private: The text to display for the `account_age` check.
        #
        # Returns a String.
        def account_age_text(value)
          created_at = DateTime.parse(value)
          "#{created_at.strftime("%b %-d, %Y")} (#{time_ago_in_words(created_at)} ago)"
        end
      end
    end
  end
end
