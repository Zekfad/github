# frozen_string_literal: true

class Stafftools::Abilities::AbilityView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :ability
  attr_reader :nested
  attr_reader :only

  def action
    ability.action
  end

  def actor_exists?
    !ability.nil?
  end

  def actor_description
    return "#{ability.actor_type} #{ability.actor_id}" if !actor_exists?
    ability.actor.ability_description
  end

  def focused_participant
    return ability.actor if show_actor?
    return ability.subject
  end

  def focused_type
    focused_participant ? focused_participant.ability_type : "incomplete"
  end

  def octicon
    return if show_actor? && show_subject?
    participant = show_actor? ? ability.actor : ability.subject

    case participant
    when nil then "alert"
    when ::Organization then "organization"
    when ::User then "person"
    when ::Repository then "repo"
    when ::Team then "jersey"
    else "gift"
    end
  end

  def direct?
    ability.direct?
  end

  def indirect?
    ability.indirect?
  end

  def kind
    direct? ? "direct" : "indirect"
  end

  def kind_class
    direct? ? "ability-direct" : "ability-indirect"
  end

  def parent
    ability.parent
  end

  def grandparent
    ability.grandparent
  end

  def show_actor?
    only != :subject
  end

  def show_subject?
    only != :actor
  end

  def subject_exists?
    !ability.subject.nil?
  end

  def subject_description
    return "#{ability.subject_type} #{ability.subject_id}" if !subject_exists?
    ability.subject.ability_description
  end

  def timestamp
    ability.updated_at
  end
end
