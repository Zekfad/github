# frozen_string_literal: true

module Stafftools
  # Adds helpers for querying GitHub.audit.
  module SecurityHelper

    # Keys in the security log that are valid stafftools search queries.
    LINKABLE_KEYS = %w[actor actor_id org org_id repo repo_id user user_id]

    # Slice just the parameter keys that you care about, and convert
    # StrongParms into a normal Hash.
    #
    # params - params from a Rails controller request.
    # *keys  - One or more Symbol keys from params.
    #
    # Returns a HashWithIndifferentAccess
    def slice_controller_params(params, *keys)
      keys << :a << :t
      params.slice(*keys).to_hash.with_indifferent_access
    end

    # Adds Audit options to every query.
    #
    # options - A Hash.
    #
    # Returns a Hash.
    def query_options(options)
      options
    end

    def latest_log
      logs.last
    end

    def next_page_params
      params.merge(t: (latest_log.created_at.to_f * 1000).round)
    end

    def add_action_option(options)
      add_string_option options, :a, :action
    end

    def add_created_option(options)
      if time = params[:t]
        time = time.to_i if time =~ /^\d+$/
        options[:created_at] = [nil, time]
      end
    end

    # Private
    def add_string_option(options, param_key, audit_key)
      if (value = params[param_key]).present?
        options[audit_key] = value
      end
    end

    # Queries the Audit log with the given options.
    #
    # options - A Hash.
    #
    # Returns an Array of Hash Audit objects.
    def query_audit(options = {})
      search_with_query(query_options(options))
    end

    # Identifies security log keys that are linkable to stafftools queries.
    #
    # key - An audit log key.
    #
    # Returns true if the key should be linked as a search query and false
    # otherwise.
    def linkable_key?(key)
      LINKABLE_KEYS.include? key
    end

    def search_with_query(query)
      return [] if query.blank?
      logs = GitHub.audit.search(query)
      AuditLogEntry.new_from_array(logs)
    end

    def is_hash?(value)
      value.is_a? Hash
    end
  end
end
