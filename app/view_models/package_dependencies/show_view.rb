# frozen_string_literal: true

module PackageDependencies
  class ShowView < PackageDependencies::View
    include PlatformHelper

    attr_reader :selected_tab, :package_manager, :package_name, :package_version, :repository, :dependent_search

    def build_path(query: raw_query, tab: selected_tab)
      urls.package_details_path(
        query: query,
        tab: tab,
        name: url_encode(package_name),
        ecosystem: package_manager,
        version: package_version,
        dependent_name: dependent_search,
      )
    end

    def tab_selected?(tab)
      selected_tab == tab
    end

    def organization_filter_link?
      false
    end

    AdvisoryIdentifierFragment = parse_query <<-'GRAPHQL'
      fragment on SecurityAdvisory {
        ghsaId
        identifiers {
          type
          value
        }
        origin
      }
    GRAPHQL

    def advisory_identifier(advisory)
      advisory = AdvisoryIdentifierFragment.new(advisory)
      identifiers = advisory.identifiers
      if identifiers
        first_cve = identifiers.detect { |identifier| identifier.type == "CVE" }
        return first_cve.value if first_cve
        first_white_source = identifiers.detect { |identifier| identifier.type == "WHITE_SOURCE" }
        return first_white_source.value if first_white_source
      end

      advisory.ghsa_id
    end

    def has_whitesource_origin?(advisory)
      advisory = AdvisoryIdentifierFragment.new(advisory)
      advisory.origin == ::Vulnerability::WHITESOURCE_ORIGIN
    end

    AdvisoryDescriptionFragment = parse_query <<-'GRAPHQL'
      fragment on SecurityAdvisory {
        id
        description
      }
    GRAPHQL

    def render_advisory_description(advisory)
      advisory = AdvisoryDescriptionFragment.new(advisory)
      GitHub::Goomba::MarkdownPipeline.to_html(advisory.description)
    rescue EncodingError, TypeError => error
      Failbot.report(error, advisory_id: advisory.id, description: advisory.description)
      nil
    end
  end
end
