# frozen_string_literal: true

class Integrations::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :integrations, :pending_transfers, :owner

  # Public: Can the owner of these integrations create new Apps?
  #
  # Returns a Boolean.
  def can_create_apps?
    return @can_create_apps if defined?(@can_create_apps)

    if owner.user?
      @can_create_apps = true
    else
      @can_create_apps = Permissions::Enforcer.authorize(
        actor: current_user,
        action: :manage_all_apps,
        subject: owner,
      ).allow?
    end
  end
end
