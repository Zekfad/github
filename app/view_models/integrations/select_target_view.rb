# frozen_string_literal: true

class Integrations::SelectTargetView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :integration, :page

  delegate :name, :url, :description, :owner, :installed_on?, to: :integration, prefix: true

  PER_PAGE  = 10

  def installable?
    integration.installable_by?(current_user)
  end

  def integration_installed?
    accounts.any? { |account| integration.installed_on?(account) }
  end

  def installation_for_account(account)
    integration.installations.with_target(account).first
  end

  def integration_github_owned?
    integration.github_owned?
  end

  def open_requests
    return [] unless logged_in?
    @open_requests ||= IntegrationInstallationRequest.includes(:target).where(
      integration: integration,
      requester: current_user,
    ).index_by(&:target)
  end

  def request_on_target(target)
    open_requests[target]
  end

  # Public: Returns the full set of accounts that the user may have
  # installation rights against. For public integrations this includes any
  # account which they have admin rights for. For private integrations this
  # only includes the account which owns the integration.
  #
  # Returns an ActiveRecord Relation of User/Organizations.
  def accounts
    accounts = \
      if !logged_in?
        Organization.none
      elsif integration.public?
        all_accounts
      elsif !actionable?
        Organization.none
      elsif integration.owner_type == "Business"
        Business.where(id: integration.owner.id)
      else
        User.where(id: integration.owner.id)
      end

    if page.present?
      accounts.paginate(page: page, per_page: PER_PAGE)
    else
      accounts
    end
  end

  def selection_needed?
    accounts.many?
  end

  # Public: which URL should configuration start at?
  #
  # Returns a path
  def configure_url
    if selection_needed?
      urls.gh_new_app_installation_path(integration)
    else
      configure_url_for(accounts.first)
    end
  end

  def configure_url_for(account)
    if (installation = installation_for_account(account))
      if installation.adminable_by?(current_user)
        urls.gh_settings_installation_path(installation)
      else
        urls.gh_edit_app_installation_path(integration, installation)
      end
    end
  end

  private

  def actionable?
    integration.installable_on_by?(target: integration.owner, actor: current_user) || \
    integration.requestable_on_by?(target: integration.owner, actor: current_user)
  end

  def all_accounts
    return [] unless logged_in?

    organization_ids = []

    # Don't bother calculating the access to repositories if
    # the integration doesn't have repository permissions.
    if integration.latest_version.any_permissions_of_type?(Repository)
      repository_ids = current_user.associated_repository_ids(including: [:direct])

      # We can find repositories owned by organizations by seeing both
      # the organization_id is set and it matches the owner_id
      organization_ids = Repository.where("organization_id = owner_id AND id IN (?)", repository_ids).distinct.pluck(:organization_id)
    end

    organization_ids |= User::OrganizationFilter.new(current_user).unscoped_ids
    all_ids = organization_ids.append(current_user.id)

    User.where(id: all_ids).order(type: "DESC", login: "ASC")
  end
end
