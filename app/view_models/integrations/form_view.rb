# frozen_string_literal: true

class Integrations::FormView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :integration

  SAME_AS_CALLBACK_URL_HINT = "Users will be redirected to the 'User authorization callback URL' to complete additional setup."
  SETUP_URL_HINT = "Users will be redirected to this URL after installing your GitHub App to complete additional setup."
  UNAVAILABLE_WHEN_REQUESTING_ON_INSTALL = "Unavailable when requesting OAuth during installation."

  delegate :owner, :default_permissions, :default_events, to: :integration

  def hook
    @hook ||= if (hook = integration.hook)
      hook
    elsif integration.new_record?
      integration.build_hook(active: true)
    else
      integration.build_hook
    end
  end

  def page_title
    if integration.persisted?
      "Edit integration - #{ integration.name_was }"
    else
      "Register new GitHub App"
    end
  end

  def selected_link
    :integrations
  end

  def hook_url_error
    errors = integration.hook.errors[:base]
    return unless errors.any? { |e| e.include? "URL" }

    if /must contain URL/.match(errors.first)
      "Webhook URL can't be blank"
    elsif /(host \S+ is not supported.*\z)/.match(errors.first)
      "Webhook URL #{$1}"
    else
      "Webhook URL is invalid: #{errors.join(", ")}"
    end
  end

  def human_event_name(item)
    item.humanize
  end

  def show_permission_fields?
    integration.new_record?
  end

  def show_privacy_fields?
    integration.new_record?
  end

  def show_event_fields?
    integration.new_record?
  end

  def submit_text
    if integration.new_record?
      "Create GitHub App"
    else
      "Save changes"
    end
  end

  # Public: the value that should be displayed to the user for the App's
  # `setup_url`. This varies depending on whether the App has selected to
  # request OAuth on installation, in which case we always show the
  # `callback_url`.
  #
  # Returns a String.
  def displayable_setup_url
    if integration.can_request_oauth_on_install?
      integration.callback_url
    else
      integration.setup_url
    end
  end

  def setup_url_hint
    if integration.can_request_oauth_on_install?
      SAME_AS_CALLBACK_URL_HINT
    else
      SETUP_URL_HINT
    end
  end

  def redirect_on_update_hint
    if integration.can_request_oauth_on_install?
      redirect_on_update_request_oauth_on_install_hint
    else
      redirect_on_update_request_setup_url_hint
    end
  end

  def redirect_on_update_request_oauth_on_install_hint
    "Redirect users to the 'User authorization callback URL' after installations are updated (E.g repositories added/removed)."
  end

  def redirect_on_update_request_setup_url_hint
    "Redirect users to the 'Setup URL' after installations are updated (E.g. repositories added/removed)."
  end
end
