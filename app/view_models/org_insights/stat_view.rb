# frozen_string_literal: true

class OrgInsights::StatView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :org,
    :time_period,
    :selected_repos,
    :metrics

  def graph_data_url
    urls.org_insight_path(org: org, period: time_period, metrics: metrics, repos: selected_repos.map(&:name))
  end

  def metric_name(metric)
    GitHub::OrgInsights::InsightsData::METRIC_DISPLAY_NAMES[metric]
  end

  def link_to_metric_details(metric, html_options = {})
    details = GitHub::OrgInsights::InsightsData::METRIC_DETAILS_PARAMS[metric]
    if details
      search_query = build_search_query(details)
      path_type = details[:path_type]
      if path_type == "pulls"
        helpers.link_to("Details", link_to_pulls(search_query), html_options)
      elsif path_type == "issues"
        helpers.link_to("Details", link_to_issues(search_query), html_options)
      end
    end
  end

  private

  def link_to_pulls(query)
    urls.all_pulls_path(q: query)
  end

  def link_to_issues(query)
    urls.all_issues_path(q: query)
  end

  def build_search_query(details)
    query = details[:query].dup

    # Add date
    period = GitHub::OrgInsights::InsightsData::TIME_PERIODS[time_period] || GitHub::OrgInsights::InsightsData::TIME_PERIODS["week"]
    duration = period[:duration]
    query << "#{details[:date_filter]}:>=#{duration.ago.to_date}"

    # We cannot use a user(org) filter and repo filter
    # on either of the search pages at the same time
    # whenever user is present, it takes precedence over repo.
    # This is ok since we are using the nwo of the repo, which is the org name
    if selected_repos.any?
      # Add repos
      selected_repos.each do |repo|
        query << "repo:#{repo.nwo}"
      end
    else
      # Add Org
      query << "user:#{org.login}"
    end

    query.join(" ")
  end
end
