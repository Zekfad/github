# frozen_string_literal: true

module PullRequests
  class RangeMenuView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :pull_comparison

    def pull
      @pull ||= pull_comparison.pull
    end

    def most_recent_review
      return unless current_user
      @most_recent_review ||= pull.latest_non_pending_review_for(current_user)
    end

    def reviewed?
      most_recent_review.present?
    end

    def revisions
      return @revisions if defined?(@revisions)

      if pull.repository&.pull_request_revisions_enabled?
        @revisions = pull.revisions.for_range_menu
      else
        @revisions = PullRequestRevision.none
      end
    end

    def previous_revision
      return unless revisions.many?
      revisions[1]
    end

    def viewer_reviewed_revision?(revision)
      return false unless current_user
      @reviewed_revision_oids ||= pull.reviewed_commit_oids(
        user: current_user,
        candidate_oids: revisions.map(&:head_oid),
      )
      @reviewed_revision_oids.include?(revision.head_oid)
    end
  end
end
