# frozen_string_literal: true

module PullRequests
  class TipView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include TipsHelper
    include PlatformHelper

    attr_reader :pull_request_node

    TipViewFragment = parse_query <<-'GRAPHQL'
      fragment TipView on PullRequest {
        resourcePath
      }
    GRAPHQL

    def initialize(pull_request_node:)
      @pull_request_node = TipViewFragment::TipView.new(pull_request_node)
    end

    def tips
      tips = [
        "Add comments to specific lines under <a href='#{pull_request_files_changed_path}'>Files changed</a>.", # rubocop:disable Rails/ViewModelHTML
        "Add <a href='#{pull_request_patch_path}' data-skip-pjax>.patch</a> or <a href='#{pull_request_diff_path}' data-skip-pjax>.diff</a> to the end of URLs for Git’s plaintext views.", # rubocop:disable Rails/ViewModelHTML
      ]
    end

    def pull_request_url
      pull_request_node.resource_path
    end

    def pull_request_files_changed_path
      "#{pull_request_url}/files"
    end

    def pull_request_patch_path
      "#{pull_request_url}.patch"
    end

    def pull_request_diff_path
      "#{pull_request_url}.diff"
    end

    def selected_tip
      render_link(tips.sample)
    end
  end
end
