# frozen_string_literal: true

module BillingSettings
  class PaymentsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::UrlHelper
    include PlatformHelper

    attr_reader :target

    UserFragment = parse_query <<-'GRAPHQL'
      fragment on Billable {
        paymentMethod
        billingTransactions(sales: true, descending: true, first: 100) {
          nodes {
            amountInCents
            refundAmountInCents
            cardType
            createdAt
            isChargedBack
            isPaypal
            isRefunded
            isSuccess
            isVoided
            lastFour
            paypalEmail
            status
            transactionId
            account {
              ... on Organization {
                login
              }
              ... on User {
                login
              }
            }
          }
        }
      }
    GRAPHQL

    def user
      @user ||= UserFragment.new(target)
    end

    def billing_transactions
      user.billing_transactions.nodes
    end

    # Whether this user has made credit card payments.
    #
    # Returns a Boolean.
    def has_payment_records?
      user.payment_method && payment_records.present?
    end

    # List of billing_transactions as PaymentRecords.
    #
    # Returns an Array of PaymentRecord, RefundedPaymentRecord, and FailedPaymentRecord objects.
    def payment_records
      @payment_records ||= billing_transactions.map do |t|
        klass = if t.is_voided? || t.is_refunded? || t.is_charged_back?
          RefundedPaymentRecord
        elsif t.is_success?
          PaymentRecord
        else
          FailedPaymentRecord
        end
        klass.new(t, self)
      end
    end
  end

  class PaymentRecord
    attr_reader :billing_transaction, :view

    delegate :amount_in_cents,
             :asset_packs_delta,
             :asset_packs_total,
             :platform_url,
             :last_four,
             :card_type,
             :created_at,
             :old_plan_name,
             :paypal_email,
             :is_paypal?,
             :plan_name,
             :refund_amount_in_cents,
             :seats_total,
             :seats_delta,
             :transaction_id,
             :transaction_type,
             :yearly?,
      to: :billing_transaction

    delegate :urls,
             :helpers,
      to: :view

    def initialize(billing_transaction, view)
      @billing_transaction = billing_transaction
      @view = view
    end

    def plan
      GitHub::Plan.find(plan_name, effective_at: user.plan_effective_at)
    end

    def old_plan
      GitHub::Plan.find(old_plan_name, effective_at: user.plan_effective_at)
    end

    def human_plan_name
      name = (plan ? plan.display_name : "").humanize
      name = "#{name} yearly" if yearly?
      name.strip
    end

    def per_seat?
      plan && plan.per_seat?
    end

    def added_seats_text
      if added_seats?
        "+ #{view.helpers.pluralize(seats_delta, "seat")} (#{seats_total})"
      end
    end

    def added_seats?
      per_seat? && seats_delta > 0
    end

    # Internal: String descriptive text about switch to per seat pricing or nil.
    def switch_to_per_seat_text
      if switched_to_per_seat?
        "Switch to #{human_plan_name} (#{seats_total})"
      end
    end

    # Internal: Boolean if billing_transaction records a switch to per seat pricing.
    def switched_to_per_seat?
      per_seat? && (transaction_type == "prorate-switch-to-seat-charge" || (old_plan && !old_plan.per_seat?))
    end

    def amount
      Billing::Money.new(amount_in_cents).format
    end

    def refund_amount
      Billing::Money.new(0).format
    end

    def formatted_date
      created_at.in_billing_timezone.strftime("%Y-%m-%d")
    end

    def status_icon
      "check"
    end

    def status
      "succeeded"
    end

    def short_transaction_id
      transaction_id&.last(8)&.upcase
    end

    def payer_identifier
      is_paypal? ? paypal_email : "#{card_type} ending in #{last_four}"
    end

    def url_for_receipt
      if billing_transaction.account.is_a?(PlatformTypes::Organization)
        urls.org_receipt_path(billing_transaction.account.login, transaction_id)
      else
        urls.receipt_path(transaction_id)
      end
    end
  end

  class RefundedPaymentRecord < PaymentRecord
    def status_icon
      "reply"
    end

    def status
      "refunded"
    end

    def refund_amount
      Billing::Money.new(refund_amount_in_cents).format
    end
  end

  class FailedPaymentRecord < PaymentRecord
    def url_for_receipt
      nil
    end

    def status_icon
      "x"
    end

    def status
      "failed"
    end
  end
end
