# frozen_string_literal: true

module BillingSettings
  # Controls the rendering of our payment method forms.
  class PaymentMethodFieldsView
    include GitHub::Application.routes.url_helpers

    # Public: Get the User or Organization that will own this credit card.
    #
    # Returns a User or Organization.
    attr_reader :owner

    # Public: Get the form builder used by credit card specific fields of this view.
    #
    # Returns a FormBuilder.
    attr_reader :form

    # Initialize a PaymentMethodFieldsView object used to control the rendering of
    # credit card forms. Used by the payment_method/fields partials used in
    # our CC and PayPal forms site-wide.
    #
    # owner    - The User or Organization that owns the CC we're updating.
    # form     - A Rails' FormBuilder object that contains these fields.
    # new_repo - A Boolean indicating whether this form is being rendered on
    #            the new repository page. Defaults to `false`.
    def initialize(owner, form)
      raise ArgumentError.new("Owner required") unless owner.present?
      raise ArgumentError.new("Form required") unless form.present?

      @owner = owner
      @form = form
    end

    def has_credit_card?
      owner.has_credit_card?
    end

    def has_paypal_account?
      owner.has_paypal_account?
    end

    def owner_login
      owner.login
    end

    def priority_countries
      priorities = ["United States of America", "China", "Germany", "United Kingdom", "India"]
      priorities.map { |country| countries.find { |i| i[0] == country } }
    end

    def countries
      ::TradeControls::Countries.currently_unsanctioned
    end

    def us_states
      [
        ["Alabama", "AL"],
        ["Alaska", "AK"],
        ["Arizona", "AZ"],
        ["Arkansas", "AR"],
        ["Armed Forces Americas", "AA"],
        ["Armed Forces Europe", "AE"],
        ["Armed Forces Pacific", "AP"],
        ["California", "CA"],
        ["Colorado", "CO"],
        ["Connecticut", "CT"],
        ["Delaware", "DE"],
        ["District of Columbia", "DC"],
        ["Florida", "FL"],
        ["Georgia", "GA"],
        ["Hawaii", "HI"],
        ["Idaho", "ID"],
        ["Illinois", "IL"],
        ["Indiana", "IN"],
        ["Iowa", "IA"],
        ["Kansas", "KS"],
        ["Kentucky", "KY"],
        ["Louisiana", "LA"],
        ["Maine", "ME"],
        ["Maryland", "MD"],
        ["Massachusetts", "MA"],
        ["Michigan", "MI"],
        ["Minnesota", "MN"],
        ["Mississippi", "MS"],
        ["Missouri", "MO"],
        ["Montana", "MT"],
        ["Nebraska", "NE"],
        ["Nevada", "NV"],
        ["New Hampshire", "NH"],
        ["New Jersey", "NJ"],
        ["New Mexico", "NM"],
        ["New York", "NY"],
        ["North Carolina", "NC"],
        ["North Dakota", "ND"],
        ["Ohio", "OH"],
        ["Oklahoma", "OK"],
        ["Oregon", "OR"],
        ["Pennsylvania", "PA"],
        ["Puerto Rico", "PR"],
        ["Rhode Island", "RI"],
        ["South Carolina", "SC"],
        ["South Dakota", "SD"],
        ["Tennessee", "TN"],
        ["Texas", "TX"],
        ["Utah", "UT"],
        ["Vermont", "VT"],
        ["Virginia", "VA"],
        ["Washington", "WA"],
        ["West Virginia", "WV"],
        ["Wisconsin", "WI"],
        ["Wyoming", "WY"],
      ]
    end

    def canada_provinces
      [
        "Alberta",
        "British Columbia",
        "Manitoba",
        "New Brunswick",
        "Newfoundland and Labrador",
        "Nova Scotia",
        "Northwest Territories",
        "Nunavut",
        "Ontario",
        "Prince Edward Island",
        "Quebec",
        "Saskatchewan",
        "Yukon",
      ]
    end

    def encrypted_text_field(method, options = {})
      options[:name] = ""

      unless options.delete(:has_card)
        options.merge! \
          "data-encrypted-name" => "billing[credit_card][#{method}]",
          :required => "required"
      end

      form.text_field(method, options)
    end

    def card_type
      if owner.payment_method.present? && owner.payment_method.credit_card?
        owner.payment_method.card_type
      end
    end

    def truncated_credit_card_number
      if owner.payment_method.present? && owner.payment_method.credit_card?
        owner.payment_method.formatted_number
      end
    end

    def expiration_month
      if owner.payment_method.present? && owner.payment_method.credit_card?
        "%02d" % owner.payment_method.expiration_month
      end
    end

    def expiration_year
      if owner.payment_method.present? && owner.payment_method.credit_card?
        owner.payment_method.expiration_year
      end
    end

    def selected_country(country)
      if owner.payment_method.present? && country == owner.payment_method.country
        "selected=selected"
      end
    end

    def selected_state(state)
      if owner.payment_method.present? && state == owner.payment_method.region
        "selected=selected"
      end
    end

    def postal_code
      return unless owner.payment_method.present?
      owner.payment_method.postal_code
    end

    def region
      owner.payment_method&.region
    end

    def expiration_month_options
      months = ("01".."12").to_a
      months.unshift(["MM", "", {disabled: true, selected: true}])
    end

    def expiration_year_options
      (Time.now.year..10.years.from_now.year).inject([]) do |array, year|
        array << [year.to_s[2..4], year]
      end.unshift(["YY", "", {disabled: true, selected: true}])
    end
  end
end
