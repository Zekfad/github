# frozen_string_literal: true

# Used for upgrading to Pro & Business
module BillingSettings
  class ConfirmationView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include GitHub::Application.routes.url_helpers
    include UrlHelper
    include BillingSettingsHelper
    extend Forwardable

    def_delegators :plan_change, \
      :changing_duration?,
      :data_pack_change,
      :duration_change_only?,
      :new_subscription,
      :price_difference,
      :renewal_list_price,
      :total_seats_price,
      :total_subscription_items_price

    def_delegators :new_subscription,
      :duration,
      :plan

    def_delegators :plan,
      :base_units

    attr_reader :account, :plan_change, :return_to

    def taken_seat_count
      @_taken_seat_count ||= Organization::LicenseAttributer.new(account).unique_count
    end

    def show_seats?
      account.upgrading_from_trial?
    end

    def show_name_address_form?
      GitHub.flipper[:name_address_collection].enabled?(account) && account.user?
    end

    def show_payment_form?
      if GitHub.flipper[:name_address_collection].enabled?(account) && account.user?
        needs_valid_payment_method? && account.has_personal_profile?
      else
        needs_valid_payment_method?
      end
    end

    def hide_upgrade_button?
      if GitHub.flipper[:name_address_collection].enabled?(account) && account.user?
        needs_valid_payment_method? || !account.has_personal_profile?
      else
        needs_valid_payment_method?
      end
    end

    def hide_business_owned_section?
      account.upgrading_from_trial? && account.company&.name.present?
    end

    def yearly?
      duration.to_s == User::YEARLY_PLAN
    end

    def action_text(show_team_as_link: false)
      return "Change your payment duration" if duration_change_only?

      if account.organization?
        show_team_as_link ? "Buy #{plan_name}" : "Upgrade to #{plan_name}"
      else
        "Upgrade from #{old_plan_name} to #{plan_name}"
      end
    end

    def call_to_action_text
      return "Change plan duration" if duration_change_only?

      if account.organization?
        "Upgrade my organization's account"
      else
        "Upgrade my account"
      end
    end

    def duration_options
      if duration_change_only?
        [duration.to_sym]
      else
        User::PLAN_DURATIONS.reverse.map(&:to_sym)
      end
    end

    def billing_action_path
      if GitHub.flipper[:name_address_collection].enabled?(account) && account.user?
        return user_profile_update_path unless account.has_personal_profile?
      end

      return cc_update_path if account.user?
      return org_switch_to_seats_path(account) if duration_change_only?
      org_switch_to_seats_path(account, new_plan: plan)
    end

    def form_method
      account.user? ? :post : :put
    end

    # Public - List price for the new plan.
    # Does not include subscription items
    #
    # Returns Money
    def plan_price(plan: self.plan)
      if yearly?
        Billing::Money.new(plan.yearly_cost_in_cents)
      else
        Billing::Money.new(plan.cost_in_cents)
      end
    end

    # Public - List unit price for the new plan.
    # Does not include subscription items
    #
    # Returns Money
    def plan_unit_price(plan: self.plan)
      if yearly?
        Billing::Money.new(plan.yearly_unit_cost_in_cents)
      else
        Billing::Money.new(plan.unit_cost_in_cents)
      end
    end

    def pro_plan_price
      plan_price(plan: GitHub::Plan.pro)
    end

    def business_plan_price
      plan_price(plan: GitHub::Plan.business)
    end

    def business_plan_unit_price
      plan_unit_price(plan: GitHub::Plan.business)
    end

    def business_plus_plan_unit_price
      plan_unit_price(plan: GitHub::Plan.business_plus)
    end

    # Public - For displaying price for a duration.
    # Includes subscription items
    #
    # month_or_year - String, 'month' or 'year'
    #
    # Returns Money
    def plan_price_for_duration(month_or_year)
      if month_or_year.to_sym == :year
        plan_change.new_subscription.undiscounted_yearly_price
      else
        plan_change.new_subscription.undiscounted_monthly_price
      end
    end

    def current_plan_cost_per_duration
      return account.plan.yearly_cost if account.yearly_plan?
      account.plan.cost
    end

    def current_plan_unit_cost_per_duration
      return account.plan.yearly_unit_cost if account.yearly_plan?
      account.plan.unit_cost
    end

    def coupon?
      account.has_an_active_coupon? && account.coupon.applicable_to?(plan)
    end

    def coupon_text
      text = "You have an active coupon for #{account.coupon.human_discount} off "
      if account.coupon.duration == 30
        text += "for one month."
      elsif account.coupon.duration < 30
        text += "for #{account.coupon.human_duration}."
      else
        text += "each month for #{account.coupon.human_duration.downcase}."
      end
    end

    # Public - The final discounted price of the plan change being confirmed,
    #          including discount and credit.
    #
    # Returns Money
    def final_price(github_only: false)
      plan_change.final_price(github_only: github_only, use_balance: !account.past_due?)
    end

    def future_balance
      [final_price, Billing::Money.new(0)].min
    end

    # Public - Whether the credit from the old subscription will cover the full cost
    #          of the new subsription (only when there's an amount to be paid before credit).
    #
    #          #payment_amount_without_credit_applied is the price with discount.
    #          #final_price is the price with discount AND credit. Negative when there is a
    #          credit balance after transaction, zero when covered by coupon or creidt.
    #
    # Returns Boolean
    def payment_amount_fully_covered_by_credit?
      payment_amount_without_credit_applied.positive? && (final_price.negative? || final_price.zero?)
    end

    # Public - Full credit user would get from this transaction
    #
    # Returns Money
    def credit_for_this_transaction
      plan_change.old_subscription.price_of_remaining_service
    end

    def current_credit
      Billing::Money.new([account.credit, 0].max * 100)
    end

    def next_billed_on
      plan_change.starting_new_subscription? ? account.new_billed_on(GitHub::Billing.today, duration) : account.next_billing_date
    end

    def price_prorated?
      plan_change && account.plan.paid? && plan.paid? && !account.past_due? && effective_immediately?
    end

    def needs_valid_payment_method?
      account.needs_valid_payment_method_to_switch_to_plan?(plan)
    end

    # Public - Will the change be applied immediately? (not a pending plan change)
    #
    # Returns Boolean
    def effective_immediately?
      return false if changing_duration?
      price_difference > 0
    end

    # Public - The Price user would pay for the next billing cycle after this change
    # Does not include subscription items
    #
    # Returns Money
    def next_plan_price
      price = if coupon? && !account.will_be_expired?(next_billed_on)
        plan_change.renewal_price(github_only: true)
      else
        renewal_list_price - total_subscription_items_price
      end
      [price + future_balance, Billing::Money.new(0)].max
    end

    # Public - Is the user upgrading to business_plus or not
    # Only returns true if the user was not previously on business_plus
    #
    # Returns Boolean
    def changing_to_business_plus?
      !duration_change_only? && new_subscription.plan.business_plus?
    end

    # Public - number of data packs purchased for this account
    def data_pack_count
      data_pack_change.total_packs
    end

    # Public - the price for each data pack per unit
    #
    # Returns a string
    def human_data_pack_unit_price
      data_pack_change.human_data_pack_unit_price
    end

    # Public - Paid subscription items tied to the account
    #
    # Returns Array of Subscription Items
    def paid_subscription_items
      @subscription_items ||= begin
        sub_items = account.subscription_items
        return [] unless sub_items.any?
        sub_items.active.select(&:paid?)
                              end
    end

    # Public - Is the user upgrading to a plan that has a minumum seat count
    # and their current plan is less than the minumum
    # e.g., Upgrading a free plan with 3 users to a team plan with 5 users
    # (minumum)
    #
    # Returns Boolean
    def using_minimum_seat_count?
      return true if plan.base_units > 1 && plan.base_units == plan_change.seats
    end

    # Public - Billing settings path for the account
    #
    # returns String
    def billing_settings_path
      if account.user?
        settings_user_billing_path
      else
        settings_org_billing_path(account)
      end
    end

    # Public - Is return_to set to create new repository path?
    # Returns true if return_to is a new repository path for an org or user
    #
    # returns Boolean
    def return_to_new_repository?
      return_to == new_org_repository_path(account) ||
      return_to == new_repository_path
    end

    private

    # Private: Payment amount for this plan with discount, without credit
    #
    # Returns Money
    def payment_amount_without_credit_applied
      Billing::Money.new(account.payment_amount(plan: plan))
    end

    # Private: The name of the old plan (Free, Pro, etc)
    #
    # Returns String
    def old_plan_name
      branded_plan_name(plan_change.old_subscription.plan)
    end

    # Private: The name of the plan (Free, Pro, etc)
    #
    # Returns String
    def plan_name
      branded_plan_name(plan)
    end
  end
end
