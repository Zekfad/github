# frozen_string_literal: true

module Hooks
  class BaseView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

    # determines if the current user is an admin on the context webhook context
    def admin_user?(context)
      context.async_action_or_role_level_for(current_user).sync == :admin
    end
  end
end
