# frozen_string_literal: true

class EditRepositories::AdminScreen::PagesVisibilitySelectItemView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :repository, :render_button, :current_selected

  def name
    return "Public" if render_button == :public
    return "Private" if render_button == :private
  end

  def description
    return "Anyone on the internet can see this page." if render_button == :public
    return "Only people with access to this repository can see this page." if render_button == :private
  end
end
