# frozen_string_literal: true

class EditRepositories::AdminScreen::PagesSourceView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include ActionView::Helpers::TagHelper
  include EscapeHelper

  attr_reader :repository
  attr_reader :params

  delegate :is_user_pages_repo?, :has_gh_pages_branch?, :has_master_branch?, :has_gh_pages?, :pages_branch, :has_generated_page?, to: :repository

  # === Source edit ===

  def pages_any_branch_enabled?
    return GitHub.flipper[:pages_any_branch].enabled?(repository.owner)
  end

  def pages_branch_names
    return @branches if defined?(@branches)
    @branches = repository.heads.refs_with_default_first.map do |branch|
      {
        heading: scrubbed_utf8(branch.name),
        value: scrubbed_utf8(branch.name),
        selected: false
      }
    end
    if source && !@branches.find { |b| b[:value] == scrubbed_utf8(source) }
      @branches += [
        {
          heading: scrubbed_utf8(source),
          value: scrubbed_utf8(source),
          selected: false
        }
      ]
    end
    @branches += [
      {
        heading: "None",
        value: nil,
        selected: false
      }
    ]
    selected = @branches.find { |b| b[:value] == scrubbed_utf8(source) }

    # Set current selected branch.
    # If current source did not find in branch list, set last item (pages did not exist) to selected.

    selected ? selected[:selected] = true : @branches.last[:selected] = true
    @branches
  end

  def select_list
    return @select_list if defined?(@select_list)
    @select_list = [
      {
        heading: "master branch",
        description: safe_join(["Use the ", content_tag(:code, "master"), " branch for GitHub Pages."]), # rubocop:disable Rails/ViewModelHTML
        value: "master",
        selected: source == "master" || (is_user_pages_repo? && has_master_branch?),
        disabled: !has_master_branch?,
      },
      {
        heading: "master branch /docs folder",
        description: safe_join(["Use only the ", content_tag(:code, "/docs"), " folder for GitHub Pages."]), # rubocop:disable Rails/ViewModelHTML
        value: "master /docs",
        selected: source == "master /docs",
        disabled: !has_master_branch? || is_user_pages_repo? || !has_master_docs_folder?,
      },
      {
        heading: "None",
        description: "Disable GitHub Pages.",
        value: "none",
        selected: !has_gh_pages?,
        disabled: (is_user_pages_repo? && has_master_branch?) ||
                  (!is_user_pages_repo? && has_gh_pages_branch?),
      },
    ]

    if has_gh_pages_branch? && !is_user_pages_repo?
      @select_list.unshift({
        heading: "gh-pages branch",
        description: safe_join(["Use the ", content_tag(:code, "gh-pages"), " branch for GitHub Pages."]), # rubocop:disable Rails/ViewModelHTML
        value: "gh-pages",
        selected: source.nil? && has_gh_pages_branch? && !is_user_pages_repo?
      })
    end

    @select_list
  end

  def source_dir
    return page && page.source_dir
  end

  def branch_list
    pages_any_branch_enabled? ? pages_branch_names : select_list
  end

  def button_text
    return "None" if (!pages_any_branch_enabled? && !has_gh_pages? || pages_any_branch_enabled? && !page)
    return source if pages_any_branch_enabled?
    selected = select_list.find { |s| s[:selected] }
    selected[:heading]
  end

  def selected_branch_text
    selected = branch_list.find { |s| s[:selected] }
    return selected[:heading] if selected
  end

  def unpublish_documentation_url
    if is_user_pages_repo?
      "#{GitHub.help_url}/github/working-with-github-pages/unpublishing-a-github-pages-site#unpublishing-a-user-or-organization-site"
    elsif !is_user_pages_repo? && has_gh_pages_branch?
      "#{GitHub.help_url}/github/working-with-github-pages/unpublishing-a-github-pages-site#unpublishing-a-project-site"
    end
  end

  def subdir_source?
    page && page.subdir_source?
  end

  def dir_button_text
    # Either return the selected folder's heading
    selected = select_dir.find { |s| s[:selected] }
    return selected[:heading] if selected && selected[:heading]

    # Or default to the first one (/) - this should never be called because in select_dir, when
    # there is no page, we fallback on the root option (when making a selection)
    return select_dir[0][:heading]
  end

  def should_hide_component?
    !page
  end

  def select_dir
    return [
      {
        heading: "/ (root)",
        value: "/",
        selected: source_dir == "/" || !page
      },
      {
        heading: "/docs",
        value: "/docs",
        selected: source_dir == "/docs"
      }
    ]
  end

  # === Theme chooser ===

  # Test for a .nojekyll page
  # If this is true, the Jekyll theme chooser becomes unavailable.
  def nojekyll?
    page.present? && page.nojekyll?
  end

  # Only show the theme chooser if:
  #
  # 1. The feature is enabled
  # 2. The user has not disabled Jekyll
  # 3. The repo is public OR they've already activated Pages
  #
  # See https://github.com/github/github/issues/80433
  def show_theme_chooser?
    return false unless GitHub.pages_gem_themes_enabled?
    return false if nojekyll?
    repository.public? || source || is_user_pages_repo?
  end

  # All choosable themes
  def choosable_themes
    GitHub::Pages::JekyllTheme.all.select do |theme|
      !theme.beta ||
      (current_user && GitHub.flipper[:pages_beta_themes].enabled?(current_user))
    end
  end

  # Determine current choosable theme
  # Reads gem theme from _config.yml and finds corresponding JekyllTheme
  # If no gem theme, falls back to matching APG theme
  #
  # Returns a JekyllTheme, or nil.
  def choosable_theme
    return @choosable_theme if defined?(@choosable_theme)

    if page && page.theme.present?
      @choosable_theme = GitHub::Pages::JekyllTheme.find_gem(page.theme)
    elsif page && repository.has_generated_page?
      @choosable_theme = GitHub::Pages::JekyllTheme.apg_match(page.index_html || "")
    else
      @choosable_theme = nil
    end
  end

  # The current theme name
  # Gets the display name of choosable themes, gem name for non-choosable themes.
  #
  # Returns String or nil
  def theme_name
    return choosable_theme.name if choosable_theme.present?
    page && page.theme
  end

  # Current or first available source from branch_list to auto-enable Pages
  # Defaults to master (when pages_any_branch_enabled is disabled), default branch when pages_any_branch_enabled is enabled
  def default_source
    if !pages_any_branch_enabled?
      return select_list.find { |s| s[:selected] && s[:value] != "none" } ||
      select_list.find { |s| !s[:disabled] && !s[:hidden] && s[:value] != "none" } ||
      select_list.find { |s| s[:value] == "master" }
    else
      select_source = pages_branch_names.find { |s| s[:selected] && s[:value] != nil }
      return select_source if select_source
      return { heading: repository.default_branch, value: repository.default_branch } if is_user_pages_repo?
      return { heading: "gh-pages", value: "gh-pages" }
    end
  end

  # Determine if the APG will be migrated
  def migrate_apg?
    page.present? && page.theme.nil? && repository.has_generated_page?
  end

  # Determine if the APG has been tainted via manual commit
  def tainted_apg?
    migrate_apg? && repository.heads.find(pages_branch).commit.message !~ /branch.*via GitHub\Z/
  end

  def plan_supports_pages?
    repository.plan_supports?(:pages)
  end

  def source
    return page && page.source_branch if pages_any_branch_enabled?
    return page && page.source
  end

  def safe_pages_branch
    scrubbed_utf8(pages_branch)
  end

  private

  def scrubbed_utf8(text)
    return if text.nil?
    return text if text.encoding == ::Encoding::UTF_8 && text.valid_encoding?
    text.dup.force_encoding("UTF-8").scrub!
  end

  def has_master_docs_folder?
    repository.includes_directory?("docs", "master")
  end

  def page
    repository.page
  end
end
