# frozen_string_literal: true

class EditRepositories::AdminScreen::PagesSourceFgpView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include PlatformHelper
  include ActionView::Helpers::TagHelper

  attr_reader :repository, :params, :data

  RepositoryFragment = parse_query <<-'GRAPHQL'
    fragment on Repository {
      name
      planSupportsPages: planSupports(feature: PAGES)
      isUserPagesRepo
      hasMasterBranch: ref(qualifiedName: "refs/heads/master")
      hasGhPagesBranch: ref(qualifiedName: "refs/heads/gh-pages")
      hasPage
      pageSource
      pageSourceDirectory
      isPrivate
      owner{
        login
        isSpammy
        pagesAnyBranchEnabled: isFeatureEnabled(name: "pages_any_branch")
      }
      includesDocsDirectory: ref(qualifiedName: "refs/heads/master"){
        directory(path: "docs"){
          id
        }
      }
      cnameError
      ghPagesUrl
      ghPagesError
      ghPagesErrorMessage
      hasHeads
      branchesIncludingDefaultFirst
    }
  GRAPHQL

  def initialize(**args)
    super(args)

    @repository = RepositoryFragment.new(args[:repository])
  end

  def pages_any_branch_enabled?
    return @repository.owner.pages_any_branch_enabled?
  end

  def pages_branch_names
    return @branches if defined?(@branches)
    @branches = @repository.branches_including_default_first.map do |branch|
      {
        heading: scrubbed_utf8(branch),
        value: scrubbed_utf8(branch),
        selected: false
      }
    end
    if source && !@branches.find { |b| b[:value] == scrubbed_utf8(source) }
      @branches += [
        {
          heading: scrubbed_utf8(source),
          value: scrubbed_utf8(source),
          selected: false
        }
      ]
    end
    @branches += [
      {
        heading: "None",
        value: nil,
        selected: false
      }
    ]
    selected = @branches.find { |b| b[:value] == scrubbed_utf8(source) }

    # Set current selected branch.
    # If current source did not find in branch list, set last item (pages did not exist) to selected.

    selected ? selected[:selected] = true : @branches.last[:selected] = true
    @branches
  end

  def select_list
    return @select_list if defined?(@select_list)
    @select_list = [
      {
        heading: "master branch",
        description: safe_join(["Use the ", content_tag(:code, "master"), " branch for GitHub Pages."]), # rubocop:disable Rails/ViewModelHTML
        value: "master",
        selected: source == "master" || (is_user_pages_repo? && has_master_branch?),
        disabled: !has_master_branch?,
      },
      {
        heading: "master branch /docs folder",
        description: safe_join(["Use only the ", content_tag(:code, "/docs"), " folder for GitHub Pages."]), # rubocop:disable Rails/ViewModelHTML
        value: "master /docs",
        selected: source == "master /docs",
        disabled: !has_master_branch? || is_user_pages_repo? || !has_master_docs_folder?,
      },
      {
        heading: "None",
        description: "Disable GitHub Pages.",
        value: "none",
        selected: !has_gh_pages?,
        disabled: (is_user_pages_repo? && has_master_branch?) ||
                  (!is_user_pages_repo? && has_gh_pages_branch?),
      },
    ]

    if has_gh_pages_branch? && !is_user_pages_repo?
      @select_list.unshift({
        heading: "gh-pages branch",
        description: safe_join(["Use the ", content_tag(:code, "gh-pages"), " branch for GitHub Pages."]), # rubocop:disable Rails/ViewModelHTML
        value: "gh-pages",
        selected: (source.nil? || source == "gh-pages") && has_gh_pages_branch? && !is_user_pages_repo?
      })
    end

    @select_list
  end

  def source_dir
    return @repository.has_page && @repository.page_source_directory
  end

  def branch_list
    pages_any_branch_enabled? ? pages_branch_names : select_list
  end

  def button_text
    return "None" if (!pages_any_branch_enabled? && !has_gh_pages? || pages_any_branch_enabled? && !@repository.has_page)
    return source if pages_any_branch_enabled?
    selected = select_list.find { |s| s[:selected] }
    selected[:heading]
  end

  def selected_branch_text
    selected = branch_list.find { |s| s[:selected] }
    return selected[:heading] if selected
  end

  def unpublish_documentation_url
    if is_user_pages_repo?
      "#{GitHub.help_url}/github/working-with-github-pages/unpublishing-a-github-pages-site#unpublishing-a-user-or-organization-site"
    elsif !is_user_pages_repo? && has_gh_pages_branch?
      "#{GitHub.help_url}/github/working-with-github-pages/unpublishing-a-github-pages-site#unpublishing-a-project-site"
    end
  end

  def dir_button_text
    # Either return the selected folder's heading
    selected = select_dir.find { |s| s[:selected] }
    return selected[:heading] if selected && selected[:heading]

    # Or default to the first one (/) - this should never be called because in select_dir, when
    # there is no page, we fallback on the root option (when making a selection)
    return select_dir[0][:heading]
  end

  def should_hide_component?
    !@repository.has_page
  end

  def select_dir
    return [
      {
        heading: "/ (root)",
        value: "/",
        selected: source_dir == "/" || !@repository.has_page
      },
      {
        heading: "/docs",
        value: "/docs",
        selected: source_dir == "/docs"
      }
    ]
  end

  def name
    @repository.name
  end

  def plan_supports_pages?
    @repository.plan_supports_pages
  end

  def is_user_pages_repo?
    @repository.is_user_pages_repo
  end

  def has_master_branch?
    !!@repository.has_master_branch
  end

  def has_gh_pages_branch?
    !!@repository.has_gh_pages_branch
  end

  def gh_pages_url
    @repository.gh_pages_url
  end

  def has_gh_pages?
    return false if !GitHub.enterprise? && name_matches_old_user_pages? && owner_owns_new_user_pages_repo?
    case source
    when "master"
      @repository.has_master_branch?
    when "gh-pages"
      @repository.has_gh_pages_branch?
    else
      !!@repository.page_source
    end
  end

  def private_repository?
    @repository.is_private?
  end

  def repository_has_heads?
    @repository.has_heads
  end

  def spammy_owner?
    @repository.owner.is_spammy?
  end

  def build_error?
    @repository.gh_pages_error?
  end

  def build_error_message
    message_html(@repository.gh_pages_error_message) if build_error?
  end

  def cname_error?
    !!@repository.cname_error
  end

  def cname_error_message
    message_html(@repository.cname_error) if cname_error?
  end

  def source
    @repository.page_source
  end

  private

  def name_matches_old_user_pages?
    return false unless @repository.owner.present?
    @repository.name == "#{@repository.owner.login.downcase}.#{GitHub.pages_host_name_v1}"
  end

  def owner_owns_new_user_pages_repo?
    @repository.owner_owns_new_user_pages_repo?
  end

  def has_master_docs_folder?
    !!@repository.includes_docs_directory?
  end

  # Copied over from EditRepositories::AdminScreen::PagesStatusView
  #
  # convert error message to html
  # stripping block elements, since this message appears in dotcom
  # following guidelines at
  # https://github.com/github/security-docs/blob/master/standards/Secure%20Coding%20Principles.md#server-side-html-generation
  #
  # msg - markdown error string
  #
  # Returns an html_safe string if possible, else msg
  def message_html(msg)
    inline_whitelist = GitHub::HTML::SanitizationFilter::WHITELIST.dup
    inline_whitelist[:elements] -= %w[p div]
    sanitizer = GitHub::Goomba::Sanitizer.from_whitelist(inline_whitelist)
    sanitizer.require_any_attributes(:a, "href", "id", "name")
    sanitizer.name_prefix = GitHub::Goomba::NAME_PREFIX

    context = {whitelist: inline_whitelist, sanitizer: sanitizer}
    GitHub::Goomba::MarkdownPipeline.to_html(msg, context)
  end

  def scrubbed_utf8(text)
    return if text.nil?
    return text if text.encoding == ::Encoding::UTF_8 && text.valid_encoding?
    text.dup.force_encoding("UTF-8").scrub!
  end
end
