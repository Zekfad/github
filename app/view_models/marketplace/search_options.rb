# frozen_string_literal: true

class Marketplace::SearchOptions
  attr_reader :category_slug, :query, :tool_type, :verification_state

  LISTING_TYPES = %w(apps actions)
  VERIFICATION_STATES = %w(verified unverified)

  def initialize(category_slug: nil, query: nil, tool_type: nil, verification_state: nil)
    @category_slug = category_slug
    @query = query
    @tool_type = tool_type
    @verification_state = verification_state
  end

  def self.find_normalized_type(raw_type)
    LISTING_TYPES.find { |type| type == raw_type.to_s.downcase }
  end

  def self.find_normalized_verification_state(raw_verification_state)
    VERIFICATION_STATES.find { |state| state == raw_verification_state.to_s.downcase }
  end

  def as_params(overrides = {})
    {
      category: category_slug,
      query: query,
      type: tool_type,
      verification: verification_state,
    }.merge(overrides).compact
  end
end
