# frozen_string_literal: true

module Marketplace::Listings
  class OnboardingPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlatformHelper
    delegate :name, :slug, to: :listing, prefix: true

    Query = parse_query <<-'GRAPHQL'
      fragment Root on Query {
        marketplaceListing(slug: $listingSlug) {
          databaseId
          name
          slug
          status
          listingDescriptionIsCompleted
          listingDetailsAreCompleted
          namingAndLinksAreCompleted
          contactInfoIsCompleted
          logoAndFeatureCardAreCompleted
          productScreenshotsAreCompleted
          plansAndPricingAreCompleted
          webhookIsCompleted
          publishedPlanCheck: plans(first: 100, published: true) {
            totalCount
            nodes {
              isPaid
              isDirectBilling
            }
          }
        }
        ...Views::MarketplaceListings::IntegratorAgreement::Root
      }
    GRAPHQL

    attr_reader :listing, :data, :platform_context

    def initialize(data:, current_user: nil, user_session: nil)
      @data = Query::Root.new(data)

      @listing = @data.marketplace_listing
    end

    # If integrator has not created any paid plans, we inform them that they won't
    # be able to create Paid plans after submission (require re-review).
    def require_free_plan_acknowledgement?
      !listing.published_plan_check.nodes.any? { |plan| (plan.is_paid || plan.is_direct_billing) }
    end

    def metrics_progress_payload
      {
        marketplace_listing_id: listing.database_id,
        marketplace_listing_status: listing.status,
        webhook_completed: listing.webhook_is_completed?,
        plans_and_pricing_completed: listing.plans_and_pricing_are_completed?,
        contact_info_completed: listing.contact_info_is_completed?,
        listing_description_completed: listing.listing_description_is_completed?,
        product_screenshots_completed: listing.product_screenshots_are_completed?,
        listing_details_completed: listing.listing_details_are_completed?,
        logo_and_feature_card_completed: listing.logo_and_feature_card_are_completed?,
        naming_and_links_completed: listing.naming_and_links_are_completed?,
      }
    end
  end
end
