# frozen_string_literal: true

module Reminders
  class PersonalReminderViewModel
    # List of personal reminders for user
    def self.all_for(current_user)
      slack = Apps::Internal.integration(:slack)
      organizations = current_user.organizations.to_a

      personal_reminders = PersonalReminder.where(user: current_user, remindable: organizations).includes(:delivery_times)
      GitHub::PrefillAssociations.prefill_belongs_to(personal_reminders, ReminderSlackWorkspace, { reminder_slack_workspace_id: :slack_workspace })
      personal_reminders_by_remindable = personal_reminders.index_by(&:remindable)

      slack_installations_by_target = IntegrationInstallation.where(target: organizations, integration: slack).index_by(&:target)

      organizations.sort_by(&:login).map do |organization|
        new(
          current_user: current_user,
          organization: organization,
          personal_reminder: personal_reminders_by_remindable[organization],
          slack_installation: slack_installations_by_target[organization]
        )
      end
    end

    delegate :slack_workspace, to: :personal_reminder, allow_nil: true

    attr_reader :current_user, :organization, :personal_reminder, :slack_installation
    def initialize(current_user:, organization:, personal_reminder:, slack_installation:)
      @current_user = current_user
      @organization = organization
      @personal_reminder = personal_reminder
      @slack_installation = slack_installation
    end

    def editable?
      editable_hint.nil?
    end

    def configured?
      personal_reminder&.persisted?
    end

    def editable_hint
      return if organization.adminable_by?(current_user)

      if slack_installation.nil?
        "To use scheduled reminders, the Slack integration needs to be installed in the #{organization.login} organization."
      elsif Orgs::RemindersHelper.slack_installation_outdated?(slack_installation)
        "To use scheduled reminders, the Slack integration needs to be updated in the #{organization.login} organization."
      elsif slack_workspaces.empty?
        "To use scheduled reminders, an organization owner needs to connect to a Slack workspace."
      end
    end

    def description
      return unless personal_reminder

      if personal_reminder.delivery_times_text
        "#{personal_reminder.slack_workspace.name} · #{personal_reminder.delivery_times_text}"
      else
        personal_reminder.slack_workspace.name
      end
    end

    private

    def slack_workspaces
      ReminderSlackWorkspace.for_remindable(organization)
    end
  end
end
