# frozen_string_literal: true

class Collections::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :repos_by_nwo, :repos_by_id, :orgs_by_id, :orgs_by_login, :collection, :author

  delegate :name, :description, :description_html, :image_url, :full_collection?, to: :collection

  # Public: Returns a GraphQL Repository based on the given name-with-owner or database ID.
  def repo_for(nwo_or_id)
    if repos_by_nwo && nwo_or_id.to_s.include?("/")
      repos_by_nwo[nwo_or_id]
    elsif repos_by_id
      repos_by_id[nwo_or_id.to_i]
    end
  end

  # Public: Returns a GraphQL Organization based on the given login or database ID.
  def org_for(login_or_id)
    if orgs_by_id && login_or_id.to_s =~ /\A[0-9]+\z/
      orgs_by_id[login_or_id.to_i]
    elsif orgs_by_login
      orgs_by_login[login_or_id]
    end
  end
end
