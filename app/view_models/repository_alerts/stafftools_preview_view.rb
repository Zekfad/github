# frozen_string_literal: true

module RepositoryAlerts
  class StafftoolsPreviewView < ShowView

    attr_reader :vulnerable_version_range

    def after_initialize
      # confirm the addr_reader attributes from the ShowView are not being set, these do not apply for previews
      raise ArgumentError, "repository must be nil for the preview view" unless @repository.nil?
      raise ArgumentError, "manifest_path must be nil for the preview view" unless @manifest_path.nil?
      raise ArgumentError, "state must be nil for the preview view" unless @state.nil?

      # make sure we have a valid range
      case vulnerable_version_range
      when VulnerableVersionRange, PendingVulnerableVersionRange
        true
      else
        raise ArgumentError, "vulnerable_version_range must be either a VulnerableVersionRange or PendingVulnerableVersionRange"
      end

      helpers.extend(NetworkHelper)
    end

    def vulnerability
      case vulnerable_version_range
      when VulnerableVersionRange
        vulnerable_version_range.vulnerability
      when PendingVulnerableVersionRange
        vulnerable_version_range.pending_vulnerability
      end
    end

    def package_name
      vulnerable_version_range.affects
    end

    def page_title
      "Alert preview #{vulnerability.ghsa_id}"
    end

    def state
      "open"
    end

    def alerts
      # make a temp alert
      [
        ::RepositoryVulnerabilityAlert.new(vulnerability: vulnerability.becomes(Vulnerability),
          vulnerable_version_range: vulnerable_version_range.becomes(VulnerableVersionRange),
          created_at: Time.now,
        ),
      ]
    end

    def related_alerts_count
      @related_alerts_count = 0
      return @related_alerts_count
    end

    def related_alerts_path
      nil
    end

    def manifest_path
      # by providing an ecosystem-specific manifest, we get the dependendency upgrade example to render
      case vulnerable_version_range.ecosystem
      when "composer"
        "composer.json"
      when "RubyGems"
        "Gemfile"
      when "npm"
        "package.json"
      when "pip"
        "requirements.txt"
      when "maven"
        "pom.xml"
      when "nuget"
        "packages.config"
      end
    end

    def manifest_blob_path
      nil
    end

    def dismiss_path(reason:)
      nil
    end

    def dependabot_visible?
      false
    end

    def dependency_graph_enabled?
      true
    end
  end
end
