# frozen_string_literal: true

module RepositoryAlerts
  class IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :query_string, :query, :alerts, :current_repository

    DISMISSAL_REASONS = [
      "A fix has already been started",
      "No bandwidth to fix this",
      "Risk is tolerable to this project",
      "Vulnerable code is not actually used",
    ]

    SORT_OPTIONS = [
      ["Newest", "newest"],
      ["Oldest", "oldest"],
      ["Severity", "severity"],
      ["Manifest path", "manifest-path"],
      ["Package name", "package-name"]]

    def each_sort_option
      SORT_OPTIONS.each do |desc, sort|
        yield desc, sort, sort_selected?(sort)
      end
    end

    def open?
      query.open?
    end

    def closed?
      query.closed?
    end

    def state
      open? ? "open" : "closed"
    end

    def total_open_count
      query.open.total_entries
    end

    def total_closed_count
      query.closed.total_entries
    end

    def sort_selected?(str)
      query.sorted_by? RepositoryVulnerabilityAlert::QueryParser::SORT_MAPPING[str]
    end

    def query_string_for(str)
      RepositoryVulnerabilityAlert::QueryParser.combine(query_string, str)
    end

    def alerts_enabled?
      return @alerts_enabled if defined? @alerts_enabled
      @alerts_enabled = current_repository.vulnerability_alerts_enabled?
    end

    def dependency_graph_enabled?
      return @dependency_graph_enabled if defined? @dependency_graph_enabled
      @dependency_graph_enabled = current_repository.dependency_graph_enabled?
    end

    def display_alerts?
      alerts_enabled? && alerts.any?
    end

    def new_to_alerts?
      total_open_count + total_closed_count == 0
    end

    def dismissal_reasons
      DISMISSAL_REASONS
    end

    def toggle_vulnerability_updates_path
      if current_repository.vulnerability_updates_enabled?
        urls.disable_vulnerability_updates_path(current_repository.owner, current_repository)
      else
        urls.enable_vulnerability_updates_path(current_repository.owner, current_repository)
      end
    end

    def dependency_updates
      return @dependency_updates if defined? @dependency_updates

      paths = alerts.map(&:vulnerable_manifest_path).uniq
      packages = alerts.map(&:package_name).uniq
      @dependency_updates = current_repository.dependency_updates.
                                               unresolved_vulnerability.
                                               visible.
                                               complete.
                                               for(path: paths, package: packages)
      GitHub::PrefillAssociations.prefill_associations(@dependency_updates, :pull_request)
      @dependency_updates
    end

    def pull_request_for_alert(alert)
      return unless current_repository.automated_security_updates_visible_to?(current_user)

      alert_update = dependency_updates.find do |update|
        update.package_name == alert.package_name &&
          update.manifest_path == alert.vulnerable_manifest_path &&
            !update.proposed_change_ignored?
      end

      alert_update&.pull_request
    end
  end
end
