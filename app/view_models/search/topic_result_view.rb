# frozen_string_literal: true

module Search
  class TopicResultView
    MAX_VISIBLE_RELATED_TOPICS = 3

    attr_reader :id, :short_description, :name, :repository_count, :topic

    # Create a new TopicResultView from a `page` document hash returned from
    # the Elasticsearch index.
    #
    # hash - Document Hash returned by Elasticsearch
    def initialize(hash)
      raise "Incorrect result type: #{hash['_type'].inspect}" unless hash["_type"] == "topic"

      source = hash["_source"]
      @id = hash["_id"]
      @topic = hash["_model"]
      @name = source["name"]
      @display_name = source["display_name"]
      @short_description = source["short_description"]
      @description = source["description"]
      @created_by = source["created_by"]
      @released = source["released"]
      @featured = source["featured"]
      @curated = source["curated"]
      @aliases = source["aliases"]
      @related = source["related"]
      @repository_count = source["repository_count"]
      @created_at = Time.parse(source["created_at"])
      @updated_at = Time.parse(source["updated_at"])
      @highlights = hash["highlight"]
    end

    def any_related_topics?
      related_topic_names.any?
    end

    def related_topic_names
      @related_topic_names ||= (@aliases | @related).uniq
    end

    def visible_related_topic_names
      @visible_related_topic_names ||= related_topic_names[0...MAX_VISIBLE_RELATED_TOPICS]
    end

    def hidden_related_topic_names
      @hidden_related_topic_names ||=
        related_topic_names[MAX_VISIBLE_RELATED_TOPICS...related_topic_names.length]
    end

    def hidden_related_topic_count
      if hidden_related_topic_names
        hidden_related_topic_names.size
      else
        0
      end
    end

    def include_comma?(index)
      index < visible_related_topic_names.length - 1 || hidden_related_topic_count > 0
    end

    def display_name
      @display_name.presence || name
    end

    def has_logo?
      logo_url.present?
    end

    def logo_url
      @topic.logo_url
    end

    def curated?
      @curated
    end

    def topic_url(topic_name)
      if GitHub.enterprise? # no topic pages on Enterprise, only search
        template_args = { name: topic_name }
        template = "/search?q=topic%3A{name}&type=Repositories"
        addressable = Addressable::Template.new(template).expand(template_args)
        addressable.to_s
      else # link to topic page on dotcom
        "/topics/#{topic_name}"
      end
    end

    def url
      topic_url(name)
    end

    # Returns true if there are highlight fragments for this topic. The
    # highlight fragments contain text from the various topic fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    def hl_short_description
      return @hl_short_description if defined? @hl_short_description

      @hl_short_description = if highlights? && @highlights.key?("short_description")
        @highlights["short_description"].first
      else
        EscapeUtils.escape_html_as_html_safe(short_description)
      end
    end

    # Return the topic display name. The display name may or may not contain highlight tags,
    # but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped display name String.
    def hl_display_name
      return @hl_display_name if defined? @hl_display_name

      @hl_display_name = if highlights? && @highlights.key?("display_name")
        @highlights["display_name"].first
      else
        EscapeUtils.escape_html_as_html_safe(display_name)
      end
    end
  end
end
