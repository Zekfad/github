# frozen_string_literal: true
module Search
  class GitHubAppResultView
    attr_reader :id, :name, :description, :created, :updated, :github_app,
      :primary_category, :secondary_category

    # Create a new GitHubAppResultView from a `github_app` document hash
    # returned from the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    #
    def initialize(hash)
      unless "github_app" == hash["_type"]
        raise "Incorrect result type: #{hash['_type'].inspect}"
      end

      source = hash["_source"]
      @id = hash["_id"]
      @github_app = hash["_model"]
      @name = source["name"]
      @description = source["description"]
      @highlights = hash["highlight"]
      @primary_category = @secondary_category = nil
    end

    def tool_type
      "github_app"
    end

    # Returns true if there are highlight fragments for this action. The
    # highlight fragments contain text from the various action fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    # Return the app name. The name may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped name String.
    def hl_name
      @hl_name ||= hl_description("name.ngram", @name)
    end

    # Return the app short description. The short description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped short description String.
    def hl_short_description
      @hl_short_description ||= hl_description("description", @description)
    end

    def logo_url(size: 62)
      github_app.preferred_avatar_url(size: size)
    end

    def logo_bgcolor
      github_app.bgcolor
    end

    private

    def hl_description(key, value)
      formatted = if highlights? && @highlights.key?(key)
        GitHub::HTML::HighlightedSearchResultPipeline.to_html(@highlights[key].first)
      else
        GitHub::HTML::DescriptionPipeline.to_html(value.to_s)
      end

      HTMLTruncator.new(formatted, 350).to_html(wrap: false)
    end
  end
end
