# frozen_string_literal: true
module Search
  class ResultsView
    include EscapeHelper
    include PlatformHelper
    include PlatformPreloadHelper

    PreloadCommitsQuery = parse_query <<-'GRAPHQL'
      query($ids: [ID!]!) {
        nodes(ids: $ids) {
          ...PlatformPreloadHelper::BaseFragment
          ...Views::Codesearch::HitCommit::Commit
        }
      }
    GRAPHQL

    LOGGER_LABEL = "Search::ResultsView"
    TTL = 3.minutes

    attr_reader :view

    def initialize(view, queries:, type:, search:, unscoped_search:, elastic_search_down:)
      @view                       = view
      @queries                    = queries
      @type                       = type
      @search                     = search
      @unscoped_search            = unscoped_search
      @elastic_search_down        = elastic_search_down
    end

    delegate :controller_name, :action_name, :log_data, :session, :request,
      # Required for platform_execute:
      :stats_ui_enabled?, :params, :unauthorized_saml_organization_ids,
      :unauthorized_ip_whitelisting_organization_ids,
      to: :view

    def current_repository
      view.current_repository
    end

    def current_user
      view.current_user
    end

    def repo_specific?
      !!current_repository
    end

    def code_is_searchable?
      !repo_specific? || current_repository.code_is_searchable?
    end

    def geyser_enabled?
      query.is_a?(Search::Queries::GeyserCodeQuery)
    end

    def render_filter
      key = [query.cache_key, "filter"].join("/")
      locals = {
        results: results,
        elastic_search_down:  @elastic_search_down,
      }
      GitHub.cache.fetch(key, ttl: TTL) do
        view_to_cache = case query
        when Search::Queries::UserQuery
          view.render partial: "codesearch/filters_user", locals: locals
        when Search::Queries::CodeQuery, Search::Queries::GeyserCodeQuery
          view.render partial: "codesearch/filters_code", locals: locals
        when Search::Queries::CommitQuery
          view.render partial: "codesearch/filters_commit", locals: locals
        when Search::Queries::IssueQuery
          view.render partial: "codesearch/filters_issue", locals: locals
        when Search::Queries::RegistryPackageQuery
          view.render partial: "codesearch/filters_package", locals: locals
        when Search::Queries::WikiQuery
          view.render partial: "codesearch/filters_wiki", locals: locals
        when Search::Queries::DiscussionQuery
          unless GitHub.enterprise?
            view.render partial: "codesearch/filters_discussion", locals: locals
          end
        else
          view.render partial: "codesearch/filters_repo", locals: locals
        end

        # Adding some logging to see what the average size of these views are
        log_rendered_view_info("Search::ResultsView#render_filter", view_to_cache)
      end
    end

    def render_sort
      case query
      when Search::Queries::UserQuery
        view.render partial: "codesearch/sort_user", locals: {results: results}
      when Search::Queries::CodeQuery, Search::Queries::GeyserCodeQuery
        view.render partial: "codesearch/sort_code", locals: {
          results: results,
          repository: repo_specific? && current_repository,
          unscoped_search: @unscoped_search,
        }
      when Search::Queries::CommitQuery
        view.render partial: "codesearch/sort_commit", locals: {
          results: results,
          repository: repo_specific? && current_repository,
          elastic_search_down: @elastic_search_down,
          unscoped_search: @unscoped_search,
        }
      when Search::Queries::IssueQuery
        view.render partial: "codesearch/sort_issue", locals: {
          results: results,
          repository: repo_specific? && current_repository,
          unscoped_search: @unscoped_search,
        }
      when Search::Queries::MarketplaceQuery
        view.render partial: "codesearch/sort_marketplace", locals: { results: results }
      when Search::Queries::RegistryPackageQuery
        view.render partial: "codesearch/sort_package", locals: {
          results: results,
          repository: repo_specific? && current_repository,
        }
      when Search::Queries::TopicQuery
        view.render partial: "codesearch/sort_topics", locals: { results: results }
      when Search::Queries::WikiQuery
        view.render partial: "codesearch/sort_wiki", locals: {
          results: results,
          repository: repo_specific? && current_repository,
          unscoped_search: @unscoped_search,
        }
      when Search::Queries::DiscussionQuery
        unless GitHub.enterprise?
          view.render partial: "codesearch/sort_discussion", locals: {
            results: results,
            repository: repo_specific? && current_repository,
            unscoped_search: @unscoped_search,
          }
        end
      else
        locals = { results: results, unscoped_search: @unscoped_search }
        if show_topic_callout?
          locals[:show_topic_callout] = true
          locals[:linked_topic], locals[:curated_topic] = topic_callout_topics
        else
          locals[:show_topic_callout] = false
        end
        view.render partial: "codesearch/sort_repo", locals: locals
      end
    end

    def render_results
      key = [query.cache_key, "results"].join("/")
      GitHub.cache.delete key if results.total == 0

      ### Uncache user results views due to https://github.com/github/github/issues/44320
      if query.is_a?(Search::Queries::UserQuery)
        return view.render partial: "codesearch/results_user", locals: {
          results: results,
          repo_specific: repo_specific?,
        }
      end

      ### Uncache topic results views to avoid CSRF caching
      if query.is_a?(Search::Queries::TopicQuery)
        return view.render partial: "codesearch/results_topic", locals: { results: results }
      end

      GitHub.cache.fetch(key, ttl: TTL) do
        view_to_cache = if is_current_view("Code") && !code_is_searchable?
          view.render partial: "repository_search/not_searchable", locals: {
            results: results,
            repo_specific: repo_specific?,
          }
        else
          case query
          when Search::Queries::CodeQuery
            view.render partial: "codesearch/results_code", locals: {
              results: results,
              repo_specific: repo_specific?,
              current_repository: current_repository,
              unscoped_search: @unscoped_search,
            }
          when Search::Queries::GeyserCodeQuery
            view.render partial: "codesearch/results_geyser_code", locals: {
              results: results,
              repo_specific: repo_specific?,
              current_repository: current_repository,
              unscoped_search: @unscoped_search,
            }
          when Search::Queries::CommitQuery
            commit_promises = results.map do |hit|
              Platform::Loaders::GitObject.load(hit["_model"], hit["_source"]["hash"], expected_type: "commit")
            end

            commits = Promise.all(commit_promises).sync.index_by do |commit|
              [commit.repository, commit.oid]
            end

            preload_platform_objects(commits.values) do |ids|
              platform_execute(PreloadCommitsQuery, variables: { ids: ids }).nodes
            end

            view.render partial: "codesearch/results_commit", locals: {
              results: results,
              repo_specific: repo_specific?,
              current_repository: current_repository,
              search: @search,
              commits: commits,
              preloaded_commits: @preloaded_platform_data,
              unscoped_search: @unscoped_search,
            }
          when Search::Queries::IssueQuery
            view.render partial: "codesearch/results_issue", locals: {
              results: results,
              repo_specific: repo_specific?,
              current_repository: current_repository,
              unscoped_search: @unscoped_search,
            }
          when Search::Queries::MarketplaceQuery
            view.render partial: "codesearch/results_marketplace", locals: {
              results: results,
              search: @search,
            }
          when Search::Queries::RegistryPackageQuery
            view.render partial: "codesearch/results_package", locals: {
              results: results,
              search: @search,
              repo_specific: repo_specific?,
              current_repository: current_repository,
            }
          when Search::Queries::WikiQuery
            view.render partial: "codesearch/results_wiki", locals: {
              results: results,
              repo_specific: repo_specific?,
              unscoped_search: @unscoped_search,
            }
          when Search::Queries::DiscussionQuery
            unless GitHub.enterprise?
              view.render partial: "codesearch/results_discussion", locals: {
                results: results,
                repo_specific: repo_specific?,
                current_repository: current_repository,
                unscoped_search: @unscoped_search,
              }
            end
          else
            view.render partial: "codesearch/results_repo", locals: {
              results: results,
              show_explore_topics: show_explore_topics?,
              repo_specific: repo_specific?,
              unscoped_search: @unscoped_search,
            }
          end
        end

        # We are adding some logging to see what the average size of these results views are.
        log_rendered_view_info("Search::ResultsView#render_results", view_to_cache)
      end
    end

    def link_to_repos(**link_options)
      link_to_menu("Repositories", **link_options)
    end

    def link_to_users(**link_options)
      link_to_menu("Users", **link_options)
    end

    def link_to_code(**link_options)
      link_to_menu("Code", **link_options)
    end

    def link_to_commits(**link_options)
      link_to_menu("Commits", **link_options)
    end

    def link_to_issues(**link_options)
      link_to_menu("Issues", **link_options)
    end

    def link_to_discussions(**link_options)
      return if GitHub.enterprise?

      beta_tag = view.render GitHub::BetaFlagComponent.new(ml: 2, font_weight: :normal, color: :gray, font_size: 6)
      label = view.content_tag(:span, safe_join(["Discussions", beta_tag]))
      options = link_options.merge(label: label, test_selector: "discussions-link")

      link_to_menu("Discussions", **options)
    end

    def link_to_packages(**link_options)
      return unless PackageRegistryHelper.show_packages?
      link_options[:label] = "Packages"
      link_to_menu("RegistryPackages", **link_options)
    end

    def link_to_wikis(**link_options)
      link_to_menu("Wikis", **link_options)
    end

    def link_to_topics(**link_options)
      link_to_menu("Topics", **link_options)
    end

    def link_to_marketplace(**link_options)
      link_to_menu("Marketplace", **link_options)
    end

    # The eligibility criteria for submitting a Geyser dark ship query is:
    #
    # - current_repository must be indexed and eligible for Geyser treatment
    # - current_repository must not be blacklisted from Geyser treatment
    # - current_user performing the query should _not_ be eligible for Geyser treatment
    # - the dark_ship_geyser_search flag should be enabled for this query (by % dial most likely)
    #
    # Eligibility filtering that doesn't require feature flag checks is applied directly in the view
    def query_eligible_for_geyser_dark_ship?
        GitHub.flipper[:dark_ship_geyser_search].enabled? &&
          current_user &&
          !current_user.exact_match_search_enabled? &&
          current_repository.geyser_indexing_enabled?
    end

    def unscoped_query_eligible_for_geyser_dark_ship?
      GitHub.flipper[:dark_ship_geyser_unscoped_search].enabled? &&
          current_user
    end

    private

    def query_has_topic_qualifier?
      query.qualifiers.key?(:topic) && query.qualifiers[:topic].must
    end

    # Should the 'Explore topics' section be shown?
    def show_explore_topics?
      query_has_topic_qualifier?
    end

    def topic_callout_topics
      return @topic_callout_topics if @topic_callout_topics

      linked_topic = Topic.find_by(name: topic_name_from_query)

      unless linked_topic
        @topic_callout_topics = []
        return @topic_callout_topics
      end

      curated_topic = if linked_topic.featured? && linked_topic.curated?
        linked_topic
      elsif (source_topic = linked_topic.alias_source_topic)
        source_topic if source_topic.featured? && source_topic.curated?
      end

      @topic_callout_topics = [linked_topic, curated_topic]
    end

    def topic_qualifier
      query.qualifiers[:topic].must.first
    end

    # Private: Returns a topic pulled from the search query.
    #
    # Returns a String or nil.
    def topic_name_from_query
      return @topic_name_from_query if defined? @topic_name_from_query
      topic_name = nil
      if query_has_topic_qualifier?
        topic_name = Topic.extract_featured_topic_name(topic_qualifier)
      end
      topic_name ||= Topic.extract_featured_topic_name(query.query)
      @topic_name_from_query = topic_name
    end

    # Private: Should a callout to visit a topic page be shown in the search results page?
    #
    # Only shows a callout for topics for which we have curated content that we want to feature.
    #
    # Returns a Boolean.
    def show_topic_callout?
      # Topic pages are not enabled on Enterprise.
      return false if GitHub.enterprise?

      # Don't show a callout for a particular topic when all the search results will be topics.
      return false if is_current_view("Topics")

      # Didn't find a topic in the query for which we had curated content:
      return false unless topic_name_from_query.present?

      # Ensure we found a topic to link to and a curated topic:
      linked_topic, curated_topic = topic_callout_topics
      linked_topic && curated_topic
    end

    # Returns the selected Query whose results are displayed.
    def query
      @queries[@type]
    end

    # Returns true if the `type` is for the results that are currently being
    # viewed.
    #
    # type - The type String
    def is_current_view(type)
      @type == type
    end

    def link_to_menu(type, label: nil, link_classes: "", fetch_counter: true, test_selector: nil)
      link_classes = "menu-item" if link_classes.empty?
      link_classes = "#{link_classes} selected" if is_current_view(type)
      url = view.link_to_search(type: type, p: nil, s: nil, o: nil, q: params[:q])

      label ||= type
      counter = counter(type, fetch_counter: fetch_counter)
      inner = safe_join([label, counter])

      options = { class: link_classes }
      options = options.merge(view.test_selector_data_hash(test_selector)) if test_selector

      view.link_to(inner, url, options)
    end

    def counter(type, fetch_counter: true)
      query = @queries[type]

      total = if is_current_view(type) && type == "Topics"
        if results.includes_public_repositories?
          results.total
        else
          0
        end
      elsif is_current_view(type)
        results.total
      end

      timed_out = is_current_view(type) ? results.timed_out : false

      count = Search::CountView.new(
        query: query,
        type: type,
        count: total,
        timed_out: timed_out,
      )

      if is_current_view(type) || count.cached?
        count.render
      elsif fetch_counter
        url = view.url_with(action: "count", type: type)
        view.content_tag("include-fragment", "", src: url)
      else
        view.content_tag(:span, "", class: "d-none js-codesearch-deferred-count", "data-search-type": type)
      end
    end

    def results
      @results ||=
        begin
          query.execute
        rescue StandardError => boom
          Failbot.report(boom.with_redacting!)
          case query
          when Search::Queries::GeyserCodeQuery
            Search::GeyserResults.empty
          else
            Search::Results.empty
          end
        end
    end

    def log_rendered_view_info(render_context, rendered_view)
      if GitHub.flipper[:code_search_rendered_view_logging].enabled?
        logger_data = {
          error: results.error?,
          label: LOGGER_LABEL,
          page_number: results.page || 1,
          per_page: results.per_page,
          query_class: query.class.name,
          query_context: query.context,
          render_context: render_context,
          rendered_view_size: rendered_view.size,
          repository_id: current_repository&.id,
          results_size: results.size,
          search: @search,
          timed_out: results.timed_out?,
          total_results: results.total,
          type: @type,
          unscoped_search: @unscoped_search,
          user_query: query.user_query,
        }
        GitHub::Logger.log(logger_data)
      end
      rendered_view
    end
  end
end
