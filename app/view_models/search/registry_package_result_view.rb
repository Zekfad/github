# frozen_string_literal: true

module Search
  class RegistryPackageResultView
    attr_reader :source, :id, :package, :name, :summary, :public, :downloads, :updated_at, :topics
    alias :public? :public

    delegate :owner, :repository, to: :package

    # Create a new RegistryPackageResultView from a `registry_package` document
    # hash returned from the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    def initialize(hash)
      raise "Incorrect result type: #{hash['_type'].inspect}" unless "registry_package" == hash["_type"]

      @source = hash["_source"]

      @id = hash["_id"]
      @package = hash["_model"]
      @name = source["name"]
      @summary = source["summary"]
      @public = source["public"]
      @downloads = source["downloads"].to_i
      @updated_at = Time.parse(source["updated_at"])

      @topics = source.fetch("ranked_topics", []).map do |ranked|
        ranked["applied"]
      end.compact
    end

    def owned_by_organization?
      owner.organization?
    end

    # Avoids calling repository.name_with_owner which may result in an additional query.
    # Package owner and repository are preloaded.
    def repo_name_with_owner
      "#{owner}/#{repository}"
    end

    def latest_version
      return @latest_version if @latest_version

      latest = source["versions"].find { |v| v["latest"] }
      latest ||= source["versions"].first

      @latest_version = latest.nil? ? "" : latest["version"]
    end
  end
end
