# frozen_string_literal: true

module Repositories
  class SecurityOverviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repository, :current_user

    INDICATORS = {
      positive: { octicon: "primitive-dot", class: "mr-3 text-green", label: "Enabled" },
      negative: { octicon: "primitive-dot", class: "mr-3 checklist-dot color-yellow-7", label: "Not enabled yet" },
    }.freeze

    def indicator_for_display(condition)
      condition ? INDICATORS[:positive] : INDICATORS[:negative]
    end

    def read_only_viewer?
      !repository.pushable_by?(current_user)
    end

    # security policy

    def security_policy_exists?
      repository.security_policy.exists?
    end

    def security_policy_path
      urls.repository_security_policy_path(repository.owner, repository)
    end

    def security_policy_indicators
      indicator_for_display(security_policy_exists?)
    end

    def security_policy_editable?
      repository.pushable_by?(current_user)
    end

    # Dependabot alerts

    # is the Dependabot alerts tab available
    def dependency_alerts_show?
      repository.show_dedicated_alerts_ui?(current_user)
    end

    # is the Dependabot alerts feature turned on for this repo
    def dependency_alerts_configured?
      repository.vulnerability_updates_enabled?
    end

    # can the current user control the Dependabot alerts feature
    def dependency_alerts_configurable?
      repository.adminable_by?(current_user)
    end

    def dependency_alerts_view_path
      urls.network_alerts_path(repository.owner, repository)
    end

    def dependency_alerts_indicators
      indicator_for_display(dependency_alerts_configured?)
    end

    # code scanning

    def code_scanning_show?
      return false if repository.code_scanning_disabled_by_feature_flag?

      # Show code scanning if feature is enabled for user and repo
      return true if repository.code_scanning_readable_by?(current_user)

      # The beta check below doesn't apply to Enterprise,
      # so return false at this point
      return false if GitHub.enterprise?

      # Show code scanning if the current user
      # can enroll the repository owner in the beta
      if repository.owner.organization?
        repository.owner.adminable_by?(current_user)
      else
        repository.owner == current_user
      end
    end

    def code_scanning_enrolled?
      repository.code_scanning_enabled?
    end

    def code_scanning_public_but_needs_private?
      repository.owner.advanced_security_public_beta_enabled? &&
        !repository.owner.advanced_security_private_beta_enabled? &&
        repository.private?
    end

    def code_scanning_configured?
      repository.code_scanning_analysis_exists?
    end

    def code_scanning_configurable?
      repository.code_scanning_readable_by?(current_user)
    end

    def code_scanning_beta_enrolment_path
      account = if repository.owner.organization?
        repository.owner.login
      else
        current_user&.login
      end
      urls.code_scanning_beta_signup_path(account: account)
    end

    def code_scanning_configure_path
      urls.repository_code_scanning_results_path(repository.owner, repository)
    end

    def code_scanning_view_path
      urls.repository_code_scanning_results_path(repository.owner, repository)
    end

    def code_scanning_indicators
      indicator_for_display(code_scanning_configured?)
    end

    # token scanning

    def token_scanning_show?
      repository.show_token_scanning_ui?(current_user)
    end

    def token_scanning_configured?
      repository.token_scanning_enabled?
    end

    def token_scanning_configurable?
      repository.adminable_by?(current_user)
    end

    def token_scanning_view_path
      urls.repository_token_scanning_results_path(repository.owner, repository)
    end

    def token_scanning_indicators
      indicator_for_display(token_scanning_configured?)
    end

    # security advisories

    # is the security advisories tab available
    def security_advisories_show?
      repository.advisories_enabled?
    end

    def security_advisories_view_path
      urls.repository_advisories_path(repository.owner, repository)
    end

    # always indicate this as active if the feature is enabled for this repo
    def security_advisories_indicators
      indicator_for_display(security_advisories_show?)
    end

    # can the current user create security advisories, or only view them
    def security_advisories_creatable?
      repository.advisory_management_authorized_for?(current_user)
    end
  end
end
