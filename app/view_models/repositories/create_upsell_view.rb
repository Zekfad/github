# frozen_string_literal: true

module Repositories
  class CreateUpsellView
    include ActionView::Helpers::NumberHelper
    include Rails.application.routes.url_helpers
    include TextHelper
    include HydroHelper

    # Initiate a CreateUpsellView object used to control the upsell forms on
    # the new repos page (upgrade or signup for paid).
    #
    # target - User account to be upgraded.
    # current_user - Currently logged-in user
    # request - the rails Request object
    #
    # Returns the new instance.
    def initialize(target, current_user, request:)
      @target = target
      @current_user = current_user
      @request = request
    end

    attr_reader :target, :current_user, :request

    # Is the target user on a free personal plan? We use this to determine
    # what kind of upsell language and form to show them.
    #
    # Returns true if the target account is on a free personal plan.
    def free_personal_plan?
      target.free_plan?
    end

    def organization?
      target.organization?
    end

    # Is the target account a paying customer that can upgrade to a new plan? We
    # use this to determine what kind of upsell language and form to show them.
    #
    # Returns true if the target user is a paying customer.
    def paid_personal_plan?
      target.paying_customer?
    end

    # Should we give the target user the option to upgrade their plan? That is, are
    # they at their repository limit?
    #
    # Returns a boolean whether to show the upsell.
    def show_upgrade?
      !target.can_add_private_repo?
    end

    def largest_plan_reached?
      !target.plan.upgradeable?
    end

    # Is the user allowed to upgrade by checking a checkbox when creating a repo?
    def can_upgrade_while_creating_repo?
      return false if target.next_plan.nil?

      !largest_plan_reached? && legacy_upgrade_restriction.allow_plan?(target.next_plan)
    end

    def legacy_upgrade_restriction
      @legacy_upgrade_restriction ||= ::Billing::PlanChange::LegacyUpgradeRestriction.new(target: target, actor: current_user)
    end

    def per_seat_pricing_model
      return nil unless target.organization?

      @per_seat_pricing_model ||=
        Billing::PlanChange::PerSeatPricingModel.new(
          target,
          seats: target.default_seats,
          plan_duration: target.plan_duration)
    end

    # Should we give the user the option to upgrade and enter a credit card
    # number? The logic should be:
    #
    # - Are they at their repository limit?
    # - We should not have their credit card on file.
    #
    # Returns a boolean whether to show the upsell.
    def show_cc_upgrade?
      return false if current_user.has_any_trade_restrictions?
      return false if !target.at_private_repo_limit?
      return false if target.gift?
      target.needs_valid_payment_method_to_switch_to_plan?(upsell_plan)
    end

    def per_seat_plan_only?
      target.per_seat_plan_only? || upsell_plan.per_seat?
    end

    # What's the plan we should be telling them to upgrade to?
    #
    # Returns a GitHub::Plan
    def upsell_plan
      @upsell_plan ||= if target.user?
        GitHub::Plan.pro
      elsif target.per_seat_plan_only?
        GitHub::Plan.business
      else
        target.next_plan || GitHub::Plan.business
      end
    end

    # How much more is this going to cost? Includes duration.
    #
    # Returns a formatted String of dollars per time.
    def upsell_cost
      cost = target.payment_amount(plan: upsell_plan) - target.payment_amount
      "#{number_to_currency(cost.to_f)} / #{target.plan_duration}"
    end

    def upselled_cost
      cost = target.payment_amount(plan: upsell_plan)
      "#{number_to_currency(cost.to_f)} / #{target.plan_duration}"
    end

    # Namespace under which the fields in the credit card form is nested.
    def credit_card_form_namespace
      "billing"
    end

    # Hide checkbox when user is prompted with cc fields
    def show_upgrade_checkbox?
      !show_cc_upgrade?
    end

    # Show callout to encourage user to switch to per seat pricing
    def show_per_seat_callout?
      organization? && target.plan.per_repository? && !price_diff_for_per_seat.negative? && !target.invoiced?
    end

    # Calculates the price difference between per seat plan and the per repo plan
    def price_diff_for_per_seat
      Billing::Money.new(target.payment_amount(plan: upsell_plan) * 100) - per_seat_pricing_model.total_seats_price
    end

    def per_seat_plan_callout_url
      upgrade_path(
        org: target,
        target: "organization",
        plan: GitHub::Plan.business,
      )
    end

    def per_seat_plan_callout_hydro_data(callout_location: nil, callout_text: nil)
      hydro_click_tracking_attributes("per_seat_plan_callout.click", {
        # N.B.: request_context is set in config/instrumentation/hydro.rb
        actor_id: current_user.id,
        organization_id: target.id,
        callout_page: request.original_fullpath,
        callout_location: callout_location,
        callout_text: callout_text,
        callout_destination: per_seat_plan_callout_url,
        current_plan: target.plan.to_s,
      })
    end

    def per_seat_plan_callout_link_data(callout_location: nil, callout_text: nil)
      data = per_seat_plan_callout_hydro_data(callout_location: callout_location, callout_text: callout_text)
      data["ga-click"] = "New Pricing, upgrade org click, restricted legacy upgrade"
      data
    end
  end
end
