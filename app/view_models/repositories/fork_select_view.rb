# frozen_string_literal: true

module Repositories
  class ForkSelectView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repository, :user, :candidate_names

    def fork_button_tooltip(is_existing_fork:, currently_viewing_fork:, owner:)
      if currently_viewing_fork
        "You’re already looking at #{owner.login}/#{repository.name}."
      elsif is_existing_fork
        "View fork of #{owner.login}/#{repository.name}."
      else
        new_name = candidate_names[owner]
        "Will be created as #{owner.login}/#{new_name}."
      end
    end

    def fork_button_options(is_existing_fork:, currently_viewing_fork:, owner:)
      options = {
        "type" => "submit",
        "name" => "organization",
        "value" => owner,
        "tabindex" => "0",
        "class" => "btn-link f5 text-bold my-2 tooltipped tooltipped-se tooltipped-align-left-1",
        "aria-label" => fork_button_tooltip(is_existing_fork: is_existing_fork, currently_viewing_fork: currently_viewing_fork, owner: owner),
        "title" =>  "@#{owner.login}",
      }

      options["method"] = :get if is_existing_fork
      options["disabled"] = true if currently_viewing_fork
      options
    end

    def fork_options_header
      if all_existing_forks.any?
        "Fork again to a different account"
      else
        "Where should we fork #{repository.name}?"
      end
    end

    # Public: Returns organizations the user can fork into.
    def organizations
      return @organizations if defined?(@organizations)
      partition_organizations!
      @organizations
    end

    # Public: Returns organizations where the user cannot create a fork.
    def organizations_disallowing_forks
      return @disallowed_organizations if defined?(@disallowed_organizations)
      partition_organizations!
      @disallowed_organizations
    end

    def oopf_disallowed_organizations
      return @oopf_organizations if defined?(@oopf_organizations)
      partition_organizations!
      @oopf_organizations
    end

    # Public: Returns personal account and organizations the user can fork into
    def available_options
      return @available_options if defined? @available_options

      fork_owner_ids = repository.network.repositories.where(owner_id: organizations).pluck(:owner_id)

      available_orgs = organizations.select do |org|
        existing = fork_owner_ids.include?(org.id)
        is_current = is_viewing_current_repository?(owner: org)

        !existing && !is_current
      end
      available_orgs.unshift(user) if !existing_user_fork && !is_viewing_current_repository?(owner: user)

      # Eagerly batch fetch candidate names here so they are available when fork_button_options
      # is called from the view on the same set.
      fetch_candidate_names(available_orgs)

      @available_options = available_orgs
    end

    # Public: Returns existing forks of personal account and organizations
    def all_existing_forks
      return @all_existing_forks if defined? @all_existing_forks

      forks = repository.network.repositories.where(owner: organizations)
      forks = forks - [repository]
      forks.unshift(existing_user_fork) if existing_user_fork && !is_viewing_current_repository?(owner: user)
      @all_existing_forks = forks
    end

    # Public: Returns the user's personal fork of the repository, or nil.
    def existing_user_fork
      return @existing_user_fork if defined? @existing_user_fork
      @existing_user_fork = user.my_fork_of(repository)
    end

    # Public: Returns if the fork is the repostitory they are viewing
    def is_viewing_current_repository?(owner:)
      repository.owner == owner
    end

    private

    def fetch_candidate_names(orgs)
      @candidate_names = {}
      candidate_name_promises = orgs.map do |org|
        Platform::Loaders::NewForkName.load(org, repository.name).then do |name|
          @candidate_names[org] = name
        end
      end
      Promise.all(candidate_name_promises).sync
    end

    def partition_organizations!
      @organizations = []
      @disallowed_organizations = []
      @oopf_organizations = []

      orgs = user.organizations.by_login
      can_create_repo_promises = orgs.map do |org|
        if !org.fork_allowed?(repo: repository, user: user)
          next @oopf_organizations << org
        end
        org.async_can_create_repository?(user, visibility: repository.visibility).then do |can_create|
          list = can_create ? @organizations : @disallowed_organizations
          list << org
        end
      end

      Promise.all(can_create_repo_promises).sync
    end
  end
end
