# frozen_string_literal: true

module RepositoryTokenScanning
  class ShowView < View
    include TreeHelper

    attr_reader :result, :commits, :raw_commits_by_oid

    delegate :locations, :first_location, :resolution?, :resolved?, :reopened?, to: :result

    def commit_url(location)
      urls.commit_path(location.commit_oid, repository)
    end

    def has_ignored_locations?
      result.locations.any? { |location| location.exclude_by_path? }
    end

    def location_url(location)
      anchor = "L#{location.start_line}-L#{location.end_line}"
      "#{urls.blob_view_url(location.path, location.commit_oid, repository)}##{anchor}"
    end

    def included_locations_count
      helpers.number_with_delimiter(result.included_locations.size)
    end

    def safe_resolver
      @safe_resolver ||= result.resolver || User.ghost
    end

    def snippet_helper(location, message_text: nil)
      location_hash = {
        start_line: location.start_line,
        end_line: location.end_line,
        start_column: location.normalized_start_column,
        end_column: location.normalized_end_column,
      }
      commit = raw_commits_by_oid[location.commit_oid] if raw_commits_by_oid.present?
      blob = location.blob unless location.found_in_archive?

      RepositoryScanning::CodeSnippetHelper.new(
        blob,
        location_hash,
        commit: commit,
        message_text: message_text,
        message_classes: "border-red",
      )
    end

    def feedback_link
      if current_user.preview_features?
        "https://github.com/github/dsp-token-scanning/issues/215"
      else
        "https://support.github.com/contact/feedback?contact[subject]=Secret+scanning+feedback&contact[category]=security"
      end
    end
  end
end
