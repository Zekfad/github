# frozen_string_literal: true

module RepositoryTokenScanning
  class IndexView < View
    attr_reader :current_page, :query, :page_size

    SORT_OPTIONS = [
      {
        label: "Newest",
        query: "created-desc",
        scope: -> (scope) { scope.newest_first },
        service_enum: :CREATED_DESCENDING,
      },
      {
        label: "Oldest",
        query: "created-asc",
        scope: -> (scope) { scope.oldest_first },
        service_enum: :CREATED_ASCENDING,
      },
      {
        label: "Recently updated",
        query: "updated-desc",
        scope: -> (scope) { scope.most_recently_updated },
        service_enum: :UPDATED_DESCENDING,
      },
      {
        label: "Least recently updated",
        query: "updated-asc",
        scope: -> (scope) { scope.least_recently_updated },
        service_enum: :UPDATED_ASCENDING,
      },
    ].freeze

    NO_TOKENS_FOUND_DESCRIPTIONS = {
      resolved: "No secrets have been closed yet.",
      unscanned: "This repository has not been scanned yet.",
      no_tokens: "There were no secrets found in this repository.",
      no_tokens_for_query: "There were no secrets found for: ",
      scanning_error: "We cannot retrieve results. Please try again later.",
      backfill_scan_pending: "Secret scanning is enabled on this repo and results will be ready soon. You'll get an email as soon as we find any secrets.",
    }.freeze

    def scope
      return TokenScanResult.none if use_token_scanning_service

      scope = repository.token_scan_results.included
      scope = scope.for_token_types(selected_token_type_filters) if selected_token_type_filters?
      scope = selected_sort[:scope].call(scope)
      scope
    end

    def results
      return @results if defined? @results

      if use_token_scanning_service
        @results = WillPaginate::Collection.create(current_page, page_size, results_count) do |pager|
          results = service_response&.data&.tokens || []
          results = GitHub::TokenScanning::Service::Client.wrap_tokens(results, repository)
          pager.replace(results)
        end
      else
        results = resolved? ? scope.resolved : scope.unresolved
        @results = results.paginate(per_page: page_size, page: current_page)
      end
    end

    def resolved?
      query.resolved?
    end

    def results_count
      resolved? ? resolved_count : unresolved_count
    end

    def unresolved_count
      @unresolved_count ||= if use_token_scanning_service
        service_response&.data&.unresolved_count || 0
      else
        scope.unresolved.count
      end
    end

    def resolved_count
      @resolved_count ||= if use_token_scanning_service
        service_response&.data&.resolved_count || 0
      else
        scope.resolved.count
      end
    end

    def has_location?(result)
      result.first_location.present?
    end

    def result_filename(result)
      File.basename(result.first_location.path)
    end

    def result_line(result)
      result.first_location.start_line
    end

    def result_path(result)
      urls.repository_token_scanning_result_path(repository.owner, repository, result.id)
    end

    def resolved_path
      results_path(query: query.stringify(resolved: true))
    end

    def unresolved_path
      results_path(query: query.stringify(resolved: false))
    end

    def all_token_types_results_path
      query_new = Search::Queries::TokenScanningQuery.new(query: nil)
      all_token_query = query_new.stringify(resolved: resolved?, sort: query.sort, sort_state: true)
      results_path(query: all_token_query)
    end

    def clear_filters_results_path
      query_new = Search::Queries::TokenScanningQuery.new(query: nil)
      results_path(query: query_new.stringify(resolved: resolved?))
    end

    def results_path(**params)
      params[:query] = query.raw if query.present? && !params.key?(:query)
      urls.repository_token_scanning_results_path(repository.owner, repository, params)
    end

    def token_type_filters
      filters = TOKEN_TYPES.map do |token_type, details|
        selected = selected_token_type_filters.include?(token_type)
        url = results_path(query: query.stringify(type: token_type, type_state: !selected, resolved: resolved?))
        { label: details[:label], selected: selected, url: url }
      end

      filters.sort_by { |filter| filter[:label] }
    end

    def sort_options
      SORT_OPTIONS.map do |details|
        selected = selected_sort == details
        url = results_path(query: query.stringify(sort: details[:query], sort_state: !selected, resolved: resolved?))
        { label: details[:label], selected: selected, url: url }
      end
    end

    def selected_sort
      return @selected_sort if defined? @selected_sort

      selected_sort = SORT_OPTIONS.find { |details| details[:query] == query.sort }
      @selected_sort = selected_sort || SORT_OPTIONS.first
    end

    def selected_token_type_filters?
      selected_token_type_filters.present?
    end

    def selected_token_type_filters
      @selected_token_type_filters ||= query.types
        .map { |type| type.upcase.to_sym }
        .select { |type| TOKEN_TYPES.key?(type) }
    end

    def selected_tokens
      token_type_filters.map { |token_type| token_type[:label] if token_type[:selected] }.compact.join(", ")
    end

    def scanned?
      GitHub.kv.get("token_scan_status_#{repository.id}").value { false } == "repository_scanned"
    end

    def unscanned?
      !scanned? && !scanning_error? && !repository.token_scan_results.exists?
    end

    def has_pending_backfill?
      repository.has_pending_backfill?
    end

    def scanning_error?
      GitHub.kv.get("token_scan_status_#{repository.id}").value { false } == "scanning_error"
    end

    def show_scanning_error?
      scanning_error? && !repository.token_scan_results.exists?
    end

    def no_tokens_found_description
      if has_pending_backfill?
        NO_TOKENS_FOUND_DESCRIPTIONS[:backfill_scan_pending]
      elsif unscanned?
        NO_TOKENS_FOUND_DESCRIPTIONS[:unscanned]
      elsif show_scanning_error?
        NO_TOKENS_FOUND_DESCRIPTIONS[:scanning_error]
      elsif selected_token_type_filters?
        NO_TOKENS_FOUND_DESCRIPTIONS[:no_tokens_for_query] + selected_tokens + "."
      elsif resolved?
        NO_TOKENS_FOUND_DESCRIPTIONS[:resolved]
      else
        NO_TOKENS_FOUND_DESCRIPTIONS[:no_tokens]
      end
    end

    def feedback_link
      if current_user.preview_features?
        "https://github.com/github/dsp-token-scanning/issues/215"
      else
        "https://support.github.com/contact/feedback?contact[subject]=Secret+scanning+feedback&contact[category]=security"
      end
    end

    def service_response
      return @service_response if defined? @service_response

      @service_response = GitHub::TokenScanning::Service::Client.get_tokens(
        repository_id: repository.id,
        token_types: selected_token_type_filters,
        token_state: resolved? ? :RESOLVED : :OPEN,
        sort_order: selected_sort[:service_enum],
      )
    end

    def include_pagination?
      results_count > 0 && !has_pending_backfill?
    end
  end
end
