# frozen_string_literal: true

module RepositoryTokenScanning
  class View < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    areas_of_responsibility :token_scanning
    attr_reader :repository, :use_token_scanning_service

    TOKEN_TYPES = {
      "ADAFRUIT_AIO_KEY": {
        label: "Adafruit IO Key",
      },
      "ALICLOUD_ACCESS_KEY": {
        label: "Alibaba Cloud AccessKey ID",
      },
      "ALICLOUD_SECRET_KEY": {
        label: "Alibaba Cloud AccessKey Secret",
      },
      "ARMORED_PEM_PRIVATE_KEY": {
        label: "GitHub Private SSH Key",
      },
      "ATLASSIAN_API_TOKEN": {
        label: "Atlassian API Token",
      },
      "ATLASSIAN_JWT": {
        label: "Atlassian JSON Web Token",
      },
      "AWS_KEYID": {
        label: "Amazon AWS Access Key ID",
      },
      "AWS_SECRET": {
        label: "Amazon AWS Secret Access Key",
      },
      "CODESHIP_GENERIC": {
        label: "CloudBees CodeShip Credential",
      },
      "DATABRICKS_API_TOKEN": {
        label: "Databricks API Key",
      },
      "DATADOG_TOKEN": {
        label: "Datadog API Key",
      },
      "DISCORD_API_TOKEN": {
        label: "Discord Bot Token",
      },
      "DROPBOX_OAUTH2_ACCESS_TOKEN": {
        label: "Dropbox Access Token",
      },
      "DROPBOX_OAUTH2_SHORT_LIVED_ACCESS_TOKEN": {
        label: "Dropbox Short Lived Access Token",
      },
      "FRAMEIO_JWT": {
        label: "Frame.io JSON Web Token",
      },
      "FRAMEIO_THIRD_PARTY_DEVELOPER_TOKEN": {
        label: "Frame.io Third-Party Developer Token",
      },
      "GENERIC_JWT": {
        label: "JSON Web Token"
      },
      "GENERIC_OAUTH_CLIENT_ID_OR_SECRET": {
        label: "OAuth Client Credential",
      },
      "GITHUB": {
        label: "GitHub Personal Access Token",
      },
      "GITHUB_APP_TOKEN": {
        label: "GitHub App Token"
      },
      "GOCARDLESS_LIVE_ACCESS_TOKEN": {
        label: "GoCardless Live Access Token",
      },
      "GOCARDLESS_SANDBOX_ACCESS_TOKEN": {
        label: "GoCardless Sandbox Access Token",
      },
      "GOOGLE_API_KEY": {
        label: "Google API Key",
      },
      "GOOGLE_GCP_PRIVATE_KEY_ID": {
        label: "Google Cloud Platform Private Key ID",
      },
      "GOOGLE_GCP_SERVICE_CERTIFICATE": {
        label: "Google Cloud Platform Service Certificate",
      },
      "HUBSPOT_HAPIKEY": {
        label: "Hubspot API Key",
      },
      "HUBSPOT_SMTP": {
        label: "Hubspot SMTP Credential",
      },
      "MAILCHIMP_API": {
        label: "Mailchimp API Key",
      },
      "MAILGUN": {
        label: "Mailgun API Key",
      },
      "MAILGUN_LEGACY": {
        label: "Mailgun Legacy API Key",
      },
      "MAILGUN_SMTP": {
        label: "Mailgun SMTP Credential",
      },
      "MESSAGEBIRD_TOKEN": {
        label: "MessageBird Token",
      },
      "MESSAGEBIRD_TOKEN_2": {
        label: "MessageBird Token",
      },
      "MICROSOFT_AAD_USER_CREDENTIAL": {
        label: "Microsoft Azure Active Directory User Credential"
      },
      "MICROSOFT_AZURE_SQLCONNSTR_V1": {
        label: "Microsoft Azure SQL Connection String",
      },
      "MICROSOFT_AZURE_STORAGEACCOUNTKEY_V1": {
        label: "Microsoft Azure Storage Account Key",
      },
      "MICROSOFT_AZURE_SUBMGMTCERT_V1": {
        label: "Microsoft Azure Service Management Certificate",
      },
      "MICROSOFT_INTERNAL_ACTIVE_DOMAIN_USER_CREDENTIAL": {
        label: "Microsoft Corporate Network User Credentials"
      },
      "MICROSOFT_SAS_TOKEN": {
        label: "Microsoft SAS Token",
      },
      "MICROSOFT_VSTS_PAT": {
        label: "Microsoft Azure DevOps Personal Access Token",
      },
      "NPM_TOKEN": {
        label: "npm Authentication Token",
      },
      "NUGET_API_KEY": {
        label: "NuGet API Key",
      },
      "PALANTIR_JWT": {
        label: "Palantir JSON Web Token",
      },
      "POSTMAN_API_KEY_V1": {
        label: "Postman API Key v1",
      },
      "POSTMAN_API_KEY_V2": {
        label: "Postman API Key v2",
      },
      "PROCTORIO_LINKAGE_KEY": {
        label: "Proctorio Linkage Key (JWT LTI v1.x and 2.0)",
      },
      "PROCTORIO_SECRET_KEY_V2": {
        label: "Proctorio Secret Key (LTI v2.0)",
      },
      "SAMSARA_API_ACCESS_TOKEN": {
        label: "Samsara API Access Token",
      },
      "SAMSARA_OAUTH2_ACCESS_TOKEN": {
        label: "Samsara OAuth Access Token",
      },
      "SLACK": {
        label: "Slack API Token",
      },
      "SLACK_WEBHOOK": {
        label: "Slack Incoming Webhook URL",
      },
      "SLACK_WORKFLOW_WEBHOOK": {
        label: "Slack Workflow Webhook URL",
      },
      "SSLMATE_API_KEY": {
        label: "SSLMate API Key",
      },
      "SSLMATE2_API_KEY": {
        label: "SSLMate API Key",
      },
      "SSLMATE_CLUSTER_SECRET": {
        label: "SSLMate Cluster Secret",
      },
      "STRIPE": {
        label: "Stripe API Key",
      },
      "TENCENT_CLOUD_SECRET_ID": {
        label: "Tencent Cloud Secret ID",
      },
      "TERRAFORM_CLOUD_ENTERPRISE_TOKEN": {
        label: "Terraform Cloud / Enterprise",
      },
      "TWILIO_ACCOUNT_SID": {
        label: "Twilio Account String Identifier",
      },
      "TWILIO_API_KEY_SID": {
        label: "Twilio API Key",
      },
    }.freeze

    def humanize_token_type(token_type)
      token = TOKEN_TYPES[token_type.to_sym]
      if token.present?
        token[:label]
      else
        token_type.titleize
      end
    end

    def token_type_description(result)
      humanize_token_type(result.token_type)
    end

    def resolution_description(resolution)
      case resolution
      when "revoked" then "revoked"
      when "false_positive" then "false positive"
      when "used_in_tests" then "used in tests"
      when "wont_fix" then "won't fix"
      end
    end

    def resolutions
      {
        revoked: "Revoked",
        false_positive: "False positive",
        used_in_tests: "Used in tests",
        wont_fix: "Won't fix",
      }
    end

    def resolve_path(resolution:, id: nil)
      urls.repository_token_scanning_resolve_path(repository.owner, repository, resolution: resolution, id: id)
    end
  end
end
