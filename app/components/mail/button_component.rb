# frozen_string_literal: true

# Use this to create a centered button in your email. Useful as the primary call-to-action.
#
# <%= render(Mail::ContainerComponent.new) do %>
#   <%= render(Mail::ButtonComponent.new(text: "Click me", url: "github.com", classes: "btn-primary btn-large")) %>
# <% end %>
class Mail::ButtonComponent < ApplicationComponent
  include ClassNameHelper
  attr_reader :classes, :text, :url

  def initialize(text:, url:, classes: nil)
    @classes = class_names("btn", classes)
    @text = text
    @url = url
  end
end
