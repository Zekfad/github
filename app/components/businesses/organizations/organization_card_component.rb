# frozen_string_literal: true

class Businesses::Organizations::OrganizationCardComponent < ApplicationComponent
  include HydroHelper

  DIALOG_OPTIONS = [
    :organization_settings,
    :remove_organization,
  ]

  def initialize(
    business:,
    organization:,
    show_options: false,
    members_count: 0,
    repos_count: 0,
    subtitle: "",
    render_border: false,
    render_repo_and_member_details: false
  )
    @business = business
    @organization = organization
    @show_options = show_options
    @members_count = members_count
    @repos_count = repos_count
    @subtitle = subtitle
    @render_border = render_border
    @render_repo_and_member_details = render_repo_and_member_details
  end

  private

  def render?
    @business.present? && @organization.present? && logged_in?
  end

  def org_click_hydro_attributes
    hydro_click_tracking_attributes(
      "enterprise_account.profile_organization_click", {
        enterprise_id: @business.id,
        organization_id: @organization.id,
        actor_id: current_user.id
      }
    )
  end

  def option_available?(option)
    case option
    when :organization_settings
      @organization.adminable_by?(current_user)
    when :remove_organization
      GitHub.flipper[:remove_enterprise_organizations].enabled?(current_user) && @business.can_self_serve?
    else
      false
    end
  end

  def show_options_dialog?
    return false unless @show_options
    return false unless @business.owner?(current_user)

    DIALOG_OPTIONS.each { |option| return true if option_available?(option) }

    false
  end

  def render_repo_and_member_details?
    @render_repo_and_member_details && @business.owner?(current_user)
  end

  def organization_admins
    return @organization_admins if defined?(@organization_admins)
    @organization.admins.order(login: :asc).first(100)
  end

  def organization_admin_logins
    return @organization_admin_logins if defined?(@organization_admin_logins)
    @organization_admin_logins = organization_admins.map(&:login).to_sentence
  end
end
