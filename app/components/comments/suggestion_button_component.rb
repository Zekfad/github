# frozen_string_literal: true
module Comments
  class SuggestionButtonComponent < ApplicationComponent
    include SuggestedChangesAnalyticsHelper, VarnishHelper

    def initialize(
      outdated: false,
      lines: [],
      contains_deletions: false,
      disabled: false,
      missing_commit: false,
      empty_selection: false,
      responsive: true,
      pull_request: nil
    )
      @outdated = outdated
      @lines = lines
      @contains_deletions = contains_deletions
      @disabled = disabled
      @missing_commit = missing_commit
      @empty_selection = empty_selection
      @responsive = responsive
      @pull_request = pull_request
    end

    def responsive?
      @responsive
    end

    def disabled?
      @disabled
    end

    def data_lines
      @lines.map { |line| line[1..-1] }.join("\n")
    end

    def data
      suggested_changes_click("insert_suggestion", current_user, @pull_request)
    end

    def label
      if @outdated
        "Suggestions cannot be applied on outdated comments."
      elsif @contains_deletions
        "Applying suggestions on deleted lines is not supported."
      elsif @missing_commit || @empty_selection
        "Suggestions cannot be applied on this comment."
      else
        "Insert a suggestion <ctrl+g>"
      end
    end

    def dismiss_notice_url
      dismiss_notice_path(UserNotice::MULTI_LINE_SUGGESTIONS_NOTICE)
    end
  end
end
