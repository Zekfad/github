# frozen_string_literal: true

module Dashboard
  module Sponsors
    class EligibleOrgsBannerComponent < ApplicationComponent
      include HydroHelper

      CONTRIBUTION_THRESHOLD = 50

      def initialize(repo: nil, container_class: "")
        @repo = repo
        @container_class = container_class
      end

      private

      def render?
        return false unless GitHub.sponsors_enabled?
        return false unless user_present_and_not_new?
        return false unless feature_flag_enabled?
        return false unless this_banner_not_dismissed?
        return false unless eligible?

        GlobalInstrumenter.instrument("sponsors.element_displayed", {
          element: "SPONSORED_ORGS_CTA",
          actor: current_user,
          sponsorable: @repo&.owner,
        })

        true
      end

      def user_present_and_not_new?
        logged_in? && !current_user.is_new?
      end

      def feature_flag_enabled?
        GitHub.flipper[:sponsors_eligible_orgs_banner].enabled?(current_user)
      end

      def this_banner_not_dismissed?
        !current_user.dismissed_notice?("sponsors_eligible_orgs_banner")
      end

      # Checks whether to display the banner.
      #
      # - If a repo is given check eligibility against the repo.
      # - Otherwise, look at the current user's organizations.
      def eligible?
        if @repo.present?
          eligible_repo?
        else
          eligible_orgs?
        end
      end

      # Checks whether the current user meets the condition to display the banner.
      #
      # - Does the current user owns/admins any organization?
      # - Are any of those orgs not already in the Sponsors program?
      # - Are there active public repos from orgs not in the program?
      # - Is there at least one repo with at least 50 commit contributions?
      def eligible_orgs?
        return @eligible_orgs if defined?(@eligible_orgs)

        @eligible_orgs = false
        return @eligible_orgs if owned_org_ids_not_in_program.blank?
        return @eligible_orgs if owned_public_repo_ids.blank?

        @eligible_orgs = eligible_contributions?(repo_ids: owned_public_repo_ids)
      end

      # Checks whether the current repo meets the condition to display the banner.
      #
      # - Is the current repo public and active?
      # - Is the current repo owned by an organization?
      # - Is the owning organization already in the Sponsors program?
      # - Does the current user owns/admins the organization?
      # - Does the current repo have at least 50 commit contributions?
      def eligible_repo?
        return @eligible_repo if defined?(@eligible_repo)

        @eligible_repo = false
        return @eligible_repo unless @repo.public?
        return @eligible_repo unless @repo.active?
        return @eligible_repo unless @repo.owner.organization?
        return @eligible_repo if @repo.owner.sponsors_membership.present?
        return @eligible_repo unless @repo.owner.adminable_by?(current_user)

        @eligible_repo = eligible_contributions?(repo_ids: [@repo.id])
      end

      def eligible_contributions?(repo_ids:)
        ::CommitContribution
          .for_repository(repo_ids)
          .group(:repository_id)
          .having("SUM(commit_count) >= ?", CONTRIBUTION_THRESHOLD)
          .exists?
      end

      # Returns all organization IDs not currently in the Sponsors program,
      # that are owned/admin by the current user
      def owned_org_ids_not_in_program
        return @owned_org_ids_not_in_program if defined?(@owned_org_ids_not_in_program)

        @owned_org_ids_not_in_program = []

        all_owned_org_ids = current_user.owned_organization_ids
        return @owned_org_ids_not_in_program if all_owned_org_ids.blank?

        owned_org_ids_in_program = ::SponsorsMembership
          .where(sponsorable_id: all_owned_org_ids, sponsorable_type: :User)
          .pluck(:sponsorable_id)

        @owned_org_ids_not_in_program = all_owned_org_ids - owned_org_ids_in_program
      end

      # Returns all public repository IDs for organizations owned/admin by
      # the current user and not in the Sponsors program.
      def owned_public_repo_ids
        return @owned_public_repo_ids if defined?(@owned_public_repo_ids)

        @owned_public_repo_ids = ::Repository
          .public_scope
          .active
          .where(owner_id: owned_org_ids_not_in_program)
          .recently_updated
          .limit(100)
          .pluck(:id)
      end

      def hydro_click_data
        hydro_click_tracking_attributes(
          "sponsors.button_click",
          button: "SPONSORED_ORGS_CTA",
          sponsored_developer_login: @repo&.owner&.login,
        )
      end

      def learn_more_url
        "#{GitHub.help_url}/en/github/supporting-the-open-source-community-with-github-sponsors/about-github-sponsors"
      end
    end
  end
end
