# frozen_string_literal: true

module Stafftools
  module Sponsors
    class TogglePayoutsComponent < ApplicationComponent
      def initialize(sponsors_listing:)
        @sponsors_listing = sponsors_listing
        @stripe_account = sponsors_listing.stripe_connect_account
        @sponsorable = sponsors_listing.sponsorable
      end

      private

      def payouts_disabled?
        return @payouts_disabled if defined? @payouts_disabled
        @payouts_disabled = @stripe_account.automated_payouts_disabled?
      end

      def button_disabled?
        if payouts_disabled?
          !@stripe_account.verified?
        else
          false
        end
      end

      def title
        if payouts_disabled?
          "Enable payouts"
        else
          "Disable payouts"
        end
      end

      def body
        if payouts_disabled?
          "Enable automatic Stripe payouts for this account"
        else
          "Disable automatic Stripe payouts for this account"
        end
      end

      def render?
        @stripe_account.present?
      end
    end
  end
end
