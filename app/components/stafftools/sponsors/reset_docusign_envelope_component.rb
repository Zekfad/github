# frozen_string_literal: true

module Stafftools
  module Sponsors
    class ResetDocusignEnvelopeComponent < ApplicationComponent
      def initialize(sponsorable:, envelope:)
        @sponsorable = sponsorable
        @envelope = envelope
      end

      private

      def title
        voidable? ? "Void" : "Deactivate"
      end

      def body
        if voidable?
          "Void this DocuSign document. Note: this action will notify the recipient."
        else
          "Deactivate this DocuSign document."
        end
      end

      def btn_text
        voidable? ? "Void" : "Deactivate"
      end

      def btn_title
        voidable? ? "Void DocuSign document" : "Deactivate DocuSign document"
      end

      def form_method
        completed? ? :put : :delete
      end

      def voidable?
        @envelope&.voidable?
      end

      def completed?
        @envelope&.completed?
      end

      def render?
        voidable? || completed?
      end
    end
  end
end
