# frozen_string_literal: true

module Stafftools
  module Sponsors
    class MembershipActionsComponent < ApplicationComponent
      def initialize(sponsorable:, membership:, queue_cursor: "")
        @sponsorable = sponsorable
        @membership = membership
        @queue_cursor = queue_cursor
      end

      private

      def render?
        @sponsorable.present? && @membership.present?
      end

      def unsupported_country?
        !(@sponsorable.user? || @membership.eligible_for_stripe_connect?)
      end

      def trade_restricted?
        @sponsorable.has_any_trade_restrictions?
      end
    end
  end
end
