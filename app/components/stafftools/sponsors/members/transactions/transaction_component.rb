# frozen_string_literal: true

class Stafftools::Sponsors::Members::Transactions::TransactionComponent < ApplicationComponent
  include PlanHelper

  DATETIME_FORMAT = "%b %-d %Y, %l:%M %p"

  def initialize(transaction:)
    @transaction = transaction
  end

  private

  def render?
    @transaction.present?
  end

  def sponsor
    @sponsor ||= @transaction.user
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def sponsor_login
    @sponsor_login ||= sponsor&.login
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def action
    @action ||= begin
      case @transaction.action
      when "sp_added" then "New sponsorship"
      when "sp_cancelled" then "Cancelled sponsorship"
      when "sp_changed" then "Tier changed"
      else
        "N/A"
      end
    end
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def timestamp
    @timestamp ||= @transaction.timestamp&.strftime(DATETIME_FORMAT)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def current_tier
    @current_tier ||= @transaction.current_subscribable
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def current_tier_name
    @current_tier_name ||= current_tier&.name
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def current_tier_price
    @current_tier_price ||= begin
      amount_in_dollars = if yearly_plan?
        current_tier&.yearly_price_in_dollars
      else
        current_tier&.monthly_price_in_dollars
      end

      casual_currency(amount_in_dollars)
    end
  end

  def current_tier_string
    @current_tier_string ||= current_tier ? "#{current_tier_name}: #{current_tier_price}" : "N/A"
  end

  def old_tier
    @old_tier ||= @transaction.old_subscribable
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def old_tier_name
    @old_tier_name ||= old_tier&.name
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def old_tier_price
    @old_tier_price ||= begin
      amount_in_dollars = if yearly_plan?
        old_tier&.yearly_price_in_dollars
      else
        old_tier&.monthly_price_in_dollars
      end

      casual_currency(amount_in_dollars)
    end
  end

  def old_tier_string
    @old_tier_string ||= old_tier ? "#{old_tier_name}: #{old_tier_price}" : "N/A"
  end

  def yearly_plan?
    @transaction.plan_duration == "year"
  end
end
