# frozen_string_literal: true

class Stafftools::Sponsors::Members::PayoutProbationStatusComponent < ApplicationComponent
  def initialize(listing:)
    @listing = listing
  end

  private

  def probation_status_icon
    if @listing.exempt_from_payout_probation?
      "skip"
    elsif @listing.completed_payout_probation?
      "check"
    else
      "primitive-dot"
    end
  end

  def probation_status_color
    if @listing.payout_probation_started_at.nil? && @listing.payout_probation_ended_at.nil?
      "gray"
    elsif @listing.on_payout_probation?
      "yellow-7"
    else
      "green"
    end
  end

  def probation_status_text
    if @listing.payout_probation_started_at.nil? && @listing.payout_probation_ended_at.nil?
      "Not started"
    elsif @listing.on_payout_probation?
      if Date.current.after?(@listing.payout_probation_end_date) && !@listing.docusign_completed?
        "Pending (Docusign not completed)"
      else
        "Pending"
      end
    elsif @listing.exempt_from_payout_probation?
      "Exempt (not matchable)"
    else
      "Complete"
    end
  end

  def probation_date_text
    return if @listing.exempt_from_payout_probation?
    return unless start = @listing.payout_probation_started_at
    start_date = start.strftime("%Y-%m-%d")

    if @listing.on_payout_probation?
      end_date = @listing.payout_probation_end_date.strftime("%Y-%m-%d")

      "Started #{start_date}, ends #{end_date}"
    elsif @listing.payout_probation_ended_at.present?
      end_date = @listing.payout_probation_ended_at.strftime("%Y-%m-%d")
      "Started #{start_date}, ended #{end_date}"
    end
  end
end
