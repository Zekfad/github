# frozen_string_literal: true

class Stafftools::Sponsors::Members::Transfers::TransferComponent < ApplicationComponent
  def initialize(transfer:, sponsorable:, latest_payout:)
    @transfer      = transfer
    @sponsorable   = sponsorable
    @latest_payout = latest_payout
  end

  private

  attr_reader :transfer, :sponsorable, :latest_payout

  def render?
    transfer.present? && sponsorable.present?
  end

  def formatted_with_default_currency(money_value)
    "#{money_value.format(no_cents_if_whole: false)} #{money_value.currency.iso_code}"
  end

  def formatted_with_destination_currency(money_value, destination_currency)
    destination_currency = destination_currency || "USD"
    destination = money_value.exchange_to(destination_currency)
    "#{destination.format(no_cents_if_whole: false)} #{destination_currency}"
  end

  # If the transfer's destination is also 'USD', we shouldn't gum up the
  # output with the conversion.
  def show_localized_amount?
    transfer.destination_currency&.upcase != "USD"
  end

  def transferred_at
    transfer.transferred_at.strftime("%Y-%m-%d")
  end

  def has_unreversed_match_amount?
    transfer.match_amount > transfer.match_amount_reversed
  end

  def show_reversal_button?
    return false unless has_unreversed_match_amount?
    return true if latest_payout.blank?

    # ensure that this transfer occurred after the last payout occurred,
    # so we don't reverse matching for already paid out funds.
    latest_payout.created < transfer.transferred_at.to_i
  end
end
