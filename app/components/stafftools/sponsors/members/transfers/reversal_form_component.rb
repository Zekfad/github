# frozen_string_literal: true

class Stafftools::Sponsors::Members::Transfers::ReversalFormComponent < ApplicationComponent
  def initialize(transfer:, sponsorable:)
    @transfer    = transfer
    @sponsorable = sponsorable
  end

  private

  attr_reader :transfer, :sponsorable

  def render?
    transfer.present? && sponsorable.present?
  end

  def max_match_amount_to_reverse
    (transfer.match_amount - transfer.match_amount_reversed).format(no_cents_if_whole: false)
  end
end
