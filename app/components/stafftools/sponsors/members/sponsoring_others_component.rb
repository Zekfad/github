# frozen_string_literal: true

class Stafftools::Sponsors::Members::SponsoringOthersComponent < ApplicationComponent

  SPONSORSHIPS_LIMIT = 10

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  attr_reader :sponsorable

  def render?
    sponsorable.present? && total_count > 0
  end

  def sponsorships
    sponsorable.sponsorships_as_sponsor.includes(:sponsorable).limit(SPONSORSHIPS_LIMIT)
  end

  def total_count
    sponsorable.sponsorships_as_sponsor.count
  end

  def overflow_count
    total_count - sponsorships.size
  end

  def sentence
    sponsoring_links = sponsorships.map do |sponsorship|
      link_to(sponsorship.sponsorable, user_path(sponsorship.sponsorable))
    end

    sponsoring_others_sentence = html_safe_to_sentence(sponsoring_links)

    if overflow_count > 0
      sponsoring_others_sentence << " and #{pluralize(overflow_count, 'other')}"
    end

    sponsoring_others_sentence
  end
end
