# frozen_string_literal: true

module Stafftools
  module Sponsors
    class DisableMatchForSponsorsComponent < ApplicationComponent
      include AvatarHelper

      def initialize(sponsorable:, listing:, membership:)
        @sponsorable = sponsorable
        @listing     = listing
        @membership  = membership
      end

      private

      attr_reader :sponsorable, :listing, :membership

      def render?
        return false if sponsorable.blank?
        return false if listing.blank?
        return false if membership.blank?
        return false if sponsorable.organization?
        return false if listing.match_disabled?
        membership.joined_waitlist_before_match_deadline?
      end
    end
  end
end
