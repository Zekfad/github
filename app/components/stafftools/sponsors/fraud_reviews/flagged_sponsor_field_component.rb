# frozen_string_literal: true

class Stafftools::Sponsors::FraudReviews::FlaggedSponsorFieldComponent < ApplicationComponent
  FIELDS = {
    matched_current_client_id: "Client ID",
    matched_historical_client_id: "Historical client ID",
    matched_current_ip: "IP address",
    matched_historical_ip: "Historical IP address",
    matched_current_ip_region_and_user_agent: "IP region and user agent",
  }

  # flagged_record - the FraudFlaggedSponsor record.
  # field - a Symbol that is a key in the FIELDS constant.
  def initialize(flagged_record:, field:)
    @flagged_record  = flagged_record
    @field           = field
  end

  private

  attr_reader :flagged_record, :field

  def render?
    flagged_record.present? && title.present?
  end

  def title
    FIELDS[field]
  end

  def value
    if flagged_record[field].present?
      flagged_record[field]
    else
      "Not matched"
    end
  end

  def value_class
    if flagged_record[field].present?
      "text-mono"
    else
      "text-italic"
    end
  end

  def search_url
    return if flagged_record[field].blank?

    case field
    when :matched_current_ip, :matched_historical_ip
      stafftools_audit_log_path(query: "actor_ip:#{value} action:sponsors.*")
    when :matched_current_client_id, :matched_historical_client_id
      stafftools_audit_log_path(query: "data.client_id:#{value} action:sponsors.*")
    end
  end
end
