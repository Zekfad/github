# frozen_string_literal: true

class Stafftools::BundledLicenseAssignments::ListItemComponent < ApplicationComponent
  def initialize(assignment:)
    @assignment = assignment
  end

  def label
    if @assignment.assigned?
      profile_link @assignment.user.login
    else
      @assignment.email
    end
  end

  def icon
    if @assignment.assigned?
      render GitHub::AvatarComponent.new(actor: @assignment.user, size: 32)
    else
      octicon("mail-read", height: 32, class: "avatar")
    end
  end

  def revoked_status
    @assignment.revoked? ? "revoked" : "non-revoked"
  end
end
