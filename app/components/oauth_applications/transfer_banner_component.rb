# frozen_string_literal: true

module OauthApplications
  class TransferBannerComponent < ApplicationComponent
    def initialize(application:)
      @application = application
    end

    def render?
      @application.pending_transfer?
    end

    def transfer
      return @transfer if defined?(@transfer)
      @transfer = @application.transfer
    end

    def transfer_target
      return @transfer_target if defined?(@transfer_target)
      @transfer_target = transfer.target
    end

    def transfer_path
      return @transfer_path if defined?(@transfer_path)

      @transfer_path = if transfer_target.organization?
        settings_org_application_transfer_path(transfer_target, transfer)
      else
        settings_user_application_transfer_path(transfer_target, transfer)
      end
    end

    def transfer_path_with_id
      return @transfer_path_with_id if defined?(@transfer_path_with_id)

      @transfer_path_with_id = if transfer_target.organization?
        settings_org_application_transfer_path(transfer_target, transfer.id)
      else
        settings_user_application_transfer_path(transfer_target, transfer.id)
      end
    end
  end
end
