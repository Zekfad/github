# frozen_string_literal: true

module PullRequests
  class StateComponent < ApplicationComponent
    include OcticonsCachingHelper

    STATE_OPTIONS = [:open, :merged, :closed]

    def initialize(state:, is_draft: false, size: Primer::StateComponent::SIZE_DEFAULT, revisions_enabled: false, revision: nil, **args)
      @state, @is_draft, @size, @revisions_enabled, @revision, @args = state, is_draft, size, revisions_enabled, revision, args
    end

    private

    def render?
      STATE_OPTIONS.include?(@state)
    end

    def octicon_height
      @size == :small ? 14 : 16
    end

    def label
      if @state == :open && @is_draft
        if @revisions_enabled
          "In progress"
        else
          "Draft"
        end
      else
        @state.to_s.capitalize
      end
    end

    def color
      case @state
      when :closed
        :red
      when :merged
        :purple
      when :open
        if @is_draft
          :default
        else
          :green
        end
      end
    end

    def title
      t = "Status: #{label}"
      t = "#{t}, Revision: #{@revision}" if @revision
      t
    end

    def revision_number
      return nil unless @state == :open && !@is_draft
      return nil if @revision.to_i < 2
      @revision
    end

    def octicon_name
      case @state
      when :merged
        "git-merge"
      when :closed, :open
        "git-pull-request"
      end
    end
  end
end
