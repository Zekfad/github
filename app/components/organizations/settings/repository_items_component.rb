# frozen_string_literal: true

class Organizations::Settings::RepositoryItemsComponent < ApplicationComponent

  def initialize(next_page:, organization:, repositories:, selected_repositories:, total_count:, policy_type:, policy_id: "")
    @next_page = next_page
    @organization = organization
    @repositories = repositories
    @selected_repositories = selected_repositories
    @show_next_page = (next_page - 1) * Orgs::ActionsSettings::RepositoryItemsController::PER_PAGE < total_count # current page * repos per page < total_count
    @policy_type = policy_type
    @policy_id = policy_id
  end

  def aria_id_prefix
        return @policy_type unless @policy_id.present?
        "#{@policy_type}-#{@policy_id}"
  end

  def render?
    @show_next_page || @repositories.any?
  end
end
