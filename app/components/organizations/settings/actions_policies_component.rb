# frozen_string_literal: true

class Organizations::Settings::ActionsPoliciesComponent < ApplicationComponent

  def initialize(organization:)
    @organization = organization
  end

  def show_repository_selector?
    selected_repos_only?
  end

  def selected_repos_count
   @_allowed_repos_count ||= @organization.actions_allowed_entities.count
  end

  def action_policy_list
    [
      GitHub::Menu::ButtonComponent.new(
        text: "All repositories",
        description: "Actions can be run by any repository in the organization",
        replace_text: "All repositories",
        checked: ::Actions::PolicyChecker.enabled_for_all_entities?(current_policy),
        name: "policy",
        value: Actions::PolicyResolver::ALL_ENTITIES,
        disabled: !configurable?,
        type: "submit",
      ),
      GitHub::Menu::ButtonComponent.new(
        text: "Selected repositories",
        description: "Actions can only be run by specifically selected repositories",
        replace_text: "Selected repositories",
        checked: selected_repos_only?,
        name: "policy",
        value: Actions::PolicyResolver::SELECTED_ENTITIES,
        disabled: !configurable?,
        type: "submit",
      ),
      GitHub::Menu::ButtonComponent.new(
        text: "Disabled",
        description: "Actions are disabled for all repositories in the organization",
        replace_text: "Disabled",
        checked: disabled?,
        name: "policy",
        value: Configurable::ActionExecutionCapabilities::DISABLED,
        disabled: !configurable?,
        type: "submit",
      ),
    ]
  end

  def action_type_policy_list
    [
      GitHub::Menu::ButtonComponent.new(
        text: "Enable local and third-party Actions for this organization",
        description: "This allows all repositories to execute any Action, whether the code for the Action exists within the same repository, same organization, or a repository owned by a third party.",
        replace_text: "Enable local and third-party Actions for this organization",
        checked: disable_action_type_selection? || ::Actions::PolicyChecker.all_action_types_allowed?(current_policy),
        name: "policy",
        value: selected_repos_only? ? Configurable::ActionExecutionCapabilities::ALL_ACTIONS_FOR_SELECTED : Configurable::ActionExecutionCapabilities::ALL_ACTIONS,
        disabled: disable_action_type_selection? || !permissions_options.include?(Configurable::ActionExecutionCapabilities::ALL_ACTIONS),
        type: "submit",
      ),
      GitHub::Menu::ButtonComponent.new(
        text: "Enable local Actions only for this organization",
        description: "This allows all repositories to execute any Action as long as the code for the Action exists within the same repository.",
        replace_text: "Enable local Actions only for this organization",
        checked: ::Actions::PolicyChecker.local_actions_only?(current_policy),
        name: "policy",
        value: selected_repos_only? ? Configurable::ActionExecutionCapabilities::LOCAL_ONLY_FOR_SELECTED : Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS,
        disabled: disable_action_type_selection? || !permissions_options.include?(Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS),
        type: "submit",
      ),
    ]
  end

  def configurable?
    permissions_options.count > 1
  end

  def disable_action_type_selection?
    disabled?
  end

  private

  def repos
    @_repos ||= @organization.visible_repositories_for(current_user)
  end

  def selected_repos_only?
    ::Actions::PolicyChecker.enabled_for_selected_entities_only?(current_policy)
  end

  def disabled?
    ::Actions::PolicyChecker.disabled?(current_policy)
  end

  def current_policy
    @_current_policy ||= @organization.action_execution_capabilities
  end

  def permissions_options
    @_options ||= @organization.available_action_execution_options
  end
end
