# frozen_string_literal: true

class Organizations::Settings::RepositorySelectionComponent < ApplicationComponent

  REPOSITORY_TYPES = ["All", "Public", "Private", "Sources", "Forks", "Archives", "Mirrors"].freeze

  def initialize(organization:, repositories:, selected_repositories:, policy_type:, policy_id: "", discard_changes_on_close: false)
    @organization = organization
    @repositories = repositories
    @selected_repositories = selected_repositories
    @policy_type = policy_type
    @policy_id = policy_id
    @discard_changes_on_close = discard_changes_on_close
  end

  def aria_id_prefix
    return @policy_type unless @policy_id.present?
    "#{@policy_type}-#{@policy_id}"
  end
end
