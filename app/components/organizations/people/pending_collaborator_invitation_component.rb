# frozen_string_literal: true

# A component to handle how we display Invitations to Pending Collaborators
class Organizations::People::PendingCollaboratorInvitationComponent < ApplicationComponent
  include AvatarHelper

  attr_reader :pending_collaborator_invitation,
              :organization,
              :repository

  def initialize(pending_collaborator_invitation:, invitee:, organization:, repository:)
    @pending_collaborator_invitation = pending_collaborator_invitation
    @invitee = invitee
    @organization = organization
    @repository = repository
  end

  # An identifer used to help make lists of this compenent unique in the DOM
  #
  # Returns a String.
  def dom_id
    "outside-collaborator_#{pending_collaborator_invitation.id}"
  end

  # If the Pending Collaborator has a name attribute, use that otherwise
  # fallback the othe user's login.
  #
  # Returns a String.
  def name_or_login
    name? ? name : pending_collaborator.login
  end

  # If the invitation is for a user and that user has a name attribute, return
  # true. Otherwise return false.
  #
  # Returns a Boolean.
  def name?
    !email_invitation? && pending_collaborator.name.present?
  end

  # If the invitation has been sent to a user, then return the invitee,
  # otherwise return the invitation email.
  #
  # Returns a User or a String.
  def pending_collaborator
    @invitee || pending_collaborator_invitation.email
  end

  # If the invitaiton was sent to an email, return true, otherwise return false.
  #
  # Returns a Boolean.
  def email_invitation?
    !!pending_collaborator_invitation.email
  end

  # The login of the invitation's organization.
  #
  # Returns a String.
  def organization_login
    organization.login
  end

  # The string version of either the invitee or the invitation email for a11y.
  #
  # Returns a String.
  def aria_label
    pending_collaborator.to_s
  end

  private

  # Pirvate If the invitation has been sent to an email, return the email as a
  # name, otherwise use the invited user's name.
  #
  # Returns a String.
  def name
    email_invitation? ? pending_collaborator : pending_collaborator.name
  end
end
