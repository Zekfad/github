# frozen_string_literal: true

module Discussions
  class IconConversationComponent < ApplicationComponent
    ICON_COLOR_DEFAULT = :default
    ICON_COLOR_MAPPINGS = {
      default: "text-gray",
      gray: "text-gray",
      white: "text-white",
    }
    ICON_COLOR_OPTIONS = [ICON_COLOR_DEFAULT, *ICON_COLOR_MAPPINGS.keys]

    def initialize(icon_color: ICON_COLOR_DEFAULT)
      @icon_color = fetch_or_fallback(ICON_COLOR_OPTIONS, icon_color, ICON_COLOR_DEFAULT)
    end

    private

    def component_class_name
      ICON_COLOR_MAPPINGS[@icon_color]
    end
  end
end
