# frozen_string_literal: true

module Discussions
  class SpammyBadgeComponent < ApplicationComponent
    private

    def render?
      logged_in? && current_user.site_admin?
    end
  end
end
