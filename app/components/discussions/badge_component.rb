# frozen_string_literal: true

module Discussions
  class BadgeComponent < ApplicationComponent
    include ClassNameHelper

    def initialize(text:, tooltip: nil, test_selector: nil)
      @tooltip = tooltip
      @text = text
      @test_selector_name = test_selector
    end

    private

    attr_reader :tooltip, :text, :test_selector_name
  end
end
