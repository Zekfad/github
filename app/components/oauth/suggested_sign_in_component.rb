# frozen_string_literal: true

class Oauth::SuggestedSignInComponent < ApplicationComponent
  def initialize(application:, original_url:, raw_suggestion:)
    @application = application
    @original_url = original_url
    @raw_suggestion = raw_suggestion
  end

  def render?
    return false if @raw_suggestion.blank?
    return false if @raw_suggestion == current_user.login

    suggested_login.present?
  end

  def suggested_login
    return @suggest_sign_in if defined?(@suggest_sign_in)
    @suggest_sign_in = nil

    # Save loading a User object into memory
    return unless User::LOGIN_REGEX.match?(@raw_suggestion)
    return unless User.where(login: @raw_suggestion).exists?

    @suggest_sign_in = @raw_suggestion
  end
end
