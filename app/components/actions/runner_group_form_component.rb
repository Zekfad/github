# frozen_string_literal: true

module Actions
  class RunnerGroupFormComponent < ApplicationComponent

    ENTERPRISE_VISIBILITIES = {
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED => {
        label: "Selected organizations",
        description: "Runners can be used by specifically selected organizations",
      },
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_ALL => {
        label: "All organizations",
        description: "Runners can be used by all organizations",
      }
    }.freeze

    ORG_VISIBILITIES = {
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED => {
        label: "Selected repositories",
        description: "Runners can be used by specifically selected repositories",
      },
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_ALL => {
        label: "All repositories",
        description: "Runners can be used by public and private repositories",
      },
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_PRIVATE => {
        label: "Private repositories",
        description: "Runners can be used by private repositories",
      },
    }.freeze

    BUSINESS_ORG_VISIBILITIES = ORG_VISIBILITIES.merge(
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_PRIVATE => {
        label: "Private and internal repositories",
        description: "Runners can be used by private and internal repositories",
      },
    ).freeze

    BUSINESS_ORG_INHERITED_GROUP_VISIBILITIES = ORG_VISIBILITIES.merge(
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_ALL => {
        label: "All repositories",
        description: "Runners can be used by private and internal repositories",
      },
    ).except(GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_PRIVATE).freeze

    def initialize(owner:, owner_settings:, runner_group: nil, has_business: false)
      @owner = owner
      @owner_settings = owner_settings
      @runner_group = runner_group
      @has_business = has_business
    end

    def is_update?
      @runner_group.present?
    end

    def title
      is_update? ? "Update group" : "Create group"
    end

    def dropdown_title
      return "New group" unless is_update?

      @runner_group.inherited? ? "Edit #{target_type} access" : "Edit name and #{target_type} access"
    end

    def form_path
      is_update? ? @owner_settings.update_runner_group_path(id: @runner_group.id) : @owner_settings.create_runner_group_path
    end

    def aria_id_prefix
      is_update? ? "runner-group-#{@runner_group.id}" : "new-runner-group"
    end

    def show_selected_targets?
      !is_update? || visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED
    end

    def selection_component_path
      @owner_settings.runner_group_targets_path(id: @runner_group&.id)
    end

    def visibility_menu_title
      "#{target_type.capitalize} access:"
    end

    def visibilities
      if @owner.organization?
        if @has_business
          is_update? && @runner_group.inherited? ? BUSINESS_ORG_INHERITED_GROUP_VISIBILITIES : BUSINESS_ORG_VISIBILITIES
        else
          ORG_VISIBILITIES
        end
      else
        ENTERPRISE_VISIBILITIES
      end
    end

    def visibility
      # Until Actions Service adds the allowPublic option,
      # Enterprise runners won't queue to public repos
      # We should map inherited enterprise runner groups
      # as the Private option is effectively the same as All
      return @runner_group.visibility unless @has_business && @runner_group.inherited?

      if @runner_group.visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_PRIVATE
        GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_ALL
      else
        @runner_group.visibility
      end
    end

    def selected_visibility
      is_update? ? visibility : default_visibility
    end

    def default_visibility
      GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED
    end

    def target_type
      @owner.organization? ? "repository" : "organization"
    end
  end
end
