# frozen_string_literal: true

module Explore
  class SignupPromptComponent < ApplicationComponent
    def initialize(source:)
      @source = source
    end

    private

    def render?
      !logged_in? && GitHub.signup_enabled?
    end
  end
end
