# frozen_string_literal: true

module Apps
  class BetaFeatureComponent < ApplicationComponent
    with_content_areas :note

    BETA_FEATURES = {
      device_authorization_grant_opt_in: {
        global_flag: :device_authorization_grant,
        title: "Device authorization flow",
      }
    }

    def initialize(application:, feature_flag:)
      @application  = application
      @feature_flag = feature_flag
      @user         = @application.user
    end

    def render?
      GitHub.flipper[global_flag].enabled?(owner)
    end

    def beta_feature_enabled?
      return @beta_feature_enabled if defined?(@beta_feature_enabled)
      @beta_feature_enabled = GitHub.flipper[@feature_flag].enabled?(@application)
    end

    def submit_value
      beta_feature_enabled? ? "Opt-out" : "Opt-in"
    end

    def title
      BETA_FEATURES[@feature_flag][:title]
    end

    def toggle_method
      beta_feature_enabled? ? :delete : :post
    end

    def toggle_path
      if @user.organization?
        settings_org_applications_beta_features_path(@user, @application)
      else
        settings_user_applications_beta_features_path(@application)
      end
    end

    def toggle_value
      beta_feature_enabled? ? "Disable" : "Enable"
    end

    private

    def owner
      return @owner if defined?(@owner)
      @owner = @application.owner
    end

    def global_flag
      return @global_flag if defined?(@global_flag)
      @global_flag = BETA_FEATURES[@feature_flag][:global_flag]
    end
  end
end
