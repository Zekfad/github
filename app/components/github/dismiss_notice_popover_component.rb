# frozen_string_literal: true

module GitHub
  class DismissNoticePopoverComponent < ApplicationComponent
    def initialize(title:, notice_type:, message:, z_index: nil)
      @title = title
      @message = message
      @notice_type = notice_type
      @z_index = z_index
    end

    private

    attr_reader :title, :notice_type, :message, :z_index

    def popover_test_selector
      "popover_#{notice_type}"
    end
  end
end
