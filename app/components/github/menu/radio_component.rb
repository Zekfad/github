# frozen_string_literal: true

module GitHub
  module Menu
    class RadioComponent < ApplicationComponent
      attr_reader :text, :checked, :replace_text, :input_classes

      def initialize(text:, checked:, name:, value:, replace_text: nil, description: nil, input_classes: nil, data: {})
        @name = name
        @checked = checked
        @value = value
        @text = text
        @replace_text = replace_text
        @description = description
        @data = data
        @input_classes = input_classes
      end
    end
  end
end
