# frozen_string_literal: true

module GitHub
  module Menu
    class LinkComponent < ApplicationComponent
      attr_reader :text, :checked, :replace_text

      def initialize(href:, text: nil, checked: nil, replace_text: nil, description: nil, data: {})
        @href = href
        @checked = checked
        @text = text
        @replace_text = replace_text
        @description = description
        @data = data
      end

      def role
        @checked.nil? ? "menuitem" : "menuitemradio"
      end

      def checkable?
        ["menuitemradio", "menuitemcheckbox"].include?(role) && !@checked.nil?
      end
    end
  end
end
