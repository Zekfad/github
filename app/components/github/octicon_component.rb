# frozen_string_literal: true

module GitHub
  class OcticonComponent < ApplicationComponent
    include ClassNameHelper
    include OcticonsCachingHelper

    SIZE_DEFAULT = :small
    SIZE_MAPPINGS = {
      SIZE_DEFAULT => 16,
      :medium => 32,
      :large => 64,
    }.freeze
    SIZE_OPTIONS = SIZE_MAPPINGS.keys

    def initialize(icon:, size: SIZE_DEFAULT, **kwargs)
      @icon, @kwargs = icon, kwargs

      @kwargs[:height] = SIZE_MAPPINGS[size]
      @kwargs[:class] = class_names(@kwargs[:class], Primer::Classify.call(**@kwargs))
    end

    def call
      octicon(@icon, **@kwargs)
    end
  end
end
