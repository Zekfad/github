# frozen_string_literal: true

module GitHub
  class AvatarComponent < ApplicationComponent
    include AvatarHelper

    DEFAULT_SIZE = 20
    ALLOWED_SIZES = [DEFAULT_SIZE, 32, 48]

    def initialize(actor:, size: DEFAULT_SIZE, **args)
      @actor, @args = actor, args

      @size = fetch_or_fallback(ALLOWED_SIZES, size, DEFAULT_SIZE)
    end

    private

    def component_class_names
      Primer::Classify.call(**@args.merge(classes: avatar_class_names(@actor)))
    end
  end
end
