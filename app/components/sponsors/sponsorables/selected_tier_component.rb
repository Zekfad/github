# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class SelectedTierComponent < ApplicationComponent
      def initialize(sponsorable:, sponsor:, sponsorship: nil, tier:, preview: false)
        @sponsorable = sponsorable
        @sponsor = sponsor
        @tier = tier
        @sponsorship = sponsorship
        @preview = preview
      end

      private

      def render?
        @tier.present?
      end

      def tier_is_sponsorship_tier?
        current_tier == @tier
      end

      def section_header
        return "Selected tier" if @sponsorship.blank?

        if tier_is_sponsorship_tier?
          "Current tier"
        else
          "New tier"
        end
      end

      def edit_tiers_path
        sponsorable_path(@sponsorable.login,
          preview: @preview,
          editing: true,
          sponsor: @sponsor.login,
        )
      end

      def current_tier
        return unless @sponsorship

        if pending_downgrade?
          pending_tier_change.subscribable
        else
          @sponsorship.tier
        end
      end

      def pending_downgrade?
        return false unless pending_tier_change?
        !pending_tier_change.cancellation?
      end

      def pending_tier_change?
        pending_tier_change.present?
      end

      def pending_tier_change
        return @pending_tier_change if defined?(@pending_tier_change)
        @pending_tier_change = @sponsorship.pending_tier_change
      end
    end
  end
end
