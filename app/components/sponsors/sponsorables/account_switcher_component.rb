# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class AccountSwitcherComponent < ApplicationComponent
      CONTACT_US_URL = "https://support.github.com/contact?form%5Bsubject%5D=GitHub+Sponsors:+Invoice+Billing&tags=sponsors"
      MAX_ACCOUNTS_TO_FETCH = 100

      include AvatarHelper
      include SponsorsButtonsHelper

      def initialize(sponsorable:,
                    sponsor:,
                    sponsorship: nil,
                    selected_tier: nil,
                    preview: false,
                    editing: false)
        @sponsorable = sponsorable
        @sponsor = sponsor
        @sponsorship = sponsorship
        @selected_tier = selected_tier
        @preview = preview
        @editing = editing
      end

      private

      def render?
        logged_in?
      end

      def eligible_accounts
        accounts_by_eligibility[:eligible]
      end

      def ineligible_accounts
        accounts_by_eligibility[:ineligible]
      end

      def accounts_by_eligibility
        @accounts_by_eligibility ||= begin
          accounts.each_with_object({ eligible: [], ineligible: [] }) do |account, result|
            eligibility = account.invoiced? ? :ineligible : :eligible
            result[eligibility].push(account)
          end
        end
      end

      def accounts
        unless corporate_sponsors_credit_card_enabled?
          return [@sponsor]
        end
        @accounts ||= current_user.potential_sponsor_accounts.limit(MAX_ACCOUNTS_TO_FETCH)
      end

      def sponsorable_path_params
        {
          tier_id: @selected_tier&.id,
          preview: @preview,
          editing: editing?
        }
      end

      def url_for_account(account)
        params = sponsorable_path_params.merge(sponsor: account.login)

        if editing?
          sponsorable_path(@sponsorable, params)
        else
          sponsorable_sponsorships_path(@sponsorable, params)
        end
      end

      def editing?
        @editing
      end

      def hydro_attrs
        sponsors_button_hydro_attributes(
          :ACCOUNT_SWITCHER_OPEN,
          @sponsorable.login
        )
      end

      def corporate_sponsors_credit_card_enabled?
        return @corporate_sponsors_credit_card_enabled if defined?(@corporate_sponsors_credit_card_enabled)
        @corporate_sponsors_credit_card_enabled = current_user.corporate_sponsors_credit_card_enabled?
      end
    end
  end
end
