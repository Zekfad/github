# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class TotalTierAmountComponent < ApplicationComponent
      def initialize(tier:, sponsor: nil, sponsorable: nil)
        @tier = tier
        @sponsor = sponsor
        @sponsorable = sponsorable
      end

      def formatted_amount
        amount = case plan_duration
        when "year"
          @tier.yearly_price_in_cents
        else
          @tier.monthly_price_in_cents
        end
        Billing::Money.new(amount).format
      end

      def plan_duration
        return "month" unless @sponsor
        @sponsor.plan_duration.downcase
      end

      def show_match_messaging?
        @sponsor.present? && @sponsorable.present?
      end
    end
  end
end
