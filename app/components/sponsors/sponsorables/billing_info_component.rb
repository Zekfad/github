# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class BillingInfoComponent < ApplicationComponent
      include AvatarHelper

      def initialize(sponsorable:,
                     sponsor:,
                     selected_tier:,
                     editing: false,
                     privacy_level: nil,
                     email_opt_in: nil)
        @sponsorable = sponsorable
        @sponsor = sponsor
        @selected_tier = selected_tier
        @editing = editing
        @privacy_level = privacy_level
        @email_opt_in = email_opt_in
      end

      private

      def payment_method
        @payment_method ||= @sponsor.payment_method
      end

      def payment_method_is_valid?
        payment_method&.valid_payment_token?
      end

      def payment_method_is_paypal?
        payment_method.paypal?
      end

      def viewer_card_type
        payment_method.card_type
      end

      def viewer_card_last_four
        "ending #{payment_method.last_four}" if payment_method.last_four
      end

      def viewer_card_expiration
        return "" unless exp_date = payment_method.expiration_date
        exp_date.strftime("%-m/%Y")
      end

      def viewer_paypal_email
        payment_method.paypal_email
      end

      def return_url
        sponsorable_path(
          id:            @sponsorable.login,
          sponsor:    @sponsor.login,
          tier_id:       @selected_tier.id,
          editing:       @editing,
          privacy_level: @privacy_level,
          email_opt_in:  @email_opt_in ? "on" : "off"
        )
      end

      def account_type
        if @sponsor.organization?
          "Organization"
        else
          "Personal"
        end
      end

      def billing_modal_path
        if @sponsor.user?
          payment_modal_path(return_to: return_url, sponsors: true)
        elsif @sponsor.organization?
          org_payment_modal_path(@sponsor.login, return_to: return_url, sponsors: true)
        end
      end
    end
  end
end
