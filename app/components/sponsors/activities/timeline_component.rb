# frozen_string_literal: true

class Sponsors::Activities::TimelineComponent < ApplicationComponent
  def initialize(sponsorable:, page: 1, per_page: Sponsors::ActivitiesController::PER_PAGE, period:)
    @sponsorable = sponsorable
    @page = page
    @per_page = per_page
    @period = fetch_or_fallback(SponsorsActivity::VALID_PERIODS, period, SponsorsActivity::DEFAULT_PERIOD)
  end

  private

  def render?
    @sponsorable.present?
  end

  def activities
    # Using .to_a here so that the activity items are loaded and will be consistent when used for
    # pagination and checking first/last activity. When used as a scope, flaky test failures
    # indicated that when the first activity in the list had the same timestamp as other activities,
    # successive uses of the scope could return different results each time.
    @activities ||=
      @sponsorable.sponsors_activities.
        for_period(@period).
        order(timestamp: :desc).
        paginate(page: @page, per_page: @per_page).
        preload(:sponsor, :sponsors_tier, :old_sponsors_tier).
        to_a
  end

  def has_next_page?
    activities.total_pages > @page
  end

  def first_activity
    activities.first if @page == 1
  end

  def last_activity
    activities.last unless has_next_page?
  end
end
