# frozen_string_literal: true

class Sponsors::Activities::ActivityComponent < ApplicationComponent
  include AvatarHelper
  include BotHelper
  include ClassNameHelper
  include PlanHelper

  def initialize(activity:, first:, last:)
    @activity = activity
    @first = fetch_or_fallback([true, false], first, false)
    @last = fetch_or_fallback([true, false], last, false)
  end

  private

  def render?
    @activity.present?
  end

  def timeline_item_classes
    class_names(
      "TimelineItem",
      "pt-0" => @first,
      "pb-0" => @last,
    )
  end

  def sponsor
    @sponsor ||= @activity.sponsor
  end

  def cycle
    @cycle ||= sponsor.plan_duration
  end

  def badge_color
    class_names("text-pink" => @activity.is_increase?)
  end

  def badge_octicon
    if @activity.is_new_sponsorship? || @activity.is_sponsor_match_disabled?
      "heart-fill"
    elsif @activity.is_cancelled_sponsorship? || @activity.is_pending_cancellation?
      "x"
    elsif @activity.is_tier_change? || @activity.is_pending_tier_change?
      if @activity.is_increase?
        "arrow-up"
      else
        "arrow-down"
      end
    elsif @activity.is_refund?
      "dash"
    end
  end

  def description
    if @activity.is_new_sponsorship?
      "started sponsoring you"
    elsif @activity.is_cancelled_sponsorship?
      "cancelled their sponsorship"
    elsif @activity.is_tier_change?
      "updated their sponsorship"
    elsif @activity.is_pending_cancellation?
      "scheduled a sponsorship cancellation"
    elsif @activity.is_pending_tier_change?
      "scheduled a sponsorship update"
    elsif @activity.is_refund?
      "received a refund for their subscription"
    end
  end

  def price
    amount_in_dollars = if sponsor.yearly_plan?
      @activity.sponsors_tier.yearly_price_in_dollars
    else
      @activity.sponsors_tier.monthly_price_in_dollars
    end

    casual_currency(amount_in_dollars)
  end

  def old_price
    amount_in_dollars = if sponsor.yearly_plan?
      @activity.old_sponsors_tier.yearly_price_in_dollars
    else
      @activity.old_sponsors_tier.monthly_price_in_dollars
    end

    casual_currency(amount_in_dollars)
  end

  def is_possessive?
    @activity.is_sponsor_match_disabled?
  end

  def show_matching_icon?
    @activity.is_new_sponsorship? && @activity.matched_sponsorship?
  end
end
