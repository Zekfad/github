# frozen_string_literal: true

class Sponsors::Activities::ShowComponent < ApplicationComponent
  DATE_FORMAT = "%b %-d"

  def initialize(sponsorable:, page: 1, per_page: Sponsors::ActivitiesController::PER_PAGE, period:)
    @sponsorable = sponsorable
    @page = page
    @per_page = per_page
    @period = fetch_or_fallback(SponsorsActivity::VALID_PERIODS, period, SponsorsActivity::DEFAULT_PERIOD)
  end

  private

  def render?
    @sponsorable.present?
  end

  def activities
    @sponsorable.sponsors_activities.for_period(@period)
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def first_activity_date
    @first_activity_date ||= if @period == :alltime
      activities.order(timestamp: :asc).first&.timestamp&.strftime(DATE_FORMAT)
    else
      period_start
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def period_menu_tooltip
    "Note: activity is not retroactive and is only available from December 2019"
  end

  def period_start
    offset = ::SponsorsActivity::PERIOD_OFFSET_MAPPING[@period]
    start_date = if offset
      Date.today - offset.days
    else
      @sponsorable.sponsors_listing.published_at || @sponsorable.sponsors_listing.created_at
    end

    start_date.strftime(DATE_FORMAT)
  end

  def period_end
    Date.today.strftime(DATE_FORMAT)
  end
end
