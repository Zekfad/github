# frozen_string_literal: true

class Sponsors::Activities::SelectPeriodComponent < ApplicationComponent
  def initialize(period:, sponsorable:)
    @period = period
    @sponsorable = sponsorable
  end

  private

  def selected?(period_option)
    period_option == @period
  end

  def title(period_option)
    if period_option == :alltime
      "All-time"
    else
      "Past #{period_option.capitalize}"
    end
  end
end
