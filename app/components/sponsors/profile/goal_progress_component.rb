# frozen_string_literal: true

class Sponsors::Profile::GoalProgressComponent < ApplicationComponent
  include AvatarHelper

  SPONSORS_STACK_LIMIT = 7

  def initialize(sponsorable:, sponsor: nil, goal:, preview: false)
    @sponsorable = sponsorable
    @sponsor = sponsor
    @goal = goal
    @preview = preview
  end

  private

  def render?
    return false if @sponsorable.blank?
    return true if @preview

    @goal.present?
  end

  def goal_percent_complete
    return 0 if @goal.blank?
    [1, @goal.percent_complete.to_i].max
  end

  def sponsors_to_render
    return @sponsors_to_render if defined?(@sponsors_to_render)

    sponsorship_scope = @sponsorable
      .sponsorships_as_sponsorable
      .preload(:sponsor)
      .ranked(for_user: current_user)
      .active
      .privacy_public
      .order(created_at: :asc)

    @sponsors_to_render = if sponsor_already_sponsoring?
      [@sponsor] + sponsorship_scope
        .where.not(sponsor_id: @sponsor.id)
        .limit(SPONSORS_STACK_LIMIT - 1)
        .map(&:sponsor)
    else
      sponsorship_scope.limit(SPONSORS_STACK_LIMIT).map(&:sponsor)
    end
  end

  def sponsors_text
    return "Be the first to sponsor this goal!" unless sponsors_count.positive?
    return @sponsors_text if defined?(@sponsors_text)

    count = sponsors_count - 1
    others_text = "and #{count} #{"other".pluralize(count)}"

    @sponsors_text = if sponsor_already_sponsoring?
      if count.positive?
        "You #{others_text} sponsor this goal"
      else
        "You sponsor this goal"
      end
    elsif sponsors_to_render.any?
      sponsor_login = sponsors_to_render.first.login
      if count.positive?
        "#{sponsor_login} #{others_text} sponsor this goal"
      else
        "#{sponsor_login} sponsors this goal"
      end
    else
      if sponsors_count == 1
        "1 other sponsors this goal"
      else
        "#{sponsors_count} others sponsor this goal"
      end
    end
  end

  def sponsors_count
    return @sponsors_count if defined?(@sponsors_count)
    @sponsors_count = @sponsorable.sponsorships_as_sponsorable.active.count
  end

  def sponsor_already_sponsoring?
    return false if @sponsor.nil?
    return @sponsor_already_sponsoring if defined?(@sponsor_already_sponsoring)
    @sponsor_already_sponsoring = @sponsorable.already_sponsored_by?(@sponsor)
  end

  def avatar_modifier_class
    if sponsors_count >= 3
      "AvatarStack--three-plus"
    elsif sponsors_count == 2
      "AvatarStack--two"
    end
  end

  def current_values_preview
    return unless @preview

    sponsors_data = "data-preview-current-value-sponsors=#{sponsors_count}"

    monthly_sponsorship = @sponsorable.sponsors_listing&.subscription_value.to_i / 100
    sponsorships_data = "data-preview-current-value-sponsorships=#{monthly_sponsorship}"

    "#{sponsors_data} #{sponsorships_data}"
  end

  def percentage_preview_css_class
    return unless @preview
    "js-sponsors-goal-percentage-preview"
  end

  def percentage_bar_preview_css_class
    return unless @preview
    "js-sponsors-goal-percentage-bar-preview"
  end

  def target_value_preview_css_class
    return unless @preview
    "js-sponsors-goal-target-progress-preview"
  end

  def margin_preview_css_class
    return "mb-5" unless @preview
    "mb-3"
  end
end
