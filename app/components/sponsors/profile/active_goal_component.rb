# frozen_string_literal: true

class Sponsors::Profile::ActiveGoalComponent < ApplicationComponent
  include AvatarHelper

  def initialize(sponsorable:, goal: nil, preview: false)
    @sponsorable = sponsorable
    @goal = goal
    @preview = preview
  end

  private

  def render?
    return false if @sponsorable.blank?

    @goal.present? || @preview
  end

  def goal_base_title
    "@#{@sponsorable.login}'s goal is to"
  end

  def goal_title
    return "" if @goal.blank?

    if @goal.total_sponsors_count?
      "have #{@goal.title}"
    else
      "earn #{@goal.title}"
    end
  end

  def goal_description
    @goal&.description.presence || "No description yet"
  end

  def target_value_preview_css_class
    return unless @preview
    "js-sponsors-goal-target-preview"
  end

  def description_preview_css_class
    return unless @preview
    "js-sponsors-goal-description-preview"
  end
end
