# frozen_string_literal: true

class Sponsors::Profile::SponsorshipCanceledSurveyComponent < ApplicationComponent
  def initialize(sponsorable:, sponsor:, sponsorship: nil)
    @sponsorable = sponsorable
    @sponsorship = sponsorship
    @sponsor = sponsor
  end

  private

  def render?
    return false unless @sponsorship
    return false unless @sponsor.present?
    return false unless GitHub.flipper[:sponsorship_canceled_survey].enabled?(@sponsor)
    return false unless sponsor_survey.survey
    return false if sponsor_survey.completed?
    return false unless @sponsorship&.has_pending_cancellation?

    GlobalInstrumenter.instrument("sponsors.element_displayed", {
      element: "SPONSORSHIP_CANCELED_SURVEY",
      actor: current_user,
      sponsor: @sponsor,
      sponsorable: @sponsorable,
    })

    true
  end

  def canceled_reason_question
    sponsor_survey.canceled_reason_question
  end

  def sponsor_survey
    @sponsor_survey ||= Sponsors::SponsorshipCanceledSurvey.new(
      sponsor: @sponsor,
      sponsorable: @sponsorable,
    )
  end
end
