# frozen_string_literal: true

class Sponsors::Profile::FeaturedWorkComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    featured_repositories.present?
  end

  def featured_repositories
    @featured_repos ||= @sponsorable.sponsors_listing.featured_repos
  end
end
