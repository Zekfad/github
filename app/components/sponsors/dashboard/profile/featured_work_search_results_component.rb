# frozen_string_literal: true

class Sponsors::Dashboard::Profile::FeaturedWorkSearchResultsComponent < ApplicationComponent

  # Limit how many repos we fetch when searching.
  # We don't want to load too many repos at once.
  REPO_LIMIT = 50

  def initialize(sponsorable:, query: "")
    @sponsorable = sponsorable
    @query = query
  end

  private

  def render?
    @sponsorable
  end

  def repositories
    @repositories ||= current_featured_repos + repos_excluding_featured
  end

  def current_featured_repos
    @current_featured_repos ||= @sponsorable
      .sponsors_listing
      .featured_repos
      .map(&:featureable)
  end

  def repos_excluding_featured
    return @repos_excluding_featured if defined?(@repos_excluding_featured)

    @repos_excluding_featured = @sponsorable.repositories

    if @sponsorable.user?
      contributed_repo_ids = CommitContribution
        .for_user(@sponsorable)
        .where(committed_date: 1.year.ago..Time.zone.now)
        .group(:repository_id)
        .pluck(:repository_id)

      @repos_excluding_featured = @repos_excluding_featured
        .or(Repository.where(id: contributed_repo_ids))
    end

    if @query.present?
      @repos_excluding_featured = @repos_excluding_featured
        .with_prefix("repositories.name", @query.strip)
    end

    @repos_excluding_featured = @repos_excluding_featured
      .public_scope
      .where.not(id: current_featured_repos.map(&:id))
      .order(::Repository.stargazer_count_column => :DESC)
      .limit(REPO_LIMIT)
  end

  def featured_repos_limit
    ::SponsorsListingFeaturedItem::FEATURED_REPOS_LIMIT_PER_LISTING
  end
end
