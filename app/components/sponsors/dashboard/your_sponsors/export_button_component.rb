# frozen_string_literal: true

class Sponsors::Dashboard::YourSponsors::ExportButtonComponent < ApplicationComponent
  include HydroHelper

  def initialize(sponsorable: nil, sponsorships: [])
    @sponsorable = sponsorable
    @sponsorships = sponsorships
  end

  private

  def render?
    @sponsorships.present?
  end

  def earliest_year
    return @earliest_year if defined?(@earliest_year)

    earliest_timestamp = Billing::BillingTransaction::LineItem
      .where(subscribable: @sponsorable.sponsors_listing.sponsors_tiers.to_a)
      .order(created_at: :asc)
      .pick(:created_at)

    @earliest_year = earliest_timestamp&.year || SponsorsListing::SponsorshipsExport::START_YEAR
  end

  def hydro_click_data
    hydro_click_tracking_attributes(
      "sponsors.button_click",
      button: "SPONSORSHIPS_EXPORT",
      sponsored_developer_login: @sponsorable.login,
    )
  end
end
