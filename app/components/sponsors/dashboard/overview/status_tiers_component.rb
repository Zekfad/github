# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusTiersComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def tiers_set?
    @tiers_set ||= @sponsorable.sponsors_listing.sponsors_tiers.with_published_state.any?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def tier_status_icon
    tiers_set? ? "check" : "primitive-dot"
  end

  def tier_status_color
    tiers_set? ? "green" : "yellow-7"
  end
end
