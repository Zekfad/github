# frozen_string_literal: true

class Sponsors::Dashboard::Overview::NextStepsComponent < ApplicationComponent
  include SvgHelper
  include SponsorsButtonsHelper

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  attr_reader :sponsorable

  def render?
    return false unless sponsorable.present?
    listing&.approved?
  end

  def sponsorable_type
    sponsorable.user? ? "user" : "organization"
  end

  def listing
    @listing ||= sponsorable.sponsors_listing
  end

  def funding_file_repo
    return @funding_file_repo if defined? @funding_file_repo
    @funding_file_repo = sponsorable.global_health_files_repository
  end

  def funding_file_enabled?
    return @funding_file_enabled if defined? @funding_file_enabled
    @funding_file_enabled = if repo = funding_file_repo
      repo.repository_funding_links_enabled? && repo.has_funding_file?
    end
  end

  def funding_yml_edit_url
    return @funding_yml_edit_url if defined? @funding_yml_edit_url
    @funding_yml_edit_url = if repo = funding_file_repo
      blob_edit_path(repo.funding_links_path, repo.default_branch, repo)
    end
  end

  def twitter_default_text
    if sponsorable.user?
      return "My GitHub Sponsors profile is live! You can sponsor me to support my open source work 💖"
    end

    name = if sponsorable.profile_twitter_username.present?
      "@#{sponsorable.profile_twitter_username}'s"
    else
      "#{sponsorable.login}'s"
    end

    "#{name} GitHub Sponsors profile is live! You can sponsor us to support #{name} open source work 💖"
  end

  def sponsor_button_code_snippet
    snippet = <<-HTML
      <iframe
        src="#{sponsorable_button_url(sponsorable)}"
        title="Sponsor #{sponsorable}"
        height="35"
        width="107"
        style="border: 0;"></iframe>
    HTML

    snippet.squish
  end

  def sponsor_card_code_snippet
    snippet = <<-HTML
      <iframe
        src="#{sponsorable_card_url(sponsorable)}"
        title="Sponsor #{sponsorable}"
        height="225"
        width="600"
        style="border: 0;"></iframe>
    HTML

    snippet.squish
  end
end
