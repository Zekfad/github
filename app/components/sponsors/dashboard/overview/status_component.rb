# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusComponent < ApplicationComponent
  def initialize(sponsorable:, stripe_request_domain:, docusign_event:)
    @sponsorable = sponsorable
    @stripe_request_domain = stripe_request_domain
    @docusign_event = docusign_event
  end

  private

  def listing
    @listing ||= @sponsorable.sponsors_listing
  end

  def header_class_names
    if listing.approved?
      "f6 text-normal text-green border border-green rounded-1 mr-2 py-0 px-1"
    else
      "f6 text-yellow border border-yellow rounded-1 mr-2 p-1"
    end
  end

  def header_title
    if listing.approved?
      "Active"
    else
      "Pending"
    end
  end

  def header_text
    if listing.approved?
      "Your GitHub Sponsors profile is live at"
    else
      "Your GitHub Sponsors profile isn't live yet. First, we'll need a few things."
    end
  end
end
