# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusStripeConnectComponent < ApplicationComponent
  def initialize(sponsorable:, stripe_request_domain:, billing_enabled: GitHub.billing_enabled?)
    @sponsorable = sponsorable
    @stripe_request_domain = stripe_request_domain
    @billing_enabled = billing_enabled
  end

  private

  def render?
    show_stripe_connect?
  end

  def show_stripe_connect?
    @billing_enabled && @sponsorable.sponsors_stripe_eligible?
  end

  def stripe_connect_url
    @stripe_connect_url ||= Billing::StripeConnect::Account::Oauth.authorization_url(
      request_identifier: Billing::StripeConnect::Account::Oauth.generate_request_state(@sponsorable.global_relay_id),
      domain: @stripe_request_domain,
      sponsorable: @sponsorable,
    )
  end
end
