# frozen_string_literal: true

class Sponsors::Dashboard::Overview::WelcomeComponent < ApplicationComponent
  include AvatarHelper

  SPONSORS_LIMIT = 6

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  delegate :csrf_hidden_input_for, to: :view_context

  def listing
    @listing ||= @sponsorable.sponsors_listing
  end

  def stripe_account?
    return false if listing.blank?

    listing.stripe_connect_account.present?
  end

  def column_size_class
    stripe_account? ? "col-md-4" : "col-md-6"
  end

  def sponsorships_scope
    @sponsorable.sponsorships_as_sponsorable.active.order(updated_at: :desc)
  end

  def sponsors_count
    sponsorships_scope.count
  end

  def sponsorships
    @sponsorships ||= sponsorships_scope.includes(:sponsor).limit(SPONSORS_LIMIT)
  end

  def sponsors_logins
    @sponsors_logins ||= sponsorships.map { |s| s.sponsor.login }.uniq.to_sentence
  end

  def dismiss_url
    dismiss_notice_path("first_sponsor")
  end

  def dismissed_first_sponsor?
    current_user.dismissed_notice?("first_sponsor")
  end

  def dismissed_welcome?
    current_user.dismissed_notice?("welcome_to_sponsors")
  end
end
