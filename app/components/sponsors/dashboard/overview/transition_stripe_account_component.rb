# frozen_string_literal: true

class Sponsors::Dashboard::Overview::TransitionStripeAccountComponent < ApplicationComponent

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  attr_reader :sponsorable

  def render?
    return false if sponsorable.blank?
    return false if listing.blank?

    listing.needs_duplicate_stripe_remediation?
  end

  def listing
    @listing ||= sponsorable.sponsors_listing
  end

  def pending_transition?
    listing.duplicate_stripe_remediation_auth_code.present?
  end

  def stripe_connect_url
    @stripe_connect_url ||= Billing::StripeConnect::Account::Oauth.authorization_url(
      request_identifier: Billing::StripeConnect::Account::Oauth.generate_request_state(sponsorable.global_relay_id),
      domain: "#{GitHub.scheme}://#{GitHub.host_name}",
      sponsorable: sponsorable,
    )
  end
end
