# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusSubmitComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def listing
    @listing ||= @sponsorable.sponsors_listing
  end

  def submit_status_icon
    listing.approved? ? "check" : "primitive-dot"
  end

  def submit_status_color
    listing.approved? ? "green" : "yellow-7"
  end

  def request_approval_disabled?
    !listing.ready_for_submission? ||
    !current_user.two_factor_authentication_enabled? ||
    @sponsorable.sponsors_stripe_pending? ||
    @sponsorable.sponsors_docusign_pending?
  end
end
