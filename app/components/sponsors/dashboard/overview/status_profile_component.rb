# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusProfileComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def bio_set?
    @bio_set ||= @sponsorable.sponsors_listing.full_description.present?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def profile_status_icon
    bio_set? ? "check" : "primitive-dot"
  end

  def profile_status_color
    bio_set? ? "green" : "yellow-7"
  end
end
