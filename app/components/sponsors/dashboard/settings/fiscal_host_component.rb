# frozen_string_literal: true

class Sponsors::Dashboard::Settings::FiscalHostComponent < ApplicationComponent
  CONTACT_US_URL = "https://support.github.com/contact?form%5Bsubject%5D=GitHub+Sponsors:+Fiscal+Host&tags=sponsors"

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    return false if @sponsorable.blank?
    return false unless @sponsorable.organization?
    return false if membership.blank?

    !membership.fiscal_host_none?
  end

  def membership
    @membership ||= @sponsorable.sponsors_membership
  end

  def fiscal_host_name
    if membership.fiscal_host_other?
      membership.fiscal_host_name.titleize
    else
      membership.fiscal_host.titleize
    end
  end
end
