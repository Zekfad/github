# frozen_string_literal: true

class Sponsors::Dashboard::Settings::OrganizationOwnersListComponent < ApplicationComponent
  include AvatarHelper

  MAX_OWNERS_TO_DISPLAY = 10

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    @sponsorable.organization?
  end

  def owners
    @owners ||= @sponsorable.admins
      .order(:login)
      .limit(MAX_OWNERS_TO_DISPLAY)
  end

  def owners_total
    @owners_total ||= @sponsorable.admins.size
  end

  def has_more_owners?
    return @has_more_owners if defined?(@has_more_owners)

    @has_more_owners = owners_total > MAX_OWNERS_TO_DISPLAY
  end

  def organization_owners_link
    org_people_path(@sponsorable, query: "role:owner")
  end
end
