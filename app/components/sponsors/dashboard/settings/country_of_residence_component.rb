# frozen_string_literal: true

class Sponsors::Dashboard::Settings::CountryOfResidenceComponent < ApplicationComponent
  def initialize(sponsorable:, return_to: nil)
    @sponsorable = sponsorable
    @return_to = return_to
  end

  private

  def render?
    return false if @sponsorable.blank?
    return false if membership.blank?
    return true if @sponsorable.user?

    membership.unsupported_fiscal_host?
  end

  def country_of_residence
    @country_of_residence ||= membership.country_of_residence
  end

  def membership
    @membership ||= @sponsorable.sponsors_membership
  end

  def country_label
    if @sponsorable.organization?
      "Country or region where your organization bank account is located"
    else
      "Country or region of residence"
    end
  end
end
