# frozen_string_literal: true

class Sponsors::Dashboard::Settings::StripePayoutsEmailComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
    @listing = sponsorable.sponsors_listing
  end

  private

  def render?
    @listing.stripe_connect_account.present?
  end
end
