# frozen_string_literal: true

class Sponsors::Dashboard::Goals::CompletedListComponent < ApplicationComponent
  include AvatarHelper

  SPONSORS_STACK_LIMIT = 5

  def initialize(sponsorable:, completed_goals:)
    @sponsorable = sponsorable
    @completed_goals = completed_goals
  end

  private

  def render?
    return false if current_user.blank?
    return false if @sponsorable.blank?

    @completed_goals.present?
  end

  def sponsors_to_render_for_goal(goal)
    goal
      .contributions
      .first(SPONSORS_STACK_LIMIT)
      .map(&:sponsor)
  end
end
