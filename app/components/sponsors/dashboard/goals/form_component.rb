# frozen_string_literal: true

class Sponsors::Dashboard::Goals::FormComponent < ApplicationComponent

  def initialize(sponsorable:, goal: nil)
    @sponsorable = sponsorable
    @goal = goal
  end

  private

  def render?
    return false if current_user.blank?

    @sponsorable.present?
  end

  def new_goal?
    !@goal&.persisted?
  end

  def goal_type_label
    return "Goal:" unless @goal

    if @goal.total_sponsors_count?
      "Number of sponsors you're aiming for:"
    else
      "Monthly amount you're aiming for:"
    end
  end
end
