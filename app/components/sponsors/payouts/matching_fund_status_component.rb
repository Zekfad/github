# frozen_string_literal: true

class Sponsors::Payouts::MatchingFundStatusComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    listing.present?
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def listing
    @listing ||= @sponsorable&.sponsors_listing
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def match_limit
    Billing::Money.new(Sponsors::MATCHING_LIMIT_AMOUNT_IN_CENTS).format
  end

  def matched
    # If we've hit the match limit and overpaid them: just lie.
    return match_limit if listing.reached_match_limit?

    Billing::Money.new(listing.total_match_in_cents).format
  end

  def help_link
    "#{GitHub.help_url}/github/supporting-the-open-source-community-with-github-sponsors/about-github-sponsors#about-the-github-sponsors-matching-fund"
  end
end
