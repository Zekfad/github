# frozen_string_literal: true

# As we migrate to the stricter ViewComponent::Base, this class exists to
# allow us to have components that inherit from the old and new base classes
class ApplicationComponent < ViewComponent::Base
  include ApplicationHelper, GitHub::FetchOrFallbackHelper

  delegate :csrf_hidden_input_for, :authenticity_token_for, to: :helpers

  def self.inherited(child)
    child.include UrlHelper unless child < UrlHelper
    child.include GitHub::RouteHelpers unless child < GitHub::RouteHelpers

    super
  end

  def current_user
    helpers.current_user
  end

  def logged_in?
    helpers.logged_in?
  end
end
