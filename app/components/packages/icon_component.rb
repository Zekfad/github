# frozen_string_literal: true

module Packages
  class IconComponent < ApplicationComponent
    include SvgHelper

    SUPPORTED_ICONS = %w(docker maven npm nuget rubygems).freeze

    def initialize(type: nil, size: 20, **args)
      @type, @size, @args = type, size, args
    end

    private

    def asset_path
      if SUPPORTED_ICONS.include?(@type&.downcase)
        "icons/packages/#{@type.downcase}.svg"
      end
    end
  end
end
