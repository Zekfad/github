# frozen_string_literal: true

module Files
  class SidebarListComponent < ApplicationComponent

    def initialize(title:, href:, count: 0, max_count: 3, **args)
      @title, @href, @count, @max_count, @args = title, href, count, max_count, args
    end

    def items_to_show
      if @count < @max_count
        @count
      elsif @count == @max_count + 1
        @max_count + 1
      else
        @max_count
      end
    end

    private

    def more_items?
      @count > @max_count + 1
    end
  end
end
