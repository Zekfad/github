# frozen_string_literal: true

module Repos
  module Alerts
    class SeverityLabelComponent < ApplicationComponent
      SEVERITY_SCHEME = {
        "critical" => :red,
        "high" =>  :orange,
        "moderate" => :yellow,
        "low"  => :dark_gray
      }

      def initialize(severity:, verbose: true, **kwargs)
        @severity = severity&.downcase
        @verbose = verbose
        @kwargs = kwargs
      end

      def render?
        severity.present?
      end

      private

      attr_reader :severity

      def scheme
        SEVERITY_SCHEME[severity]
      end

      def verbose?
        @verbose
      end

      def text
        "#{severity}" + (verbose? ? " severity" : "")
      end
    end
  end
end
