# frozen_string_literal: true

class Codespaces::LoadingComponent < ApplicationComponent

  attr_reader :codespace

  def initialize(codespace:)
    @codespace = codespace
  end

  # This value is used to initialize a view loading state. For the iframed flow, this value is updated as loading
  # progresses in codespaces.ts to display loading progress to the user.
  # We will likely revisit or remove this when we remove the iframed flow.
  def loading_state
    codespace.stuck_provisioning? ? "stuck" : codespace.state
  end

end
