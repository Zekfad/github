# frozen_string_literal: true

class Codespaces::WorkbenchFormComponent < ApplicationComponent
  attr_reader :codespace,
              :form_action_source,
              :github_token,
              :service_url,
              :user

  HOME_INDICATOR = {
    icon: "github-inverted",
    href: "https://github.com/codespaces",
    title: "Go Home",
  }

  DEFAULT_WORKBENCH_SETTINGS = {
    "workbench.colorTheme": "GitHub Light",
    "workbench.startupEditor": "welcomePageInEmptyWorkbench"
  }

  DEFAULT_EXTENSIONS = %w[GitHub.vscode-pull-request-github github.github-vscode-theme ms-vsliveshare.vsliveshare]

  CODESPACES_FEATURE_FLAGS = %w[codespaces_serverless_enabled codespaces_port_forwarding_service_enabled]

  def initialize(codespace:, github_token:, user:)
    @codespace, @github_token, @user = codespace, github_token, user

    # The url that the form will POST to.
    @service_url = URI::HTTPS.build(
      host: "#{codespace.name}.#{auth_host_domain}",
      path: "/",
    ).to_s
  end

  def partner_info
    # This payload is documented at the url found in `GitHub::Config::VSCS_PLATFORM_AUTH_SCHEMA_URL`
    JSON.generate({
      partnerName: "github",
      managementPortalUrl: auth_redirect_codespaces_url,
      codespaceId: codespace.guid,
      # Cascade token used to authenticate on the VSCS side. `CASCADE_TOKEN_PLACEHOLDER` is replaced with the token in a typescript callback.
      codespaceToken: "%CASCADE_TOKEN_PLACEHOLDER%",
      credentials: [{
        token: github_token,
        host: GitHub.host_name,
        path: "/"
      },
      {
        token: github_token,
        host: GitHub.host_name
      }],
      favicon: {
        stable: "https://github.com/favicons/favicon-codespaces.svg",
        insider: "https://github.com/favicons/favicon-codespacesinsider.svg"
      },
      featureFlags: vscs_feauture_flags,
      vscodeSettings: default_vscode_settings,
    })
  end

  def default_vscode_settings
    {
      vscodeChannel: "insider",
      enableSyncByDefault: true,
      # Used to display the home icon in the workbench.
      homeIndicator: HOME_INDICATOR,
      # Settings used when browser storage data is not set and there is no sync data.
      defaultSettings: DEFAULT_WORKBENCH_SETTINGS,
      defaultExtensions: default_extensions,
      defaultAuthSessions: [
        {
          type: "github",
          id: "github-session-github-pr",
          accessToken: github_token,
          scopes: ["read:user", "user:email", "repo"].sort,
        },
        {
          type: "github",
          id: "github-session-sync-service",
          accessToken: github_token,
          scopes: ["email"],
        },
        {
          type: "github",
          id: "github-session-vs-codespaces",
          accessToken: github_token,
          scopes: ["read:user", "user:email", "repo", "write:discussion"].sort
        }
      ],
      authenticationSessionId: "github-session-sync-service"
    }
  end

  def default_extensions
    DEFAULT_EXTENSIONS.map { |e| { "id": e } }
  end

  def vscs_feauture_flags
    Hash[CODESPACES_FEATURE_FLAGS.collect { |flag| [flag.remove("codespaces_").camelize(:lower), GitHub.flipper[flag].enabled?(@user)] }]
  end

  private

  def auth_host_domain
    codespace.vscs_target_config[:platform_auth_host]
  end
end
