# frozen_string_literal: true

class Codespaces::DeleteFormComponent < ApplicationComponent
  include CodespacesHelper

  with_content_areas :delete_button

  attr_reader :codespace, :repository_id, :ref, :pull_request_id, :delete_class, :codespaces_context, :pr_dropdown, :wide

  def initialize(
    codespace:,
    repository_id: nil,
    ref: nil,
    pull_request_id: nil,
    delete_class: "btn btn-danger btn-sm",
    codespaces_context: nil,
    pr_dropdown: false,
    wide: false
  )
    @codespace, @repository_id, @pull_request_id, @delete_class, @codespaces_context, @pr_dropdown, @wide =
      codespace, repository_id, pull_request_id, delete_class, codespaces_context, pr_dropdown, wide

    @ref = Git::Ref.value_for_display(ref) if ref
  end

  def hydro_attributes
    @hydro_attributes ||= destroy_codespace_attributes(codespace: codespace)
  end
end
