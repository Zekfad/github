# frozen_string_literal: true

class Codespaces::UnauthorizedSamlTargetsComponent < ApplicationComponent
  include SsoHelper

  attr_reader :unauthorized_saml_targets

  def initialize(unauthorized_saml_targets:)
    @unauthorized_saml_targets = Array(unauthorized_saml_targets).reject do |target|
      !GitHub.flipper[:workspaces].enabled?(target)
    end
  end
end
