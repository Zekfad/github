# frozen_string_literal: true

class Codespaces::CreateButtonComponent < ApplicationComponent
  FIREFOX_TEXT = "Codespace support for Firefox is coming soon! For the best experience we recommend using Chrome."
  LIMIT_TEXT = "Beta limit of #{GitHub.codespaces_per_user_limit} codespaces reached. Remove an existing codespace to create a new one."

  attr_reader :codespace, :vscs_target, :text, :disabled_button_text, :btn_class, :button_icon, :icon_class, :data, :form_class

  delegate :should_disable_firefox?, to: :helpers

  include ClassNameHelper

  # codespace            - The codespace to be created. Should be built with a `repository` and either a `ref` or
  #                        `pull_request_id`.
  # prompt_at_limit      - Whether we should prompt the user to manage/delete their codespaces when limited (optional, default: true)
  # text                 - a button text String. (optional, default: "Open in codespace")
  # disabled_button_text - a button text String. (optional, defualt: "Opening in codespace")
  # btn_class            - a CSS class String to use for the button element. (optional, default: "btn")
  # button_icon          - an octicon name that will be inserted before `text`. (optional, default: nil)
  # data                 - a Hash for collecting keys/values that will be rendered as data attributes. (optional, default: {})
  # form_class           - a CSS class that will be applied to the <form> element. (optional, default: "")
  # tooltip_class        - a CSS class that will be applied to the <button> element while it's disabled. (optional, default: "tooltipped tooltipped-w")
  def initialize(
    codespace:,
    vscs_target: nil,
    prompt_at_limit: true,
    text: "Open in codespace",
    disabled_button_text: "Opening in codespace",
    btn_class: "",
    button_icon: nil,
    icon_class: "",
    data: {},
    form_class: "",
    tooltip_class: "")

    @codespace, @vscs_target, @prompt_at_limit, @text, @disabled_button_text, @btn_class, @button_icon, @icon_class, @data, @form_class, @tooltip_class =
      codespace, vscs_target, prompt_at_limit, text, disabled_button_text, btn_class, button_icon, icon_class, data, form_class, tooltip_class
  end

  def before_render
    return unless @codespace.present?
    SecureHeaders.append_content_security_policy_directives(
      request,
      connect_src: [prefetch_location]
    )
  end

  def accessible_codespaces_url
    codespaces_path(
      context: Codespaces::Query::ACCESSIBLE_CODESPACES,
      prompt_at_limit: false,
      event_target: @event_target,
      repo: @codespace.repository&.id,
      codespace: { ref: @codespace.ref, pull_request_id: @codespace.pull_request_id }
    )
  end

  def prefetch_location
    if vscs_target
      Codespaces.vscs_locations_url_for_target(vscs_target)
    else
      Codespaces::Azure.vso_locations_url(current_user)
    end
  end

  def disabled?
    should_disable_firefox? || (!@prompt_at_limit && current_user.at_codespace_limit?)
  end

  def prompt?
    @prompt_at_limit && current_user.at_codespace_limit?
  end

  def disabled_text
    if should_disable_firefox?
      FIREFOX_TEXT
    else
      LIMIT_TEXT
    end
  end

  def tooltip_class
    return @tooltip_class if @tooltip_class.present?
    if should_disable_firefox?
      "tooltipped tooltipped-w"
    end
  end
end
