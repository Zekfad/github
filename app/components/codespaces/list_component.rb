# frozen_string_literal: true

class Codespaces::ListComponent < ApplicationComponent

  include ClassNameHelper
  include CodespacesHelper

  attr_reader :query, :event_target, :pr_dropdown, :prompt_at_limit, :unauthorized_saml_targets, :dividers_and_items

  delegate :build_codespace, :repository, :ref, :pull_request, :codespaces_context, to: :query

  # repository                - The repository which the codespaces use
  # unpushed_id               - ID of a codespace that has unpushed changes and should display a warning box prompting for delete confirmation.
  # prompt_at_limit           - If the user is at their codespace limit, prompt them to delete?. @see CreateButtonComponent
  # ref                       - The ref that will be used to create a new codespace for the current_user, (optional, default: nil)
  # pull_request              - The PR that will be used to create a new codespace for the current_user, (optional: default: nil)
  # event_target              - The event target that should be passed to hydro (optional, default: "")
  # codespaces_context        - A string, currently used to optionally query _all_ a current_user's codespaces, not ones for the repo and ref/PR (optional, default: nil)
  # pr_dropdown               - A boolean, used to render this component differently if in the context of a PR dropdown (optional, default: false)
  # unauthorized_saml_targets - The Array of Organizations that do not have active SAML sessions (optional, default: nil)
  def initialize(
    repository:,
    unpushed_id: nil,
    prompt_at_limit: true,
    ref: nil,
    pull_request: nil,
    event_target: "",
    codespaces_context: nil,
    pr_dropdown: false,
    unauthorized_saml_targets: nil
  )
    raise ArgumentError, "`unauthorized_saml_targets` is required for displaying all accessible codespaces" if codespaces_context == Codespaces::Query::ACCESSIBLE_CODESPACES && unauthorized_saml_targets.nil?

    @repository, @unpushed_id, @prompt_at_limit, @ref, @pull_request, @event_target, @codespaces_context, @pr_dropdown, @unauthorized_saml_targets =
      repository, unpushed_id, prompt_at_limit, ref, pull_request, event_target, codespaces_context, pr_dropdown, unauthorized_saml_targets
  end

  def before_render
    @query = Codespaces::Query.new(
      current_user: current_user,
      repository: @repository,
      ref: @ref,
      pull_request: @pull_request,
      codespaces_context: @codespaces_context,
      unauthorized_saml_targets: unauthorized_saml_targets
    )

    codespaces = @query.codespaces

    @dividers_and_items = if @codespaces_context == Codespaces::Query::ACCESSIBLE_CODESPACES
      codespaces
    else
      codespaces_by_repo_id = {}
      codespaces.each do |codespace|
        repo_id = codespace.repository_id
        (codespaces_by_repo_id[repo_id] ||= []).push(codespace)
      end

      items = []
      codespaces_by_repo_id.each do |repo_id, codespaces|
        if repo_id == @repository.id
          items.insert(0, codespaces)
        else
          items.push({divider: true, nwo: codespaces[0].repository.nwo})
          items.push(codespaces)
        end
      end
      items.flatten
    end
  end

  def show_unauthorized_saml_targets_warning?
    codespaces_context == Codespaces::Query::ACCESSIBLE_CODESPACES && unauthorized_saml_targets.present?
  end

  def show_unpushed_warning?(codespace)
    codespace.id == @unpushed_id&.to_i
  end
end
