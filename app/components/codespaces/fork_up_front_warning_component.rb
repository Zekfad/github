# frozen_string_literal: true

class Codespaces::ForkUpFrontWarningComponent < ApplicationComponent

  # user            - The user or the owner opening/creating codespace(s)
  # repository      - The repository which the codespaces use
  def initialize(user:, repository:, ref: nil, **kwargs)
    @user, @repository, @ref, @kwargs = user, repository, ref, kwargs
  end

  def render?
    return false unless GitHub.flipper[:codespaces_fork_repos].enabled?(@user)
    @repository && Codespaces::Policy.should_fork_up_front?(@repository, @user, ref: @ref)
  end
end
