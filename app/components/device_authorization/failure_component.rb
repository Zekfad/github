# frozen_string_literal: true

module DeviceAuthorization
  class FailureComponent < ApplicationComponent
    APPLICATION_SUSPENDED_DESCRIPTON = \
      GitHub::HTMLSafeString.make("Check out our <a href='#{DeviceAuthorizationRequest::APPLICATION_SUSPENDED}'>developer docs</a> to find out more.")

    def initialize(reason: nil)
      @reason = reason&.to_sym
    end

    def description
      case @reason
      when :application_suspended
        APPLICATION_SUSPENDED_DESCRIPTON
      when :expired
        "Please go back to your device to request another code."
      when :not_found
        "Please make sure you entered the user code correctly."
      else
        "Please try again."
      end
    end

    def octicon_name
      case @reason
      when :expired
        "clock"
      when :not_found
        "question"
      else
        "x"
      end
    end

    def text_color
      case @reason
      when :expired, :not_found
        "gray"
      else
        "red"
      end
    end

    def title
      case @reason
      when :application_suspended
        "Application is suspended"
      when :expired
        "Expired user code"
      when :not_found
        "Uh oh, we couldn't find anything"
      else
        "Uh oh, something went wrong"
      end
    end
  end
end
