# frozen_string_literal: true

module Repositories
  class DeploymentStatusComponent < ApplicationComponent
    STATE_DEFAULT = :default
    STATE_MAPPINGS = {
      STATE_DEFAULT => "",
      :abandoned => "",
      :inactive => "",
      :destroyed => "",
      :active => "green",
      :error => "red",
      :failure => "red",
      :in_progress => "yellow",
      :pending => "yellow",
      :queued => "yellow",
    }.freeze

    def initialize(state: STATE_DEFAULT, **args)
      @state, @args = state, args

      @args[:color] = STATE_MAPPINGS[fetch_or_fallback(STATE_MAPPINGS.keys, state.to_s.downcase.to_sym, STATE_DEFAULT)]
    end

    private

    def render?
      !@state.nil?
    end
  end
end
