# frozen_string_literal: true

module Repositories
  module Settings
    class SetVisibilityComponent < ApplicationComponent
      # Needed for #case_insensitive_pattern
      include SettingsHelper

      # Needed for #feature_gate_upsell_click_attrs. TODO: Remove when
      # feature_gate_upsell.click tracking is removed.
      include FeatureGateHelper

      def initialize(repository:)
        @repository = repository
      end

      def disable_button?
        repository.fork? || public_repo_with_trade_restrictions?
      end

      def can_change_visibility?
        members_can_change_visibility? && owner_can_privatize?
      end

      def button_copy
        "Change visibility"
      end

      def description
        if repository.fork?
          fork_description
        elsif public_repo_with_trade_restrictions?
          TradeControls::Notices.generic_prevent_toggle_to_private(type: "repository")
        elsif !members_can_change_visibility?
          "Organization members can’t change repo visibility."
        elsif owner_can_privatize?
          default_description
        elsif !plan_owner.organization?
          upgrade_user_plan_description
        elsif !plan_owner.adminable_by?(current_user) && repository.adminable_by?(current_user)
          ask_owner_to_upgrade_description
        elsif !plan_owner.plan.per_seat?
          upgrade_org_plan_description
        elsif !repository.owner_has_seats_for_collaborators?
          add_seats_description
        elsif !repository.owner_has_seats_for_collaborators?(pending_cycle: true)
          cancel_downgrade_description
        else
          default_description
        end
      end

      def available_visibilities
        if !owner_can_privatize?
          [
            current_visibility, # Could be internal or private
            Repository::PUBLIC_VISIBILITY,
          ]
        elsif repository.owner&.business
          [
            Repository::PUBLIC_VISIBILITY,
            Repository::PRIVATE_VISIBILITY,
            Repository::INTERNAL_VISIBILITY,
          ]
        else
          [
            Repository::PUBLIC_VISIBILITY,
            Repository::PRIVATE_VISIBILITY,
          ]
        end
      end

      def current_visibility
        @current_visibility ||= repository.visibility
      end

      def plan_owner
        @plan_owner ||= repository.plan_owner
      end

      private

      attr_reader :repository

      def public_repo_with_trade_restrictions?
        return @public_repo_with_trade_restrictions if defined?(@public_repo_with_trade_restrictions)
        @public_repo_with_trade_restrictions = repository.public? && repository.has_any_trade_restrictions?
      end

      def members_can_change_visibility?
        return @members_can_change_visibility if defined?(@members_can_change_visibility)
        @members_can_change_visibility = repository.can_change_repo_visibility?(current_user)
      end

      def owner_can_privatize?
        return @owner_can_privatize if defined?(@owner_can_privatize)
        @owner_can_privatize = repository.can_privatize?
      end

      def default_description
        "This repository is currently #{current_visibility}."
      end

      def fork_description
        safe_join([
          "You cannot change the visibility of a fork. Please ",
          link_to("duplicate the repository", "#{GitHub.help_url}/articles/duplicating-a-repository"),
          "."
        ])
      end

      def upgrade_user_plan_description
        safe_join([
          "Please ",
          link_to("upgrade your plan", settings_user_billing_path, data: feature_gate_upsell_click_attrs),
          " to change this repository's visibility."
        ])
      end

      def ask_owner_to_upgrade_description
        upgrade_text = plan_owner.plan.per_seat? ? "add seats to" : "upgrade"
        "Please ask one of the owners to #{upgrade_text} #{plan_owner} if you want to change this repository's visibility."
      end

      def upgrade_org_plan_description
        safe_join([
          "Please ",
          link_to("upgrade #{plan_owner}", settings_org_billing_path(plan_owner), data: feature_gate_upsell_click_attrs),
          "."
        ])
      end

      def add_seats_description
        seats_needed = plan_owner.seats_needed_for_collaborators_on(repository)
        link_url = org_seats_path(plan_owner, seats: seats_needed, return_to: edit_repository_path(repository))
        link_copy = "add #{seats_needed} #{"seat".pluralize(seats_needed)} to #{plan_owner}"

        safe_join([
          "Please ",
          link_to(link_copy, link_url, data: feature_gate_upsell_click_attrs("collaborators")),
          " to change this repository's visibility.",
        ])
      end

      def cancel_downgrade_description
        confirm = "Are you sure you want to cancel these pending plan changes?"

        form_tag(update_pending_plan_change_path(plan_owner), method: :put) do
          hidden_field_tag(:cancel_seats, true)
          safe_join([
            "Please ",
            button_tag("cancel the pending seat downgrade", class: "btn-link", data: {confirm: confirm}),
            " to ensure there are seats for collaborators to change this repository's visibility."
          ])
        end
      end
    end
  end
end
