# frozen_string_literal: true

module Repositories
  module Settings
    class VisibilityOptionComponent < ApplicationComponent

      # Needed for #feature_gate_upsell_click_attrs. TODO: Remove when
      # feature_gate_upsell.click tracking is removed.
      include FeatureGateHelper

      # Needed for #trade_controls_restricted_public_abilities_for_repository
      include TradeControlsHelper

      attr_reader :new_visibility, :owner_name

      def initialize(new_visibility:, current_visibility:, owner_name:, repository:)
        @new_visibility = new_visibility
        @current_visibility = current_visibility
        @owner_name = owner_name
        @repository = repository
      end

      def label_id
        "help-text-for-#{new_visibility}-checkbox"
      end

      def label_help_text
        if same_visibility?
          return "This repository is currently #{current_visibility}."
        end

        case new_visibility
        when Repository::PUBLIC_VISIBILITY
          "Make this repository visible to anyone."
        when Repository::PRIVATE_VISIBILITY
          "Hide this repository from the public."
        when Repository::INTERNAL_VISIBILITY
          "Make this repository visible only to the people in #{owner_name}."
        end
      end

      def same_visibility?
        new_visibility == current_visibility
      end

      def show_warnings?
        !same_visibility?
      end

      # Changing from private->internal
      def changing_from_private_to_internal?
        currently_private? && new_visibility == Repository::INTERNAL_VISIBILITY
      end

      # Changing from internal->private
      def changing_from_internal_to_private?
        currently_internal? && new_visibility == Repository::PRIVATE_VISIBILITY
      end

      # Changing from private/internal->public
      def changing_to_public?
        return false if same_visibility?
        new_visibility == Repository::PUBLIC_VISIBILITY
      end

      # Changing from public->private/internal
      #
      # Note: changing from public->private/internal results in the loss of
      # many features, including published GitHub Pages and Actions, anonymous
      # git access, and any features exclusive to the GitHub Free plan.
      def changing_from_public?
        return @changing_from_public if defined?(@changing_from_public)
        @changing_from_public = currently_public? && new_visibility != Repository::PUBLIC_VISIBILITY
      end

      # Possible when changing from public->private/internal only
      def will_lose_pages?
        return false unless changing_from_public?
        !repository.plan_supports?(:pages, visibility: new_visibility)
      end

      # Possible when changing from public->private/internal only
      def will_lose_actions?
        return false unless changing_from_public?
        GitHub.actions_enabled? && RepositoryAction.exists?(repository: repository)
      end

      # Possible when changing from public->private/internal only
      def will_lose_anonymous_git_access?
        return false unless changing_from_public?
        repository.anonymous_git_access_enabled?
      end

      # Possible when changing from public->private/internal only
      def can_upgrade_plan?
        return false unless changing_from_public?
        GitHub.billing_enabled? && current_user.free_plan?
      end

      # Possible when changing to/from any visibility
      def will_detach_from_network?
        return @will_detach_from_network if defined?(@will_detach_from_network)
        @will_detach_from_network = repository.network_root? && !repository.sole_repo_in_network?
      end

      # Possible when changing from private/internal->public only
      def will_have_restricted_public_abilities?
        return false unless changing_to_public?
        repository.has_any_trade_restrictions?
      end

      # Possible when changing from private/internal->public only
      def profile_readme_will_become_visible?
        return false unless changing_to_public?
        return false unless repository.user_configuration_repository?
        return false unless current_user.eligible_to_display_profile_readme?

        repository.has_readme?
      end

      # Possible when changing from public->private/internal only
      def profile_readme_will_become_hidden?
        return false unless changing_from_public?
        current_user.profile_readme_visible?
      end

      private

      attr_reader :current_visibility, :repository

      def currently_public?
        current_visibility == Repository::PUBLIC_VISIBILITY
      end

      def currently_private?
        current_visibility == Repository::PRIVATE_VISIBILITY
      end

      def currently_internal?
        current_visibility == Repository::INTERNAL_VISIBILITY
      end
    end
  end
end
