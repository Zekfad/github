# frozen_string_literal: true

module Repositories
  class WatchComponent < ApplicationComponent
    include AnalyticsHelper
    include ClassNameHelper
    include HydroHelper
    include RepositoryAnalyticsHelper
    include UsersHelper

    SHOW_COUNT_DEFAULT = false
    SHOW_COUNT_OPTIONS = [SHOW_COUNT_DEFAULT, true]

    BUTTON_BLOCK_DEFAULT = false
    BUTTON_BLOCK_OPTIONS = [BUTTON_BLOCK_DEFAULT, true]

    def initialize(
      repository:,
      status: nil,
      show_count: SHOW_COUNT_DEFAULT,
      button_block: BUTTON_BLOCK_DEFAULT
    )
      @repository = repository
      @status = status
      @button_block = fetch_or_fallback(BUTTON_BLOCK_OPTIONS, button_block, BUTTON_BLOCK_DEFAULT)

      if @button_block
        @show_count = false
      else
        @show_count = fetch_or_fallback(SHOW_COUNT_OPTIONS, show_count, SHOW_COUNT_DEFAULT)
      end
    end

    private

    def button_class_names
      class_names(
        "btn btn-sm",
        "btn-with-count": @show_count,
        "btn-block": @button_block
      )
    end

    def render?
      @repository.present?
    end

    def subscription_type
      if !@status&.success?
        nil
      elsif @status.subscribed?
        :watching
      elsif @status.thread_type_only?(Release)
        :releases_only
      elsif @status.participation_only?
        :none
      elsif @status.ignored?
        :ignoring
      end
    end
  end
end
