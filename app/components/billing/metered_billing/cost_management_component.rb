# frozen_string_literal: true

module Billing
  module MeteredBilling
    class CostManagementComponent < ApplicationComponent
      attr_reader :product, :title, :enforce_spending_limit, :spending_limit_in_subunits, :form_action

      def initialize(
        product:,
        title:,
        form_action: "",
        enforce_spending_limit: true,
        spending_limit_in_subunits: 0,
        disabled: false
      )
        @product = product
        @title = title
        @disabled = disabled
        @form_action = form_action
        @enforce_spending_limit = enforce_spending_limit
        @spending_limit_in_subunits = spending_limit_in_subunits
      end

      def spending_limit_value
        number_with_precision(spending_limit_in_subunits / 100.0, precision: 2)
      end

      def disabled?
        @disabled
      end

      def disabled_attribute
        disabled? ? "disabled" : ""
      end
    end
  end
end
