# frozen_string_literal: true

module UploadHelper
  include TagAttributeHelper

  class InvalidPolicyError < StandardError; end

  # Helper to get an array of all of the extensions that users are allowed to upload for
  # a given model that an upload is for.
  #
  # model_name - The String or Symbol model that the upload is for (Eg. `:avatars`).
  #
  # Returns an array of file extensions (eg: `[".jpg", ".gif"]`)
  def upload_policy_allowed_extensions(model_names)
    extensions = Array(model_names).map do |model_name|
      policy = ::Storage.policy_creator.for(model_name)

      unless policy
        raise InvalidPolicyError, "Storage policy for #{model_name} not found"
      end

      policy.model.user_allowed_content_extensions
    end

    extensions.flatten.uniq
  end

  # Helper to make an `accept` attribute for input[type="file"], which prevents users from
  # selecting file types that will be rejected by the upload policy.
  #
  # model_name - The String or Symbol model that the upload is for (Eg. `:avatars`).
  #
  # Returns an HTML safe String for the `accept` attribute
  def upload_policy_accept_extensions_attribute(model_names)
    attrs = {
      "accept" => upload_policy_allowed_extensions(model_names).join(","),
    }

    tag_attributes(attrs)
  end

  # Helper to make a human-readable list of allowed extensions, used to populate error messages
  # on the form if an unacceptable file is uploaded.
  #
  # model_name - The String or Symbol model that the upload is for (Eg. `:avatars`).
  #
  # Returns a String of uppercase file extensions (minus the "." stop character)
  # and separated by a comma and a space (eg: "JPG, PNG")
  def upload_policy_friendly_extensions(model_names)
    extensions = upload_policy_allowed_extensions(model_names)

    # remove stop characters, convert to upper case
    extensions = extensions.map { |ext| ext.tr(".", "").upcase }

    "#{extensions.to_sentence(last_word_connector: ' or ')}."
  end

  # Renders a <file-attachment> custom element if file uploads are enabled,
  # otherwise just renders the block content.
  #
  # Examples
  #
  #   <%= file_attachment_tag(model: :assets, class: "is-default") do |enabled| %>
  #     <div class="<%= "upload-enabled" if enabled %>"></div>
  #   <% end %>
  #
  # Returns an HTML string.
  def file_attachment_tag(**options)
    model = options.delete(:model)

    path = upload_policy_path(model)
    options = options.merge(
      "data-upload-policy-url" => path,
    )

    enabled = options.key?(:enabled) ? options.delete(:enabled) : true
    enabled = enabled && attachments_enabled?

    if enabled
      content_tag("file-attachment", options) do
        concat(csrf_hidden_input_for(path, class: "js-data-upload-policy-url-csrf"))
        yield enabled
      end
    else
      capture do
        yield enabled
      end
    end
  end
end
