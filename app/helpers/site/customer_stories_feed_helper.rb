# frozen_string_literal: true

module Site::CustomerStoriesFeedHelper
  ORDERED_INDUSTRIES = [
    "All",
    "Software & Technology",
    "Retail & eCommerce",
    "Travel & Hospitality",
    "Media & Entertainment",
    "Manufacturing",
    "Operations & Logistics",
    "Automotive",
    "Financial Services",
    "Healthcare",
    "Marketing & Communications",
    "Communications",
    "Energy & Utilities",
    "Non-Profit",
    "Government",
    "Education",
  ]

  ORDERED_REGIONS = [
    "All",
    "Americas",
    "APAC",
    "EMEA",
  ]

  ORDERED_TYPES = %w(
    All
    Developers
    Enterprise
    Team
  )

  LIST_TYPE_MAPPING = {
    industry: ORDERED_INDUSTRIES,
    region: ORDERED_REGIONS,
    type: ORDERED_TYPES,
  }

  def filter_name_for(filter, filter_param)
    filter_list_for(filter).find do |entry|
      entry.parameterize == filter_param
    end
  end

  def filter_list_for(filter, existing_filters = {})
    filter_list = LIST_TYPE_MAPPING.fetch(filter, []).select do |filter_entry|
      filter != :industry || story_exists_for_industry?(filter_entry, existing_filters)
    end

    filter_list.compact
  end

  def story_exists_for_industry?(filter_entry, existing_filters)
    return true if filter_entry == "All"

    ExploreFeed::CustomerStory
      .all
      .filter_by_region(existing_filters[:region])
      .filter_by_type(existing_filters[:type])
      .any? do |story|
      story.industry_filters && story.industry_filters.include?(filter_entry.parameterize)
    end
  end

  def list_entry_link_for(filter, list_entry, existing_params = {})
    Rails.application.routes.url_helpers.customer_stories_path(
      existing_params.merge(
        filter => list_entry.parameterize,
        :anchor => "results",
      ),
    )
  end

  def link_class_for(list_entry, selected_list_entry)
    [
      "d-block",
      "position-relative",
      "no-underline",
      list_entry_selected?(list_entry, selected_list_entry) ?
        "link-gray-dark text-bold" : "link-gray",
      "border-bottom-0",
      "pr-3",
      "pl-4",
      "my-3",
    ].join(" ")
  end

  def octicon_class_for(list_entry, selected_list_entry)
    [
      show_check_mark?(list_entry, selected_list_entry) ?
        "d-inline-block" : "d-none",
      "text-gray-dark",
      "position-absolute",
      "left-0",
      "mt-1",
    ].join(" ")
  end

  private

  def show_check_mark?(list_entry, selected_list_entry)
    (selected_list_entry.nil? && list_entry.in?(["Featured", "all"])) ||
      list_entry_selected?(list_entry, selected_list_entry)
  end

  def list_entry_selected?(list_entry, selected_list_entry)
    list_entry.downcase == selected_list_entry
  end
end
