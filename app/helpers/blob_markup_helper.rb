# frozen_string_literal: true

require "cache_key_logging_blacklist"

# Helpers for rendering blob contents with the HTML pipeline
module BlobMarkupHelper
  include Scientist
  include OcticonsCachingHelper

  # Renders the README for the current repository.
  #
  # Returns an HTML String. If the README can't be rendered, returns the blob
  # content as plaintext.
  def format_readme(readme, relative_paths: false, entity: nil, committish: nil, classes: "")
    if GitHub::HTML::MarkupFilter.can_render_blob?(readme) || readme.viewable?
      context = {}
      context[:path] = relative_paths ? nil : File.dirname(readme.path)
      context[:entity] = entity unless entity.nil?
      context[:committish] = committish unless committish.nil?

      result = markup_blob(readme, context)
      output = result[:output]
      output = output.html_safe if result[:html_safe] # rubocop:disable Rails/OutputSafety

      if result[:rendered]
        formatted_blob_content output, classes
      else
        plaintext_blob_content output
      end
    end
  rescue => e
    failbot(e)
    plaintext_blob(readme)
  end

  # Renders a blob through the HTML Pipeline
  #
  # blob - An instance of Walker::CachedContent
  #
  # Returns the rendered HTML if the blob is formattable, otherwise nil
  def format_blob(blob, context = {})
    return nil unless GitHub::HTML::MarkupFilter.can_render_blob?(blob)
    result = markup_blob(blob, context)
    return nil unless result[:rendered]
    output = result[:output]
    output = output.html_safe if result[:html_safe] # rubocop:disable Rails/OutputSafety
    formatted_blob_content output
  end

  # Internal: Given a blob (presumably a plaintext README), formats it to be displayed
  # with <pre> tags and linking any obvious URLs.
  def plaintext_blob(blob)
    plaintext_blob_content markup_blob_content(blob, plaintext: true)
  end

  # Public: Wrap the HTML blob content in an <article> with appropriate classes.
  #
  # Produces the following markup:
  #   <article class="markdown-body entry-content container-lg">
  #     CONTENT
  #   </article>
  def formatted_blob_content(html, classes = "")
    base_classes = "markdown-body entry-content container-lg"
    base_classes += " #{classes}" unless classes.blank?
    content_tag(:article, html, class: base_classes, itemprop: "text")
  end

  # Internal: Wrap the plaintext blob content in a <pre> tag
  #
  # Produces the following markup:
  #   <div class="plain"><pre>CONTENT</pre></div>
  def plaintext_blob_content(html)
    content_tag(:div, content_tag(:pre, html, style: "white-space: pre-wrap"), class: "plain")
  end

  # Internal: Process a blob through the HTML pipeline
  #
  # blob    - The TreeEntry to process.
  # context - Additional options to pass to the HTML pipeline.
  #
  # Returns a hash from the HTML pipeline
  def markup_blob(blob, context = {})
    should_cache = true
    if context[:data]
      blob = blob.dup.tap { |b| b.data = context[:data] }
      context.delete(:data)
      # This isn't a real blob with an OID. Presumably it's temporary (e.g.,
      # for rendering prose diff previews). Don't bother caching it.
      should_cache = false
    end

    context = blob_html_context(blob).update(context)

    return markup_blob!(blob, context) unless should_cache

    key = blob_markup_cache_key(blob, context)
    cached = GitHub.cache.get(key)

    unless cached.nil?
      GitHub.dogstats.increment("cache.hit", tags: ["action:markup_blob"])
      return cached
    end

    GitHub.dogstats.increment("cache.miss", tags: ["action:markup_blob"])

    begin
      result = markup_blob!(blob, context)
      GitHub.cache.set(key, result, 0, false)
      result
    rescue Timeout::Error => e
      failbot(e)
      raise if context[:plaintext]
      # It took too long to render the markup. Let's just show a plaintext
      # version instead and cache it for a couple of minutes. Maybe something
      # will change that will let us render it without timing out later.
      result = markup_blob(blob, context.merge(plaintext: true))
      GitHub.cache.set(key, result, 2.minutes.to_i, false)
      result
    end
  end

  def markup_blob!(blob, context)
    start = GitHub::Dogstats.monotonic_time
    begin
      Timeout.timeout 5 do
        result = GitHub::Goomba::MarkupPipeline.call(nil, context)
        GitHub.dogstats.timing_since("blob.process.markup.success", start)
        result
      end
    rescue Exception # rubocop:disable Lint/RescueException
      GitHub.dogstats.timing_since("blob.process.markup.error", start)
      raise
    end
  end

  # Nota Bene: Every context key that affects the output of the
  # pipeline belongs here.
  def blob_markup_cache_key(blob, context)
    [
      CacheKeyLoggingBlacklist::BLOB_MARKUP_PREFIX,
      "v18",

      # We of course want to re-render if the blob's contents change.
      blob.oid,

      # The blob's filename affects what markup language it's interpreted as.
      # Ideally we'd get GitHub::Markup to tell us what language the file is,
      # but there's no API for that currently.
      blob.name.b,

      # Is the blob for a snippet for Gist? If so, include a snippet key in the cache
      # This ensures the full version, and the snippet get cached independently.
      ("snippet" if blob.snippet?),

      # Basic options that can affect the rendered output.
      ("plaintext" if context[:plaintext]),
      ("math" if context[:entity].try(:math_filter_enabled?)),

      # These items affect how relative links are resolved.
      context[:entity].try(:name_with_owner),
      context[:path].try(:b),
      context[:committish],
      context[:view],
      context[:sanitize_orphan_hrefs],

      # Changes to our syntax highlighting code affect the rendering of fenced
      # code blocks.
      GitHub::Colorize.cache_key_prefix,

      # Changes to task lists affect blob rendering
      GitHub.task_list_cache_version,
    ].join(":")
  end

  # Internal: Process a blob through the HTML pipeline and return the `output`
  #           from the result hash, marking the string `html_safe` if the
  #           pipeline sanitized the output.
  #
  # blob - An instance of Walker::CachedContent
  # context - Additional options to pass to the HTML pipeline
  #
  # Returns a String
  def markup_blob_content(blob, context = {})
    result = markup_blob(blob, context)
    output = result[:output]
    output = output.html_safe if result[:html_safe] # rubocop:disable Rails/OutputSafety
    output
  end

  # Public: Same as `markup_blob_content`, but return `nil` if the markup does
  # not succeed.
  #
  # blob - An instance of Walker::CachedContent
  # context - Additional options to pass to the HTML pipeline
  #
  # Returns a String or nil
  def markup_blob_content_if_successful(blob, context = {})
    result = markup_blob(blob, context)
    return unless result[:rendered]
    output = result[:output]
    output = output.html_safe if result[:html_safe] # rubocop:disable Rails/OutputSafety
    output
  end

  # Internal: *.rb.md style extensions.
  LITERATE_EXTENSIONS = { "rb" => "ruby" }

  # Internal: The context to use when rendering a blob through the HTML pipeline
  #
  # blob - An instance of Walker::CachedContent
  #
  # Returns a Hash
  def blob_html_context(blob)
    default_html_filter_context.merge(basic_html_context).tap do |context|
      context[:blob] = blob
      context[:name] = blob.path
      context[:anchor_icon] = octicon("link")

      context[:highlight] = "coffee" if blob.name =~ /litcoffee$/

      # allow "foo.coffee.md" or "foo.rb.md" to specify a default highlight
      segments = File.basename(blob.name).split(".")[1..-2]
      extension = segments.try(:last)

      if extension.present?
        context[:highlight] = LITERATE_EXTENSIONS[extension] || extension
      end
    end
  end
end
