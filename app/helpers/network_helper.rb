# frozen_string_literal: true

module NetworkHelper
  # Public: draw a network tree.
  #
  # root       - the root Repository of the repo network
  # repo_map   - a Hash of repository network children, indexed by parent id
  # indent:    - the indentation level. Defaults to -1
  # draw_root: - whether or not to draw the root. true by default.
  # out:       - the output buffer. Defaults to a new ActiveSupport::SafeBuffer
  # use_icons: - whether or not to use person/org icons in place of gravatars.
  #              Defaults to false, set to true in stafftools.
  #
  # Returns an ActiveSupport::SafeBuffer with a rendered repo tree.
  def draw_network_tree(root, repo_map, indent: -1, draw_root: true, out: ActiveSupport::SafeBuffer.new, use_icons: false)
    out << draw_repository(root, use_icons: use_icons) if draw_root

    array = Array(repo_map[root.id]).sort_by { |repo| repo.owner.name.downcase }

    array.each do |repo|
      tree_icon = repo == array.last ? "l" : "t"
      out << draw_repository(repo, tree_icon: tree_icon, indent: indent + 1, use_icons: use_icons)
      draw_network_tree(repo, repo_map, indent: indent + 1, draw_root: false, out: out, use_icons: use_icons)
    end

    out
  end

  # Internal: render a repository node in a network tree.
  #
  # repo       - the Repository to draw.
  # tree_icon  - a String, 'l' or 't', describing the the graph T or L shape to
  #              draw on the left hand side of this node. Defaults to nil for
  #              no shape.
  # indent:    - optional indentation amount for this node. Defaults to 0.
  # use_icons: - whether or not to use person/org icons in place of gravatars
  #              as the tree icon for each node. Defaults to false.
  #
  # Returns a rendered partial.
  def draw_repository(repo, tree_icon: nil, indent: 0, use_icons: false)
    render partial: "network/repository", locals: {
      repo: repo,
      tree_icon: tree_icon,
      indent: indent,
      use_icons: use_icons,
    }
  end

  def dependency_upgrade_examples(manifest_path:, package_name:, suggested_version:)
    manifest_type = DependencyManifestFile.corresponding_manifest_type(path: manifest_path)
    examples = []
    highlight_scope = nil

    case manifest_type
    when :gemfile, :gemfile_lock
      examples << %(gem "#{package_name}", ">= #{suggested_version}")
      highlight_scope = "source.ruby.gemfile"
    when :gemspec
      examples << %(spec.add_dependency "#{package_name}", ">= #{suggested_version}")
      examples << %(spec.add_development_dependency "#{package_name}", ">= #{suggested_version}")
      highlight_scope = "source.ruby"
    when :package_json, :package_lock_json
      examples << %("dependencies": {\n  "#{package_name}": ">=#{suggested_version}"\n})
      examples << %("devDependencies": {\n  "#{package_name}": ">=#{suggested_version}"\n})
      highlight_scope = "source.json"
    when :requirements_txt
      examples << %(#{package_name}>=#{suggested_version})
    when :pipfile, :pipfile_lock
      examples << %([packages]\n#{package_name} = ">=#{suggested_version}")
      examples << %([dev-packages]\n#{package_name} = ">=#{suggested_version}")
    when :setup_py
      examples << %(install_requires=[\n    '#{package_name}>=#{suggested_version}'\n],)
      examples << %(extra_requires=[\n    '#{package_name}>=#{suggested_version}'\n],)
      highlight_scope = "source.python"
    when :pom_xml
      group_id, artifact_id = package_name.split(":", 2)
      examples << %{<dependency>\n  <groupId>#{group_id}</groupId>\n  <artifactId>#{artifact_id}</artifactId>\n  <version>[#{suggested_version},)</version>\n</dependency>}
      highlight_scope = "text.xml"
    when :nuspec
      examples << %(<dependency id="#{package_name}" version="#{suggested_version}" />)
      highlight_scope = "text.xml"
    when :csproj, :vbproj, :vcxproj, :fsproj
      examples << %(<PackageReference Include="#{package_name}" Version="#{suggested_version}" />)
      highlight_scope = "text.xml"
    when :package_config
      examples << %(<package id="#{package_name}" version="#{suggested_version}" />)
      highlight_scope = "text.xml"
    when :yarn_lock
      examples << %(#{package_name}@^#{suggested_version}:\n  version "#{suggested_version}")
    when :composer_json, :composer_lock
      examples << %("require": {\n  "#{package_name}": "#{suggested_version}"\n})
    end

    Array.new.tap do |safe_examples|
      examples.each do |example|
        lines = GitHub::Colorize.highlight_one(highlight_scope, example)
        safe_examples << safe_join(lines, "\n") if lines
      end
    end
  end

  def corresponding_manifest_file(package_manager)
    DependencyManifestFile.corresponding_manifest_file(package_manager: package_manager)
  end
end
