# frozen_string_literal: true

module FeatureGateHelper
  include HydroHelper

  def feature_gate_upsell_click_attrs(feature = nil, user: current_user)
    hydro_click_tracking_attributes("feature_gate_upsell.click",
      feature_name: feature,
      user_id: user&.id,
    )
  end
end
