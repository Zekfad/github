# frozen_string_literal: true

require "octicons"
require "octicons_helper"
require "action_view"

module OcticonsCachingHelper
  include ActionView::Helpers::TagHelper

  mattr_accessor :octicons_helper_cache, default: {}

  V1_TO_V2_REMAPPINGS = {
    "arrow-small-down" => "arrow-down",
    "arrow-small-left" => "arrow-left",
    "arrow-small-right" => "arrow-right",
    "arrow-small-up" => "arrow-up",
    "circuit-board" => "cpu",
    "cloud-download" => "download",
    "cloud-upload" => "upload",
    "dependent" => "package-dependents",
    "dashboard" => "meter",
    "diff" => "file-diff",
    "file-media" => "image",
    "file-pdf" => "file",
    "file-symlink-directory" => "file-submodule",
    "gist" => "code-square",
    "gist-secret" => "lock",
    "github-action" => "play",
    "heart-outline" => "heart",
    "heart" => "heart-fill",
    "internal-repo" => "repo",
    "jersey" => "people",
    "kebab-vertical" => "kebab-horizontal",
    "keyboard" => "info",
    "line-arrow-down" => "arrow-down",
    "line-arrow-left" => "arrow-left",
    "line-arrow-right" => "arrow-right",
    "line-arrow-up" => "arrow-up",
    "mail-read" => "mail",
    "mute" => "bell-slash",
    "no-newline" => "no-entry",
    "paintcan" => "paintbrush",
    "plus-small" => "plus",
    "primitive-dot" => "dot-fill",
    "primitive-dot-stroke" => "dot",
    "primitive-square" => "square-fill",
    "radio-tower" => "broadcast",
    "repo-force-push" => "repo-push",
    "repo-template-private" => "lock",
    "request-changes" => "diff",
    "saved" => "bookmark",
    "settings" => "filter",
    "text-size" => "typography",
    "unmute" => "bell",
    "unsaved" => "bookmark-slash",
    "watch" => "hourglass",
    "workflow-all" => "workflow"
  }

  def octicon(symbol, options = {})
    return "" if symbol.nil?

    cache_key = [symbol, options].to_s

    if octicons_helper_cache[cache_key]
      octicons_helper_cache[cache_key]
    else
      icon =
        if Octicons::OCTICON_SYMBOLS.keys.include?(symbol.to_s)
          Octicons::Octicon.new(symbol, options)
        else
          Octicons::Octicon.new(V1_TO_V2_REMAPPINGS[symbol.to_s], options)
        end

      tag = content_tag(:svg, icon.path.html_safe, icon.options).freeze # rubocop:disable Rails/OutputSafety
      octicons_helper_cache[cache_key] = tag
      tag
    end
  end
end
