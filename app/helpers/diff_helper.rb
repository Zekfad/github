# frozen_string_literal: true

module DiffHelper
  DOTFILE_REGEX = %r{\A[.]}
  DIFF_FILE_LABEL_TRUNCATION_LENGTH = 92
  TOGGLEABLE_LENGTH = 10
  REVIEW_DISMISSED_LENGTH = 15

  include GitHub::UTF8
  include EscapeHelper
  extend ActionView::Helpers::TagHelper
  extend EscapeHelper

  # Determine if the diff should be soft-wrapped based on the filename.
  #
  # path - a filename with the extension
  def soft_wrap?(path)
    languages = Linguist::Language.find_by_filename(path)
    languages = Linguist::Language.find_by_extension(path) if languages.empty?

    languages.any? { |lang| lang.wrap }
  end

  def diff_file_type(diff)
    return "Binary" if diff.binary?
    return "Empty"  if diff.changes == 0 && !diff.renamed?
  end

  def diff_label(diff)
    return "BIN" if diff.binary?
    return "0"   if diff.changes == 0
  end

  def format_diffstat_line(diff, total)
    format_diffstat_line_from(total: total, changes: diff.changes, additions: diff.additions,
                              deletions: diff.deletions)
  end
  module_function :format_diffstat_line

  def format_diffstat_line_from(total:, changes:, additions:, deletions:)
    stat_line = diffstat_line_from(columns: total, changes: changes, additions: additions,
                                   deletions: deletions)

    adjusted_additions = stat_line.count("+")
    adjusted_deletions = stat_line.count("-")
    padding = total - (adjusted_additions + adjusted_deletions)

    tags = []
    tags += [content_tag(:span, "", class: "diffstat-block-added")] * adjusted_additions
    tags += [content_tag(:span, "", class: "diffstat-block-deleted")] * adjusted_deletions
    tags += [content_tag(:span, "", class: "diffstat-block-neutral")] * padding
    safe_join(tags)
  end
  module_function :format_diffstat_line_from

  def diffstat_line_from(changes:, additions:, deletions:, columns: 80, addition_symbol: "+",
                         deletion_symbol: "-")
    adjust = changes > columns ? columns / changes.to_f : 1.0

    (addition_symbol * (additions * adjust)) + (deletion_symbol * (deletions * adjust))
  end
  module_function :diffstat_line_from

  def code_marker_for(line_type)
    case line_type
    when :addition
      "+"
    when :deletion
      "-"
    else
      " "
    end
  end

  def diff_file_label(diff, truncate = true, toggleable: false, review_dismissed: false)
    if diff.copied? || diff.renamed?
      a_path, b_path = diff.a_path, diff.b_path
      original_path = truncate ? reverse_truncate(a_path, length: 44) : a_path
      new_name = truncate ? reverse_truncate(b_path, length: 44) : b_path
      safe_join([original_path, "→", new_name], " ")
    else
      path = diff.path || diff.a_path
      length = DIFF_FILE_LABEL_TRUNCATION_LENGTH
      length -= TOGGLEABLE_LENGTH if toggleable
      length -= REVIEW_DISMISSED_LENGTH if review_dismissed
      truncate ? reverse_truncate(path, length: length) : path
    end
  end

  def reverse_truncate(text, options = {})
    escape = options.fetch(:escape) { true }
    # Never escape inside truncate, as return value is reversed
    options = options.merge(escape: false)

    result = truncate(text.reverse, options).reverse
    escape ? h(result) : result
  end

  def head_repository
    if @comparison && @comparison.head_repo
      @comparison.head_repo
    else
      current_repository
    end
  end

  def base_repository
    if @comparison && @comparison.base_repo
      @comparison.base_repo
    else
      current_repository
    end
  end

  def diff_base_blob(diff, repository = nil)
    TreeEntry.new(repository || head_repository, {
      "oid" => diff.a_blob,
      "path" => diff.a_path,
      "mode" => diff.a_mode,
      "type" => "blob",
    })
  end

  def diff_head_blob(diff, repository = nil)
    TreeEntry.new(repository || head_repository, {
      "oid" => diff.b_blob,
      "path" => diff.b_path,
      "mode" => diff.b_mode,
      "type" => "blob",
    })
  end

  def diff_blob(diff, repository = nil)
    if diff.deleted?
      diff_base_blob(diff, repository)
    else
      diff_head_blob(diff, repository)
    end
  end

  # The path to view the raw head blob in the same domain (no redirect to raw.)
  def diff_head_blob_url(diff)
    "/#{head_repository.name_with_owner}/raw/#{diff.b_sha}/#{escape_url_branch(diff.b_path)}"
  end

  # The path to view the raw base blob in the same domain (no redirect to raw.)
  def diff_base_blob_url(diff)
    "/#{base_repository.name_with_owner}/raw/#{diff.a_sha}/#{escape_url_branch(diff.a_path)}"
  end

  def diff_head_tree_url(diff)
    "/#{head_repository.name_with_owner}/tree/#{diff.b_sha}/#{escape_url_branch(diff.b_path)}"
  end

  def diff_base_tree_url(diff)
    "/#{head_repository.name_with_owner}/tree/#{diff.a_sha}/#{escape_url_branch(diff.a_path)}"
  end

  def diff_blob_excerpt_url(sha, path, mode, params = {})
    query_params = params.dup
    query_params[:path] = path
    query_params[:mode] = mode
    query_params[:diff] = params[:diff] if params[:diff]
    "/#{base_repository.name_with_owner}/blob_excerpt/#{sha}?#{query_params.to_query}"
  end

  # The path to view the blob page at a specific SHA
  def diff_blob_path(diff, params = {})
    sha = diff.deleted? ? diff.a_sha : diff.b_sha
    file_path = diff.deleted? ? diff.a_path : diff.b_path
    path = "/#{base_repository.name_with_owner}/blob/#{sha}/#{escape_url_branch(file_path)}"

    if params.empty?
      path
    else
      "#{path}?#{params.to_query}"
    end
  end

  # The path to view the blob page on the branch of the current comparison. If
  # the comparison's head is a SHA1, this falls back to diff_blob_path.
  #
  # diff   - The GitHub::Diff::Entry identifying the blob to link to.
  # action - Either :view or :edit.
  # params - A object containing any URL parameters
  #
  # Returns the URL encoded path to the blob page for the diff entry.
  def diff_blob_branch_path(diff, action = :view, params = {})
    action = :blob if action.nil? || action == :view
    if !diff.deleted? && @pull && @pull.head_ref_exist?
      path = "/#{head_repository.name_with_owner}/#{action}/#{escape_url_branch(@pull.head_ref_name)}/#{escape_url_branch(diff.b_path)}"

      if !params.empty?
        path += "?#{params.to_query}"
      end

      path
    else
      diff_blob_path(diff, params)
    end
  end

  # Get the "type" of this diff if it's not usable in the online editor.
  # Used to fill in the following madlibs in diff/_diff.html.erb:
  #
  #   Online editor is disabled for {type} files.
  #
  # Returns a string "type" name, or nil if the blob this diff describes is
  # editable.
  def diff_uneditable_type(diff)
    if diff.binary?
      "binary"
    elsif diff.lfs_pointer?
      "Git LFS"
    end
  end

  # Can the current user edit blobs for the current pull request? Must be logged
  # in and under an open pull request's context. Edit controls are only shown
  # for users that have push access to the pull request's head repository.
  #
  # This has some kinks. PRs sent from forks are only editable by the fork's
  # owner or collaborators which does not include the owner of the repository
  # the PR is submitted on by default. Hoping we can eventually make this more
  # open at some point.
  #
  # Returns true when the current user can edit the PR's blobs, falsey otherwise.
  def can_edit_pull_request_blobs?
    return @can_edit_pull_request_blobs if defined?(@can_edit_pull_request_blobs)
    @can_edit_pull_request_blobs = can_edit_pull_request_blobs_check
  end

  # Perform the current user can edit blobs check for the current pull request.
  # This is an expensive operation.
  def can_edit_pull_request_blobs_check
    logged_in? &&
    @pull && !@pull.closed? && @pull.head_ref_exist? &&
    @pull.head_ref_pushable_by?(current_user)
  end

  def diff_short_path(diff)
    path = diff.try(:path) || diff.a_path || diff.b_path
    Digest::MD5.hexdigest(path)[0, 7]
  end

  def diff_file_path(diff)
    "#{diff.deleted? ? diff.a_path : diff.b_path}"
  end

  # Can the diff be edited in secret edit mode?
  def can_edit_in_secret_edit_mode?(diff)
    current_repository_writable? && collaborative_editing_enabled? && can_edit_pull_request_blobs? && diff_uneditable_type(diff).nil?
  end

  # Abbreviates two paths like 'git diff --stat'
  #   >> abbreviate_rename 'foo/bar/baz.txt', 'foo/bling/baz.txt'
  #   => "foo/{bar => bling}/baz.txt
  def abbreviate_rename(from, to, arrow = "=>")
    from, to = from.split("/"), to.split("/")

    front = []
    while from.first && from.first == to.first
      from.shift
      front << to.shift
    end

    back = []
    while from.last && from.last == to.last
      from.pop
      back.unshift to.pop
    end

    from, to = from.join("/"), to.join("/")
    if front.empty? && back.empty?
      [from, arrow, to].join
    else
      (front + ["{" + from + arrow + to + "}"] + back).join("/")
    end
  end

  def diff_toc_label(diff, length: 100)
    if diff.copied? || diff.renamed?
      path = abbreviate_rename(diff.a_path, diff.b_path, " → ")
      reverse_truncate(path, length: length)
    else
      reverse_truncate(diff.path, length: length)
    end
  end

  def diff_toc_basename(diff, length: 100)
    if diff.copied? || diff.renamed?
      path = abbreviate_rename(File.basename(diff.a_path), File.basename(diff.b_path), " → ")
      reverse_truncate(path, length: length)
    else
      reverse_truncate(File.basename(diff.path), length: length)
    end
  end

  def diff_mode_label(diff)
    a, b = diff.a_mode, diff.b_mode
    if a && b && a != b && a != GitHub::NULL_MODE && b != GitHub::NULL_MODE
      safe_join([diff.a_mode, "→", diff.b_mode], " ")
    end
  end

  #
  # Diff Line Comments
  #

  # Outputs a short linked line pointing to the comment. Useful for comment
  # headers
  def diff_comment_shortline(comment)
    case comment
    when CommitComment
      commit_comment_shortline(comment)
    when PullRequestReviewComment
      review_comment_shortline(comment)
    else raise TypeError
    end
  end

  # DOM id for a review comment.
  #
  # Note that for a PullRequestReviewComment, you **must** specify if it is
  # rendering in a diff comment_context or discussion comment_context.
  # NOTE: the discussion comment_context isn't actually a "diff" view, but
  # this method handles that anyways.
  #
  # Returns a String
  def diff_comment_id(comment, comment_context = "diff")
    case comment
    when CommitComment
      "commitcomment-#{comment.id}"
    when PullRequestReviewComment
      case comment_context.to_s
      when "discussion"
        "#{CommentsHelper::DISCUSSION_DOM_ID_PREFIX}#{comment.id}"
      when "diff"
        "#{CommentsHelper::COMMIT_COMMENT_DOM_ID_PREFIX}#{comment.id}"
      else
        "#{CommentsHelper::COMMIT_COMMENT_DOM_ID_PREFIX}#{comment.id}"
      end
    else
      raise TypeError, "unknown comment type: #{comment.class}"
    end
  end

  # Public: Get a DOM id for a diff in the timeline.
  #
  # thread - DeprecatedPullRequestReviewThread or CommitCommentThread
  #
  # Returns a string.
  def timeline_diff_id(thread)
    "diff-for-comment-#{thread.reply_to_id}"
  end

  #
  # Colorization
  #

  COLORIZED_LINE_LENGTH_LIMIT = 512
  # When used with String#split, this will split the string into a sequence of
  # atomic words and individual non-word characters. This means that when a
  # single letter within a word is changed, the whole word will be highlighted.
  # When a single non-word character is changed, only that character will be
  # highlighted.
  DIFF_WORD_BOUNDARY = /(?<=\p{^Word})|(?=\p{^Word})/
  # Changed words separated by fewer than MINIMUM_UNCHANGED_LENGTH characters
  # will get combined into a single larger change.
  MINIMUM_UNCHANGED_LENGTH = 3

  # Detects changes within a line from a diff and surrounds them with <span
  # class="x"> elements.
  #
  # line - A GitHub::Diff::Line (hopefully with a #related_line).
  #
  # Returns a String containing `line.text`, possibly with <span class="x">
  # elements added to highlight changes.
  def mark_intra_line_changes(line)
    return line.text unless line.related_line
    GitHub.instrument("diff.mark_intra_line_changes") do
      GitHub.dogstats.time("diff", tags: ["action:highlight_words", "type:single_line"]) do
        blocks = compute_intra_line_changes(line.type, line.text, line.related_line.text)
        return line.text if blocks.empty?
        highlighted = blocks.map do |block|
          if block[:changed]
            content_tag(:span, block[:text], {class: "x x-first x-last"})
          else
            block[:text]
          end
        end

        safe_join(highlighted)
      end
    end
  end

  # Detects changes within a syntax-highlighted line from a diff and surrounds
  # them with <span class="x"> elements.
  #
  # line         - A GitHub::Diff::Line.
  # html         - The syntax-highlighted HTML String of this line from the diff.
  # related_html - The syntax-highlighted HTML String of the related line from
  #                the diff. If nil no highlighting will be performed.
  # prefixed     - Whether or not the provided line is prefixed with +/-. Default: true
  #
  # Returns an HTML String containing `html`, possibly with <span class="x">
  # elements added to highlight changes.  The return string is always distinct
  # from the input HTML.
  def mark_intra_line_changes_html(line, html, related_html, prefixed: true)
    return html.dup unless related_html
    return html.dup if line.text.size > COLORIZED_LINE_LENGTH_LIMIT || line.related_line.text.size > COLORIZED_LINE_LENGTH_LIMIT

    return html.dup if GitHub.request_timer.elapsed("diff.mark_intra_line_changes_html") > 1.second && !Rails.test?
    GitHub.instrument("diff.mark_intra_line_changes_html") do
      GitHub.dogstats.time("diff", tags: ["action:highlight_words_html", "type:single_line"]) do
        # We wrap the HTML in <div> tags because some versions of libxml won't
        # return top-level text nodes inside a DocumentFragment when we use
        # .xpath(".//text()") below.
        doc = Nokogiri::HTML::DocumentFragment.parse("<div>#{html}</div>")
        related_doc = Nokogiri::HTML::DocumentFragment.parse("<div>#{related_html}</div>")

        blocks = compute_intra_line_changes(line.type, doc.text, related_doc.text, prefixed: prefixed)
        return html.dup if blocks.empty?

        # Convert the blocks into a list of changed ranges.
        change_start = 0
        changed_ranges = []
        blocks.each do |block|
          change_end = change_start + block[:text].length
          changed_ranges << (change_start...change_end) if block[:changed]
          change_start = change_end
        end

        # Wrap changed text in <span class="x"> elements inside of any existing
        # elements.
        text_start = 0
        doc.xpath(".//text()").each do |node|
          range = changed_ranges.first
          break unless range
          # Loop invariant: text_start < range.last
          # I.e., any changed ranges that end before the current node have already
          # been removed from changed_ranges.

          text = node.text
          text_end = text_start + text.length
          if text_end <= range.first
            # We haven't reached the next changed range yet. Just move along to the
            # next node.
            text_start = text_end
            next
          end

          # The changed range intersects this node.

          if range.first <= text_start && text_end <= range.last && node.parent.name == "span" && node.parent.children.count == 1
            classes = "x".dup
            classes << " x-first" if text_start <= range.first
            classes << " x-last" if range.last <= text_end
            # The entire text node is changed, and we're already within a <span>
            # element. Let's just add "x" to the existing element's class to keep
            # the resulting DOM a bit smaller.
            if !node.parent["class"]
              node.parent["class"] = classes
            elsif node.parent["class"] !~ /\bx\b/
              node.parent["class"] += " #{classes}"
            end
          else
            # The changed range covers some portion of this node. Subsequent ranges
            # may also intersect this node. Let's handle all of them at once.
            consumed = 0
            new_contents = Nokogiri::XML::DocumentFragment.new(doc)
            changed_ranges.each do |range|
              break if text_end <= range.first
              classes = "x".dup
              classes << " x-first" if text_start <= range.first
              classes << " x-last" if range.last <= text_end

              change_start = [text_start, range.first].max - text_start
              change_end = [text_end, range.last].min - text_start
              # Everything before this change is unchanged.
              new_contents << Nokogiri::XML::Text.new(text[consumed...change_start], doc)
              span = Nokogiri::XML::Element.new("span", doc)
              span["class"] = classes
              span << Nokogiri::XML::Text.new(text[change_start...change_end], doc)
              new_contents << span
              consumed = change_end
            end
            # Everything after the last change is unchanged.
            new_contents << Nokogiri::XML::Text.new(text[consumed...text.length], doc)
            node.replace(new_contents)
          end

          # Move past any ranges that end within this node.
          while changed_ranges.any?
            range = changed_ranges.first
            break if text_end < range.last
            changed_ranges.shift
          end

          # On to the next node!
          text_start = text_end
        end

        # Dig the relevant HTML out of the wrapper container <div> we added at the
        # start.
        result = doc.child.inner_html
        if html.html_safe?
          result.html_safe # rubocop:disable Rails/OutputSafety
        else
          result
        end
      end
    end
  end

  # Private: Compute the changed/unchanged portions of two lines of text from a diff.
  #
  # type     - The type Symbol for this line in the diff, as returned by
  #           GitHub::Diff::Line#type.
  # text     - The unformatted text String of the line from the diff.
  # related  - The unformatted text String of the related line from the diff as
  #           provided by GitHub::Diff::Line#related_line#text. If nil, no
  #           changes will be highlighted.
  # prefixed - Whether or not the provided line is prefixed with +/-. Default: true
  #
  # Returns an Array of Hashes, each with the following keys:
  #   :text    - String containing some substring of `text`.
  #   :changed - Boolean indicating whether this substring is changed or not.
  def compute_intra_line_changes(type, text, related, prefixed: true)
    return [] unless related
    return [] if text.size > COLORIZED_LINE_LENGTH_LIMIT || related.size > COLORIZED_LINE_LENGTH_LIMIT

    old, new = case type
    when :addition
      [related, text]
    when :deletion
      [text, related]
    end

    return [] unless old && new

    old.scrub!
    new.scrub!

    plus_or_minus = (type == :deletion ? old : new)[0..0]

    start = prefixed ? 1 : 0
    old = old[start..-1]
    new = new[start..-1]

    return [] if old.nil? || new.nil?

    old_words = old.split(DIFF_WORD_BOUNDARY)
    new_words = new.split(DIFF_WORD_BOUNDARY)
    words = type == :deletion ? old_words : new_words

    # blocks is an Array describing the changed/unchanged sequences of words.
    # Each item in the Array is a Hash with the following keys:
    #   :changed - Boolean stating whether these words were changed or not.
    #   :range   - The Range of words this block describes. range.exclude_end?
    #              is always true.
    blocks = []

    # Compute changed/unchanged blocks based on the word diff.
    Diff::LCS.diff(old_words, new_words, Diff::LCS::ContextDiffCallbacks).each do |changes|
      previous_change_end = blocks.last ? blocks.last[:range].last : 0
      change_start = type == :deletion ? changes.first.old_position : changes.first.new_position
      # We want change_end to point to just after the last changed word. If the
      # change is for the highlighted string, position will be pointing *at*
      # the last changed word. If the change is for the non-highlighted string,
      # it'll already be pointing just after the last changed word.
      change_is_for_highlighted_string = type == :deletion ? changes.last.deleting? : changes.last.adding?
      change_end = type == :deletion ? changes.last.old_position : changes.last.new_position
      change_end += 1 if change_is_for_highlighted_string

      # Everything between the last change and this one must be unchanged.
      unchanged_range = previous_change_end...change_start

      if blocks.last && words[unchanged_range].sum(&:length) < MINIMUM_UNCHANGED_LENGTH
        # There are so few characters between the last change and this one that
        # it would look noisy to consider them separate changes. Let's just
        # combine them into one big change.
        blocks.last[:range] = blocks.last[:range].first...change_end
        next
      end

      blocks << { changed: false, range: unchanged_range }
      blocks << { changed: true,  range: change_start...change_end }
    end

    # Everything after the last block is unchanged.
    blocks << { changed: false, range: blocks.last[:range].last...words.length } if blocks.last

    return [] unless blocks.any? { |block| block[:changed] }
    blocks.each do |block|
      block_words = words[block[:range]] || []
      block[:text] = block_words.join("")
      block.delete(:range)
    end
    blocks.reject! { |block| block[:text].empty? }

    if prefixed
      [{ changed: false, text: plus_or_minus }] + blocks
    else
      blocks
    end
  end
  private :compute_intra_line_changes

  # Detect next path after the current in a set of changes.
  #
  # change - Array of Hash changes
  # page   - Integer page number
  #
  # Returns
  def diff_previous_changed_path(changes, page)
    filenames = changes.map { |c| c["old_file"]["path"] }
    filenames[page - 2] if page > 1
  end

  # Detect next path after the current in a set of changes.
  #
  # change - Array of Hash changes
  # page   - Integer page number
  #
  # Returns
  def diff_next_changed_path(changes, page)
    filenames = changes.map { |c| c["old_file"]["path"] }
    filenames[page]
  end

  def diff_enum_peek(enumerator)
    enumerator.peek
  rescue StopIteration
    nil
  end

  def record_diff_metrics(diff, summary: false)
    record_diff_metrics_to_dogstats(diff)
    record_diff_summary_metrics(diff) if summary

    result = case
    when diff.truncated_for_max_files?
      "truncated.max_files"
    when diff.truncated_for_max_lines?
      "truncated.max_lines"
    when diff.truncated_for_max_size?
      "truncated.max_size"
    when diff.truncated_for_timeout?
      "truncated.timeout"
    when diff.timed_out?
      "error.timeout"
    when diff.too_busy?
      "error.too_busy"
    when diff.corrupt?
      "error.corrupt"
    when diff.missing_commits?
      "error.missing_commits"
    end

    tags = ["action:render", "type:#{params[:action].parameterize}", "controller:#{params[:controller].parameterize}"]
    tags << "result:#{result}" if result

    GitHub.dogstats.increment("diff", tags: tags)

    too_many_lines_count = 0
    too_many_bytes_count = 0
    diff.entries.each do |entry|
      case
      when entry.skipped_for_max_lines?
        too_many_lines_count += 1
      when entry.skipped_for_max_size?
        too_many_bytes_count += 1
      end
    end

    if too_many_lines_count > 0
      GitHub.dogstats.histogram("diff.file_skipped", too_many_lines_count, tags: tags + ["result:max_lines"])
    end

    if too_many_bytes_count > 0
      GitHub.dogstats.histogram("diff.file_skipped", too_many_bytes_count, tags: tags + ["result:max_size"])
    end

    limited_files_count = too_many_lines_count + too_many_bytes_count
    if limited_files_count > 0
      GitHub.dogstats.histogram("diff.file_skipped", limited_files_count, tags: tags + ["result:limited_files"])
    end
  end

  FILES_RANGE_BOUNDARIES = [1, 3, 10, 30, 100, 300, 1000, 3000, 10_000, 20_000, 40_000].freeze
  LINES_RANGE_BOUNDARIES = [1, 3, 10, 30, 100, 300, 1000, 3000, 10_000, 20_000].freeze
  BYTES_RANGE_BOUNDARIES = [1, 30, 100, 500, 1000, 5000, 10_000, 20_000].freeze

  def record_diff_summary_metrics(diff)
    prefix = "diff.render.summary.count"

    record_single_diff_metric_to_dogstats(name: "#{prefix}.files", value: diff.changed_files,
                                          range_boundaries: FILES_RANGE_BOUNDARIES)
    record_single_diff_metric_to_dogstats(name: "#{prefix}.changes", value: diff.changes,
                                          range_boundaries: LINES_RANGE_BOUNDARIES)
  end

  # Public: We use dogstats for new metrics
  #
  # diff - GitHub::Diff
  #
  # Returns nothing
  def record_diff_metrics_to_dogstats(diff)
    prefix = "diff.render.count"

    record_single_diff_metric_to_dogstats(name: "#{prefix}.files", value: diff.size,
                                          range_boundaries: FILES_RANGE_BOUNDARIES)
    record_single_diff_metric_to_dogstats(name: "#{prefix}.lines", value: diff.total_line_count,
                                          range_boundaries: LINES_RANGE_BOUNDARIES)
    record_single_diff_metric_to_dogstats(name: "#{prefix}.bytes", value: diff.total_byte_count,
                                          range_boundaries: BYTES_RANGE_BOUNDARIES)

    diff.entries.each do |entry|
      record_single_diff_metric_to_dogstats(name: "#{prefix}.per_file.lines",
                                            value: entry.line_count,
                                            range_boundaries: LINES_RANGE_BOUNDARIES,
                                            options: { sample_rate: 0.1 })
      record_single_diff_metric_to_dogstats(name: "#{prefix}.per_file.bytes",
                                            value: entry.byte_count,
                                            range_boundaries: BYTES_RANGE_BOUNDARIES,
                                            options: { sample_rate: 0.1 })
    end
  end

  # Private: Records count metric to dogstats
  #
  # Returns nothing
  def record_single_diff_metric_to_dogstats(name:, value:, range_boundaries:, infinite: true, options: {})
    options[:tags] = dogstats_request_tags

    if value <= 0
      range_string = "0"
    else
      range_boundaries += [Float::INFINITY] if infinite
      ranges = range_boundaries.each_cons(2).map { |lower, upper| Range.new(lower, upper - 1) }

      lower_boundary = range_boundaries.first
      ranges.unshift((0..lower_boundary - 1))

      relevant_range = ranges.find { |range| range.include?(value) }
      range_string = if relevant_range.size == 1
        "#{relevant_range.begin}"
      else
        "#{relevant_range.begin}-#{relevant_range.end}"
      end
    end

    options[:tags] << "range:#{range_string}"

    GitHub.dogstats.increment(name, options)
  end

  def get_file_type(path)
    return if path.blank?

    file_type = File.extname(path)
    return utf8(file_type) if file_type.present?

    basename = File.basename(path)
    dotfile = DOTFILE_REGEX.match(utf8(basename))
    return "dotfile" if dotfile

    "No extension"
  end
end
