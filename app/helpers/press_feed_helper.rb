# frozen_string_literal: true

module PressFeedHelper
  DISPLAY_DATE_FORMAT = "%m/%d/%y"

  def current_or_past_entries
    all_entries
      .select(&:active?)
      .select(&:current_or_past?)
      .sort_by(&:date)
      .reverse
  end

  private

  def all_entries
    @all_entries ||= PressFeedDecorator
      .all_from_collection(press_feed_collection, self)
      .sort_by(&:date)
  end

  class PressFeedDecorator < OpenStruct
    def self.all_from_collection(collection, helper)
      collection.map { |entry| PressFeedDecorator.new entry, helper }
    end

    def active?
      active
    end

    def current_or_past?
      current? || in_the_past?
    end

    def formatted_date
      date.strftime(DISPLAY_DATE_FORMAT)
    end

    def date
      Date.parse super
    end

    private

    delegate :params, to: :@helper

    def initialize(event, helper)
      super event
      @helper = helper
      freeze
    end

    def current?
      date.today?
    end

    def in_the_past?
      !future?
    end

    def future?
      date.future?
    end
  end
end
