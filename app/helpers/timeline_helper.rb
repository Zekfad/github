# frozen_string_literal: true

module TimelineHelper
  include PlatformHelper
  include TimelineMarkerHelper

  DEFAULT_PAGE_SIZE = 60
  THUMBS_UPS = %w(:+1: +1 👍 👍🏻 👍🏼 👍🏽 👍🏾 👍🏿).freeze
  SCOPED_TIMELINE_EVENTS = %w(
    ISSUE_COMMENT
    CROSS_REFERENCED_EVENT
    CLOSED_EVENT
    CONNECTED_EVENT
    DISCONNECTED_EVENT
    REOPENED_EVENT
    COMPOSABLE_COMMENT
  ).freeze

  # Public: Returns the page size for the timeline
  #
  # Users can pass `timeline_per_page=N` to set the used page size to N on
  # issue and pull request timelines.
  #
  # Returns Integer
  def timeline_page_size
    return DEFAULT_PAGE_SIZE unless params[:timeline_per_page]
    [params[:timeline_per_page].to_i.abs, DEFAULT_PAGE_SIZE].min
  end

  def initial_timeline_page_size
    (timeline_page_size / 2.0).to_i
  end

  CommentsAreSimilarFragment = parse_query <<-'GRAPHQL'
    fragment on Comment {
      body
    }
  GRAPHQL

  def comments_are_similar?(node_a, node_b)
    comment_a = CommentsAreSimilarFragment.new(node_a).body.strip
    comment_b = CommentsAreSimilarFragment.new(node_b).body.strip

    comment_a == comment_b || (THUMBS_UPS.include?(comment_a) && THUMBS_UPS.include?(comment_b))
  end

  GroupTimelineNodesFragment = parse_query <<-'GRAPHQL'
    fragment on Comment {
      ...TimelineHelper::CommentsAreSimilarFragment
    }
  GRAPHQL

  # Public: Group Array of GraphQL nodes by their types.
  #
  # Only groups consecutive nodes together that have the same type.
  # Some types of node will always end up in their own, lonely group.
  #
  # nodes - Array of GraphQL nodes
  #
  # Returns Array<Array> of nodes
  def group_timeline_nodes(nodes, group_repeated_comments: false)
    nodes.chunk_while { |node_before, node_after|
      case node_before
      when PlatformTypes::PullRequestReview,
           PlatformTypes::PullRequestReviewThread,
           PlatformTypes::PullRequestCommitCommentThread,
           PlatformTypes::ComposableComment
        # These nodes are never grouped
        false
      when PlatformTypes::IssueComment
        next unless group_repeated_comments
        # Repeated comments get grouped together
        comments_are_similar?(node_before, node_after) if node_after.is_a?(PlatformTypes::IssueComment)
      when node_after.class
        # Group consecutive nodes of the same type
        true
      when PlatformTypes::TimelineEvent
        # Group timeline events together
        node_after.is_a?(PlatformTypes::TimelineEvent)
      else
        # Otherwise we start a new group
        false
      end
    }.to_a
  end

  def timeline_owner_response_for(query:, id:, defer_collapsed_threads: false, scope_timeline: false)
    item_types = scope_timeline ? SCOPED_TIMELINE_EVENTS : nil
    platform_execute(query, variables: {
      id: id,
      timelineSince: discussion_last_modified_at&.iso8601,
      timelinePageSize: initial_timeline_page_size,
      syntaxHighlightingEnabled: syntax_highlighted_diffs_enabled?,
      hasFocusedReviewThread: false,
      hasFocusedReviewComment: false,
      deferCollapsedThreads: defer_collapsed_threads,
      scopedItemTypes: item_types,
    })
  end

  # Public: Should collapsed threads be deferred?
  #
  # This is disabled for the TimelineController which is responsible
  # for loading focused (i.e. anchor-linked) threads/comments. For JS
  # expanding to work we need collapsed thread content to be rendered even
  # though it will be hidden at first.
  #
  # Returns a Boolean.
  def defer_collapsed_threads?
    controller_name != "timeline"
  end

  def count_to_range(total_count)
    case total_count
    when nil then "nil"
    when 0 then "0"
    when 1..9 then "1-9"
    when 10..29 then "10-29"
    when 30..59 then "30-59"
    when 60..119 then "60-119"
    when 120..299 then "120-299"
    else "300+"
    end
  end

  def record_timeline_size_metric(total_count)
    options = {
      tags: dogstats_request_tags,
    }

    range = count_to_range(total_count)
    options[:tags] << "range:#{range}"

    GitHub.dogstats.increment("timeline.size", options)
  end

  def track_query_execution(query_name, additional_tags = [])
    timer = Timer.start
    issue_node = yield
    timer.stop
    combined_tags = query_range_tags(issue_node).concat(additional_tags).uniq
    GitHub.dogstats.distribution("issue_#{ query_name }_query_execute.dist.time", timer.elapsed_ms, tags: combined_tags)
    issue_node
  end

  private

  def query_range_tags(issue_node)
    total_count = count_to_range(issue_node.to_h.dig(:timeline, :totalCount))
    total_range_tag = total_count.nil? ? [] : ["range:#{ count_to_range(total_count.to_i) }"]
    timeline_start_tags = node_range_tags("timeline_start", issue_node.to_h.dig(:timelineStart, :nodes))
    timeline_end_tags = node_range_tags("timeline_end", issue_node.to_h.dig(:timelineEnd, :nodes))
    total_range_tag.concat(timeline_start_tags, timeline_end_tags)
  end

  def node_range_tags(tag_prefix, timeline_nodes = [])
    timeline_nodes
      .group_by { |node| node[:__typename] }
      .map { |node_type, group| "#{ tag_prefix }_range:#{ node_type }:#{ count_to_range(group.size) }" }
  end
end
