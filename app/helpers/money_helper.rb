# frozen_string_literal: true

module MoneyHelper

  CURRENCY_FORMATS = {
    "USD" => :no_cents_if_whole,
    "JPY" => :no_cents,
    "TWD" => :no_cents,
  }

  def money(usd_in_cents, currency_code = nil)
    currency_code ||= location_currency_code
    converted = Billing::Money.new(usd_in_cents).exchange_to(currency_code)
    if currency_code == "USD" || converted.currency.symbol != "$"
      converted.format(format_opts(currency_code))
    else
      "#{currency_code} #{converted.format(format_opts(currency_code))}"
    end
  end

  def format_opts(currency_code = nil)
    currency_code ||= location_currency_code
    CURRENCY_FORMATS[currency_code].nil? ? {} : { CURRENCY_FORMATS[currency_code] => true }
  end

  def location_currency_code
    country_code = GitHub::Location.look_up(request.remote_ip)[:country_code]
    GitHub::Billing::Currency.currency_of(country_code)
  end

  def usd?
    location_currency_code == "USD"
  end

  def show_currency?
    !usd?
  end

  def price_with_localization(price)
    content_tag(:span, money(price, "USD"), class: "default-currency") +
    content_tag(:span, money(price), class: "local-currency")
  end
end
