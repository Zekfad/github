# frozen_string_literal: true

module DashboardAnalyticsHelper
  GOOGLE_ANALYTICS_DIMENSION_DELIMITER = ";"
  OCTOLYTICS_DIMENSION_DELIMITER = ","

  module Dashboard::EventContext
    NEW_USER_BANNER = :NEW_USER_BANNER
    EMPTY_FEED = :EMPTY_FEED
    ORGANIZATIONS = :ORGANIZATIONS
    DISCOVER_REPOSITORIES = :DISCOVER_REPOSITORIES

    module Sidebar
      REPOSITORIES     = :REPOSITORIES
      TEAMS            = :TEAMS
      RECENT_ACTIVITY  = :RECENT_ACTIVITY
      ACCOUNT_SWITCHER = :ACCOUNT_SWITCHER
      CONTRIBUTED_REPOSITORIES = :CONTRIBUTED_REPOSITORIES
      BROADCAST = :BROADCAST
    end
  end

  # Internal: Log dashboard.discovery_feed event
  #
  # user - a User
  # analytics_hash - a Hash of additional tracking dimensions (default: {})
  #   currently_starred_repos_count - number of repos starred by the viewer
  #   currently_following_users_count - number of users viewer is following
  #   events_shown: true if any feed events are shown
  #   banner_shown: true if a top level banner is shown
  #   banner_dismissable: true if a top level banner is shown and dismissable
  #   page_type: the page type ("activity" or "discover")
  #
  # Returns nothing
  def instrument_dashboard_page_view(user, payload:)
    GlobalInstrumenter.instrument("dashboard.page_view", payload.merge(
      dashboard_version: dashboard_version,
      user: user,
    ))
  end

  # Internal: Formats ga and octolytics attributes for front-end analytics event tracking
  #
  # user - a User
  # event_name - top level event name
  # analytics_hash - a Hash of additional tracking dimensions
  # action - the action that we're tracking
  #
  # Returns a Hash
  def dashboard_data_attributes(user, event_name:, analytics_hash:, action:)
    return {} if atom_feed?

    hash = analytics_hash.dup
    repo_id = hash.delete(:repository_id) # report repo_id to octolytics, but not GA

    octolytics_dimensions = default_octolytics_dimensions(user: user, action: action)
    octolytics_dimensions = octolytics_dimensions.map { |key, value| "#{key}:#{value}" }
    octolytics_dimensions = octolytics_dimensions.concat(["repository_id:#{repo_id}"]) if repo_id

    shared_dimensions = hash.map { |key, value| "#{key}:#{value}" }

    {
      "ga-click" => "#{event_name}, #{shared_dimensions.join(GOOGLE_ANALYTICS_DIMENSION_DELIMITER)}",
      "octo-click" => event_name,
      "octo-dimensions" => octolytics_dimensions.
        concat(shared_dimensions).
        join(OCTOLYTICS_DIMENSION_DELIMITER),
    }
  end

  # Set up data attributes for analytics (ga, octolytics, and hydro)
  #
  # action - the action that we're tracking
  #
  # Returns a Hash
  def dashboard_activity_feed_analytics(analytics_dimensions:, action:)
    dashboard_data_attributes(
      current_user,
      event_name: "activity_feed",
      analytics_hash: analytics_dimensions,
      action: action,
    )
  end

  # Provide a wrapper around the standard dashboard parameters sent to hydro for analytics
  #
  # event_context - symbol representing the context for the event to be sent to hydro
  # target -  symbol representing the event
  #
  # Returns a Hash
  def dashboard_hydro_click_tracking_attributes(event_context:, target:)
    context = current_context.class.to_s.downcase

    hydro_click_tracking_attributes(
      "dashboard.click",
      event_context: event_context,
      dashboard_context: context,
      dashboard_version: dashboard_version,
      target: target,
      user_id: current_user&.id,
    )
  end

  def dashboard_empty_feed_watch_data_attributes(analytics_dimensions)
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::EMPTY_FEED,
      target: :WATCH,
    )

    dashboard_activity_feed_analytics(
      analytics_dimensions: analytics_dimensions,
      action: "banner-click-watch",
    ).merge(hydro_attributes)
  end

  def dashboard_empty_feed_follow_data_attributes(analytics_dimensions)
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::EMPTY_FEED,
      target: :FOLLOW,
    )

    dashboard_activity_feed_analytics(
      analytics_dimensions: analytics_dimensions,
      action: "banner-click-follow",
    ).merge(hydro_attributes)
  end

  def dashboard_empty_feed_explore_data_attributes(analytics_dimensions)
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::EMPTY_FEED,
      target: :EXPLORE,
    )

    dashboard_activity_feed_analytics(
      analytics_dimensions: analytics_dimensions,
      action: "banner-click-explore",
    ).merge(hydro_attributes)
  end

  def dashboard_empty_feed_student_pack_data_attributes(analytics_dimensions)
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::EMPTY_FEED,
      target: :STUDENT_DEVELOPER_PACK,
    )

    dashboard_activity_feed_analytics(
      analytics_dimensions: analytics_dimensions,
      action: "banner-click-student-developer-pack",
    ).merge(hydro_attributes)
  end

  def dashboard_repositories_org_profile_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::Sidebar::REPOSITORIES,
      target: :ORG_PROFILE,
    )

    {"ga-click" => "Dashboard, click, Nav menu - item:org-profile context:organization"}
      .merge(hydro_attributes)
  end

  def dashboard_repositories_create_repo_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::Sidebar::REPOSITORIES,
      target: :CREATE_REPO,
    )

    {"ga-click" =>"Dashboard, click, Sidebar blankslate new repository link - context:organization"}
      .merge(hydro_attributes)
  end

  def dashboard_repositories_learn_more_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::Sidebar::REPOSITORIES,
      target: :LEARN_MORE,
    )

    {"ga-click" => "Dashboard, click, Sidebar blankslate help link - context:organization"}
      .merge(hydro_attributes)
  end

  def organization_create_repo_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::ORGANIZATIONS,
      target: :CREATE_REPO,
    )

    {"ga-click" => "Dashboard, click, Welcome link:create-repo context:organization role:owner"}
      .merge(hydro_attributes)
  end

  def organization_view_team_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::ORGANIZATIONS,
      target: :VIEW_TEAM,
    )

    {"ga-click" => "Dashboard, click, Welcome link:view-teams context:organization role:owner"}
      .merge(hydro_attributes)
  end

  def organization_see_owners_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::ORGANIZATIONS,
      target: :SEE_OWNERS,
    )

    {"ga-click" => "Dashboard, click, Welcome link:view-owners context:organization role:owner"}
      .merge(hydro_attributes)
  end

  def organization_edit_settings_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::ORGANIZATIONS,
      target: :EDIT_SETTINGS,
    )

    {"ga-click" => "Dashboard, click, Welcome link:org-settings context:organization role:owner"}
      .merge(hydro_attributes)
  end

  def organization_return_personal_dash_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::ORGANIZATIONS,
      target: :RETURN_PERSONAL_DASH,
    )

    {"ga-click" => "Dashboard, click, Welcome link:user-dashboard context:organization role:owner"}
      .merge(hydro_attributes)
  end

  def organization_learn_more_data_attributes
    hydro_attributes = dashboard_hydro_click_tracking_attributes(
      event_context: Dashboard::EventContext::ORGANIZATIONS,
      target: :LEARN_MORE,
    )

    {"ga-click" => "Dashboard, click, Welcome link:learn-more context:organization role:owner"}
      .merge(hydro_attributes)
  end

  # Internal: Default octolytics dimensions
  #
  # user - a User
  # action - the action we're tracking
  #
  # Returns an Array
  def octolytics_dimensions(user:, action:)
    dimensions = default_octolytics_dimensions(user: user, action: action)
    dimensions.map { |key, value| "#{key}:#{value}" }
  end

  # Internal: Default octolytics dimensions as a Hash
  #
  # user - a User
  # action - the action we're tracking
  #
  # Returns a Hash
  def default_octolytics_dimensions(user:, action:)
    {
      actor_id: user.id,
      action: action,
    }
  end

  def discover_repositories_explore_attributes
    context = current_context.class.to_s.downcase
    hydro_click_tracking_attributes(
      "dashboard.click",
      event_context: Dashboard::EventContext::DISCOVER_REPOSITORIES,
      target: :EXPLORE,
      dashboard_context: context,
      dashboard_version: dashboard_version,
      user_id: current_user&.id,
    ).merge(
      "ga-click": "Explore, go to Discover, location:dashboard;context:#{context}",
    )
  end

  def discover_repositories_attributes(repo_id)
    context = current_context.class.to_s.downcase
    hydro_click_tracking_attributes(
      "dashboard.click",
      event_context: Dashboard::EventContext::DISCOVER_REPOSITORIES,
      target: :REPOSITORY,
      record_id: repo_id,
      dashboard_context: context,
      dashboard_version: dashboard_version,
      user_id: current_user&.id,
    ).merge(
      "ga-click": "Explore, go to repository, location:dashboard;context:#{context}",
    )
  end

  def sidebar_repository_attributes(repo: nil, button: false)
    context = current_context.class.to_s.downcase
    if button
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::REPOSITORIES,
        target: :NEW_REPOSITORY_BUTTON,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Sidebar header new repo button - context:#{context}",
      )
    elsif repo
      visibility = repo[:private] ? "private" : "public"
      repo_id = Platform::Helpers::NodeIdentification.from_global_id(repo[:id]).last&.to_i
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::REPOSITORIES,
        target: :REPOSITORY,
        record_id: repo_id,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Repo list item click - context:#{context} visibility:#{visibility} fork:#{repo[:fork]}",
      )
    else
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::REPOSITORIES,
        target: :SEE_MORE,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Ajax more repos link - context:#{context}",
      )
    end
  end

  def sidebar_team_attributes(team: nil)
    context = current_context.class.to_s.downcase
    if team
      team_id = Platform::Helpers::NodeIdentification.from_global_id(team[:id]).last&.to_i
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::TEAMS,
        target: :TEAM,
        record_id: team_id,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Team list item click - context:#{context}",
      )
    else
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::TEAMS,
        target: :SEE_MORE,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Ajax more teams link - context:#{context}",
      )
    end
  end

  def sidebar_recent_interaction_attributes(type: nil, reason: nil, id: nil)
    context = current_context.class.to_s.downcase
    if type && reason && id
      record_id = Platform::Helpers::NodeIdentification.from_global_id(id).last&.to_i
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::RECENT_ACTIVITY,
        target: type.sub(" ", "_").upcase.to_sym,
        record_id: record_id,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Recent activity list item click - interaction:#{reason.downcase} type:#{type} context:#{context}",
      )
    else
      hydro_click_tracking_attributes("dashboard.click",
        event_context: Dashboard::EventContext::Sidebar::RECENT_ACTIVITY,
        target: :SEE_MORE,
        dashboard_context: context,
        dashboard_version: dashboard_version,
        user_id: current_user&.id,
      ).merge(
        "ga-click": "Dashboard, click, Ajax more recent activity link - context:#{context}",
      )
    end
  end

  def sidebar_account_switcher_attributes(target: nil, to: nil, from: nil)
    if target
      context = current_context.class.to_s.downcase
      { "ga-click": "Dashboard, click, #{target} - context:#{context}" }
    else
      to_class = to.class.to_s.downcase
      from_class = from.class.to_s.downcase
      { "ga-click": "Dashboard, switch context, Switch dashboard context from:#{from_class} to:#{to_class}" }
    end
  end

  def dashboard_hello_world_banner_data_attributes(target)
    context = current_context.class.to_s.downcase

    hydro_click_tracking_attributes("dashboard.click",
      event_context: Dashboard::EventContext::NEW_USER_BANNER,
      dashboard_context: context,
      dashboard_version: dashboard_version,
      target: target,
      user_id: current_user&.id,
    )
  end

  def dashboard_hello_world_banner_dismiss_data_attributes
    dashboard_hello_world_banner_data_attributes(:DISMISS_BANNER).merge(
      "ga-click": "Hello World, click, Dismissed Hello World",
      "ga-load": "Hello World, linkview, Viewed Hello World",
    )
  end

  def dashboard_hello_world_banner_start_project_data_attributes
    dashboard_hello_world_banner_data_attributes(:START_PROJECT).merge(
      "ga-click": "Hello World, click, Clicked new repository button - context:user",
    )
  end

  def dashboard_hello_world_banner_read_guide_data_attributes
    dashboard_hello_world_banner_data_attributes(:READ_GUIDE).merge(
      "ga-click": "Hello World, click, Clicked Let's get started button",
    )
  end

  def dashboard_version
    2
  end
end
