# frozen_string_literal: true

module ExperimentHelper
  include FeatureFlagHelper

  class InvalidActorForOptimizelyError < StandardError; end

  # This method is a helper for typical Optimizely experiments.
  # There is a feature flag bypass that will force a true return by checking for a feature flag of the same name as your experiment and variation
  #   - This is for testing purposes as an easier way to ensure we can check different flows.
  #
  # experiment: String
  #   the name of the experiment key as it is in Optimizely
  #
  # actor: Object
  #   the actor to be enrolled. For Optimizely the actor must respond to analytics_tracking_id. Typically a User/Organization.
  #
  # enabled_variations: Array
  #   the experiment variations that you are checking the actors enrollment against
  #
  # override_flag_actor: Object - optional
  #   If used this is the object that will be checked against feature flags instead of the actor
  #     optimizely_experiment_enabled?(experiment:, actor:, variation:, override_flag_actor: nil)
  #
  # Example:
  # An experiment named "best_optimizely_experiment" with two variations: "alternative" and "control"
  #  optimizely_experiment_enabled?(
  #    experiment: "best_optimizely_experiment",
  #    actor: some_user,
  #    variation: ["alternative"]
  #  )
  #
  # The experiment force flag would be "best_optimizely_experiment_as_alternative"
  # We would check some_user enrolled variation against "alternative"
  def optimizely_experiment_enabled?(experiment:, actor:, user_attributes: {}, enabled_variations: [], override_flag_actor: nil)
    raise InvalidActorForOptimizelyError if !actor.respond_to?(:analytics_tracking_id)

    return true if enabled_variations.any? do |variation|
      feature_enabled_for_user_or_current_visitor?(feature_name: "#{experiment}_as_#{variation}", subject: override_flag_actor || actor)
    end

    variation = GitHub.optimizely.activate(experiment, actor.analytics_tracking_id, user_attributes)

    enabled_variations.include?(variation)
  end
end
