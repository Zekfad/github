# frozen_string_literal: true

module NotificationsHelper
  include PlatformHelper

  def paginate_subscriptions(total_count, options = {})
    raw_paginate total_count.to_i, NotificationsController::SUBSCRIPTIONS_PER_PAGE, subscriptions_page, options
  end

  OcticonFragment = parse_query <<-"GRAPHQL"
    fragment Subject on NotificationsSubject {
      ... on Issue {
        state
      }
      ... on PullRequest {
        state
        isDraft
      }
      ... on CheckSuite {
        conclusion
      }
      ... on RepositoryAdvisory {
        state
      }
      ... on AdvisoryCredit {
        state
      }
    }
  GRAPHQL

  def graphql_notification_octicon_for(subject, classes: "")
    obj = OcticonFragment::Subject.new(subject)

    # This intentionally omits PlatformTypes::CheckSuite.
    # See https://github.com/github/github/issues/122642 for details.
    case obj
    when PlatformTypes::Commit
      octicon("git-commit", class: classes)
    when PlatformTypes::Issue
      case obj.state
      when "OPEN"
        octicon("issue-opened", class: classes + " text-green")
      when "CLOSED"
        octicon("issue-closed", class: classes + " text-red")
      end
    when PlatformTypes::PullRequest
      icon = obj.state == "MERGED" ? "git-merge" : "git-pull-request"
      color = graphql_notification_icon_color_for(obj.state, obj.is_draft)
      octicon(icon, class: classes + " #{color}")
    when PlatformTypes::Release
      octicon("tag", class: classes)
    when PlatformTypes::RepositoryInvitation
      octicon("mail", class: classes)
    when PlatformTypes::RepositoryVulnerabilityAlert
      octicon("alert", class: classes)
    when PlatformTypes::TeamDiscussion, PlatformTypes::Discussion
      octicon("comment-discussion", class: classes)
    when PlatformTypes::RepositoryAdvisory
      icon =
        case obj.state
        when "CLOSED" then "shield-x"
        when "PUBLISHED" then "shield-check"
        else "shield"
        end
      color = graphql_notification_icon_color_for(obj.state, obj.state == "OPEN")
      octicon(icon, class: "#{classes} #{color}")
    when PlatformTypes::AdvisoryCredit
      color = graphql_notification_icon_color_for(obj.state, false)
      octicon("shield", class: "#{classes} #{color}")
    when PlatformTypes::Gist
      octicon("code", class: classes)
    end
  end

  def graphql_notification_icon_color_for(state, is_draft = false)
    case state
    when "OPEN", "ACCEPTED", "PUBLISHED"
      if is_draft
        "text-gray-light"
      else
        "text-green"
      end
    when "CLOSED", "DECLINED"
      "text-red"
    when "MERGED"
      "text-purple"
    when "PENDING"
      "text-yellow"
    end
  end

  def graphql_notification_icon_state_for(state, is_draft = false)
    case state
    when "OPEN"
      if is_draft
        "type-icon-state-draft"
      else
        "type-icon-state-open"
      end
    when "CLOSED"
      "type-icon-state-closed"
    when "MERGED"
      "type-icon-state-merged"
    when "PUBLISHED"
      "type-icon-state-published"
    else
      "type-icon-state-none"
    end
  end

  def notification_reason_label(enum_reason)
    case enum_reason
    when "ASSIGN"
      "assigned"
    when "COMMENT"
      "commented"
    else
      enum_reason.humanize(capitalize: false)
    end
  end

  def display_list_name(list)
    prefix = "@" if list.class.name.downcase == "team"
    "#{prefix}#{list.name_with_owner}"
  end
end
