# frozen_string_literal: true

module UserDashboardPinsHelper
  include UrlHelper
  include PlatformHelper

  GistTitleFragment = parse_query <<~'GRAPHQL'
    fragment Gist on Gist {
      description
      sha
      name
      files(limit: 1) {
        name
      }
    }
  GRAPHQL

  def gist_title(gist)
    gist = GistTitleFragment::Gist.new(gist)
    gist.description.presence || gist.files.first&.name.presence || gist.sha || gist.name
  end

  def dashboard_pin_form_method
    if item_is_pinned_to_dashboard?
      :delete
    else
      :post
    end
  end

  def dashboard_pin_url_for(user, pinned_item_id, is_gist:)
    if is_gist
      user_dashboard_gist_pin_path(user, pinned_item_id)
    else
      user_dashboard_item_pin_path(user, pinned_item_id)
    end
  end

  def dashboard_pin_url
    dashboard_pin_url_for(current_user, item_to_pin.global_relay_id,
                          is_gist: item_to_pin.is_a?(Gist))
  end

  def pinning_button_text
    if item_is_pinned_to_dashboard?
      "Unpin from dashboard"
    else
      "Pin to dashboard"
    end
  end

  def item_to_pin_to_dashboard?
    !!item_to_pin
  end

  def item_to_pin
    return @item_to_pin if instance_variable_defined?(:@item_to_pin)

    @item_to_pin = identify_item_by_global_relay_id if dashboard_pinnable_item_id
  end

  def item_is_pinned_to_dashboard?
    return false unless user_dashboard_pins_enabled?
    return false unless logged_in?

    UserDashboardPin.pinned_by_user(current_user).for_item(item_to_pin, item_to_pin.class.name).any?
  end

  def identify_item_by_global_relay_id
    pinned_item_type, item_id = Platform::Helpers::NodeIdentification.from_global_id(dashboard_pinnable_item_id)

    if pinned_item_type == "Gist"
      pinned_item_type.constantize.find_by(repo_name: item_id)
    else
      pinned_item_type.constantize.find(item_id)
    end
  end
end
