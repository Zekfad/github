# frozen_string_literal: true

module Orgs::TeamDiscussionCommentsHelper
  # Internal: Returns the value that should be used for the `id` attribute of
  # the top-level DOM element used to display a TeamDiscussionComment or
  # OrganizationDiscussionComment GraphQL object.
  #
  # Returns: a String.
  def team_discussion_comment_dom_id(discussion_number:, comment_number:)
    "discussion-#{discussion_number}-comment-#{comment_number}"
  end
end
