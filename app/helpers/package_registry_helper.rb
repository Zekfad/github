# frozen_string_literal: true

module PackageRegistryHelper

  # Internal: Indicates whether package registry is enabled. Caches return value for 10 sec.
  def self.show_packages?
    return true unless GitHub.enterprise?
    return false unless GitHub.registry_enabled_for_enterprise?

    key = "repository:packages:global_availability"
    GitHub.cache.fetch(key, ttl: 10.seconds) do
      Configurable::PackageAvailability.package_availability != Configurable::PackageAvailability::DISABLED
    end
  end

  # Returns true is this is an enterprise, and the registry feature flag is enabled
  def self.ghes_registry_enabled?
    GitHub.enterprise? && GitHub.registry_enabled_for_enterprise?
  end
end
