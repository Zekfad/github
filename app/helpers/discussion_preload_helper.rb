# frozen_string_literal: true

# Hacky module for managing preloaded timeline data while discussion timelines
# are being refactored into a single GraphQL query.
module DiscussionPreloadHelper
  include PlatformHelper
  include PlatformPreloadHelper

  DiscussionItemsQuery = parse_query <<-'GRAPHQL'
    query($ids: [ID!]!) {
      nodes(ids: $ids) {
        ...PlatformPreloadHelper::BaseFragment

        ...Views::Commit::TimelineMarker::Comment
        ...Views::Comments::ReviewComment::Comment
        ...Views::Gists::Gists::TimelineMarker::GistComment
        ...Views::Diff::ReviewThread::ReviewThread
      }
    }
  GRAPHQL

  MobileDiscussionItemsQuery = parse_query <<-'GRAPHQL'
    query($ids: [ID!]!) {
      nodes(ids: $ids) {
        ...PlatformPreloadHelper::BaseFragment

        ...Views::Mobile::Discussions::Reactions::Subject
        ...Views::Mobile::Discussions::Comment
        ...Views::Mobile::Discussions::CommitCommentThread
        ...Views::Mobile::Discussions::PullRequestReviewThread
        ...Views::Mobile::Discussions::TimelineReviewThread::MobileTimelineReviewThread
        ...Views::Mobile::Diff::ReviewThread::MobileTimelineReviewThread
      }
    }
  GRAPHQL

  # Public: load and cache the platform representations of the given objects
  #
  # This will execute a platform query to get the requested platform objects
  # and will cache the results by their global id.
  #
  # groups - an array of timeline items or arrays of timeline items
  def preload_discussion_group_data(groups, mobile: false)
    subjects = groups.flat_map do |group|
      if group.is_a?(PullRequestReview)
        if GitHub.flipper[:use_review_thread_ar_model].enabled?(group.repository)
          [group].concat(group.review_threads)
        else
          [group].concat(group.threads)
        end

      elsif group.is_a?(DeprecatedPullRequestReviewThread)
        if GitHub.flipper[:use_review_thread_ar_model].enabled?(group.repository)
          group.activerecord_pull_request_review_thread
        else
          group
        end
      elsif group.is_a?(CommitCommentThread) && @pull
        # NOTE: temporary hack to determine we're on the PR conversation tab
        Platform::Models::PullRequestCommitCommentThread.new(@pull, group.repository.id, group.commit_id, group.path, group.position)
      elsif group.respond_to?(:timeline_children)
        group.timeline_children
      else
        group
      end
    end

    preload_platform_objects(subjects) do |ids|
      if mobile
        platform_execute(MobileDiscussionItemsQuery,
          variables: { ids: ids },
        ).nodes
      else
        platform_execute(DiscussionItemsQuery,
          variables: {
            ids: ids,
            syntaxHighlightingEnabled: syntax_highlighted_diffs_enabled?,
            hasFocusedReviewComment: false,
          },
        ).nodes
      end
    end
  end
end
