# frozen_string_literal: true

module UserContributionsHelper
  # Public: Should the 'Activity overview' section be shown on this user's profile?
  def activity_overview_enabled?
    return @activity_overview_enabled if defined? @activity_overview_enabled

    profile_settings = this_user.profile_settings
    @activity_overview_enabled = profile_settings.activity_overview_enabled?
  end

  def activity_overview_variables
    variables = {
      id: this_user.global_relay_id,
      selected_organization_login: scoped_organization&.login || "",
      show_teams: show_teams?,
      logged_in: logged_in?,
    }

    # By default we want to expand the dates given in :to and :from to a range spanning
    # the entire year, but when a square is clicked on in the graph we want to use the exact
    # dates given. The graph passes an exact_date=1 param when this happens
    #
    if params["exact_date"] == "1"
      set_graphql_exact_date_params_on(variables)
    else
      set_graphql_year_date_params_on(variables)
    end

    if activity_overview_enabled? && scoped_organization
      set_hovercard_subject(scoped_organization)
      variables[:selected_organization_id] = scoped_organization.global_relay_id
      variables[:organization_selected] = true
    else
      variables[:organization_selected] = false
    end
    variables
  end

  def show_teams?
    # Don't show teams when a particular organization filter has not been applied
    return false unless scoped_organization

    now = Time.zone.now
    cutoff = 366.days

    # Ensure the date range is for a recent period, so the teams the user currently
    # belongs to should be relevant
    if params[:from].present?
      from = parse_date_from(params[:from])
      return false if from && from <= now && (now - from) > cutoff
    end

    if params[:to].present?
      to = parse_date_from(params[:to])
      return false if to && to <= now && (now - to) > cutoff
    end

    true
  end

  def this_user
    return @user if defined?(@user)
    @user = User.find_by_login(params[:user_id]) if (params[:user_id] && GitHub::UTF8.valid_unicode3?(params[:user_id]))
  end

  def scoped_organization
    return @scoped_organization if defined? @scoped_organization

    org_login = params[:org]
    @scoped_organization = org_login && Organization.find_by_login(org_login)
  end

  def set_graphql_exact_date_params_on(variables)
    if params[:from].present? && from_date = parse_date_from(params[:from])
      # Defaults to beginning of the day when a date is passed as 'from', e.g., '2018-07-30'.
      variables[:from] = from_date.iso8601
    end

    if params[:to].present? && to_date = parse_date_from(params[:to])
      # If 'to' was passed as just today's date, e.g., '2018-07-30', go ahead and include any
      # contributions made today as well by bumping 'to' to the current time. This matches the
      # default 'to' in `Platform::Loaders::ContributionCollector`.
      now = Time.zone.now
      to_date = now if to_date == now.beginning_of_day

      variables[:to] = to_date.iso8601
    end
  end

  def set_graphql_year_date_params_on(variables)
    base_from_date = params[:from].present? && parse_date_from(params[:from])
    base_to_date = params[:to].present? && parse_date_from(params[:to])
    return unless base_date = base_to_date || base_from_date

    variables[:from] = base_date.beginning_of_year.iso8601
    variables[:to] = base_date.end_of_year.iso8601
  end

  def month_date_params
    from = parse_date_from(params[:from]) if params[:from].present?
    to = parse_date_from(params[:to]) if params[:to].present?

    if from.nil? && to.nil?
      to = Time.zone.now
      from = to.beginning_of_month
    else
      to ||= from
      from ||= to
    end

    to = [to, from + 31.days].min

    from = from.beginning_of_day
    to = to.end_of_day

    [from, to]
  end

  def set_graphql_month_date_params_on(variables)
    from, to = month_date_params
    variables[:timelineFrom] = from.iso8601
    variables[:timelineTo] = to.iso8601
  end

  # Public: Parse a date string into a time in the current time zone.
  #
  # date_attr - a String like "2018-09-27" in the format YYYY-MM-DD
  #
  # Returns a Time or nil.
  def parse_date_from(raw_date_str)
    date_str = (raw_date_str || "")[0..9]
    return unless date_str =~ /\d{4}-\d{2}-\d{2}/

    begin
      Time.zone.parse(date_str)
    rescue ArgumentError
      nil
    end
  end
end
