# frozen_string_literal: true

# Helpers for User/Organization/Business membership in things like the pre-release and
# developer programs.
module AccountMembershipHelper

  # Public: Array of enterprise accounts the current_user has admin access to.
  def adminable_businesses
    return [] unless logged_in?

    @adminable_businesses ||= current_user.businesses(membership_type: :admin)
  end

  # Public: Array of org accounts the current_user has admin access to.
  def adminable_organizations
    return [] unless logged_in?

    @adminable_organizations ||= current_user.organizations.select do |org|
      org.adminable_by?(current_user)
    end
  end

  # Public: Array of user/org/enterprise accounts the current_user has admin
  # access to.
  #
  # include_businesses - Whether adminable enterprise accounts should be included.
  #                      Defaults to true. If your code is not ready to handle
  #                      enterprise accounts, pass `include_businesses: false`
  #                      instead.
  #
  # Returns an Array of account objects.
  def adminable_accounts(include_businesses: true)
    return [] unless logged_in?
    return adminable_accounts_without_businesses unless include_businesses

    @adminable_accounts ||= adminable_accounts_without_businesses + adminable_businesses
  end

  # Public: Array of user/org accounts the current_user has admin
  # access to.
  #
  # Returns an Array of account objects.
  def adminable_accounts_without_businesses
    return [] unless logged_in?

    @adminable_accounts_without_businesses ||= begin
      accounts = adminable_organizations
      accounts.unshift current_user
      accounts
    end
  end

  # Public: Current User, Organization, or Business being used for membership.
  def account
    @account ||= find_account
  end

  # Public: Find the user/org/business to grant membership.
  #
  # Accounts are found with this priority:
  # - The user_id (and optionally member_type) params are checked to find a
  #   user/org/business.
  # - The account param is checked for a user/org login.
  # - The business param is checked for a business slug.
  #
  # Returns a User, Organization, or Business depending on which type of
  # account is found.
  def find_account
    account = if params[:member_type].present?
      case params[:member_type]
      when "Business"
        Business.find_by(id: params[:user_id])
      when "User"
        User.find_by(id: params[:user_id])
      end
    else
      User.find_by(id: params[:user_id])
    end

    unless account
      account = User.find_by(login: params[:account]) ||
        Business.find_by(slug: params[:business])
    end

    if account && adminable_accounts.include?(account)
      account
    else
      current_user
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless account.present?

    # TEMPORARY SECURITY BYPASS: see https://github.com/github/github/pull/149329#issuecomment-658181097
    return :no_target_for_conditional_access  if account.instance_of?(Business)

    account
  end
end
