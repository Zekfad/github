# frozen_string_literal: true

module SvgHelper
  def svg(*args)
    inline_svg(*args)
  end
end
