# frozen_string_literal: true

module SudoHelper
  # https://github.com/brianhempel/hash_to_hidden_fields
  def hash_to_hidden_fields(hash)
    cleaned_hash = hash.reject { |k, v| v.nil? }
    pairs        = cleaned_hash.to_query.split(Rack::Utils::DEFAULT_SEP)

    tags = pairs.map do |pair|
      key, value = pair.split("=", 2).map { |str| Rack::Utils.unescape(str) }
      hidden_field_tag(key, value)
    end

    safe_join(tags, "\n")
  end

  def sudo_method
    # We don't want to submit creds in a get request
    if request.get?
      :post
    else
      request.request_method_symbol
    end
  end

  def sudo_action
    request.get? ? sudo_url : request.path
  end

  def redirect_params
    p = request.parameters.dup
    p.delete "action"
    p.delete "controller"
    p.delete "_method"
    p.delete "authenticity_token"
    p.delete "sudo_password"
    p.delete "u2f_response"

    # We don't want to submit the user's credentials in a GET request,
    # but it wont work to POST them to a GET endpoint. If the original
    # request was a GET, then we backup the original path
    # and have the credentials POST to 'sessions/sudo'. That endpoint
    # then redirects them to their original destination.
    p["sudo_return_to"] = request.url if request.get?
    p["sudo_referrer"] = params[:sudo_referrer] || request.referrer

    p
  end

  # Is U2F an option for sudo mode?
  #
  # Returns boolean.
  def sudo_u2f_available?
    security_key_enabled_for_request? && logged_in? && current_user.has_registered_security_key?
  end
end
