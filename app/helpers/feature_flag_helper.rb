# frozen_string_literal: true

module FeatureFlagHelper
  def feature_enabled_for_current_user?(feature_name:, subject: nil)
    if subject
      GitHub.flipper[feature_name].enabled?(subject)
    else
      logged_in? && GitHub.flipper[feature_name].enabled?(current_user)
    end
  end

  def feature_enabled_for_current_visitor?(feature_name:)
    current_visitor && GitHub.flipper[feature_name].enabled?(User::CurrentVisitorActor.from_current_visitor(current_visitor.octolytics_id))
  end

  def feature_enabled_globally_or_for_user?(feature_name:, subject: nil)
    GitHub.flipper[feature_name].enabled? ||  feature_enabled_for_current_user?(feature_name: feature_name, subject: subject)
  end

  def feature_enabled_for_user_or_current_visitor?(feature_name:, subject: nil)
    return feature_enabled_globally_or_for_user?(feature_name: feature_name, subject: subject) if subject

    feature_enabled_globally_or_for_user?(feature_name: feature_name, subject: subject) || feature_enabled_for_current_visitor?(feature_name: feature_name)
  end
end
