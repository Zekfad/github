# frozen_string_literal: true

module BotHelper
  include ActionView::Helpers::TagHelper

  def bot_identifier(actor)
    return unless actor.is_a?(Bot) || actor.is_a?(PlatformTypes::Bot)
    bot_identifier_tag
  end

  def bot_identifier_tag
    content_tag(:span, "bot", class: "Label Label--outline")
  end
end
