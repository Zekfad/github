# frozen_string_literal: true

require "graphql/client"

module PlatformHelper
  class Error < StandardError; end
  class ExecutionError < Error; end
  class MutationInputError < Error; end
  class InvalidCursorError < Error; end
  class ConditionalAccessError < Error; end
  class SamlError < ConditionalAccessError; end
  class IpAllowListError < ConditionalAccessError; end

  class PlatformExecute
    def self.execute(document:, operation_name: nil, variables: {}, context: {})
      # Validate in development & test, but skip validation in production because speed
      validate = !Rails.env.production?
      Platform.execute(
        document,
        target: :internal,
        context: context,
        variables: variables,
        raise_exceptions: true,
        validate: validate,
      ).to_h
    end
  end

  def self.included(base)
    base.instance_eval do
      def parse_query(str)
        location = caller_locations(1, 1).first
        PlatformHelper::PlatformClient.parse(str, location.path, location.lineno)
      end
    end
  end

  PlatformClient = GraphQL::Client.new(
    schema: Platform::Schema,
    execute: PlatformExecute,
    enforce_collocated_callers: ENV["FASTDEV"] ? false : (Rails.development? || Rails.test?),
  )

  MAX_PLATFORM_EXECUTE_CALLS = 6
  STAFF_BAR_CALLS = 1

  def platform_execute(operation, variables: {}, context: {})
    if Rails.development? || Rails.test?
      @platform_execute_calls ||= 0
      @platform_execute_calls += 1

      max_calls = MAX_PLATFORM_EXECUTE_CALLS
      max_calls += STAFF_BAR_CALLS if (respond_to?(:staff_bar_enabled?) && staff_bar_enabled?)

      if @platform_execute_calls > max_calls
       fail "Too many platform_execute calls"
      end
    end

    response = PlatformHelper::PlatformClient.query(operation, context: platform_context.merge(context), variables: variables)

    response.errors[:data].each do |message|
      case message
      when /Variable \$input/
        raise MutationInputError, message
      when /valid cursor/
        # A malformed cursor was entered to the UI (maybe user-generated).
        # It was posted to datadog by Errors::Cursor#initialize.
        Failbot.push(app: "github-invalid-cursor")
        raise InvalidCursorError, message
      else
        raise ExecutionError, message
      end
    end

    response.errors.all[:data].each do |message|
      case message
      when /IP not allowed/
        raise IpAllowListError, message
      when /saml error/
        raise SamlError, message
      end
    end

    response.data
  end

  def platform_context
    {
      origin: Platform::ORIGIN_INTERNAL,
      viewer: current_user,
      log_data: log_data,
      session: session,
      rails_request: request,
      force_readonly_primary: !(request.get? || request.head?),
      controller: controller_name,
      action: action_name,
      unauthorized_organization_ids: unauthorized_organization_ids,
      performance_trace: stats_ui_enabled? && params["graphql_query_trace"],
      performance_trace_field_profile_path: stats_ui_enabled? ? params["graphql_query_trace_profile_path"] : nil,
      performance_trace_field_profile_type: stats_ui_enabled? ? (params["graphql_query_trace_profile_kind"] == "lines" ? :lines : :allocations) : nil,
    }
  end

  def unauthorized_organization_ids
    (
      unauthorized_saml_organization_ids +
      unauthorized_ip_whitelisting_organization_ids
    ).uniq
  end

  def typed_object_from_id(possible_type_or_types, id)
    Platform::Security::RepositoryAccess.with_viewer(current_user) do
      permission    = Platform::Authorization::Permission.new(viewer: current_user)
      Platform::Helpers::NodeIdentification.typed_object_from_id(possible_type_or_types, id, permission: permission)
    end
  end

  # Our template precompiler can't handle recursive partials, so work around that
  def render_graphql_node(node)
    if node.is_a?(Platform::PerformancePaneTracer::DisplayPhase)
      render partial: "site/graphql_stats_phase", locals: { node: node }
    else
      render partial: "site/graphql_stats_step", locals: { node: node }
    end
  end

  def render_graphql_ms(milliseconds)
    "%.1fms" % milliseconds
  end

  # Public: Given a date string, will return an ISO-8601 DateTime. Can be used to
  # populate GraphQL DateTime fields from URL parameters.
  #
  # date_str - a String representing a date, e.g., "2017-03-15"
  #
  # Returns a String like "2017-03-15T00:00:00Z" or nil.
  def parse_graphql_datetime(date_str)
    return unless date_str

    # Some invalid date strings raise an exception, others return nil
    time = Time.zone.parse(date_str)
    time.iso8601 if time
  rescue ArgumentError
    nil
  end
end
