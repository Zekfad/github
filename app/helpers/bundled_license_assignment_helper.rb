# frozen_string_literal: true

module BundledLicenseAssignmentHelper
  def bundled_license_assignments_enabled?(actor)
    return @bundled_license_assignments_enabled if defined?(@bundled_license_assignments_enabled)

    @bundled_license_assignments_enabled =
      GitHub.flipper[:bundled_license_assignments].enabled? || GitHub.flipper[:bundled_license_assignments].enabled?(actor)
  end
end
