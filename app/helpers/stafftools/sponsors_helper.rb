# frozen_string_literal: true

module Stafftools
  module SponsorsHelper
    include PlatformHelper

    SponsorsListingStateBadge = parse_query <<-'GRAPHQL'
      fragment Listing on SponsorsListing {
        state
        isDraft
        hasFailedVerification
        isVerifiedState
        isApproved
        isUnverifiedAndPendingApproval
        isDisabled
      }
    GRAPHQL

    # Public: Create a Sponsor button for a sponsorable user for the current viewer.
    #
    # listing - A PlatformTypes::SponsorsListing from GraphQL.
    #
    # Returns HTML content.
    def graphql_sponsors_listing_state_badge_for(listing)
      return if listing.nil?
      listing = SponsorsListingStateBadge::Listing.new(listing)

      state_color = if listing.is_draft?
        "blue"
      elsif listing.has_failed_verification?
        "red"
      elsif listing.is_verified_state? || listing.is_approved?
        "green"
      elsif listing.is_unverified_and_pending_approval?
        "yellow"
      elsif listing.is_disabled?
        "red"
      else
        "purple"
      end

      state_octicon = if listing.is_draft?
        "pencil"
      elsif listing.has_failed_verification? || listing.is_disabled?
        "circle-slash"
      elsif listing.is_verified_state? || listing.is_approved?
        "check"
      else
        "eye"
      end

      span_class = "no-wrap bg-#{state_color}-light py-1 px-2 text-gray-dark d-inline-block v-align-middle h6 rounded-1 mr-1"

      content_tag(:span, class: span_class) do
        octicon(state_octicon, class: "mr-1 v-align-middle") + listing.state
      end
    end

    # Public: Create a state badge for a SponsorsListing.
    #
    # listing - A SponsorsListing to generate a badge for.
    #
    # Returns HTML content.
    def sponsors_listing_state_badge_for(listing)
      return if listing.nil?

      state_color = if listing.draft?
        "blue"
      elsif listing.failed_verification?
        "red"
      elsif listing.verified? || listing.approved?
        "green"
      elsif listing.unverified_and_pending_approval?
        "yellow"
      elsif listing.disabled?
        "red"
      else
        "purple"
      end

      state_octicon = if listing.draft?
        "pencil"
      elsif listing.failed_verification? || listing.disabled?
        "circle-slash"
      elsif listing.verified? || listing.approved?
        "check"
      else
        "eye"
      end

      span_class = "no-wrap bg-#{state_color}-light py-1 px-2 text-gray-dark d-inline-block v-align-middle h6 rounded-1 mr-1"

      content_tag(:span, class: span_class) do
        octicon(state_octicon, class: "mr-1 v-align-middle") + listing.current_state.name.to_s.humanize
      end
    end
  end
end
