# frozen_string_literal: true

require "cache_key_logging_blacklist"

module TreeHelper
  include GitHub::Encoding

  def deeper_tree_path(path, base_path = path_string_for_display)
    tree_path [base_path, path].join("/"), tree_name
  end

  def deeper_blob_path(path, base_path = path_string_for_display)
    blob_path [base_path, path].join("/"), tree_name
  end

  def up_tree_path
    tree_path current_path.parent, tree_name
  end

  def commit_tree_path(commit)
    tree_path path_string, commit
  end

  def content_type(content)
    if content.tree?
      :directory
    elsif content.submodule?
      :submodule
    elsif content.symlink?
      target = if content.respond_to?(:symlink_target_oid) && content.symlink_target_oid
        current_repository.rpc.read_objects([content.symlink_target_oid]).first
      end

      # everything that's not a file or another symlink, is a directory
      if target && target["type"] == "tree"
        :symlink_directory
      else
        :symlink_file
      end
    else
      :file
    end
  end

  def content_icon(content)
    icon = ""
    label = ""
    if content.tree?
      icon = "file-directory"
      label = "directory"
    elsif content.submodule?
      icon = "file-submodule"
      label = "submodule"
    elsif content.symlink?
      target = if content.respond_to?(:symlink_target_oid) && content.symlink_target_oid
        current_repository.rpc.read_objects([content.symlink_target_oid]).first
      end

      # everything that's not a file or another symlink, is a directory
      if target && target["type"] == "tree"
        icon = "file-symlink-directory"
        label = "symlink directory"
      else
        icon = "file-symlink-file"
        label = "symlink file"
      end
    end

    if icon.blank? && show_mobile_view?
      if content.formatted?
        icon = "book"
        label = "markup"
      elsif (mime_type = simple_mime_type(content.mime_type)) && mime_type != "unknown" && mime_type != "android" && mime_type != "text"
        icon = "file-#{mime_type}"
        label = mime_type
      elsif content.binary? || content.language && content.language.type == :data
        icon = "file-binary"
        label = "binary"
      end
    end

    icon = "file" if icon.blank?
    label = "file" if label.blank?
    octicon(icon, "aria-label": label)
  end

  # Public: Get the URL for some git content.
  #
  # content - Content to get the URL for. Should be a TreeEntry, but can also
  # be anything that responds to #to_s.
  #
  # Returns a string.
  def content_url(content, base_path = path_string)
    name = content.respond_to?(:name) ? content.name : content.to_s
    url  = nil

    if content.respond_to?(:blob?) && content.blob?
      url = deeper_blob_path(name, base_path)
    elsif content.respond_to?(:submodule?) && content.submodule?
      submodule = submodule_for(content)
      if submodule && submodule_linkable?(submodule)
        url = submodule_content_url(submodule)
      end
    else
      # Use tree path for everything else
      url = if content.respond_to?(:simplified_path?) && content.simplified_path?
        tree_path(content.simplified_path, tree_name)
      else
        deeper_tree_path(name, base_path)
      end
    end

    url
  end

  # Public: Return the commit count from the passed oid, by default it uses the current_repository,
  # however a repo can be passed in as well.
  #
  # No limit is passed to the RPC method, so it should always return
  # the correct number
  #
  # Returns a string.
  def limitless_commit_count(oid, repo = nil)
    repo ||= current_repository
    count = repo.rpc.fast_commit_count(oid, nil, timeout: 2)
    number_with_delimiter count
  rescue GitRPC::Timeout
    # It should never take this long to retrieve the "fast" commit count. Most likely the
    # bitmap for the repo is missing. It's not safe to automatically generate it here though
    # as an availability incident could trigger timeouts resulting in large numbers
    # of erroneous recalculation attempts. Log this failure so we can monitor it in aggregate.
    GitHub.dogstats.increment("fast_commit_count.timeout")
    "∞"
  end

  # Public: Get the HTML link for some git content.
  #
  # content - Content to get the URL for. Should be a TreeEntry, but can also
  # be anything that responds to #to_s.
  #
  # Returns a string.
  def content_link(content, base_path = path_string_for_display, classname: "")
    name = content.respond_to?(:display_name) ? content.display_name : content.to_s

    link_opts = {class: "js-navigation-open #{classname}", title: name}
    link_opts[:rel] = "nofollow" if tree_name == commit_sha

    if content.submodule?
      link = submodule_content_link(content)
    else
      # we need the binary version of path_string, so call that rather than the
      # display version
      url = content_url(content, path_string)

      if content.tree? || content.blob?
        name = content_path(content, base_path)
        if content.simplified_path?
          link_opts.merge!(id: "#{name.to_md5}-#{content.id}", title: "This path skips through empty directories")
        else
          link_opts[:id] = "#{name.to_md5}-#{content.id}"
          link_opts[:itemprop] = "license" if content.license?
        end
      else
        link_opts = {id: content.id}
      end

      link = link_to(name, url, link_opts)
    end

    link
  end

  # Public: Get the formatted path name for a link. For paths that can be
  # simplified, this path name will include `<span>` tags to denote greyed-out
  # text. This result can be passed into a link_to helper.
  #
  # Returns a UTF-8 safe String that has been scrubbed for bad bytes.
  def content_path(content, base_path = path_string_for_display)
    if (content.tree? || content.blob?) && content.simplified_path?
      name = "#{content.simplified_utf8_path.gsub(/^#{Regexp.escape(base_path)}\//, "")}"
      dirs = name.split("/")
      last_dir  = dirs.pop
      grey_block = content_tag(:span, safe_join([safe_join(dirs, "/"), "/"]), class: "text-gray-light")

      safe_join([grey_block, last_dir])
    else
      content.display_name
    end
  end

  def submodule_linkable?(submodule)
    submodule.gist || submodule.user && submodule.repo
  end

  def submodule_for(content)
    submodule_path = File.join(path_string, content.name)
    current_repository.submodule(commit_sha, submodule_path)
  end

  def submodule_content_link(content)
    submodule = submodule_for(content)

    if submodule
      title = "#{content.display_name} @ #{content.id[0..6]}"

      content = if submodule_linkable?(submodule)
        path = submodule_content_url(submodule)
        tree = submodule_content_url(submodule, content)

        link_to(title, tree, "data-skip-pjax" => true)
      else
        title
      end
      content_tag :span, content, title: title
    else
      # for some reason, no .gitmodules file was found,
      # or this submodule couldn't be found.
      h(content.display_name)
    end
  end

  # The readme for the current tree and path.
  #
  # Returns a Repository::PreferredReadme.
  def current_readme
    return @current_readme if defined?(@current_readme)

    @current_readme = current_directory.preferred_readme
  end

  # Find out whether or not there's a readme at the current tree and
  # path.
  #
  # Returns a boolean.
  def has_readme?
    current_readme.present?
  rescue => e
    failbot e
    false
  end

  # The extension of the readme, formatted as a class.
  #
  # Returns a String (blank if there's no README).
  def readme_extension_class_name(readme = nil)
    if readme = readme || current_readme
      readme.extension.to_s.sub(".", "").gsub(/\s+/, "").force_encoding("utf-8").scrub!
    else
      ""
    end
  rescue => e
    failbot e
    ""
  end

  # Should we recommend that you add a README?
  def should_recommend_readme?
    params[:path].blank? &&
      current_user_can_push? &&
      current_directory &&
      !current_directory.truncated? &&
      !current_repository.user_configuration_repository?
  end

  def current_branch_or_tag_name
    return @current_branch_or_tag_name if defined? @current_branch_or_tag_name

    @current_branch_or_tag_name =
      if params[:name].blank?
        if repository_offline?
          "master"
        else
          current_repository.default_branch
        end
      elsif repository_offline?
        params[:name]
      elsif current_repository.refs.exist?(params[:name])
        params[:name]
      end
  end

  def display_current_branch_or_tag_name
    current_branch_or_tag_name.try { |s| s.dup.force_encoding("utf-8").scrub! }
  end

  def branch_or_tag_name
    current_branch_or_tag_name || current_repository.default_branch
  end

  # Used to render lines inside of blob/_blob_content
  #
  # lines - Array of String lines, as returned by TreeEntry#colorized_lines.
  #
  # Returns an HTML String.
  def wrap_in_line_number_spans(lines)
    out = []
    lines.each_with_index do |line, i|
      line ||= ""
      line = line.scrub.gsub(/^( )+/) { |s| "&nbsp;" * s.size }
      out << "<div class='line js-file-line' id='LC#{i + 1}'>#{line.blank? ? '<br>' : line}</div>"
    end
    text = out.join("")

    text = %Q{<div class="code-body highlight"><pre>#{text}</pre></div>}

    text.html_safe # rubocop:disable Rails/OutputSafety
  end

  # Format a commit message as HTML and truncate anything over 72 characters.
  # The Commit#commit_message should be used instead when a Commit object
  # is available.
  def format_commit_message_short(message, repository)
    context = {
      entity: repository || try(:current_repository),
      base_url: base_url,
    }

    cache_key = [
      CacheKeyLoggingBlacklist::TREE_HELPER_PREFIX,
      message.to_s.to_md5,
      context[:entity].try(:name_with_owner),
      "v2",
    ].join(":")

    GitHub.cache.fetch(cache_key) do
      format_commit_message_short!(message, context)
    end
  end

  def format_commit_message_short!(message, context = {})
    commit_message = GitHub::CommitMessageHTML.new(message, context)
    commit_message.subject
  end

  def default_branch?
    return false unless current_repository.git
    [current_repository.default_branch, current_repository.default_oid].include?(tree_name)
  end

  def finder_newb?
    logged_in? && !current_user.dismissed_notice?("tree_finder_help")
  end

  # Show the branch information bar? Shows some quick stats about the branch,
  # and all of the discussable information related to it.
  def show_branch_infobar?
    return false if tree_type != "branch"
    return true if current_repository.fork?

    current_branch_or_tag_name != current_repository.base_branch(current_branch_or_tag_name, current_user)
  end
end
