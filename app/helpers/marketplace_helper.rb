# frozen_string_literal: true

module MarketplaceHelper
  extend ActionView::Helpers::TagHelper
  include PlatformHelper

  # Public: Generates an optimized image tag. Loads the correct asset based on the user's type of display (high resolution vs low)
  #   Only for CDN assets that have Fastly IO enabled.
  #   Fastly IO docs: https://docs.fastly.com/api/imageopto
  def marketplace_optimized_image_tag(url, options = {})
    optimized_url = Addressable::URI.parse(url)

    image_params = {
      width: options[:width],
      height: options[:height],
      format: options[:format] || "jpeg",
      auto: options[:auto] || "webp",
    }.compact

    optimized_url.query_values = (optimized_url.query_values || {}).merge(image_params)

    options[:srcset] = "#{optimized_url}&dpr=1.5 1.5x, #{optimized_url}&dpr=2 2x"

    ActionController::Base.helpers.image_tag(optimized_url.to_s, options.except(:format, :auto))
  end

  def marketplace_logo(name:, logo_url:, bgcolor:, classes: nil, by_github: false)
    classes = "CircleBadge #{classes}"
    classes = "#{classes} CircleBadge--github" if by_github

    style = "background-color: ##{ bgcolor };"

    content_tag(:div, class: classes, style: style) do
      image_tag(logo_url.to_s, class: "CircleBadge-icon", alt: "")
    end
  end

  def marketplace_plan_cancel_button(on_free_trial:, next_billing_date:, options: {})
    if on_free_trial
      button_text = "Cancel free trial"
      options[:"data-confirm"] = "Cancelling this free trial will cancel your subscription to this app and your free trial will expire. This change will take effect immediately. Are you sure you wish to continue?"
    else
      button_text = "Cancel this plan"
      options[:"data-confirm"]  = "Cancelling this plan will end your subscription to this app on #{next_billing_date.to_s(:date)}. Are you sure you wish to continue?"
    end
    button_tag(button_text, options)
  end

  def marketplace_logo_text_class(light = true)
    if light
      "text-white"
    else
      "text-gray-dark"
    end
  end

  def marketplace_card_background_image(background_url)
    if background_url
      "background-image:url('#{image_path background_url}');"
    else
      ""
    end
  end

  # Public: Should any Marketplace-related calls to action be shown?
  def show_marketplace_calls_to_action?
    marketplace_enabled? && logged_in?
  end

  # Public: Should the Marketplace call to action for continuous integration be shown
  # for the given repository?
  def show_marketplace_ci_cta?(repository)
    return false unless repository && !repository.advisory_workspace?
    return false unless show_marketplace_calls_to_action?
    return false if repository.has_ci?
    repository.adminable_by?(current_user)
  end

  def marketplace_edit_menu_item(path, selected)
    options = { class: "menu-item" }

    if selected
      options["aria-current"] = "page"
      options[:class] = "menu-item selected"
    end

    link_to(path, options) do
      yield
    end
  end

  def marketplace_edit_mobile_menu_item(text, path, selected)
    if selected
      classes = "reponav-item selected"
    else
      classes = "reponav-item"
    end

    link_to(text, path, class: classes)
  end

  def marketplace_onboarding_status_icon(completed = false)
    if completed
      octicon("check", class: "text-green")
    else
      octicon("primitive-dot", class: "color-yellow-7 mr-1")
    end
  end

  def marketplace_search_page_info(search_type)
    if search_type.nil?
      search_type = "tools"
      canonical_url = marketplace_search_url
    else
      canonical_url = marketplace_search_url(type: search_type)
    end

    page_info(
      title: "GitHub Marketplace · #{search_type.capitalize} to improve your workflow",
      description: "Find the #{search_type} that help your team build better, together.",
      container_xl: true,
      stafftools: biztools_marketplace_path,
      canonical_url: canonical_url,
      richweb: {
        title: "GitHub Marketplace: #{search_type} to improve your workflow",
        url: request.original_url,
        description: "Find the #{search_type} that help your team build better, together.",
        image: image_path("modules/site/social-cards/marketplace.png"),
      })
  end

  def show_marketplace_social_proof_part_2?
    logged_in?
  end

  def paying_user?
    logged_in? && current_user.paying_customer?
  end
end
