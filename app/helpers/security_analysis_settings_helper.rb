# frozen_string_literal: true

module SecurityAnalysisSettingsHelper
  include UrlHelper

  # Provides link to manage repo-level security settings
  def security_analysis_settings_path(repository)
    repo_settings_path = edit_repository_path(repository)
    "#{repo_settings_path}/security_analysis"
  end

  def security_analysis_update_path(owner)
    if owner.organization?
      settings_org_security_analysis_update_path(owner: owner)
    else
      settings_user_security_analysis_update_path(owner: owner)
    end
  end

  # variance in the ending of the enable all / disable all popup dialog
  def user_or_org_text(owner, private_only)
    if owner.organization?
      if private_only
        "private repositories in #{owner.name}"
      else
        "repositories in #{owner.name}"
      end
    elsif private_only
      "your private repositories"
    else
      "your repositories"
    end
  end
end
