# frozen_string_literal: true

# Helper module
# Collection of Pure functions to assist the token scanning path discovery job which has to process the output
# of the git diff-tree -r command and create duplicate token scan result locations for every blob that maps
# to more than one path.
module TokenPathHelper
  # Takes a hash of commit to token scan result locations and a hash of commit to GitRPC::Diff::Delta
  # And finds any token scan result locations where the token found is in a blob that maps to
  # more than one location.
  #
  # Because of how the token scanning job records path for a token scan result (see git rev-list --find),
  # it is possible for a token to exist in 2 different files without the user ever seeing one of those files.
  # this is because git stores file content seperately from their respective paths, and if 2 files have the
  # same content, they will share a blob.
  #
  # returns an array of hashes containing {scan_result_location: ActiveRecord_object for the location that needs to be cloned, paths: Array of hashes containing parsed diff-tree path and blob info}
  def find_multiple_paths_for_blobs(commits_to_token_scan_result_locations, commit_to_diff_tree)
    result_locations_with_hidden_paths = []
    commit_to_diff_tree.each do |commit, diff_deltas|
      # find blobs that exist in multiple locations in a given commit.
      blob_to_deltas = diff_deltas.group_by { |delta| delta.new_file.oid }
      blobs_with_multiple_paths = blob_to_deltas.select { |blob, deltas| deltas.length > 1 }
      token_scan_result_locations_for_commit = commits_to_token_scan_result_locations[commit]

      blobs_with_multiple_paths.each do |blob, deltas|
        token_scan_result_locations = (token_scan_result_locations_for_commit.select { |location| location[:blob_oid] == blob })
        token_scan_result_locations.each do |location|
          result_locations_with_hidden_paths << {scan_result_location: location, diff_deltas: deltas}
        end
      end
    end
    result_locations_with_hidden_paths
  end

  # Takes a collection of hashes containing TokenScanLocation objects and lists of diff-tree blob path results
  # and builds a list of copied TokenScanLocations for each of the corresponding paths where the path values are replaced.
  # These results will have the blob_paths_processed property set to true.
  def build_new_token_scan_result_location_records(result_locations_with_hidden_paths)
    new_token_scan_result_location_records = []
    if result_locations_with_hidden_paths
      result_locations_with_hidden_paths.each do |location_with_diff_deltas|
        original_token_scan_result_location = location_with_diff_deltas[:scan_result_location].dup
        deltas_with_different_path_to_blob = location_with_diff_deltas[:diff_deltas].select { |delta| delta.new_file.path != original_token_scan_result_location[:path] }
        deltas_with_different_path_to_blob.each do |delta|
          new_location = {}
          new_location[:path] = delta.path.dup
          new_location[:commit_oid] = original_token_scan_result_location.commit_oid
          new_location[:blob_oid] = original_token_scan_result_location.blob_oid
          new_location[:start_line] = original_token_scan_result_location.start_line.to_int
          new_location[:end_line] = original_token_scan_result_location.end_line.to_int
          new_location[:start_column] = original_token_scan_result_location.start_column.to_int
          new_location[:end_column] = original_token_scan_result_location.end_column.to_int
          new_location[:blob_paths_processed] = true
          new_token_scan_result_location_records << { token: original_token_scan_result_location.token_scan_result, location: new_location }
        end
      end
    end
    new_token_scan_result_location_records
  end
end
