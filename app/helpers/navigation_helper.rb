# frozen_string_literal: true

module NavigationHelper
  # Render a "progress steps" list step <li> with the appropriate state class.
  #
  # n            - The 1-indexed Integer indicating the step's position in the
  #                list.
  # current_step - The Integer indicating which step is the current step.
  #
  # Examples:
  #
  #   render_progress_step(2, ;current_step => 3) do
  #     Step 2
  #   end
  #
  #   # => "<li class='complete'>Step 2</li>"
  #
  # Returns a String.
  def render_progress_step(n, current_step:, &block)
    html_class =  if current_step == n then "current text-center text-md-left"
    elsif current_step > n then "complete text-center text-md-left"
    else "text-center text-md-left"
    end

    content_tag :li, class: html_class, &block
  end

  class RepositoryNavigationView
    include Scientist
    include ActionView::Helpers::NumberHelper

    attr_reader :current_user, :current_repository

    def initialize(user, repo)
      @current_user = user
      @current_repository = repo
    end

    def logged_in?
      !!current_user
    end

    def highlights_for_projects
      [:repo_projects, :new_repo_project, :repo_project]
    end

    # What symbols should we trigger highlighting for various tabs?
    #
    # Returns an array of symbols.
    def highlights_for_code
      [:repo_source, :repo_downloads, :repo_commits, :repo_releases, :repo_tags, :repo_branches, :repo_packages, :repo_deployments]
    end

    def highlights_for_pulls
      [:repo_pulls, :checks]
    end

    def highlights_for_issues
      [:repo_issues, :repo_labels, :repo_milestones]
    end

    def highlights_for_discussions
      [:repo_discussions]
    end

    def highlights_for_security
      [:security, :overview, :alerts, :policy, :token_scanning, :code_scanning]
    end

    def highlights_for_wiki
      [:repo_wiki]
    end

    def highlights_for_actions
      [:repo_actions]
    end

    def highlights_for_graphs
      highlights = [:repo_graphs, :repo_contributors, :dependency_graph, :dependabot_updates, :pulse, :people]
      highlights << :alerts unless show_security?
      highlights
    end

    def highlights_for_settings
      [:repo_settings, :repo_branch_settings, :hooks, :integration_installations,
       :repo_keys_settings, :issue_template_editor, :secrets_settings,
       :key_links_settings, :repo_actions_settings, :notifications]
    end

    def show_discussions?
      current_repository.show_discussions?
    end

    # Should we show the issues tab?
    #
    # Returns true to show the tab.
    def show_issues?
      current_repository.has_issues?
    end

    # Should we show the projects tab?
    #
    # Returns true to show the tab.
    def show_projects?
      current_repository.repository_projects_enabled?
    end

    # Should we show the community tab?
    # Only if it's a public repo and only if the current user is a repo admin
    #
    # Returns a boolean
    def show_community?
      GitHub.community_profile_enabled? &&
      logged_in? &&
      plan_supports_insights? &&
      current_repository.public? &&
      !current_repository.fork?
    end

    # Should we show the configure tab?
    #
    # Returns true to show the tab.
    def show_config?
      if GitHub.flipper[:custom_roles].enabled?(current_repository.owner) && GitHub.flipper[:repository_settings_tab_for_fgp].enabled?(current_repository.owner)
        show_config_authzd.allow?
      else
        science "show_config_fgp" do |e|
          e.context user: current_user,
                    repo: current_repository

          e.use { show_config_original? }
          e.try { show_config_authzd }
          e.compare { |control, candidate| control == candidate.allow? }

          # Reduce noise of this experiment by ignoring network failures
          e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
        end
      end
    end

    def show_config_original?
      (current_repository.adminable_by?(current_user) && !current_repository.advisory_workspace?) || show_fine_grained_config?
    end

    # Should we show the new FGP configure tab?
    #
    # Returns boolean.
    def show_fine_grained_config?
      return false unless logged_in?
      current_repository.async_action_or_role_level_for(current_user).sync == :maintain
    end

    def show_config_authzd
      return Authzd::Response.from_decision(Authzd::Proto::Decision.deny) unless !current_repository.advisory_workspace?
      return show_fine_grained_config_authzd
    end

    def show_fine_grained_config_authzd
      current_repository.async_can_view_repository_settings?(current_user).sync
    end

    def show_insights?
      !current_repository.advisory_workspace?
    end

    def show_wiki?
      return false unless current_repository.has_wiki?
      unless GitHub.flipper[:wikis_visible_by_default].enabled? && (current_repository.owner == current_user)
        return false unless current_repository.plan_supports?(:wikis)
      end
      if logged_in?
        return true
      else
        wiki = current_repository.unsullied_wiki
        wiki.exist? && wiki.pages.count > 0
      end
    end

    def plan_supports_insights?
      current_repository.plan_supports?(:insights)
    end

    def show_actions?
      GitHub.actions_enabled? && !current_repository.all_action_executions_disabled?
    end

    def open_issue_count
      current_repository.open_issue_count_for(current_user)
    end

    def open_pull_request_count
      current_repository.open_pull_request_count_for(current_user)
    end

    def open_project_count
      current_repository.projects.open_projects.count
    end

    def security_count
      @security_count ||= (show_alerts? ? security_network_alerts_count : 0) +
        security_advisories_count +
        (show_token_scanning? && current_repository.show_token_scanning_results?(@current_user) ? security_token_scanning_count : 0) +
        (show_code_scanning? && security_code_scanning_count > 0 ? security_code_scanning_count : 0)
    end

    def formatted_security_count(limit: 1000)
      "#{number_with_delimiter([security_count, limit].min)}#{security_count > limit ? "+" : ""}"
    end

    def security_network_alerts_count
      @security_network_alerts_count ||= RepositoryVulnerabilityAlert::AlertQuery.new(current_repository).resolve.count
    end

    def security_advisories_count
      @security_advisories_count ||= current_repository.repository_advisories.published.count
    end

    def security_token_scanning_count
      @security_token_scanning_count ||= if current_repository.use_token_scanning_service?(current_user)
        current_repository.token_scanning_service_unresolved_count
      else
        current_repository.token_scan_results.unresolved.included.count
      end
    end

    def security_code_scanning_count
      @security_code_scanning_count ||= current_repository.code_scanning_open_alerts_count
    end

    # Should we show the dependency_graph tab
    #
    # Returns true to show the tab
    def show_dependency_graph?
      GitHub.dependency_graph_enabled? && (
        !GitHub.enterprise? || current_repository.dependency_graph_enabled?
      )
    end

    def show_alerts_under_insights?
      return false if show_security?

      show_alerts?
    end

    def show_alerts?
      current_repository.show_dedicated_alerts_ui?(current_user)
    end

    def show_token_scanning?
      current_repository.show_token_scanning_ui?(current_user) && !current_repository.token_scanning_staff_disabled?
    end

    def show_code_scanning?
      current_repository.code_scanning_readable_by?(current_user)
    end

    # Public: Should the Security tab be shown at all?
    # Currently enabled for all repos on github.com, and on GHES behind a configuration key
    def show_security?
      return GitHub.configuration_advanced_security_enabled?
    end

    # Public: Should the People tab be displayed for this repository?
    #
    # Returns a Boolean.
    def show_people?
      return false unless current_repository.owner.organization?
      current_repository.owner.adminable_by?(current_user)
    end
  end
end
