# frozen_string_literal: true

module ProjectsHelper
  include PlatformHelper

  DEFAULT_SORT = "created-desc"
  SORTS = [
             ["Newest", "created-desc"],
             ["Oldest", "created-asc"],
             ["Recently updated", "updated-desc"],
             ["Least recently updated", "updated-asc"],
             ["Name", "name-asc"],
          ].freeze

  def card_movable?(card)
    !card.redacted? && card.project.writable_by?(current_user)
  end

  def card_menu(card, &block)
    if card.persisted?
      render partial: "projects/card_menu", locals: { card: card, capture: capture(&block) }
    end
  end

  def enable_dragging?(project)
    if project.is_a?(PlatformTypes::Updatable)
      project.viewer_can_update?
    else
      project.writable_by?(current_user)
    end
  end

  def issue_label(issue)
    if issue.state == "locked"
      "Locked Issue"
    elsif issue.pull_request
      "Pull Request"
    elsif issue.open?
      "Open Issue"
    else
      "Closed Issue"
    end
  end

  def issue_icon(issue)
    if issue.state == "locked"
      "lock"
    elsif issue.pull_request
      "git-pull-request"
    elsif issue.open?
      "issue-opened"
    else
      "issue-closed"
    end
  end

  def issue_status(issue)
    if issue.pull_request? && issue.pull_request.merged?
      "merged"
    elsif issue.pull_request? && (issue.pull_request.draft? && issue.pull_request.open?)
      "draft"
    else
      issue.open? ? "open" : "closed"
    end
  end

  def redacted_card_icon(reason_for_redaction)
    if reason_for_redaction == :repository_missing
      "circle-slash"
    elsif reason_for_redaction == :unauthorized
      "shield-lock"
    else
      "lock"
    end
  end

  def redacted_card_title(reason_for_redaction)
    if reason_for_redaction == :repository_missing
      "Card content unavailable"
    else
      "You can't see this card"
    end
  end

  def redacted_card_message(reason_for_redaction)
    if reason_for_redaction == :spammy
      "This card references something that has spammy content."
    elsif reason_for_redaction == :repository_missing
      "This card references something in a repository that was deleted."
    elsif reason_for_redaction == :issue_missing
      "This card references a deleted issue."
    else
      "This card references something you don't have access to."
    end
  end

  # Returns a friendly list of automation purposes for all columns
  # Requires an array of pre-fetched ProjectColumns from GraphQL
  #
  def automation_summary(column_purposes)
    purposes = column_purposes.compact
    purposes.map do |purpose|
      if purpose == "TODO"
        "To do"
      else
        purpose.humanize
      end
    end.uniq.to_sentence
  end

  def clone_settings_summary(workflow_count)
    settings = ["column names", "positions"]
    settings << "automation settings" if workflow_count > 0
    settings.to_sentence
  end

  def cloneable_name(project_name)
    "[COPY] #{project_name}".truncate(Project::MAX_NAME_LENGTH, omission: "")
  end

  def is_account_project?(project)
    %w[Organization User].include?(project.owner_type)
  end

  # Default query for use on the "Add Cards" search pane
  #
  def default_card_search_value
    "is:open"
  end

  def projects_search_term_values(key)
    parsed_projects_query.select { |(k, v)| k == key }.map { |(k, v)| v }
  end

  def project_search_slug_owner_type(slug)
    case slug && slug.count("/")
    when 1
      # The slug looks like "org/123".
      "Organization"
    when 2
      # The slug looks like "user/repo/123".
      "Repository"
    else
      # The slug is probably bad.
      nil
    end
  end

  # creates a new search query by duplicating the current parsed query and then
  # applies any of the arguments to said duplicate
  def projects_search_query(replace: {}, append: [], remove: {})
    components = parsed_projects_query.dup

    replace.each_pair do |key, value|
      if value
        components << [key, value]
      else
        components.reject! { |k, v| k == key }
      end
    end

    components.delete_if do |key, value|
      remove.value?(value)
    end

    components += append
    Search::Queries::ProjectQuery.stringify(components)
  end

  def format_search_query(query)
    if query.present?
      "#{query.rstrip} "
    elsif defined?(project_index_query)
      "#{project_index_query} "
    else
      ""
    end
  end

  def filtered_project_sorts
    projects_search_term_values(:sort)[0] || DEFAULT_SORT
  end

  def selected_project_sort?(sort)
    filtered_project_sorts == sort
  end

  def hide_project?(state_filter, project_state)
    return false unless state_filter.present?
    return false unless state_filter == "open" || state_filter == "closed"
    state_filter.upcase != project_state
  end

  def showing_filtered_projects?
    parsed_projects_query != [[:is, "open"]]
  end

  def search_includes_name?
    # look for a user supplied string
    # :sort and :is queries will be an array in the parsed query
    parsed_projects_query.any? { |component| component.is_a?(String) }
  end

  # Truncate the project body markdown to just the first paragraph
  def truncate_project_body_markdown(markdown)
    transcoded = GitHub::Encoding::try_guess_and_transcode(markdown)
    doc = Nokogiri::HTML::DocumentFragment.parse(transcoded)

    first_paragraph = doc.at("p")
    return unless first_paragraph

    # Don't truncate if the entire document is one single paragraph
    return if first_paragraph == doc.first_element_child && doc.element_children.length == 1

    html = first_paragraph.inner_html
    html = content_tag(:span, html, nil, false) if markdown.html_safe?
    html
  end

  def project_body_markdown(body)
    markdown = github_simplified_markdown(body)
    truncated_markdown = truncate_project_body_markdown(markdown)
    yield markdown, truncated_markdown
  end

  def column_purpose_options
    {
      nil => { name: "None", description: "This column will not be automated"},
      "TODO" => { name: "To do", description: "Planned but not started"},
      "IN_PROGRESS" => { name: "In progress", description: "Actively being worked on"},
      "DONE" => { name: "Done", description: "Items are complete"},
    }
  end

  def column_purpose_name(purpose_value)
    column_purpose_options[purpose_value][:name]
  end

  def project_automation_description(trigger_type, verbose: false)
    ProjectWorkflow.workflow_description(trigger_type, verbose: verbose)
  end

  def include_staff_bar(view_id:)
    return unless staff_bar_enabled?

    if params[:xhr_stats].present? && !request.xhr?
      render partial: "projects/staff_bar"
    else
      render partial: "site/stats", locals: {
        view: Site::ProjectStats.new(request: request, element_id_suffix: "-#{view_id}"),
      }
    end
  end

  # Parses a GraphQL generated Addressable::URI object for query values
  # and adds a search query to open the add cards panel
  #
  # Returns a new Addressable::URI
  def uri_with_add_cards_query(project_uri)
    uri = project_uri.dup
    uri.query_values = (uri.query_values || {}).reverse_merge({ add_cards_query: "is:open" })
    uri
  end

  ProjectColumnAnchorUrl = parse_query <<-'GRAPHQL'
    fragment ProjectColumn on ProjectColumn {
      databaseId

      project {
        url
      }
    }
  GRAPHQL

  def project_column_anchor_url(project_column)
    project_column = ProjectColumnAnchorUrl::ProjectColumn.new(project_column)
    "#{project_column.project.url}#column-#{project_column.database_id}"
  end

  # Should dragging columns and cards be limited to only a specific dragging
  # handle instead being able to drag by the entire card/column element?
  #
  # On phone/tablet touch devices a dragging handle should be used so that
  # the scroll events can be distinguished from drag events.
  def enable_drag_by_handle?
    return true if mobile?

    # Also enable drag by handle on iPads and Android tablets
    request.user_agent.to_s =~ /iPad;|Android/
  end
end
