# frozen_string_literal: true

module ReactionsHelper
  include PlatformHelper
  include EscapeHelper

  TOOLTIP_SOFT_TRUNCATION_THRESHOLD = 11
  TOOLTIP_HARD_TRUNCATION_THRESHOLD = 10

  ReactionCountTooltipGroup = parse_query <<-'GRAPHQL'
    fragment on ReactionGroup {
      content
      truncatedUsers: users(first: 11) {
        totalCount
        edges {
          node {
            login
          }
        }
      }
    }
  GRAPHQL

  # Public: Generate the text for a tooltip describing who posted a given
  # reaction.
  #
  # group - The Reaction Group
  # bold_usernames - Boolean, indicates if the usernames should be wrapped in <span class="text-bold">. Defaults to false.
  #
  # Returns a string.
  def reaction_count_tooltip(group, bold_usernames: false)
    group = ReactionCountTooltipGroup.new(group)

    reaction_count_tooltip_for_model(
      Emotion.find_by_platform_enum(group.content),
      names_from_reactions(group.truncated_users.edges.first(TOOLTIP_SOFT_TRUNCATION_THRESHOLD)),
      group.truncated_users.total_count,
    )
  end

  # Public: Generate the text for a tooltip describing who posted a given
  # reaction.
  #
  # content - The content of the reaction group
  # truncated_user_logins - array with the first TOOLTIP_SOFT_TRUNCATION_THRESHOLD users of the reaction group
  # total_users - total number of users on the reaction group
  # bold_usernames - Boolean, indicates if the usernames should be wrapped in <span class="text-bold">. Defaults to false.
  #
  # Returns a string.
  def reaction_count_tooltip_for_model(emotion, truncated_user_logins, total_users, bold_usernames: false)
    return "" if truncated_user_logins.empty?

    is_truncated = total_users > TOOLTIP_SOFT_TRUNCATION_THRESHOLD

    names = if is_truncated
      truncated_user_logins.first(TOOLTIP_HARD_TRUNCATION_THRESHOLD)
    else
      truncated_user_logins
    end

    names = make_bold_usernames(names) if bold_usernames
    names += ["#{total_users - TOOLTIP_HARD_TRUNCATION_THRESHOLD} more"] if is_truncated

    safe_join([html_safe_to_sentence(names),
      " reacted with ", emotion.pronounceable_label, " emoji"
    ])
  end

  private

  # Internal: Get the names of the people who left Reactions.
  #
  # reactions - The Reactions to get names for.
  #
  # Returns an array.
  def names_from_reactions(reactions)
    reactions.map { |r| r.node.login }
  end

  # Internal: Wrap usernames in <span class="text-bold">
  #
  # logins - An array of logins (usernames) to get wrapped in <span>s
  #
  # Returns an array.
  def make_bold_usernames(logins)
    logins.map { |login| content_tag(:span, login, class: "text-bold") }
  end
end
