# frozen_string_literal: true
module KeyboardShortcutsHelper

  # Generate page level meta tag with the keyboard shortcuts available
  def keyboard_shortcuts_meta_tag
    contexts = []
    url = request.path

    contexts.push("repository") if current_repository
    contexts.push("code-editor") if url =~ %r{\A/[^/]+/[^/]+/edit/}
    contexts.push("source-code") if url =~ /\/(tree|blob|compare)/
    contexts.push("issues") if url =~ /issues/ && url !~ /dashboard\/issues/
    contexts.push("commits") if url =~ /commit\//
    contexts.push("commit-list") if url =~ /(?<![pull\/\d+])\/commits/
    contexts.push("dashboards") if url =~ /\A(\/|\/pulls|\/issues)\Z/

    # Whether /notifications requires v1 or v2 keyboard shortcuts depends on a feature flag
    if url =~ /notifications(?!\/beta).*/
      if !GitHub.notifications_v2_disabled?
        contexts.push("notifications-v2")
      else
        contexts.push("notifications")
      end
    end

    contexts.push("notifications-v2") if show_notification_shelf? || url =~ /notifications\/beta/
    contexts.push("pull-request-list", "pull-request-conversation", "pull-request-files-changed") if url =~ /\/pulls?/
    contexts.push("project-cards", "moving-a-card", "moving-columns") if url =~ /projects\/\d+/
    contexts.push("network-graph") if url =~ /network/
    contexts.push("checks") if url =~ /\/checks/

    tag(:meta, name: "github-keyboard-shortcuts", content: contexts.join(","), "data-pjax-transient": true)
  end
end
