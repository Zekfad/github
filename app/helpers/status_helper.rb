# frozen_string_literal: true

module StatusHelper
  # What is the human representation of what a status' state?
  def sentence_for_status(status)
    case status.state.downcase
    when "expected" then "Waiting for status to be reported"
    when "error" then "Some checks experienced an error"
    when *StatusCheckRollup::PENDING_STATES then "Waiting to hear about #{status.sha[0, 7]}"
    when *StatusCheckRollup::SUCCESS_STATES then "All checks have passed"
    when *StatusCheckRollup::FAILURE_STATES then "Some checks have failed"
    end
  end

  def icon_symbol_for_state(state)
    case state.downcase
    when StatusCheckRollup::NEUTRAL            then "primitive-square"
    when StatusCheckRollup::STALE              then "issue-reopened"
    when StatusCheckRollup::SUCCESS            then "check"
    when StatusCheckRollup::CANCELLED          then "stop"
    when StatusCheckRollup::SKIPPED            then "skip"
    when *StatusCheckRollup::PENDING_STATES    then "primitive-dot"
    when *StatusCheckRollup::INCOMPLETE_STATES then "x"
    when *StatusCheckRollup::FAILURE_STATES    then "x"
    end
  end

  # used to color the icons
  def status_state_text_class(state)
    case state.to_s.downcase
    when StatusCheckRollup::NEUTRAL            then "neutral-check"
    when StatusCheckRollup::CANCELLED          then "neutral-check"
    when StatusCheckRollup::STALE              then "neutral-check"
    when StatusCheckRollup::SKIPPED            then "neutral-check"
    when StatusCheckRollup::SUCCESS            then "text-green"
    when *StatusCheckRollup::FAILURE_STATES    then "text-red"
    when *StatusCheckRollup::INCOMPLETE_STATES then "text-red"
    when *StatusCheckRollup::PENDING_STATES    then "color-yellow-7"
    when "expected"                            then "color-yellow-7"
    end
  end

  # used to color the text
  def status_state_text_color(state)
    case state.to_s.downcase
    when "expected"                            then "color-yellow-7"
    when StatusCheckRollup::NEUTRAL            then "text-gray-light"
    when StatusCheckRollup::STALE              then "text-gray-light"
    when StatusCheckRollup::CANCELLED          then "text-gray-light"
    when StatusCheckRollup::SKIPPED            then "text-gray-light"
    when StatusCheckRollup::SUCCESS            then "text-green"
    when *StatusCheckRollup::FAILURE_STATES    then "text-red"
    when *StatusCheckRollup::INCOMPLETE_STATES then "text-red"
    when *StatusCheckRollup::PENDING_STATES    then "text-gray-light"
    end
  end

  def additional_status_check_context(state, duration_in_seconds)
    state = state.downcase
    case state
    when *StatusCheckRollup::PENDING_STATES
      StatusCheckRollup.adjective_state(state).capitalize
    else
      time_to_complete(state, duration_in_seconds).capitalize
    end
  end

  def default_status_check_description(state)
    case state.downcase
    when StatusCheckRollup::IN_PROGRESS
      "This check has started..."
    when StatusCheckRollup::QUEUED
      "Waiting to run this check..."
    when StatusCheckRollup::STALE
      "This check was marked as stale"
    else
      # no default
    end
  end

  def status_check_pending?(state)
    StatusCheckRollup::PENDING_STATES.include?(state.downcase)
  end

  def time_to_complete(state, seconds)
    return "Skipped" if state == StatusCheckRollup::SKIPPED
    return "" if seconds.zero?
    return "" if seconds.negative?

    state = state.downcase
    preposition = "after"
    if StatusCheckRollup::SUCCESS_STATES.include?(state)
      preposition = "in"
    end

    adjective = StatusCheckRollup.adjective_state(state)
    if state == StatusCheckRollup::NEUTRAL
      adjective = "completed"
    end

    "#{adjective} #{preposition} #{duration(seconds)}"
  end

  def duration(seconds)
    case seconds
    when 0
      ""
    when (1..59)
      "#{seconds}s"
    else
      "#{seconds/60}m"
    end
  end

  def precise_duration(raw_seconds)
    seconds = raw_seconds.round
    minutes, remainder_seconds = seconds.divmod(60)
    hours, remainder_minutes = minutes.divmod(60)
    days, remainder_hours = hours.divmod(24)

    if minutes < 1
      "#{seconds}s"
    elsif hours < 1
      "#{minutes}m #{remainder_seconds}s"
    elsif days < 1
      "#{hours}h #{remainder_minutes}m #{remainder_seconds}s"
    else
      "#{days}d #{remainder_hours}h #{remainder_minutes}m #{remainder_seconds}s"
    end
  end

  def status_state_font_style(state)
    case state.to_s.downcase
    when *StatusCheckRollup::PENDING_STATES then "text-italic"
    else "text-bold"
    end
  end

  def status_state_adjective(state)
    StatusCheckRollup.adjective_state(state.to_s.downcase)
  end

  # Public: Status summary for tooltip.
  #
  # status - A Status or CombinedStatus
  #
  # Returns String.
  def status_tooltip(status)
    case status
    when Status
      single_status_tooltip(status)
    when CombinedStatus
      if status.count > 1
        combined_status_tooltip(status)
      elsif status.count == 1
        single_status_tooltip(status.single_status)
      end
    end
  end

  # Public: Single Status summary.
  #
  # Used on status icon tooltip.
  #
  #   "Success: The Travis CI build passed"
  #
  # status - A single Status
  #
  # Returns String.
  def single_status_tooltip(status)
    [status.state.downcase.gsub("_", " ").capitalize, status.description].compact.join(": ")
  end

  # Public: Combined Status summary text of successful checks.
  #
  # Used on status icon tooltip.
  #
  # combined_status - A CombinedStatus
  #
  # Returns String.
  def combined_status_tooltip(combined_status)
    successful_count = combined_status.contexts.count { |context| StatusCheckRollup::SUCCESS_STATES.include?(context.state.downcase) }
    "#{successful_count} / #{combined_status.contexts.count} checks OK"
  end

  # Public: Return totals of each status state.
  #
  #   "1 failing, 5 pending, 5 errored, and 5 successful checks"
  #
  # Returns String.
  def status_summary_of_check_totals(statuses, check_name = "check")
    counts = statuses.group_by { |status| StatusCheckRollup.adjective_state(status.state) }.map { |state, statuses| "#{statuses.length} #{state}" }
    "#{counts.to_sentence} #{check_name}".pluralize(statuses.length)
  end
end
