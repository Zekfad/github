# frozen_string_literal: true

module InvitationsHelper
  include ExperimentHelper

  # Show multi user invite for dotcom
  # TODO:
    # Add support for bulk add member to new org for enterprise server that mirrors PeopleController#add_member_for_new_org
    # Once we have that we can show multi user invite on enterprise server
  def multi_user_invite_enabled?
    !GitHub.bypass_org_invites_enabled?
  end
end
