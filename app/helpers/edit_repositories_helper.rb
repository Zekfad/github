# frozen_string_literal: true

module EditRepositoriesHelper

  def show_archive_program_settings?
    current_repository.can_participate_in_archive_program?
  end
end
