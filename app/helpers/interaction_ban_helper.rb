# frozen_string_literal: true

module InteractionBanHelper
  include PlatformHelper
  include ActionView::Helpers::DateHelper

  def interaction_ban_copy(repo, user, action = "comment", tooltip = false)
    if RepositoryInteractionAbility.restricted_by_limit?(:sockpuppet_ban, repo, user)
      "An owner of this repository has limited the ability to #{action} from new users."
    elsif RepositoryInteractionAbility.restricted_by_limit?(:contributors_only, repo, user)
      "An owner of this repository has limited the ability to #{action} to users that have contributed to this repository in the past."
    elsif RepositoryInteractionAbility.restricted_by_limit?(:collaborators_only, repo, user)
      "An owner of this repository has limited the ability to #{action} to users that are collaborators on this repository."
    else
      result = []
      result << "Your ability to #{action} has been suspended for "
      result << distance_of_time_in_words(DateTime.now, User::InteractionAbility.ban_expiry(user.id))
      unless tooltip
        result << " If you feel this is in error please "
        result << link_to("contact GitHub support", contact_path(form: {subject: "Interaction ability suspended"}))
        result << "."
      end

      safe_join(result)
    end
  end
end
