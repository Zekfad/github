# frozen_string_literal: true
require "asset_bundles"

module BundleHelper
  def asset_bundle
    @asset_bundle ||= AssetBundles.get
  end

  def javascript_bundle(source, lazy: false)
    options = { crossorigin: GitHub.asset_crossorigin_with_credentials? ? "use-credentials" : "anonymous" }

    if feature_enabled?(:js_in_head)
      options[:defer] = true
    # Environment/development bundle should be the only synchronous bundle
    elsif !source.to_s.start_with?("environment") && source != "development"
      options[:async] = true
    end

    if !source.to_s.ends_with?(".js")
      source = "#{source}.js"
    end
    filename = asset_bundle.expand_bundle_name(source)
    hash = asset_bundle.integrity(filename)
    if hash
      options[:integrity] = hash
    end

    url = asset_bundle.bundle_url(source)

    options[:type] = "application/javascript"
    if source == "compat.js"
      options["id"] = "js-conditional-compat"
      options["data-src"] = url
    elsif lazy
      options["data-module-id"] = "./#{source}"
      options["data-src"] = url
    else
      options[:src] = url
    end
    content_tag(:script, "", options)
  end

  # A chunk is a file created by Rollup during compilation that contains
  # module code referenced by multiple bundles. The goal is to download and
  # cache code only once regardless of how many bundles depend on it. As we
  # navigate the site, the chunks needed for those pages are downloaded and
  # executed as needed. We're never loading the full product code on any
  # page.
  #
  # Each bundle declares a dependency on the chunk file and import it so it
  # has access to the exported functions within the chunk. Rollup rewrites
  # imports for us depending on where the source module ends up in the
  # compiled output. The real output uses a System loader, but fundamentally
  # it's doing this:
  #
  # `import {fetch} from './fetch'` -> `import {f} from 'chunk-deadbeef.js'`
  #
  # Rollup determines which modules belong in which chunks and wires the
  # exports and imports together. The app is responsible for downloading
  # the chunk files when they are needed. We could choose to download all
  # chunks on every page, but that defeats the goal of loading only the
  # code required on each page.
  #
  # We do the following:
  #
  # - Render one *inert* script element per chunk file in the HTML document:
  #   `<script src="" data-src="chunk-1.js">`.
  # - Use system-lite.js to activate those scripts by moving the `data-src`
  #   attribute to `src`. The browser then downloads and executes the JS file.
  #   That chunk file contains calls to `System.register()` to add its exported modules
  #   to the registry. That resolves any other Promise waiting to import those
  #   functions.
  #
  # Returns an Array<String> of chunk file names to include as `<script>` tags.
  def javascript_chunks
    asset_bundle.dynamic_chunks
  end

  def stylesheet_bundle(source)
    options = { crossorigin: GitHub.asset_crossorigin_with_credentials? ? "use-credentials" : "anonymous" }
    options[:media] = "all"

    filename = asset_bundle.expand_bundle_name("#{source}.css")
    hash = asset_bundle.integrity(filename)
    if hash
      options[:integrity] = hash
    end

    options[:rel] = "stylesheet"
    options[:href] = asset_bundle.bundle_url("#{source}.css")
    tag(:link, options)
  end

  def page_javascript_bundles
    @page_javascript_bundles ||= Set.new
  end

  def controller_stylesheet_bundles
    safe_join(stylesheet_bundles.map { |source| stylesheet_bundle(source) }, "\n")
  end

  def controller_javascript_bundles
    safe_join((page_javascript_bundles + javascript_bundles).map { |source| javascript_bundle(source) }, "\n")
  end
end
