# frozen_string_literal: true

# Controller and View helpers for working with prefetched resources.
#
#   <link rel="prefetch" href="//example.com/next-page.html">
#
# Can be used to indicate to the browser prefetch a future resource before its
# needed. This request appears as a normal document request, but sets a
# `Purpose: prefetch` header to differentiate it.
#
# Typically, we don't need to do much different in this case. However, there are
# edge cases where GET requests are non-idempotent, such as marking a page as
# viewed in the database. In the prefetched case, the user didn't actually view
# the page yet. So we'll want to defer that mark until the page is actually
# viewed by the user.
#
# This happens in two request stages:
#
# 1. Initial request comes in with `Purpose: prefetch`, any write actions are
# skipped and instead signal that they need a subsequent `prefetch-viewed`
# request.
#
# 2. When this prefetched document is actually viewed, scripts will pickup the
# <link> tag and issue a HEAD request to the same URL with `Purpose:
# prefetch-viewed`. The controller can then process the deferred viewing
# actions.
#
# See Also
#   app/assets/modules/github/link-prefetch-viewed.js
#   https://w3c.github.io/resource-hints/
#
module PrefetchHelper
  # Public: Check if request's purpose is to prefetch content.
  #
  # Returns true if the Purpose header is "prefetch".
  def prefetch?
    request.headers["Purpose"] == "prefetch"
  end

  # Public: Check if request's purpose is mark prefetched content as viewed.
  #
  # Returns true if the Purpose header is "prefetch-viewed".
  def prefetch_viewed?
    request.headers["Purpose"] == "prefetch-viewed"
  end

  # Public: Invokes block for normal requests. Else if `Purpose: prefetch` is
  # set, the block is skipped and a flag is set to signal the
  # link[rel=prefetch-linked] link to be emitted.
  #
  # Examples
  #
  #   prefetch_deferred do
  #     mark_page_as_viewed
  #   end
  #
  # Returns nothing.
  def prefetch_deferred
    if prefetch?
      @emit_prefetch_viewed_link_tag = true
    else
      yield
    end
    return
  end

  # Public: Omit <link rel="prefetch-viewed"> if necessary.
  #
  # Returns html safe String or nil if no tag is needed.
  def prefetch_viewed_link_tag
    if @emit_prefetch_viewed_link_tag == true && prefetch?
      tag(:link, "rel" => "prefetch-viewed", "data-pjax-transient" => "")
    end
  end
end
