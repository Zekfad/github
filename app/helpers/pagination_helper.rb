# frozen_string_literal: true

require "will_paginate/view_helpers/action_view"

module PaginationHelper
  def will_paginate(collection, options = {})
    unless options.key?(:renderer)
      options = options.merge(renderer: WillPaginateRenderer)
    end
    super(collection, options)
  end

  class WillPaginateRenderer < WillPaginate::ActionView::LinkRenderer
    def merge_get_params(url_params)
      if @template.respond_to? :request and @template.request and @template.request.get?
        symbolized_update(url_params, existing_parameters)
      end
      url_params.delete(:script_name)
      url_params
    end

    def existing_parameters
      template_params_hash =
        @template.params.dup.permit!.to_h

      valid_params(
        template_params_hash.reject { |name, value|
          name.to_s.include?("\0")
        }.with_indifferent_access,
      )
    end

    def valid_params(options)
      unless options[:params].respond_to?(:merge)
        options.delete(:params)
      end
      options
    end

    def page_number(page)
      if page == current_page
        tag(:em, page, class: "current", "data-total-pages": total_pages)
      else
        super(page)
      end
    end
  end
end
