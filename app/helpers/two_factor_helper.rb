# frozen_string_literal: true

module TwoFactorHelper
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def backup_parts
    @backup_parts ||= begin
      if number = current_user.two_factor_credential.backup_sms_number
        number.split(" ", 2)
      end
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def backup_country_code
    @backup_country_code ||= backup_parts ? backup_parts[0] : ""
  end

  def backup_national_number
    @backup_national_number ||= backup_parts ? backup_parts[1] : ""
  end

  # return only the last 4 digits of a phone number for privacy concerns in case the security checkup dialog pops up randomly for the user
  def hide_sms_number(number)
    "ending in #{number[-4, 4]}"
  end
end
