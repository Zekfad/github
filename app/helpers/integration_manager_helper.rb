# frozen_string_literal: true

module IntegrationManagerHelper

  # FIXME the three methods do an adhoc admin check at the beginning.
  # This is not desirable: we want the authorization decision to be taken by the authorizer
  # However, this is temporarily necessary because calls to the Authorizer are
  # feature flagged, and we need to fallback to the previous coarse-grained authorization logic

  # Internal: Does this user have permission to manage all integrations for the
  # target (User or Organization).
  #
  # Returns a Boolean.
  def manages_all_integrations?(user:, owner:)
    return false unless owner

    ::Permissions::Enforcer.authorize(
      actor: user,
      action: :manage_all_apps,
      subject: owner,
    ).allow?
  end

  def manages_integration?(user:, integration:)
    return false unless integration

    ::Permissions::Enforcer.authorize(
      actor: user,
      action: :manage_app,
      subject: integration,
    ).allow?
  end

  def manages_any_integration?(user:, organization:)
    return false unless organization

    return manages_all_integrations?(user: user, owner: organization) if organization.integrations.empty?

    organization.integrations.map do |integration|
      response = ::Permissions::Enforcer.authorize(
        actor: user,
        action: :manage_app,
        subject: integration,
      )
      return true if response.allow?
    end
    false
  end
end
