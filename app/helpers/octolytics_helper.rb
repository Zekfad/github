# frozen_string_literal: true

module OctolyticsHelper
  # Public: Generate the "global" octolytics meta tags.
  def octolytics_meta_tag
    return "" unless octolytics_enabled?

    actor = if logged_in?
      tag(:meta, name: "octolytics-actor-id",     content: current_user.id) <<
      tag(:meta, name: "octolytics-actor-login",  content: current_user.login) <<
      tag(:meta, name: "octolytics-actor-hash",   content: OpenSSL::HMAC.hexdigest("sha256", octolytics_secret, current_user.id.to_s))
    else
      ""
    end

    octolytics_event_url = "#{GitHub.octolytics_collector_url}/github-external/browser_event"

    tag(:meta, name: "octolytics-host",      content: GitHub.octolytics_collector_host) <<
    tag(:meta, name: "octolytics-app-id",    content: octolytics_app_id) <<
    tag(:meta, name: "octolytics-event-url", content: octolytics_event_url) <<

    # Used to tie the ga id and visitor id together
    tag(:meta, name: "octolytics-dimension-ga_id", content: "", class: "js-octo-ga-id") <<
    actor
  end

  # Public: Generate octolytics meta tags for mobile pages.
  def octolytics_mobile_tag
    return "" unless octolytics_enabled?
    tag(:meta, name: "octolytics-dimension-device", content: "mobile")
  end

  # Public: Generate octolytics dimension data for a repository.
  def octolytics_repository_tags(repository)
    return "" unless octolytics_enabled?
    return "" unless repository
    html = []
    html << octolytics_user_tags(repository.owner)
    html << tag(:meta, name: "octolytics-dimension-repository_id", content: repository.id)
    html << tag(:meta, name: "octolytics-dimension-repository_nwo", content: repository.nwo)
    html << tag(:meta, name: "octolytics-dimension-repository_public", content: repository.public?)
    html << tag(:meta, name: "octolytics-dimension-repository_is_fork", content: repository.fork?)
    if repository.fork? && repository.parent
      html << tag(:meta, name: "octolytics-dimension-repository_parent_id", content: repository.parent.id)
      html << tag(:meta, name: "octolytics-dimension-repository_parent_nwo", content: repository.parent.nwo)
    end
    if root = repository.root
      html << tag(:meta, name: "octolytics-dimension-repository_network_root_id", content: root.id)
      html << tag(:meta, name: "octolytics-dimension-repository_network_root_nwo", content: root.nwo)
      html << tag(:meta, name: "octolytics-dimension-repository_explore_github_marketplace_ci_cta_shown", content: show_marketplace_ci_cta?(repository))
    end
    safe_join(html)
  end

  # Public: Generate octolytics dimension data for a user.
  def octolytics_user_tags(user)
    return "" unless octolytics_enabled?
    return "" unless user
    tag(:meta, name: "octolytics-dimension-user_id",       content: user.id) <<
    tag(:meta, name: "octolytics-dimension-user_login",    content: user.login)
  end

  # Public: Generates octolytics dimension data for a gist.
  def octolytics_gist_tag(gist)
    return "" unless octolytics_enabled? && gist

    tags = []
    tags << tag(:meta, name: "octolytics-dimension-public", content: gist.public?)
    tags << tag(:meta, name: "octolytics-dimension-gist_id", content: gist.id)
    tags << tag(:meta, name: "octolytics-dimension-gist_name", content: gist.repo_name)

    tags << tag(:meta, name: "octolytics-dimension-anonymous", content: gist.anonymous?)
    unless gist.anonymous?
      tags << tag(:meta, name: "octolytics-dimension-owner_id", content: gist.owner.id)
      tags << tag(:meta, name: "octolytics-dimension-owner_login", content: gist.owner.login)
    end

    tags << tag(:meta, name: "octolytics-dimension-forked", content: gist.fork?)
    if gist.parent.present?
      tags << tag(:meta, name: "octolytics-dimension-parent_gist_id", content: gist.parent.id)
      tags << tag(:meta, name: "octolytics-dimension-parent_gist_name", content: gist.parent.repo_name)
      tags << tag(:meta, name: "octolytics-dimension-parent_anonymous", content: gist.parent.anonymous?)
      unless gist.parent.anonymous?
        tags << tag(:meta, name: "octolytics-dimension-parent_owner_id", content: gist.parent.owner.id)
        tags << tag(:meta, name: "octolytics-dimension-parent_owner_login", content: gist.parent.owner.login)
      end
    end

    safe_join(tags)
  end

  # Public: Generates octolytics dimension data for a marketplace listing.
  def octolytics_marketplace_listing_tags(marketplace_listing_id)
    return "" unless octolytics_enabled? && marketplace_listing_id
    tag(:meta, name: "octolytics-dimension-marketplace_id", content: marketplace_listing_id)
  end

  # Internal: Determines appropriage Octolytics app id to use for analytics depending on
  # if we are serving GitHub or Gist.
  def octolytics_app_id
    if serving_gist3_standalone?
      GitHub.gist_octolytics_app_id
    else
      GitHub.octolytics_app_id
    end
  end

  # Internal: Determines appropriage Octolytics secret to use for analytics depending on
  # if we are serving GitHub or Gist.
  def octolytics_secret
    if serving_gist3_standalone?
      GitHub.gist_octolytics_secret
    else
      GitHub.octolytics_secret
    end
  end
end
