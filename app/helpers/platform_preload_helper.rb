# frozen_string_literal: true

# Hacky module for managing preloaded platform objects while we migrate to being fully
# based on GraphQL.
module PlatformPreloadHelper
  include PlatformHelper

  class DataNotPrefilledError < StandardError; end

  BaseFragment = parse_query <<-'GRAPHQL'
    fragment on Node { id }
  GRAPHQL

  # Public: preload and cache the platform representation of the model objects.
  #
  # This method yields an array of global relay ids to the provided block which is responsible
  # for retrieving the data from the platform and returning the nodes.
  #
  # subjects - the model objects which respond to `global_relay_id`
  def preload_platform_objects(subjects)
    subjects.select! { |subject| subject.respond_to?(:global_relay_id) }

    subjects_by_id = subjects.index_by(&:global_relay_id)

    @preloaded_platform_data = {}

    nodes = yield subjects_by_id.keys

    nodes.each do |raw_node|
      node = BaseFragment.new(raw_node)
      @preloaded_platform_data[subjects_by_id[node.id]] = raw_node if node
    end

    nil
  end

  # Public: retrieves the platform object representation of the active record.
  #
  # NOTE: preload_platform_objects MUST be called prior to calling this method.
  #
  # Returns the platform item's node or raises DataNotPrefilledError if it is missing.
  def preloaded_platform_object(ar_item)
    item_node = preloaded_platform_data[ar_item]

    unless item_node
      raise PlatformPreloadHelper::DataNotPrefilledError,
        "expected GraphQL data to be preloaded, but wasn't: #{ar_item.class} #{ar_item.global_relay_id}"
    end

    item_node
  end

  private

  def preloaded_platform_data
    @preloaded_platform_data ||= {}
  end
end
