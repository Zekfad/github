# frozen_string_literal: true

module IssuesHelper
  include PlatformHelper

  # Generate href path to Issue.
  #
  # issue - Issue or PullRequest
  #
  # Returns String pathname.
  def issue_permalink_path(issue)
    if issue.pull_request?
      pull_request_path(issue)
    else
      issue_path(issue)
    end
  end

  def discussion_for_issue(issue)
    if show_discussions?
      Discussion.for_repository(issue.repository).converted_from_issue(issue).first
    end
  end

  def current_discussion_for_issue
    return @current_discussion_for_issue if defined?(@current_discussion_for_issue)
    @current_discussion_for_issue = discussion_for_issue(current_issue)
  end

  def issue_page_description(issue)
    return nil if issue.body_text.nil?
    truncate(issue.body_text.squish, length: 240)
  end

  def show_issue_comment_box?
    return false if current_repository_locked_for_migration?
    return false if current_discussion_for_issue
    true
  end

  def can_modify_issue?(issue)
    return false unless logged_in?
    return false if discussion_for_issue(issue)
    return true if issue.user == current_user
    current_user_can_push?
  end

  def milestone_percentage_tag(milestone)
    content_tag :span, number_to_percentage(milestone.progress_percentage.floor, precision: 0), class: "progress-percent"
  end

  def milestone_sort_label(sort, direction)
    case "#{sort}-#{direction}"
    when "due_date-asc"      then "Closest due date"
    when "due_date-desc"     then "Furthest due date"
    when "completeness-desc" then "Most complete"
    when "completeness-asc"  then "Least complete"
    when "title-asc"         then "Alphabetically"
    when "title-desc"        then "Reverse alphabetically"
    when "count-desc"        then "Most issues"
    when "count-asc"         then "Least issues"
    else "Recently updated"
    end
  end

  def label_sort(sort, direction)
    case "#{sort}-#{direction}"
    when "name-asc"   then "Alphabetically"
    when "name-desc"  then "Reverse alphabetically"
    when "count-desc" then "Most issues"
    when "count-asc"  then "Fewest issues"
    end
  end

  def issues_sort_menu_options
    [
      ["Newest", "created-desc"],
      ["Oldest", "created-asc"],
      ["Most commented", "comments-desc"],
      ["Least commented", "comments-asc"],
      ["Recently updated", "updated-desc"],
      ["Least recently updated", "updated-asc"],
    ]
  end

  def issues_sort_labels
    {
      %w[created desc] => "Newest",
      %w[created asc]  => "Oldest",
      %w[comments desc] => "Most commented",
      %w[comments asc]  => "Least commented",
      %w[updated desc]  => "Recently updated",
      %w[updated asc]   => "Least recently updated",
    }
  end

  # Issue page <title>. This is "Issues - <user>/<repo>" for index pages
  # or "<issue.title> - Issues - <user>/<repo>" for individual issue pages.
  def issue_page_title(issue = nil)
    [("#{issue.title} · Issue ##{issue.number}" if issue),
      ("Issues" if !issue),
      current_repository.name_with_owner,
    ].compact.join(" · ")
  end

  def pull_request_page_title(pull)
    attribution = pull.user ? " by #{pull.user}" : ""
    "#{pull.title}#{attribution} · Pull Request ##{pull.number} · #{current_repository.name_with_owner}"
  end

  def milestone_due_text(milestone)
    if milestone.closed?
      state = content_tag "strong" do
        "Closed "
      end
      safe_join [state, time_ago_in_words_js(milestone.closed_at)]
    elsif !milestone.due_on
      safe_join [" No due date"]
    elsif milestone.past_due?
      icon = content_tag(:span, octicon("alert"), class: "mr-1")
      content_tag(:strong,
        safe_join(
          [icon, " Past due by ", distance_of_time_in_words_to_now(milestone.due_date)],
        ),
        class: "description-warning",
        title: milestone.due_date.to_date.to_s(:long),
      )
    else
      icon = content_tag(:span, octicon("calendar"), class: "mr-1")
      safe_join [icon, " Due by ", milestone.due_date.to_date.to_s(:long)]
    end
  end

  # Figure out the extra text for an issue close event
  #
  # Examples:
  #
  #   in <code><a href="...">decafba</a></code>
  #   in <code><a href="...">user/repo@decafba</a></code>
  #   in <code>decafba</code>
  #   in <a href="...">#15</a>
  #   in <a href="...">user/repo#15</a>
  #   in #15
  #
  # Returns a String, or nil if there is no text to show
  def event_close_text(event)
    close_text = nil

    if event.commit_id
      close_text = event.commit_id[0, 7]

      if event.repository_for_commit.pullable_by?(current_user)
        close_text = "#{event.repository_for_commit.name_with_owner}@#{close_text}" if event.repository_for_commit != event.repository
        close_text = link_to close_text, commit_path(event.commit_id, event.repository_for_commit)
      end

      close_text = content_tag(:code, close_text)
    elsif issue = event.referencing_issue
      return unless issue.readable_by?(current_user)

      close_text = "##{issue.number}"
      close_text = "#{issue.repository.name_with_owner}#{close_text}" if issue.repository != event.repository

      link = self.respond_to?(:urls) ? urls.issue_path(issue) : issue
      close_text = link_to close_text, link
    end

    return unless close_text

    safe_join(["in ", close_text])
  end

  def issues_search_term_values(search_key, excluded: false)
    parsed_issues_query.select do |(key, value, negation)|
      if negation && !excluded
        # search_key, if negated, is in the form "-term".
        # the equivalent parsed_query value would be [:"term", "value", true]
        # as such, we must prepend `key` with a `-` if negation is true to correctly match negated terms
        "-#{key}" == search_key.to_s
      elsif negation || excluded
        negation && excluded && key == search_key
      else
        key == search_key
      end
    end.map { |(k, v)| v }
  end

  def issues_search_query(
    replace: {},
    append: [],
    pulls_only: false,
    repo_link: false,
    repo_name: nil,
    repo_owner_login: nil
  )
    components = parsed_issues_query.dup

    replace.each_pair do |replace_key, replace_val|
      case replace_val
      when NilClass
        components.reject! { |comp_key, _| comp_key == replace_key }
      when Hash
        components.reject! { |comp_key, comp_val|
          next unless comp_key == replace_key
          next unless replace_val.one?
          replace_val.keys.first == comp_val && replace_val.values.first.nil?
        }
      else
        components << [replace_key, replace_val]
      end
    end

    components += append

    if current_repository || repo_link
      return issues_path({
        q: Search::Queries::IssueQuery.stringify(components),
        repository: repo_link ? repo_name : nil,
        user_id: repo_link ? repo_owner_login : nil,
      }.compact)
    end

    if pulls_only
      all_pulls_path(q: Search::Queries::IssueQuery.stringify(components))
    else
      all_issues_path(q: Search::Queries::IssueQuery.stringify(components))
    end
  end

  def review_filter_menu_items
    terms = [
      ["No reviews", [[:review, "none"]]],
      ["Review required", [[:review, "required"]]],
      ["Approved review", [[:review, "approved"]]],
      ["Changes requested", [[:review, "changes-requested"]]],
    ]

    if logged_in?
      search_param = Search::Query::MACRO_ME
      terms.concat([
        ["Reviewed by you", [[:"reviewed-by", search_param]]],
        ["Not reviewed by you", [[:"-reviewed-by", search_param]]],
        ["Awaiting review from you", [[:"review-requested", search_param]]],
      ])
    end

    out = terms.map do |description, pairs|
      key = pairs.first.first
      values = pairs.map { |key, value| value }
      selected = issues_search_term_values(key) == values
      url = issues_search_query(replace: { review: nil, "reviewed-by": nil, "-reviewed-by": nil, "review-requested": nil }, append: pairs)
      {url: url, selected: selected, description: description}
    end
  end

  # These methods would be good candidates to move to a view model.
  # See https://github.com/github/github/pull/39990#issuecomment-86220009
  def showing_filtered_pulls?
    parsed_issues_query != [[:is, "pr"], [:is, "open"]]
  end

  def showing_filtered_issues?
    parsed_issues_query != [[:is, "issue"], [:is, "open"]]
  end

  # Internal: Find issues matching query or ids
  #
  # BROKEN AND NEEDS TO BE REPLACED
  #
  # Returns an Array of Issues and total Integer count.
  def issues_triage_menu_find_issues(query)
    case query
    when String
      result = Search::Queries::IssueQuery.new({
        phrase: query,
        current_user: current_user,
        repo_id: current_repository.id,
        per_page: 100,
        fields: [],
        normalizer: lambda { |results| results.map! { |h| h["_model"] }.compact },
      }).execute
      return result, result.total
    when Array
      issues = current_repository.issues.where("issues.number IN (?)", query).to_a
      return issues, issues.size
    else
      return [], 0
    end
  end

  # Public: Count how many milestones match the current search query.
  #
  # milestones - Array of Milestones
  # query      - String query or Array of issue IDs
  # &block
  #   milestone - Milestone in milestones
  #   count     - Integer count of milestones in issue query
  #   total     - Integer total number of milestones matching issue query
  #
  # Returns nothing.
  def issues_triage_menu_milestone_counts(milestones, query)
    # FIXME: Loads all Issue records into memory
    issues, total = issues_triage_menu_find_issues(query)

    # preload counts
    with_counts = {}
    milestones.each do |milestone|
      count = issues.count { |issue| issue.milestone_id == milestone.id }
      with_counts[milestone.id] = count
    end

    # selected items to the top
    milestones = milestones.partition do |milestone|
      with_counts[milestone.id] > 0
    end.flatten

    milestones.each do |milestone|
      yield milestone, with_counts[milestone.id], total
    end
  end

  # Public: Count how many assigned users match the current search query.
  #
  # users - Array of Users
  # query - String query or Array of issue IDs
  # &block
  #   user  - User in users
  #   count - Integer count of users in issue query
  #   total - Integer total number of users matching issue query
  #
  # Returns nothing.
  def issues_triage_menu_assignee_counts(users, query)
    issues, total = issues_triage_menu_find_issues(query)

    # preload assignment counts
    assignments = Assignment
      .select("assignee_id, COUNT(issue_id) AS assigned_count")
      .where(issue: issues)
      .group("assignee_id")
      .index_by(&:assignee_id)

    with_counts = users.each_with_object({}) do |user, hash|
      hash[user.login] = assignments[user.id]&.assigned_count.to_i
    end

    # selected items to the top
    users = users.partition do |user|
      with_counts[user.login] > 0
    end.flatten

    users.each do |user|
      yield user, with_counts[user.login], total
    end
  end

  # Public: Count how many labels match the current search query.
  #
  # labels - Array of Labels
  # query  - String query or Array of issue IDs
  # &block
  #   label - Label in labels
  #   count - Integer count of labels in issue query
  #   total - Integer total number of labels matching issue query
  #
  # Returns nothing.
  def issues_triage_menu_label_counts(labels, query)
    issues, total = issues_triage_menu_find_issues(query)

    issue_label_sql = Issue.github_sql.run(<<-SQL, issue_ids: issues.map(&:id))
      SELECT label_id, COUNT(issue_id) AS labeled_count
      FROM issues_labels
      WHERE issue_id IN :issue_ids
      GROUP BY label_id
    SQL

    labeled = issue_label_sql.results.to_h

    with_counts = labels.each_with_object({}) do |label, hash|
      hash[label.name] = labeled[label.id].to_i
    end

    # selected items to the top
    labels = labels.partition do |label|
      with_counts[label.name] > 0
    end.flatten

    labels.each do |label|
      yield label, with_counts[label.name], total
    end
  end

  def issues_triage_menu_hidden_fields(issues)
    case issues
    when String
      field = tag(:input,
        type: "hidden",
        name: "issues",
        value: issues,
      )
      content_tag(:div, field, class: "js-issues-triage-fields")
    when Array
      fields = []
      issues.each do |number|
        fields << tag(:input,
          type: "hidden",
          name: "issues[]",
          value: number,
        )
      end
      content_tag(:div, safe_join(fields), class: "js-issues-triage-fields")
    end
  end

  # Give me the task list link for this item.
  #
  # Returns a String.
  def task_list(object, include_left_margin: true)
    summary = TaskLists::SummaryView.new(object.task_list_summary)
    percent = number_to_percentage(((summary.complete_count / summary.item_count.to_f) * 100), precision: 0)

    content = octicon "checklist"
    content += content_tag(:span, "#{summary.complete_count} of #{summary.item_count}", class: "task-progress-counts")
    content += content_tag(:span, content_tag(:span, "", class: "progress", style: "width: #{percent}") , class: "progress-bar v-align-middle")

    content_tag(:span, content, class: "issue-meta-section task-progress #{"ml-2" if include_left_margin}")
  end

  def issue_thread_commentable?
    if !logged_in?
      false
    elsif subject = @pull || @issue
      !subject.locked_for?(current_user)
    else
      true
    end
  end

  def issue_show_partial_path(issue: nil, partial: nil)
    if issue.nil?
      show_partial_new_issue_path(partial: partial)
    elsif issue.new_record?
      options = {partial: partial, issue: {}}
      options[:issue][:user_assignee_ids] = issue.assignees.map(&:id) if issue.assignees
      options[:issue][:milestone_id] = issue.milestone_id if issue.milestone
      options[:issue][:label_ids] = issue.label_ids if issue.label_ids.any?
      if issue.projects.any?
        options[:issue_project_ids] = Hash[issue.projects.map { |project| [project.id, "on"] }]
      end
      show_partial_new_issue_path(options)
    else
      show_partial_issue_path(id: issue.number, partial: partial)
    end
  end

  # When a logged out user signs up or logs in via the new issue modal,
  # they're redirected to the new issue page
  def modal_issues_signup_path
    signup_path(return_to: new_issue_path)
  end

  def modal_issues_login_path
    login_path(return_to: new_issue_path)
  end

  def show_preview_pages_toggle_ui?(user, repository, issue)
    issue.pull_request &&
      user &&
      GitHub.flipper[:pages_preview_builds].enabled?(user) &&
      repository.writable_by?(user) &&
      repository.page&.preview_deployments_enabled?
  end

  def preview_pages_button_text(issue)
    preview_enabled = issue.repository.page.deployment_enabled_for?(issue.pull_request.head_ref)
    "#{preview_enabled ? "Disable" : "Enable"} GitHub Pages"
  end

  def show_projects_ui?(repository)
    !repository.advisory_workspace? && (
      repository.projects_enabled? ||
      show_organization_projects_ui?(repository) ||
      show_user_projects_ui?(repository)
    )
  end

  def show_organization_projects_ui?(repository)
    repository.owner.organization? && repository.owner.organization_projects_enabled?
  end

  def show_user_projects_ui?(repository)
    repository.owner.user?
  end

  def show_memexes_ui?(repository, issue)
    !repository.advisory_workspace? &&
      issue.memex_items_enabled_for_viewer?(current_user) &&
      memex_issue_sidebar_enabled?
  end

  def show_labels_ui?(repository)
    !repository.advisory_workspace?
  end

  def show_milestones_ui?(repository)
    !repository.advisory_workspace?
  end

  def show_references_ui?(repository)
    !repository.advisory_workspace?
  end

  CloserReferenceTextFragment = parse_query <<~'GRAPHQL'
    fragment on ClosedEvent {
      closable {
        ... on Issue {
          repository {
            id
          }
        }
        ... on PullRequest {
          repository {
            id
          }
        }
      }
      closer {
        ... on PullRequest {
          number
          repository {
            id
            nameWithOwner
          }
        }
        ... on Commit {
          abbreviatedOid
          repository {
            id
            nameWithOwner
          }
        }
      }
    }
  GRAPHQL

  # Figure out the extra text for an issue close event
  #
  # Examples:
  #
  #   decafba
  #   user/repo@decafba
  #   #15
  #   user/repo#15
  #
  # Returns a String, or nil if there is no text to show
  def closer_reference_text(closed_event)
    closed_event = CloserReferenceTextFragment.new(closed_event)
    identifier = if closed_event.closer.is_a?(PlatformTypes::Commit)
      closed_event.closer.abbreviated_oid
    else
      "##{closed_event.closer.number}"
    end
    if closed_event.closable.repository.id != closed_event.closer.repository.id
      safe_join([
        closed_event.closer.repository.name_with_owner,
        closed_event.closer.is_a?(PlatformTypes::Commit) ? "@" : "",
        identifier,
      ])
    else
      identifier
    end
  end

  def find_card_for_sidebar(issue:, card_id:)
    issue.associated_cards(only_for_enabled_projects: true).find_by_id(card_id)
  end

  def issue_sidebar_permissions(issue, actor, action)
    @issue_sidebar_permissions ||= begin
      permission_promises = [
        issue.async_labelable_by?(actor: actor),
        issue.async_assignable_by?(actor: actor),
        issue.async_can_set_milestone?(actor),
      ]

      labelable, assignable, milestoneable = Promise.all(permission_promises).sync

      {
        labelable: labelable,
        assignable: assignable,
        milestoneable: milestoneable,
      }
    end
    @issue_sidebar_permissions[action]
  end

  def issue_pr_state_octicon(object, state, is_draft = false)
    if object.is_a? PlatformTypes::PullRequest
      if state == PlatformTypes::PullRequestState::MERGED
        octicon("git-merge", class: "merged text-purple", title: "Merged")
      elsif state == PlatformTypes::PullRequestState::CLOSED
        octicon("git-pull-request", class: "closed text-red", title: "Closed")
      elsif is_draft
        octicon("git-pull-request", class: "closed text-gray", title: "Draft")
      else
        octicon("git-pull-request", class: "open text-green", title: "Open")
      end
    else
      if state == PlatformTypes::IssueState::OPEN
        octicon("issue-opened", class: "open text-green", title: "Open")
      else
        octicon("issue-closed", class: "closed text-red", title: "Closed")
      end
    end
  end

  def issue_pr_state_octicon_ar(object)
    if object.is_a? PullRequest
      if object.merged?
        octicon("git-merge", class: "merged", title: "Merged")
      elsif object.closed?
        octicon("git-pull-request", class: "closed", title: "Closed")
      elsif object.draft?
        octicon("git-pull-request", class: "draft", title: "Draft")
      else
        octicon("git-pull-request", class: "open", title: "Open")
      end
    else
      if object.open?
        octicon("issue-opened", class: "open", title: "Open")
      else
        octicon("issue-closed", class: "closed", title: "Closed")
      end
    end
  end

  def blank_issue_hydro_tracking(user_id, repo_id)
    hydro_click_tracking_attributes("blank_issue.click",
      actor_id: user_id,
      repository_id: repo_id,
      blank_issue_clicked: true,
    )
  end

  def close_issue_references_for(issue_or_pr:)
    @close_issue_references ||= if issue_or_pr.is_a?(PullRequest)
      issue_or_pr.close_issue_references_for(viewer: current_user, unauthorized_organization_ids: unauthorized_saml_organization_ids)
    else
      issue_or_pr.closed_by_pull_requests_references_for(viewer: current_user, unauthorized_organization_ids: unauthorized_saml_organization_ids)
    end
  end

  def track_render_partial(partial_name, additional_tags = [])
    timer = Timer.start
    yield
    timer.stop
    combined_tags = ["partial:#{partial_name}", "logged_in:#{logged_in?}"].concat(additional_tags).uniq
    GitHub.dogstats.distribution("request.render.issue.partial", timer.elapsed_ms, tags: combined_tags)
  end
end
