# frozen_string_literal: true

module DeploymentsHelper
  def deployment_description_text(state)
    case state
    when PlatformTypes::DeploymentState::PENDING, PlatformTypes::DeploymentState::ABANDONED, "IN_PROGRESS", "QUEUED"
      "requested a deployment"
    when PlatformTypes::DeploymentState::ACTIVE
      "deployed"
    when PlatformTypes::DeploymentState::INACTIVE, PlatformTypes::DeploymentState::DESTROYED
      "temporarily deployed"
    else # "failure", "error", nil?
      "had a problem deploying"
    end
  end

  def deployment_icon_classes(state)
    case state
    when PlatformTypes::DeploymentState::ERROR, PlatformTypes::DeploymentState::FAILURE
      "bg-red text-white"
    when PlatformTypes::DeploymentState::ACTIVE
      "bg-green text-white"
    when PlatformTypes::DeploymentState::IN_PROGRESS, PlatformTypes::DeploymentState::PENDING, PlatformTypes::DeploymentState::QUEUED
      "bg-yellow-dark text-white"
    else
      ""
    end
  end
end
