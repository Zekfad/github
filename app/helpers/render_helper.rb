# frozen_string_literal: true

require "active_support/core_ext/numeric"

module RenderHelper
  include BlobHelper

  # Encode the URL as a hex string to never have to worry about
  # any character encoding not matching what we can send in URL
  #
  # Returns a String
  def render_encode_url(url)
    url.unpack1("H*")
  end

  def rich_added_url(url, format, extra: {})
    params = (extra || {}).merge({enc_url: render_encode_url(url)}).to_param
    path = "/added/#{CGI.escape(format.to_s)}"
    "#{GitHub.render_host_url}#{path}?#{params}"
  end

  # Constructs a render diff url
  def rich_diff_url(diff, format, git_repo:, file_view:, file_list_view:)
    url1, url2 = raw_diff_urls(git_repo, diff)

    # Added diffs are special cased to avoid processing
    # a diff against nothing.
    if diff.added?
      extra = {
        repository_id: git_repo.try(:id),

        # Added only has b_*, since they are treated by
        # render as /view/ requests
        path: diff.b_path,
        commit: diff.b_sha,
      }
      return rich_added_url(url2, format, extra: extra)
    end

    params = {}
    params[:enc_url1] = render_encode_url(url1) if url1
    params[:enc_url2] = render_encode_url(url2) if url2
    if file_view && file_list_view && diff.binary?
      file_view.set_diff_blob_sizes(file_list_view)
      size1 = file_view.diff_base_blob_size
      size2 = file_view.diff_head_blob_size
      if !size1.zero? || !size2.zero?
        params[:size1] = size1
        params[:size2] = size2
      end
    end

    # I'm not sure how to deal with Gist diffs, or if we even support them.
    params[:repository_id] = git_repo.try(:id)

    # We'll use the diff.a_* values to match the scope we'll use in the cluster
    # case to store/fetch RenderBlobs through, which must be accessible by the
    # first URL's RawBlob token.
    params[:path] = diff.a_path
    params[:commit] = diff.a_sha

    query = params.to_param
    path = "/diff/#{CGI.escape(format.to_s)}"

    "#{GitHub.render_host_url}#{path}?#{query}"
  end

  def raw_diff_urls(git_repo, diff)
    pointer1, pointer2 = Media::Blob.pointers_from_diff(diff)

    [
      raw_diff_base_blob_url(git_repo, diff, pointer1),
      raw_diff_head_blob_url(git_repo, diff, pointer2),
    ]
  end

  def raw_diff_base_blob_url(git_repo, diff, pointer = nil)
    raw_diff_hunk_blob_url(git_repo, diff.a_path, diff.a_sha, pointer)
  end

  def raw_diff_head_blob_url(git_repo, diff, pointer = nil)
    raw_diff_hunk_blob_url(git_repo, diff.b_path, diff.b_sha, pointer)
  end

  def raw_diff_hunk_blob_url(git_repo, path, sha, pointer = nil)
    return unless path && sha

    if pointer && pointer.valid?
      TreeEntry::RenderHelper.lfs_blob_url(current_user, current_repository, sha, path)
    elsif git_repo.is_a?(Gist)
      build_raw_url(type: :gist, route_options: {
        user_id: git_repo.user_param,
        gist_id: git_repo.repo_name,
        sha: sha,
        file: path,
      })
    else
      TreeEntry::RenderHelper.raw_blob_url(current_user, current_repository, sha, Array(path).join("/"), expires_key: :render, host: GitHub.render_raw_host_name)
    end
  end
end
