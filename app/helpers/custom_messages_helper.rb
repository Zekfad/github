# frozen_string_literal: true

module CustomMessagesHelper
  # EXAMPLE USAGE
  # <%= customized(:suspended_message) %>
  def customized(type)
    return "" unless GitHub.enterprise?
    return "" unless [:sign_in_message, :sign_out_message, :suspended_message].include?(type)
    ::CustomMessages.instance.public_send(type).to_s
  end

  def default_suspended_message
    if GitHub.enterprise?
      @message = enterprise_suspended_message
    else
      @message = "Access to your account has been suspended due to a violation of our
      [Terms of Service](#{GitHub.help_url}/articles/github-terms-of-service).<br><br>
      Please [contact support](https://support.github.com/contact) for more information."
    end
  end

  def enterprise_suspended_message
    msg = "Sorry. Your account is suspended. Please check with your installation administrator.".dup
    if GitHub.support_link_not_enterprise_default?
      msg << " #{GitHub.support_link_text}"
    end
    msg
  end
end
