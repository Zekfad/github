# frozen_string_literal: true

# Shared logic for exporting audit logs in controllers
module AuditLogExportHelper
  def render_audit_log_export(export)
    return render_404 unless export.remote_object?

    GitHub.dogstats.increment("audit_log_export", tags: ["action:download"])

    response.headers["Content-Type"] = export.content_type
    response.headers["Content-Length"] = export.remote_object.size.to_s
    response.headers["Content-Disposition"] = "attachment; filename=\"#{export.human_filename}\""
    response.headers["Last-Modified"] = export.created_at.ctime
    self.response_body = Enumerator.new do |output|
      export.remote_object.get do |chunk|
        output << chunk
      end
    end
  end

  def respond_with_audit_log_export(export:, export_url:)
    respond_to do |format|
      format.json do
        if export.persisted?
          body = {
            export_url: export_url,
            job_url: job_status_url(export),
          }

          render json: body, status: 201
        else
          render nothing: true, status: 400
        end
      end

      format.all do
        render_404
      end
    end
  end
end
