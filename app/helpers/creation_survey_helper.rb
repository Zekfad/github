# frozen_string_literal: true

module CreationSurveyHelper
  def redirect_to_organization_creation_survey?(org)
   !GitHub.enterprise? && org.plan.free?
  end
end
