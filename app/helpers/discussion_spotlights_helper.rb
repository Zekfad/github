# frozen_string_literal: true

module DiscussionSpotlightsHelper
  def discussion_spotlight_gradient_background_style(color_stops)
    color1, color2 = color_stops
    gradient_style = "radial-gradient(circle farthest-side at bottom, ##{color1}, ##{color2})"
    "background-image: #{gradient_style}"
  end

  def discussion_spotlight_gradient_radio(preconfigured_color, selected: false)
    color_stops = DiscussionSpotlight.color_stops_for(preconfigured_color)
    return unless color_stops

    label_tag(
      preconfigured_color,
      "aria-label": "Background #{preconfigured_color.to_s.humanize}",
      class: "mr-1 mb-2 border border-black-fade flex-shrink-0 " \
        "discussion-spotlight-gradient btn px-0 text-center",
      style: discussion_spotlight_gradient_background_style(color_stops),
      "aria-selected": selected,
      for: "discussion_spotlight_preconfigured_color_#{preconfigured_color}"
    ) do
      safe_join([
        radio_button_tag("discussion_spotlight[preconfigured_color]", preconfigured_color, selected,
          class: "v-hidden js-discussion-spotlight-preview-trigger", style: "width: 0"),
        octicon(:check, class: "discussion-spotlight-gradient-selected-indicator text-white")
      ], "")
    end
  end

  def discussion_spotlight_pattern_radio(pattern, selected: false)
    return unless DiscussionSpotlight.patterns.key?(pattern)

    label_tag(
      pattern,
      "aria-label": pattern.humanize,
      class: "mr-2 mb-2 px-2 text-gray btn",
      "aria-selected": selected,
      for: "discussion_spotlight_pattern_#{pattern}"
    ) do
      safe_join([
        radio_button_tag("discussion_spotlight[pattern]", pattern, selected,
          class: "v-hidden js-discussion-spotlight-preview-trigger",
          style: "width: 0",
          required: true),
        octicon(pattern, height: 16)
      ], "")
    end
  end

  SPOTLIGHT_HORIZONTAL_PATTERN_COUNT = 20
  SPOTLIGHT_VERTICAL_PATTERN_COUNT = 6

  def discussion_spotlight_pattern_icons(pattern)
    return unless pattern && DiscussionSpotlight.patterns.key?(pattern)

    rows = SPOTLIGHT_VERTICAL_PATTERN_COUNT.times.map do
      content_tag(:div, class: "d-flex flex-justify-between") do
        row = SPOTLIGHT_HORIZONTAL_PATTERN_COUNT.times.map do
          octicon(pattern, height: 16, class: "mx-2")
        end
        safe_join(row, "")
      end
    end

    content_tag(:div,
      class: "discussion-spotlight-pattern-container position-absolute d-flex " \
        "flex-justify-around flex-column width-full flex-items-stretch height-full",
      style: "z-index: 0"
    ) do
      safe_join(rows, "")
    end
  end

  def discussion_spotlight_category_emoji(emoji_name)
    emoji = emoji_for(emoji_name)
    return unless emoji

    emoji_tag(emoji, alias: emoji_name)
  end

  def link_to_discussion_spotlight_category(category)
    return unless category

    link_to(discussions_list_path(discussions_q: "category:#{category.name}"), class: "d-flex text-white") do
      safe_join([
        discussion_spotlight_category_emoji(category.emoji),
        content_tag(:span, category.name, class: "d-inline-block ml-2 lh-condensed")
      ].compact, " ")
    end
  end

  def link_to_discussion_spotlight_title(discussion, total_spotlights:)
    max_width = if total_spotlights > 2
      200
    else
      318
    end
    link_to(discussion.title, discussion_path(discussion),
      class: "text-white f2 mt-n1 lh-condensed css-truncate-target",
      style: "max-width: #{max_width}px")
  end

  def link_to_discussion_spotlight_author(author)
    link_to(user_path(author), class: "f5 d-flex flex-items-center text-white mt-2") do
      safe_join([
        avatar_for(author, 24, class: "mr-2 avatar circle"),
        content_tag(:span, author, class: "d-none d-md-inline-block mr-1"),
        bot_identifier(author)
      ], " ")
    end
  end

  def discussion_spotlight_emoji(emoji_name)
    emoji = emoji_for(emoji_name)
    return unless emoji

    emoji_tag(emoji, alias: emoji_name, class: "position-absolute discussion-spotlight-emoji")
  end

  def discussion_spotlight_preview_style(preconfigured_color:)
    if preconfigured_color
      color_stops = DiscussionSpotlight.color_stops_for(preconfigured_color)
      discussion_spotlight_gradient_background_style(color_stops) if color_stops
    end
  end
end
