# frozen_string_literal: true

module SecurityKeyHelper
  # Options that need to be passed to `form_tag` for U2F/WebAuthn authentication.
  #
  # Returns a Hash.
  def u2f_auth_form_options(user)
    {
      :class                       => "js-u2f-auth-form",
      "data-webauthn-sign-request" => webauthn_sign_request(user: user).to_json,
    }
  end

  # Options that need to be passed to `form_tag` for U2F/WebAuthn registration.
  #
  # Returns a Hash.
  def u2f_register_form_options
    {
      :class                           => "add-u2f-registration-form js-add-u2f-registration-form",
      "data-webauthn-register-request" => webauthn_register_request.to_json,
    }
  end
end
