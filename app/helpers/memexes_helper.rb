# frozen_string_literal: true

module MemexesHelper
  def self.included(base)
    return unless base.respond_to? :helper_method

    base.helper_method(
      :add_memex_csp_exceptions,
      :html_safe_json_island,
      :memex_asset_urls,
      :memex_env,
      :use_local_memex_server?,
    )
  end

  # The String environment which memex-frontend should use.
  def memex_env
    if use_local_memex_server?
      "development"
    else
      "production"
    end
  end

  # Should the Memex frontend JS be loaded from the frontend dev server or
  # from the vendored JS?
  def use_local_memex_server?
    Rails.env.development? && local_server_endpoint.present?
  end

  def memex_asset_urls
    # Don't bother making the network call to load the asset manifest if we're
    # not gonna end up serving them.
    return [] unless use_local_memex_server?

    begin
      JSON.parse(assets_payload).
        select { |key| key.end_with?(".js") }.
        map { |key, value| "#{local_server_endpoint}#{value}" }
    rescue JSON::ParserError
      []
    end
  end

  def add_memex_csp_exceptions
    if use_local_memex_server?
      SecureHeaders.append_content_security_policy_directives(
        request,
        script_src: [local_server_endpoint],
      )
    end
  end

  def require_memex_enabled
    render_404 unless memex_beta_enabled?
  end

  def require_this_memex
    render_404 unless this_memex
  end

  def require_verified_email
    unless current_user.verified_emails?
      render_json_error(
        error: "You must verify an email address in order to take that action.",
        status: :unauthorized,
      )
    end
  end

  def verify_content_params(params)
    content_type = params[:content_type]
    content = params[:content]

    if MemexProjectItem::VALID_CONTENT_TYPES.include?(content_type)
      missing_params = []
      missing_params << "content.title" if content_type == DraftIssue.name && !content[:title]
      missing_params << "content.id" if content_type != DraftIssue.name && !content[:id]
      missing_params << "content.repositoryId" if content_type != DraftIssue.name && !content[:repository_id]

      if missing_params.any?
        render_json_error(
          error: "Must provide #{missing_params.join(" and ")} to create item of type #{content_type}",
          status: :unprocessable_entity,
        )
      end
    elsif !content_type
      render_json_error(
        error: "You must provide a content_type",
        status: :unprocessable_entity,
      )
    else
      render_json_error(
        error: "Cannot create an item of type #{content_type}",
        status: :unprocessable_entity,
      )
    end
  end

  def find_content(repository:, content_type:, content_id:)
    case content_type
    when Issue.name
      repository.issues.find_by(id: content_id)
    when PullRequest.name
      repository.pull_requests.find_by(id: content_id)
    else
      nil
    end
  end

  def html_safe_json_island(id, data)
    return unless data
    content_tag(
      :script,
      json_escape(camelize_keys(data).to_json).html_safe, # rubocop:disable Rails/OutputSafety
      type: "application/json",
      id: id,
    )
  end

  def camelize_keys(data)
    case data
    when Hash
      data.deep_transform_keys { |k| k.to_s.camelize(:lower) }
    when Array
      data.map { |h| camelize_keys(h) }
    end
  end

  def render_json_error(error:, status:)
    render(json: { errors: [error] }, status: status) # rubocop:disable GitHub/RailsViewRenderLiteral
  end

  def underscored_params
    @underscored_params ||= ActionController::Parameters.new(
      params.permit!.to_h.deep_transform_keys(&:underscore),
    )
  end

  private

  def local_server_endpoint
    ENV["MEMEX_ENDPOINT"]
  end

  def assets_payload
    assets_uri = URI(
      "#{local_server_endpoint}/assets/manifest.json",
    )

    http = Net::HTTP.new(assets_uri.host, assets_uri.port)
    http.use_ssl = false
    http.open_timeout = 5
    http.read_timeout = 5

    http.request(Net::HTTP::Get.new(assets_uri)).body
  end
end
