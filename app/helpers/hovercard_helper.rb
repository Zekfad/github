# frozen_string_literal: true

module HovercardHelper
  extend self
  extend DashboardHelper
  include DashboardHelper
  include PlatformHelper

  ORG_SSO_PATH = "%{path}{?return_to}"
  ORG_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/orgs/{login}/hovercard")
  USER_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/users/{login}/hovercard")
  ISSUE_PR_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/hovercard{?query*}")
  TEAM_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/orgs/{org}/teams/{slug}/hovercard")
  ADVISORY_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/advisories/{ghsa_id}/hovercard")
  DISCUSSION_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/{owner}/{name}/discussions/{number}/hovercard{?query*}")
  ACV_BADGE_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/users/{login}/acv/hovercard")
  CWE_HOVERCARD_PATH_TEMPLATE = Addressable::Template.new("/{owner}/{name}/security/code-scanning/cwes/{id}/hovercard")

  def review_status_octicon_class(review_decision)
    case review_decision
    when PlatformTypes::PullRequestReviewDecision::CHANGES_REQUESTED
      "text-red"
    when PlatformTypes::PullRequestReviewDecision::APPROVED
      "text-green"
    when PlatformTypes::PullRequestReviewDecision::REVIEW_REQUIRED
      "text-gray"
    end
  end

  DataAttributesFragment = parse_query <<-'GRAPHQL'
    fragment IssueOrPullRequest on IssueOrPullRequest {
      ... on UniformResourceLocatable {
        resourcePath
      }
    }
    fragment User on User {
      databaseId
      login
    }
    fragment Team on Team {
      organization {
        login
      }
      slug
    }
    fragment Repository on Repository {
      name
      owner {
        login
      }
    }
    fragment RepositoryOwner on RepositoryOwner {
      databaseId
      login
    }
    fragment EnterpriseUserAccount on EnterpriseUserAccount {
      user {
        databaseId
        login
      }
    }
  GRAPHQL

  def hovercard_data_attributes_for_repository(obj)
    owner, name = if obj.is_a?(PlatformTypes::Repository)
      fragment = DataAttributesFragment::Repository.new(obj)
      [fragment.owner.login, fragment.name]
    elsif obj.instance_of?(Repository)
      [obj.owner.login, obj.name]
    end

    hovercard_data_attributes_for_repo_owner_and_name(owner, name)
  end

  def hovercard_data_attributes_for_repo_owner_and_name(owner_login, repo_name)
    return {} unless repo_name && owner_login

    {
      "hovercard-type" => "repository",
      "hovercard-url" =>  Rails.application.routes.url_helpers.repository_hovercard_path(owner_login, repo_name),
    }
  end

  # Public: Shortcut to build data attributes for a hovercard representing the owner of a repo - either user or org.
  #
  # obj - Object, either an AR model or a GraphQL object, representing either a User or Org
  #
  # Returns a hash that can be passed as the `data` argument to a rails tag helper
  # Returns an empty hash if the object is a different type
  def hovercard_data_attributes_for_repository_owner(obj)
    user_login =
      case
      when obj.is_a?(PlatformTypes::User) then DataAttributesFragment::RepositoryOwner.new(obj).login
      when obj.instance_of?(User) then obj.login
      end

    return hovercard_data_attributes_for_user_login(user_login) if user_login

    org_login =
      case
      when obj.is_a?(PlatformTypes::Organization) then DataAttributesFragment::RepositoryOwner.new(obj).login
      when obj.instance_of?(Organization) then obj.login
      end

    return hovercard_data_attributes_for_org(login: org_login) if org_login

    {}
  end

  # Public: Build data attributes to add hovercard support to a user link
  #
  # obj - The user instance, either an AR model or a GraphQL object
  #
  # Returns a hash that can be passed as the `data` argument to a rails tag helper
  # Returns an empty hash if the object is a different type
  def hovercard_data_attributes_for_user(obj, tracking: true)
    user_login =
      case
      when obj.is_a?(PlatformTypes::User) then DataAttributesFragment::User.new(obj).login
      when obj.instance_of?(User) then obj.login
      end

    return {} unless user_login

    hovercard_data_attributes_for_user_login(user_login, tracking: tracking)
  end

  # Public: Build data attributes to add hovercard support to a business user
  # account link.
  #
  # obj - The user instance, either a BusinessUserAccount AR model or a
  #   Platform::Objects::EnterpriseUserAccount GraphQL object.
  #
  # Returns a hash that can be passed as the `data` argument to a rails tag helper
  # Returns an empty hash if the object is a different type
  def hovercard_data_attributes_for_business_user_account(obj, tracking: true)
    user_login =
      case obj
      when PlatformTypes::EnterpriseUserAccount
        DataAttributesFragment::EnterpriseUserAccount.new(obj)&.user&.login
      when BusinessUserAccount
        obj&.user&.login
      end

    return {} unless user_login

    hovercard_data_attributes_for_user_login(user_login, tracking: tracking)
  end

  # Return the data attributes necessary to add hovercard support for a given
  # user_login
  #
  # user_login - Login of the user to generate data attrs for
  def hovercard_data_attributes_for_user_login(user_login, tracking: true)
    return {} if !user_login || atom_feed?

    data = {
      "hovercard-type" => "user",
      "hovercard-url" => user_hovercard_path(user_login: user_login),
    }

    if tracking
      data.merge!(
        "octo-click" => "hovercard-link-click",
        "octo-dimensions" => "link_type:self",
      )
    end

    data
  end

  def hovercard_data_attributes_for_team(obj)
    org_login, team_slug = if obj.is_a?(PlatformTypes::Team)
      fragment = DataAttributesFragment::Team.new(obj)
      [fragment.organization.login, fragment.slug]
    elsif obj.instance_of?(Team)
      [obj.organization.login, obj.slug]
    end

    hovercard_data_attributes_for_org_and_team(org_login, team_slug)
  end

  def hovercard_data_attributes_for_discussion(repo_owner, repo_name, number, comment_id: nil)
    return {} unless repo_owner.present? && repo_name.present? && number.present?

    {
      "hovercard-type" => "discussion",
      "hovercard-url" => hovercard_url_for_discussion(repo_owner,
        repo_name, number, comment_id: comment_id),
    }
  end

  def hovercard_url_for_discussion(repo_owner, repo_name, number, comment_id: nil)
    query = {}
    query[:comment_id] = comment_id if comment_id
    DISCUSSION_HOVERCARD_PATH_TEMPLATE.expand(owner: repo_owner, name: repo_name,
      number: number, query: query).to_s
  end

  def hovercard_data_attributes_for_org_and_team(org_login, team_slug)
    return {} unless org_login.present? && team_slug.present?

    {
      "hovercard-type" => "team",
      "hovercard-url" => hovercard_url_for_org_and_team(org_login, team_slug),
    }
  end

  def hovercard_url_for_org_and_team(org_login, team_slug)
    TEAM_HOVERCARD_PATH_TEMPLATE.expand(org: org_login, slug: team_slug).to_s
  end

  def hovercard_data_attributes_for_acv_badge(user_login)
    return {} if !user_login
    {
      "hovercard-type" => "acv_badge",
      "hovercard-url" => hovercard_url_for_acv_badge(user_login),
    }
  end

  def hovercard_url_for_acv_badge(user_login)
    ACV_BADGE_HOVERCARD_PATH_TEMPLATE.expand(login: user_login).to_s
  end

  # Public: get the hovercard data attributes for an Issue or PullRequest
  #
  # obj - Issue or PullRequest, either AR model or GraphQL instance
  # comment_id - optional Integer database ID for an IssueComment on the given issue or pull request
  #
  # Examples
  #
  #   hovercard_data_attributes_for_issue_or_pr(pull_request)
  #   # => { hovercard_url: "/owner/repo/pull/123/hovercard", hovercard_type: "pull_request" }
  #
  #   hovercard_data_attributes_for_issue_or_pr(issue)
  #   # => { hovercard_url: "/owner/repo/issues/123/hovercard", hovercard_type: "issue" }
  #
  #   hovercard_data_attributes_for_issue_or_pr(pull_request.issue)
  #   # => { hovercard_url: "/owner/repo/pull/123/hovercard", hovercard_type: "pull_request" }
  #
  #   # To properly set data attributes on a tag, this should be used like so:
  #   content_tag(:a, data: hovercard_data_attributes_for_issue_or_pr(issue)) do
  #     # link content
  #   end
  #
  # Returns a hash of attributes to be passed as a rails tag helper's `data` keyword
  def hovercard_data_attributes_for_issue_or_pr(obj, comment_id: nil, comment_type: nil)
    hovercard_type =
      case obj
      when PlatformTypes::Issue then "issue"
      when Issue then obj.pull_request? ? "pull_request" : "issue"
      when PlatformTypes::PullRequest, PullRequest then "pull_request"
      else
        raise "Not an issue or a pull request: #{obj.class}"
      end

    issue_or_pr_path = if obj.is_a?(PlatformTypes::IssueOrPullRequest)
      DataAttributesFragment::IssueOrPullRequest.new(obj).resource_path
    else
      obj.async_path_uri.sync
    end

    query = {}
    query[:comment_id] = comment_id if comment_id
    query[:comment_type] = comment_type if comment_type
    hovercard_url = "#{issue_or_pr_path}#{ISSUE_PR_HOVERCARD_PATH_TEMPLATE.expand(query: query)}"

    { "hovercard-type" => hovercard_type, "hovercard-url" => hovercard_url }
  end

  # Can't use path helpers because hovercard_data_attributes_for_user
  # is used in Goomba pipeline, where we don't have a `request` instance
  def user_hovercard_path(user_login:)
    template_args = { login: user_login }
    USER_HOVERCARD_PATH_TEMPLATE.expand(template_args).to_s
  end

  # Public: Build data attributes to add hovercard support to an organization link
  #
  # login - The login for the organization as a String
  #
  # Returns a hash that can be passed as the `data` argument to a rails tag helper
  # Returns an empty hash if `login` is `nil`
  def hovercard_data_attributes_for_org(login:)
    return {} unless login

    hovercard_url = ORG_HOVERCARD_PATH_TEMPLATE.expand(login: login).to_s
    { "hovercard-type" => "organization", "hovercard-url" => hovercard_url }
  end

  # Public: returns the sso path with the given return_to query param
  #
  # Returns a String url
  def org_sso_path(path:, return_to: nil)
    Addressable::Template.new(ORG_SSO_PATH % {path: path}).expand(return_to: return_to)
  end

  def hovercard_data_attributes_for_commit(commit_url:)
    return {} if atom_feed?

    {
      "hovercard-type" => "commit",
      "hovercard-url" => [commit_url, "hovercard"].join("/"),
    }
  end

  ProjectAttributesFragment = parse_query <<-'GRAPHQL'
    fragment Project on Project {
      number
      resourcePath
    }
  GRAPHQL

  def hovercard_data_attributes_for_project(project:)
    hovercard_url = nil

    if project.is_a?(PlatformTypes::Project)
      project = ProjectAttributesFragment::Project.new(project)
      hovercard_url = "#{project.resource_path}/hovercard"
    elsif project.is_a?(Project)
      owner = project.owner
      hovercard_url = if owner.is_a?(Organization)
        org_project_hovercard_path(org: owner, number: project)
      elsif owner.is_a?(User)
        user_project_hovercard_path(user_id: owner, number: project)
      elsif owner.is_a?(Repository)
        "/#{owner.nwo}/projects/#{project.number}/hovercard"
      end
    end

    {
      "hovercard-type": "project",
      "hovercard-url": hovercard_url,
    }
  end

  def hovercard_data_attributes_for_advisory(ghsa_id:)
    {
      "hovercard-type": "advisory",
      "hovercard-url":  ADVISORY_HOVERCARD_PATH_TEMPLATE.expand(ghsa_id: ghsa_id).to_s,
    }
  end

  def hovercard_data_attributes_for_cwe(repository:, id:)
    {
      "hovercard-type": "cwe",
      "hovercard-url":  CWE_HOVERCARD_PATH_TEMPLATE.expand(owner: repository.owner.login, name: repository.name, id: id).to_s
    }
  end
end
