# frozen_string_literal: true

module GeneratedPagesHelper
  def link_to_theme(theme, url_params, *args, &block)
    href = generated_pages_preview_path(url_params)
    args_options_index = block_given? ? 0 : 1
    args[args_options_index] ||= {}
    args[args_options_index].merge!(
      "data-theme-name": theme.name,
      "data-theme-slug": theme.slug,
    )
    args.insert args_options_index, href

    link_to *args, &block
  end
end
