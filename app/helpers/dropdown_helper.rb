# frozen_string_literal: true

module DropdownHelper
  # Internal: Define symbol shorthand to direction class name.
  DROPDOWN_DIRECTIONS = {
    w: "dropdown-menu-w",
    e: "dropdown-menu-e",
    s: "dropdown-menu-s",
    sw: "dropdown-menu-sw",
    se: "dropdown-menu-se",

    west: "dropdown-menu-w",
    east: "dropdown-menu-e",
    southwest: "dropdown-menu-sw",
    southeast: "dropdown-menu-se",
  }

  # Public: Get dropdown class name for direction.
  #
  # See dropdown.scss
  #
  # direction - Symbol direction in DROPDOWN_DIRECTIONS.
  #
  # Returns String class name.
  def dropdown_menu_class(direction)
    klass = DROPDOWN_DIRECTIONS[direction.to_sym]

    unless klass
      raise ArgumentError, "unknown dropdown direction: #{direction.inspect}"
    end

    klass
  end
end
