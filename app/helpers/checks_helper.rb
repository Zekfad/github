# frozen_string_literal: true

# Frontend check related helper methods.
module ChecksHelper
  CONCLUSION_DESCRIPTIONS = {
    "timed_out"       => "This check timed out",
    "failure"         => "This check failed",
    "neutral"         => "This check was neutral",
    "success"         => "This check passed",
    "cancelled"       => "This check was cancelled",
    "action_required" => "This check requires action with the application",
    "skipped"         => "This check was skipped",
    "stale"           => "This check was marked stale by GitHub because it took too long",
  }.freeze

  CONCLUSION_ICONS = {
    "timed_out"       => "x",
    "failure"         => "x",
    "neutral"         => "primitive-square",
    "success"         => "check",
    "cancelled"       => "stop",
    "action_required" => "alert",
    "skipped"         => "skip",
    "stale"           => "issue-reopened",
  }.freeze

  CONCLUSION_ICONS_CLASS = {
    "timed_out"       => "text-red selected-color-white",
    "failure"         => "text-red selected-color-white",
    "neutral"         => "neutral-check selected-color-white",
    "success"         => "text-green selected-color-white",
    "cancelled"       => "neutral-check selected-color-white",
    "action_required" => "text-red selected-color-white",
    "skipped"         => "text-gray-light selected-color-white",
    "stale"           => "neutral-check selected-color-white",
  }.freeze

  # Produces an appropriately sized check output image URL.
  #
  # original_url  - Original image URL you want to display.
  # proxied       - Whether the returned URL should be proxied through our image proxy service.
  #                 Use this if the URL is going to be used to insert an img tag, otherwise
  #                 our Content Security Policy will block the image.
  #
  # Returns a String.
  def check_run_image_url_for(original_url, proxied: true)
    url = original_url
    if proxied && url.present?
      GitHub::HTML::CamoFilter.asset_proxy_url(url)
    else
      url
    end
  end

  def check_run_state_description(check_run)
    return "This check is #{check_run.status.humanize(capitalize: false)}" if check_run.conclusion.nil?
    CONCLUSION_DESCRIPTIONS[check_run.conclusion]
  end

  def check_run_state_icon(check_run)
    return "primitive-dot" if check_run.conclusion.nil?
    CONCLUSION_ICONS[check_run.conclusion]
  end

  def check_run_state_icon_class(check_run)
    return "text-yellow selected-color-white" if check_run.conclusion.nil?
    CONCLUSION_ICONS_CLASS[check_run.conclusion]
  end
end
