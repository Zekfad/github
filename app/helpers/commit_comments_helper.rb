# frozen_string_literal: true

module CommitCommentsHelper
  include PlatformHelper

  # Outputs a short linked line pointing to the comment. Useful for comment
  # headers
  def commit_comment_shortline(comment)
    result = []

    commit_url = commit_path(comment.commit_id, comment.repository)

    if comment.inline?
      # FIXME: this duplicates logic from Comments::ShowView#dom_id
      url = "#{commit_url}#commitcomment-#{comment.id}"
      inline_link = link_to(comment.path, url)
      result << content_tag(:code, inline_link)
      result << " in "
    elsif comment.path.present?
      url = "#{commit_url}/#{comment.path}"
      path_link = link_to(comment.path, url)
      result << content_tag(:code, path_link)
      result << " in "
    end

    short_sha  = link_to(comment.commit_id[0, 7], commit_url)
    result    << content_tag(:code, short_sha)

    safe_join(result)
  end

  def commit_comment_action_text(comment)
    if comment.thread && comment.thread.comments.first != comment
      "replied"
    else
      safe_join(["commented on ", diff_comment_shortline(comment)])
    end
  end

  CommitCommentActionTextFragment = parse_query <<-'GRAPHQL'
    fragment on CommitComment {
      databaseId
      path
      position
      resourcePath
      commit {
        url
        abbreviatedOid
      }
    }
  GRAPHQL

  def graphql_commit_comment_action_text(comment, reply: false)
    return "replied" if reply

    comment = CommitCommentActionTextFragment.new(comment)

    result = ["commented on "]

    if comment.path
      link = if comment.position
        link_to(comment.path, comment.resource_path.to_s)
      else
        link_to(comment.path, "#{comment.commit.url}/#{comment.path}")
      end

      result << content_tag(:code, link)
      result << " in "
    end

    short_sha = link_to(comment.commit.abbreviated_oid, comment.commit.url.to_s)
    result << content_tag(:code, short_sha)

    safe_join(result)
  end

  # Returns a url suitable for ajax requests to get the autocomplete suggestions for a repo
  # (i.e., members of teams with access to the repo, users that have committed to the repo.).
  # This is used for our comment suggester.
  #
  # Deprecated: Call suggestions_path directly instead so this can be removed.
  def suggestions_url(repository, subject)
    if subject
      subject_type = subject.class.name.underscore
      subject_id = subject.suggestion_id if subject.respond_to?(:suggestion_id)
    end
    suggestions_path(user_id: repository.owner.login, repository: repository.name,
                     subject_type: subject_type, subject_id: subject_id)
  end
end
