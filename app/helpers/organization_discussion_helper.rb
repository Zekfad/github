# frozen_string_literal: true

module OrganizationDiscussionHelper
  def org_discussion_date(date)
    now = Time.zone.now
    if now.year == date.year
      date.strftime("%b %e")
    else
      date.strftime("%b %e, %Y")
    end
  end
end
