# frozen_string_literal: true
# Tweak environment
ENV["RACK_ENV"] ||= ENV["RAILS_ENV"]
ENV["GITAUTH"] = "1"

# Load basics
require_relative "config/application"
require "failbot/middleware"
require "flipper/middleware/setup_env"
require "flipper/middleware/memoizer"
require "rack/process_utilization"
require "rack/octotracer"
require "rack/warmup"
require "rack/reloader"
require "rack/request_id"
require "mysql_failsafe"
require "pinglish"

# Load all libraries and models.
GitHub::Application.config.eager_load_paths = [
  Rails.root.join("lib"),
  Rails.root.join("app/models")
]

MysqlFailsafe.failsafe("Application#initialize!") do
  GitHub::Application.initialize!
end

# Records time each process spends actively processing a request vs. being idle
# waiting on a request.
use Rack::ProcessUtilization, "gitauth", GitHub.current_sha[0, 7],
  :stats => GitHub.create_statsd(namespace: "gitauth"),
  :dogstats => GitHub.dogstats

# Records stats about each request. Keep this near the top of this file as
# anything above it won't count toward our request timing metrics.  Depends
# on `Rack::ProcessUtilization` for GC info
use GitHub::Middleware::Stats

# Tell Failbot not to include sensitive parameters from uncaught exceptions.
# This needs to come *before* Failbot::Rescuer.
use Rack::Config do |env|
  env["filter_words"] = GitHub.filtered_params + [:otp]
end

use Failbot::Rescuer, :app => "github"

# This middleware resets `GitHub.context`, which stores global context for each
# request. This should happen early in the chain to avoid wiping out any context
# set by other middleware.
use GitHub::ContextMiddleware

# Records the rack env from the last request so that the kill trap
# introspection can report it to failbot.
use GitHub::RecordRackEnvMiddleware if defined? Unicorn::WorkerTimeout

# Enable and reset the local per-request cache
use GitHub::Cache::ResetMiddleware

# Enable Flipper feature memoization
flipper_block = lambda { GitHub.flipper }
use Flipper::Middleware::SetupEnv, flipper_block
use Flipper::Middleware::Memoizer

Marginalia.application_name = "gitauth"
Marginalia::Comment.components = [:application, :category]
module Marginalia
  module Comment
    def self.category
      "gitauth"
    end
  end
end

use Rack::OctoTracer,
    tracer: GitHub.tracer,
    enable_if: ->(env) { GitHub.tracing_enabled? },
    include_urls: true

# Enable Rack middleware for capturing (or generating) request id's
use Rack::RequestId

use Rack::RequestLogger

# Automatically reload for changes during development
if Rails.development?
  use Rack::Reloader
end

# Serve a 200 on /_ping once we've fully initialized
use Pinglish do |ping|
  ping.check do
    !GitHub.unicorn_master_start_time.nil?
  end
end

run GitHub::RepoPermissions

if ENV["RACK_ENV"] != "development" || ENV["PRELOAD"]
  warmup do |app|
    MysqlFailsafe.failsafe("WARMUP /_gitauth") do
      client = Rack::MockRequest.new(app)
      client.post("/_gitauth", :params => {"action" => "read", "proto" => "git", "path" => "gist/c4f4a5e127016bbcb69c.git"})
    end unless GitHub.enterprise?
    GC.start
  end
end

GitHub.unicorn_master_pid ||= Process.pid
GitHub.unicorn_master_start_time = Time.now.utc
GitHub.stats.increment("unicorn.master.birth") if GitHub.enterprise?
