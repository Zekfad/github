#!/bin/sh
#/ Usage: git ref-contains <oid>
#/
#/ Output the list of refs containing the specified commit. Exit with
#/ retcode 0 if any such references were found; otherwise exit with
#/ retcode 1.
#/
#/ NOTE: This script is meant to be used for investigative matters,
#/ not for code.
#/
#/ See https://github.com/github/github/pull/15504 and
#/ https://githubber.com/article/crafts/engineering/support/deleting-and-cleaning-up-a-pr-pull-request-from-a-repository

set -e

# Show usage.
usage () {
    grep '^#/' <"$0" | cut -c 4-
}

if test $# -ne 1
then
    echo >&2 "$(basename "$0") requires exactly one argument"
    usage >&2
    exit 2
fi

if test "$1" = "--help"
then
    usage
    exit 0
fi

if ! oid="$(git rev-parse --verify --quiet "$1")"
then
    echo >&2 "error: '$1' is not an object in this repository"
    exit 1
fi

type="$(git cat-file -t "$oid")"

if test "$oid" != "$1"
then
    name="$type '$1' ($oid)"
else
    name="$type $oid"
fi

if test "$type" = "tag"
then
    tagoid="$oid"
    if ! oid="$(git rev-parse --verify --quiet "$1^{}")"
    then
        echo >&2 "error: $name is a tag, but cannot be dereferenced"
        exit 1
    fi

    type="$(git cat-file -t "$oid")"
    name="tag $name → $type $oid"
fi

case "$type" in
commit)
    git for-each-ref --format='%(refname)' --contains="$oid" |
        sort |
        grep . ||
        {
            echo >&2 "No references contain $name"
            exit 1
        }
    ;;
tree)
    echo >&2 "error: $name is not a commit-ish or blob"
    exit 2
    ;;
blob)
    # This recipe would mostly also work for trees, but it doesn't
    # seem to be reliable (for example, it misses the top-level tree).
    tmp=$(mktemp -t git-ref-contains.XXXXX)
    trap 'rm -rf "$tmp"' 0

    git rev-list --all |
        git diff-tree --stdin -m -s --root --find-object="$oid" >"$tmp"

    # Guard against `xargs` getting confused by zero results. (If we
    # knew we were using GNU xargs, we could use `--no-run-if-empty`,
    # but we don't.)
    if test -s "$tmp"
    then
        sed 's/^/--contains=/' "$tmp" |
            xargs git for-each-ref --format='%(refname)' |
            sort -u
    fi |
    grep . ||
    {
        echo >&2 "No references contain $name"
        exit 1
    }
    ;;
*)
    echo >&2 "error: '$1' is not an object in this repository"
    exit 1
    ;;
esac
