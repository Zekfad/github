// @ts-check
/* eslint sort-imports: off */

/**
 * This is the Rollup configuration for building our JS files into packaged
 * bundles.
 *
 * Given a set of entry files (suffixed `-bootstrap`) Rollup reads the code,
 * resolves any `import` statements and builds a graph which it then compiles
 * down into a set of static bundles. Rollup has plugins for parsing,
 * transformation and output which we use extensively.
 */

/**
 * These are all of the off-the-shelf plugins we use for Rollup.
 * We also have some custom plugins which are written inline in the configuration bellow.
 */

/**
 * Common is used to resolve commonjs (Node.js style `require`) files in the
 * correct manner. Rollup does not come with CommonJS support out of the box,
 * so this plugin is needed.
 */
import common from '@rollup/plugin-commonjs'

/**
 * Resolve is used to resolve import statements into their relative file
 * paths, so - essentially - a module specifier like `foo` becomes
 * `./node_modules/foo/index.js`. Rollup does not come with module resolution
 * out of the box, and only supports relative file paths by default. This
 * plugin gives us support for "bare style" imports.
 */
import resolve from '@rollup/plugin-node-resolve'

/**
 * Terser is a minifier. It runs safe transforms on the code like shrinking
 * variable names and removing whitespace.  Minifying JavaScript results in
 * smaller bundle sizes, which means shorter download times and a better GitHub
 * experience.
 */
import {terser} from 'rollup-plugin-terser'

import typescript from '@rollup/plugin-typescript'

import lockfile from 'rollup-plugin-lockfile'

import path from 'path'
import {mkdirSync, readFileSync, readdirSync, symlinkSync, unlinkSync, writeFileSync} from 'fs'
import {createHash} from 'crypto'

const pkgJson = JSON.parse(readFileSync('./package.json', 'utf8'))
const mainBundles = new Set(Object.keys(pkgJson['main-bundles']))

/** @param {string} key */
function popBundle(key) {
  if (mainBundles.delete(key)) return key
  throw new Error(`${key} not found in package.json main-bundles`)
}

/**
 * All available modules are iterated over to speed up path resolution in Node.
 * We have a good understanding that `import '@foo/bar'` will resolve to
 * `import 'node_modules/@foo/bar` and so generating this list quickly ahead of
 * time saves a lot of time with subsequent compiles.
 *
 * @type {string[]}
 */
const modules = readdirSync('node_modules').flatMap(module => {
  // Ignore the `.bin` folder
  if (module.startsWith('.')) {
    return []
    // Expand `@foo/bar` scoped modules
  } else if (module.startsWith('@')) {
    return readdirSync(`node_modules/${module}`).map(d => `${module}/${d}`)
  } else {
    return module
  }
})

/**
 * Rollup will try to split code into various "chunks", the algorithm is quite
 * complex but a general rule of thumb is "if a module is imported more than
 * once it will be split into a chunk".
 *
 * Rollup also has a mechanism when you can manually declare a module should be
 * split out into its own, named chunk.
 *
 * Currently we don't support chunking because we're missing a few
 * infrastructure pieces, so we're using the manual declaration feature to tell
 * Rollup that every chunk candidate is actually to be manually chunked into
 * the "frameworks" or "vendor" bundles.
 *
 * This array is list of chunk file names whose module contents are hard coded
 * in package.json.
 *
 * @type {string[]}
 */
const allowedChunkFiles = [
  ...Object.keys(pkgJson['named-chunks']),
  'drag-drop.js',
  'gist-vendor.js',
  'image-crop-element-loader.js',
  'jump-to.js',
  'profile-pins-element.js',
  'randomColor.js',
  'Sortable.js',
  'tweetsodium.js',
  'user-status-submit.js'
]

const directory = process.env.DESTINATION || 'public/assets'

const normaliseEntryName =
  /**
   * @param {string} entry
   * @returns {string}
   */
  entry => entry.replace(/-bootstrap/, '')

/** @type {Map<string,string>} */
const symlinkEntries = new Map()

const options = {
  /**
   * @see {@link https://rollupjs.org/guide/en/#onwarn}
   *
   * The onwarn lifecycle hook allows us to hook into warnings from the
   * compile. There are a some warnings we are happy to ignore to keep the logs
   * quite clean, and a some which we want to stop the world for.
   *
   *  - UNRESOLVED_IMPORT - This needs to stop the build because we don't want
   *  to produce a bundle with missing dependencies (which rollup allows by
   *  default).
   *
   * - THIS_IS_UNDEFINED - An annoying error, we ideally want to refactor these
   *   out. Streamline the error message into something readable.
   *
   *  - CIRCULAR_DEPENDENCY - We care about these for internal modules, but
   *  some node modules have circular dependencies and we don't really care
   *  about this.
   *
   * @param {string|import('rollup').RollupWarning} warning
   *
   * @returns {void}
   */
  onwarn: (warning /*: RollupWarning*/) => {
    if (typeof warning === 'string') return
    const {code = '', message, loc = {file: '', line: '', column: ''}} = warning
    if (code === 'UNRESOLVED_IMPORT') throw new Error(`refusing to build because ${message}`)
    if (code === 'THIS_IS_UNDEFINED') {
      return console.error(`Module ${loc.file || ''} uses \`this\` as a global. This should be refactored`)
    }
    if (code === 'CIRCULAR_DEPENDENCY') {
      if (message.startsWith('Circular dependency: node_modules/')) return
      return console.error(message)
    }
    const locString = loc ? ` ${loc.file || ''}:${loc.line}:${loc.column}` : ''
    console.error(`Rollup:${code}${locString} ${message}`)
  },

  /**
   * @see {@link https://rollupjs.org/guide/en/#chunkgroupingsize}
   *
   * This is used by Rollup to determine how big each "chunk" should be when
   * splitting. We set it to a specific size to optimize the tradeoff between
   * JS bundle size and number of HTTP round trips. There's likely a lot of
   * consideration that has gone into the specific number this is set to, so
   * only override this if you know what you're doing.
   */
  chunkGroupingSize: 500 * 1024, // 500kb

  /**
   * @see {@link https://rollupjs.org/guide/en/#treeshake}
   *
   * This tells Rollup to enable "tree shaking" which is both the chunking
   * algorithm and Dead Code Elimination.
   */
  treeshake: true,

  plugins: [
    /**
     * This is how we resolve modules from bare import specifiers. It is not
     * fully standard. There is currently no official standard for bare imports
     * but the defacto is Node's resolution which is to look inside
     * `node_modules`. We mostly follow this with a few caveats:
     *
     *  - Chiefly, we treat `app/assets/modules/` like we do `node_modules`, so a
     *    `import "foo"` will first check `app/assets/modules/foo` then
     *    `node_modules/foo`. This keeps specifier names nice and concise
     *    within `app/assets/modules`
     *  - We also respect the `module` field inside of a package.json which
     *    should point us to a file with ES2016 import/export syntax (as
     *    opposed to a commonjs file) - which Rollup handles better for tree shaking.
     *  - We also respect the `browser` field inside of a package.json which
     *    should point us to a file that does not use any Node.js modules - as
     *    an alternative to the `main` module which might.
     *  - We limit the allowed import to only be `.js` files. The default
     *    option is to look at `.mjs` files but we have no modules that use
     *    `.mjs` so this saves a bit of time.
     *  - The other options here are used to tweak the performance of this
     *    plugin, limiting the amount of times it does lookups on disk.
     *
     */
    resolve({
      dedupe: modules,
      mainFields: ['browser', 'module'],
      extensions: ['.js', '.ts'],
      customResolveOptions: {
        package: {},
        moduleDirectory: ['app/assets/modules', 'node_modules']
      }
    }),

    /**
     * This transform just strips TypeScript types from TypeScript files.
     *
     * It does not check for code or type errors. Instead run `bin/tsc` to
     * see any errors.
     */
    typescript(),

    /**
     * Some of the dependencies we have only use the legacy commonjs (Node.js
     * `require`) style module syntax. Rollup does not support this syntax out
     * of the box but does provide a plugin which will allow for it.
     *
     * We manually specifiy a list of node_modules we have which use commonjs.
     * We could just include `node_modules/**` but then A) the build takes
     * longer and B) we don't have a definitive list of modules to refactor ;).
     */
    common({
      include: [
        'node_modules/@github/braintree-encryption/**',
        'node_modules/@github/sortablejs/**',
        'node_modules/@github/zuorajs/**',
        'node_modules/@optimizely/**',
        'node_modules/assertion-error/**',
        'node_modules/axe-core/**',
        'node_modules/blakejs/**',
        'node_modules/chai/**',
        'node_modules/check-error/**',
        'node_modules/codemirror/**',
        'node_modules/codemirror-contrib/**',
        'node_modules/get-func-name/**',
        'node_modules/murmurhash/**',
        'node_modules/pathval/**',
        'node_modules/randomcolor/**',
        'node_modules/smoothscroll-polyfill/**',
        'node_modules/typed.js/**',
        'node_modules/tweetnacl/**',
        'node_modules/uuid/**',
        'node_modules/leven/**'
      ],
      namedExports: {
        chai: ['assert'],
        'node_modules/@github/zuorajs/zuora.js': ['Z'],
        '@optimizely/js-sdk-logging': [
          'ConsoleLogHandler',
          'getLogger',
          'setLogLevel',
          'LogLevel',
          'setLogHandler',
          'setErrorHandler',
          'getErrorHandler'
        ],
        '@optimizely/js-sdk-event-processor': ['LogTierV1EventProcessor', 'LocalStoragePendingEventsDispatcher']
      }
    }),

    /**
     * Terser minifies all of our code so that it's few bytes over the wire.
     *
     * Skipped in development because minifying is very slow.
     */
    terser({
      /**
       * By default it'll minify every class name and also every function name.
       * This breaks our custom elements though - which are not imported but
       * are implicit globals, and so need to be kept to their original names
       * so they can be correctly referenced. The `keep_fnames` option is set
       * to false, as we _want all_ function names to be compressed, while
       * `keep_classnames` is set to `true` as we want _all classnames_ to be
       * uncompressed in the minified output.
       */
      keep_fnames: false,
      keep_classnames: true,
      /**
       * Safari 10 has a bug where `if (!await foo())` will error. It instead
       * needs to be `if(!(await foo())`. Enabling the `safari10` option in
       * Terser means it'll minify code to ensure that this bug doesn't get
       * invoked.
       *
       * @see {@link https://bugs.webkit.org/show_bug.cgi?id=176685}
       */
      safari10: true
    }),

    // TODO: Turn this hack into a proper plugin
    /**
     * This plugin writes out a manifest file which is a JSON map of
     * entryName->hashedName to allow rails to know what the name of the
     * compiled bundle is.
     *
     * This plugin also does a check to ensure no "chunk" files have been made.
     * We do not yet have the infrastructure to support chunk files and so want
     * to avoid them. If this plugin detects a chunk file it'll stop the world
     * and fail the build.
     */
    {
      name: 'github-manifest',
      /**
       * Rollup cannot be trusted to make hashes that reflect the bytes on disk, and so
       * this function will capture the bundle before it is being written, and
       * write out the sourcemap and mutate the filename accordingly.
       *
       * @param {import('rollup').OutputOptions} opts
       * @param {import('rollup').OutputBundle} bundles
       */
      async generateBundle(
        opts /*: {sourcemap?: string | boolean, dir?: string } */,
        bundles /*: {[string]: RollupBundle} */
      ) {
        const {sourcemap, dir = ''} = opts
        opts.sourcemap = false
        for (const filename in bundles) {
          const bundle = bundles[filename]
          if ('isAsset' in bundle) continue
          if (sourcemap === true && bundle.map) {
            if (process.env.NODE_ENV !== 'development') bundle.map.sourcesContent = []
            const mapHash = createHash('sha512').update(bundle.map.toString()).digest('hex').substr(0, 8)
            const mapPath = `${bundle.name}-${mapHash}.js.map`
            mkdirSync(dir, {recursive: true})
            writeFileSync(path.join(dir, mapPath), bundle.map.toString())
            bundle.code += `//# sourceMappingURL=${mapPath}\n`
          }
          const bundleHash = createHash('sha512').update(bundle.code).digest('hex').substr(0, 8)
          bundle.fileName = `${bundle.name}-${bundleHash}${path.extname(filename)}`
        }
      },
      /**
       * @param {import('rollup').OutputBundle} bundles
       */
      writeBundle(bundles) {
        /** @type {Object.<string, string>} */
        let manifest = {}
        const manifestFile = `${directory}/manifest.json`
        try {
          manifest = JSON.parse(readFileSync(manifestFile, 'utf-8'))
        } catch {
          /* ignore */
        }
        let fail = false
        for (const file in bundles) {
          const bundle = bundles[file]
          if ('isAsset' in bundle) continue
          /** @type {null|string} */
          const entry = normaliseEntryName(file)
          if (!bundle.isEntry && !allowedChunkFiles.includes(entry)) {
            /** @type {{[key: string]: string[]}} */
            const moduleGroups = {nodeModules: [], otherModules: []}
            for (const modulePath in bundle.modules) {
              moduleGroups[modulePath.includes('node_modules') ? 'nodeModules' : 'otherModules'].push(modulePath)
            }
            let errorMessage = `\u001b[31m
FATAL ERROR!!

This compile would have created a new entry point which we don't yet support.
To fix this, try adding these modules to a named-chunks bundle in package.json.
Reach out to #js if you need help.

These are the most likely candidates:

`
            if (moduleGroups.nodeModules.length > 0) {
              errorMessage += `${moduleGroups.nodeModules.join('\n')}

If adding those doesn't work, here are some others:

`
            }

            errorMessage += moduleGroups.otherModules.join('\n')
            errorMessage += `\u001b[39m`
            console.error(errorMessage)
            fail = true
          }
          const circular = bundle.imports.find(module => {
            const importedBundle = bundles[module]
            if ('isAsset' in importedBundle) return false
            return importedBundle.imports.includes(file)
          })
          if (circular) {
            console.error(`\u001b[31m
FATAL ERROR!!

The ${file} bundle has a circular dependency ${circular}!
            \u001b[39m`)
            fail = true
          }
          manifest[entry] = bundle.fileName
          manifest[normaliseEntryName(entry)] = bundle.fileName
        }
        if (fail) {
          return process.exit(1)
        }
        writeFileSync(manifestFile, JSON.stringify(manifest, null, 2), 'utf-8')
      }
    },

    // TODO: Turn this hack into a proper plugin
    /**
     * This plugin takes the bundle name and creates a symlinked file that
     * points to the hashed name. So when Rollup takes a file like
     * `github-bootstrap.js` and turns it into `github-bootstrap-deadbeef.js`,
     * this plugin will ensure that a symlink of `github-bootstrap.js` exists
     * as a simlink to `github-bootstrap-deadbeef.js`.
     */
    {
      name: 'github-symlink',
      /**
       * @param {string} id
       */
      watchChange(id /*: string */) {
        try {
          const file = symlinkEntries.get(id)
          if (!file) return
          unlinkSync(file)
        } catch (e) {
          /* ignore */
        }
      },
      /**
       * @param {import('rollup').OutputBundle} bundles
       */
      writeBundle(bundles) {
        for (const file in bundles) {
          const bundle = bundles[file]
          if ('isAsset' in bundle) continue
          /** @type {string|null} */
          const entry = normaliseEntryName(file)
          const symlinkPath = `${directory}/${entry}`
          // write symlink
          try {
            unlinkSync(`${symlinkPath}`)
          } catch (e) {
            /* ignore */
          }
          symlinkSync(bundle.fileName, `${symlinkPath}`, 'file')
          for (const mf in bundle.modules) {
            symlinkEntries.set(mf, `${symlinkPath}`)
          }
        }
      }
    }
  ],

  watch: {
    /**
     * Chokidar is a module which improves the Node.js built in file-watcher,
     * fixing many bugs within what is a locked Node.js built in. We want to
     * enable this to eradicate bugs in file watching, but it is not enabled by
     * default by Rollup.
     */
    chokidar: true,
    /**
     * As this build is run as part of a sub-process, we don't want to clear
     * the screen on subsequent compiles as that will clear logs from other
     * processes.
     */
    clearScreen: false
  },
  output: {
    /**
     * We want to enable sourcemaps as they're useful for debugging. Rollup
     * does not enable them by default as it adds to compile times.
     */
    sourcemap: true,
    /**
     * Strips relative directory traversal from source path file names so we
     * can resolve exception stack traces to teams in CODEOWNERS.
     *
     * "../../app/assets/modules/github/html-safe.ts" -> "app/assets/modules/github/html-safe.ts".
     *
     * @param {string} sourcePath
     */
    sourcemapPathTransform: sourcePath => {
      const root = path.resolve(directory)
      const absolute = path.resolve(root, sourcePath)
      return path.relative(process.cwd(), absolute)
    },
    dir: directory,
    /**
     * These options define the "file name format" of the output files.
     */
    entryFileNames: '[name].js',
    chunkFileNames: '[name].js',
    assetFileNames: '[name][extname]',
    /**
     * By default we use `iife` (Immediately Invoked Function Expression) as an
     * output format because most bundles will likely shrink down to a single
     * file which does not need to have any module system in place. This is
     * overridden from specific bundles.
     */
    format: 'iife'
  }
}

if (process.env.NODE_ENV === 'development') {
  // Remove slow plugins in development.
  options.plugins = withoutPlugins(['terser'])

  // Write lockfiles to output directory while compiling to help asset-server
  // know when it can serve updated files
  options.plugins.push(lockfile({dir: directory}))
}

/**
 * Filter list of plugins by name.
 *
 * @param {string[]} names - The names of plugins to exclude.
 * @returns {Array<{name: string}>} - A filtered plugin set.
 */
function withoutPlugins(names /*: string[]) */) /*: Array<{name: string}> */ {
  return options.plugins.filter(({name}) => !names.includes(name))
}

const environmentBundle = {
  ...options,
  plugins: withoutPlugins(['commonjs']),
  input: [popBundle('app/assets/modules/environment-bootstrap.ts')]
}

const compatBundle = {
  ...options,
  plugins: withoutPlugins(['commonjs']),
  input: [popBundle('app/assets/modules/compat-bootstrap.ts')]
}

const marketingBundle = {
  ...options,
  input: [popBundle('app/assets/modules/marketing-bootstrap.ts')]
}

const unsupportedBundle = {
  ...options,
  plugins: withoutPlugins(['commonjs']),
  input: [popBundle('app/assets/modules/unsupported-bootstrap.ts')]
}

const styleGuideBundle = {
  ...options,
  input: [popBundle('app/assets/modules/styleguide-bootstrap.ts')]
}

const staffBundle = {
  ...options,
  plugins: withoutPlugins(['commonjs']),
  input: [popBundle('app/assets/modules/github/staff.ts')]
}

const developmentBundle = {
  ...options,
  input: [popBundle('app/assets/modules/development-bootstrap.ts')]
}

const gistVendor = new Set([
  'app/assets/modules/github/google-analytics.ts',
  '@github/query-selector',
  'selector-observer'
])

const gistBundle = {
  ...options,
  /** @param {string} modulePath */
  manualChunks: (modulePath /*: string*/) => {
    const moduleId = path.relative(process.cwd(), modulePath)
    if (isPackage(moduleId, gistVendor) || gistVendor.has(moduleId)) return 'gist-vendor'
    return null
  },
  input: [popBundle('app/assets/modules/gist-bootstrap.ts')],
  output: [
    {
      ...options.output,
      format: 'system'
    }
  ]
}

const gistEditorBundle = {
  ...options,
  input: [popBundle('app/assets/modules/gist-editor-bootstrap.ts')],
  output: [
    {
      ...options.output,
      format: 'system'
    }
  ]
}

const mobileBundle = {
  ...options,
  plugins: withoutPlugins(['commonjs']),
  input: [popBundle('app/assets/modules/mobile-bootstrap.ts')]
}

const socketBundle = {
  ...options,
  plugins: withoutPlugins(['commonjs']),
  input: [popBundle('app/assets/modules/github/socket-worker.ts')]
}

const memexFrontEndBundle = {
  ...options,
  input: [popBundle('app/assets/modules/github/memex.ts')]
}

const optimizelyBundle = {
  ...options,
  plugins: [
    // The main resolve plugin configured above sets 'browser' as the preferred entrypoint,
    // but since optimizely ships browser as UMD, we must prefer 'module' (which is ESM).
    // node-resolve's default preference is module then browser,
    // so we need to use the node-resolve plugin with its defaults;
    // not as configured for other bundles.
    resolve({mainFields: ['module', 'browser']}),
    ...withoutPlugins(['node-resolve'])
  ],
  input: [popBundle('app/assets/modules/optimizely-bootstrap.ts')]
}

/**
 * @param {string} moduleId
 * @param {string[] | Set<string>} entries
 * @return boolean
 */
function isPackage(moduleId, entries) {
  for (const name of entries) {
    if (moduleId.startsWith(`node_modules/${name}/`)) {
      return true
    }
  }
  return false
}

const mainBundle = {
  ...options,
  /** @param {string} modulePath */
  manualChunks: modulePath => {
    // Some chunks generated by rollup like to put a null byte as the first character,
    // which is a useful sentinel for transpilers to ignore, but useless for us trying
    // to put these modules into a bundle, so we need to strip it
    if (modulePath[0] === '\x00') modulePath = modulePath.slice(1)

    const moduleId = path.relative(process.cwd(), modulePath)
    for (const [name, entries] of Object.entries(pkgJson['named-chunks'])) {
      const isApp = entries.includes(moduleId)
      if (isPackage(moduleId, entries) || isApp) {
        return path.basename(name, '.js')
      }
    }
    return null
  },
  input: [...mainBundles],
  output: [
    {
      ...options.output,
      format: 'system'
    }
  ]
}

const testBundle = {
  ...options,
  input: ['test/js/test.js']
}

const bundles = []

if (process.env.ROLLUP_TARGET === 'test') {
  bundles.push(compatBundle, testBundle)
} else {
  bundles.push(
    environmentBundle,
    mainBundle,
    compatBundle,
    marketingBundle,
    unsupportedBundle,
    styleGuideBundle,
    mobileBundle,
    socketBundle,
    staffBundle,
    developmentBundle,
    gistBundle,
    gistEditorBundle,
    memexFrontEndBundle,
    optimizelyBundle
  )
}

export default bundles
