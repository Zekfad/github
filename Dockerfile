FROM ruby:2.5-slim-stretch

ARG GITHUB_TOKEN
ARG GITHUB_TOKEN_URL="https://git:$GITHUB_TOKEN@github.com"
ARG LANG=C.UTF-8
ARG RAILS_ENV=test
ARG GITHUB_CI=0
ARG GO_VERSION=1.11.1
ENV PATH=$PATH:/usr/local/go/bin
RUN apt-get update && apt-get install -y --no-install-recommends \
  curl \
  bash \
  g++ \
  make \
  git \
  openssh-client \
  time \
  libcurl4-openssl-dev \
  libexpat1-dev \
  autopoint \
  gettext \
  libjansson-dev \
  bc \
  autoconf \
  libjemalloc-dev \
  bison \
  libicu-dev \
  cmake \
  pkg-config \
  libsasl2-dev \
  libreadline-dev \
  netcat \
  sysstat \
  rsync \
  pigz \
  rsync \
  software-properties-common \
  lsb-release \
  gnupg2 \
  libsodium-dev

RUN curl -L -O https://dl.google.com/go/go$GO_VERSION.linux-amd64.tar.gz &&\
  tar -C /usr/local -xzf go$GO_VERSION.linux-amd64.tar.gz &&\
  /usr/local/go/bin/go version &&\
  echo 'export PATH=$PATH:/usr/local/go/bin' >> /etc/profile && \
  rm go$GO_VERSION.linux-amd64.tar.gz

COPY test/db/conf.d/github.cnf /etc/mysql/conf.d/github.cnf
RUN curl -L -O https://repo.percona.com/apt/percona-release_latest.stretch_all.deb && \
    dpkg -i percona-release_latest.stretch_all.deb && \
    rm percona-release_latest.stretch_all.deb && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install libperconaserverclient20 libperconaserverclient20-dev percona-server-server-5.7 percona-server-common-5.7 percona-server-client-5.7

# Cleanup apt cruft
RUN apt -y autoremove && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*


RUN echo $GITHUB_TOKEN_URL > /root/.git-credentials
RUN git config --global credential.helper store
RUN chmod 755 /usr/local/bundle
COPY . /github/
RUN mkdir /github/log
RUN touch /github/public/robots.txt
RUN find /var/lib/mysql -type f -exec touch {} \; && \
    service mysql start && \
    cd /github &&\
    script/bootstrap --local --deployment &&\
    rm /root/.git-credentials
WORKDIR /github
